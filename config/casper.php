<?php

return [
    'upload'           => env('CASPER_UPLOAD'),
    'download'         => env('CASPER_DOWNLOAD'),
    'scenario_url'     => env('CASPER_URL'),
    'scenario_one_url' => env('CASPER_ONE_URL'),
    'scenario_two_url' => env('CASPER_TWO_URL'),
    'scenario2_one_url' => env('CASPER_ONE_URL_NEW'),
    'scenario2_two_url' => env('CASPER_TWO_URL_NEW'),
    'scenario3_one_url' => env('CASPER_ONE_URL_3'),
    'scenario3_two_url' => env('CASPER_TWO_URL_3'),
];
