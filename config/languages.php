<?php

return [
	[
		'code' => 'all',
		'name' => 'All Languages'
	],
	[
		'code' => 'ar',
		'name' => 'Arabic (العربية)'
	],
	[
		'code' => 'bg',
		'name' => 'Bulgarian (Български)'
	],
	[
		'code' => 'ca',
		'name' => 'Catalan (Català)'
	],
	[
		'code' => 'cs',
		'name' => 'Czech (Čeština)'
	],
	[
		'code' => 'da',
		'name' => 'Danish (Dansk)'
	],
	[
		'code' => 'de',
		'name' => 'German (Deutsch)'
	],
	[
		'code' => 'el',
		'name' => 'Greek (ελληνικά)'
	],
	[
		'code' => 'en',
		'name' => 'English'
	],
	[
		'code' => 'es',
		'name' => 'Spanish (Español)'	
	],
	[
		'code' => 'et',
		'name' => 'Estonian (Eesti)'
	],
	[
		'code' => 'fi',
		'name' => 'Finnish (Suomi)'
	],
	[
		'code' => 'fr',
		'name' => 'French (Français)'
	],
	[
		'code' => 'hi',
		'name' => 'Hindi (हिंदी)'
	],
	[
		'code' => 'hr',
		'name' => 'Croatian (Hrvatski)'
	],
	[
		'code' => 'hu',
		'name' => 'Hungarian (Magyar)'
	],
	[
		'code' => 'id',
		'name' => 'Indonesian (Bahasa Indonesia)'
	],
	[
		'code' => 'is',
		'name' => 'Icelandic'
	],
	[
		'code' => 'it',
		'name' => 'Italian (Italiano)'
	],
	[
		'code' => 'iw',
		'name' => 'Hebrew (עברית)'
	],
	[
		'code' => 'ja',
		'name' => 'Japanese (日本語)'
	],
	[
		'code' => 'ko',
		'name' => 'Korean (한국어)'
	],
	[
		'code' => 'lt',
		'name' => 'Lithuanian (Lietuvių Kalba)'
	],
	[
		'code' => 'lv',
		'name' => 'Latvian (Latviešu Valoda)'
	],
	[
		'code' => 'ms',
		'name' => 'Malay (Bahasa Melayu)'
	],
	[
		'code' => 'nl',
		'name' => 'Dutch (Nederlands)'
	],
	[
		'code' => 'no',
		'name' => 'Norwegian (Norsk)'
	],
	[
		'code' => 'pl',
		'name' => 'Polish (Polski)'
	],
	[
		'code' => 'pt',
		'name' => 'Portuguese (Português)'
	],
	[
		'code' => 'ro',
		'name' => 'Romanian (Român)'
	],
	[
		'code' => 'ru',
		'name' => 'Russian (Русский)'
	],
	[
		'code' => 'sk',
		'name' => 'Slovak (Slovenský)'
	],
	[
		'code' => 'sl',
		'name' => 'Slovenian (Slovenščina)'
	],
	[
		'code' => 'sr',
		'name' => 'Serbian (Cрпски)'
	],
	[
		'code' => 'sv',
		'name' => 'Swedish (Svenska)'
	],
	[
		'code' => 'th',
		'name' => 'Thai (ภาษาไทย)'
	],
	[
		'code' => 'tl',
		'name' => 'Filipino (Tagalog)'
	],
	[
		'code' => 'tr',
		'name' => 'Turkish (Türk)'
	],
	[
		'code' => 'uk',
		'name' => 'Ukrainian (Українська)'
	],
	[
		'code' => 'ur',
		'name' => 'Urdu (اُردُو‎)'
	],
	[
		'code' => 'vi',
		'name' => 'Vietnamese (Việt)'
	],
	[
		'code' => 'zh-CN',
		'name' => 'Chinese - Simplified (中国 - 简体)'
	],
	[
		'code' => 'zh-TW',
		'name' => 'Chinese - Traditional (中文 - 繁體)'
    ],
];