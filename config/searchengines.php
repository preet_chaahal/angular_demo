<?php

return [
    [
        'name' => 'All Locations',
        'engine' => 'google.com',
        'short_form' => 'all'
    ],
    [
        'name' => 'Vietnam',
        'engine' => 'google.com.vn',
        'short_form' => 'vn'
    ],
    [
        'name' => 'United States',
        'engine' => 'google.com',
        'short_form' => 'us'
    ],
    [
        'name' => 'United Kingdom',
        'engine' => 'google.co.uk',
        'short_form' => 'gb'
    ],
    [
        'name' => 'Canada',
        'engine' => 'google.ca',
        'short_form' => 'ca'
    ],
    [
        'name' => 'India',
        'engine' => 'google.co.in',
        'short_form' => 'in'
    ],
    [
        'name' => 'Philippines',
        'short_form' => 'ph',
        'engine' => 'google.com.ph'
    ],
    [
        'name' => 'Slovenia',
        'engine' => 'google.si',
        'short_form' => 'si'
    ],
    [
        'name' => 'Spain',
        'engine' => 'google.es',
        'short_form' => 'es'
    ],
    [
        'name' => 'Malaysia',
        'engine' => 'google.com.my',
        'short_form' => 'my'
    ],
    [
        'name' => 'Indonesia',
        'engine' => 'google.co.id',
        'short_form' => 'id',
    ],
    [
        'name' => 'Australia',
        'engine' => 'google.com.au',
        'short_form' => 'au'
    ],
    [
        'name' => 'Singapore',
        'short_form' => 'sg',
        'engine' => 'google.com.sg'
    ],
    [
        'name' => 'Brazil',
        'engine' => 'google.com.br',
        'short_form' => 'br'
    ],
    [
        'name' => 'Argentina',
        'engine' => 'google.com.ar',
        'short_form' => 'ar'
    ],
    [
        'name' => 'Sweden',
        'engine' => 'google.se',
        'short_form' => 'se'
    ],
    [
        'name' => 'France',
        'engine' => 'google.fr',
        'short_form' => 'fr'
    ],
    [
        'name' => 'Italy',
        'engine' => 'google.it',
        'short_form' => 'it'
    ],
    [
        'name' => 'Germany',
        'engine' => 'google.de',
        'short_form' => 'de'
    ],
    [
        'name' => 'Dominican Republic',
        'engine' => 'google.com.do',
        'short_form' => 'do'
    ],
    [
        'name' => 'Switzerland',
        'engine' => 'google.ch',
        'short_form' => 'ch',
    ],
    [
        'name' => 'Poland',
        'engine' => 'google.pl',
        'short_form' => 'pl'
    ],
    [
        'name' => 'Ireland',
        'engine' => 'google.ie',
        'short_form' => 'ie',
    ],
    [
        'name' => 'Lithuania',
        'engine' => 'google.lt',
        'short_form' => 'lt'
    ],
    [
        'name' => 'Netherlands',
        'engine' => 'google.nl',
        'short_form' => 'nl'
    ],
    [
        'name' => 'Portugal',
        'engine' => 'google.pt',
        'short_form' => 'pt'
    ],
    [
        'name' => 'Chile',
        'engine' => 'google.cl',
        'short_form' => 'cl'
    ],
    [
        'name' => 'Mexico',
        'engine' => 'google.com.mx',
        'short_form' => 'mx'
    ],
    [
        'name' => 'Estonia',
        'engine' => 'google.ee',
        'short_form' => 'ee'
    ],
    [
        'name' => 'United Arab Emirates',
        'engine' => 'google.ae',
        'short_form' => 'ae',
    ],
    [
        'name' => 'Turkey',
        'engine' => 'google.com.tr',
        'short_form' => 'tr',
    ],
    [
        'name' => 'Slovakia',
        'engine' => 'google.sk',
        'short_form' => 'sk'
    ],
    [
        'name' => 'Costa Rica',
        'engine' => 'google.co.cr',
        'short_form' => 'cr'
    ],
    [
        'name' => 'Sri Lanka',
        'engine' => 'google.lk',
        'short_form' => 'lk'
    ],
    [
        'name' => 'Belgium',
        'engine' => 'google.be',
        'short_form' => 'be'
    ],
    [
        'name' => 'Romania',
        'engine' => 'google.ro',
        'short_form' => 'ro'
    ],
    [
        'name' => 'Pakistan',
        'engine' => 'google.com.pk',
        'short_form' => 'pk'
    ],
    [
        'name' => 'Thailand',
        'engine' => 'google.co.th',
        'short_form' => 'th'
    ],
    [
        'name' => 'New Zealand',
        'engine' => 'google.co.nz',
        'short_form' => 'nz',
    ],
    [
        'name' => 'Saudi Arabia',
        'engine' => 'google.com.sa',
        'short_form' => 'sa'
    ],
    [
        'name' => 'Egypt',
        'engine' => 'google.com.eg',
        'short_form' => 'eg'
    ],
    [
        'name' => 'Greece',
        'engine' => 'google.gr',
        'short_form' => 'gr'
    ],
    [
        'name' => 'Ecuador',
        'engine' => 'google.com.ec',
        'short_form' => 'ec'
    ],
    [
        'name' => 'Israel',
        'engine' => 'google.co.il',
        'short_form' => 'il'
    ],
    [
        'name' => 'Denmark',
        'engine' => 'google.dk',
        'short_form' => 'dk'
    ],
    [
        'name' => 'South Africa',
        'engine' => 'google.co.za',
        'short_form' => 'za'
    ],
    [
        'name' => 'Hungary',
        'engine' => 'google.hu',
        'short_form' => 'hu'
    ],
    [
        'name' => 'Finland',
        'engine' => 'google.fi',
        'short_form' => 'fi'
    ],
    [
        'name' => 'Norway',
        'engine' => 'google.no',
        'short_form' => 'no'
    ],
    [
        'name' => 'Czech Republic',
        'engine' => 'google.cz',
        'short_form' => 'cz'
    ],
    [
        'name' => 'Bulgaria',
        'engine' => 'google.bg',
        'short_form' => 'bg'
    ],
    [
        'name' => 'Colombia',
        'engine' => 'google.com.co',
        'short_form' => 'co'
    ],
    [
        'name' => 'Hong Kong',
        'engine' => 'google.com.hk',
        'short_form' => 'HK',
    ],
    [
        'name' => 'Guatemala',
        'engine' => 'google.com.gt',
        'short_form' => 'gt'
    ],
    [
        'name' => 'Russia',
        'short_form' => 'ru',
        'engine' => 'google.ru'
    ],
    [
        'name' => 'Ukraine',
        'engine' => 'google.com.ua',
        'short_form' => 'ua'
    ],
    [
        'name' => 'Peru',
        'engine' => 'google.com.pe',
        'short_form' => 'pe'
    ],
    [
        'name' => 'Serbia',
        'engine' => 'google.rs',
        'short_form' => 'rs'
    ],
    [
        'name' => 'Latvia',
        'engine' => 'google.lv',
        'short_form' => 'lv'
    ],
    [
        'name' => 'Ethiopia',
        'short_form' => 'et',
        'engine' => 'google.com.et'
    ],
    [
        'name' => 'Croatia',
        'engine' => 'google.hr',
        'short_form' => 'hr'
    ],
    [
        'name' => 'Venezuela',
        'engine' => 'google.co.ve',
        'short_form' => 've'
    ],
    [
        'name' => 'Algeria',
        'engine' => 'google.dz',
        'short_form' => 'dz'
    ],
    [
        'name' => 'Morocco',
        'short_form' => 'ma',
        'engine' => 'google.co.ma'
    ],
    [
        'name' => 'Nigeria',
        'engine' => 'google.com.ng',
        'short_form' => 'ng'
    ],
    [
        'name' => 'Bangladesh',
        'engine' => 'google.com.bd',
        'short_form' => 'bd'
    ],
    [
        'name' => 'Puerto Rico',
        'engine' => 'google.com.pr',
        'short_form' => 'pr'
    ],
    [
        'name' => 'Georgia',
        'engine' => 'google.ge',
        'short_form' => 'ge'
    ],
    [
        'name' => 'Kuwait',
        'engine' => 'google.com.kw',
        'short_form' => 'kw'
    ],
    [
        'name' => 'Japan',
        'engine' => 'google.co.jp',
        'short_form' => 'jp'
    ],
    [
        'name' => 'Austria',
        'engine' => 'google.at',
        'short_form' => 'at'
    ],
    [
        'name' => 'Cyprus',
        'short_form' => 'cy',
        'engine' => 'google.com.cy'
    ],
    [
        'name' => 'Panama',
        'engine' => 'google.com.pa',
        'short_form' => 'pa'
    ],
    [
        'name' => 'Uruguay',
        'engine' => 'google.com.uy',
        'short_form' => 'uy'
    ],
    [
        'name' => 'South Korea',
        'engine' => 'google.co.kr',
        'short_form' => 'kr'
    ],
    [
        'name' => 'Bolivia',
        'engine' => 'google.com.bo',
        'short_form' => 'bo'
    ],
    [
        'name' => 'Tunisia',
        'engine' => 'google.tn',
        'short_form' => 'tn'
    ],
    [
        'name' => 'Kenya',
        'engine' => 'google.co.ke',
        'short_form' => 'ke'
    ],
      [
        'name' => 'Lebanon',
        'engine' => 'google.com.lb',
        'short_form' => 'lb'
    ],
    [
        'name' => 'Taiwan',
        'engine' => 'google.com.tw',
        'short_form' => 'tw'
    ],
        [
        'name' => 'Paraguay',
        'engine' => 'google.com.py',
        'short_form' => 'py'
    ],
];