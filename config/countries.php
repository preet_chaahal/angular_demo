<?php

return [
    [
        'name' => 'All Locations',
        'code' => 'all'
    ],
    [
        'name' => 'Vietnam',
        'code' => 'vn'
    ],
    [
        'name' => 'United States',
        'code' => 'us'
    ],
    [
        'name' => 'United Kingdom',
        'code' => 'uk'
    ],
    [
        'name' => 'Canada',
        'code' => 'ca'
    ],
    [
        'name' => 'India',
        'code' => 'in'
    ],
    [
        'name' => 'Philippines',
        'code' => 'ph'
    ],
    [
        'name' => 'Slovenia',
        'code' => 'si'
    ],
    [
        'name' => 'Spain',
        'code' => 'es'
    ],
    [
        'name' => 'Malaysia',
        'code' => 'my'
    ],
    [
        'name' => 'Indonesia',
        'code' => 'id',
    ],
    [
        'name' => 'Australia',
        'code' => 'au'
    ],
    [
        'name' => 'Singapore',
        'code' => 'sg'
    ],
    [
        'name' => 'Brazil',
        'code' => 'br'
    ],
    [
        'name' => 'Argentina',
        'code' => 'ar'
    ],
    [
        'name' => 'Sweden',
        'code' => 'se'
    ],
    [
        'name' => 'France',
        'code' => 'fr'
    ],
    [
        'name' => 'Italy',
        'code' => 'it'
    ],
    [
        'name' => 'Vietnam',
        'code' => 'vn'
    ],
    [
        'name' => 'Germany',
        'code' => 'de'
    ],
    [
        'name' => 'Dominican Republic',
        'code' => 'do'
    ],
    [
        'name' => 'Switzerland',
        'code' => 'ch',
    ],
    [
        'name' => 'Poland',
        'code' => 'pl'
    ],
    [
        'name' => 'Irelandآ',
        'code' => 'ie',
    ],
    [
        'name' => 'Lithuaniaآ',
        'code' => 'lt'
    ],
    [
        'name' => 'Netherlands',
        'code' => 'nl'
    ],
    [
        'name' => 'Portugal',
        'code' => 'pt'
    ],
    [
        'name' => 'Chile',
        'code' => 'cl'
    ],
    [
        'name' => 'Mexico',
        'code' => 'mx'
    ],
    [
        'name' => 'Estonia',
        'code' => 'ee'
    ],
    [
        'name' => 'United Arab Emirates',
        'code' => 'ae',
    ],
    [
        'name' => 'Turkey',
        'code' => 'tr',
    ],
    [
        'name' => 'Slovakia',
        'code' => 'sk'
    ],
    [
        'name' => 'Costa Rica',
        'code' => 'cr'
    ],
    [
        'name' => 'Sri Lanka',
        'code' => 'lk'
    ],
    [
        'name' => 'Belgium',
        'code' => 'be'
    ],
    [
        'name' => 'Romania',
        'code' => 'ro'
    ],
    [
        'name' => 'Pakistan',
        'code' => 'pk'
    ],
    [
        'name' => 'Thailand',
        'code' => 'th'
    ],
    [
        'name' => 'New Zealand',
        'code' => 'nz',
    ],
    [
        'name' => 'Saudi Arabia',
        'code' => 'sa'
    ],
    [
        'name' => 'Egypt',
        'code' => 'eg'
    ],
    [
        'name' => 'Greece',
        'code' => 'gr'
    ],
    [
        'name' => 'Ecuador',
        'code' => 'ec'
    ],
    [
        'name' => 'Israel',
        'code' => 'il'
    ],
    [
        'name' => 'Denmark',
        'code' => 'dk'
    ],
    [
        'name' => 'South Africa',
        'code' => 'za'
    ],
    [
        'name' => 'Hungary',
        'code' => 'hu'
    ],
    [
        'name' => 'Finland',
        'code' => 'fi'
    ],
    [
        'name' => 'Norway',
        'code' => 'no'
    ],
    [
        'name' => 'Czech Republic',
        'code' => 'cz'
    ],
    [
        'name' => 'Bulgaria',
        'code' => 'bg'
    ],
    [
        'name' => 'Colombia',
        'code' => 'co'
    ],
    [
        'name' => 'Hong Kong',
        'code' => 'hk',
    ],
    [
        'name' => 'Guatemala',
        'code' => 'gt'
    ],
    [
        'name' => 'Russia',
        'code' => 'ru'
    ],
    [
        'name' => 'Ukraine',
        'code' => 'ua'
    ],
    [
        'name' => 'Peru',
        'code' => 'pe'
    ],
    [
        'name' => 'Serbia',
        'code' => 'rs'
    ],
    [
        'name' => 'Latvia',
        'code' => 'lv'
    ],
    [
        'name' => 'Ethiopia',
        'code' => 'et'
    ],
    [
        'name' => 'Croatia',
        'code' => 'hr'
    ],
    [
        'name' => 'Venezuela',
        'code' => 've'
    ],
    [
        'name' => 'Algeria',
        'code' => 'dz'
    ],
    [
        'name' => 'Morocco',
        'code' => 'ma'
    ],
    [
        'name' => 'Nigeria',
        'code' => 'ng'
    ],
    [
        'name' => 'Bangladesh',
        'code' => 'bd'
    ],
    [
        'name' => 'Puerto Rico',
        'code' => 'pr'
    ],
    [
        'name' => 'Georgia',
        'code' => 'ge'
    ],
    [
        'name' => 'Kuwait',
        'code' => 'kw'
    ],
    [
        'name' => 'Japan',
        'code' => 'jp'
    ],
    [
        'name' => 'Austria',
        'code' => 'at'
    ],
    [
        'name' => 'Cyprus',
        'code' => 'cy'
    ],
    [
        'name' => 'Panama',
        'code' => 'pa'
    ],
    [
        'name' => 'Uruguay',
        'code' => 'uy'
    ],
    [
        'name' => 'South Korea',
        'code' => 'kr'
    ],
    [
        'name' => 'Bolivia',
        'code' => 'bo'
    ],
];