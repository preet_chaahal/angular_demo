<?php

namespace App;

/**
 * App\Brainstorm
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $keyword
 * @property integer $group_count
 * @property integer $keyword_count
 * @property string $data
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read User $user
 * @method static \Illuminate\Database\Query\Builder|\App\Brainstorm whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Brainstorm whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Brainstorm whereKeyword($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Brainstorm whereGroupCount($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Brainstorm whereKeywordCount($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Brainstorm whereData($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Brainstorm whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Brainstorm whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string $cpc_data
 * @method static \Illuminate\Database\Query\Builder|\App\Brainstorm whereCpcData($value)
 */
class Brainstorm extends BaseModel
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'brainstorm';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'keyword', 'group_count', 'keyword_count', 'data', 'cpc_data'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];


    /**
     * the brainstorm item belongs to a user
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
