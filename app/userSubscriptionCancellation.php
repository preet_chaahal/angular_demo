<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class userSubscriptionCancellation extends Model
{
    protected $table = 'user_subscription_cancellations';

    protected $fillable = ['user_id', 'plan_id'];
}
