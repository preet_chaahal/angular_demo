<?php

namespace App;

/**
 * App\EvaluationsUrl
 *
 * @mixin \Eloquent
 */
class UnsavedEvaluationsUrl extends BaseModel
{
    protected $table = 'unsaved_evaluations_url';

    protected $fillable = [
        'keyword_unsaved_research_id',
        'top_ten_url',
        'page_authority',
        'domain_authority',
        'back_links',
        'moz_rank',
        'pinterest_share',
        'google_plus_1s',
        'fb_share',
        'fb_like',
        'tweets',
        'url',
        'title',
        'desc',
        'h1'
    ];
}