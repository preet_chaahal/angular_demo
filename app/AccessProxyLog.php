<?php
/**
 * Created by PhpStorm.
 * User: harshadvala
 * Date: 1/23/2016
 * Time: 9:57 AM
 */

namespace App;


/**
 * App\AccessProxyLog
 *
 * @property integer $id
 * @property string $ip
 * @property string $error_code
 * @property string $error
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\SProxy $proxy
 * @method static \Illuminate\Database\Query\Builder|\App\AccessProxyLog whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\AccessProxyLog whereIp($value)
 * @method static \Illuminate\Database\Query\Builder|\App\AccessProxyLog whereErrorCode($value)
 * @method static \Illuminate\Database\Query\Builder|\App\AccessProxyLog whereError($value)
 * @method static \Illuminate\Database\Query\Builder|\App\AccessProxyLog whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\AccessProxyLog whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property integer $proxy_id
 * @method static \Illuminate\Database\Query\Builder|\App\AccessProxyLog whereProxyId($value)
 */
class AccessProxyLog extends BaseModel
{
    protected $table = 'tbl_access_proxy_logs';


    protected $fillable = ['ip', 'error_code', 'error'];

    public function proxy()
    {
        return $this->belongsTo(SProxy::class, 'url', 'ip');
    }
}