<?php
/**
 * Created by PhpStorm.
 * User: harshadvala
 * Date: 1/20/2016
 * Time: 6:42 PM
 */

namespace App;

/**
 * App\EvaluationsUrl
 *
 * @property integer $id
 * @property integer $keyword_research_id
 * @property string $top_ten_url
 * @property integer $page_authority
 * @property integer $domain_authority
 * @property integer $back_links
 * @property integer $moz_rank
 * @property integer $page_rank
 * @property integer $google_plus_1s
 * @property integer $fb_share
 * @property integer $fb_like
 * @property integer $tweets
 * @property boolean $url
 * @property boolean $title
 * @property boolean $desc
 * @property boolean $h1
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\EvaluationsUrl whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\EvaluationsUrl whereKeywordResearchId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\EvaluationsUrl whereTopTenUrl($value)
 * @method static \Illuminate\Database\Query\Builder|\App\EvaluationsUrl wherePageAuthority($value)
 * @method static \Illuminate\Database\Query\Builder|\App\EvaluationsUrl whereDomainAuthority($value)
 * @method static \Illuminate\Database\Query\Builder|\App\EvaluationsUrl whereBackLinks($value)
 * @method static \Illuminate\Database\Query\Builder|\App\EvaluationsUrl whereMozRank($value)
 * @method static \Illuminate\Database\Query\Builder|\App\EvaluationsUrl wherePageRank($value)
 * @method static \Illuminate\Database\Query\Builder|\App\EvaluationsUrl whereGooglePlus1s($value)
 * @method static \Illuminate\Database\Query\Builder|\App\EvaluationsUrl whereFbShare($value)
 * @method static \Illuminate\Database\Query\Builder|\App\EvaluationsUrl whereFbLike($value)
 * @method static \Illuminate\Database\Query\Builder|\App\EvaluationsUrl whereTweets($value)
 * @method static \Illuminate\Database\Query\Builder|\App\EvaluationsUrl whereUrl($value)
 * @method static \Illuminate\Database\Query\Builder|\App\EvaluationsUrl whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\App\EvaluationsUrl whereDesc($value)
 * @method static \Illuminate\Database\Query\Builder|\App\EvaluationsUrl whereH1($value)
 * @method static \Illuminate\Database\Query\Builder|\App\EvaluationsUrl whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\EvaluationsUrl whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class EvaluationsUrl extends BaseModel
{
    protected $table = 'evaluations_url';

    protected $fillable = [
        'keyword_research_id',
        'top_ten_url',
        'page_authority',
        'domain_authority',
        'back_links',
        'moz_rank',
        'pinterest_share',
        'google_plus_1s',
        'fb_share',
        'fb_like',
        'tweets',
        'url',
        'title',
        'desc',
        'h1'
    ];
}