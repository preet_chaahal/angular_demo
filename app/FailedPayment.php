<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FailedPayment extends Model
{
    protected $table = 'failed_payments';

    protected $fillable = ['gateway','user_id','invoice_id','customer','downgraded'];
    
    /**
     * Get the user record associated with the failed_payment.
     */
    public function user(){
      return $this->belongsTo('App\User');
    }
}
