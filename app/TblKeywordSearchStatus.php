<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\TblEvaluationStatus
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $keyword_search_id
 * @property integer $evaluate_id
 * @property string $keyword
 * @property integer $monthly_searches
 * @property float $keyword_cpc
 * @property float $estimated_profile
 * @property integer $word_count
 * @property string $google_engine
 * @property integer $user_current_level
 * @property integer $difficulty
 * @property string $location
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\TblKeywordStatus $keyword_status
 * @method static \Illuminate\Database\Query\Builder|\App\TblEvaluationStatus whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\TblEvaluationStatus whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\TblEvaluationStatus whereKeywordSearchId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\TblEvaluationStatus whereEvaluateId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\TblEvaluationStatus whereKeyword($value)
 * @method static \Illuminate\Database\Query\Builder|\App\TblEvaluationStatus whereMonthlySearches($value)
 * @method static \Illuminate\Database\Query\Builder|\App\TblEvaluationStatus whereKeywordCpc($value)
 * @method static \Illuminate\Database\Query\Builder|\App\TblEvaluationStatus whereEstimatedProfile($value)
 * @method static \Illuminate\Database\Query\Builder|\App\TblEvaluationStatus whereWordCount($value)
 * @method static \Illuminate\Database\Query\Builder|\App\TblEvaluationStatus whereGoogleEngine($value)
 * @method static \Illuminate\Database\Query\Builder|\App\TblEvaluationStatus whereUserCurrentLevel($value)
 * @method static \Illuminate\Database\Query\Builder|\App\TblEvaluationStatus whereDifficulty($value)
 * @method static \Illuminate\Database\Query\Builder|\App\TblEvaluationStatus whereLocation($value)
 * @method static \Illuminate\Database\Query\Builder|\App\TblEvaluationStatus whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\TblEvaluationStatus whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class TblKeywordSearchStatus extends Model
{
    protected $primaryKey = 'id';

    protected $table = 'tbl_keyword_search_status';
    
}
