<?php
/**
 * Created by PhpStorm.
 * User: harshadvala
 * Date: 1/7/2016
 * Time: 12:59 PM
 */

namespace App\Repositories;

use App\RankCheck;

class AdminRankRepository
{
    public function all()
    {
        return RankCheck::all();
    }

    public function find($id)
    {
        return RankCheck::where('id', $id)->first();
    }

    public function paginate($numOfPages)
    {
        return RankCheck::paginate($numOfPages);
    }
}