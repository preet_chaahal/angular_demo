<?php namespace App\Repositories;

use App\User;

/**
 * Class AdminRepository
 * @package App\Repositories
 */
class UserRepository
{
    public function all()
    {
        return User::all();
    }

    public function paginate($numOfPages)
    {
        return User::paginate($numOfPages);
    }

    public function find($id)
    {
        return User::where('id', $id)->first();
    }

    public function get($start, $length, $search, $order)
    {
        $titles = [
            'name',
            'username',
            'email',
            'stripe_plan',
            'payment_type',
            'stripe_id',
            'stripe_subscription',
            'is_admin',
            'disabled',
            'toggle_free',
            'created_at'
        ];

        return User::where($titles[0], 'LIKE', "%{$search['value']}%")
            ->orWhere($titles[1], 'LIKE', "%{$search['value']}%")
            ->orWhere($titles[2], 'LIKE', "%{$search['value']}%")
            ->orWhere($titles[3], 'LIKE', "%{$search['value']}%")
            ->orWhere($titles[4], 'LIKE', "%{$search['value']}%")
            ->orWhere($titles[5], 'LIKE', "%{$search['value']}%")
            ->orWhere($titles[6], 'LIKE', "%{$search['value']}%")
            ->orderBy($titles[ $order[0]['column'] ], $order[0]['dir'])
            ->skip($start)
            ->take($length)
            ->get();
    }

    public function save($input)
    {
        if (isset($input['id']) && ! empty(isset($input['id']))) {

            // UPDATE
            $record = User::where('id', intval($input['id']))->first();
            $record->setRawAttributes($input);
            $record->save();

            return $record;
        } else {

            // CREATE
            $record = new User($input);
            $record->save();

            return $record;
        }
    }

    public function delete($id)
    {
        return User::where('id', $id)->delete();
    }

}
