<?php
/**
 * Created by PhpStorm.
 * User: harshadvala
 * Date: 1/7/2016
 * Time: 12:38 PM
 */

namespace App\Repositories;


use App\Brainstorm;
use App\Utilities\Suggestions;
use RecursiveArrayIterator;
use RecursiveIteratorIterator;

class AdminBrainstormRepository
{
    public function all()
    {
        return Brainstorm::all();

    }

    public function find($id)
    {
        return Brainstorm::where('id', $id)->first();

    }

    public function save($input)
    {
        if (isset($input['id']) && ! empty(isset($input['id']))) {

            // UPDATE
            $proxy = Brainstorm::where('id', intval($input['id']))->first();
            $proxy->setRawAttributes($input);
            $proxy->save();

            return $proxy;
        } else {

            // CREATE
            $proxy = new Brainstorm($input);
            $proxy->save();

            return $proxy;
        }
    }

    public function delete($id)
    {
        /** @var Brainstorm $record */
        $record = Brainstorm::where('id', $id)->first();
        if ( ! empty($record)) {
            return $record->delete();
        }

        return $record;

    }

    public function getSuggestions($brainstormId)
    {
        $brainstorm = Brainstorm::where('id', $brainstormId)->first();

        // how many levels can we go? (subgroups)
        $depth = 3;
        // our suggestion class
        $suggestions = new Suggestions;

        // ensure we have a valid brainstorm item
        if ( ! $brainstorm) {
            return;
        }

        // let's grab some real data
        $keywords           = $suggestions->scrape($brainstorm->keyword, $depth);
        $recur_flat_arr_obj = new RecursiveIteratorIterator(new RecursiveArrayIterator($keywords));
        $keywords           = $recur_flat_arr_obj->getArrayCopy();
        $json_array         = array();

        // ensure we have a proper array to convert into D3 JSON
        if (count($keywords) > 0) {
            // tell the view we have data
            if (count($keywords) > 0) {
                // convert our array into a D3 format
                $json_array = array(
                    'name'     => $brainstorm->keyword,
                    'children' => $suggestions->toD3JSON($keywords),
                );

                // let's save these into the database with some safe serialization
                $brainstorm->data = base64_encode(serialize($json_array));
                // how many groups did we have?
                // how many keywords did we have?

                $brainstorm->save();
            }
            // else we have some error, mostlikely it is the proxies
        }

        // we are done here
        return;
    }
}