<?php namespace App\Repositories;

use App\AccessProxy;
use App\AccessProxyLog;
use App\RegistrationDisable;
use App\RegistrationManagement;
use App\SProxy;
use Carbon\Carbon;

/**
 * Class AdminRepository
 * @package App\Repositories
 */
class AdminRepository
{

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function proxies($moduleId)
    {
        $proxies = AccessProxy::where('module', $moduleId)->get();
        return $proxies;
    }
    /**
     * @param $input
     *
     * @return SProxy
     */
    public function saveProxy($input)
    {
        if ($input['port'] == null) {
            unset($input['port']);
        }
        if ($input['username'] == null) {
            unset($input['username']);
        }
        if ($input['password'] == null) {
            unset($input['password']);
        }

        if (isset($input['id']) && !empty(isset($input['id']))) {

            // UPDATE
            $proxy = AccessProxy::whereId($input['id'])->first();
            $proxy->status=$input['status'];
            $proxy->save();
            return $proxy;
        } else {

            // CREATE
            $proxy = new AccessProxy($input);
            $proxy->ip=trim($input['ip']);
            $proxy->module = $input['module'];
            $proxy->status=1;
            $proxy->save();

            return $proxy;
        }
    }

    /**
     * @param $textAr
     * @param $type
     *
     * @return array
     */
    public function proxiesImport($textAr, $type, $module)
    {
        $proxyArr = [];
        foreach ($textAr as $line) {
            if ($type == 'iface') {
                $array = [$line];
            } else {
                $array = explode(':', $line);
            }

            $proxy = new AccessProxy();
            $proxy->type = $type;
            $proxy->ip = $array[0];
            $proxy->port = isset($array[1]) ? $array[1] : ' ';
            $proxy->module = $module;
            $proxy->status = 1;

            if (isset($array[2])) {
                $proxy->user = $array[2];
            }
            if (isset($array[3])) {
                $proxy->password = $array[3];
            }

            $proxy->save();
            $proxyArr[] = $proxy;
        }

        return $proxyArr;
    }

    /**
     * @param $id
     *
     * @return bool
     */
    public function proxiesDelete($id)
    {
        $proxy = AccessProxy::where('id', intval($id))->first();

        if (empty($proxy)) {
            return false;
        }
        // delete the itself
        return $proxy->delete();
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function findProxy($id)
    {
       $proxy = AccessProxy::where('id', intval($id))->first();
        return $proxy;
    }
    /*
    End Proxy settings
     */


    /*
     * Registration Management
     */

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getRegistrationManagement()
    {
        return RegistrationManagement::all();
    }

    /**
     * @param $input
     *
     * @return RegistrationManagement
     */
    public function saveRegistrationManagement($input)
    {
        if (isset($input['id']) && !empty(isset($input['id']))) {

            // UPDATE
            $record = RegistrationManagement::where('id', intval($input['id']))->first();
            $record->setRawAttributes($input);
            $record->save();

            return $record;
        } else {

            // CREATE
            $record = new RegistrationManagement($input);
            $record->save();

            return $record;
        }
    }

    /**
     * @param $id
     *
     * @return bool
     */
    public function deleteRegistrationManagement($id)
    {
        $record = RegistrationManagement::where('id', intval($id));

        if (empty($record)) {
            return false;
        }

        // delete the RegistrationManagement
        return $record->delete();
    }

    /**
     * @return mixed
     */
    public function findRegistrationManagementToggle()
    {
        return RegistrationDisable::whereNotNull('id')->first();
    }

    /**
     * @param array $input
     *
     * @return mixed
     */
    public function findOrCreateRegistrationManagementToggle($input)
    {
        $registration = RegistrationDisable::find('1');

        if ($registration) {
            $registration->disabled = $input['disabled'];
            $registration->save();
        } else {
            RegistrationDisable::create([
                'disabled' => true
            ]);
        }

        return $registration;
    }

}
