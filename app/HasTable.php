<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HasTable extends Model
{
    protected $table = 'has_tables';
}
