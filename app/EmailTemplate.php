<?php

namespace App;

use Html;

/**
 * App\EmailTemplate
 *
 * @property integer $id
 * @property string $name
 * @property string $subject
 * @property string $body
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\EmailTemplate whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\EmailTemplate whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\EmailTemplate whereSubject($value)
 * @method static \Illuminate\Database\Query\Builder|\App\EmailTemplate whereBody($value)
 * @method static \Illuminate\Database\Query\Builder|\App\EmailTemplate whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\EmailTemplate whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class EmailTemplate extends BaseModel
{
    /** @const WELCOME_EMAIL_TEMPLATE */
    const WELCOME_EMAIL_TEMPLATE = 'WELCOME';
    /** @const FORGET_PASSWORD_EMAIL_TEMPLATE */
    const FORGET_PASSWORD_EMAIL_TEMPLATE = 'FORGET PASSWORD';
    /** @const CHANGE_PASSWORD_EMAIL_TEMPLATE */
    const CHANGE_PASSWORD_EMAIL_TEMPLATE = 'CHANGE PASSWORD';    
    /** @const NEW_SUBSCRIBER_EMAIL_TEMPLATE */
    const NEW_SUBSCRIBER_EMAIL_TEMPLATE = 'NEW SUBSCRIBER';
    /** @const SUBSCRIPTION_CANClE_EMAIL_TEMPLATE */
    const SUBSCRIPTION_CANClE_EMAIL_TEMPLATE = 'SUBSCRIPTION CANClE';
    /** @const CONTACT_US_EMAIL_TEMPLATE */
    const CONTACT_US_EMAIL_TEMPLATE = 'CONTACT US';
    /** @const CONTACT_US_EMAIL_TEMPLATE */
    const CONFORM_EMAIL_ACTIVATE_ACCOUNT_TEMPLATE = 'CONFIRM EMAIL';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'email_templates';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'subject', 'body'];
    /**
     * id used for primary key
     * @var integer
     */
    public $primaryKey = 'id';

    /**
     * @param $value
     */
    public function setBodyAttribute($value)
    {
        $this->attributes['body'] =$value;// HTML::entities($value);
    }
}