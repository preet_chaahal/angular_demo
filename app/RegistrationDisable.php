<?php

namespace App;

/**
 * App\RegistrationDisable
 *
 * @property integer $id
 * @property string $disabled
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\RegistrationDisable whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\RegistrationDisable whereDisabled($value)
 * @method static \Illuminate\Database\Query\Builder|\App\RegistrationDisable whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\RegistrationDisable whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class RegistrationDisable extends BaseModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'registration_disable';

    /**
     * the dates on the table
     */
    protected $dates = [];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['disabled'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
}
