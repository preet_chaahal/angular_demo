<?php

namespace App\Libraries\Logger;

use Monolog\Handler\RotatingFileHandler;
use Monolog\Logger;

class StripeLogger
{
    public $monoLog;

    function __construct()
    {
        $this->monoLog = new Logger('log');
        $logFile = storage_path('logs/stripe.log');
        $this->monoLog->pushHandler(new RotatingFileHandler($logFile));
    }

    public static function info($message, array $context = array())
    {
        $logger = new StripeLogger();
        $logger->monoLog->info($message, $context);
    }

    public static function error($message, array $context = array())
    {
        $logger = new StripeLogger();
        $logger->monoLog->error($message, $context);
    }

    public static function debug($message, array $context = array())
    {
        $logger = new StripeLogger();
        $logger->monoLog->debug($message, $context);
    }

    public static function notice($message, array $context = array())
    {
        $logger = new StripeLogger();
        $logger->monoLog->notice($message, $context);
    }

    public static function warning($message, array $context = array())
    {
        $logger = new StripeLogger();
        $logger->monoLog->warning($message, $context);
    }

    public static function critical($message, array $context = array())
    {
        $logger = new StripeLogger();
        $logger->monoLog->critical($message, $context);
    }

    public static function alert($message, array $context = array())
    {
        $logger = new StripeLogger();
        $logger->monoLog->alert($message, $context);
    }
}