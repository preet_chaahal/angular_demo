<?php

namespace App;

/**
 * App\SGroup
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|SKeywords[] $keywords
 * @property-read STarget $target
 * @property-read \Illuminate\Database\Eloquent\Collection|SCheck[] $lastRan
 * @property integer $idGroup
 * @property string $name
 * @property string $module
 * @property string $options
 * @property integer $position
 * @method static \Illuminate\Database\Query\Builder|\App\SGroup whereIdGroup($value)
 * @method static \Illuminate\Database\Query\Builder|\App\SGroup whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\SGroup whereModule($value)
 * @method static \Illuminate\Database\Query\Builder|\App\SGroup whereOptions($value)
 * @method static \Illuminate\Database\Query\Builder|\App\SGroup wherePosition($value)
 * @mixin \Eloquent
 */
class SGroup extends BaseModel
{

    public $timestamps = false;
    protected $primaryKey = 'idGroup';

    protected $connection = 'mysql_serposcope';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'group';

    /**
     * the dates on the table
     */
    protected $dates = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'options' => 'array',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'module', 'options', 'position','user_id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];


    public function keywords()
    {
        return $this->hasMany(SKeywords::class, 'idGroup');
    }

    public function target()
    {
        return $this->hasOne(STarget::class, 'idGroup');
    }

    public function lastRan()
    {
        return $this->hasMany(SCheck::class, 'idGroup');
    }
}
