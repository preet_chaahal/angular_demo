<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Session;
use Redirect;
use Auth;

class Authenticate
{
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($this->auth->guest()) {
            if ($request->ajax()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->guest('auth/login')->withErrors(['Unauthorized Access, please login']);
            }
        }

        //Deny login attempt if IP is in the banned list
        $ip = \App\RegistrationManagement::where('ip', '=', $_SERVER['REMOTE_ADDR'])->first();

        if ($ip != null)
        {
            //Logout user
            Auth::logout();
            // flash user
            Session::flash('nosuccess', ['Your account has been disabled. Please contact support.']);

            return redirect()->guest('auth/login');
        }

        //Deny login attempt if user account is disabled
        if (Auth::user()->disabled == 1)
        {
            //Logout user
            Auth::logout();
            // flash user
            Session::flash('nosuccess', ['Your account has been disabled. Please contact support.']);

            return redirect()->guest('auth/login');

        }

        return $next($request);
    }
}
