<?php

namespace App\Http\Middleware;

use Closure;
use Session;
use App\Account;

class AvailablePlan
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      if (!Account::where('name', $request->get('plan'))->where('disabled',0)->first()) {
        Session::flash('nosuccess', ['Invalid Plan, Please try again!']);
        return redirect()->back();
      }
        return $next($request);
    }
}
