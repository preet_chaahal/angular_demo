<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Auth;
use Session;

class RedirectIfAuthenticated
{
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //Deny login attempt if IP is in the banned list
        $ip = \App\RegistrationManagement::where('ip', '=', $_SERVER['REMOTE_ADDR'])->first();

        if ($ip != null)
        {
            //Logout user
            Auth::logout();
            // flash user
            Session::flash('nosuccess', ['Your account has been disabled. Please contact support.']);

            /*return redirect()->guest('auth/login');*/
        }

        if ($this->auth->check()) {
            // @TODO change /home to a dashboard
            //return redirect('/home');
            return redirect('/tools/keywordresearch');
        }

        return $next($request);
    }
}
