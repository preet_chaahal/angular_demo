<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Redirect;
use Session;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user()->is_admin)
        {
            return $next($request);
        }

        // redirect them
        return Redirect::route('admin.users');
        
    }
}
