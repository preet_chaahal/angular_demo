<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Redirect;

class Subscribed
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
//        if(Auth::check() && Auth::user()->subscribed())
//        {
//            return redirect('/auth/subscription')->with('notice', 'You need to be subscribed to do that.');
//        }

        return $next($request);
    }
}
