<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class VerifiedEmail
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();
        if ($user && $user->campaign && !$user->is_registered) {
            return redirect()->route('front.pages.landing.verify');
        }
        return $next($request);
    }
}
