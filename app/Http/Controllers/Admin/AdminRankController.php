<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\RankCheck;
use App\Repositories\AdminRankRepository;
use App\Repositories\UserRepository;
use App\SCheck;
use App\SEvent;
use App\SGroup;
use App\SKeywords;
use App\SLogs;
use App\SOptions;
use App\SRank;
use App\STarget;
use App\User;
use App\Utilities\CasperCPC;
use App\Utilities\ExportCsv;
use App\Utilities\Utility;
use Auth;
use Config;
use Illuminate\Http\Request;
use Input;
use Log;
use Redirect;
use Session;

class AdminRankController extends Controller
{
    private $adminRankRepository;
    private $userRepo;

    function __construct(AdminRankRepository $adminRankRepository, UserRepository $userRepo)
    {
        $this->adminRankRepository = $adminRankRepository;
        $this->userRepo            = $userRepo;
    }

    public function index()
    {
        $domains = $this->adminRankRepository->all();

        Session::forget('rt_date_start');
        Session::forget('rt_date_stop');

        return View('admin.tools.ranktracker.index', ['domains' => $domains]);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function view($id)
    {
        // First lets see if we have any dates
        $userDates = [
            'start' => Session::get('rt_date_start', date('Y-m-') . '01'),
            'stop' => Session::get('rt_date_stop', date('Y-m-t')),
        ];

        $rankChecker = RankCheck::where('group_id', intval($id))
            ->first();

        if (empty($rankChecker)) {
            return Redirect::route('backend.tools.ranktracker');
        }

        $group = SGroup::where('idGroup', intval($id))->with('target')->first();

        if (empty($group)) {
            return Redirect::route('backend.tools.ranktracker');
        }

        $keywords = array();
        $rankTrackerData = array();
        $chartJs = array(
            'legend' => array(),
        ); // will be converted into JSON


        // fill up our keywords to store the information
        /** @var  SGroup $keyword */
        foreach ($group->keywords as $keyword) {
            $keywords[$keyword->idKeyword] = [
                'id' => $keyword->idKeyword,
                'keyword' => $keyword->name,
                'position' => null,
            ];
        }

        // fill up our array with the dates we need to check for
        $numberOfDays = abs(strtotime($userDates['start']) - strtotime($userDates['stop']));
        $numberOfDays = floor($numberOfDays / (60 * 60 * 24));

        for ($day = 0; $day <= $numberOfDays; $day++) {
            $currentDate = date('Ymd', strtotime('+' . $day . ' days', strtotime($userDates['start'])));

            $rankTrackerData[$currentDate] = array(
                'hasData' => false,
                'keywords' => $keywords,
                'events' => array(),
            );
        }

        // now going through each date
        foreach ($rankTrackerData as $date => $data) {
            $dateFormatted = date('Y-m-d', strtotime($date));

            // do we have any events on this day
            if ($group->target->events()->where('date', $dateFormatted)->count() > 0) {
                $rankTrackerData[$date]['hasData'] = true;

                foreach ($group->target->events()->where('date', $dateFormatted)->get() as $event) {
                    $rankTrackerData[$date]['events'][uniqid()] = [
                        'position' => -2,
                        'label' => 'Event: ' . $event->event
                    ];
                }
            }

            // do we have any keywords on this day
            $keywordScan = $group->lastRan()->whereBetween('date',
                [$dateFormatted . ' 00:00:00', $dateFormatted . ' 23:59:59']);
            if ($keywordScan->count() > 0) {
                $rankTrackerData[$date]['hasData'] = true;

                // our scan has idCheck
                // we can use this to find the position (SRANK)
                foreach ($keywordScan->get() as $scan) {
                    foreach ($keywords as $keyword) {
                        // check for rank
                        $rank = SRank::where('idKeyword', $keyword['id'])
                            ->where('idCheck', $scan->idCheck)
                            ->first();

                        // did this keyword have a rank?
                        if ($rank) {
                            $rankTrackerData[$date]['keywords'][$keyword['id']]['position'] = $rank->position;
                        }
                    }
                }
            }
        }

        // now we have to make it into something useable by ChartJS
        $dataSetIndex = 0;
        foreach ($rankTrackerData as $date => $data) {
            $chartJs['labels'][] = $dateFormatted = date('M d, Y', strtotime($date));

            // keywords
            foreach ($data['keywords'] as $keyword) {
                // check to see if we already created this dataset
                if (!isset($chartJs['datasets'][$keyword['id']])) {
                    // random dataset colours
                    $r = rand(0, 255);
                    $g = rand(0, 255);
                    $b = rand(0, 255);

                    // setup the keyword dataset
                    $chartJs['datasets'][$keyword['id']] = [
                        'label' => $keyword['keyword'],
                        'fillColor' => "rgba(" . $r . "," . $g . "," . $b . ",0.2)",
                        'strokeColor' => "rgba(" . $r . "," . $g . "," . $b . ",1)",
                        'pointColor' => "rgba(" . $r . "," . $g . "," . $b . ",1)",
                        'pointStrokeColor' => '#fff',
                        'pointHighlightFill' => '#fff',
                        'pointHighlightStroke' => "rgba(" . $r . "," . $g . "," . $b . ",1)",
                        'data' => array(),
                    ];

                    // add label
                    $chartJs['legend'][] = [
                        'label' => $keyword['keyword'],
                        'fillColor' => "rgba(" . $r . "," . $g . "," . $b . ",1)",
                        'strokeColor' => "rgba(" . $r . "," . $g . "," . $b . ",1)",
                    ];
                }

                // assign the specific data for this date and keyword
                $chartJs['datasets'][$keyword['id']]['data'][] = $keyword['position'];
            }

            // events
            foreach ($data['events'] as $event_id => $event) {
                // check to see if we already created the event dataset
                if (!isset($chartJs['datasets'][$event_id])) {
                    $eventDates = array_keys($rankTrackerData);

                    $chartJs['datasets'][$event_id] = [
                        'label' => $event['label'],
                        'fillColor' => "rgba(0, 0, 0, 0.2)",
                        'strokeColor' => "rgba(0, 0, 0, 1)",
                        'pointColor' => "rgba(0, 0, 0, 1)",
                        'pointStrokeColor' => '#fff',
                        'pointHighlightFill' => '#fff',
                        'pointHighlightStroke' => "rgba(0, 0, 0, 1)",
                        'data' => array(),
                    ];

                    // add label
                    $chartJs['legend']['event'] = [
                        'label' => 'Event',
                        'fillColor' => "rgba(0, 0, 0, 1)",
                        'strokeColor' => "rgba(0, 0, 0, 1)",
                    ];

                    foreach ($eventDates as $eventDate) {
                        $chartJs['datasets'][$event_id]['data'][$eventDate] = null;
                    }
                }

                // assign the specific data for this date and event
                $chartJs['datasets'][$event_id]['data'][$date] = -2;
            }
        }

        // turn everything into JSON
        if (!empty($chartJs['datasets'])) {
            $chartJs['labels'] = json_encode($chartJs['labels']);
            $chartJs['datasets'] = json_encode($chartJs['datasets']);
        }

//        $id = 36;
        $top3 = $top10 = $top20 = $top30 = $top100 = 0;
        $keywordPositionChartTableData = [3 => $top3, 10 => $top10, 20 => $top20, 30 => $top30, 100 => $top100];
        $keywordRank = [];
        $check = SCheck::where('idGroup', $group->idGroup)->orderBy('idCheck')->take(2)->skip(0)->get();

        $utility = new Utility();
        $visibilityScore = 0;
        $keywordsCount = 0;
        $groupKeywords = SKeywords::where('idGroup', $group->idGroup)->get();
        foreach ($check as $idcheck) {
            foreach ($groupKeywords as $groupKeyword) {
                // check for rank
                if (!isset($keywordRank[$groupKeyword->name])) {
                    $keywordsCount++;
                    $keywordRank[$groupKeyword->name] = ['change' => 0, 'newRank' => 'N/A', 'oldRank' => 0, 'bestRank' => 'N/A'];
                    $rank = SRank::where('idKeyword', $groupKeyword->idKeyword)
                        ->where('idCheck', $idcheck->idCheck)
                        ->first();
                    if (!empty($rank)) {
                        $keywordRank[$groupKeyword->name]['oldRank'] = $rank->position;
                        $keywordRank[$groupKeyword->name]['newRank'] = $rank->position;
                        $keywordRank[$groupKeyword->name]['bestRank'] = $rank->position;
                        $visibilityScore += $utility->getKeywordVisibilityPointByPosition($rank->position);
                    } else {
                        $keywordRank[$groupKeyword->name]['oldRank'] = 'N/A';
                    }

                } else {
                    $rank = SRank::where('idKeyword', $groupKeyword->idKeyword)
                        ->where('idCheck', $idcheck->idCheck)
                        ->first();
                    if (!empty($rank)) {
                        Log::info($groupKeyword->name);
                        Log::info($keywordRank[$groupKeyword->name]);
                        Log::info($rank);
                        $keywordRank[$groupKeyword->name]['newRank'] = $rank->position;
                        $keywordRank[$groupKeyword->name]['change'] = $keywordRank[$groupKeyword->name]['oldRank'] - $rank->position;
                    }

                    $bestRank = SRank::where('idKeyword', $groupKeyword->idKeyword)->min('position');
                    if (!empty($bestRank)) {
                        $keywordRank[$groupKeyword->name]['bestRank'] = $bestRank;
                    }
                }
            }
        }
        // check to see if we have any casper items
        $casperResults = array();
        if ($rankChecker->casper()->count() > 0) {
            $casper = $rankChecker->casper->where('group_id', $rankChecker->group_id)->where('is_downloaded',
                true)->latest()->first();
            if ($casper) {
                $casperResults = $casper->results;
            }
        }

        if (!empty($keywordRank)) {
            foreach ($keywordRank as $name => $position) {
                foreach ($casperResults as $casperResult) {
                    if ($casperResult->keyword == $name) {
                        $casperResult->change = $position['change'];
                        $casperResult->newRank = $position['newRank'];
                        $casperResult->oldRank = $position['oldRank'];
                        $casperResult->bestRank = $position['bestRank'];
                        $keywordPositionChartTableData = $utility->setKeywordPosition($keywordPositionChartTableData, $position['newRank']);
                    }
                }
            }
        }

        $keywordPositionChartData = "['Top 3', " . $keywordPositionChartTableData[3] . "],
                        ['Top 10', " . $keywordPositionChartTableData[10] . "],
                        ['Top 20', " . $keywordPositionChartTableData[20] . "],
                        ['Top 30', " . $keywordPositionChartTableData[30] . "],
                        ['Top 100'," . $keywordPositionChartTableData[100] . "]";


        if ($visibilityScore != 0) {
            $visibilityPercentage = ($visibilityScore / ($keywordsCount * 30)) * 100;
        } else {
            $visibilityPercentage = 0;
        }
        return view('backend.tools.ranktracker.view', [
            'chartJs' => $chartJs,
            'group' => $group,
            'dates' => $userDates,
            'casperResults' => $casperResults,
            'keywordPositionChartData' => $keywordPositionChartData,
            'keywordPositionChartTableData' => $keywordPositionChartTableData,
            'visibilityScore' => $visibilityScore,
            'visibilityPercentage' => round($visibilityPercentage, 0)
        ]);
    }

    public function delete($id)
    {
        $rankChecker = RankCheck::where('group_id', intval($id));

        if (empty($rankChecker)) {
            return Redirect::route('admin.tools.ranktracker.index');
        }

        // delete the rankcheck itself
        $rankChecker->delete();

        // look up the group
        $group = SGroup::where('idGroup', intval($id))->first();
        if (empty($group)) {
            return Redirect::route('admin.tools.ranktracker.index');
        }

        $group->delete();

        // return
        return Redirect::route('admin.tools.ranktracker.index');
    }

    public function edit($id)
    {
        $group = SGroup::where('idGroup', intval($id))->first();

        if (empty($group)) {
            return Redirect::route('admin.tools.ranktracker.index');
        }

        // we need to fix the keywords themselfs
        $keywordString = '';
        foreach ($group->keywords as $keyword) {
            if (strlen($keywordString) > 0) {
                $keywordString .= "\r\n";
            }
            $keywordString .= $keyword->name;
        }

        return view('admin.tools.ranktracker.edit', ['group' => $group, 'keywordString' => $keywordString]);
    }

    public function update(Request $request)
    {
        $rules = [
            'name'     => 'required',
            'keywords' => 'required',
            'domain'   => 'required',
            'module'   => 'required',
        ];

        // validate input
        $this->validate($request, $rules);

        /** @var SGroup $group */
        $group = SGroup::where('idGroup', intval($request->input('id')))->first();

        if (empty($group)) {
            return Redirect::route('admin.tools.ranktracker.index');
        }

        // save the group information
        $group->name   = $request->input('name');
        $group->module = 'Google';
        // $group->options = '{"tld":"com","datacenter":"","parameters":"","local":""}'; // for now
        $group->save();

        // Keywords
        $groupKeywords = $group->keywords->lists('name', 'idKeyword')->toArray();
        $keywordDelete = [];
        $keywordNew    = [];

        // lets see what the user typed in
        $keywords = explode("\r\n", $request->input('keywords'));
        foreach ($keywords as $word) {
            $word      = strtolower(trim($word));
            $wordFound = false;

            // for each of our original keywords, lets search
            foreach ($groupKeywords as $index => $keyword) {
                if ($word == $keyword) {
                    // so we found the keyword
                    // we do not have to tell our software we are keeping it
                    // but we do need to tell our loop
                    $wordFound = true;
                    // also remove it from our original list, we will use the list at the end to decide what to remove
                    unset($groupKeywords[$index]);
                }
            }

            // if we didn't find the word, it means that it is one that we are adding
            if ( ! $wordFound) {
                $keywordNew[] = $word;
            }
        }

        // if we have keywords to add, lets add them
        if (count($keywordNew) > 0) {
            foreach ($keywordNew as $word) {
                SKeywords::create([
                    'idGroup' => $group->idGroup,
                    'name'    => strtolower(trim($word)),
                ]);
            }
        }

        // if we have anything left in our original array, we need to remove them
        if (count($groupKeywords) > 0) {
            foreach ($groupKeywords as $id => $keyword) {
                /** @var SKeywords $sKeyWord */
                $sKeyWord = SKeywords::findOrFail($id);
                $sKeyWord->delete();
            }
        }

        $group->target->name = $request->input('domain');
        $group->target->save();

        // tell the user

        // return
        return Redirect::route('admin.tools.ranktracker.index');
    }

    public function add()
    {
        $tldOptions = Config::get('searchengines');
        $allUsers   = User::all();

        return view('admin.tools.ranktracker.add', ['tldOptions' => json_encode($tldOptions), 'allUsers' => $allUsers]);
    }

    public function insert(Request $request)
    {
        $rules = [
            'useraccount' => 'required|not_in:Select',
            'name'        => 'required',
            'keywords'    => 'required',
            'domain'      => 'required',
            'module'      => 'required',
        ];

        // validate input
        $this->validate($request, $rules);

        //Google URL
        $new_domain = $request->input('domain');
        $url        = 'http://webcache.googleusercontent.com/search?q=cache:' . $new_domain;

        $handle = curl_init($url);
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);

        /* Get the HTML or whatever is linked in $url. */
        $response = curl_exec($handle);

        /* Check for 404 (file not found). */
        $httpCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);

        if ($httpCode == 404) {
            // tell user
            Session::flash('success', array('This domain is not yet listed in Google SERP'));

            // redirect them
            return Redirect::route('admin.tools.ranktracker.index');
        }

        // create the Group
        $serpo          = new SGroup;
        $serpo->name    = $request->input('name');
        $serpo->module  = 'Google';
        $serpo->options = '{"tld":"com","datacenter":"","parameters":"","local":""}'; // for now
        $serpo->save();

        // Grab user id from input useraccount
        $userAccount   = $request->input('useraccount');
        $userLookup    = User::where('username', $userAccount)->first();
        $userAccountID = $userLookup->id;

        // store the group into the laravel application
        $rankCheck = RankCheck::create([
            'user_id'  => $userAccountID,
            'group_id' => $serpo->idGroup
        ]);

        // Store the keywords for the group
        $keywords = explode("\r\n", $request->input('keywords'));
        foreach ($keywords as $word) {
            if (strlen(trim($word)) > 0) {
                SKeywords::create([
                    'idGroup' => $serpo->idGroup,
                    'name'    => strtolower(trim($word)),
                ]);
            }
        }

        // Store the target domain for that group
        STarget::create([
            'idGroup' => $serpo->idGroup,
            'name'    => trim($request->input('domain'))
        ]);

        // tell user
        Session::flash('success', array('Great News! Your new group has been added.'));

        // redirect them
        return Redirect::route('admin.tools.ranktracker.index', $serpo->idGroup);
    }

    public function download($id)
    {
        // First lets see if we have any dates
        $userDates = [
            'start' => Session::get('rt_date_start', date('Y-m-') . '01'),
            'stop'  => Session::get('rt_date_stop', date('Y-m-t')),
        ];

        /*$rankChecker = RankCheck::where('group_id', intval($id))
            ->where('user_id', Auth::id())
            ->first();

        if(empty($rankChecker))
        {
            return Redirect::route('backend.tools.ranktracker');
        }*/
        /** @var SGroup $group */
        $group = SGroup::where('idGroup', intval($id))->first();

        if (empty($group)) {
            return Redirect::route('admin.tools.ranktracker.index');
        }

        $keywords        = array();
        $rankTrackerData = array();
        $chartJs         = array(
            'legend' => array(),
        ); // will be converted into JSON

        // fill up our keywords to store the information
        foreach ($group->keywords->lists('name', 'idKeyword')->toArray() as $id => $keyword) {
            $keywords[$id] = [
                'id'       => $id,
                'keyword'  => $keyword,
                'position' => null,
                'url'      => null,
            ];
        }

        // fill up our array with the dates we need to check for
        $numberOfDays = abs(strtotime($userDates['start']) - strtotime($userDates['stop']));
        $numberOfDays = floor($numberOfDays / (60 * 60 * 24));

        for ($day = 0; $day <= $numberOfDays; $day++) {
            $currentDate = date('Ymd', strtotime('+' . $day . ' days', strtotime($userDates['start'])));

            $rankTrackerData[$currentDate] = array(
                'hasData'  => false,
                'keywords' => $keywords,
                'events'   => array(),
            );
        }

        // now going through each date
        foreach ($rankTrackerData as $date => $data) {
            $dateFormatted = date('Y-m-d', strtotime($date));

            // do we have any events on this day
            if ($group->target->events()->where('date', $dateFormatted)->count() > 0) {
                $rankTrackerData[$date]['hasData'] = true;

                foreach ($group->target->events()->where('date', $dateFormatted)->get() as $event) {
                    $rankTrackerData[$date]['events'][uniqid()] = [
                        'position' => -2,
                        'label'    => 'Event: ' . $event->event
                    ];
                }
            }

            // do we have any keywords on this day
            $keywordScan = $group->lastRan()->whereBetween('date',
                [$dateFormatted . ' 00:00:00', $dateFormatted . ' 23:59:59']);
            if ($keywordScan->count() > 0) {
                $rankTrackerData[$date]['hasData'] = true;

                // our scan has idCheck
                // we can use this to find the position (SRANK)
                foreach ($keywordScan->get() as $scan) {
                    foreach ($keywords as $keyword) {
                        // check for rank
                        $rank = SRank::where('idKeyword', $keyword['id'])
                                     ->where('idCheck', $scan->idCheck)
                                     ->first();

                        // did this keyword have a rank?
                        if ($rank) {
                            $rankTrackerData[$date]['keywords'][$keyword['id']]['position'] = $rank->position;
                            $rankTrackerData[$date]['keywords'][$keyword['id']]['url']      = $rank->url;
                        }
                    }
                }
            }
        }

        $csvData = array();

        foreach ($rankTrackerData as $date => $data) {
            foreach ($data['keywords'] as $keyword) {
                $csvData[] = [
                    'date'     => $date,
                    'keyword'  => $keyword['keyword'],
                    'position' => $keyword['position'],
                    'url'      => $keyword['url'],
                ];
            }

            foreach ($data['events'] as $event) {
                $csvData[] = [
                    'date'     => $date,
                    'keyword'  => $event['label'],
                    'position' => null,
                    'url'      => null,
                ];
            }
        }

        $filename = 'export_KeywordRevealer_' . date('Y-m-d', strtotime($userDates['start'])) . '_' . date('Y-m-d',
                strtotime($userDates['stop'])) . '.csv';
        $csv      = new ExportCsv($csvData, $filename);
        $csv->send();
    }

    /**
     * used to set the session date
     *
     * @param string $dateStart start date
     * @param string $dateStop stop date
     */
    public function setDate(Request $request)
    {
        if ( ! $request->has('id')) {
            return Redirect::route('admin.tools.rankchecker.index');
        }

        Session::put('rt_date_start', $request->input('start_date'));
        Session::put('rt_date_stop', $request->input('stop_date'));

        return Redirect::route('admin.tools.ranktracker.view', $request->input('id'));
    }

    public function eventInsert(Request $request)
    {
        $rules = [
            'date'        => 'required|date',
            'description' => 'required'
        ];

        $this->validate($request, $rules);

        $target = STarget::where('idTarget', $request->input('target_id'))->first();

        if ( ! $target) {
            return Redirect::route('admin.tools.ranktracker.index');
        }

        // ensure we own this target
        $group = $target->group;
        /*$rankTracker = RankCheck::where('group_id', $group->idGroup)->where('user_id', Auth::id())->first();
        if(! $rankTracker)
        {
            return Redirect::route('admin.tools.ranktracker.index');
        }*/

        // if we are here we own everything above, so insert
        SEvent::create([
            'idTarget' => $target->idTarget,
            'date'     => date('Y-m-d', strtotime($request->input('date'))),
            'event'    => $request->input('description')
        ]);

        // tell user
        Session::flash('success', array('The event has been added'));

        return Redirect::route('admin.tools.ranktracker.view', $group->idGroup);
    }

    public function settings()
    {
        return Redirect::route('admin.tools.ranktracker.options');
    }

    /**
     * Options
     */
    public function options()
    {
        //Checks to see if the timeout value is available in the database
        if (SOptions::where('name', "general_timeout")->first() != null) {
            $timeout = SOptions::where('name', "general_timeout")->first();
        } else {
            $timeout = "";
        }
        //checks to see if the dbc user value is in the database
        if (SOptions::where('name', "general_dbc_user")->first() != null) {
            $dbc_user = SOptions::where('name', "general_dbc_user")->first();
        } else {
            $dbc_user = "";
        }
        //checks to see if the dbc pass is available in the database
        if (SOptions::where('name', "general_dbc_pass")->first() != null) {
            $dbc_pass = SOptions::where('name', "general_dbc_pass")->first();
        } else {
            $dbc_pass = "";
        }
        //checks to see if the proxy auto rotate is available in the database
        if (SOptions::where('name', "general_proxy_auto_rotate")->first() != null) {
            $proxy_auto = SOptions::where('name', "general_proxy_auto_rotate")->first();
        } else {
            $proxy_auto = "";
        }
        //checks to see if the max retry is in the database
        if (SOptions::where('name', "general_fetch_retry")->first() != null) {
            $retry = SOptions::where('name', "general_fetch_retry")->first();
        } else {
            $retry = "";
        }
        //checks to see if the general rendering is in the database
        if (SOptions::where('name', "general_rendering")->first() != null) {
            $gen_rend = SOptions::where('name', "general_rendering")->first();
        } else {
            $gen_rend = "";
        }
        //checks to see if the bad proxies is in the database
        if (SOptions::where('name', "general_rm_bad_proxies")->first() != null) {
            $badproxies = SOptions::where('name', "general_rm_bad_proxies")->first();
        } else {
            $badproxies = "";
        }
        //checks to see if the captcha is in the database
        if (SOptions::where('name', "Google_captcha_basesleep")->first() != null) {
            $captcha = SOptions::where('name', "Google_captcha_basesleep")->first();
        } else {
            $captcha = "";
        }
        //checks to see if the page sleep is in the database
        if (SOptions::where('name', "Google_page_sleep")->first() != null) {
            $sleep = SOptions::where('name', "Google_page_sleep")->first();
        } else {
            $sleep = "";
        }
        //checks to see if the tld is in the database
        if (SOptions::where('name', "Google_tld")->first() != null) {
            $tld = SOptions::where('name', "Google_tld")->first();
        } else {
            $tld = "";
        }

        return View('admin.tools.ranktracker.options.index', [
            'timeout'    => $timeout,
            'retry'      => $retry,
            'dbc_pass'   => $dbc_pass,
            'dbc_user'   => $dbc_user,
            'proxy_auto' => $proxy_auto,
            'gen_rend'   => $gen_rend,
            'badproxies' => $badproxies,
            'captcha'    => $captcha,
            'sleep'      => $sleep,
            'tld'        => $tld,
        ]);
    }
    //Updates options in Database
    /**
     * Anytime a value is set to null by the user it deletes the row from the database to use a default
     */
    public function optionsUpdate()
    {
        //Timeout
        if (Input::get('general_timeout') == "") {
            //Deletes the name and value from the serposcope option table
            if (SOptions::where('name', 'general_timeout')->first() != null) {
                $timeout = SOptions::where('name', 'general_timeout');
                $timeout->delete();
            }
        } else {
            if (SOptions::where('name', 'general_timeout')->first() != null) {
                //If the row exist this will update the value based on user input
                SOptions::where('name', 'general_timeout')->update(['value' => Input::get('general_timeout')]);
            } else {
                //If the row doesn't exist this will create a new one
                $timeout = new SOptions;

                $timeout->name  = "general_timeout";
                $timeout->value = Input::get('general_timeout');

                $timeout->save();
            }
        }
        //Fetch
        if (Input::get('general_fetch_retry') == "") {
            //Deletes the name and value from the serposcope option table
            if (SOptions::where('name', 'general_fetch_retry')->first() != null) {
                $timeout = SOptions::where('name', 'general_fetch_retry');
                $timeout->delete();
            }
        } else {
            if (SOptions::where('name', 'general_fetch_retry')->first() != null) {
                //If the row exist this will update the value based on user input
                SOptions::where('name',
                    'general_fetch_retry')->update(['value' => Input::get('general_fetch_retry')]);
            } else {
                //If the row doesn't exist this will create a new one
                $timeout = new SOptions;

                $timeout->name  = "general_fetch_retry";
                $timeout->value = Input::get('general_fetch_retry');

                $timeout->save();
            }
        }
        //Bad Proxies
        if (Input::get('general_rm_bad_proxies') == "") {
            //Deletes the name and value from the serposcope option table
            if (SOptions::where('name', 'general_rm_bad_proxies')->first() != null) {
                $timeout = SOptions::where('name', 'general_rm_bad_proxies');
                $timeout->delete();
            }
        } else {
            if (SOptions::where('name', 'general_rm_bad_proxies')->first() != null) {
                //If the row exist this will update the value based on user input
                SOptions::where('name',
                    'general_rm_bad_proxies')->update(['value' => Input::get('general_rm_bad_proxies')]);
            } else {
                //If the row doesn't exist this will create a new one
                $timeout = new SOptions;

                $timeout->name  = "general_rm_bad_proxies";
                $timeout->value = Input::get('general_rm_bad_proxies');

                $timeout->save();
            }
        }
        //Proxy Auto Rotate
        if (Input::get('general_proxy_auto_rotate') == "yes") {
            //Deletes the name and value from the serposcope option table
            if (SOptions::where('name', 'general_proxy_auto_rotate')->first() != null) {
                $timeout = SOptions::where('name', 'general_proxy_auto_rotate');
                $timeout->delete();
            }
        } else {
            if (SOptions::where('name', 'general_proxy_auto_rotate')->first() != null) {
                //If the row exist this will update the value based on user input
                SOptions::where('name',
                    'general_proxy_auto_rotate')->update(['value' => Input::get('general_proxy_auto_rotate')]);
            } else {
                //If the row doesn't exist this will create a new one
                $timeout = new SOptions;

                $timeout->name  = "general_proxy_auto_rotate";
                $timeout->value = Input::get('general_proxy_auto_rotate');

                $timeout->save();
            }
        }
        //Rendering
        if (Input::get('general_rendering') == "") {
            //Deletes the name and value from the serposcope option table
            if (SOptions::where('name', 'general_rendering')->first() != null) {
                $timeout = SOptions::where('name', 'general_rendering');
                $timeout->delete();
            }
        } else {
            if (SOptions::where('name', 'general_rendering')->first() != null) {
                //If the row exist this will update the value based on user input
                SOptions::where('name', 'general_rendering')->update(['value' => Input::get('general_rendering')]);
            } else {
                //If the row doesn't exist this will create a new one
                $timeout = new SOptions;

                $timeout->name  = "general_rendering";
                $timeout->value = Input::get('general_rendering');

                $timeout->save();
            }
        }
        //DBC User
        if (Input::get('general_dbc_user') == "") {
            //Deletes the name and value from the serposcope option table
            if (SOptions::where('name', 'general_dbc_user')->first() != null) {
                $timeout = SOptions::where('name', 'general_dbc_user');
                $timeout->delete();
            }
        } else {
            if (SOptions::where('name', 'general_dbc_user')->first() != null) {
                //If the row exist this will update the value based on user input
                SOptions::where('name', 'general_dbc_user')->update(['value' => Input::get('general_dbc_user')]);
            } else {
                //If the row doesn't exist this will create a new one
                $timeout = new SOptions;

                $timeout->name  = "general_dbc_user";
                $timeout->value = Input::get('general_dbc_user');

                $timeout->save();
            }
        }
        //DBC Pass
        if (Input::get('general_dbc_pass') == "") {
            //Deletes the name and value from the serposcope option table
            if (SOptions::where('name', 'general_dbc_pass')->first() != null) {
                $timeout = SOptions::where('name', 'general_dbc_pass');
                $timeout->delete();
            }
        } else {
            if (SOptions::where('name', 'general_dbc_pass')->first() != null) {
                //If the row exist this will update the value based on user input
                SOptions::where('name', 'general_dbc_pass')->update(['value' => Input::get('general_dbc_pass')]);
            } else {
                //If the row doesn't exist this will create a new one
                $timeout = new SOptions;

                $timeout->name  = "general_dbc_pass";
                $timeout->value = Input::get('general_dbc_pass');

                $timeout->save();
            }
        }
        //Page Sleep
        if (Input::get('Google_page_sleep') == "") {
            //Deletes the name and value from the serposcope option table
            if (SOptions::where('name', 'Google_page_sleep')->first() != null) {
                $timeout = SOptions::where('name', 'Google_page_sleep');
                $timeout->delete();
            }
        } else {
            if (SOptions::where('name', 'Google_page_sleep')->first() != null) {
                //If the row exist this will update the value based on user input
                SOptions::where('name', 'Google_page_sleep')->update(['value' => Input::get('Google_page_sleep')]);
            } else {
                //If the row doesn't exist this will create a new one
                $timeout = new SOptions;

                $timeout->name  = "Google_page_sleep";
                $timeout->value = Input::get('Google_page_sleep');

                $timeout->save();
            }
        }
        //Captcha Base Sleep
        if (Input::get('Google_captcha_basesleep') == "") {
            //Deletes the name and value from the serposcope option table
            if (SOptions::where('name', 'Google_captcha_basesleep')->first() != null) {
                $timeout = SOptions::where('name', 'Google_captcha_basesleep');
                $timeout->delete();
            }
        } else {
            if (SOptions::where('name', 'Google_captcha_basesleep')->first() != null) {
                //If the row exist this will update the value based on user input
                SOptions::where('name',
                    'Google_captcha_basesleep')->update(['value' => Input::get('Google_captcha_basesleep')]);
            } else {
                //If the row doesn't exist this will create a new one
                $timeout = new SOptions;

                $timeout->name  = "Google_captcha_basesleep";
                $timeout->value = Input::get('Google_captcha_basesleep');

                $timeout->save();
            }
        }
        //TLD
        if (Input::get('Google_tld') == "") {
            //Deletes the name and value from the serposcope option table
            if (SOptions::where('name', 'Google_tld')->first() != null) {
                $timeout = SOptions::where('name', 'Google_tld');
                $timeout->delete();
            }
        } else {
            if (SOptions::where('name', 'Google_tld')->first() != null) {
                //If the row exist this will update the value based on user input
                SOptions::where('name', 'Google_tld')->update(['value' => Input::get('Google_tld')]);
            } else {
                //If the row doesn't exist this will create a new one
                $timeout = new SOptions;

                $timeout->name  = "Google_tld";
                $timeout->value = Input::get('Google_tld');

                $timeout->save();
            }
        }

        // tell user
        Session::flash('success', array('The options have been updated'));

        // redirect them
        return Redirect::route('admin.tools.ranktracker.options');
    }

    //Serposcope logs get
    public function logs()
    {
        $logitems = SLogs::orderBy('idRun','DSC')->paginate(100);

        return View('admin.tools.ranktracker.logs.index', ['logitems' => $logitems]);
    }

    public function viewLogs($id)
    {
        $logview = SLogs::where('idRun', intval($id))->first();

        return View('admin.tools.ranktracker.logs.view', ['logview' => $logview]);
    }

    public function deleteLog($id)
    {
        $log = SLogs::where('idRun', intval($id))->first();

        if (empty($log)) {
            return Redirect::route('admin.tools.ranktracker.logs');
        }

        // because there is no unique id, delete with a where statement instead
        SLogs::where('idRun', intval($id))->delete();

        // tell user
        Session::flash('success', array('The log has been deleted'));

        return Redirect::route('admin.tools.ranktracker.logs');
    }

    public function casperGenerator($groupId)
    {
        // Casper CPC
        $Casper = new CasperCPC(Auth::id());
        // Rank Tracker
        $rankChecker = RankCheck::where('group_id', intval($groupId))
                                ->where('user_id', Auth::id())
                                ->first();

        // The group from Laravel
        $group = SGroup::where('idGroup', intval($groupId))->first();

        // ensure that everything is right
        if (empty($rankChecker) || empty($group)) {
            return '';
        }

        // Send the keywords
        $Casper->setKeywords($group->keywords->lists('name')->toArray());

        // set the tracker id
        $Casper->setRanktrackerId(intval($groupId));

        // tell our class to generate the casper files
        $Casper->generateKeywords();

        // tell our class to send the casper file to the API
        $Casper->sendAPIRequest();

        // debug
        //$Casper->setCasper(9);

        // process the file itself
        $Casper->processFile();

        // send back results as json

        // but for now, just redirect
        return Redirect::route('admin.tools.ranktracker.view', $groupId);
    }
}
