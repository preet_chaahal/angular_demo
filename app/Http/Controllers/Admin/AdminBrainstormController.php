<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Repositories\AdminBrainstormRepository;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Redirect;
use Session;

class AdminBrainstormController extends Controller
{

    private $adminBrainstormRepository;
    private $userRepo;

    function __construct(AdminBrainstormRepository $adminBrainstormRepository, UserRepository $userRepo)
    {
        $this->adminBrainstormRepository = $adminBrainstormRepository;
        $this->userRepo                  = $userRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $brainstorms = $this->adminBrainstormRepository->all();

        return view('admin.tools.brainstorm.index', ['brainstorms' => $brainstorms]);
    }

    public function view($brainstormId)
    {
        $brainstorm = $this->adminBrainstormRepository->find($brainstormId);
        $json_array = array();
        $hasData    = false;

        // if we have any data, lets decode it
        if ( ! empty($brainstorm->data)) {
            $hasData    = true;
            $json_array = unserialize(base64_decode($brainstorm->data));
        }

        return view('admin.tools.brainstorm.view', [
            'hasData'    => $hasData,
            'brainstorm' => $brainstorm,
            'keywords'   => json_encode($json_array)
        ]);
    }

    public function add()
    {
        //retrieve all users
        $allUsers = $this->userRepo->all();

        return view('admin.tools.brainstorm.add', ['allUsers' => $allUsers]);
    }

    public function insert(Request $request)
    {
        $rules = [
            'useraccount' => 'required|not_in:Select',
            'keyword'     => 'required'
        ];

        $this->validate($request, $rules);

        // Grab user id from input useraccount
        $userAccount   = $request->input('useraccount');
        $userLookup    = $userLookup = $this->userRepo->find($userAccount);
        $userAccountID = $userLookup->id;

        $brainstorm = $this->adminBrainstormRepository->save([
            'user_id' => $userAccountID,
            'keyword' => $request->input('keyword')
        ]);

        // generate the suggestions
        $this->adminBrainstormRepository->getSuggestions($brainstorm->id);

        // forward the user on the the right route
        return Redirect::route('admin.tools.brainstorm.view', $brainstorm->id);
    }

    public function refresh($brainstormId)
    {
        $brainstorm = $this->adminBrainstormRepository->find($brainstormId);

        if (empty($brainstorm)) {
            return Redirect::route('admin.tools.brainstorm.index');
        }

        // generate the suggestions
        $this->adminBrainstormRepository->getSuggestions($brainstorm->id);

        // forward the user on the the right route
        return Redirect::route('admin.tools.brainstorm.view', $brainstorm->id);
    }

    public function delete($brainstormId)
    {
        $brainstorm = $this->adminBrainstormRepository->delete($brainstormId);

        if (empty($brainstorm)) {
            return Redirect::route('admin.tools.brainstorm.index');
        }

        // tell user
        Session::flash('success', array('The keyword has been deleted.'));

        // return
        return Redirect::route('admin.tools.brainstorm.index');
    }
}
