<?php

namespace App\Http\Controllers\Admin;

use Auth;
use App\Role;
use App\Account;
use App\RoleLimit;
use App\Permission;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use App\Http\Controllers\Controller;
use DB;
use Validator;
use ChargeBee_Environment;
use ChargeBee_Plan;
class RoleManagementController extends Controller
{
    public function index($roleName)
    {
        /**
        * @var Collection $accounts
        * @var Account $currentAccount
        * @var Role $role
        */
        ChargeBee_Environment::configure(getenv('CHARGEBEE_SITE'), getenv('CHARGEBEE_KEY'));
        $all = ChargeBee_Plan::all(array(
          "limit" => 40, 
          "status[is]" => "active"));
        $plans = array();
        if(isset($all) && !empty($all)) {
            $i = 0;
            foreach($all as $entry) {
                if(!isset($entry->plan()->trialPeriod) && $entry->plan()->price && $entry->plan()->status == 'active' && $entry->plan()->periodUnit != "week") {
                    $plans[$i]['id'] = $entry->plan()->id;
                    $plans[$i]['name'] = $entry->plan()->name;
                    $plans[$i]['period'] = $entry->plan()->periodUnit;
                    $i++;
                }
            }
        }

        $accounts = Account::where(["disabled" => 0])->join('roles', 'accounts.name', '=', 'roles.name')->get();

        $currentAccount = $accounts->where('name', $roleName)->first();
        if(!$currentAccount) {
            return redirect()->back();
        }

        $role = $currentAccount->getRole();
        if(!$role) {
            return redirect()->back();
        }

        $rolePermissions = $role->perms()->get();
        $permissions = array_pluck($rolePermissions->toArray(), 'name');

        $roleLimit = RoleLimit::where('role_id', $role->id)->first()->toArray();
        $allPermissions = Permission::all();
        $data = array_pluck($allPermissions->toArray(), 'name');

        foreach ($data as $perm) {
            $data[$perm] = false;
        }
        foreach ($permissions as $perm) {
            $data[$perm] = true;
        }

        $roleId['roleName'] = $roleName;
        $roleId['role'] = $role->name;
        $roleId['screen_name'] = (isset($role->screen_name) ? $role->screen_name : '');
        $roleId['default_plan'] = (isset($role->default_plan) ? $role->default_plan : '');
        $roleId['chargebeeplan_id'] = $role->chargebee_plan_id;
        $roleId['is_trial'] = (isset($role->is_trial) ? $role->is_trial : 0);
        $roleId['trial_days'] = (isset($role->trial_days) ? $role->trial_days : 0);
        $roleId['sorting_order'] = (isset($role->sorting_order) ? $role->sorting_order : 0);
        $roleId['plan_type'] = (isset($role->plan_type) ? $role->plan_type : 0);
        $roleId['status'] = (isset($role->status) ? $role->status : 0);
        $data = array_merge($roleLimit, $data);
        $data['accounts'] = $accounts;
        $data['chargebee_plan'] = $plans;

        //echo "<pre>"; print_r($data); exit();
        return View('admin.plan.index', $data, $roleId);
    }

    public function planUpdate(Request $request, $role_name)
    {
        $inputs = $request->all();
        $role   = Role::where('name', $inputs['role'])->first();
        $role_limit = RoleLimit::where('role_id', $role->id)->first();
        $accnt  = Account::where('name', $inputs['role'])->first();
        if (!$accnt) {
            return redirect()->back();
        }
        if (empty($role_limit)) {
            $role_limit = new RoleLimit();
            $role_limit->role_id = $role->id;
        }

        $validator = Validator::make($request->all(), [
            'is_trial' => 'unique:roles,is_trial,'.$role->id,
            'research'  => 'numeric',
            'evolution' => 'numeric',
            'screen_name' =>'required|min:3|max:8|unique:roles,screen_name,'.$role->id,
        ]);

        $validator->sometimes(['can_save','project'], 'numeric', function($data){
           return $data->keyword_save == true;
        });

        $validator->sometimes(['brainstorm'], 'numeric', function($data){
           return $data->brainstorm_tool == true;
        });

        $validator->sometimes(['tracking'], 'numeric', function($data){
           return $data->rank_checker == true;
        });

        if ($validator->fails()) {
            return redirect('admin/plan/free')
                        ->withErrors($validator)
                        ->withInput();
        }
        $trialCheck = (isset($inputs['is_trial']) ? $inputs['is_trial'] : 0);
        if($trialCheck == 1) {
            if($role_name != 'free') {
                $this->validate($request, [
                    'trial_days' =>'required|numeric|min:1|max:30',
                ]);
            }
        }
        $accnt->description     = "Keyword Revealer ".(isset($inputs['screen_name']) ? $inputs['screen_name'] : '');
        $accnt->save();

        $role_limit->research   = $inputs['research'];
        $role_limit->brainstorm = isset($inputs['brainstorm_tool'])?$inputs['brainstorm']:0;
        $role_limit->evolution  = $inputs['evolution'];
        $role_limit->tracking   = isset($inputs['rank_checker'])?$inputs['tracking']:0;
        $role_limit->can_save   = isset($inputs['keyword_save'])?$inputs['can_save']:0;
        $role_limit->project    = isset($inputs['keyword_save'])?$inputs['project']:0;
        $role_limit->save();

        $role->screen_name = (isset($inputs['screen_name']) ? $inputs['screen_name'] : '');
        $role->status           = (isset($inputs['plan_status']) ? $inputs['plan_status'] : 0);

        if($role->is_trial > 0) {
            $role->trial_days    = (isset($inputs['trial_days']) ? $inputs['trial_days'] : 0);
            //$role->chargebee_plan_id = "";
        } else {
            $role->trial_days    = 0;
            //if($role_name == 'free' || $role_name == 'trial') {
            //    $role->chargebee_plan_id = "";  
            //} else {
            //    $role->chargebee_plan_id = (isset($inputs['chargebee_plan_id']) ? $inputs['chargebee_plan_id'] : '');  
            //}
        }
        $role->sorting_order = (isset($inputs['sorting_order']) ? $inputs['sorting_order'] : 4);
        $role->save();

        $roleId['role'] = $role->name;
        foreach ($inputs as $input) {
            $searchPermission[] = array_search("true", $inputs);
            $key = array_search("true", $inputs);
            unset($inputs[$key]);
        }

        foreach ($searchPermission as $sperms) {
            if ($sperms != false) {
                $updatePermissions[] = Permission::where('name', $sperms)->first()->id;
            }
        }

        $role->perms()->sync($updatePermissions);
        $role_permissions = $role->perms()->get();

        $permissions = array_pluck($role_permissions->toArray(), 'name');

        $data = [];
        $roleLimit = RoleLimit::where('role_id', $role->id)->first()->toArray();
        $allPermissions = Permission::all();
        $data = array_pluck($allPermissions->toArray(), 'name');
        //$allPermissions = array_flip($allPermissions);
        foreach ($data as $perm) {
            $data[$perm] = false;
        }
        foreach ($permissions as $perm) {
            $data[$perm] = true;
        }

        $roleId['role'] = $role->name;
        $data = array_merge($roleLimit, $data);

        return redirect(route('admin.plan', $role->name));
    }

    //check current user limits
    public function getCurrentUserLimit($limit_field)
    {
        $roles = Auth::user()->roles;
        $role  = $roles[0];
        $limit = RoleLimit::where('role_id', $role->id)->first();

        if (!empty($limit)) {
            $limitCount = $limit->$limit_field;
        } else {
            $limitCount = 0;
        }

        return $limitCount;
    }

    //check trial account subscription
    public function checkTrialSubscription()
    {
        $user = Auth::user();
        if(isset($user->is_trial) && $user->is_trial == 1) {
            $cDate      = date("Y-m-d");
            if($cDate > $user->trial_expire_date) {
                return true;
            }
        }
        return false;
    }

    public function planAdd() {
        ChargeBee_Environment::configure(getenv('CHARGEBEE_SITE'), getenv('CHARGEBEE_KEY'));
        $all = ChargeBee_Plan::all(array(
          "limit" => 40, 
          "status[is]" => "active"));
        $plans = array();
        if(isset($all) && !empty($all)) {
            $i = 0;
            foreach($all as $entry) {
                if(!isset($entry->plan()->trialPeriod) && $entry->plan()->price && $entry->plan()->status == 'active' && $entry->plan()->periodUnit != "week") {
                    $plans[$i]['id'] = $entry->plan()->id;
                    $plans[$i]['name'] = $entry->plan()->name;
                    $plans[$i]['period'] = $entry->plan()->periodUnit;
                    $i++;
                }
            }
        }
        //$accounts = Account::where(["disabled" => 0])->get();
        $accounts = Account::where(["disabled" => 0])->join('roles', 'accounts.name', '=', 'roles.name')->get();
        $roleId['roleName']     = "";
        $roleId['role']         = "";
        $roleId['screen_name']  = (isset($role->screen_name) ? $role->screen_name : '');
        $roleId['default_plan'] = (isset($role->default_plan) ? $role->default_plan : '');
        $roleId['chargebeeplan_id'] = "";
        $roleId['is_trial']     = (isset($role->is_trial) ? $role->is_trial : 0);
        $roleId['trial_days']   = (isset($role->trial_days) ? $role->trial_days : 0);
        $roleId['sorting_order']= (isset($role->sorting_order) ? $role->sorting_order : 0);
        $roleId['plan_type']    = (isset($role->plan_type) ? $role->plan_type : 0);
        $roleId['status']       = (isset($role->status) ? $role->status : 0);
        //$data                   = array_merge($roleLimit, $data);
        $data['accounts'] = $accounts;
        $data['chargebee_plan'] = $plans;
        return View('admin.plan.add', $data, $roleId);
    }

    public function planCreate(Request $request) {
        DB::beginTransaction();
        try {
            $inputs     = $request->all();
            if(isset($inputs['screen_name']) && str_replace(" ", "", $inputs['screen_name']) != "") {
                $name = strtolower(str_replace(" ", "_", $inputs['screen_name']));
            } else {
                $name = "";
            }
            
            $inputs['name'] = $name;
            $role      = new Role();
            $role_limit= new RoleLimit();
            $accnt     = new Account();

            $validator = Validator::make($inputs, [
                    'name'          => 'required|unique:roles,name|max:12',
                    'research'      => 'numeric',
                    'evolution'     => 'numeric',
                    'screen_name'   => 'required|min:3|max:8|unique:roles,screen_name',
                    'chargebee_plan_id' => 'required|unique:roles,chargebee_plan_id',
                ],
                [
                    'name.required' => 'Please enter the valid screen name.',
                    'name.unique'   => 'Please enter the valid screen name.',
                    'name.max'      => 'Screen name length may not be greater than 8 characters.',
                    'chargebee_plan_id.unique' => "The chargebee plan has already been taken."
                ]
            );

            $role->name = $name;
            $validator->sometimes(['name'], 'required|unique:roles,name', function($data) {
               return $data->name == true;
            });

            $validator->sometimes(['can_save','project'], 'numeric', function($data) {
               return $data->keyword_save == true;
            });

            $validator->sometimes(['brainstorm'], 'numeric', function($data){
               return $data->brainstorm_tool == true;
            });

            $validator->sometimes(['tracking'], 'numeric', function($data){
               return $data->rank_checker == true;
            });

            if ($validator->fails()) {
                return redirect('admin/newplan/add')
                            ->withErrors($validator)
                            ->withInput();
            }

            $role->display_name = (isset($inputs['screen_name']) ? $inputs['screen_name']." Plan" : '');
            $role->description  = (isset($inputs['screen_name']) ? $inputs['screen_name']." Plan" : '');
            $role->screen_name  = (isset($inputs['screen_name']) ? $inputs['screen_name'] : '');
            $role->status            = (isset($inputs['plan_status']) ? $inputs['plan_status'] : 0);

            $role->chargebee_plan_id = (isset($inputs['chargebee_plan_id']) ? $inputs['chargebee_plan_id'] : '');

            $role->sorting_order     = (isset($inputs['sorting_order']) ? $inputs['sorting_order'] : 4);
            $role->save();

            $roleId['role'] = $role->name;
            $accnt->name    = $role->name;
            $accnt->stripe_plan     = "Keyword Revealer ".(isset($inputs['screen_name']) ? $inputs['screen_name'] : '');
            $accnt->paypal_plan     = "Keyword Revealer ".(isset($inputs['screen_name']) ? $inputs['screen_name'] : '');
            $accnt->description     = "Keyword Revealer ".(isset($inputs['screen_name']) ? $inputs['screen_name'] : '');
            $accnt->save();

            $role_limit->role_id    = $role->id;
            $role_limit->research   = $inputs['research'];
            $role_limit->brainstorm = isset($inputs['brainstorm_tool'])?$inputs['brainstorm']:0;
            $role_limit->evolution  = $inputs['evolution'];
            $role_limit->tracking   = isset($inputs['rank_checker'])?$inputs['tracking']:0;
            $role_limit->can_save   = isset($inputs['keyword_save'])?$inputs['can_save']:0;
            $role_limit->project    = isset($inputs['keyword_save'])?$inputs['project']:0;
            $role_limit->description= (isset($inputs['screen_name']) ? $inputs['screen_name']." Plan" : '');
            $role_limit->save();
            DB::commit();

            foreach ($inputs as $input) {
                $searchPermission[] = array_search("true", $inputs);
                $key = array_search("true", $inputs);
                unset($inputs[$key]);
            }

            foreach ($searchPermission as $sperms) {
                if ($sperms != false) {
                    $updatePermissions[] = Permission::where('name', $sperms)->first()->id;
                }
            }

            $role->perms()->sync($updatePermissions);
            $role_permissions = $role->perms()->get();
            $permissions    = array_pluck($role_permissions->toArray(), 'name');

            $data           = [];
            $roleLimit      = RoleLimit::where('role_id', $role->id)->first()->toArray();
            $allPermissions = Permission::all();
            $data = array_pluck($allPermissions->toArray(), 'name');
            //$allPermissions = array_flip($allPermissions);
            foreach ($data as $perm) {
                $data[$perm]= false;
            }
            foreach ($permissions as $perm) {
                $data[$perm]= true;
            }

            $roleId['role'] = $role->name;
            $data           = array_merge($roleLimit, $data);
            return redirect(route('admin.plan', $role->name));
        } catch (\Exception $e) {
            DB::rollBack();
            $msg = $e->getMessage() ? $e->getMessage() : "Unexpected errors, please try again!";
            return redirect(route('admin.newplan.add'));
        }
    }
}
