<?php

namespace App\Http\Controllers\Admin;

use App\AccessProxyLog;
use App\AccessProxy;
use App\Research;
use App\TblEvaluationStatus;
use App\TblKeywordStatus;
use App\TblBrainstormStatus;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Evaluation;
use App\User;
use App\Payment;
use App\Invoice;
use App\Account;
use Redirect;
use App\Utilities\ExportCsv;
use App\Services\ApiLoggerService;
use App\ApiLog;

class ReportController extends Controller
{
    public function __construct()
    {
    }

    /**
     * Display a listing of the user.
     *
     * @return Response
     */
    public function index()
    {
        return View('admin.reports.index', [
        ]);
    }

    public function users()
    {
        return view('admin.reports.users', [
        ]);
    }

    public function userReport(Request $request)
    {
        try {
            $date =$request->date;
            $dates = explode(' - ', $date);
            $start_date = date('Y-m-d', strtotime($dates[0])) . ' 00:00:00';
            $end_date = date('Y-m-d', strtotime($dates[1])) . ' 23:59:59';

            $totalRegUser = User::all()->count();
            $rangeUser = User::whereBetween('created_at', [$start_date, $end_date])->where('is_registered', '1')->count();
            $start = Carbon::createFromFormat('Y-m-d', substr($start_date, 0, 10));
            $end = Carbon::createFromFormat('Y-m-d', substr($end_date, 0, 10));

            $freeId = Account::getByAccountName('free')->id;
            $basicId = Account::getByAccountName('basic')->id;
            $proId = Account::getByAccountName('pro')->id;
            $proyearlyId = Account::getByAccountName('proyearly')->id;
            $eliteId = Account::getByAccountName('elite')->id;
            $agencyId = Account::getByAccountName('agency')->id;
            $basicYearlyId = Account::getByAccountName('basic_yearly')->id;
            $eliteYearlyId = Account::getByAccountName('elite_yearly')->id;
            $agencyYearlyId = Account::getByAccountName('agency_yearly')->id;

            $planIdsArr = [$freeId, $basicId, $proId, $proyearlyId, $eliteId, $agencyId, $basicYearlyId, $eliteYearlyId, $agencyYearlyId];

            $userDateArr = [];
            while ($start <= $end) {
                $userDateArr[] = $start->copy()->format('m-d-Y');
                $start->addDay();
            }

            foreach ($userDateArr as $date) {
                $sub_start_date = Carbon::createFromFormat('m-d-Y',$date)->startOfDay();
                $sub_end_date =  Carbon::createFromFormat('m-d-Y',$date)->endOfDay();
                $regUserCountArr[] = User::whereBetween('created_at', [$sub_start_date, $sub_end_date])->where('is_registered', '1')->count();

                $stripeSubCount = Invoice::where('status', 'paid')->whereBetween('date', [$sub_start_date, $sub_end_date])->count();
                $paypalSubCount = Payment::where('payment_status', 'Completed')->whereBetween('payment_date', [$sub_start_date, $sub_end_date])->count();

                $sameMonthSubCountArr[] = $stripeSubCount + $paypalSubCount;
            }
            // get subscription Details
            $pm_sdate = Carbon::createFromFormat('Y-m-d', substr($start_date, 0, 10));
            $pm_edate = Carbon::createFromFormat('Y-m-d', substr($end_date, 0, 10));

            $pm_sdate->subMonth()->format('Y-m-d');
            $pm_edate->subMonth()->format('Y-m-d');

            $start = Carbon::createFromFormat('Y-m-d', substr($pm_sdate, 0, 10));
            $end = Carbon::createFromFormat('Y-m-d', substr($pm_edate, 0, 10));

            $prevMonthSubdateArr = [];
            while ($start <= $end) {
                $prevMonthSubdateArr[] = $start->copy();//->format('m-d-Y');
                $start->addDay();
            }
            foreach ($prevMonthSubdateArr as $date) {
                $sub_start_date = date('Y-m-d', strtotime($date)) . ' 00:00:00';
                $sub_end_date = date('Y-m-d', strtotime($date)) . ' 23:59:59';

                $stripeSubCount = Invoice::where('status', 'paid')->whereBetween('date', [$sub_start_date, $sub_end_date])->count();
                $paypalSubCount = Payment::where('payment_status', 'Completed')->whereBetween('payment_date', [$sub_start_date, $sub_end_date])->count();

                $prevMonthSubCountArr[] = $stripeSubCount + $paypalSubCount;
            }

            $rangeBasicPlanStripe = Invoice::where('plan_id', $basicId)->where('status', 'paid')->whereBetween('date', [$start_date, $end_date])->count();
            $rangeProPlanStripe = Invoice::where('plan_id', $proId)->where('status', 'paid')->whereBetween('date', [$start_date, $end_date])->count();
            $rangeElitePlanStripe = Invoice::where('plan_id', $eliteId)->where('status', 'paid')->whereBetween('date', [$start_date, $end_date])->count();
            $rangeAgencyPlanStripe = Invoice::where('plan_id', $agencyId)->where('status', 'paid')->whereBetween('date', [$start_date, $end_date])->count();
            $rangeBasicYearlyPlanStripe = Invoice::where('plan_id', $basicYearlyId)->where('status', 'paid')->whereBetween('date', [$start_date, $end_date])->count();
            $rangeProYearlyPlanStripe = Invoice::where('plan_id', $proyearlyId)->where('status', 'paid')->whereBetween('date', [$start_date, $end_date])->count();
            $rangeEliteYearlyPlanStripe = Invoice::where('plan_id', $eliteYearlyId)->where('status', 'paid')->whereBetween('date', [$start_date, $end_date])->count();
            $rangeAgencyYearlyPlanStripe = Invoice::where('plan_id', $agencyYearlyId)->where('status', 'paid')->whereBetween('date', [$start_date, $end_date])->count();

            $rangeBasicPlanPaypal = Payment::where('plan_type', 'Keyword Revealer Basic')->where('payment_status', 'Completed')->whereBetween('payment_date', [$start_date, $end_date])->count();
            $rangeProPlanPaypal = Payment::where('plan_type', 'Keyword Revealer Pro')->where('payment_status', 'Completed')->whereBetween('payment_date', [$start_date, $end_date])->count();
            $rangeElitePlanPaypal = Payment::where('plan_type', 'Keyword Revealer Monthly Elite')->where('payment_status', 'Completed')->whereBetween('payment_date', [$start_date, $end_date])->count();
            $rangeAgencyPlanPaypal = Payment::where('plan_type', 'Keyword Revealer Monthly Agency')->where('payment_status', 'Completed')->whereBetween('payment_date', [$start_date, $end_date])->count();
            $rangeBasicYearlyPlanPaypal = Payment::where('plan_type', 'Keyword Revealer Yearly Basic')->where('payment_status', 'Completed')->whereBetween('payment_date', [$start_date, $end_date])->count();
            $rangeProYearlyPlanPaypal = Payment::where('plan_type', 'Keyword Revealer Pro Yearly')->where('payment_status', 'Completed')->whereBetween('payment_date', [$start_date, $end_date])->count();
            $rangeEliteYearlyPlanPaypal = Payment::where('plan_type', 'Keyword Revealer Yearly Elite')->where('payment_status', 'Completed')->whereBetween('payment_date', [$start_date, $end_date])->count();
            $rangeAgencyYearlyPlanPaypal = Payment::where('plan_type', 'Keyword Revealer Yearly Agency')->where('payment_status', 'Completed')->whereBetween('payment_date', [$start_date, $end_date])->count();

            $startDay = Carbon::now()->startOfDay();
            $endDay = Carbon::now()->endOfDay();

            $todayBasicPlanStripe = Invoice::where('plan_id', $basicId)->where('status', 'paid')->whereBetween('date', [$startDay, $endDay])->count();
            $todayProPlanStripe = Invoice::where('plan_id', $proId)->where('status', 'paid')->whereBetween('date', [$startDay, $endDay])->count();
            $todayElitePlanStripe = Invoice::where('plan_id', $eliteId)->where('status', 'paid')->whereBetween('date', [$startDay, $endDay])->count();
            $todayAgencyPlanStripe = Invoice::where('plan_id', $agencyId)->where('status', 'paid')->whereBetween('date', [$startDay, $endDay])->count();
            $todayBasicYearlyPlanStripe = Invoice::where('plan_id', $basicYearlyId)->where('status', 'paid')->whereBetween('date', [$startDay, $endDay])->count();
            $todayProYearlyPlanStripe = Invoice::where('plan_id', $proyearlyId)->where('status', 'paid')->whereBetween('date', [$startDay, $endDay])->count();
            $todayEliteYearlyPlanStripe = Invoice::where('plan_id', $eliteYearlyId)->where('status', 'paid')->whereBetween('date', [$startDay, $endDay])->count();
            $todayAgencyYearlyPlanStripe = Invoice::where('plan_id', $agencyYearlyId)->where('status', 'paid')->whereBetween('date', [$startDay, $endDay])->count();

            $todayBasicPlanPaypal = Payment::where('plan_type', 'Keyword Revealer Basic')->where('payment_status', 'Completed')->whereBetween('payment_date', [$startDay, $endDay])->count();
            $todayProPlanPaypal = Payment::where('plan_type', 'Keyword Revealer Pro')->where('payment_status', 'Completed')->whereBetween('payment_date', [$startDay, $endDay])->count();
            $todayElitePlanPaypal = Payment::where('plan_type', 'Keyword Revealer Monthly Elite')->where('payment_status', 'Completed')->whereBetween('payment_date', [$startDay, $endDay])->count();
            $todayAgencyPlanPaypal = Payment::where('plan_type', 'Keyword Revealer Monthly Agency')->where('payment_status', 'Completed')->whereBetween('payment_date', [$startDay, $endDay])->count();
            $todayBasicYearlyPlanPaypal = Payment::where('plan_type', 'Keyword Revealer Yearly Basic')->where('payment_status', 'Completed')->whereBetween('payment_date', [$startDay, $endDay])->count();
            $todayProYearlyPlanPaypal = Payment::where('plan_type', 'Keyword Revealer Pro Yearly')->where('payment_status', 'Completed')->whereBetween('payment_date', [$startDay, $endDay])->count();
            $todayEliteYearlyPlanPaypal = Payment::where('plan_type', 'Keyword Revealer Yearly Elite')->where('payment_status', 'Completed')->whereBetween('payment_date', [$startDay, $endDay])->count();
            $todayAgencyYearlyPlanPaypal = Payment::where('plan_type', 'Keyword Revealer Yearly Agency')->where('payment_status', 'Completed')->whereBetween('payment_date', [$startDay, $endDay])->count();

            $canceledBasicPlan = User::where('account_id', $basicId)->whereBetween('subscription_ends_at', [$start_date, $end_date])->count();
            $canceledProPlan = User::where('account_id', $proId)->whereBetween('subscription_ends_at', [$start_date, $end_date])->count();
            $canceledElitePlan = User::where('account_id', $eliteId)->whereBetween('subscription_ends_at', [$start_date, $end_date])->count();
            $canceledAgencyPlan = User::where('account_id', $agencyId)->whereBetween('subscription_ends_at', [$start_date, $end_date])->count();
            $canceledBasicYearlyPlan = User::where('account_id', $basicYearlyId)->whereBetween('subscription_ends_at', [$start_date, $end_date])->count();
            $canceledProYearlyPlan = User::where('account_id', $proyearlyId)->whereBetween('subscription_ends_at', [$start_date, $end_date])->count();
            $canceledEliteYearlyPlan = User::where('account_id', $eliteYearlyId)->whereBetween('subscription_ends_at', [$start_date, $end_date])->count();
            $canceledAgencyYearlyPlan = User::where('account_id', $agencyYearlyId)->whereBetween('subscription_ends_at', [$start_date, $end_date])->count();

            $start_date = date('Y-m-d', strtotime($dates[1])) . ' 00:00:00';
            $end_date = date('Y-m-d', strtotime($dates[1])) . ' 23:59:59';
            $start = Carbon::createFromFormat('Y-m-d', substr($start_date, 0, 10));
            $end  = Carbon::createFromFormat('Y-m-d', substr($end_date, 0, 10));
            $start->subDays(4);


            $researchDateArr = [];
            while ($start <= $end) {
                $researchDateArr[] = $start->copy()->format('m-d-Y');
                $start->addDay();
            }
            foreach ($researchDateArr as $date) {
                $sub_start_date = Carbon::createFromFormat('m-d-Y',$date)->startOfDay();
                $sub_end_date =  Carbon::createFromFormat('m-d-Y',$date)->endOfDay();

                $researchCountArr[] = TblKeywordStatus::whereBetween('created_at', [$sub_start_date, $sub_end_date])
                                        ->whereIn('user_current_level', $planIdsArr)->count();
                $researchesFreePlan[] = $this->countResearchesByPlan($sub_start_date, $sub_end_date, $freeId);
                $researchesBasicPlan[] = $this->countResearchesByPlan($sub_start_date, $sub_end_date, $basicId);
                $researchesProPlan[] = $this->countResearchesByPlan($sub_start_date, $sub_end_date, $proId);
                $researchesElitePlan[] = $this->countResearchesByPlan($sub_start_date, $sub_end_date, $eliteId);
                $researchesAgencyPlan[] = $this->countResearchesByPlan($sub_start_date, $sub_end_date, $agencyId);
                $researchesBasicYearlyPlan[] = $this->countResearchesByPlan($sub_start_date, $sub_end_date, $basicYearlyId);
                $researchesProYearlyPlan[] = $this->countResearchesByPlan($sub_start_date, $sub_end_date, $proyearlyId);
                $researchesEliteYearlyPlan[] = $this->countResearchesByPlan($sub_start_date, $sub_end_date, $eliteYearlyId);
                $researchesAgencyYearlyPlan[] = $this->countResearchesByPlan($sub_start_date, $sub_end_date, $agencyYearlyId);

                $evaluationsCountArr[] = TblEvaluationStatus::whereBetween('created_at', [$sub_start_date, $sub_end_date])
                                        ->whereIn('user_current_level', $planIdsArr)->count();
                $evaluationsFreePlan[] = $this->countEvaluationsByPlan($sub_start_date, $sub_end_date, $freeId);
                $evaluationsBasicPlan[] = $this->countEvaluationsByPlan($sub_start_date, $sub_end_date, $basicId);
                $evaluationsProPlan[] = $this->countEvaluationsByPlan($sub_start_date, $sub_end_date, $proId);
                $evaluationsElitePlan[] = $this->countEvaluationsByPlan($sub_start_date, $sub_end_date, $eliteId);
                $evaluationsAgencyPlan[] = $this->countEvaluationsByPlan($sub_start_date, $sub_end_date, $agencyId);
                $evaluationsBasicYearlyPlan[] = $this->countEvaluationsByPlan($sub_start_date, $sub_end_date, $basicYearlyId);
                $evaluationsProYearlyPlan[] = $this->countEvaluationsByPlan($sub_start_date, $sub_end_date, $proyearlyId);
                $evaluationsEliteYearlyPlan[] = $this->countEvaluationsByPlan($sub_start_date, $sub_end_date, $eliteYearlyId);
                $evaluationsAgencyYearlyPlan[] = $this->countEvaluationsByPlan($sub_start_date, $sub_end_date, $agencyYearlyId);

                $brainstormsCountArr[] = TblBrainstormStatus::whereBetween('created_at', [$sub_start_date, $sub_end_date])
                                        ->whereIn('user_current_level', $planIdsArr)->count();
                $brainstormsFreePlan[] = $this->countBrainstormsByPlan($sub_start_date, $sub_end_date, $freeId);
                $brainstormsBasicPlan[] = $this->countBrainstormsByPlan($sub_start_date, $sub_end_date, $basicId);
                $brainstormsProPlan[] = $this->countBrainstormsByPlan($sub_start_date, $sub_end_date, $proId);
                $brainstormsElitePlan[] = $this->countBrainstormsByPlan($sub_start_date, $sub_end_date, $eliteId);
                $brainstormsAgencyPlan[] = $this->countBrainstormsByPlan($sub_start_date, $sub_end_date, $agencyId);
                $brainstormsBasicYearlyPlan[] = $this->countBrainstormsByPlan($sub_start_date, $sub_end_date, $basicYearlyId);
                $brainstormsProYearlyPlan[] = $this->countBrainstormsByPlan($sub_start_date, $sub_end_date, $proyearlyId);
                $brainstormsEliteYearlyPlan[] = $this->countBrainstormsByPlan($sub_start_date, $sub_end_date, $eliteYearlyId);
                $brainstormsAgencyYearlyPlan[] = $this->countBrainstormsByPlan($sub_start_date, $sub_end_date, $agencyYearlyId);

                $errorEmptyResponse[] = ApiLog::where('api_type', ApiLoggerService::ERROR_EMPTY_RESPONSE)->whereBetween('created_at', [$sub_start_date, $sub_end_date])->count();
                $errorKeywordsSearch[] = ApiLog::where('api_type', ApiLoggerService::API_TYPE_KEYWORDS_SEARCH)->whereBetween('created_at', [$sub_start_date, $sub_end_date])->count();
                $errorKeywordsAdditionalSearch[] = ApiLog::where('api_type', ApiLoggerService::API_TYPE_KEYWORDS_ADDITIONAL_SEARCH)->whereBetween('created_at', [$sub_start_date, $sub_end_date])->count();
                $errorSocialNetworks[] = ApiLog::where('api_type', ApiLoggerService::API_TYPE_SOCIAL_NETWORKS)->whereBetween('created_at', [$sub_start_date, $sub_end_date])->count();
                $errorGoogleMoz[] = ApiLog::where('api_type', ApiLoggerService::API_TYPE_GOOGLE_MOZ)->whereBetween('created_at', [$sub_start_date, $sub_end_date])->count();
                
                $resErrorCountArr[] = ApiLog::whereBetween('created_at', [$sub_start_date, $sub_end_date])->count();
            }

            // get proxy error Details
            $evaluationBadProxy = AccessProxy::where('status', 0)->where('module', 1)->count();
            $brainstormBadProxy = AccessProxy::where('status', 0)->where('module', 2)->count();
            $rankBadProxy = AccessProxy::where('status', 0)->where('module', 3)->count();

            $data = array(
                'totalRegUser'=>$totalRegUser,
                'userDateArr'=>$userDateArr,
                'prevMonthSubdatetArr'=>$prevMonthSubdateArr,
                'sameMonthSubCountArr'=>$sameMonthSubCountArr,
                'prevMonthSubCountArr'=>$prevMonthSubCountArr,
                'regUserCountArr'=>$regUserCountArr,

                'rangeBasicPlan' => $rangeBasicPlanStripe + $rangeBasicPlanPaypal,
                'rangeProPlan' => $rangeProPlanStripe + $rangeProPlanPaypal,
                'rangeElitePlan' => $rangeElitePlanStripe + $rangeElitePlanPaypal,
                'rangeAgencyPlan' => $rangeAgencyPlanStripe + $rangeAgencyPlanPaypal,
                'rangeBasicYearlyPlan' => $rangeBasicYearlyPlanStripe + $rangeBasicYearlyPlanPaypal,
                'rangeProYearlyPlan' => $rangeProYearlyPlanStripe + $rangeProYearlyPlanPaypal,
                'rangeEliteYearlyPlan' => $rangeEliteYearlyPlanStripe + $rangeEliteYearlyPlanPaypal,
                'rangeAgencyYearlyPlan' => $rangeAgencyYearlyPlanStripe + $rangeAgencyYearlyPlanPaypal,

                'todayBasicPlan' => $todayBasicPlanStripe + $todayBasicPlanPaypal,
                'todayProPlan' => $todayProPlanStripe + $todayProPlanPaypal,
                'todayElitePlan' => $todayElitePlanStripe + $todayElitePlanPaypal,
                'todayAgencyPlan' => $todayAgencyPlanStripe + $todayAgencyPlanPaypal,
                'todayBasicYearlyPlan' => $todayBasicYearlyPlanStripe + $todayBasicYearlyPlanPaypal,
                'todayProYearlyPlan' => $todayProYearlyPlanStripe + $todayProYearlyPlanPaypal,
                'todayEliteYearlyPlan' => $todayEliteYearlyPlanStripe + $todayEliteYearlyPlanPaypal,
                'todayAgencyYearlyPlan' => $todayAgencyYearlyPlanStripe + $todayAgencyYearlyPlanPaypal,

                'canceledBasicPlan' => $canceledBasicPlan,
                'canceledProPlan' => $canceledProPlan,
                'canceledElitePlan' => $canceledElitePlan,
                'canceledAgencyPlan' => $canceledAgencyPlan,
                'canceledBasicYearlyPlan' => $canceledBasicYearlyPlan,
                'canceledProYearlyPlan' => $canceledProYearlyPlan,
                'canceledEliteYearlyPlan' => $canceledEliteYearlyPlan,
                'canceledAgencyYearlyPlan' => $canceledAgencyYearlyPlan,


                'researchDateArr'=>$researchDateArr,
                'researchCountArr'=>$researchCountArr,
                'researchesFreePlan'=>$researchesFreePlan,
                'researchesBasicPlan'=>$researchesBasicPlan,
                'researchesProPlan'=>$researchesProPlan,
                'researchesElitePlan'=>$researchesElitePlan,
                'researchesAgencyPlan'=>$researchesAgencyPlan,
                'researchesBasicYearlyPlan'=>$researchesBasicYearlyPlan,
                'researchesProYearlyPlan'=>$researchesProYearlyPlan,
                'researchesEliteYearlyPlan'=>$researchesEliteYearlyPlan,
                'researchesAgencyYearlyPlan'=>$researchesAgencyYearlyPlan,

                'evaluationsDateArr'=>$researchDateArr,
                'evaluationsCountArr'=>$evaluationsCountArr,
                'evaluationsFreePlan'=>$evaluationsFreePlan,
                'evaluationsBasicPlan'=>$evaluationsBasicPlan,
                'evaluationsProPlan'=>$evaluationsProPlan,
                'evaluationsElitePlan'=>$evaluationsElitePlan,
                'evaluationsAgencyPlan'=>$evaluationsAgencyPlan,
                'evaluationsBasicYearlyPlan'=>$evaluationsBasicYearlyPlan,
                'evaluationsProYearlyPlan'=>$evaluationsProYearlyPlan,
                'evaluationsEliteYearlyPlan'=>$evaluationsEliteYearlyPlan,
                'evaluationsAgencyYearlyPlan'=>$evaluationsAgencyYearlyPlan,

                'brainstormsDateArr' => $researchDateArr,
                'brainstormsCountArr' => $brainstormsCountArr,
                'brainstormsFreePlan' => $brainstormsFreePlan,
                'brainstormsBasicPlan' => $brainstormsBasicPlan,
                'brainstormsProPlan' => $brainstormsProPlan,
                'brainstormsElitePlan' => $brainstormsElitePlan,
                'brainstormsAgencyPlan' => $brainstormsAgencyPlan,
                'brainstormsBasicYearlyPlan' => $brainstormsBasicYearlyPlan,
                'brainstormsProYearlyPlan' => $brainstormsProYearlyPlan,
                'brainstormsEliteYearlyPlan' => $brainstormsEliteYearlyPlan,
                'brainstormsAgencyYearlyPlan' => $brainstormsAgencyYearlyPlan,

                'resErrorDateArr' => $researchDateArr,
                'resErrorCountArr' => $resErrorCountArr,
                'errorEmptyResponse' => $errorEmptyResponse,
                'errorKeywordsSearch' => $errorKeywordsSearch,
                'errorKeywordsAdditionalSearch' => $errorKeywordsAdditionalSearch,
                'errorSocialNetworks' => $errorSocialNetworks,
                'errorGoogleMoz' => $errorGoogleMoz,

                'rangeUser'=>$rangeUser,

                'evaluationBadProxy' => $evaluationBadProxy,
                'brainstormBadProxy' => $brainstormBadProxy,
                'rankBadProxy' => $rankBadProxy,
            );

            return response()->json($data);
        } catch (\Exception $e) {
            dd($e);
        }
    }

    public function countResearchesByPlan($start_date, $end_date, $plan_id)
    {
        return TblKeywordStatus::whereBetween('created_at', [$start_date, $end_date])->where('user_current_level', $plan_id)->count();
    }

    public function countEvaluationsByPlan($start_date, $end_date, $plan_id)
    {
        return TblEvaluationStatus::whereBetween('created_at', [$start_date, $end_date])->where('user_current_level', $plan_id)->count();
    }

    public function countBrainstormsByPlan($start_date, $end_date, $plan_id)
    {
        return TblBrainstormStatus::whereBetween('created_at', [$start_date, $end_date])->where('user_current_level', $plan_id)->count();
    }

    public function usersDownload(Request $request)
    {
        $csvData     = array();
        $rules       = [
            'date_range' => 'required|regex:/[0-9]+\/[0-9]+\/[0-9]+.-.[0-9]+\/[0-9]+\/[0-9]+/',
            'type'       => 'required'
        ];

        $this->validate($request, $rules);

        // lets split our dates up
        $dates = explode(' - ', $request->input('date_range'));
        $dates[0] = date('Y-m-d', strtotime($dates[0])) .' 00:00:00';
        $dates[1] = date('Y-m-d', strtotime($dates[1])) .' 23:59:59';


        $users = User::whereBetween('last_login', [$dates[0], $dates[1]]);

        switch(intval($request->input('type')))
        {
            case 1:
                $users->whereBetween('created_at', [$dates[0], $dates[1]]);
                break;

            default:
                break;
        }

        foreach($users->get() as $user)
        {
            $csvData[] = $user->toArray();
        }

        $csv = new ExportCsv($csvData, 'user_report.csv', false);
        $csv->send();
        die;
    }

    public function evaluation()
    {
        return view('admin.reports.evaluation', [
        ]);
    }

    public function evaluationDownload(Request $request)
    {
        $csvData     = array();
        $rules       = [
            'date_range' => 'required|regex:/[0-9]+\/[0-9]+\/[0-9]+.-.[0-9]+\/[0-9]+\/[0-9]+/'
        ];

        $this->validate($request, $rules);

        // lets split our dates up
        $dates = explode(' - ', $request->input('date_range'));
        $dates[0] = date('Y-m-d', strtotime($dates[0])) .' 00:00:00';
        $dates[1] = date('Y-m-d', strtotime($dates[1])) .' 23:59:59';

        $evaluations = Evaluation::whereBetween('created_at', [$dates[0], $dates[1]])->get();

        foreach($evaluations as $evaluation)
        {
            $csvData[] = $evaluation->toArray();
        }

        $csv = new ExportCsv($csvData, 'evluation_report.csv', false);
        $csv->send();
        die;
    }

}
