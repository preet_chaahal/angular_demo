<?php

namespace App\Http\Controllers\Admin;

use App\AccessProxy;
use App\AccessProxyLog;
use App\Exceptions\ApiOperationFailedException;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Permission;
use App\Repositories\AdminRepository;
use App\Role;
use App\RoleLimit;
use Illuminate\Http\Request;
use Redirect;
use Session;

class AdminController extends Controller
{
    private $adminRepository;

    function __construct(AdminRepository $adminRepository)
    {
        $this->adminRepository = $adminRepository;
    }

    public function dashboard()
    {
        return Redirect::to('/admin/users');
    }

    /*
     * Proxy settings
     *
     */
    public function proxies($module)
    {
        if ($module == 'rank-tool-proxy') {
            $moduleId = 3;
        } elseif ($module == 'keyword-evolution-proxy') {
            $moduleId = 1;
        } elseif ($module == 'keyword-brainstorming-proxy') {
            $moduleId = 2;
        }
        $proxies = $this->adminRepository->proxies($moduleId);
        return View('admin.proxies.index', ['proxies' => $proxies, 'module' => $module]);
    }

    public function proxiesLog(Request $request,$id){
        $proxy = AccessProxy::whereId($request->get('id'))->first();
        if (empty($proxy)) {
            return redirect()->back()->withErrors([
                'message' => 'Record not found',
            ]);
        }
        $proxyLog = AccessProxyLog::whereProxyId($proxy->id)->orderBy('updated_at', 'desc')->get();
        return view('admin.proxies.proxy-log', ['ip' => $id, 'logs' => $proxyLog]);
    }

    public function planUpdate(Request $request)
    {
        $inputs = $request->all();
        $role = Role::where('name', $inputs['role'])->first();
        $role_limit = RoleLimit::where('role_id', $role->id)->first();
        if (empty($role_limit)) {
            $role_limit = new RoleLimit();
            $role_limit->role_id = $role->id;
        }
        $role_limit->research = $inputs['research'];
        $role_limit->brainstorm = $inputs['brainstorm'];
        $role_limit->evolution = $inputs['evolution'];
        $role_limit->tracking = $inputs['tracking'];
        $role_limit->can_save = $inputs['can_save'];
        $role_limit->save();
        $roleId['role'] = $role->name;
        foreach ($inputs as $input) {
            $searchPermission[] = array_search("true", $inputs);
            $key = array_search("true", $inputs);
            unset($inputs[$key]);
        }

        foreach ($searchPermission as $sperms) {
            if ($sperms != false) {
                $updatePermissions[] = Permission::where('name', $sperms)->first()->id;
            }

        }
        $role->perms()->sync($updatePermissions);
        $role_permissions = $role->perms()->get();

        $permissions = array_pluck($role_permissions->toArray(), 'name');

        $data = [];
        $roleLimit = RoleLimit::where('role_id', $role->id)->first();
        $allPermissions = Permission::all();
        $allPermissions = array_pluck($allPermissions->toArray(), 'name');
//        $allPermissions = array_flip($allPermissions);
        $data = array_merge($data, $allPermissions);
        foreach ($data as $perm) {
            $data[$perm] = false;
        }
        foreach ($permissions as $perm) {
            $data[$perm] = true;
        }

        return View('admin.plan.index', $data, $roleId, $roleLimit);
    }

    public function proxiesAdd($module)
    {
        if ($module == 'rank-tool-proxy') {
            $moduleId = 3;
        } elseif ($module == 'keyword-evolution-proxy') {
            $moduleId = 1;
        } elseif ($module == 'keyword-brainstorming-proxy') {
            $moduleId = 2;
        }
        return View('admin.proxies.add',['module'=>$moduleId]);
    }

    public function proxiesInsert(Request $request)
    {
        try {
            $rules = [
                'type' => 'required',
                'ip' => 'required'
            ];

            // validate input
            $this->validate($request, $rules);

            // validate against type iface
            if ($request->input('type') == 'iface' && $request->input('port') != null && $request->input('username') != null && $request->input('password') != null) {
                return Redirect::route('admin.proxies.add')->withErrors(['iface proxy doesn\'t need port/username or password']);
            }
            if ($request->input('type') == 'iface' && $request->input('username') != null) {
                return Redirect::route('admin.proxies.add')->withErrors(['iface proxy doesn\'t need port/username or password']);
            }
            if ($request->input('type') == 'iface' && $request->input('password') != null) {
                return Redirect::route('admin.proxies.add')->withErrors(['iface proxy doesn\'t need port/username or password']);
            }

            $proxy = AccessProxy::where('ip', trim($request->get('ip')))->whereModule($request->get('module'))->first();
            if ($request->get('module') == 3) {
                $module = 'rank tracker';
            } elseif ($request->get('module') == 2) {
                $module = 'keyword evaluation';
            } elseif ($request->get('module') == 1) {
                $module = 'keyword research';
            }
            if(!empty($proxy))
            {
                return redirect()->back()->withInput($request->all())->withErrors([
                    'message' => "Given proxy ".$request->get('ip')." already exist in ".$module,
                ]);
            }
            $this->adminRepository->saveProxy($request->all());

            // tell user

            $module = $request->input('module');

            // tell user
            Session::flash('success', array('The proxy has been added'));
            // redirect them
            if ($module == 3) {
                return Redirect::route('admin.proxies', 'rank-tool-proxy');
            } elseif ($module == 1) {
                return Redirect::route('admin.proxies', 'keyword-evolution-proxy');
            } elseif ($module == 2) {
                return Redirect::route('admin.proxies', 'keyword-brainstorming-proxy');
            }
        } catch (\Exception $e) {
            return redirect()->back()->withInput($request->all())->withErrors([
                'message' => $e->getMessage(),
            ]);
        }

    }

    public function proxiesImport($module)
    {
        if ($module == 'rank-tool-proxy') {
            $moduleId = 3;
        } elseif ($module == 'keyword-evolution-proxy') {
            $moduleId = 1;
        } elseif ($module == 'keyword-brainstorming-proxy') {
            $moduleId = 2;
        }
        return View('admin.proxies.import',['module'=>$moduleId]);
    }

    public function proxiesImportPost(Request $request)
    {
        try {
            $rules = [
                'type' => 'required',
                'bulkProxy' => 'required'
            ];

            // validate input
            $this->validate($request, $rules);

            //Get bulk input
            $text = trim($request->input('bulkProxy'));
            $textAr = explode("\n", $text);
            $textAr = array_filter($textAr, 'trim'); // remove any extra \r characters left behind

            $this->adminRepository->proxiesImport($textAr, $request->input('type'), $request->input('module'));
            // tell user

            $module = $request->input('module');

            // tell user
            Session::flash('success', array('The proxies have been Imported'));
            // redirect them
            if ($module == 3) {
                return Redirect::route('admin.proxies', 'rank-tool-proxy');
            } elseif ($module == 1) {
                return Redirect::route('admin.proxies', 'keyword-evolution-proxy');
            } elseif ($module == 2) {
                return Redirect::route('admin.proxies', 'keyword-brainstorming-proxy');
            }
        } catch (\Exception $e) {
            return redirect()->back()->withInput($request->all())->withErrors([
                'message' => $e->getMessage(),
            ]);
        }
    }

    public function proxiesDelete($id)
    {
        $module = AccessProxy::where('id', $id)->first();
        $result = $this->adminRepository->proxiesDelete($id);

        if (!$result) {
            return Redirect::route('admin.proxies');
        }

        // tell user


        // tell user
        Session::flash('success', array('The proxy has been deleted'));

        if ($module->module == 3) {
            return Redirect::route('admin.proxies', 'rank-tool-proxy');
        } elseif ($module->module == 1) {
            return Redirect::route('admin.proxies', 'keyword-evolution-proxy');
        } elseif ($module->module == 2) {
            return Redirect::route('admin.proxies', 'keyword-brainstorming-proxy');
        }
    }

    public function proxiesEdit($id)
    {
        $proxy = $this->adminRepository->findProxy($id);

        if (empty($proxy)) {
            return Redirect::route('admin.proxies.index');
        }

        return view('admin.proxies.edit', ['proxy' => $proxy]);
    }

    public function proxiesUpdate(Request $request)
    {
        $rules = [
            'type' => 'required',
            'ip' => 'required'
        ];

        // validate input
        $this->validate($request, $rules);

        // validate against type iface
        if ($request->input('type') == 'iface' && $request->input('port') != null && $request->input('username') != null && $request->input('password') != null) {
            return Redirect::route('admin.proxies.add')->withErrors(['iface proxy doesn\'t need port/username or password']);
        }
        if ($request->input('type') == 'iface' && $request->input('username') != null) {
            return Redirect::route('admin.proxies.add')->withErrors(['iface proxy doesn\'t need port/username or password']);
        }
        if ($request->input('type') == 'iface' && $request->input('password') != null) {
            return Redirect::route('admin.proxies.add')->withErrors(['iface proxy doesn\'t need port/username or password']);
        }


        $this->adminRepository->saveProxy($request->all());
        $input = $request->all();
        $module = AccessProxy::where('id', $input['id'])->first();
        // tell user
        Session::flash('success', array('The proxy has been updated'));

        if ($module->module == 3) {
            return Redirect::route('admin.proxies', 'rank-tool-proxy');
        } elseif ($module->module == 1) {
            return Redirect::route('admin.proxies', 'keyword-evolution-proxy');
        } elseif ($module->module == 2) {
            return Redirect::route('admin.proxies', 'keyword-brainstorming-proxy');
        }
    }

    /*
    End Proxy settings
     */

    /*
     * Registration Management
     */
    public function registrationManagement()
    {
        $regs = $this->adminRepository->getRegistrationManagement();

        return View('admin.registrationmanagement', ['regs' => $regs]);
    }

    public function disableIP()
    {
        return View('admin.disableip');
    }

    public function disableIPPost(Request $request)
    {
        $rules = [
            'ip' => 'required'
        ];

        // validate input
        $this->validate($request, $rules);

        $input = [];
        $input['ip'] = $request->input('ip');
        $input['disabled'] = "1";

        $this->adminRepository->saveRegistrationManagement($input);

        // tell user
        Session::flash('success', array('The IP has been added'));

        // redirect them
        return Redirect::route('admin.registration.management');
    }

    public function disableIPDelete($id)
    {
        $ip = $this->adminRepository->deleteRegistrationManagement($id);

        if (!$ip) {
            return Redirect::route('admin.registration.management');
        }

        // tell user
        Session::flash('success', array('The IP has been deleted'));

        // return
        return Redirect::route('admin.registration.management');
    }

    public function disableEmail()
    {
        return View('admin.disableemail');
    }

    public function disableEmailPost(Request $request)
    {
        $rules = [
            'email' => 'required'
        ];

        // validate input
        $this->validate($request, $rules);

        $input = [];
        $input['email_domain'] = $request->input('email');
        $input['disabled'] = "1";

        $this->adminRepository->saveRegistrationManagement($input);

        // tell user
        Session::flash('success', array('The Email has been added'));

        // redirect them
        return Redirect::route('admin.registration.management');
    }

    public function disableEMailDelete($id)
    {
        $email = $this->adminRepository->deleteRegistrationManagement($id);

        if (!$email) {
            return Redirect::route('admin.registration.management');
        }

        // tell user
        Session::flash('success', array('The Email has been deleted'));

        // return
        return Redirect::route('admin.registration.management');
    }

    public function registrationManagementToggle()
    {
        $reg = $this->adminRepository->findRegistrationManagementToggle();

        return View('admin.registrationmanagementtoggle', ['reg' => $reg]);
    }

    public function registrationManagementTogglePost(Request $request)
    {
        $this->adminRepository->findOrCreateRegistrationManagementToggle($request->all());

        // tell user
        Session::flash('success', array('Registration preferences have been updated'));

        // redirect them
        return Redirect::route('admin.registration.management');
    }

}
