<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Backend\Tools\KeywordResearchController as KeywordResearchController;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Repositories\UserRepository;
use App\User;
use App\Account;
use App\Role;
use Hash;
use Illuminate\Support\Facades\URL;
use Illuminate\Http\Request;
use Mail;
use Redirect;
use Session;
use ChargeBee_Environment;
use ChargeBee_Subscription;

/**
 * Class UserManagementController
 * @package App\Http\Controllers\Admin
 */
class UserManagementController extends Controller
{
    /**
     * @var UserRepository
     */
    public $userManageRepo;
    /** @var  User */
    public $user;

    /**
     * UserManagementController constructor.
     *
     * @param UserRepository $userManageRepo
     */
    public function __construct(UserRepository $userManageRepo)
    {
        $this->beforeFilter('auth');
        $this->userManageRepo = $userManageRepo;

    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $plans = Account::getPlans();
        $users = $this->userManageRepo->paginate(10);
        $deferLoading = $this->userManageRepo->all()->count();


        return View('admin.user.index', [
            'users' => $users,
            'plans' => $plans,
            'deferLoading' => $deferLoading
        ]);
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return View('admin.user.create');
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required',
            'username' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required',
            'account' => 'required|exists:accounts,id'
        ];

        // validate input
        $this->validate($request, $rules);
        // find default search engine for new user
        $location = new KeywordResearchController();
        $defaultSearchEngine = $location->getLocation(null, 'All Locations');

        $input = [];
        $input['name'] = $request->input('name');
        $input['username'] = $request->input('username');
        $input['email'] = $request->input('email');
        $input['account_id'] = $request->input('account');
        $input['password'] = Hash::make($request->input('password'));
        $input['is_admin'] = $request->input('admin');
        $input['password'] = Hash::make($request->input('password'));
        $input['defaultSearchEngine'] = $defaultSearchEngine;
        $this->userManageRepo->save($input);

        return Redirect::to('/admin/users');
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $user = $this->userManageRepo->find($id);

        return View('admin.user.edit', ['user' => $user]);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request)
    {
        $trial_plan = Role::where('is_trial',1)->first();
        if (!$trial_plan) {
            return redirect()->back();
        }

        $currentDate = date("Y-m-d");
        $id = $request->input('id');

        $rules = [
            'name' => 'required',
            'username' => 'required',
            'email' => 'required|email|unique:users,email,' . $id,
            'account' => 'required|exists:accounts,id'
        ];

        $accounts = Account::get();
        $currentAccount = $accounts->where('id',(int)$request->input('account'))->first();
        $roleData = $currentAccount->getRole();

        //validate input
        $this->validate($request, $rules);
        $this->user = $this->userManageRepo->find(intval($request->input('id')));

        //if ($this->user->stripe_active == 1) {
            // $plan_type = Account::getStripePlanById($request->input('account'));
            $trial_account_log = false;
            if ((isset($roleData['is_trial']) && $roleData['is_trial'] == 1) || (isset($roleData['default_plan']) && $roleData['default_plan'] == 1)) {
                if($this->user->payment_type == "chargebee" && $this->user->chargebee_subscription_id != "") {
                    if($this->user->chargebee_subscription_end_date < $currentDate) {
                        // cancel chargebee plan
                        $is_cancelled = $this->cancelChargebeeSubscription($this->user);
                        if($is_cancelled == true) {
                            $trial_account_log = true;
                        }
                    }
                } else {
                    $this->user->roles()->sync([$roleData->id]);
                    $data = array(
                        'name' => $this->user->name,
                        'canceldate' => $this->user->subscription_ends_at ? $this->user->subscription_ends_at->format('D M d Y') : null
                    );

                    Mail::send('emails.subscription.changefree', $data, function ($message) {
                        $message->to($this->user->email, $this->user->name)->subject('Keyword Revealer');
                    });
                    $trial_account_log = true;
                }
            }
            else {
                // $this->user->subscription($plan_type)->swap();
                /*
                $plan = Account::where('id', intval($request->input('account')))->first();
                $role = $plan->getRole();
                $this->user->roles()->sync([$role->id]);
                */
            }
        //}

        //get trial plan detials
        $trial_days = (isset($trial_plan['trial_days']) ? $trial_plan['trial_days'] : 0);
        $cDate      = date("Y-m-d");
        $trial_expire_date = date('Y-m-d', strtotime($cDate. ' + '.$trial_days.' days'));
        $this->user->name       = $request->input('name');
        $this->user->username   = $request->input('username');
        $this->user->email      = $request->input('email');

        if($trial_account_log == true) {
            $this->user->account_id                 = $request->input('account');
            $this->user->chargebee_customer_id      = "";
            $this->user->chargebee_subscription_id  = "";
            $this->user->chargebee_active           = 0;
            $this->user->is_trial                   = 1;
            $this->user->trial_expire_date          = $trial_expire_date;
        } else {
            $this->user->account_id                 = $this->user->account_id;
        }

        $this->user->disabled    = $request->input('disabled');
        $this->user->is_admin    = $request->input('admin');
        $this->user->toggle_free = $request->input('togglefree');

        if ($request->input('password') != null) {
            $this->user->password = Hash::make($request->input('password'));
        }

        $this->user->save();

        // flash user
        Session::flash('success', ['This user has been updated.']);

        // flash user
        Session::flash('success', ['This user has been updated.']);
        return Redirect::to('/admin/users');
    }

    public function cancelChargebeeSubscription($userinfo) {
        ChargeBee_Environment::configure(getenv('CHARGEBEE_SITE'), getenv('CHARGEBEE_KEY'));

        $this->user = $userinfo;
        $result = ChargeBee_Subscription::retrieve($this->user->chargebee_subscription_id);
        $subscription = $result->subscription();
        //$customer = $result->customer();
        //$card = $result->card();

        if(isset($subscription->status) && $subscription->status == "cancelled") {
            return true;
        }

        try {
            if(isset($this->user->chargebee_subscription_id) && !empty($this->user->chargebee_subscription_id)) {
                $result = ChargeBee_Subscription::cancel($this->user->chargebee_subscription_id);
                if(isset($result->subscription()->status) && $result->subscription()->status == "cancelled") {
                    $subscription   = $result->subscription();
                    $customer       = $result->customer();
                    $card           = $result->card();
                    $invoice        = $result->invoice();
                    //$unbilledCharges= $result->unbilledCharges();
                    //$creditNotes    = $result->creditNotes();
                    return true;
                }
            }
        } catch (\Exception $e) {
            Log::info(json_encode($e));
            Session::flash('nosuccess', array("failed to purchase failded to cancel. please try again later or contact our support"));
        }
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $this->userManageRepo->delete($id);

        return Redirect::to('/admin/users')->with('notice', 'User has been deleted.');
    }

    public function get(Request $request)
    {
        $totalUsersNum = $this->userManageRepo->all()->count();

        $filteredUsersNum = $this->userManageRepo
                    ->get(
                        0,
                        $totalUsersNum,
                        $request->get('search'),
                        $request->get('order')
                    )
                    ->count();

        $users = $this->userManageRepo
                    ->get(
                        $request->get('start', '0'),
                        $request->get('length', '10'),
                        $request->get('search'),
                        $request->get('order')
                    );

        $plans = Account::getPlans();

        $data = [];

        foreach ($users as $user) {
            $controls = '<a href="' . URL::route('admin.users.edit', $user->id) . '">Edit</a> | <a href="' . URL::route('admin.users.delete', $user->id) . '" onclick="return confirm(\'Are you sure you wish to delete this group?\');">Delete</a>';

            if($user->payment_type == 'chargebee') {
                $arr = array(
                    $user->name,
                    $user->username,
                    $user->email,
                    substr($plans[$user->account_id], 17),
                    $user->payment_type != '' ? $user->payment_type : 'N/A',
                    $user->chargebee_subscription_id,
                    $user->chargebee_subscription_id,
                    $user->chargebee_subscription_start_date,
                    $user->chargebee_subscription_end_date,
                    $user->is_admin ? 'Yes' : 'No',
                    $user->disabled ? 'Yes' : 'No',
                    $user->toggle_free ? 'Yes' : 'No',
                    $user->created_at->format('m/d/Y'),
                    $controls
                );
            } else {
                $arr = array(
                    $user->name,
                    $user->username,
                    $user->email,
                    substr($plans[$user->account_id], 17),
                    $user->payment_type != '' ? $user->payment_type : 'N/A',
                    $user->stripe_id,
                    $user->stripe_subscription,
                    $user->chargebee_subscription_start_date,
                    $user->subscription_ends_at,                    
                    $user->is_admin ? 'Yes' : 'No',
                    $user->disabled ? 'Yes' : 'No',
                    $user->toggle_free ? 'Yes' : 'No',
                    $user->created_at->format('m/d/Y'),
                    $controls
                );
            }
            $data[] = $arr;
        }

        $dataTableData = [
            "draw" => $request->get('draw', '0'),
            "recordsTotal" => $filteredUsersNum,
            "recordsFiltered" => $filteredUsersNum,
            "data" => $data
        ];

        return $dataTableData;
    }
}
