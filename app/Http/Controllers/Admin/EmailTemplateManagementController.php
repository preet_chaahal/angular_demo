<?php

namespace App\Http\Controllers\Admin;

use App\EmailTemplate;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Session;
use Validator;

/**
 * Class EmailTemplateManagementController
 * @package App\Http\Controllers\Admin
 */
class EmailTemplateManagementController extends Controller
{

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $emails = EmailTemplate::all();
        return View('admin.email.index', ['emails' => $emails]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add()
    {
        return View('admin.email.new');
    }

    /**
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        // validate input
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'subject' => 'required',
            'body' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator);
        }

        $email = new EmailTemplate();
        $email->name = $request->get('name');
        $email->subject = $request->get('subject');
        $email->body = $request->get('body');
        $email->save();
        Session::flash('success', ['Email Template has been added Successfully.']);
        return redirect('admin/email-template');
    }

    /**
     * @param $id
     * @return $this|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null
     */
    public function edit($id)
    {
        $email = EmailTemplate::find($id);
        if (empty($email)) {
            return redirect()->back()->withErrors('recoded can not Found');
        }
        return View('admin.email.edit', ['email' => $email]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null
     */
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'subject' => 'required',
            'body' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        $id = $request->get('id');
        $email = EmailTemplate::find($id);
        $email->name = $request->get('name');
        $email->subject = $request->get('subject');
        $email->body = $request->get('body');
        $email->save();
        Session::flash('success', ['Email Template has been updated Successfully.']);
        return redirect('admin/email-template');
    }

    /**
     * @param $id
     * @return $this|bool|null
     * @throws \Exception
     */
    public function delete($id)
    {
        $email = EmailTemplate::find($id)->delete();
        if (empty($email)) {
            return redirect()->back()->withErrors('recoded can not deleted');
        }
        Session::flash('success', ['Email Template has been deleted Successfully.']);
        return redirect('admin/email-template');
    }

    /**
     * @param $name
     * @return \Illuminate\Database\Eloquent\Model|null|static
     */
    public function getEmailTemplateByName($name)
    {
        return EmailTemplate::where('name',$name)->first();
    }
}