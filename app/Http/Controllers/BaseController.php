<?php namespace App\Http\Controllers;

use Request;
use Response;


abstract class BaseController extends Controller
{

    /**
     * Make standard response for just created resource
     *
     * @param object|array $data Data to be sent as JSON
     * @param int $code optional HTTP response code, default to 201
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondCreated($data, $code = 201)
    {
        return Response::json($data, $code);
    }

    /**
     * Make standard response with some data
     *
     * @param object|array $data Data to be send as JSON
     * @param int $code optional HTTP response code, default to 200
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithData($data, $code = 200)
    {
        return Response::json([
            'success' => true,
            'data' => $data
        ], $code);
    }


    /**
     * Make standard successful response ['success' => true, 'message' => $message]
     *
     * @param string $message Success message
     * @param int $code HTTP response code, default to 200
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondSuccess($message = 'Done!', $code = 200)
    {
        return Response::json([
            'success' => true,
            'message' => $message
        ], $code);
    }

    /**
     * Make standard response with error ['success' => false, 'message' => $message]
     *
     * @param string $message Error message
     * @param int $code HTTP response code, default to 500
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithError($message = 'Server error', $code = 500)
    {
        return Response::json([
            'success' => false,
            'message' => $message
        ], $code);
    }

    /**
     * Get JSON data from request, and validate if it can be processed
     *
     * @param bool $assoc should we return objects as arrays?
     *
     * @return array|object
     * @throws UnprocessableInputException
     */
    protected function getJson($assoc = true)
    {
        $json = Request::getContent();
        $data = json_decode($json, $assoc);

        return $data;
    }
}
