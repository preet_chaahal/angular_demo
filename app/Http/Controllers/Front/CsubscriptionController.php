<?php
namespace App\Http\Controllers\Front;
use App\User;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

/**
* Class SubscriptionController
* @package App\Http\Controllers\Front
*/
class CsubscriptionController extends Controller
{
	/**
	* @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	*/
    public function create() {
        $user = User::first();
        Auth::login($user);
		//$url = $user->subscription($planId)->withAddon($addonId)->getCheckoutUrl($embed);
        //Create the embedded checkout form and return it to the view.
        $url = $user->subscriptionfromchargebee('cbdemo_nuts')->withAddon('cbdemo_extranuts')->getCheckoutUrl(true);
        //return view('front.chargebeesubscription.hostedpage')->with(compact(['url', 'user']));
        return view('front.chargebeesubscription.hostedpage', [
            'url' => $url,
            'user' => $user
        ]);
    }

    public function handleCallback(Request $request)
    {
        //Get the authenticated user. Again, this is dummy code just for demonstration.
        $user = User::first();
        //Attach the subscription to the user from the hosted page identifier.

		$data = $user->subscription()->registerFromHostedPage($request->id);
		print_r($data); exit();

        //Return the user to a success page.
        return view('subscribe')->with(compact(['user']));
    }
}
