<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;

use Auth;
use Exception;
use App\Account;
use App\Http\Requests;
use App\RegistrationDisable;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Auth\SubscriptionController;

class LandingPageController extends Controller
{

    /**
     * AuthController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth', ['only' => ['checkout','verify']]);
    }

    /*
    * Handle Index
    */
    public function index(){
        return view('front.pages.landing.index');
    }

    /*
    * Handle Pricing
    */
    public function pricing(){

        $registration = RegistrationDisable::find('1');
        if (!$registration) {
            $registration = new \stdClass();
            $registration->disabled = false;
        }
        return view('front.pages.landing.pricing', [
            'registration' => $registration
        ]);
    }

    /*
    * Handle Checkout
    */
    public function checkout(Request $request){

         $user = Auth::user();
        if ($user->stripe_active) {
            $request->session()->flash("errorClass","cyan");
            return redirect()->route('subscription')
                             ->withErrors('You are currently subscribed to <b>'.$user->account->stripe_plan.'</b>, if you want to subscribe to a lifetime plan please cancel your current plan first.');
        }

        return (new SubscriptionController)->getJoin($request);
    }

    /*
    * Handle Checkout
    */
    public function verify(Request $request){
        return view('front.pages.landing.verify');
    }
}
