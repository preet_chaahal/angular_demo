<?php
namespace App\Http\Controllers\Front;

use App;
use App\Account;
use App\EmailTemplate;
use App\Http\Controllers\Admin\EmailTemplateManagementController;
use App\Http\Controllers\Backend\Tools\KeywordResearchController as KeywordResearchController;
use App\Http\Controllers\Auth\IntercomController as IntercomController;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\RegistrationDisable;
use App\RegistrationManagement;
use App\Role;
use App\User;
use Auth;
use Carbon\Carbon;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Mail;
use Redirect;
use Session;

/**
 * Class SubscriptionController
 * @package App\Http\Controllers\Front
 */
class SubscriptionController extends Controller
{

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function free()
    {
        if (Auth::user()) {
            Session::flash('success', ['you already logged in']);
            return redirect(route('backend.tools.keywordresearch'));
        }
        /** @var RegistrationDisable $registration */
        $registration = RegistrationDisable::find('1');

        if (!$registration) {
            $registration = new \stdClass();
            $registration->disabled = false;
        }

        return view('front.subscription.free', [
            'registration' => $registration
        ]);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postFree(Request $request)
    {
        // rules
        $rules = [
            // 'name' => 'required|min:3',
            'email' => 'required|unique:users|email',
            'password' => 'required|min:3|confirmed',
            // 'username' => 'required|unique:users',
        ];


        $hasCheckEmails = App\HasTable::all();

        foreach ($hasCheckEmails as $hasCheckEmail){
            if (Hash::check($request->email, $hasCheckEmail->email)) {

                Session::flash('nosuccess', ['This email already registered. Please contact support.']);
                return redirect()->back();

            }
        }

        /* Validate the campaign parameters */
        if ($request->landing_page) {
            $rules['plan'] = 'required|string|in:lifetime_agency,lifetime_marketer,lifetime_consultant';
        }

        $this->validate($request, $rules);

        //Deny login attempt if Email is in the banned list
        $email_input = explode("@", $request->input('email'));
        $email_domain = $email_input[1];

        $email = RegistrationManagement::where('email_domain', '=', $email_domain)->first();

        if ($email != null) {
            // flash user
            Session::flash('nosuccess', ['This email cannot be registered. Please contact support.']);

            return redirect()->back();
        }

        //Fake Email Verification
        $build_url = 'https://api.kickbox.io/v2/verify?email=' . $request->input('email'). '&apikey=' . getenv('kickbox_api');
        $ch=curl_init();
        $timeout=5;
        curl_setopt($ch, CURLOPT_URL, $build_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_TIMEOUT,$timeout);
        $result=curl_exec($ch);
        curl_close($ch);
        $check_email_response = json_decode($result);
        if (!empty($check_email_response))
        {
            //if ($check_email_response->result === 'undeliverable' || $check_email_response->result === 'unknown' || $check_email_response->result === 'risky')
            if ($check_email_response->result === 'undeliverable' || $check_email_response->result === 'unknown')
            {
                // flash user
                Session::flash('nosuccess', ['This email cannot be registered. Please contact support.']);

                return redirect()->back();
            }
        }

        //IP Verification
        $ip = $_SERVER['REMOTE_ADDR'];

        $usedIp = User::whereIn('ip',  array($ip))
            ->where('created_at', '>=', Carbon::now()->subHour(24))->count();

        if ($usedIp >= 3) {
            $disabled = true;
        } else {
            $disabled = false;
        }

        /** @var Account $freeAccount
        * @var $location
        * @var $defaultSearchEngine
        */
        $trial_plan = Role::where(['is_trial' => 1, 'status' => '1'])->first();
        if (isset($trial_plan) && !empty($trial_plan)) {
            $trial_days = (isset($trial_plan['trial_days']) ? $trial_plan['trial_days'] : 0);
            $cDate      = date("Y-m-d");
            $trial_expire_date = date('Y-m-d', strtotime($cDate. ' + '.$trial_days.' days'));          
            $freeAccount = Account::where('name', 'trial')->first();
            $is_trial    = 1;
            $trial_expire= $trial_expire_date;
            $used_trial_period=1;
        } else {
            $freeAccount = Account::where('name', 'free')->first();
            $is_trial    = 0;
            $trial_expire= "";
            $used_trial_period=0;            
        }

        if (empty($freeAccount)) {
            return redirect()->back()->withInput()->withErrors(['Free plan not exist, Please contact support.']);
        }
        // find default search engine for new user
        $location = new KeywordResearchController();
        $defaultSearchEngine = $location->getLocation(null, 'All Locations');

        $uname = explode("@",$request->input('email'));
        $u_name= (isset($uname[0]) ? $uname[0] : '--');
        // we are here so now we need to register the user and trigger the email
        $user = User::create([
            'name'      => $u_name,
            'username'  => strtolower($request->input('email')),
            'email'     => strtolower($request->input('email')),
            'password'  => Hash::make($request->input('password')),
            'defaultSearchEngine'   => $defaultSearchEngine,
            'defaultSearchLanguage' => 'all',
            'default_location'      => 'all',
            'is_registered'         => false,
            'register_token'        => md5(date('Y-m-d H:i:s')),
            'account_id'            => $freeAccount->id,
            'is_trial'              => $is_trial,
            'trial_expire_date'     => $trial_expire,
            'used_trial_period'     => $used_trial_period,
            'ip'        => $ip,
            'disabled'  => $disabled,
            'campaign'  => ($request->landing_page && $request->plan) ? $request->plan : null,
            'email_status'=> $check_email_response->result === 'deliverable'?'deliverable':'risky'
        ]);

        /*
        // Post_Affiliate_Pro
		$ch = curl_init();
		curl_setopt($ch,CURLOPT_URL,'http://keywordrevealer.postaffiliatepro.com/scripts/vdzfn3n4sm8?AccountId=default1&visitorId='.substr($request->input('pap_cookie'),8).'&OrderID='.$user->id.'&ProductID=free signup&ActionCode=registration&ip='.$ip.'&data1='.$request->input('email'));
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT,5);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
		curl_exec($ch);
		curl_close($ch);
        */
        if($is_trial == 1) {
            $freeRole = Role::where('name', 'trial')->first();
        } else {
            $freeRole = Role::where('name', 'free')->first();
        }

        if (!empty($freeRole)) {
            $user->roles()->sync([$freeRole->id]);
        }

        $this->sendActivationEmail($user->id);
        $intercomclient = new IntercomController();
        $intercomclient->createIntercomUser($user);
        if ($request->landing_page) {
            Auth::login($user);
            return Redirect::route('front.pages.landing.pricing.checkout', ['plan' => $request->plan,'_token' => $request->_token]);
        }

        // flash user
        Session::flash('success', ['Please check your email to authorize your email address']);
        session()->put('email',$request->email);
        // redirect

        $user_email = session('email',$request->email);
        $first_name = $u_name;

        return view('front.subscription.thankyou',compact('user_email','first_name'));
    }

    /**
     * @param $user_id
     */
    public function sendActivationEmail($user_id)
    {
        /** @var User $user */
        $user = User::where('id', $user_id)->first();

        /** @var EmailTemplateManagementController $emailTemplate */
        $emailTemplate = App::make(EmailTemplateManagementController::class);

        $template = $emailTemplate->getEmailTemplateByName(EmailTemplate::CONFORM_EMAIL_ACTIVATE_ACCOUNT_TEMPLATE);
        if (!empty($template)) {
            $body = $template->body;
            $body = str_replace('{USER_NAME}', $user->username, $body);
            $body = str_replace('{URL}', URL::route('auth.hash.index', $user->register_token), $body);
            $mailData = ['body' => $body];

            $email = $user->email;
            $subject = $template->subject;

            Mail::send('emails.blank', $mailData, function ($message) use ($email, $subject) {
                $message->to($email)->subject($subject);
            });
        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function thanks()
    {
        return view('front.subscription.thankyou');
    }


    public function getResendVerifyEmail(){

        return view('front.subscription.resend');
    }
   
   
    public function postResendVerifyEmail(Request $request){

         $checkUser = User::where('email',$request->email)->first();
           
        if(empty($checkUser)){

            Session::flash('nosuccess', ['Your email address is invalid. Please enter a valid address.']);

            return redirect('subscription/free');

        }elseif($checkUser->is_registered == 1){

            Session::flash('nosuccess', ['This email address is already verified']);

            return redirect('auth/login');

        }elseif ($checkUser->is_registered == 0) {

            $this->sendActivationEmail($checkUser->id);

            Session::flash('success', ['Verification email sent successfully, please check your email.']);

            return redirect()->back();

        }
        
    }

}
