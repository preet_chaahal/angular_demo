<?php
    namespace App\Http\Controllers\Front;

    use App;
    use App\EmailTemplate;
    use App\Http\Controllers\Admin\EmailTemplateManagementController;
    use Illuminate\Http\Request;
    use App\Http\Requests;
    use App\Http\Controllers\Controller;
    use Mail;
    use Input;
    use Session;
    use Redirect;
    use App\Role;
    use App\Account;
    use App\RoleLimit;
    use App\Permission;    
    use DB;
    use ChargeBee_Environment;
    use ChargeBee_Plan;
    class PagesController extends Controller
    {
        /**
         * default index
         * @return [type] [description]
         */
        private $chargebee_plan_limit = 40;        
        public function index()
        {
            ChargeBee_Environment::configure(getenv('CHARGEBEE_SITE'), getenv('CHARGEBEE_KEY'));
            $all = ChargeBee_Plan::all(array(
                "limit" => $this->chargebee_plan_limit, 
                "status[is]" => "active",
                "periodUnit[is]" => "year",
            ));

            $dataArr  = array();
            $dataArrY = array();
            $dataArrT = array();
            $responseArr = $this->getChargebeeplans($all);
            $dataArr  = $responseArr["dataArr"];
            $dataArrY = $responseArr["dataArrY"];
            // trial account
            $dataArrT = $this->getTrialPlanInformation();
           
            return view('front.pages.index',[
                'data' => $dataArr,
                'records_y' => $dataArrY,
                'data_trial'=> $dataArrT, 
            ]);
        }

        public function getTrialPlanInformation() {
            $dataArrT = array();
            $roleT    = Role::where('status', '1')->where('is_trial',1)->first();
            if(isset($roleT) && !empty($roleT)) {
                $rolePermissionsT = $roleT->perms()->get();
                $permissionsT     = array_pluck($rolePermissionsT->toArray(), 'name');
                
                $roleLimitT       = RoleLimit::where('role_id', $roleT->id)->first()->toArray();
                $allPermissionsT  = Permission::all();
                $dataT = array_pluck($allPermissionsT->toArray(), 'name');

                foreach ($dataT as $permT) {
                    $dataT[$permT] = false;
                }

                foreach ($permissionsT as $permT) {
                    $dataT[$permT] = true;
                }
                $dataArrT['role']  = $roleT;
                $dataArrT['role_limits'] = array_merge($roleLimitT, $dataT);
            }
            return $dataArrT;
        }

        public function getChargebeeplans($all) {
            if(isset($all) && !empty($all)) {
                $i = 0;
                $j = 0;
                $dataArr  = array();
                $dataArrY = array();
                foreach ($all as $entry) {
                    $ChargeBee_Plan = $entry->plan();
                    if(isset($ChargeBee_Plan->periodUnit) && $ChargeBee_Plan->periodUnit == "year" && $j < 4) {
                        $role = Role::where('status', '1')->where('chargebee_plan_id', $ChargeBee_Plan->id)->where('is_trial',0)->first();
                        if(isset($role) && !empty($role)) {
                            $rolePermissions = $role->perms()->get();
                            $permissions = array_pluck($rolePermissions->toArray(), 'name');
                            
                            $roleLimit = RoleLimit::where('role_id', $role->id)->first()->toArray();
                            $allPermissions = Permission::all();
                            $data = array_pluck($allPermissions->toArray(), 'name');

                            foreach ($data as $perm) {
                                $data[$perm] = false;
                            }

                            foreach ($permissions as $perm) {
                                $data[$perm] = true;
                            }
                            $dataArrY[$j]['sorting_order'] = (isset($role->sorting_order) ? $role->sorting_order : '');                        
                            $dataArrY[$j]['chargebee']['id'] = (isset($ChargeBee_Plan->id) ? $ChargeBee_Plan->id : '');
                            $dataArrY[$j]['chargebee']['invoice_name'] = (isset($ChargeBee_Plan->invoice_name) ? $ChargeBee_Plan->invoice_name : '');
                            $dataArrY[$j]['chargebee']['price'] = (isset($ChargeBee_Plan->price) ? $ChargeBee_Plan->price : '');
                            $dataArrY[$j]['chargebee']['period'] = (isset($ChargeBee_Plan->period) ? $ChargeBee_Plan->period : '');
                            $dataArrY[$j]['chargebee']['period_unit'] = (isset($ChargeBee_Plan->period_unit) ? $ChargeBee_Plan->period_unit : '');
                            $dataArrY[$j]['chargebee']['trial_period'] = (isset($ChargeBee_Plan->trial_period) ? $ChargeBee_Plan->trial_period : '');
                            $dataArrY[$j]['chargebee']['trial_period_unit'] = (isset($ChargeBee_Plan->trial_period_unit) ? $ChargeBee_Plan->trial_period_unit : '');
                            $dataArrY[$j]['chargebee']['charge_model'] = (isset($ChargeBee_Plan->charge_model) ? $ChargeBee_Plan->charge_model : '');
                            $dataArrY[$j]['chargebee']['free_quantity'] = (isset($ChargeBee_Plan->free_quantity) ? $ChargeBee_Plan->free_quantity : '');
                            $dataArrY[$j]['chargebee']['status'] = (isset($ChargeBee_Plan->status) ? $ChargeBee_Plan->status : '');
                            $dataArrY[$j]['chargebee']['enabled_in_hosted_pages'] = (isset($ChargeBee_Plan->enabled_in_hosted_pages) ? $ChargeBee_Plan->enabled_in_hosted_pages : '');
                            $dataArrY[$j]['chargebee']['enabled_in_portal'] = (isset($ChargeBee_Plan->enabled_in_portal) ? $ChargeBee_Plan->enabled_in_portal : '');
                            $dataArrY[$j]['chargebee']['taxable'] = (isset($ChargeBee_Plan->taxable) ? $ChargeBee_Plan->taxable : '');
                            $dataArrY[$j]['role'] = $role;
                            $dataArrY[$j]['role_limits'] = array_merge($roleLimit, $data);
                            $j++;
                        }
                    } else {
                        if($i < 4) {
                        $role = Role::where('status', '1')->where('chargebee_plan_id', $ChargeBee_Plan->id)->where('is_trial',0)->first();
                        if(isset($role) && !empty($role)) {
                            $rolePermissions = $role->perms()->get();
                            $permissions = array_pluck($rolePermissions->toArray(), 'name');
                            
                            $roleLimit = RoleLimit::where('role_id', $role->id)->first()->toArray();
                            $allPermissions = Permission::all();
                            $data = array_pluck($allPermissions->toArray(), 'name');

                            foreach ($data as $perm) {
                                $data[$perm] = false;
                            }

                            foreach ($permissions as $perm) {
                                $data[$perm] = true;
                            }
                            $dataArr[$i]['sorting_order'] = (isset($role->sorting_order) ? $role->sorting_order : '');
                            $dataArr[$i]['chargebee']['id'] = (isset($ChargeBee_Plan->id) ? $ChargeBee_Plan->id : '');
                            $dataArr[$i]['chargebee']['invoice_name'] = (isset($ChargeBee_Plan->invoice_name) ? $ChargeBee_Plan->invoice_name : '');
                            $dataArr[$i]['chargebee']['price'] = (isset($ChargeBee_Plan->price) ? $ChargeBee_Plan->price : '');
                            $dataArr[$i]['chargebee']['period'] = (isset($ChargeBee_Plan->period) ? $ChargeBee_Plan->period : '');
                            $dataArr[$i]['chargebee']['period_unit'] = (isset($ChargeBee_Plan->period_unit) ? $ChargeBee_Plan->period_unit : '');
                            $dataArr[$i]['chargebee']['trial_period'] = (isset($ChargeBee_Plan->trial_period) ? $ChargeBee_Plan->trial_period : '');
                            $dataArr[$i]['chargebee']['trial_period_unit'] = (isset($ChargeBee_Plan->trial_period_unit) ? $ChargeBee_Plan->trial_period_unit : '');
                            $dataArr[$i]['chargebee']['charge_model'] = (isset($ChargeBee_Plan->charge_model) ? $ChargeBee_Plan->charge_model : '');
                            $dataArr[$i]['chargebee']['free_quantity'] = (isset($ChargeBee_Plan->free_quantity) ? $ChargeBee_Plan->free_quantity : '');
                            $dataArr[$i]['chargebee']['status'] = (isset($ChargeBee_Plan->status) ? $ChargeBee_Plan->status : '');
                            $dataArr[$i]['chargebee']['enabled_in_hosted_pages'] = (isset($ChargeBee_Plan->enabled_in_hosted_pages) ? $ChargeBee_Plan->enabled_in_hosted_pages : '');
                            $dataArr[$i]['chargebee']['enabled_in_portal'] = (isset($ChargeBee_Plan->enabled_in_portal) ? $ChargeBee_Plan->enabled_in_portal : '');
                            $dataArr[$i]['chargebee']['taxable'] = (isset($ChargeBee_Plan->taxable) ? $ChargeBee_Plan->taxable : '');
                            $dataArr[$i]['role'] = $role;
                            $dataArr[$i]['role_limits'] = array_merge($roleLimit, $data);
                            $i++;
                        }
                        }
                    }
                }
            }

            array_multisort(array_column($dataArr, 'sorting_order'), SORT_ASC, $dataArr);
            array_multisort(array_column($dataArrY, 'sorting_order'), SORT_ASC, $dataArrY);
            return array("dataArr" => $dataArr, "dataArrY" => $dataArrY);
        }

        /**
        * contact page
        * @return [type] [description]
        */
        public function contact()
        {
            // captcha
            $captchaA = rand(0, 9);
            $captchaB = rand(0, 9);

            Session::put('captcha', md5($captchaA + $captchaB));

            return view('front.pages.contact', [
                'captchaA' => $captchaA,
                'captchaB' => $captchaB,
            ]);
        }

        public function contactPost(Request $request)
        {
            $rules = [
                'name'    => 'required',
                'email'   => 'required|email',
                'reason'  => 'required',
                'message' => 'required',
                'captcha' => 'required',
            ];

            $this->validate($request, $rules);

            // validate captcha
            if(! Session::has('captcha'))
            {
                return redirect()->back()->withInput()->withErrors(['Invalid Captcha']);
            }
            else
            {
                if(Session::get('captcha') != md5($request->input('captcha')))
                {
                    return redirect()->back()->withInput()->withErrors(['Invalid Captcha']);
                }
            }

            //Fake Email Verification
	        $build_url = 'https://api.kickbox.io/v2/verify?email=' . $request->input('email') . '&apikey=' . getenv('kickbox_api');
	        $ch=curl_init();
	        $timeout=5;
	        curl_setopt($ch, CURLOPT_URL, $build_url);
	        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
	        curl_setopt($ch, CURLOPT_TIMEOUT,$timeout);
	        $result=curl_exec($ch);
	        curl_close($ch);
	        $check_email_response = json_decode($result);
	        if (!empty($check_email_response))
	        {
	            if ($check_email_response->result === 'undeliverable' || $check_email_response->result === 'unknown' || $check_email_response->result === 'risky')
	            {
	                return redirect()->back()->withInput()->withErrors(['Invalid email address, Please check your email address and try again.']);
	            }
	        }



            // remove captcha from the session
            Session::forget('captcha');

            // we are here because everthing passed
            $name     = $request->input('name');
            $email    = $request->input('email');
            $reason   = $request->input('reason');
            $comments = $request->input('message');

            $data = array('name' => $name, 'email' => $email, 'reason' => $reason, 'comments' => $comments);
            $emailTemplate = App::make(EmailTemplateManagementController::class);
            $template = $emailTemplate->getEmailTemplateByName(EmailTemplate::CONTACT_US_EMAIL_TEMPLATE);
            if (!empty($template)) {
                $body = $template->body;
                $body = str_replace('{NAME}', $name, $body);
                $body = str_replace('{EMAIL}', $email , $body);
                $body = str_replace('{REASON}',  $reason, $body);
                $body = str_replace('{COMMENTS}', $comments , $body);
                $mailData = ['body' => $body];
                $email = 'help@keywordrevealer.com';
                $subject = $template->subject;

                /*Mail::send('emails.blank', $mailData, function ($message) use ($email, $subject) {
                    $message->to($email)->subject($subject);
                });*/

            }
            Mail::send('emails.contact', $data, function($message) use ($email, $reason)
            {
                $message->to('nz3wpa1l@inbound.intercom-mail.com')->subject($reason);
            });

            // tell user
            Session::flash('success', array('Your message has been sent'));

            return Redirect::route('front.contact');
        }

        /**
         * terms page
         * @return [type] [description]
         */
        public function terms()
        {
            return view('front.pages.terms');
        }

        /**
         * privacy page
         * @return [type] [description]
         */
        public function privacy()
        {
            return view('front.pages.privacy');
        }

    }