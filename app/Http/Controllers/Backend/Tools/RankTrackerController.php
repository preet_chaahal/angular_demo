<?php
namespace App\Http\Controllers\Backend\Tools;

use App;
use App\AccessProxyLog;
use App\Exceptions\ApiOperationFailedException;
use App\Http\Controllers\Admin\RoleManagementController;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\RankCheck;
use App\SCheck;
use App\SEvent;
use App\SGroup;
use App\SKeywords;
use App\SRank;
use App\STarget;
use App\Utilities\CasperCPC;
use App\Utilities\ExportCsv;
use App\Utilities\Proxy;
use App\Utilities\StringValidator;
use Auth;
use Carbon\Carbon;
use Config;
use DB;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Collection;
use Log;
use Redirect;
use Response;
use Session;

class RankTrackerController extends Controller
{
    public $roleManagementController;
    var $curlOptions;
    public $proxy;

    public function __construct()
    {
        // Must be signed in to access anything in here
        $this->proxyStatus = [
            'isAvailable' => true,
            'errMessage' => ''
        ];
        $this->middleware('auth');
        $this->roleManagementController = App::make(RoleManagementController::class);
        $module = 3;
        try {
            $this->proxy = $proxy = Proxy::select($module);
        } catch (\PhpSpec\Exception\Exception $e) {
            $this->proxyStatus['isAvailable'] = false;
            $this->proxyStatus['errMessage'] = $e->getMessage();
        }

        // setup CURL options
        if (!empty($proxy)) {
            if (strtolower($proxy->type) == 'http') {
                $proxy_address = $proxy->ip;
                if (count(trim($proxy->port)) > 0) {
                    $proxy_address .= ':' . $proxy->port;
                }

                $this->curlOptions[CURLOPT_PROXY] = $proxy_address;
            } elseif (strtolower($proxy->type) == 'iface') {
                $this->curlOptions[CURLOPT_INTERFACE] = $proxy->ip;
            }
        }
    }

    public function index()
    {
         $domains = Auth::user()->toolRankChecks;

        Session::forget('rt_date_start');
        Session::forget('rt_date_stop');

        return view('backend.tools.ranktracker.index', ['domains' => $domains, 'proxyStatus' => $this->proxyStatus]);
    }

    /**
     * just loads the add view
     */
    public function add()
    {
        $user = Auth::user();
        $tldOptions = Config::get('searchengines');
        $languages = Config::get('languages');

        usort($tldOptions, function($a,$b) {
            if ($a['name'] == 'All Locations') {
                return -1;
            } else if ($b['name'] == 'All Locations') {
                return 1;
            }
            return strcmp($a['name'], $b['name']);
        });

        $selectedEngine = 'All Locations';
        if ($user->default_location) {
            foreach ($tldOptions as $engine) {
                if ($engine['short_form'] == $user->default_location) {
                    $selectedEngine = $engine['name'];
                    break;
                }
            }
        } else {
            foreach ($tldOptions as $engine) {
                if ($engine['engine'] == $user->defaultSearchEngine) {
                    $selectedEngine = $engine['name'];
                    break;
                }
            }
        }

        $keyword = isset($_GET['projectname']) ? $_GET['projectname'] : '';

        $research = DB::connection('mysql_serposcope')->table('group as sg')
            ->leftJoin(env('DB_DATABASE').'.rankchecker as r', 'r.group_id', '=', 'sg.idGroup')
            ->where('sg.name', $keyword)
            ->where('user_id', Auth::id())
            ->first();
        if (!empty($research)) {
            return redirect()->back()->withErrors([
                'message' => 'This Keyword already searched.',
            ]);
        }
        return view('backend.tools.ranktracker.add', [
            'tldOptions' => json_encode($tldOptions),
            'selectedEngine' => $selectedEngine,
            'languages' => json_encode($languages),
            'selectedLanguage' => Auth::user()->defaultSearchLanguage
        ]);
    }

    public function insert(Request $request)
    {
        $user = Auth::user();
        $userSearches = $roles = DB::connection('mysql_serposcope')->table('keyword as kwd')
            ->leftjoin(env('DB_DATABASE').'.rankchecker as r', 'r.group_id', '=', 'kwd.idGroup')
            ->whereBetween('r.created_at',
                [Carbon::now()->startOfDay(), Carbon::now()->endOfDay()])->where('kwd.user_id', Auth::id())->count();
        $keywords = explode("\r\n", $request->input('keywords'));
        $keyword = count($keywords);
        $userSearched = $keyword + $userSearches;
        if (!$user->hasRole('admin')) {
            if ($this->roleManagementController->checkTrialSubscription() == true) {
                return redirect()->back()->withInput($request->all())->withErrors([
                    'message' => getenv('TRIAL_PLAN_MESSAGE'),
                ]);
            }

            if ($userSearched > $this->roleManagementController->getCurrentUserLimit('tracking')) {
                return redirect()->back()->withInput($request->all())->withErrors([
                    'message' => 'Limit reached, Upgrade your plan to increase your limit.',
                ]);
            }
        }
        $research = DB::connection('mysql_serposcope')->table('group as sg')
            ->leftJoin(env('DB_DATABASE').'.rankchecker as r', 'r.group_id', '=', 'sg.idGroup')
            ->where('sg.name', $request->input('keywords'))
            ->where('user_id', Auth::id())
            ->first();
        if (!empty($research)) {
            return redirect()->back()->withInput($request->all())->withErrors([
                'message' => 'This Project already searched.',
            ]);
        }
        $keywords = StringValidator::validate($request->input('keywords'));
        Input::replace(array_merge($request->all(), array('keywords' => $keywords)));

        $rules = [
            'name' => 'required',
            'keywords' => 'required',
            'domain' => 'required',
            'module' => 'required',
        ];

        // validate input
        $this->validate($request, $rules);
        if (empty($this->curlOptions)) {
            return redirect()->back()->withInput($request->all())->withErrors([
                'message' => 'Server is overloaded right now, try again later.']);
        }

        //Google URL
        $new_domain = $request->input('domain');
        $url = 'http://webcache.googleusercontent.com/search?q=cache:';
        $content = $this->curlUrl($url,$new_domain);

        if($content['not_found'] == 404){

            $domain_url = $content['domain'];

            Session::flash('nosuccess', array("Your domain seems not indexed by google, please check if your domain has been indexed by google here $domain_url, if you feel this is an error please contact support."));

            return Redirect::route('backend.tools.ranktracker');
        }

        if ( is_array($content) && isset($content['valid']) && $content['valid'] == false ) {
            $exp = explode(':', $content['error']);
            if (!isset($exp[0]) && !empty($exp[0])) {
                $log = ['ip' => $this->proxy->ip, 'error' => $exp[0]];
                AccessProxyLog::create($log);
            }
            $accessProxy = $this->proxy;
            $accessProxy->status = 0;
            $accessProxy->save();
        }
        /* Check for 404 (file not found). */

        $engines = Config::get('searchengines');
        $location = 'all';

        foreach ($engines as $engine) {
            if ($engine['name'] == $request->input('tld')) {
                $location = $engine['short_form'];
                $searchEngine = $engine['engine'];
                break;
            }
        }

        $id = $user->id;
        // create the Group
        $serpo = new SGroup;
        $serpo->name = $request->input('name');
        $serpo->module = 'Google';
        $serpo->options = [
            'tld' => $searchEngine,
            'location' => $location,
            'language' => $request->input('lang'),
            'datacenter' => $request->input('datacenter'),
            'local' => $request->input('local'),
            'parameters' => $request->input('parameters'),
            'user_id' => $id,
        ];
        $serpo->save();

        // store the group into the laravel application
        RankCheck::create([
            'user_id' => Auth::id(),
            'group_id' => $serpo->idGroup
        ]);

        // Store the keywords for the group
        $keywords = explode("\r\n", $request->input('keywords'));
        foreach (array_unique($keywords) as $word) {
            if (strlen(trim($word)) > 0) {
                $skey = SKeywords::create([
                    'idGroup' => $serpo->idGroup,
                    'name' => strtolower(trim($word)),
                    'user_id' => $id,
                ]);
            }
        }

        // Store the target domain for that group
        STarget::create([
            'idGroup' => $serpo->idGroup,
            'name' => $content['domain']
        ]);

        // tell user
        Session::flash('success', array('Great News! Your new group has been added.'));

        // redirect them
        // return Redirect::route('backend.tools.ranktracker.view', $serpo->idGroup);

        return $this->casperGenerator($serpo->idGroup);
    }

    /**
     * insert the specified keywords to check with the domain
     *
     * @param  Request $request [description]
     *
     * @return [type]           [description]
     */
    public function curlUrl($url,$domain)
    {
        $content = array(
            'valid' => false
        );

        if(preg_match('(http(s)?://)',$domain)){
            $with_http_url = parse_url($domain);
            $cache_url = $url.$with_http_url['host'];
        }else{
            $cache_url = $url.$domain;
            $with_http_url['host'] = $domain;
        //    dd($cache_url);
        }


        $curl = curl_init($cache_url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        // extra options
//        curl_setopt_array($curl, $options);

        // execute the curl and return the data (if there is some)
        $html = curl_exec($curl);
        $errorNumber = curl_errno($curl);
        $error = curl_error($curl);
        $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);

        if ($httpCode == 404) {
//            dd($with_http_url['host']);

            if(preg_match('/^www./',$with_http_url['host'])) {
                $remove_www_url = trim($with_http_url['host'], "www.");
                $cache_url_with_www = $url . $remove_www_url;
                $domain_url = $remove_www_url;
            }else{
                $cache_url_with_www  = $url.'www.'.$with_http_url['host'];
                $domain_url = 'www.'.$with_http_url['host'] ;
            }

            $www_cache_url = curl_init($cache_url_with_www);
            curl_setopt($www_cache_url, CURLOPT_RETURNTRANSFER, true);
            $res_html = curl_exec($www_cache_url);
            $www_httpCode = curl_getinfo($www_cache_url, CURLINFO_HTTP_CODE);
            curl_close($www_cache_url);

            if($www_httpCode == 404){
                // redirect them
                $www_content['not_found'] = 404;
                $www_content['domain']= $domain_url;

                return $www_content;
            }

            $www_content['valid'] = true;
            $www_content['response'] = $res_html;
            $www_content['error'] = null;
            $www_content['error_code'] = null;
            $www_content['domain']= $domain_url;
            $www_content['not_found'] = null;
//            // tell user
//            Session::flash('success', array('This domain is not yet listed in Google SERP'));
//
//            // redirect them
//            return Redirect::route('backend.tools.ranktracker');

            return $www_content;
        }

        if (!$errorNumber) {
            $content['valid'] = true;
            $content['response'] = $html;
            $content['error'] = null;
            $content['error_code'] = null;
            $content['domain'] =  $with_http_url['host'] ;
            $content['not_found'] = null;
        } else {
            // could log the error number and error itself
            $content['error'] = $error;
        }

        return $content;
    }

    /**
     * view the specific domain
     * AT SOME POINT THIS DOES NEED TO BE RESTRUCTURED... TOO MUCH HAPPENING HERE
     *
     * @param  integer $id [description]
     *
     * @return [type]     [description]
     */
    public function view(Request $request, $id)
    {
        // First lets see if we have any dates
        $userDates = $userRequestDates = [
            'start' => Session::get('rt_date_start', date('Y-m-') . '01'),
            'stop' => Session::get('rt_date_stop', date('Y-m-t')),
        ];

        $rankChecker = RankCheck::where('group_id', intval($id))
            ->where('user_id', Auth::id())
            ->first();

        if (empty($rankChecker)) {
            return Redirect::route('backend.tools.ranktracker');
        }

        $group = SGroup::where('idGroup', intval($id))->with('target')->first();

        if (empty($group)) {
            return Redirect::route('backend.tools.ranktracker');
        }

        $keywords = array();
        $rankTrackerData = array();
        $chartJs = array(
            'legend' => array(),
        ); // will be converted into JSON


        // fill up our keywords to store the information
        /** @var  SGroup $keyword */
        foreach ($group->keywords as $keyword) {
            $keywords[$keyword->idKeyword] = [
                'id' => $keyword->idKeyword,
                'keyword' => $keyword->name,
                'position' => null,
            ];
        }

        // fill up our array with the dates we need to check for
        $numberOfDays = abs(strtotime($userDates['start']) - strtotime($userDates['stop']));
        $numberOfDays = floor($numberOfDays / (60 * 60 * 24));

        for ($day = 0; $day <= $numberOfDays; $day++) {
            $currentDate = date('Ymd', strtotime('+' . $day . ' days', strtotime($userDates['start'])));

            $rankTrackerData[$currentDate] = array(
                'hasData' => false,
                'keywords' => $keywords,
                'events' => array(),
            );
        }

        // now going through each date
        foreach ($rankTrackerData as $date => $data) {
            $dateFormatted = date('Y-m-d', strtotime($date));

            // do we have any events on this day
            if ($group->target->events()->where('date', $dateFormatted)->count() > 0) {
                $rankTrackerData[$date]['hasData'] = true;

                foreach ($group->target->events()->where('date', $dateFormatted)->get() as $event) {
                    $rankTrackerData[$date]['events'][uniqid()] = [
                        'position' => -2,
                        'label' => 'Event: ' . $event->event
                    ];
                }
            }

            // do we have any keywords on this day
            $keywordScan = $group->lastRan()->whereBetween('date',
                [$dateFormatted . ' 00:00:00', $dateFormatted . ' 23:59:59']);

            if ($keywordScan->count() > 0) {
                $rankTrackerData[$date]['hasData'] = true;

                // our scan has idCheck
                // we can use this to find the position (SRANK)
                foreach ($keywordScan->get() as $scan) {
                    foreach ($keywords as $keyword) {
                        // check for rank
                        $rank = SRank::where('idKeyword', $keyword['id'])
                            ->where('idCheck', $scan->idCheck)
                            ->first();

                        // did this keyword have a rank?
                        if ($rank) {
                            $rankTrackerData[$date]['keywords'][$keyword['id']]['position'] = $rank->position;
                        }
                    }
                }
            }
        }

        // now we have to make it into something useable by ChartJS
        $dataSetIndex = 0;
        foreach ($rankTrackerData as $date => $data) {
            $chartJs['labels'][] = $dateFormatted = date('M d, Y', strtotime($date));

            // keywords
            foreach ($data['keywords'] as $keyword) {
                // check to see if we already created this dataset
                if (!isset($chartJs['datasets'][$keyword['id']])) {
                    // setup the keyword dataset
                    $chartJs['datasets'][$keyword['id']] = [
                        'label' => $keyword['keyword'],
                        'data' => array()
                    ];
                }

                // assign the specific data for this date and keyword
                $chartJs['datasets'][$keyword['id']]['data'][] = $keyword['position'];
            }
            // events
            foreach ($data['events'] as $event_id => $event) {
                // check to see if we already created the event dataset
                if (!isset($chartJs['events'][$event_id])) {
                    $eventDates = array_keys($rankTrackerData);

                    $events[$event_id] = [
                        'label' => $event['label'],
                        'data' => array(),
                    ];

                    foreach ($eventDates as $eventDate) {
                        $events[$event_id]['data'][$eventDate] = null;
                    }
                }

                // assign the specific data for this date and event
                $events[$event_id]['data'][$date] = -2;
                $chartJs['events'][$event_id]['label'] = $events[$event_id]['label'];

                foreach ($events[$event_id]['data'] as $key => $val) {
                    $chartJs['events'][$event_id]['data'][] = $val;
                }
            }
        }

        // turn everything into JSON
        if (!empty($chartJs['datasets'])) {
            $chartJs['labels'] = json_encode($chartJs['labels']);
            $chartJs['datasets'] = json_encode($chartJs['datasets']);
        }
        if (!empty($chartJs['events'])) {
            $chartJs['events'] = json_encode($chartJs['events']);
        } else {
            $chartJs['events'] = json_encode([]);
        }

//        $id = 36;
        $top3 = $top10 = $top20 = $top30 = $top100 = 0;
        $keywordPositionChartTableData = [3 => $top3, 10 => $top10, 20 => $top20, 30 => $top30, 100 => $top100];
        $keywordRank = [];
        $check = SCheck::where('idGroup', $group->idGroup)->orderBy('idCheck','desc')->take(2)->skip(0)->get();

        $utility = new App\Utilities\Utility();
        $visibilityScore = 0;
        $keywordsCount = 0;
        $groupKeywords = SKeywords::where('idGroup', $group->idGroup)->get();
        foreach ($check as $idcheck) {
            foreach ($groupKeywords as $groupKeyword) {
                // check for rank
                if (!isset($keywordRank[$groupKeyword->name])) {
                    $keywordsCount++;
                    $keywordRank[$groupKeyword->name] = ['change' => 0, 'newRank' => 'N/A', 'oldRank' => 0, 'bestRank' => 'N/A'];
                    $rank = SRank::where('idKeyword', $groupKeyword->idKeyword)
                        ->where('idCheck', $idcheck->idCheck)
                        ->first();
                    if (!empty($rank)) {
                        $keywordRank[$groupKeyword->name]['oldRank'] = $rank->position;
                        $keywordRank[$groupKeyword->name]['newRank'] = $rank->position;
                        $keywordRank[$groupKeyword->name]['bestRank'] = $rank->position;
                        $visibilityScore += $utility->getKeywordVisibilityPointByPosition($rank->position);
                    } else {
                        $keywordRank[$groupKeyword->name]['oldRank'] = 'N/A';
                    }

                } else {
                    $rank = SRank::where('idKeyword', $groupKeyword->idKeyword)
                        ->where('idCheck', $idcheck->idCheck)
                        ->first();
                    if (!empty($rank)) {
                        Log::info($groupKeyword->name);
                        Log::info($keywordRank[$groupKeyword->name]);
                        Log::info($rank);
                        $keywordRank[$groupKeyword->name]['newRank'] = $rank->position;
                        $keywordRank[$groupKeyword->name]['change'] = (int)$keywordRank[$groupKeyword->name]['oldRank'] - (int)$rank->position;
                    }

                    $bestRank = SRank::where('idKeyword', $groupKeyword->idKeyword)->min('position');
                    if (!empty($bestRank)) {
                        $keywordRank[$groupKeyword->name]['bestRank'] = $bestRank;
                    }
                }
            }
        }
        // check to see if we have any casper items
        $casperResults = new Collection;
        if ($rankChecker->casper()->count() > 0) {
            $casper = $rankChecker->casper->where('group_id', $rankChecker->group_id)->where('is_downloaded',
                true)->latest()->first();
            if ($casper) {
                $casperResults = $casper->results;
            }
        }

        if (!empty($keywordRank)) {
            foreach ($keywordRank as $name => $position) {
                foreach ($casperResults as $casperResult) {
                    if ($casperResult->keyword == $name) {
                        $casperResult->change = $position['change'];
                        $casperResult->newRank = $position['newRank'];
                        $casperResult->oldRank = $position['oldRank'];
                        $casperResult->bestRank = $position['bestRank'];
                        $keywordPositionChartTableData = $utility->setKeywordPosition($keywordPositionChartTableData, $position['newRank']);
                    }
                    foreach ($group->keywords as $keyword) {
                        if ($casperResult->keyword == $keyword->name) {
                            $casperResult->idKeyword = $keyword->idKeyword;
                            $casperResult->groupId = $rankChecker->group_id;
                        }
                    }
                }
            }
        }
        else {
            foreach ($group->keywords as $keyword) {
                foreach ($casperResults as $casperResult) {
                    if ($casperResult->keyword == $keyword->name) {
                        $casperResult->groupId = $rankChecker->group_id;
                        $casperResult->idKeyword = $keyword->idKeyword;
                        $casperResult->newRank = '-';
                        $casperResult->bestRank = '-';
                    }
                }
            }
        }

        $keywordPositionChartData = '[ ["Top 3", "Top 10", "Top 20", "Top 30", "Top 100"], [' . 
                                    $keywordPositionChartTableData[3] . ',' .
                                    $keywordPositionChartTableData[10] . ',' .
                                    $keywordPositionChartTableData[20] . ',' .
                                    $keywordPositionChartTableData[30] . ',' .
                                    $keywordPositionChartTableData[100] . '] ]';

        if ($visibilityScore != 0) {
            $visibilityPercentage = ($visibilityScore / ($keywordsCount * 30)) * 100;
        } else {
            $visibilityPercentage = 0;
        }

        $casperResultsSorted = $casperResults->sortBy('avg_month_searches')->values();

        if ( $request->ajax() ) {
            return Response::json([
                'chartJs' => $chartJs,
                'dates' => $userRequestDates,
                'casperResults' => $casperResultsSorted,
                'keywordPositionChartData' => $keywordPositionChartData,
                'visibilityScore' => $visibilityScore,
                'visibilityPercentage' => round($visibilityPercentage, 0)
            ], 200);
        }

        if ($request->has('downloadCSV')) {
            $csvData = [];

            foreach ($casperResultsSorted as $item) {
                $csvData[] = [
                    'project_name' => $group->name,
                    'keyword' => $item->keyword,
                    'avg_month_searches' => $item->avg_month_searches,
                    'keyword_cpc' => $item->suggested_bid,
                    'newRank' => $item->newRank,
                    'bestRank' => $item->change,
                    'domain' => $group->options['tld'],
                    'language' => $group->options['language'],
                ];
            }

            $file_name = (isset($csvData[0]['project_name']) && !empty($csvData[0]['project_name']) ? str_replace(" ", "_", $csvData[0]['project_name']).'.csv' : 'rank_tracker_project.csv');

            $csv = new ExportCsv($csvData, $file_name, false);
            $csv->send();
            die;
        }

        return view('backend.tools.ranktracker.view', [
            'projectId' => $id,
            'chartJs' => $chartJs,
            'group' => $group,
            'dates' => $userRequestDates,
            'casperResults' => $casperResultsSorted,
            'keywordPositionChartData' => $keywordPositionChartData,
            'keywordPositionChartTableData' => $keywordPositionChartTableData,
            'visibilityScore' => $visibilityScore,
            'visibilityPercentage' => round($visibilityPercentage, 0)
        ]);
    }

    /**
     * delete the specified group
     *
     * @param  integer $groupId [description]
     *
     * @return [type]          [description]
     */
    public function delete(Request $request)
    {
        $id = $request['id'];
        $rankChecker = RankCheck::where('group_id', intval($id))
            ->where('user_id', Auth::id())
            ->first();

        if (empty($rankChecker)) {
            return Redirect::route('backend.tools.ranktracker');
        }

        // delete the rankcheck itself
        $rankChecker->delete();

        // look up the group
        $group = SGroup::where('idGroup', intval($id))->first();
        if (empty($group)) {
            return Redirect::route('backend.tools.ranktracker');
        }

        $group->delete();
        
        return Response::json([
            'success' => true
        ], 200);

//        // tell user
//        Session::flash('success', array('The chosen group has been deleted'));
//
//        // return
//        return Redirect::route('backend.tools.ranktracker');
    }

    public function edit($id)
    {

        $rankChecker = RankCheck::where('group_id', intval($id))
            ->where('user_id', Auth::id())
            ->first();

        if (empty($rankChecker)) {
            return Redirect::route('backend.tools.ranktracker');
        }

        $group = SGroup::where('idGroup', intval($id))->first();

        if (empty($group)) {
            return Redirect::route('backend.tools.ranktracker');
        }

        // we need to fix the keywords themselfs
        $keywordString = '';
        foreach ($group->keywords as $keyword) {
            if (strlen($keywordString) > 0) {
                $keywordString .= "\r\n";
            }
            $keywordString .= $keyword->name;
        }
        $selectedEngine = '';
        $selectedLanguage = isset($group->options['language']) ? $group->options['language'] : 'all';

        // grab options
        $tldOptions = Config::get('searchengines');
        $languages = Config::get('languages');

        usort($tldOptions, function($a,$b) {
            if ($a['name'] == 'All Locations') {
                return -1;
            } else if ($b['name'] == 'All Locations') {
                return 1;
            }
            return strcmp($a['name'], $b['name']);
        });

        $data = $group->options;
        foreach ($tldOptions as $engine) {
            if ($engine['short_form'] == $data['location']) {
                $selectedEngine = $engine['name'];
                break;
            }
        }

        // return view
        return view('backend.tools.ranktracker.edit', [
            'group' => $group,
            'keywordString' => $keywordString,
            'selectedEngine'=> $selectedEngine,
            'selectedLanguage' => $selectedLanguage,
            'tldOptions' => json_encode($tldOptions),
            'languages' => json_encode($languages)
        ]);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function update(Request $request)
    {
        $rules = [
            'name' => 'required',
            'keywords' => 'required',
            'module' => 'required',
        ];

        $user = Auth::user();

        $userSearches = $roles = DB::connection('mysql_serposcope')->table('keyword as kwd')
            ->leftjoin(env('DB_DATABASE').'.rankchecker as r', 'r.group_id', '=', 'kwd.idGroup')
            ->whereBetween('r.created_at',
                [Carbon::now()->startOfDay(), Carbon::now()->endOfDay()])->where('kwd.user_id', Auth::id())->count();

        $groupKeyword = DB::connection('mysql_serposcope')->table('keyword as kwd')
            ->leftjoin(env('DB_DATABASE').'.rankchecker as r', 'r.group_id', '=', 'kwd.idGroup')
            ->where('r.group_id', $request->input('id'))
            ->where('kwd.user_id', Auth::id())->count();

        $keywords = explode("\r\n", $request->input('keywords'));

        $keyword = count($keywords);

        $userSearched = $keyword + $userSearches - $groupKeyword;
        if (!$user->hasRole('admin')) {
            if ($this->roleManagementController->checkTrialSubscription() == true) {
                return redirect()->back()->withInput($request->all())->withErrors([
                    'message' => getenv('TRIAL_PLAN_MESSAGE'),
                ]);
            }

            if ($userSearched > $this->roleManagementController->getCurrentUserLimit('tracking')) {
                return redirect()->back()->withInput($request->all())->withErrors([
                    'message' => 'Limit reached, Upgrade your plan to increase your limit.',
                ]);
            }
        }
        // validate input
        $this->validate($request, $rules);

        $rankChecker = RankCheck::where('group_id', intval($request->input('id')))
            ->where('user_id', Auth::id())
            ->first();

        if (empty($rankChecker)) {
            return Redirect::route('backend.tools.ranktracker');
        }

        $group = SGroup::where('idGroup', intval($request->input('id')))->first();

        if (empty($group)) {
            return Redirect::route('backend.tools.ranktracker');
        }

        $engines = Config::get('searchengines');
        foreach ($engines as $engine) {
            if ($engine['name'] == $request->input('tld')) {
                $tld = $engine['engine'];
                $location = $engine['short_form'];
                break;
            }
        }

        // save the group information
        $group->name = $request->input('name');
        $group->module = 'Google';
        $group->options = [
            'tld' =>  $tld,
            'location' => $location,
            'language' => $request->input('lang'),
            'datacenter' => $request->input('datacenter'),
            'local' => $request->input('local'),
            'parameters' => $request->input('parameters'),
            'user_id' => $user->id,
        ];
        $group->save();

        // Keywords
        $groupKeywords = $group->keywords->lists('name', 'idKeyword')->toArray();
        $keywordDelete = [];
        $keywordNew = [];

        // lets see what the user typed in
        $keywords = explode("\r\n", $request->input('keywords'));

        foreach (array_unique($keywords) as $word) {
            $word = strtolower(trim($word));
            $wordFound = false;

            // for each of our original keywords, lets search
            foreach ($groupKeywords as $index => $keyword) {
                if ($word == $keyword) {
                    // so we found the keyword
                    // we do not have to tell our software we are keeping it
                    // but we do need to tell our loop
                    $wordFound = true;
                    // also remove it from our original list, we will use the list at the end to decide what to remove
                    unset($groupKeywords[$index]);
                }
            }
            // if we didn't find the word, it means that it is one that we are adding
            if (!$wordFound) {
                $keywordNew[] = $word;
            }
        }

        // if we have keywords to add, lets add them
        if (count($keywordNew) > 0) {
            foreach ($keywordNew as $word) {
                SKeywords::create([
                    'idGroup' => $group->idGroup,
                    'name' => strtolower(trim($word)),
                    'user_id' => $user->id,
                ]);
            }
        }

        // if we have anything left in our original array, we need to remove them
        if (count($groupKeywords) > 0) {
            foreach ($groupKeywords as $id => $keyword) {
                $skeyword = SKeywords::findOrFail($id);
                $skeyword->delete();
            }
        }

//        $url = 'http://webcache.googleusercontent.com/search?q=cache:';
//        $content = $this->curlUrl($url,$request->input('domain'));


        // tell user
        Session::flash('success', array('The group has been updated'));

        // return
        // return Redirect::route('backend.tools.ranktracker');
        return $this->casperGenerator($group->idGroup);
        
    }

    public function download($id)
    {
        // First lets see if we have any dates
        $userDates = [
            'start' => Session::get('rt_date_start', date('Y-m-') . '01'),
            'stop' => Session::get('rt_date_stop', date('Y-m-t')),
        ];

        $rankChecker = RankCheck::where('group_id', intval($id))
            ->where('user_id', Auth::id())
            ->first();

        if (empty($rankChecker)) {
            return Redirect::route('backend.tools.ranktracker');
        }

        $group = SGroup::where('idGroup', intval($id))->first();

        if (empty($group)) {
            return Redirect::route('backend.tools.ranktracker');
        }

        $keywords = array();
        $ranktrackerData = array();
        $chartJs = array(
            'legend' => array(),
        ); // will be converted into JSON

        // fill up our keywords to store the information
        foreach ($group->keywords->lists('name', 'idKeyword')->toArray() as $id => $keyword) {
            $keywords[$id] = [
                'id' => $id,
                'keyword' => $keyword,
                'position' => null,
                'url' => null,
            ];
        }

        // fill up our array with the dates we need to check for
        $numberOfDays = abs(strtotime($userDates['start']) - strtotime($userDates['stop']));
        $numberOfDays = floor($numberOfDays / (60 * 60 * 24));

        for ($day = 0; $day <= $numberOfDays; $day++) {
            $currentDate = date('Ymd', strtotime('+' . $day . ' days', strtotime($userDates['start'])));

            $ranktrackerData[$currentDate] = array(
                'hasData' => false,
                'keywords' => $keywords,
                'events' => array(),
            );
        }

        // now going through each date
        foreach ($ranktrackerData as $date => $data) {
            $dateFormatted = date('Y-m-d', strtotime($date));

            // do we have any events on this day
            if ($group->target->events()->where('date', $dateFormatted)->count() > 0) {
                $ranktrackerData[$date]['hasData'] = true;

                foreach ($group->target->events()->where('date', $dateFormatted)->get() as $event) {
                    $ranktrackerData[$date]['events'][uniqid()] = [
                        'position' => -2,
                        'label' => 'Event: ' . $event->event
                    ];
                }
            }

            // do we have any keywords on this day
            $keywordScan = $group->lastRan()->whereBetween('date',
                [$dateFormatted . ' 00:00:00', $dateFormatted . ' 23:59:59']);
            if ($keywordScan->count() > 0) {
                $ranktrackerData[$date]['hasData'] = true;

                // our scan has idCheck
                // we can use this to find the position (SRANK)
                foreach ($keywordScan->get() as $scan) {
                    foreach ($keywords as $keyword) {
                        // check for rank
                        $rank = SRank::where('idKeyword', $keyword['id'])
                            ->where('idCheck', $scan->idCheck)
                            ->first();

                        // did this keyword have a rank?
                        if ($rank) {
                            $ranktrackerData[$date]['keywords'][$keyword['id']]['position'] = $rank->position;
                            $ranktrackerData[$date]['keywords'][$keyword['id']]['url'] = $rank->url;
                        }
                    }
                }
            }
        }

        $csvData = array();

        foreach ($ranktrackerData as $date => $data) {
            foreach ($data['keywords'] as $keyword) {
                $csvData[] = [
                    'date' => $date,
                    'keyword' => $keyword['keyword'],
                    'position' => $keyword['position'],
                    'url' => $keyword['url'],
                ];
            }

            foreach ($data['events'] as $event) {
                $csvData[] = [
                    'date' => $date,
                    'keyword' => $event['label'],
                    'position' => null,
                    'url' => null,
                ];
            }
        }

        $filename = 'export_KeywordRevealer_' . date('Y-m-d', strtotime($userDates['start'])) . '_' . date('Y-m-d',
                strtotime($userDates['stop'])) . '.csv';
        $csv = new ExportCsv($csvData, $filename);
        $csv->send();
    }

    /**
     * Inserts an event for a given target
     * @return [type] [description]
     */
    public function eventInsert(Request $request)
    {
        $rules = [
            'date' => 'required|date',
            'description' => 'required'
        ];

        $this->validate($request, $rules);

        $target = STarget::where('idTarget', $request->input('target_id'))->first();

        if (!$target) {
            return Redirect::route('backend.tools.ranktracker');
        }

        // ensure we own this target
        $group = $target->group;
        $rankTracker = RankCheck::where('group_id', $group->idGroup)->where('user_id', Auth::id())->first();
        if (!$rankTracker) {
            return Redirect::route('backend.tools.ranktracker');
        }

        // if we are here we own everything above, so insert
        SEvent::create([
            'idTarget' => $target->idTarget,
            'date' => date('Y-m-d', strtotime($request->input('date'))),
            'event' => $request->input('description')
        ]);

        // tell user
        Session::flash('success', array('The event has been added'));

        return Redirect::route('backend.tools.ranktracker.view', $group->idGroup);
    }

    /**
     * used to set the session date
     *
     * @param string $dateStart start date
     * @param string $dateStop stop date
     */
    public function setDate(Request $request)
    {
        if (!$request->has('id')) {
            return Redirect::route('backend.tools.rankchecker');
        }

        Session::put('rt_date_start', $request->input('start_date'));
        Session::put('rt_date_stop', $request->input('stop_date'));

        return Redirect::route('backend.tools.ranktracker.view', $request->input('id'));
    }

    public function casperGenerator($groupId)
    {
        // Casper CPC
        $Casper = new CasperCPC(Auth::id());
        // Rank Tracker
        $rankChecker = RankCheck::where('group_id', intval($groupId))
            ->where('user_id', Auth::id())
            ->first();

        // The group from Laravel
        $group = SGroup::where('idGroup', intval($groupId))->first();
        

        // ensure that everything is right
        if (empty($rankChecker) || empty($group)) {
            return;
        }

        // Send the keywords
        $Casper->setKeywords($group->keywords->lists('name')->toArray());

        $language = isset($group->options['language']) ? $group->options['language'] : 'all';
        $location = isset($group->options['location']) ? $group->options['location'] : 'all';
        $Casper->setLanguage($language);
        $Casper->setLocation($location);

        // set the tracker id
        $Casper->setRanktrackerId(intval($groupId));

        // tell our class to generate the casper files
        $Casper->generateKeywords();

        // tell our class to send the casper file to the API
        $results = $Casper->sendAPIRequest();

        // debug
        //$Casper->setCasper(9);

        // process the file itself
//        $results = $Casper->processFile();

        // send back results as json

        // but for now, just redirect
        return Redirect::route('backend.tools.ranktracker.view', $groupId);
    }

    public function bulkDelete(Request $request)
    {
        try {
            DB::beginTransaction();
            foreach ($request['mass_ids'] as $rankCheck) {

                $rank_check = RankCheck::whereGroupId($rankCheck);
                SGroup::where('IdGroup', $rankCheck)->delete();
                STarget::where('IdGroup', $rankCheck)->delete();
                SKeywords::where('IdGroup', $rankCheck)->delete();
                if (!empty($rankCheck)) {
                    $rank_check->delete();
                }
            }
            DB::commit();
            return Response::json([
                'success' => true
            ], 200);
        } catch (QueryException $e) {
            DB::rollBack();
            throw new ApiOperationFailedException($e->getMessage());
        }
    }


}
