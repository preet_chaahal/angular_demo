<?php
namespace App\Http\Controllers\Backend\Tools;

use App;
use App\Brainstorm;
use App\Exceptions\ApiOperationFailedException;
use App\Http\Controllers\Admin\RoleManagementController;
use App\Http\Controllers\BaseController;
use App\Http\Requests;
use App\Utilities\StringValidator;
use Auth;
use Carbon\Carbon;
use Config;
use DB;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use RecursiveArrayIterator;
use RecursiveIteratorIterator;
use Redirect;
use Response;
use Session;
use GuzzleHttp\Client;
use App\Utilities\ExportCsv;

// Only do tree view for now

class BrainstormController extends BaseController
{
    public $roleManagementController;

    /**
     * BrainstormController constructor.
     */
    public function __construct()
    {
        // Must be signed in to access anything in here
        $this->middleware('auth');
        $this->roleManagementController = App::make(RoleManagementController::class);
    }

    public function index()
    {
		$user = Auth::user();
        $brainstorms = Auth::user()->brainstorms;
		$languages = Config::get('languages');
		$tldOptions = Config::get('searchengines');
      // this is going to be used as a default
      $selectedEngine = '';

      usort($tldOptions, function ($a, $b) {
          if ($a['name'] == 'All Locations') {
              return -1;
          } else if ($b['name'] == 'All Locations') {
              return 1;
          }
          return strcmp($a['name'], $b['name']);
      });

      if ($user->default_location) {
          foreach ($tldOptions as $engine) {
              if ($engine['short_form'] == $user->default_location) {
                  $selectedEngine = $engine['name'];
              }
          }
      } else {
          foreach ($tldOptions as $engine) {
              if ($engine['engine'] == $user->defaultSearchEngine) {
                  $selectedEngine = $engine['name'];
              }
          }
      }

		return view('backend.tools.brainstorm.index', [
			'brainstorms' => $brainstorms,
            'keyword' =>  \request('keyword') ? \request('keyword') : '',			
			'tldOptions' => json_encode($tldOptions),
			'languages' => json_encode($languages),
			'selectedEngine' => $selectedEngine,
			'selectedLanguage' => $user->defaultSearchLanguage
		]);
    }

    public function add()
    {
        return view('backend.tools.brainstorm.add');
    }

    public function apiData(Request $request)
    {
        $currentAPI = env('CURRENT_ACTIVE_API');
        $id = $request->get('id');
        $arrWithGaps = array_unique( explode(",", $id) );
        $arr = explode(",", implode(",", $arrWithGaps));

        if ($currentAPI == 1) {
            $params = array(
                'apikey' => env('API_KEY'),
                'keyword' => json_encode($arr),
                'metrics_network' => 'googlesearch',
                'metrics' => 'true',
                'metrics_location' => '',
                'metrics_language' => '',
                'metrics_currency' => 'USD',
                'output' => 'json',
            );

            $url = \Config::get('casper.scenario2_two_url');

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url . http_build_query($params));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

            try {
                $content = curl_exec($ch);
            }
            catch(Exception $e){
                \Session::flash('nosuccess', $e->getMessage());
                return redirect()->back();
            }
        }
        else {
            $params = array(
                'key' => env('API_KEY2'),
            );

            $url = \Config::get('casper.scenario3_two_url');
            $client = new Client();
            
            try {
                $response = $client->post( $url . http_build_query($params), [
                    'json' => [
                        "keywords" => $arr,
                        "language" => Auth::user()->defaultSearchLanguage,
                        "country" => Auth::user()->default_location
                    ]
                ]);
            }
            catch(Exception $e){
                \Session::flash('nosuccess', $e->getMessage());
                return $this->respondWithData(['error' => $e->getMessage()]);
                // return redirect()->back();
            }
            $content = $response->getBody()->getContents();
        }

        $response = json_decode($content);

        if (isset($response->error) && !empty($response->error)) {
            throw new ApiOperationFailedException($response->error);
        }

        return $this->respondWithData( $this->transformDataFromApi3($response, $arrWithGaps) );
    }

    public function transformDataFromApi2($data)
    {
        $dataExt = [];
        foreach ($data->results as $value) {
            $object = (object) [
                'name' => $value->string,
                'avg' => (is_numeric($value->volume) ? $value->volume : '-'),
                'bid' => (is_numeric($value->cpc) ? $value->cpc : '-')
            ];
            array_push($dataExt, $object);
        }
        return json_encode($dataExt);
    }

    public function transformDataFromApi3($data, $initArr)
    {
        $dataExt = [];
        foreach ($data->stats as $value) {
            if ( is_numeric($value->keyword) ) {
                $name = $initArr[$value->keyword];
            }
            else {
                $name = $value->keyword;
            }
            $object = (object) [
                'name' => $name,
                'avg' => (is_numeric($value->volume) ? $value->volume : '-'),
                'bid' => (is_numeric($value->cpc) ? round($value->cpc / 1000000, 2) : '-')
            ];
            array_push($dataExt, $object);
        }
        return json_encode($dataExt);
    }

    public function search(Request $request)
    {
        $user = Auth::user();
        $userSearched = $roles = Brainstorm::where('user_id', Auth::id())
            ->whereBetween('created_at',
                [Carbon::now()->startOfDay(), Carbon::now()->endOfDay()])->count();
       $brainstorm=Brainstorm::where('keyword',$request->get('keyword'))->whereUserId(Auth::id())->first();

        if(!empty($brainstorm)){
            return redirect()->back()->withInput($request->all())->withErrors([
                'message' => 'This Keyword already Brainstorming',
            ]);
        }
        if (!$user->hasRole('admin')) {
            //validate trial account subscription
            if ($this->roleManagementController->checkTrialSubscription() == true) {
                return redirect()->back()->withInput($request->all())->withErrors([
                    'message' => getenv('TRIAL_PLAN_MESSAGE'),
                ]);
            }

            if ($userSearched >= $this->roleManagementController->getCurrentUserLimit('brainstorm')) {
                return redirect()->back()->withInput($request->all())->withErrors([
                    'message' => 'Keyword brainstorming limit reached, Upgrade your plan to increase your limit.',
                ]);
            }
        }

        $keyword = StringValidator::validate($request->input('keyword'));
        $lang 	 = StringValidator::validate($request->input('lang'));
        $location= StringValidator::validate($request->input('tld'));
        
        Input::replace(array_merge($request->all(), array('keyword' => $keyword)));
        $rules = [
            'keyword' => 'required|max:80',
        ];

        $this->validate($request, $rules);

        $selectedEngine = array(
            'name' => 'United States',
            'engine' => 'google.com',
            'short_form' => 'us'
        );

        // generate the suggestions
        $d3ChartData = $this->getBrainstormData(str_replace("'","",$keyword),$lang,$location);
        $key = new App\TblBrainstormStatus();
        $key->keyword = $keyword;
        $key->user_current_level = $user->account_id;
        $key->google_engine = Auth::user()->defaultSearchEngine;
        $key->user_id = Auth::id();
        $key->save();


        $json_array = array();
        $hasData = false;
        if ($d3ChartData) {
            $hasData = true;
            $json_array = unserialize(base64_decode($d3ChartData));
            $json_array['children'] = $this->removeFirstNode($json_array);
        }
        return view('backend.tools.brainstorm.view-tree', [
            'hasData' => $hasData,
            'brainstorm' => '',
            'keyword' => $keyword,
            'cpc_data' => '',
            'keywords' => json_encode($json_array),
            'location'=>$this->getLocation(Auth::user()->defaultSearchEngine, null),
            'language'=>$this->getLanguage()
        ]);
    }

    public function getLocation($inputEngine = null, $inputName = null){
        $engines = Config::get('searchengines');
        /** @var string $location*/
        foreach ($engines as $engine){
            if (!is_null($inputEngine)) {
                if ($engine['engine'] == $inputEngine) {
                    $location = $engine['name'];
                    break;
                }
            } elseif (!is_null($inputName)) {
                if ($engine['name'] == $inputName) {
                    $location = $engine['engine'];
                    break;
                }
            } else {
                $location = 'all';
            }
        }
        return $location;
    }

    public function getLanguage($inputLanguage = null){
        $languages = Config::get('languages');
        foreach ($languages as $language) {
            if (!is_null($inputLanguage)) {
                if ($language['code'] == $inputLanguage){
                    $lang = $inputLanguage;
                    break;
                }
            } else {
                $lang ='all';
            }
        }
        return $lang;
    }

    public function save(Request $request)
    {
        try {
            $input = $request->all();

            $keyword = StringValidator::validate($request->input('keyword'));
            Input::replace(array_merge($input, array('keyword' => $keyword)));
            $rules = [
                'keyword' => 'required',
                'data' => 'required',
            ];

            $this->validate($request, $rules);
            //keyword
            $jsonData1 = $input['data'];
            $jsonData = json_decode($jsonData1);
            $serializedData = serialize($jsonData);
            $data = base64_encode($serializedData);
            //cpc data
            $jsonCpcData1 = $input['cpc_data'];
            $cpcData='';
            if(!empty($jsonCpcData1)){
                $cpcData = base64_encode($jsonCpcData1);
            }
            $input['data'] = $data;
            $input['cpc_data']=$cpcData;
            $input['user_id'] = Auth::id();
            $input['keyword_count'] = $input['keywordCount'];

            if (!isset($input['id']) || empty($input['id'])) {

                $research = Brainstorm::whereKeyword($request->get('keyword'))->whereUserId(Auth::id())->first();
                if (!empty($research)) {
                    return $this->respondWithError('This Keyword already Brainstorming.');
                }

                $brainstorm = Brainstorm::create($input);
            } else {
                $brainstorm = Brainstorm::find($input['id']);

                $brainstorm->fill($input);
                $brainstorm->save();
            }

            return $this->respondWithData($brainstorm);
        } catch (\Exception $e) {
            return $this->respondWithError($e->getMessage());
        }
    }

    public function storeCpcData(Request $request)
    {
        $jsonCpcData1 = $request->get('cpc_data');
        $cpcData = '';
        if (!empty($jsonCpcData1)) {
            $cpcData = base64_encode($jsonCpcData1);
        }
        $researchCpcData = $request['cpc_data'] = $cpcData;
        Brainstorm::whereId($request->get('id'))->update(['cpc_data' => $researchCpcData]);
    }

    public function view($brainstormId)
    {

        $brainstorm = Brainstorm::where('id', $brainstormId)->where('user_id', Auth::id())->first();
        if(empty($brainstorm)){
            return redirect()->back()->withErrors([
                'message' => 'Record not found',
            ]);
        }
        $json_array = array();
        $hasData = false;
        $cpcData = '';
        // if we have any data, lets decode it
        if (!empty($brainstorm->data)) {
            $hasData = true;
            $json_array = unserialize(base64_decode($brainstorm->data));
//            $json_array['children'] = $this->removeFirstNode($json_array);
        }
        // Remove first child node which is duplicate of parents
        if (!empty($brainstorm->cpc_data)) {
            $hasData = true;
            $jsonCpcArray = base64_decode($brainstorm->cpc_data);

            $cpcData = str_replace('\u2013', '-', $jsonCpcArray);
        }

        return view('backend.tools.brainstorm.view-tree', [
            'hasData' => $hasData,
            'brainstorm' => $brainstorm,
            'keyword' => $brainstorm->keyword,
            'cpc_data' => $cpcData,
            'keywords' => json_encode($json_array),
            'location'=>$this->getLocation(Auth::user()->defaultSearchEngine, null),
            'language'=>$this->getLanguage()
        ]);
    }

    public function downloadSavedResearch($brainstormId)
    {
        $brainstorm = Brainstorm::where('id', $brainstormId)->where('user_id', Auth::id())->first();
        if(empty($brainstorm)){
            return redirect()->back()->withErrors([
                'message' => 'Record not found',
            ]);
        }
        $cpcData = '';

        // Remove first child node which is duplicate of parents
        if (!empty($brainstorm->cpc_data)) {
            $jsonCpcArray = base64_decode($brainstorm->cpc_data);
            $cpcData = str_replace('\u2013', '-', $jsonCpcArray);
        }

        $cpcData = json_decode($cpcData);

        foreach ($cpcData as $item) {
            $csvData[] = [
                'Keyword' => $item->name,
                'AVG' => $item->avg,
                'CPC' => $item->bid,
            ];
        }
        $file_name = (isset($brainstorm->keyword) && !empty($brainstorm->keyword) ? str_replace(" ", "_", $brainstorm->keyword).'.csv' : 'keyword_brainstorming.csv');
        $csv = new ExportCsv($csvData, $file_name, false);
        //$csv = new ExportCsv($csvData, 'keyword_brainstorming.csv', false);
        $csv->send();
        die;
    }

    private function removeFirstNode($node)
    {

        if (isset($node['children']) && !empty($node['children'])) {
            $name = $node['name'];
            $index = 0;
            foreach ($node['children'] as $child) {
                if (!empty($child['children'])) {
                    $node['children'][$index]['children'] = $this->removeFirstNode($child);
                }
                $index++;
            }
            if ($node['children'][0]['name'] == $name) {
                 $node['children'] = array_splice($node['children'], 1);
            }
        }

        return $node['children'];
    }

    /**
     * this is used when the user wants to refresh their options
     * @param $brainstormId
     * @return \Illuminate\Http\RedirectResponse [type]               [description]
     * @internal param $ [type] $brainstormId [description]
     */
    public function refresh($brainstormId)
    {
        $brainstorm = \App\Brainstorm::where('id', $brainstormId)->where('user_id', Auth::id())->first();

        if (empty($brainstorm)) {
            return Redirect::route('backend.tools.brainstorm');
        }

        // generate the suggestions
        $this->getSuggestions($brainstorm->id);

        // forward the user on the the right route
        return Redirect::route('backend.tools.brainstorm.view', $brainstorm->id);
    }

    private function getBrainstormData($keyword,$lang,$location)
    {
        // how many levels can we go? (subgroups)
        $depth = 3;
        // our suggestion class
        $suggestions = new \App\Utilities\Suggestions;

        // let's grab some real data
        $keywords = $suggestions->scrape($keyword, $depth,true,$lang,$location);
        if ($keywords == false) {
            return false;
        }
        //echo "<pre>"; print_r($keywords); exit();
        $recur_flat_arr_obj = new RecursiveIteratorIterator(new RecursiveArrayIterator($keywords));
         $keywords = $recur_flat_arr_obj->getArrayCopy();
        $json_array = array();

        $d3ChartData = '';
        // ensure we have a proper array to convert into D3 JSON
        if (count($keywords) > 0) {
            // tell the view we have data
            if (count($keywords) > 0) {
                // convert our array into a D3 format
                $json_array = array(
                    'name' => str_replace("'", "", $keyword),
//                    'name' => stripslashes($keyword),
                    'children' => $suggestions->toD3JSON($keywords),
                );

                // let's save these into the database with some safe serialization
               $d3ChartData = base64_encode(serialize($json_array));
                // how many groups did we have?
                // how many keywords did we have?

            }
            // else we have some error, mostlikely it is the proxies
        }
        // we are done here
        return $d3ChartData;

    }

    public function getSuggestions($brainstormId)
    {
        $brainstorm = \App\Brainstorm::where('id', $brainstormId)->where('user_id', Auth::id())->first();

        // how many levels can we go? (subgroups)
        $depth = 3;
        // our suggestion class
        $suggestions = new \App\Utilities\Suggestions;

        // ensure we have a valid brainstorm item
        if (!$brainstorm) {
            return;
        }

        // let's grab some real data
        $keywords = $suggestions->scrape($brainstorm->keyword, $depth);
        if ($keywords == false) {
            return false;
        }
        $recur_flat_arr_obj = new RecursiveIteratorIterator(new RecursiveArrayIterator($keywords));
        $keywords = $recur_flat_arr_obj->getArrayCopy();
        $json_array = array();

        // ensure we have a proper array to convert into D3 JSON
        if (count($keywords) > 0) {
            // tell the view we have data
            if (count($keywords) > 0) {
                // convert our array into a D3 format
                $json_array = array(
                    'name' => $brainstorm->keyword,
                    'children' => $suggestions->toD3JSON($keywords),
                );

                // let's save these into the database with some safe serialization
                $brainstorm->data = base64_encode(serialize($json_array));
                // how many groups did we have?
                // how many keywords did we have?

                $brainstorm->save();
            }
            // else we have some error, mostlikely it is the proxies
        }

        // we are done here
        return;
    }

    public function delete(Request $request)
    {
        $brainstormId = $request['id'];
        $brainstorm = \App\Brainstorm::where('id', $brainstormId)->where('user_id', Auth::id())->first();

        if (empty($brainstorm)) {
            return Redirect::route('backend.tools.brainstorm');
        }

        // delete the rankcheck itself
        $brainstorm->delete();

        return Response::json([
            'success' => true
        ], 200);
    }

    public function bulkDelete(Request $request)
    {
        try {
            DB::beginTransaction();
            foreach ($request['mass_ids'] as $brainstorm) {
                $brainstorm_data = Brainstorm::find($brainstorm);
                if (!empty($brainstorm)) {
                    $brainstorm_data->delete();
                }
            }
            DB::commit();
            return Response::json([
                'success' => true
            ], 200);
        } catch (QueryException $e) {
            DB::rollBack();
            throw new ApiOperationFailedException($e->getMessage());
        }
    }

}
