<?php

namespace App\Http\Controllers\Backend\Tools;

use App;
use App\EvaluationsUrl;
use App\Exceptions\ApiOperationFailedException;
use App\Http\Controllers\Admin\RoleManagementController;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Research;
use App\ResearchKeyword;
use App\ResearchTmp;
use App\User;
use App\Utilities\Domain;
use App\Utilities\ExportCsv;
use App\Utilities\Google;
use App\Utilities\Social;
use App\Utilities\StringValidator;
use App\Utilities\Namecheap;
use App\UnsavedResearch;
use App\UnsavedEvaluationsUrl;
use Auth;
use Carbon\Carbon;
use Config;
use DB;
use Doctrine\DBAL\Query\QueryException;
use GuzzleHttp\Exception\BadResponseException;
use Illuminate\Http\Request;
use Input;
use Log;
use PhpSpec\Exception\Exception;
use Redirect;
use Response;
use Session;
use GuzzleHttp\Client;
use App\Services\ApiLoggerService;

/**
 * Class KeywordResearchController
 * @package App\Http\Controllers\Backend\Tools
 */
class KeywordResearchController extends Controller
{

    /**
     * @var bool
     */
    var $debug;

    /** @var RoleManagementController $roleManagementController */
    public $roleManagementController;

    /**
     * KeywordResearchController constructor.
     */
    public function __construct()
    {
        // Must be signed in to access anything in here
        $this->middleware('auth');
        $this->roleManagementController = App::make(RoleManagementController::class);
        $this->debug = false;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {

      /** @var User $user */
      $user = Auth::user();

      /**
       * grab all search engines
       */
      $tldOptions = Config::get('searchengines');
      $languages = Config::get('languages');

      // this is going to be used as a default
      $selectedEngine = '';

      usort($tldOptions, function ($a, $b) {
          if ($a['name'] == 'All Locations') {
              return -1;
          } else if ($b['name'] == 'All Locations') {
              return 1;
          }
          return strcmp($a['name'], $b['name']);
      });

      if ($user->default_location) {
          foreach ($tldOptions as $engine) {
              if ($engine['short_form'] == $user->default_location) {
                  $selectedEngine = $engine['name'];
              }
          }
      } else {
          foreach ($tldOptions as $engine) {
              if ($engine['engine'] == $user->defaultSearchEngine) {
                  $selectedEngine = $engine['name'];
              }
          }
      }

      $searchedCount = App\TblKeywordSearchStatus::where('user_id', Auth::id())
          ->whereBetween('created_at',
              [Carbon::now()->startOfDay(), Carbon::now()->endOfDay()])
          ->groupBy('keyword')
          ->get();

      // $invoices = $user->invoices();
      // dd($invoices);
      // Include pending invoices in the results...
      // $invoices2 = $user->invoicesIncludingPending();

      // echo "<pre>"; print_r($searchedCount); echo "</pre>"; die();

        \Illuminate\Support\Facades\Session::forget('loginAttemptTime');
        \Illuminate\Support\Facades\Session::forget('loginAttempts');

      return view('backend.tools.keywordresearch.index', [
          'searchedCount' => $searchedCount,
          'keywords' =>  Auth::user()->research,
          'keyword' =>  \request('keyword') ? \request('keyword') : '',
          'keyword_status' => Auth::user()->keywordStatus,
          'tldOptions' => json_encode($tldOptions),
          'languages' => json_encode($languages),
          'selectedEngine' => $selectedEngine,
          'selectedLanguage' => $user->defaultSearchLanguage
      ]);
    }
    /* Angular methods */
    public function indexData(){

      $user = Auth::user();
      /**
       * grab all search engines
       */
      $tldOptions = Config::get('searchengines');
      $languages = Config::get('languages');

      // this is going to be used as a default
      $selectedEngine = '';

      usort($tldOptions, function ($a, $b) {
          if ($a['name'] == 'All Locations') {
              return -1;
          } else if ($b['name'] == 'All Locations') {
              return 1;
          }
          return strcmp($a['name'], $b['name']);
      });

      if ($user->default_location) {
          foreach ($tldOptions as $engine) {
              if ($engine['short_form'] == $user->default_location) {
                  $selectedEngine = $engine['name'];
              }
          }
      } else {
          foreach ($tldOptions as $engine) {
              if ($engine['engine'] == $user->defaultSearchEngine) {
                  $selectedEngine = $engine['name'];
              }
          }
      }

      return Response::json([
        'searchedToday'         => $this->searchedToday(),
        'savedSearches'         => $this->getSavedKeywords(),
        'languages'             => $languages,
        'tldOptions'            => $tldOptions,
        'selectedEngine'        => $selectedEngine,
        'selectedLanguage'      => $user->defaultSearchLanguage,
        'userCanImportKeywords' => $user->can('import_keywords'),
        'userCanFindDomain'     => $user->can('domain_finder'),
        'userCanSaveKeyword'    => $user->can('keyword_save'),
        'keyword' => \request('keyword') ? \request('keyword') : '',
      ], 200);
    }

    public function searchedToday(){
      return App\TblKeywordSearchStatus::where('user_id', Auth::id())
          ->whereBetween('created_at',
              [Carbon::now()->startOfDay(), Carbon::now()->endOfDay()])
          ->groupBy('keyword')
          ->get();
    }

    /*
    * handle saved keywords to make compatibale with angular
    */
    public function getSavedKeywords(){
      $savedSearches = Auth::user()->researchKeyword()->with('research')->get();

      $modifiedSavedSearches = [];
      $index = 0;
      foreach ($savedSearches as $key => $value) {
        $value['tmp_id'] = $value['research_tmp_id'];
        $value['index'] = $index;
        $value['child'] = false;
        $bestDifficulity = 0;
        $modifiedSavedSearches[] = $value;
        foreach ($value['research'] as $x => $y) {
          $y['firstChild'] = $x == 0 ? true : false;
          $y['child']   = true;
          $y['childOf'] = $index;
          if($y['difficulty'] > $bestDifficulity){$bestDifficulity=$y['difficulty'];}
          $modifiedSavedSearches[] = $y;
          $index++;
        }
        $modifiedSavedSearches[ ((count($modifiedSavedSearches)-count($value['research']))-1) ]['bestDifficulity'] = $bestDifficulity;
        $index++;
      }
      return $modifiedSavedSearches;
    }

    public function deleteResearch($id)
    {
        ResearchKeyword::find($id)->delete();
        $data = $this->index();
        return $data;
    }

    public function deleteEvaluate($id)
    {
        Research::find($id)->delete();
        $data = $this->index();
        return $data;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add()
    {
        // store the user
        $user = Auth::user();
        // grab all search engines
        $tmp = Config::get('searchengines');
        $tldOptions = array();

        // this is going to be used as a default
        $selectedEngine = '';

        foreach ($tmp as $engine) {
            $tldOptions[] = ['name' => $engine['name'] . ' (' . $engine['engine'] . ')', 'engine' => $engine['name']];

            if ($engine['engine'] == $user->defaultSearchEngine) {
                $selectedEngine = $engine['name'];
                break;
            } else {
                $selectedEngine = 'United States';
            }
        }

        return view('backend.tools.keywordresearch.add', [
            'tldOptions' => json_encode($tldOptions),
            'selectedEngine' => $selectedEngine
        ]);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function importkeyword(Request $request)
    {
        $request = request();
        $apiLogger = app(ApiLoggerService::class);
        /* start the transaction */
        DB::beginTransaction();

        try {      
        $currentAPI = env('CURRENT_ACTIVE_API');
        // tmp id used to hold the tmp search
        $user = Auth::user();
        $tmp_id = 0;
        $keywords = $request->input('keyword');
        $keywordsArr = explode("\n", $keywords);
        $arr = array();
        $project_name = '';

        $userSearched = App\TblKeywordStatus::where('user_id', Auth::id())
              ->whereBetween('created_at',
                  [Carbon::now()->startOfDay(), Carbon::now()->endOfDay()])->count();
          if (!$user->hasRole('admin')) {
              if ($this->roleManagementController->checkTrialSubscription() == true) {
                  return Response::json(getenv('TRIAL_PLAN_MESSAGE'), 422);
              }

              if ($userSearched >= $this->roleManagementController->getCurrentUserLimit('research')) {
                  return Response::json('Keyword research limit reached, Upgrade your plan to increase your limit.', 422);
              }
          }

        $keywordd = str_replace("'","",StringValidator::validate($request->input('keyword')));

        Input::replace(array_merge($request->all(), array('keyword' => $keywordd)));
        $this->validate($request, [
            'keyword' => 'required',
            'tld' => 'required',
            'lang' => 'required',
        ]);

        $keywordsArr = str_replace("'","",StringValidator::validate($keywordsArr));

        Input::replace(array_merge($request->all(), array('keyword' => $keywordsArr)));

        for ($i = 0; $i < count($keywordsArr); $i++) {
            $keyword = str_replace("'","",trim($keywordsArr[$i]));
            $arr[] = $keyword;

            if (!$project_name) {
                $isResearchExist = ResearchKeyword::where('keyword', $keyword)->whereUserId(Auth::id())->exists();
                if (!$isResearchExist) {
                    $project_name = $keyword;
                }
            }
        }
        if (!$project_name) {
            return Response::json('All the words that you are importing already been saved.', 422);
        }
        $arr = array_unique($arr);
        if (count($arr) > 700) {
          return Response::json("Sorry, you can't import more than 700 keyword.", 422);
        }

        // create some rules
        $this->validate($request, [
            'keyword' => 'required',
            'tld' => 'required',
            'lang' => 'required',
        ]);

        $selectedEngine = array(
            'name' => 'United States',
            'engine' => 'google.com',
            'short_form' => 'us'
        );
        // filled with just default information
        $engines = Config::get('searchengines');
        $country = 'all';
        $language = 'all';
        $location = $request->input('tld');

        foreach ($engines as $engine) {
            if ($engine['name'] == $location) {
                $selectedEngine = $engine;
                $country = $engine['short_form'];
                break;
            }
        }

        $language = $request->input('lang');

        if ($currentAPI == 1) {
            $params = array(
                'apikey' => env('API_KEY'),
                'keyword' => json_encode($arr),
                'metrics_network' => 'googlesearch',
                'metrics' => 'true',
                'metrics_location' => '',
                'metrics_language' => '',
                'metrics_currency' => 'USD',
                'output' => 'json',
            );

            $url = \Config::get('casper.scenario2_two_url');

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url . http_build_query($params));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);


            try {
                $content = curl_exec($ch);
            } catch (Exception $e) {
                return Response::json($e->getMessage(), 422);
            }

            $data = json_decode($content, TRUE);

            if (!empty($data['error'])) {
                return Response::json('API error! ' . $data['error']['message'], 422);
            }

            $data = $data['results'];
        } else {
            $params = array(
                'key' => env('API_KEY2'),
            );

            $url = \Config::get('casper.scenario3_two_url');
            $client = new Client();

            try {
                $response = $client->post($url . http_build_query($params), [
                    'json' => [
                        "keywords" => $keywordsArr,
                        "language" => $language,
                        "country" => $country
                    ]
                ]);
            } catch (Exception $e) {
                return Response::json($e->getMessage(), 422);
            }
            $data = json_decode($response->getBody()->getContents(), TRUE);

            $data = $data['stats'];
        }

        // $data = Config::get('mocked_api_data_2');
        $researchData = [];

        if (!empty($data)) {
            $i = 0;
            foreach ($data as $value) {
                if ($i == 0) {
                    $key = new App\TblKeywordStatus();
                    $key->keyword = $project_name;
                    $key->user_current_level = $user->account_id;
                    $key->google_engine = $selectedEngine['engine'];
                    $key->user_id = Auth::id();
                    $key->location = $location;
                    $key->save();
                }

                if ($currentAPI == 1) {
                    $researchData[] = [
                        'keyword_id' => $key->id,
                        'keyword' => str_replace("'","",$value['string']),
                        'searches' => (is_numeric($value['volume']) ? number_format($value['volume'], 0, '', ',') : 0),
                        'cpc' => (is_numeric($value['cpc']) ? $value['cpc'] : 0),
                        'profit' => (is_numeric($value['volume']) && is_numeric($value['cpc'])) ? number_format(($value['cpc'] * $value['volume']) * 0.01, 2, '.', ',') : 0,
                        'word_count' => intval(count(explode(' ', $value['string']))),
                        'location' => $location,
                        'language' => $language,
                    ];
                    $i++;
                } else {
                    $cpc = is_numeric($value['cpc']) ? round($value['cpc'] / 1000000, 2) : 0;
                    $researchData[] = [
                        'keyword_id' => $key->id,
                        'keyword' => str_replace("'","",$value['keyword']),
                        'searches' => $this->handleNumFormat((is_numeric($value['volume']) ? number_format($value['volume'], 0, '', ',') : 0)),
                        'cpc' => $cpc,
                        'profit' => $this->handleNumFormat(is_numeric($value['volume']) ? number_format(($cpc * $value['volume']) * 0.01, 2, '.', ',') : 0),
                        'word_count' => intval(count(explode(' ', $value['keyword']))),
                        'location' => $location,
                        'language' => $language,
                    ];
                    $i++;
                }
            }

            $tmp = new ResearchTmp();
            $tmp->user_id = Auth::id();
            $tmp->serialized_data = base64_encode(serialize($researchData));
            $tmp->save();
            $tmp_id = $tmp->id;
        }

        $eval_data = [];
        DB::commit();
        return Response::json([
            'researchData' => $researchData,
            'searchedToday' => $this->searchedToday(),
            'keyword' => $project_name,
            'eval_data' => $eval_data,
            'location' => $location,
            'language' => $language,
            'google_engine' => $engine['engine'],
            'tmp_id' => $tmp_id,
            'isSaved' => false,
            'isImported' => true,
        ], 200);
        } catch (\Exception $e) {
          /* roll back there is a problem with the api */
          DB::rollBack();
          $msg = $e->getMessage() ? $e->getMessage() : "Unexpected errors, please try again!";
          return Response::json($msg, 422);
        }        

    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function userCountsAndLimits()
    {
       $user = Auth::user();
        $searchedCount = App\TblKeywordStatus::where('user_id', Auth::id())
            ->whereBetween('created_at',
                [Carbon::now()->startOfDay(), Carbon::now()->endOfDay()])->count();
        $userResearchLimit = $this->roleManagementController->getCurrentUserLimit('research');
        $evaluatedCount = App\TblEvaluationStatus::where('user_id', Auth::id())
            ->whereBetween('created_at',
                [Carbon::now()->startOfDay(), Carbon::now()->endOfDay()])->count();
        $userEvolutionLimit = $this->roleManagementController->getCurrentUserLimit('evolution');

        $saveKeywordsLimit = $this->roleManagementController->getCurrentUserLimit('can_save');
        $saveProjectsLimit = $this->roleManagementController->getCurrentUserLimit('project');

        $savedProjectsCount = Auth::user()->researchKeyword->count();

        $searchedKeywords = Auth::user()->researchKeyword->pluck('research');
        $saveKeywordsCount = 0;
        foreach ($searchedKeywords as $v) {
          $saveKeywordsCount += count($v);
        }

        return Response::json([
            'search'    => ['count' => $searchedCount, 'limit' => $userResearchLimit, 'id' => 'keywordSearchesBar'],
            'evolution' => ['count' => $evaluatedCount, 'limit' => $userEvolutionLimit, 'id' => 'keywordEvaluationsBar'],
            'projects'  => ['count' => $savedProjectsCount, 'limit' => $saveProjectsLimit, 'id' => 'projectsBar'],
            'keywords'  => ['count' => $saveKeywordsCount, 'limit' => $saveKeywordsLimit, 'id' => 'keywordsBar'],
        ], 200);
    }

    /**
     * @param Request $request
     * @param ApiLoggerService $apiLogger
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function insert(Request $request, ApiLoggerService $apiLogger)
    {

        $user = Auth::user();
        $apiLogger->setUser($user);

        $userSearched = App\TblKeywordStatus::where('user_id', Auth::id())
            ->whereBetween('created_at',
                [Carbon::now()->startOfDay(), Carbon::now()->endOfDay()])->count();
        if (!$user->hasRole('admin')) {
            if ($this->roleManagementController->checkTrialSubscription() == true) {
                return Response::json(getenv('TRIAL_PLAN_MESSAGE'), 422);
            }

            if ($userSearched >= $this->roleManagementController->getCurrentUserLimit('research')) {
                return Response::json('Keyword research limit reached, Kindly Upgrade your plan to increase your limit.', 422);
            }
        }
        $research = ResearchKeyword::where('keyword', $request->get('keyword'))->whereUserId(Auth::id())->first();
        if (!empty($research)) {
            return Response::json('This Keyword is already Searched', 422);
        }

        if ($request->has('is_check')) {
            return Response::json(['Error' => 'Check has been passed.'], 422);
        }

        $keyword = str_replace("'","",StringValidator::validate($request->input('keyword')));

        Input::replace(array_merge($request->all(), array('keyword' => $keyword)));
        // create some rules
        $this->validate($request, [
            'keyword' => 'required|max:80',
            'tld' => 'required',
            'lang' => 'required',
        ]);
        $loc = $request->input('tld');
        $lang = $request->input('lang');

        $engines = Config::get('searchengines');
        $selectedEngine = array(
            'name' => 'United States',
            'engine' => 'google.com',
            'short_form' => 'us'
        ); // filled with just default information

        foreach ($engines as $engine) {
            if ($engine['name'] == $loc) {
                $selectedEngine = $engine;
                break;
            }
        }

        $key = new App\TblKeywordStatus();
        $key->keyword = $keyword;
        $key->user_current_level = $user->account_id;
        $key->google_engine = $selectedEngine['engine'];
        $key->user_id = Auth::id();
        $key->location = $loc;
        $key->language = $lang;
        $key->save();

        // ====================================================================================================================
        $tmp_id = 0;
        $currentAPI = env('CURRENT_ACTIVE_API');
        $additionalApiCall = true; // with this we join seed keyword to API request results

        $engines = Config::get('searchengines');
        $country = 'all';

        foreach ($engines as $engine) {
            if ($engine['name'] == $loc) {
                $country = $engine['short_form'];
                break;
            }
        }

        $client = new Client();

        if ($currentAPI == 1) {
            $params = [
                'apikey' => env('API_KEY'),
                'keyword' => $request->input('keyword'),
                'metrics_network' => 'googlesearch',
                'metrics' => 'true',
                'metrics_location' => '',
                'metrics_language' => '',
                'metrics_currency' => 'USD',
                'output' => 'json'
            ];

            $data = $this->makeRequest([
                'url' => Config::get('casper.scenario2_one_url') . http_build_query($params),
                'type' => ApiLoggerService::API_TYPE_KEYWORDS_SEARCH
            ]);

            if (is_array($data) === false) {
                return $data;
            }

            // $data = Config::get('mocked_api_data');
            $data = $data['results'];
            $researchData = [];
            $unickKeyWords = [];
            $unickData = [];

            if (!empty($data)) {
                foreach ($data as $value) {
                    for ($i = 0; $i < count($value); $i++) {
                        if (!in_array($value[$i]['string'], $unickKeyWords)) {
                            array_push($unickKeyWords, $value[$i]['string']);
                            $researchData[] = [
                                'keyword_id' => $key->id,
                                'keyword' => str_replace("'","",$value[$i]['string']),
                                'searches' => (is_numeric($value[$i]['volume']) ? number_format($value[$i]['volume'], 0, '', ',') : 0),
                                'cpc' => (is_numeric($value[$i]['cpc']) ? $value[$i]['cpc'] : 0),
                                'profit' => (is_numeric($value[$i]['volume']) && is_numeric($value[$i]['cpc'])) ? number_format(($value[$i]['cpc'] * $value[$i]['volume']) * 0.01, 2, '.', ',') : 0,
                                'word_count' => intval(count(explode(' ', $value[$i]['string']))),
                                'location' => $loc,
                                'language' => $lang,
                            ];
                        }
                    }
                }
                $tmp = new ResearchTmp();
                $tmp->user_id = Auth::id();
                $tmp->serialized_data = base64_encode(serialize($researchData));
                $tmp->save();
                $tmp_id = $tmp->id;
            }
        } else {
            $params = array(
                'key' => env('API_KEY2'),
                'keyword' => $request->get('keyword'),
                'language' => $lang,
                'country' => $country,
                'source' => 'adwords'
            );

             $data = $this->makeRequest([
                'url' => Config::get('casper.scenario3_one_url') . http_build_query($params),
                'type' => ApiLoggerService::API_TYPE_KEYWORDS_SEARCH,
            ]);

            if (is_array($data) === false) {
                return $data;
            }

            if ($additionalApiCall) {
                $data = $data['stats'];
                $params = ['key' => env('API_KEY2')];

                $aditionalData = $this->makeRequest([
                    'url' => Config::get('casper.scenario3_two_url') . http_build_query($params),
                    'params' => [
                        'json' => [
                            'keywords' => [$request->input('keyword')],
                            'language' => $lang,
                            'country' => $country
                        ]
                    ],
                    'type' => ApiLoggerService::API_TYPE_KEYWORDS_ADDITIONAL_SEARCH,
                ]);

                if (is_array($aditionalData) === false) {
                    return $aditionalData;
                }

                if (!empty($aditionalData['stats'][0])) {
                    $data[] = $aditionalData['stats'][0];
                }
            }

            // $data = Config::get('mocked_api_data3');
            $researchData = [];
            $unickKeyWords = [];
            $unickData = [];

            if (!empty($data)) {
                for ($i = 0; $i < count($data); $i++) {
                    if (!in_array($data[$i]['keyword'], $unickKeyWords)) {
                        array_push($unickKeyWords, $data[$i]['keyword']);
                        $cpc = is_numeric($data[$i]['cpc']) ? round($data[$i]['cpc'] / 1000000, 2) : 0;
                        $researchData[] = [
                            'keyword_id' => $key->id,
                            'keyword' => str_replace("'","",$data[$i]['keyword']),
                            'searches' => (is_numeric($data[$i]['volume']) ? number_format($data[$i]['volume'], 0, '', ',') : 0),
                            'cpc' => $cpc,
                            'profit' => is_numeric($data[$i]['volume']) ? number_format(($cpc * $data[$i]['volume']) * 0.01, 2, '.', ',') : 0,
                            'word_count' => intval(count(explode(' ', $data[$i]['keyword']))),
                            'location' => $loc,
                            'language' => $lang,
                        ];
                    }
                }

                $tmp = new ResearchTmp();
                $tmp->user_id = Auth::id();
                $tmp->serialized_data = base64_encode(serialize($researchData));
                $tmp->save();
                $tmp_id = $tmp->id;
            }
        }

        return Response::json([
            'researchData' => $researchData,
            'eval_data' => [],
            'keyword' => $request->input('keyword'),
            'selectedLanguage' => $lang,
            'selectedLocation' => $loc,
            'eval_detail' => [],
            'tmp_id' => $tmp_id,
            'isSaved' => false,
            'isImported' => false,
        ], 200);
    }

    public function insertData($savedKeyword=false)
    {
        $request = request();
        $apiLogger = app(ApiLoggerService::class);
        /* start the transaction */
        DB::beginTransaction();

        try {

          $user = Auth::user();
          $apiLogger->setUser($user);

          $userSearched = App\TblKeywordStatus::where('user_id', Auth::id())
              ->whereBetween('created_at',
                  [Carbon::now()->startOfDay(), Carbon::now()->endOfDay()])->count();
          if (!$user->hasRole('admin')) {
              if ($this->roleManagementController->checkTrialSubscription() == true) {
                  return Response::json(getenv('TRIAL_PLAN_MESSAGE'), 422);
              }            
              if ($userSearched >= $this->roleManagementController->getCurrentUserLimit('research')) {
                  return Response::json('Keyword research limit reached, Upgrade your plan to increase your limit.', 422);
              }
          }
          $research = ResearchKeyword::where('keyword', $request->get('keyword'))->whereUserId(Auth::id())->first();
          if (!empty($research) && !$savedKeyword) {
              return Response::json('This keyword is already in your saved searches, Please load or delete it to proceed!', 422);
          }

//          if ($request->has('is_check')) {
//              return Response::json('Check has been passed.', 422);
//          }

          $keyword = str_replace("'","",StringValidator::validate($request->input('keyword')));

          Input::replace(array_merge($request->all(), array('keyword' => $keyword)));
          // create some rules
          $this->validate($request, [
              'keyword' => 'required|max:80',
              'tld' => 'required',
              'lang' => 'string',
          ]);

          $loc = $request->input('tld');
          $lang = $request->lang ? $request->lang : 'all';

          $engines = Config::get('searchengines');
          $selectedEngine = array(
              'name' => 'United States',
              'engine' => 'google.com',
              'short_form' => 'us'
          ); // filled with just default information

          foreach ($engines as $engine) {
              if ($engine['name'] == $loc) {
                  $selectedEngine = $engine;
                  break;
              }
          }

          $keywordSearchedToday = App\TblKeywordStatus::where('user_id', Auth::id())
              ->where('keyword',$keyword)
              ->whereBetween('created_at',
                  [Carbon::now()->startOfDay(), Carbon::now()->endOfDay()])->first();

          if (!$keywordSearchedToday) {
            $key = new App\TblKeywordStatus();
            $key->keyword = $keyword;
            $key->user_current_level = $user->account_id;
            $key->google_engine = $selectedEngine['engine'];
            $key->user_id = Auth::id();
            $key->location = $loc;
            $key->language = $lang;
            $key->save();
          }else{
            $key = $keywordSearchedToday;
          }

          // ====================================================================================================================
          $tmp_id = 0;
          $currentAPI = env('CURRENT_ACTIVE_API');
          $additionalApiCall = true; // with this we join seed keyword to API request results

          $engines = Config::get('searchengines');
          $country = 'all';

          foreach ($engines as $engine) {
              if ($engine['name'] == $loc) {
                  $country = $engine['short_form'];
                  break;
              }
          }

          $client = new Client();

          if ($currentAPI == 1) {
              $params = [
                  'apikey' => env('API_KEY'),
                  'keyword' => $request->input('keyword'),
                  'metrics_network' => 'googlesearch',
                  'metrics' => 'true',
                  'metrics_location' => '',
                  'metrics_language' => '',
                  'metrics_currency' => 'USD',
                  'output' => 'json'
              ];

              $data = $this->makeRequest([
                  'url' => Config::get('casper.scenario2_one_url') . http_build_query($params),
                  'type' => ApiLoggerService::API_TYPE_KEYWORDS_SEARCH
              ]);

              if (is_array($data) === false) {
                  return $data;
              }

              // $data = Config::get('mocked_api_data');
              $data = $data['results'];
              $researchData = [];
              $unickKeyWords = [];
              $unickData = [];

              if (!empty($data)) {
                  foreach ($data as $value) {
                      for ($i = 0; $i < count($value); $i++) {
                          if (!in_array($value[$i]['string'], $unickKeyWords)) {
                              array_push($unickKeyWords, $value[$i]['string']);
                              $researchData[] = [
                                  'keyword_id' => $key->id,
                                  'keyword' => str_replace("'","",$value[$i]['string']),
                                  'searches' => (is_numeric($value[$i]['volume']) ? number_format($value[$i]['volume'], 0, '', ',') : 0),
                                  'cpc' => (is_numeric($value[$i]['cpc']) ? $value[$i]['cpc'] : 0),
                                  'profit' => (is_numeric($value[$i]['volume']) && is_numeric($value[$i]['cpc'])) ? number_format(($value[$i]['cpc'] * $value[$i]['volume']) * 0.01, 2, '.', ',') : 0,
                                  'word_count' => intval(count(explode(' ', $value[$i]['string']))),
                                  'location' => $loc,
                                  'language' => $lang,
                              ];
                          }
                      }
                  }
                  $tmp = new ResearchTmp();
                  $tmp->user_id = Auth::id();
                  $tmp->serialized_data = base64_encode(serialize($researchData));
                  $tmp->save();
                  $tmp_id = $tmp->id;
              }
          } else {
              $params = array(
                  'key' => env('API_KEY2'),
                  'keyword' => $request->get('keyword'),
                  'language' => $lang,
                  'country' => $country,
                  'source' => 'adwords'
              );

              $data = $this->makeRequest([
                  'url' => Config::get('casper.scenario3_one_url') . http_build_query($params),
                  'type' => ApiLoggerService::API_TYPE_KEYWORDS_SEARCH,
              ]);

              if (is_array($data) === false) {
                  return $data;
              }

              if ($additionalApiCall) {
                  $data = $data['stats'];
                  $params = ['key' => env('API_KEY2')];

                  $aditionalData = $this->makeRequest([
                      'url' => Config::get('casper.scenario3_two_url') . http_build_query($params),
                      'params' => [
                          'json' => [
                              'keywords' => [$request->input('keyword')],
                              'language' => $lang,
                              'country' => $country
                          ]
                      ],
                      'type' => ApiLoggerService::API_TYPE_KEYWORDS_ADDITIONAL_SEARCH,
                  ]);

                  if (is_array($aditionalData) === false) {
                      return $aditionalData;
                  }

                  if (!empty($aditionalData['stats'][0])) {
                      $data[] = $aditionalData['stats'][0];
                  }
              }

              // $data = Config::get('mocked_api_data3');
              $researchData = [];
              $unickKeyWords = [];
              $unickData = [];

              if (!empty($data)) {
                  for ($i = 0; $i < count($data); $i++) {
                      if (!in_array($data[$i]['keyword'], $unickKeyWords)) {
                          array_push($unickKeyWords, $data[$i]['keyword']);
                          $cpc = is_numeric($data[$i]['cpc']) ? round($data[$i]['cpc'] / 1000000, 2) : 0;
                          $researchData[] = [
                              'keyword_id' => $key->id,
                              'keyword' => str_replace("'","",$data[$i]['keyword']),
                              'searches' => (is_numeric($data[$i]['volume']) ? number_format($data[$i]['volume'], 0, '', ',') : 0),
                              'cpc' => $cpc,
                              'profit' => is_numeric($data[$i]['volume']) ? number_format(($cpc * $data[$i]['volume']) * 0.01, 2, '.', ',') : 0,
                              'word_count' => intval(count(explode(' ', $data[$i]['keyword']))),
                              'location' => $loc,
                              'language' => $lang,
                          ];
                      }
                  }
                  $tmp = new ResearchTmp();
                  $tmp->user_id = Auth::id();
                  $tmp->serialized_data = base64_encode(serialize($researchData));
                  $tmp->save();
                  $tmp_id = $tmp->id;
              }
          }

          foreach ($researchData as $i => $v) {
            $researchData[$i]['word_count'] = intval($v['word_count']);
            $researchData[$i]['searches']   = $this->handleNumFormat($v['searches']);
            $researchData[$i]['profit']     = $this->handleNumFormat($v['profit'], true);
            /* make the searched keyword the first one in the table */
            if ($v['keyword'] == $request->keyword) {
              $researchData[$i] = $researchData[0];
              $v['searches']=$this->handleNumFormat($v['searches']);
              $v['profit']=$this->handleNumFormat($v['profit'], true);
              $researchData[0]  = $v;
            }
          }
          /* everthing is going well */
          DB::commit();
          $finalData = [
            'researchData' => $researchData,
            'searchedToday' => $this->searchedToday(),
            'search' => [
              'keyword' => $request->input('keyword'),
              'location' => $loc,
              'language' => $lang,
              'tmp_id' => $tmp_id,
              'keyword_id' => $key->id,
            ],
            'eval_data' => [],
            'evaluatedItems' => [],
            'id' => $key->id,
            'google_engine' => $key->google_engine,
            'keyword' => $request->input('keyword'),
            'selectedLanguage' => $lang,
            'language' => $lang,
            'selectedLocation' => $loc,
            'location' => $loc,
            'eval_detail' => [],
            'evaluatedDetails' => [],
            'tmp_id' => $tmp_id,
            'isSaved' => false,
            'isImported' => false,
          ];
          /* I will this function with saved keyword to search again */
          if ($savedKeyword) {
            return $finalData;
          }

          return Response::json($finalData, 200);

        } catch (\Exception $e) {
          /* roll back there is a problem with the api */
          DB::rollBack();
          $msg = $e->getMessage() ? $e->getMessage() : "Unexpected errors, please try again!";
          return Response::json($msg, 422);
        }
    }

    public function handleNumFormat($str, $int=false){
      if ($int) {
        $pos = strrpos($str, '.');
        $str = $pos && is_numeric($pos) ? substr($str,0,$pos) : $str;
      }

      $str = preg_replace('/[^0-9.]/s', '', $str);
      if (strrpos($str, '.') && is_numeric(strrpos($str, '.'))) {
        return (float) $str;
      }
      return (int) $str;
    }

    public function getLocation($inputEngine = null, $inputName = null)
    {
        $engines = Config::get('searchengines');
        /** @var string $location */
        foreach ($engines as $engine) {
            if (!is_null($inputEngine)) {
                if ($engine['engine'] == $inputEngine) {
                    $location = $engine['name'];
                    break;
                }
            } elseif (!is_null($inputName)) {
                if ($engine['name'] == $inputName) {
                    $location = $engine['engine'];
                    break;
                }
            } else {
                $location = '';
            }

        }
        return $location;
    }

    public function findResearch($id)
    {
        /** @var ResearchKeyword $keyword */
        $keyword = ResearchKeyword::find($id);
        /** @var Research $research */
        $eval_data = Research::whereKeywordSearchId($id)->get();
        foreach ($eval_data as $item) {
          $item->savedKeyword = true;
          $item->cpc = $this->handleNumFormat($item->keyword_cpc);
          $item->profit = $this->handleNumFormat($item->estimated_profile);
          $item->searches = $this->handleNumFormat($item->monthly_searches);
        }

        /** @var $location $view */
        $location = $this->getLocation($keyword->google_engine, null);
        $language = $keyword->language ? $keyword->language : 'all';

        $is_ajax_view = 0;

        return $this->retrieveSearchData($keyword->keyword, $keyword->id, $location, $language, $eval_data, null, $saved = 1, $is_ajax_view);
    }

    public function evaluated($evaluate_id)
    {
        $evaluate = Research::where('id', $evaluate_id)->first();

        if (empty($evaluate)) {
            return Response::json('Data Does not exist', 422);
        }

        $language = $evaluate->language ? $evaluate->language : 'all';

        $data = [];
        $data['keyword'] = $evaluate->keyword;
        $data['keyword_id'] = $evaluate->keyword_search_id;
        $data['searches'] = (int)str_replace(',','', $evaluate->monthly_searches);
        $data['cpc'] = is_numeric($evaluate->keyword_cpc) ? round($evaluate->keyword_cpc / 1000000, 2) : 0;
        $data['profit'] = (int)str_replace(',','', $evaluate->estimated_profile);
        $data['word_count'] = intval($evaluate->word_count);
        $data['location'] = $evaluate->location;
        $data['language'] = $language;
        $data['evaluate_id'] = $evaluate->id;

        $keyword = ResearchKeyword::find($evaluate->keyword_search_id);
        $evals = Research::where('keyword_search_id', $evaluate->keyword_search_id)->get();
        $eval_data = [];
        $savedKeywordsArr = [];
        foreach ($evals as $eval) {
            $savedKeywordsArr[] = $eval->keyword;
            $eval_data[$eval->keyword] = array(
                'id' => $eval->id,
                'difficulty' => $eval->difficulty,
                'isUnsaved' => false
            );
        }

        $unsaved = UnsavedResearch::where('user_id', Auth::id())
            ->where('project_name', $keyword->keyword)
            ->whereBetween('created_at',
                [Carbon::now()->subHours(12), Carbon::now()])
            ->whereNotIn('keyword', $savedKeywordsArr)
            ->get();

        foreach ($unsaved as $eval) {
            $eval_data[$eval->keyword] = array(
                'id' => $eval->id,
                'difficulty' => $eval->difficulty,
                'isUnsaved' => true
            );
        }
        $saved = 1;
        $is_ajax_view = 0;

        return $this->retrieveSearchData($keyword->keyword, $keyword->id, $evaluate->location, $language, $eval_data, $data, $saved, $is_ajax_view);
    }

    public function getDomainAvailability($keyword_name)
    {
        $domainCheckResult = [];
        if (ctype_alnum(trim(str_replace(' ', '', $keyword_name)))) {
            if (Auth::user()->can('domain_finder') && class_exists('RDNS') && !empty($keyword_name)) {
                $domain = new Domain($keyword_name);
                $domainCheckResult = $domain->check();
            }

            if (isset($domainCheckResult[$keyword_name])) {
                $domainResult = $domainCheckResult[$keyword_name];
            } else {
                $domainResult = array(
                    'com' => false,
                    'net' => false,
                    'org' => false,
                );
            }
        } else {
            $domainResult = array(
                'com' => true,
                'net' => true,
                'org' => true,
                'flag' => true
            );
        }
        return $domainResult;
    }

    public function postDomainAvailability(Request $request)
    {
        $inputs = $request->all();
        $keywords = $inputs['keyword'];
        $isArabic = [];
        $isNotArabic = [];
        $domainsToCheck = [];
        $domainResult = [];
        $domainResults = [];

        $nc_api = array(
            'api_user' => env('TLD_API_USER'),
            'api_key' => env('TLD_API_KEY'),
            'api_ip' => env('TLD_API_IP')
        );

        foreach ($keywords as $key => $word) {
            $domain = trim(str_replace(' ', '', $word));
            if (ctype_alnum($domain)) {
                $isNotArabic[$key] = $word;
                $domainsToCheck[] = $domain . '.com';
                $domainsToCheck[] = $domain . '.net';
                $domainsToCheck[] = $domain . '.org';
            } else {
                $isArabic[$key] = $word;
            }
        }
        $domain = '';

        if (!empty($isNotArabic)) {
            if (Auth::user()->can('domain_finder')) {
                $namecheap = new Namecheap($nc_api);
                $domainsCheck = $namecheap->domainsCheck($domainsToCheck);

                foreach ($isNotArabic as $word) {
                    $domain = trim(str_replace(' ', '', $word));

                    $domainResults[$word] = array(
                        'com' => $domainsCheck[$domain . '.com'],
                        'net' => $domainsCheck[$domain . '.net'],
                        'org' => $domainsCheck[$domain . '.org'],
                    );
                }
            }
        }

        foreach ($keywords as $key => $keyword) {
            if (in_array($keyword, $isArabic)) {
                $domainResult[$keyword] = array(
                    'com' => true,
                    'net' => true,
                    'org' => true,
                    'flag' => true
                );
            } else {
                $index = array_key_exists($keyword, $domainResults);
                if ($index) {
                    $domainResult[$keyword] = $domainResults[$keyword];
                }
            }
        }

        return $domainResult;
    }

    public function retrieveSearchData($keyword_name, $keyword_id, $location, $language, $eval_data, $eval_detail = null, $saved = false, $is_ajax_view = 0)
    {

        $research = ResearchKeyword::whereId($keyword_id)->whereUserId(Auth::id())->first(['research_tmp_id']);

        if (!$research) {
          return Response::json('There is a problem, Please try Agian!', 422);
        }
        $researchTmp = ResearchTmp::where('user_id', Auth::id())->whereId($research->research_tmp_id)->first();
        // tmp id used to hold the tmp search
        $tmp_id = 0;
        $researchData = [];
        if ($researchTmp) {
            $researchData = unserialize(base64_decode($researchTmp->serialized_data));
            $tmp_id = $researchTmp->id;
        }else{
          $request = request();
          $request->merge(['keyword' => $keyword_name, 'lang'=> $language,'tld'=> $location]);

          $response = $this->insertData(true);
          if (is_object($response)) {
            return $response;
          }
          $researchData = $response['researchData'];
          $tmp_id = $response['tmp_id'];
          ResearchKeyword::where('id', $keyword_id)->update(['research_tmp_id' => $tmp_id]);
        }
        $modifiedResearchData = $researchData;
        $modifiedEvalData = [];
        foreach ($eval_data as $i => $eval) {
          $eval['savedKeyword'] = true;
          $eval['cpc'] = $this->handleNumFormat($eval['keyword_cpc']);
          $eval['searches'] = $this->handleNumFormat($eval['searches']);
          $eval['profit'] = $this->handleNumFormat($eval['profit'], true);
          $modifiedEvalData[$eval['keyword']] = $eval;
          // array_unshift($modifiedResearchData, $eval);
        }
        $keys = array_keys($modifiedEvalData);
        foreach ($researchData as $i => $item) {
          $item['cpc'] = $this->handleNumFormat($item['cpc']);
          $item['searches'] = $this->handleNumFormat($item['searches']);
          $item['profit'] = $this->handleNumFormat($item['profit'], true);
          $modifiedResearchData[$i] = $item;
          if (in_array($item['keyword'],$keys)) {
            $modifiedResearchData[$i] = $modifiedEvalData[$item['keyword']];
            unset($modifiedEvalData[$item['keyword']]);
          }
        }
        $modifiedResearchData = array_merge(array_values($modifiedEvalData),$modifiedResearchData);
        if(!$researchTmp){
          ResearchTmp::where('id', $tmp_id)->update(['serialized_data' => base64_encode(serialize($researchData))]);
        }
        return Response::json([
            'researchData' => $modifiedResearchData,
            'evaluatedKeywords' => $eval_data,
            'keyword' => $keyword_name,
            'eval_detail' => $eval_detail,
            'keyword_id' => $keyword_id,
            'tmp_id' => $tmp_id,
            'searchedToday' => $this->searchedToday(),
            'isSaved' => $saved,
            'isImported' => false
        ], 200);
    }

    /**
     * @param $id
     */
    public function downloadPreview($id)
    {
        /** @var ResearchTmp $research */
        $research = ResearchTmp::where('user_id', Auth::id())->where('id', $id)->first();
        $csvData = [];

        if ($research) {
            $data = unserialize(base64_decode($research->serialized_data));

            foreach ($data as $item) {

                $csvData[] = [
                    'keyword' => html_entity_decode($item['keyword'], ENT_NOQUOTES, 'UTF-8'),
                    'monthly_searches' => $item['searches'],
                    'adword_cpc' => $item['cpc'],
                    'estimated profit' => $item['profit'],
                    'word_count' => intval($item['word_count']),
                    'location' => $item['location']
                ];
            }
        }
        $fileName = (isset($csvData[0]['keyword']) && !empty($csvData[0]['keyword']) ? str_replace(' ', '_', $csvData[0]['keyword'])."csv" : 'keyword_research.csv');

        $csv = new ExportCsv($csvData, $fileName, false);
        $csv->send();
        die;
    }

    public function downloadEvaluated($id, $downloadEvaluated)
    {
        $data = request()->researches ? request()->researches : [];
        $csvData = [];

        foreach ($data as $item) {
            $csvData[] = [
                'keyword' => html_entity_decode($item['keyword'], ENT_NOQUOTES, 'UTF-8'),
                'monthly_searches' => $item['searches'],
                'adword_cpc' => $item['cpc'],
                'estimated profit' => $item['profit'],
                'difficulty' => isset($item['difficulty']) ? $item['difficulty'] : '-',
                'word_count' => intval($item['word_count']),
                'location' => $item['location']
            ];
        }
        session()->put('downloadSearchedData', $csvData);
        return;
        /*
        $research = ResearchTmp::where('user_id', Auth::id())->where('id', $id)->first();
        $csvData = [];

        if ($research) {
            $data = unserialize(base64_decode($research->serialized_data));
            $downloadEvaluated = $downloadEvaluated === 'true' ? true : false;

            if ($downloadEvaluated) {
                $data = $evalData;
                foreach ($data as $item) {
                  $csvData[] = [
                      'keyword' => html_entity_decode($item['keyword'], ENT_NOQUOTES, 'UTF-8'),
                      'monthly_searches' => $item['searches'],
                      'adword_cpc' => $item['cpc'],
                      'estimated_profile' => $item['profit'],
                      'difficulty' => $item['difficulty'],
                      'word_count' => $item['word_count'],
                      'location' => $item['location']
                  ];
                }
            } else {
                foreach ($data as $item) {
                    $keyword = html_entity_decode($item['keyword'], ENT_NOQUOTES, 'UTF-8');

                    // $item['difficulty'] = isset($item['difficulty']) ? $item['difficulty'] : '-';

                    $csvData[] = [
                        'keyword' => html_entity_decode($item['keyword'], ENT_NOQUOTES, 'UTF-8'),
                        'monthly_searches' => $item['searches'],
                        'adword_cpc' => $item['cpc'],
                        'estimated_profile' => $item['profit'],
                        'difficulty' => isset($item['difficulty']) ? $item['difficulty'] : '-',
                        'word_count' => $item['word_count'],
                        'location' => $item['location']
                    ];
                }
            }
          }
          session()->put('downloadSearchedData', $csvData);
          */
    }

    public function downloadSearchedData(){
      $data = session()->pull('downloadSearchedData', []);
      $fname = (isset($data[0]['keyword']) ? str_replace(' ', '_', $data[0]['keyword']).'.csv' : 'keyword_research.csv');
      $csv = new ExportCsv($data, $fname, false);
      $csv->send();
      die;
    }

    public function download()
    {
        $research = DB::table('research')
            ->join('research_keywords', 'research.keyword_search_id', '=', 'research_keywords.id')
            ->select('research.*', DB::raw('research_keywords.keyword as project_name'))
            ->where('research.user_id', '=', Auth::id())
            ->get();
        $csvData = [];

        foreach ($research as $item) {
            $csvData[] = [
                'project_name' => $item->project_name,
                'keyword' => $item->keyword,
                'monthly_searches' => $item->monthly_searches,
                'keyword_cpc' => $item->keyword_cpc,
                'estimated profit' => $item->estimated_profile,
                'word_count' => intval($item->word_count),
                'location' => $item->location,
                'difficulty' => $item->difficulty,
            ];
        }
        $fileName = (isset($csvData[0]['keyword']) && !empty($csvData[0]['keyword']) ? str_replace(' ', '_', $csvData[0]['keyword'])."csv" : 'keyword_research.csv');
        $csv = new ExportCsv($csvData, $fileName, false);
        $csv->send();
        die;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws ApiOperationFailedException
     */
    public function store(Request $request)
    {

        $user = Auth::user();
        $userSavedKeywords = Research::where('user_id', Auth::id())->count();
        $userSavedProjects = ResearchKeyword::where('user_id', Auth::id())->count();
        if (!$user->hasRole('admin')) {
            if ($this->roleManagementController->checkTrialSubscription() == true) {
                return Response::json(getenv('TRIAL_PLAN_MESSAGE'), 422);
            }    
            if ($userSavedKeywords >= $this->roleManagementController->getCurrentUserLimit('can_save')) {
                // throw new ApiOperationFailedException('Saved keywords limit reached, Upgrade your plan to increase your limit.');
                return Response::json('Saved keywords limit reached, Upgrade your plan to increase your limit.', 422);
            }
        }

            if ($request->has("main_keyword_id") && $request->input("main_keyword_id") != "") {

                $ResearchKeyword = ResearchKeyword::find($request->input("main_keyword_id"));
                $research = Research::create([
                    'user_id' => Auth::id(),
                    'keyword' => $request->input('keyword'),
                    'monthly_searches' => str_replace(",", "", $request->input('searches')),
                    'keyword_cpc' => $request->input('cpc'),
                    'estimated_profile' => str_replace(",", "", $request->input('profit')),
                    'word_count' => $request->input('word_count'),
                    'location' => $request->input('location'),
                    'language' => $request->input('language'),
                    'difficulty' => $request->input('difficulty'),
                    'keyword_search_id' => $ResearchKeyword->id,
                    'user_current_level' => Auth::user()->account_id,
                    'google_engine' => '',
                ]);
            }else{

                if (!$user->hasRole('admin')) {
                    if ($this->roleManagementController->checkTrialSubscription() == true) {
                        return Response::json(getenv('TRIAL_PLAN_MESSAGE'), 422);
                    }     
                    if ($userSavedProjects >= $this->roleManagementController->getCurrentUserLimit('project')) {
                        // throw new ApiOperationFailedException('Saved projects limit reached, Upgrade your plan to increase your limit.');
                        return Response::json('Saved projects limit reached, Upgrade your plan to increase your limit.', 422);
                  }
                }

                $ResearchKeyword = ResearchKeyword::where('keyword',$request->input("main_keyword"))->first();

                if(empty($ResearchKeyword)){

                    $googleEngine = $this->getLocation(null, $request->input('location'));
                    $ResearchKeyword = new ResearchKeyword();
                    $ResearchKeyword->keyword = $request->input('main_keyword') ? $request->input('main_keyword') : "";
                    $ResearchKeyword->research_tmp_id = $request->input('tmp_id');
                    $ResearchKeyword->google_engine = $googleEngine;
                    $ResearchKeyword->language = $request->input('language');
                    $ResearchKeyword->user_current_level = Auth::user()->account_id;
                    $ResearchKeyword->user_id = Auth::user()->id;
                    $ResearchKeyword->save();

                    $research = Research::create([
                        'user_id' => Auth::id(),
                        'keyword' => $request->input('keyword'),
                        'monthly_searches' => str_replace(",", "", $request->input('searches')),
                        'keyword_cpc' => $request->input('cpc'),
                        'estimated_profile' => str_replace(",", "", $request->input('profit')),
                        'word_count' => $request->input('word_count'),
                        'location' => $request->input('location'),
                        'language' => $request->input('language'),
                        'difficulty' => $request->input('difficulty'),
                        'keyword_search_id' => $ResearchKeyword->id,
                        'user_current_level' => Auth::user()->account_id,
                        'google_engine' => '',
                    ]);

                }else{

                    $research = Research::create([
                        'user_id' => Auth::id(),
                        'keyword' => $request->input('keyword'),
                        'monthly_searches' => str_replace(",", "", $request->input('searches')),
                        'keyword_cpc' => $request->input('cpc'),
                        'estimated_profile' => str_replace(",", "", $request->input('profit')),
                        'word_count' => $request->input('word_count'),
                        'location' => $request->input('location'),
                        'language' => $request->input('language'),
                        'difficulty' => $request->input('difficulty'),
                        'keyword_search_id' => $ResearchKeyword->id,
                        'user_current_level' => Auth::user()->account_id,
                        'google_engine' => '',
                    ]);
                }
            }

//            $research = Research::whereKeyword($research->keyword)->whereKeywordSearchId($ResearchKeyword->id)->whereUserId(Auth::id())->first();
//            if (!empty($research)) {
//                // return Response::json( 'This keyword is already saved.', 422);
//            }

        $inputs = $request->get('googleResults');
        // $inputs = json_decode($inputs, true);
        if (!empty($inputs)) {
            $this->storeEvaluationUrl($inputs, $research->id);
        }
        $research->savedKeyword = true;
        $research->cpc = $this->handleNumFormat($research->keyword_cpc);
        $research->profit = $this->handleNumFormat($research->estimated_profile);
        $research->searches = $this->handleNumFormat($research->monthly_searches);
        $research->tmp_id = $request->tmp_id;
        return Response::json($this->getSavedKeywords(), 200);
    }

    /**
    * @param Request $request
    * @return \Illuminate\Http\JsonResponse
    * @throws ApiOperationFailedException
    */
    public function storeFilteredKeyword(Request $request) {
      $postRow        = $request['keyword_data'][0];
      $main_keyword   = (isset($request['main_keyword']) ? $request['main_keyword'] : '');
      $main_keyword_id= (isset($request['main_keyword_id']) ? $request['main_keyword_id'] : '');
      $tmp_id         = (isset($request['tmp_id']) ? $request['tmp_id'] : '');
      $language       = (isset($request['lang']) ? $request['lang'] : '');

      $user = Auth::user();
      $userSavedKeywords = Research::where('user_id', Auth::id())->count();
      $userSavedProjects = ResearchKeyword::where('user_id', Auth::id())->count();

      if (!$user->hasRole('admin')) {
          if ($this->roleManagementController->checkTrialSubscription() == true) {
              return Response::json(getenv('TRIAL_PLAN_MESSAGE'), 422);
          }    
          if ($userSavedKeywords >= $this->roleManagementController->getCurrentUserLimit('can_save')) {
              return Response::json('Saved keywords limit reached, Upgrade your plan to increase your limit.', 422);
          }
      }

      if(isset($main_keyword_id) && !empty($main_keyword_id)) {
            $ResearchKeyword = ResearchKeyword::find($main_keyword_id);
            if(empty($ResearchKeyword)) {
              $googleEngine = $this->getLocation(null, $postRow['location']);
              $ResearchKeyword = new ResearchKeyword();
              $ResearchKeyword->keyword = $main_keyword;
              $ResearchKeyword->research_tmp_id = $tmp_id;
              $ResearchKeyword->google_engine = $googleEngine;
              $ResearchKeyword->language = $language;
              $ResearchKeyword->user_current_level = Auth::user()->account_id;
              $ResearchKeyword->user_id = Auth::user()->id;
              $ResearchKeyword->save();
            }

            $research        = Research::create([
                'user_id'    => Auth::id(),
                'keyword'    => $postRow['keyword'],
                'monthly_searches' => str_replace(",", "", $postRow['searches']),
                'keyword_cpc'=> (isset($postRow['cpc']) ? $postRow['cpc'] : ''),
                'estimated_profile' => (isset($postRow['profit']) ? str_replace(",", "", $postRow['profit']) : ''),
                'word_count' => (isset($postRow['word_count']) ? $postRow['word_count'] : ''),
                'location'   => (isset($postRow['location']) ? $postRow['location'] : ''),
                'language'   => (isset($postRow['language']) ? $postRow['language'] : ''),
                'difficulty' => (isset($postRow['difficulty']) ? $postRow['difficulty'] : ''),
                'keyword_search_id'  => (isset($ResearchKeyword->id) ? $ResearchKeyword->id : ''),
                'user_current_level' => Auth::user()->account_id,
                'google_engine' => '',
            ]);
      } else {
            if (!$user->hasRole('admin')) {
                if ($this->roleManagementController->checkTrialSubscription() == true) {
                    return Response::json(getenv('TRIAL_PLAN_MESSAGE'), 422);
                }
                if ($userSavedProjects >= $this->roleManagementController->getCurrentUserLimit('project')) {
                    return Response::json('Saved projects limit reached, Upgrade your plan to increase your limit.', 422);
                }
            }

            $ResearchKeyword  = ResearchKeyword::where('keyword',$main_keyword)->first();
            if(empty($ResearchKeyword)) {
                $googleEngine = $this->getLocation(null, $postRow['location']);
                $ResearchKeyword = new ResearchKeyword();
                $ResearchKeyword->keyword = $main_keyword;
                $ResearchKeyword->research_tmp_id = $tmp_id;
                $ResearchKeyword->google_engine = $googleEngine;
                $ResearchKeyword->language = $language;
                $ResearchKeyword->user_current_level = Auth::user()->account_id;
                $ResearchKeyword->user_id = Auth::user()->id;
                $ResearchKeyword->save();

                $research = Research::create([
                  'user_id'    => Auth::id(),
                  'keyword'    => $postRow['keyword'],
                  'monthly_searches' => str_replace(",", "", $postRow['searches']),
                  'keyword_cpc'=> (isset($postRow['cpc']) ? $postRow['cpc'] : ''),
                  'estimated_profile' => (isset($postRow['profit']) ? str_replace(",", "", $postRow['profit']) : ''),
                  'word_count' => (isset($postRow['word_count']) ? $postRow['word_count'] : ''),
                  'location'   => (isset($postRow['location']) ? $postRow['location'] : ''),
                  'language'   => (isset($postRow['language']) ? $postRow['language'] : ''),
                  'difficulty' => (isset($postRow['difficulty']) ? $postRow['difficulty'] : ''),
                  'keyword_search_id'  => (isset($ResearchKeyword->id) ? $ResearchKeyword->id : ''),
                  'user_current_level' => Auth::user()->account_id,
                  'google_engine' => '',                    
                ]);
            } else {
                $research = Research::create([
                  'user_id'    => Auth::id(),
                  'keyword'    => $postRow['keyword'],
                  'monthly_searches' => str_replace(",", "", $postRow['searches']),
                  'keyword_cpc'=> (isset($postRow['cpc']) ? $postRow['cpc'] : ''),
                  'estimated_profile' => (isset($postRow['profit']) ? str_replace(",", "", $postRow['profit']) : ''),
                  'word_count' => (isset($postRow['word_count']) ? $postRow['word_count'] : ''),
                  'location'   => (isset($postRow['location']) ? $postRow['location'] : ''),
                  'language'   => (isset($postRow['language']) ? $postRow['language'] : ''),
                  'difficulty' => (isset($postRow['difficulty']) ? $postRow['difficulty'] : ''),
                  'keyword_search_id'  => (isset($ResearchKeyword->id) ? $ResearchKeyword->id : ''),
                  'user_current_level' => Auth::user()->account_id,
                  'google_engine' => '',                    
                ]);
            }
        }

        $inputs = $request->get('googleResults');
        // $inputs = json_decode($inputs, true);
        if (!empty($inputs)) {
            $this->storeEvaluationUrl($inputs, $research->id);
        }

        $research->savedKeyword = true;
        $research->cpc      = $this->handleNumFormat($research->keyword_cpc);
        $research->profit   = $this->handleNumFormat($research->estimated_profile);
        $research->searches = $this->handleNumFormat($research->monthly_searches);
        $research->tmp_id   = $tmp_id;
        return Response::json($this->getSavedKeywords(), 200);
    }

    /**
     * this function will evaluate the given keyword (string) and return the json information
     *
     * @param $keyword
     * @param $searches
     * @param $cpc
     * @param $profit
     * @param $word_count
     * @param $location
     * @param $evaluated_id
     * @param ApiLoggerService $apiLogger
     *
     * @throws ApiOperationFailedException
     * @return \Illuminate\Http\JsonResponse
     */
    public function evaluate(
        $keyword, $keyword_id, $searches, $cpc, $profit,
        $word_count, $location, $language, $search_keyword, $evaluated_id,
        $isEvaluated, $isUnsaved, $projectName, $tmp_id,
        ApiLoggerService $apiLogger
    ) {
        Log::info('------------# EVALUATION REQUEST #-----------------');
        Log::info(Carbon::now()->toDateTimeString());

        //$keyword, $keyword_id, $searches, $cpc, $profit,
        //$word_count, $location, $language, $search_keyword, $evaluated_id,
        //$isEvaluated, $isUnsaved, $projectName, $tmp_id,
        //ApiLoggerService $apiLogger
        //Request $request

        try {
            DB::beginTransaction();
            $user = Auth::user();
            $apiLogger->setUser($user);

//            return 'evaluated_id : '.$evaluated_id.' Is_Evaluated : '.$isEvaluated;

            if (($evaluated_id == 'true' && $isEvaluated == "true") || ($evaluated_id == 'false' && $isEvaluated == "false")) {

                if (!$user->hasRole('admin')) {
                    $evaluatedCount = App\TblEvaluationStatus::where('user_id', Auth::id())
                        ->whereBetween('created_at',
                            [Carbon::now()->startOfDay(), Carbon::now()->endOfDay()])->count();

                    if ($this->roleManagementController->checkTrialSubscription() == true) {
                        return Response::json(getenv('TRIAL_PLAN_MESSAGE'), 422);
                    }

                    if ($evaluatedCount >= $this->roleManagementController->getCurrentUserLimit('evolution')) {
                        // throw new ApiOperationFailedException('Keyword evaluations limit reached, Upgrade your plan to increase your limit.');
                        return Response::json('Keyword evaluations limit reached, Upgrade your plan to increase your limit.', 422);

                    }
                }

                $this->debug = true;
                $pageCounter = 0;
                $seomozLinks = '';
                $difficultyScore = 0;
                $maxPageCount = env('KeywordResearch_MaxPages', 10);

                // clean up the location
                $location = urldecode($location);
                $engines = Config::get('searchengines');
                $selectedEngine = array(
                    'name' => 'United States',
                    'engine' => '.com',
                    'short_form' => 'com'
                ); // filled with just default information

                foreach ($engines as $engine) {
                    if ($engine['name'] == $location) {
                        $selectedEngine = $engine;
                        break;
                    }
                }

                // check domain
                $domainResult = $this->getDomainAvailability($keyword);

                $module = 1;
                $searchurl = "https://www." . $selectedEngine['engine'] . "/search?pws=0&gl=" . $selectedEngine['short_form'] . "&q=" . str_replace(' ', '+', $keyword) . "&num=20";
                // do some google result scraping
                $googleData = new Google();
                $googleResults = [];
                if ($this->debug) {
                    $googleData = new Google(2);
                    $googleResults = $googleData->scrape(
                        $selectedEngine['engine'],
                        $selectedEngine['short_form'],
                        $keyword,
                        $module
                    );
                    /*
                    * make sure that the ten urls are unique.
                    */
                    $googleResults = array_values(array_unique($googleResults, SORT_STRING));

                    // If error appears
                    if (!$googleResults || empty($googleResults)) {
                        $apiLogger->log([
                            'url' => $googleData->getScrapeUrl(
                                $selectedEngine['engine'],
                                $selectedEngine['short_form'],
                                $keyword
                            ),
                            'api_type' =>  ApiLoggerService::API_TYPE_SOCIAL_NETWORKS,
                            'systemErrorCode' => ApiLoggerService::ERROR_EMPTY_RESPONSE,
                        ]);
                    }
                }


                // for now we are going to use this
                $googleListing = $graphResults = array(); // this will hold everything for our google search

                // social information
                /** @var Social $social */
                $social = new Social();
                $social->keyword = $keyword;

                Log::info('------------ GETTING SOCIAL DATA -----------------');
                Log::info(Carbon::now()->toDateTimeString());
//                dd($graphResults);

                $social->getSocialInformation(array_slice($googleResults, 0, $maxPageCount), $module);

                Log::info('------------------- GOT SOCIAL DATA ------------------');
                Log::info(Carbon::now()->toDateTimeString());

                $graphResults['meta_url']['positive'] = $graphResults['meta_url']['negative'] = $graphResults['meta_desc']['positive'] = $graphResults['meta_desc']['negative'] = $graphResults['meta_title']['positive'] = $graphResults['meta_title']['negative'] = $graphResults['meta_h1']['positive'] = $graphResults['meta_h1']['negative'] = 0;

                // grab all the social media
                foreach ($googleResults as $idx => $url) {
                    // only allow 10 pages
                    if ($pageCounter >= $maxPageCount) {
                        break;
                    }

                    $googleListing[$idx] = array();

                    // first store the search url
                    $googleListing[$idx]['url'] = $url;

                    // put links together for Seomoz
                    if (strlen(trim($seomozLinks)) > 0) {
                        $seomozLinks .= 'MOZURLSEPRATOR';
                    }
                    $seomozLinks .= $googleListing[$idx]['url'];//parse_url($googleListing[$idx]['url'], PHP_URL_HOST);

                    // Facebook Shares
                    $googleListing[$idx]['facebook_shares'] = $social->facebook[$googleListing[$idx]['url']];
                    // Google Plus
                    $googleListing[$idx]['googlePlus'] = $social->googlePlus[$googleListing[$idx]['url']];
                    // Twitter
                    $googleListing[$idx]['tweets'] = $social->twitter[$googleListing[$idx]['url']];

                    // meta data
                    $googleListing[$idx]['meta'] = $social->metaResults[$googleListing[$idx]['url']];

                    // pinterest share
                    $googleListing[$idx]['pinterest'] = $social->pinterest[$googleListing[$idx]['url']];

                    $graphResults['facebook_shares'][$idx] = $social->facebook[$googleListing[$idx]['url']];
                    $graphResults['tweets'][$idx] = $social->twitter[$googleListing[$idx]['url']];
                    $graphResults['googlePlus'][$idx] = $social->googlePlus[$googleListing[$idx]['url']];
                    $graphResults['pinterest'][$idx] = $social->pinterest[$googleListing[$idx]['url']];

                    // increase the page count
                    $pageCounter++;
                }

                // do some Seomoz
                // seomoz, called in a group to save on requests
                if (strlen($seomozLinks) > 0) {
                    $seomoz = $social->seomoz($seomozLinks);

                    if (is_array($seomoz)) {
                        if ($seomoz['valid']) {
                            foreach ($googleListing as $idx => $data) {
                                $googleListing[$idx]['page_authority'] = round($seomoz['domains'][$idx]['page_authority']);
                                $googleListing[$idx]['domain_authority'] = round($seomoz['domains'][$idx]['domain_authority']);
                                $googleListing[$idx]['backlinks'] = $seomoz['domains'][$idx]['backlinks'];
                                $googleListing[$idx]['score'] = $seomoz['domains'][$idx]['score'];
                                $difficultyScore += round($seomoz['domains'][$idx]['page_authority']);

                                $graphResults['backlinks'][$idx] = round($seomoz['domains'][$idx]['domain_authority']);
                                $graphResults['meta_url']['positive'] = $googleListing[$idx]['meta']['url'] === true ? $graphResults['meta_url']['positive'] + 1 : $graphResults['meta_url']['positive'];
                                $graphResults['meta_url']['negative'] = $googleListing[$idx]['meta']['url'] === false ? $graphResults['meta_url']['negative'] + 1 : $graphResults['meta_url']['negative'];
                                $graphResults['meta_desc']['positive'] = $googleListing[$idx]['meta']['desc'] === true ? $graphResults['meta_desc']['positive'] + 1 : $graphResults['meta_desc']['positive'];
                                $graphResults['meta_desc']['negative'] = $googleListing[$idx]['meta']['desc'] === false ? $graphResults['meta_desc']['negative'] + 1 : $graphResults['meta_desc']['negative'];
                                $graphResults['meta_title']['positive'] = $googleListing[$idx]['meta']['title'] === true ? $graphResults['meta_title']['positive'] + 1 : $graphResults['meta_title']['positive'];
                                $graphResults['meta_title']['negative'] = $googleListing[$idx]['meta']['title'] === false ? $graphResults['meta_title']['negative'] + 1 : $graphResults['meta_title']['negative'];
                                $graphResults['meta_h1']['positive'] = $googleListing[$idx]['meta']['h1'] === true ? $graphResults['meta_h1']['positive'] + 1 : $graphResults['meta_h1']['positive'];
                                $graphResults['meta_h1']['negative'] = $googleListing[$idx]['meta']['h1'] === false ? $graphResults['meta_h1']['negative'] + 1 : $graphResults['meta_h1']['negative'];

                            }
                        }
                    }
                }
                if ($difficultyScore != 0) {
                    $difficulty = intval($difficultyScore / count($googleListing));
                } else {
                    $difficulty = 0;
                }

//                $evaluate = Research::whereKeyword($keyword)->whereKeywordSearchId($keyword_id)->first();

                $evaluate = Research::whereKeyword($keyword)->where('user_id',Auth::id())->first();

                $unsavedResearch = UnsavedResearch::whereUserId(Auth::id())->whereKeyword($keyword)->first();

                if (empty($evaluate)) {
                    // let's store this information into our database to run reports on it
                    $research = App\TblEvaluationStatus::create([
                        'user_id' => Auth::id(),
                        'keyword' => $keyword,
                        'monthly_searches' => $searches,
                        'keyword_cpc' => $cpc,
                        'estimated_profile' => $profit,
                        'word_count' => $word_count,
                        'difficulty' => $difficulty,
                        'location' => $location,
                        'language' => $language,
                        'user_current_level' => $user->account_id,
                        'google_engine' => $selectedEngine['engine']
                    ]);

                    $evaluated_id = $research->id;
                    $savedFlag = 0;
                }



                if ($projectName !== '') {

                    if (!empty($unsavedResearch)) {
                        $unsavedResearch->delete();
                    }

                    $unsavedResearch = UnsavedResearch::create([
                        'user_id' => Auth::id(),
                        'keyword' => $keyword,
                        'project_name' => $projectName,
                        'monthly_searches' => $searches,
                        'keyword_cpc' => $cpc,
                        'estimated_profile' => $profit,
                        'word_count' => $word_count,
                        'location' => $location,
                        'language' => $language,
                        'difficulty' => $difficulty,
                        'google_engine' => $selectedEngine['engine']
                    ]);

                    $this->storeUnsavedEvaluationUrl($googleListing, $unsavedResearch->id);
                }
                /* prepare Social data for the chart*/
                $graphResults["socialChartData"] = [
                  $graphResults["facebook_shares"],
                  $graphResults["tweets"],
                  $graphResults["googlePlus"],
                  $graphResults["pinterest"]
                ];
                $graphResults["domainStrengthData"] = [
                  $graphResults["backlinks"]
                ];

                /** @var $savedFlag $data */
                $data = array(
                    'keyword' => $keyword,
                    'difficulty' => $difficulty,
                    'evaluated_id' => $evaluated_id,
                    'graphResults' => $graphResults,
                    'newEvaluate' => true,
                    'project_name' => $projectName,
                    'keyword_id' => $keyword_id,
                    'research_id' => isset($evaluate) ? $evaluate->id : '',
                    're_evaluate_keyword' => isset($evaluate) ? true : false,
                    'searches' => $this->handleNumFormat($searches),
                    'cpc' => $this->handleNumFormat($cpc),
                    'profit' => $this->handleNumFormat($profit),
                    'word_count' => intval($word_count),
                    'location' => $location,
                    'language' => $language,
                    'unsavedResearch_id' => $unsavedResearch->id,
                    'difficulty' => $difficulty, // for now
                    'created_at' => $unsavedResearch->created_at,
                    'googleResults' => $googleListing,
                    'userCanSaveKeyword' => Auth::user()->can('keyword_save'),
                    'savedKeyword' => isset($savedFlag) ? ($savedFlag?true:false) : true,

                    // 'html' => view('backend.tools.keywordresearch.evaluation', [
                    //     'research_id' => isset($evaluate) ? $evaluate->id : '',
                    //     'tmp_id' => $tmp_id,
                    //     'search_keyword' => $projectName,
                    //     'uniqueId' => uniqid(),
                    //     'keyword' => $keyword,
                    //     'keyword_id' => $keyword_id,
                    //     'URIComponent' => $this->encodeURIComponent($keyword),
                    //     'searches' => (int) $searches,
                    //     'searchurl' => $searchurl,
                    //     'cpc' => (int) $cpc,
                    //     'profit' => (int) $profit,
                    //     'word_count' => $word_count,
                    //     'location' => $location,
                    //     'language' => $language,
                    //     'difficulty' => $difficulty, // for now
                    //     'googleResults' => $googleListing,
                    //     'domainResult' => $domainResult,
                    //     'save' => isset($savedFlag) ? $savedFlag : 1,
                    // ])->render(),
                );
            } else {

                if ($isUnsaved == "true") {
                    $research = UnsavedResearch::whereKeyword($keyword)->whereProjectName($keyword_id)->first();
                    if (!$research) {
                      $research = UnsavedResearch::where('user_id', Auth::id())
                      ->where('project_name', $keyword_id)
                      ->whereBetween('created_at',
                      [Carbon::now()->subHours(12), Carbon::now()])
                      ->first();
                    }
                    // return Response::json($research->google_engine,425);
                    $googleEngine = $research->google_engine;
                    $googleResults = UnsavedEvaluationsUrl::whereKeywordUnsavedResearchId($research->id)->get();
                } else {
//                    return 'Keyword : '.$keyword.' keyword_id : '.$keyword_id;
                    $research = Research::whereKeyword($keyword)->whereUserId(Auth::id())->first();
                    $keywordSearch = ResearchKeyword::whereId($research->keyword_search_id)->first();
                    $googleEngine = $keywordSearch->google_engine;
                    $projectName = $keywordSearch->keyword;
                    $googleResults = EvaluationsUrl::whereKeywordResearchId($research->id)->get();
                }

                $domainResult = $this->getDomainAvailability($research->keyword);
                $engines = Config::get('searchengines');
                /** @var string $location */
                foreach ($engines as $engine) {
                    if ($engine['engine'] == $googleEngine) {
                        $short_form = $engine['short_form'];
                        break;
                    }
                }
                $googleListing = $graphResults = [];
                $searchurl = "https://www." . $googleEngine . "/search?pws=0&gl=" . $short_form . "&q=" . str_replace(' ', '+', $research->keyword) . "&num=20";
                if (!empty($googleResults)) {
                    $graphResults['meta_url']['positive'] = $graphResults['meta_url']['negative'] = $graphResults['meta_desc']['positive'] = $graphResults['meta_desc']['negative'] = $graphResults['meta_title']['positive'] = $graphResults['meta_title']['negative'] = $graphResults['meta_h1']['positive'] = $graphResults['meta_h1']['negative'] = 0;

                    foreach ($googleResults as $idx => $url) {
                        $googleListing[$idx] = array();
                        $googleListing[$idx]['url'] = $url->top_ten_url;
                        $googleListing[$idx]['page_authority'] = $url->page_authority;
                        $googleListing[$idx]['domain_authority'] = $url->domain_authority;
                        $googleListing[$idx]['backlinks'] = $url->back_links;
                        $googleListing[$idx]['score'] = $url->moz_rank;
                        $googleListing[$idx]['pinterest'] = $url->pinterest_share;
                        $googleListing[$idx]['googlePlus'] = $url->google_plus_1s;
                        $googleListing[$idx]['facebook_shares'] = $url->fb_share;
                        $googleListing[$idx]['tweets'] = $url->tweets;
                        if (!isset($googleListing[$idx]['meta'])) {
                            $googleListing[$idx]['meta'] = [];
                        }
                        $meta = ['title' => $url->title, 'url' => $url->url, 'desc' => $url->desc, 'h1' => $url->h1];
                        $googleListing[$idx]['meta'] = $meta;

                        $graphResults['facebook_shares'][$idx] = $url->fb_share;
                        $graphResults['tweets'][$idx] = $url->tweets;
                        $graphResults['pinterest'][$idx] = $url->pinterest_share;
                        $graphResults['googlePlus'][$idx] = $url->google_plus_1s;
                        $graphResults['backlinks'][$idx] = $url->domain_authority;
                        $graphResults['meta_url']['positive'] = (bool)$url->url ? $graphResults['meta_url']['positive'] + 1 : $graphResults['meta_url']['positive'];
                        $graphResults['meta_url']['negative'] = !(bool)$url->url ? $graphResults['meta_url']['negative'] + 1 : $graphResults['meta_url']['negative'];
                        $graphResults['meta_desc']['positive'] = (bool)$url->desc ? $graphResults['meta_desc']['positive'] + 1 : $graphResults['meta_desc']['positive'];
                        $graphResults['meta_desc']['negative'] = !(bool)$url->desc ? $graphResults['meta_desc']['negative'] + 1 : $graphResults['meta_desc']['negative'];
                        $graphResults['meta_title']['positive'] = (bool)$url->title ? $graphResults['meta_title']['positive'] + 1 : $graphResults['meta_title']['positive'];
                        $graphResults['meta_title']['negative'] = !(bool)$url->title ? $graphResults['meta_title']['negative'] + 1 : $graphResults['meta_title']['negative'];
                        $graphResults['meta_h1']['positive'] = (bool)$url->h1 ? $graphResults['meta_h1']['positive'] + 1 : $graphResults['meta_h1']['positive'];
                        $graphResults['meta_h1']['negative'] = !(bool)$url->h1 ? $graphResults['meta_h1']['negative'] + 1 : $graphResults['meta_h1']['negative'];
                    }
                }

                /* prepare Social data for the chart*/
                $graphResults["socialChartData"] = [
                  $graphResults["facebook_shares"],
                  $graphResults["tweets"],
                  $graphResults["googlePlus"],
                  $graphResults["pinterest"]
                ];
                $graphResults["domainStrengthData"] = [
                  $graphResults["backlinks"]
                ];

                $data = array(
                    'keyword' => $research->keyword,
                    'difficulty' => $this->handleNumFormat($research->difficulty),
                    'evaluated_id' => $research->id,
                    'graphResults' => $graphResults,
                    'newEvaluate' => false,
                    'googleResults' => $googleListing,
                    'userCanSaveKeyword' => Auth::user()->can('keyword_save'),
                    // 'html' => view('backend.tools.keywordresearch.evaluation', [
                    //     'research_id' => $research->id,
                    //     'tmp_id' => $tmp_id,
                    //     'search_keyword' => $projectName,
                    //     'uniqueId' => uniqid(),
                    //     'keyword' => $research->keyword,
                    //     'URIComponent' => $this->encodeURIComponent($research->keyword),
                    //     'searches' => (int) $research->monthly_searches,
                    //     'searchurl' => $searchurl,
                    //     'cpc' => (int) $research->keyword_cpc,
                    //     'profit' => (int) $research->estimated_profile,
                    //     'word_count' => $research->word_count,
                    //     'location' => $research->location,
                    //     'language' => $research->language,
                    //     'difficulty' => $research->difficulty, // for now
                    //     'googleResults' => $googleListing,
                    //     'domainResult' => $domainResult,
                    //     'save' => 1,
                    // ])->render(),
                );
            }

            Log::info('------------# EVALUATION RESPONSE #-----------------');
            Log::info(Carbon::now()->toDateTimeString());

            // send back the information in a JSON format

            Log::info('------------# RESPONSE RESULT #-----------------');

            DB::commit();
            return Response::json($data, 200);

        } catch (\Exception $e) {

            DB::rollBack();
            return Response::json('We were unable to complete your request: ' . $e->getMessage(), 422);
            // throw new ApiOperationFailedException('We were unable to complete your request: ' . $e->getMessage());
        }
    }
    public function angularTemplates($x){
      return view('ang.'.$x);
    }

    /** Insert and Update evaluation Top ten Url and Social data of details of keyword
     * @param $googleListing
     * @param $researchId
     * @return bool
     */
    public function storeEvaluationUrl($googleListing, $researchId)
    {
        Log::info('------------# EVALUATION STORE AND UPDATE #-----------------');
        Log::info(Carbon::now()->toDateTimeString());
        try {
            if (!empty($googleListing)) {
                DB::beginTransaction();
                $evaluations = EvaluationsUrl::whereKeywordResearchId($researchId)->first();
                if (!empty($evaluations)) {
                    EvaluationsUrl::whereKeywordResearchId($researchId)->delete();
                }
                foreach ($googleListing as $input) {
                    $data = [
                        'keyword_research_id' => $researchId,
                        'top_ten_url' => $input['url'],
                        'page_authority' => isset($input['page_authority']) ? $input['page_authority'] : 0,
                        'domain_authority' => isset($input['domain_authority']) ? $input['domain_authority'] : 0,
                        'back_links' => isset($input['backlinks']) ? $input['backlinks'] : 0,
                        'moz_rank' => isset($input['score']) ? $input['score'] : 0,
                        'pinterest_share' => isset($input['pinterest']) ? $input['pinterest'] : 0,
                        'google_plus_1s' => isset($input['googlePlus']) ? $input['googlePlus'] : 0,
                        'fb_share' => isset($input['facebook_shares']) ? $input['facebook_shares'] : 0,
                        'tweets' => isset($input['tweets']) ? $input['tweets'] : 0,
                        'url' => $input['meta']['url'],
                        'title' => $input['meta']['title'],
                        'desc' => $input['meta']['desc'],
                        'h1' => $input['meta']['h1']
                    ];
                    EvaluationsUrl::create($data);
                }
                DB::commit();
            }
            Log::info('------------# STORE RESULT #-----------------');
            return true;

        } catch (QueryException $e) {
            DB::rollBack();
            return false;
        }
    }

    public function storeUnsavedEvaluationUrl($googleListing, $unsavedeRsearchId)
    {
        Log::info('------------# UNSAVED EVALUATION STORE AND UPDATE #-----------------');
        Log::info(Carbon::now()->toDateTimeString());

        try {
            if (!empty($googleListing)) {
                DB::beginTransaction();
                $evaluations = UnsavedEvaluationsUrl::whereKeywordUnsavedResearchId($unsavedeRsearchId)->first();
                if (!empty($evaluations)) {
                    UnsavedEvaluationsUrl::whereKeywordUnsavedResearchId($unsavedeRsearchId)->delete();
                }
                foreach ($googleListing as $input) {
                    $data = [
                        'keyword_unsaved_research_id' => $unsavedeRsearchId,
                        'top_ten_url' => $input['url'],
                        'page_authority' => isset($input['page_authority']) ? $input['page_authority'] : 0,
                        'domain_authority' => isset($input['domain_authority']) ? $input['domain_authority'] : 0,
                        'back_links' => isset($input['backlinks']) ? $input['backlinks'] : 0,
                        'moz_rank' => isset($input['score']) ? $input['score'] : 0,
                        'pinterest_share' => isset($input['pinterest']) ? $input['pinterest'] : 0,
                        'google_plus_1s' => isset($input['googlePlus']) ? $input['googlePlus'] : 0,
                        'fb_share' => isset($input['facebook_shares']) ? $input['facebook_shares'] : 0,
                        'tweets' => isset($input['tweets']) ? $input['tweets'] : 0,
                        'url' => $input['meta']['url'],
                        'title' => $input['meta']['title'],
                        'desc' => $input['meta']['desc'],
                        'h1' => $input['meta']['h1']
                    ];
                    UnsavedEvaluationsUrl::create($data);
                }
                DB::commit();
            }
            Log::info('------------# STORE RESULT #-----------------');
            return true;

        } catch (QueryException $e) {
            DB::rollBack();
            return false;
        }
    }

    /**
     * PHP version of JavaScript's encodeURIComponent
     *
     * @param $str
     *
     * @return string
     */
    public function encodeURIComponent($str)
    {
        $revert = array('%21' => '!', '%2A' => '*', '%27' => "'", '%28' => '(', '%29' => ')');

        return strtr(rawurlencode($str), $revert);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function saved($keyword, $keyIndexList)
    {
        $keywords = ResearchKeyword::whereKeyword($keyword)->whereUserId(Auth::id())->first();
        $research = [];
        if (!empty($keywords)) {
            $research = Research::whereKeywordSearchId($keywords->id)->where('user_id', Auth::id())->get();
        }
        $keyIndexArr = isset($keyIndexList) ? json_decode($keyIndexList) : array();
        if (!empty($playerlist)) {
            foreach ($research as $key => $res) {
                $res['keyIndex'] = $keyIndexArr[$key];
            }
        }
        /** @var Object $research */
        return Response::json([
            'success' => true,
            'data' => view('backend.tools.keywordresearch.saved-table',
                ['saved' => $research])->render(),
            'data2' => $this->notSaved($keyword, $research, TRUE)
        ], 200);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function notSaved($keyword, $research, $notAjax)
    {
        $savedKeywordsArr = [];

        if (!empty($research)) {
            foreach ($research as $savedKeyword) {
                $savedKeywordsArr[] = $savedKeyword->keyword;
            }
        }

        $notSavedKeywords = UnsavedResearch::where('user_id', Auth::id())
            ->where('project_name', $keyword)
            ->whereBetween('created_at',
                [Carbon::now()->subHours(12), Carbon::now()])
            ->whereNotIn('keyword', $savedKeywordsArr)
            // ->groupBy('keyword')
            ->get();

        $notSavedKeywordsArr = [];
        foreach ($notSavedKeywords as $value) {
            $notSavedKeywordsArr[$value->id] = $value->keyword;
        }

        if ($notAjax) {
            return view('backend.tools.keywordresearch.unsaved-table', ['saved' => $notSavedKeywords, 'keywordsArr' => $notSavedKeywordsArr])->render();
        }

        return Response::json([
            'success' => true,
            'data2' => view('backend.tools.keywordresearch.unsaved-table',
                ['saved' => $notSavedEvaluations, 'keyword_search_id' => $keywords->id])->render(),
        ], 200);
    }

    /**
     * @param $keyword
     *
     * @return string
     */
    public function googleTrends($keyword)
    {
        return '<script type="text/javascript" src="https://www.google.com/trends/embed.js?hl=en-US&q=' . $this->encodeURIComponent($keyword) . '&cmpt=q&content=1&cid=TIMESERIES_GRAPH_0&export=5&w=900&h=330"></script>';
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $research = Research::where('id', $id)->where('user_id', Auth::id())->first();

        if (!empty($research)) {
            $research->delete();
        }

        // tell user
        Session::flash('success', array('The keyword has been deleted.'));

        // return
        return Redirect::route('backend.tools.keywordresearch');
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroyAjax(Request $request)
    {
     
//        return $request->all();
        $id = $request['id'];
        $keyword = $request['keyword'];
        $researchKeyword = ResearchKeyword::where(['id'=>$id,'keyword'=>$keyword])->where('user_id', Auth::id())->first();

        if (!empty($researchKeyword)){
//            return $researchKeyword;
            $unsavedResearch = UnsavedResearch::where('project_name', $researchKeyword->keyword)->where('user_id', Auth::id());
            $unsavedResearch->delete();
            $researchKeyword->delete();
        }else{
            $research = Research::where(['id'=>$id,'keyword'=>$keyword])->where('user_id', Auth::id())->first();
            $unsavedResearch = UnsavedResearch::where('project_name', $research->keyword)->where('user_id', Auth::id());
            $unsavedResearch->delete();
            $research->delete();
        }



//        if (!empty($research)) {
//            $unsavedResearch = UnsavedResearch::where('project_name', $research->keyword)->where('user_id', Auth::id());
//            $unsavedResearch->delete();
//            $research->delete();
//        }

        return Response::json([
            'success' => true
        ], 200);
    }

    public function destroyResearach(Request $request)
    {
//        return $request->all();
        $id = $request['id'];
        $isJustSaved = $request['isJustSaved'];
        $isResearachUnsaved = $request['isResearachUnsaved'];

        if ($isResearachUnsaved == 'true') {
            $research = UnsavedResearch::where('id', $id)->where('user_id', Auth::id())->first();
        } else if ($isJustSaved == 'true') {
            $unsavedResearch = UnsavedResearch::where('id', $id)->where('user_id', Auth::id())->first();
            $research = Research::where('keyword', $unsavedResearch->keyword)->where('user_id', Auth::id())->first();
        } else {
            $research = Research::where('id', $id)->where('user_id', Auth::id())->first();
        }

        if (!empty($research)) {
            $research->delete();
        }

        return Response::json([
            'success' => true
        ], 200);
    }

    public function bulkDelete(Request $request)
    {
        foreach ($request['mass_ids'] as $research) {
            $research = ResearchKeyword::find($research);
            if (!empty($research)) {
                $research->delete();
            }
        }
        return Response::json([
            'success' => true
        ], 200);
    }

    //save keyword click on savekeyword button
    public function saveKeyword(Request $request)
    {
        $userSavedProjects = ResearchKeyword::where('user_id', Auth::id())->count();
        if (!Auth::user()->hasRole('admin')) {
            if ($this->roleManagementController->checkTrialSubscription() == true) {
                return Response::json(getenv('TRIAL_PLAN_MESSAGE'), 422);
            }

            if ($userSavedProjects >= $this->roleManagementController->getCurrentUserLimit('project')) {
                // throw new ApiOperationFailedException('Project store exceeds the allowable limit');
                return Response::json('Project store exceeds the allowable limit.', 422);
            }
        }

        $location = $this->getLocation(null, $request->location);
        $ResearchKeyword = new ResearchKeyword();
        $ResearchKeyword->keyword = $request->search_keyword;
        $ResearchKeyword->research_tmp_id = $request->tmp_id;
        $ResearchKeyword->google_engine = $location;
        $ResearchKeyword->language = $request->language;
        $ResearchKeyword->user_current_level = Auth::user()->account_id;
        $ResearchKeyword->user_id = Auth::user()->id;
        $ResearchKeyword->save();
        $ResearchKeyword->research;
        $ResearchKeyword->tmp_id = $ResearchKeyword->research_tmp_id;

        /* This is for angular to ease the inition function */
        $ResearchKeyword->isSaved = true;
        $ResearchKeyword->savedSearches = $this->getSavedKeywords();

        return Response::json($ResearchKeyword, 200);
    }

    public function updateEvaluation(Request $request)
    {
        $ResearchKeyword = ResearchKeyword::whereKeyword($request->search_keyword)->whereUserId(Auth::id())->first();
        if (empty($ResearchKeyword)) {
            return Response::json([
                'success' => false,
                'message' => 'Data does not exist.'
            ], 200);
        }
        $evaluated = Research::whereId($request->id)->whereUserId(Auth::id())->first();
        if (empty($evaluated)) {
            return Response::json([
                'success' => false,
                'message' => 'Data does not exist.'
            ], 200);
        }
        $research = [
            'user_id' => Auth::id(),
            'keyword' => $request->keyword,
            'monthly_searches' => $request->searches,
            'keyword_cpc' => $request->cpc,
            'estimated_profile' => $request->profit,
            'word_count' => intval($request->word_count),
            'location' => $request->location,
            'difficulty' => $request->difficulty,
            'keyword_search_id' => $ResearchKeyword->id,
            'user_current_level' => Auth::user()->account_id,
            'google_engine' => $ResearchKeyword->google_engine,
        ];
        $evaluated->update($research);
        $inputs = $request->googleResults;
        $googleResults = json_decode($inputs, true);
        if (!empty($inputs)) {
            $this->storeEvaluationUrl($googleResults, $evaluated->id);
        }
        return Response::json([
            'success' => true,
        ], 200);

    }

    private function makeRequest(array $data)
    {
        $client = new Client();
        /** @var ApiLoggerService $apiLogger */
        $apiLogger = app(ApiLoggerService::class);

        $requestUrl = $data['url'];
        $requestParams = isset($data['params']) ? $data['params'] : null;
        $apiType = isset($data['type']) ? $data['type'] : null;

        try {
            $response = $client->post($requestUrl, $requestParams);
        } catch (\Exception $e) {
            $apiLogger->logException($e, [
                'apiType' => $apiType,
                'url' => $requestUrl
            ]);
            /* Throw new exception to rollback */
            throw new \Exception("There was an unexpected error. Please try again later.", 1);
        }

        $responseBody = json_decode($response->getBody(), true);

        if (empty($responseBody['error']) === false) {
            $apiLogger->log([
                'url' => $requestUrl,
                'error' => $responseBody['error']['message'],
                'api_type' => $apiType
            ]);
            /* Throw new exception to rollback */
            throw new \Exception('API error! ' . $responseBody['error']['message'], 1);
        }

        return $responseBody;
    }
}
