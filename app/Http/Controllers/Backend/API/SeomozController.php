<?php
namespace App\Http\Controllers\Backend\API;

use App\Services\ApiLoggerService;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Redirect;
use Response;


/*
    couple things about seomoz
    it does not work with rolling curl
 */

class SeomozController extends Controller
{
    public function __construct()
    {
        // Must be signed in to access anything in here
        $this->middleware('auth');
    }

    // the url's are to be separated from
    // http://www.google.ca https://www.yahoo.ca
    // to
    // http;www.google.ca,https;www.yahoo.ca
    public function get($urls)
    {
        error_log('Getting Seomoz Account');

        $seomoz  = \App\Seomoz::orderBy('last_accessed')->first();
        /** @var ApiLoggerService $apiLogger */
        $apiLogger = app(ApiLoggerService::class);

        $results = array(
            'valid'   => false,
            'domains' => array(),
        );

        if(empty($seomoz))
        {
            // we don't have any accounts
            return $results;
        }

        error_log('Using the following account: '. print_r($seomoz, true));

        // transform the urls
        $fullUrls = array();
        $tmp      = explode('MOZURLSEPRATOR', $urls);
        foreach($tmp as $url)
        {
            // ensure it is something.something.com and not http(s)://something.com
            $fullUrls[] = $url;
        }

        // as long as we have some real URL's lets update the seomoz database
        if(empty($fullUrls))
        {
            return $results;
        }

        // update our Seomoz
        $seomoz->last_accessed = date('Y-m-d H:i:s');
        $seomoz->update();

        error_log('Checking the following urls: '. print_r($fullUrls, true));

        // query our Seomoz
        $accessId         = $seomoz->username;
        $secretKey        = $seomoz->password;
        $expires          = time() + 30000;
        $stringToSign     = $seomoz->username ."\n". $expires;
        $binarySignature  = hash_hmac('sha1', $stringToSign, $secretKey, true);
        $urlSafeSignature = urlencode(base64_encode($binarySignature));
        // Learn more here: http://apiwiki.seomoz.org/categories/api-reference
        $cols = "103079233568";
        //External Links  32  ueid
        //MozRank 16384 umrp umrr
        //Page Authority  34359738368 upa
        //Domain Authority  68719476736 pda
        //Links 2048  uid The number of links (juice-passing or not, internal or external) to the URL

        // the url we are going to curl
        $requestUrl = "http://lsapi.seomoz.com/linkscape/url-metrics/?Cols=". $cols ."&AccessID=". $accessId ."&Expires=". $expires. "&Signature=". $urlSafeSignature;

        $content = $this->curlUrl($requestUrl, $seomoz->ip_address, $fullUrls);
        // only use below if we are testing
        // $content = array();
        // $content[] = array(
        //     "pda"  => '100',
        //     "ueid" => '4635594',
        //     "uid"  => '4979384',
        //     "umrp" => '7.816988133671934',
        //     "umrr" => '1.5892749246263748e-06',
        //     "upa"  => '96.91383184106036'
        // );
        // $content[] = array(
        //     "pda"  => '65.4370165640191',
        //     "ueid" => '1500',
        //     "uid"  => '1632',
        //     "umrp" => '6.006310975768902',
        //     "umrr" => '1.6435679887914545e-09',
        //     "upa"  => '71.42212583655112'
        // );

        if (is_array($content) === false) {
            $apiLogger->log([
                'url' => $requestUrl,
                'systemErrorCode' => ApiLoggerService::ERROR_EMPTY_RESPONSE,
                'api_type' => ApiLoggerService::API_TYPE_GOOGLE_MOZ,
                'request_body' => $fullUrls,
                'error' => $content
            ]);

            return $results;
        }

        // reformat it better
        $results['valid']   = true;
        $results['domains'] = array();
        foreach($content as $data)
        {
            $results['domains'][] = array(
                'page_authority'   => (isset($data->upa) ? $data->upa : ''),
                'domain_authority' => (isset($data->pda) ? $data->pda : ''),
                'backlinks'        => (isset($data->uid) ? $data->uid : ''),
                'score'            => (isset($data->umrp) ? $data->umrp : ''), // normalizes score
            );
        }

        return $results;
    }



    public function curlUrl($url, $ip_address, $parameters)
    {
        $content = array(
            'valid'    => false,
            'response' => ''
        );

        $options = array(
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POSTFIELDS     => json_encode($parameters),
            CURLOPT_USERAGENT => 'KeywordRevealer'
//            CURLOPT_INTERFACE      => $ip_address
        );

        error_log('Sending Curl');
        // init the curl
        $ch = curl_init($url);
        // set the options
        curl_setopt_array($ch, $options);
        // fire the event
        $content = curl_exec($ch);
        // clean up
        curl_close($ch);

        error_log('Content: '. $content);

        // return the content
        return json_decode($content);
    }
}
// EOF