<?php

namespace App\Http\Controllers\Auth;

use Laravel\Cashier\WebhookController;

use Config;
use Exception;
use Stripe\Event as StripeEvent;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Libraries\Logger\StripeLogger;

use Auth;
use App\User;
use App\Invoice;
use App\Account;
use App\Role;
use Carbon\Carbon;

use Mail;
use App\FailedPayment;

class StripeWebhookController extends WebhookController
{
	// customer.created
	protected function handleСustomerCreated()
	{
		return new Response('Webhook Handled111', 200);
	}

	// customer.updated
	protected function handleСustomerUpdated($payload)
	{
		return new Response('Webhook Handled', 200);
	}

	// customer.source.created
	protected function handleСustomerSourceCreated($payload)
	{
		return new Response('Webhook Handled', 200);
	}

	// customer.subscription.created
	protected function handleСustomerSubscriptionCreated($payload)
	{
		return new Response('Webhook Handled', 200);
	}

	// customer.subscription.updated
	protected function handleСustomerSubscriptionUpdated($payload)
	{
		return new Response('Webhook Handled', 200);
	}

	// charge.succeeded
	protected function handleChargeSucceeded($payload)
	{
		return new Response('Webhook Handled', 200);
	}

	// invoiceitem.created
	protected function handleInvoiceitemCreated()
	{
		return new Response('Webhook Handled', 200);
	}

	// invoice.created
	protected function handleInvoiceCreated(array $payload)
	{
		StripeLogger::info('Invoice Stripe Created');
		StripeLogger::info(print_r($payload, true));

		try {
			$invoice = Invoice::where('invoice_id', $payload['data']['object']['id'])->first();

			if (!$invoice) {
				$billable = $this->getBillable($payload['data']['object']['customer']);
				$subscription = Invoice::where('subscription_id', $payload['data']['object']['subscription'])->first();

				$invoice = new Invoice();

				$invoice->user_id = $billable->id;
				$invoice->customer_id = $payload['data']['object']['customer'];
				$invoice->subscription_id = $payload['data']['object']['subscription'];
				$invoice->invoice_id = $payload['data']['object']['id'];
				$invoice->date = Carbon::createFromTimestamp($payload['data']['object']['date'])->toDateTimeString();

				$linesData = $payload['data']['object']['lines']['data'];

				if ($subscription) {
					if (isset($linesData[1])) {
						$invoice->plan_id = Account::getPlanBySystemId($linesData[1]['plan']['id'])->id;
						$invoice->amount = $linesData[1]['amount'];
						$invoice->start_date = Carbon::createFromTimestamp($linesData[1]['period']['start'])->toDateTimeString();
						$invoice->end_date = Carbon::createFromTimestamp($linesData[1]['period']['end'])->toDateTimeString();
						$invoice->is_recuring = 0;
					} else {
						$invoice->plan_id = Account::getPlanBySystemId($linesData[0]['plan']['id'])->id;
						$invoice->amount = $linesData[0]['amount'];
						$invoice->start_date = Carbon::createFromTimestamp($linesData[0]['period']['start'])->toDateTimeString();
						$invoice->end_date = Carbon::createFromTimestamp($linesData[0]['period']['end'])->toDateTimeString();
						$invoice->is_recuring = 1;
					}
				} else {
					$invoice->plan_id = Account::getPlanBySystemId($linesData[0]['plan']['id'])->id;
					$invoice->amount = $linesData[0]['amount'];
					$invoice->start_date = Carbon::createFromTimestamp($linesData[0]['period']['start'])->toDateTimeString();
					$invoice->end_date = Carbon::createFromTimestamp($linesData[0]['period']['end'])->toDateTimeString();
					$invoice->is_recuring = 0;
				}

				$invoice->status = 'pending';
				$invoice->save();
			}
		} catch (\Exception $e) {
			StripeLogger::error($e->getMessage());
			StripeLogger::error($e);
			return new Response('Webhook Handled handleInvoiceCreated Error', 477);
		}


		return new Response('Webhook Handled handleInvoiceCreated', 200);
	}

	// invoice.payment_succeeded
	protected function handleInvoicePaymentSucceeded(array $payload)
	{
		StripeLogger::info('Invoice Stripe Succeeded');

		try {
			$invoice = Invoice::where('invoice_id', $payload['data']['object']['id'])->first();

			if ($invoice) {
				StripeLogger::info($payload['data']['object']['id']);
				$invoice->status = 'paid';
				$invoice->save();
				return new Response('Webhook Handled handleInvoicePaymentSucceeded - 1', 200);
			} else {
				StripeLogger::info('Invoice Stripe Created from PaymentSucceeded');
				StripeLogger::info(print_r($payload, true));

				$billable = $this->getBillable($payload['data']['object']['customer']);
				$subscription = Invoice::where('subscription_id', $payload['data']['object']['subscription'])->first();

				$invoice = new Invoice();

				$invoice->user_id = $billable->id;
				$invoice->customer_id = $payload['data']['object']['customer'];
				$invoice->subscription_id = $payload['data']['object']['subscription'];
				$invoice->invoice_id = $payload['data']['object']['id'];
				$invoice->date = Carbon::createFromTimestamp($payload['data']['object']['date'])->toDateTimeString();

				$linesData = $payload['data']['object']['lines']['data'];

				if ($subscription) {
					if (isset($linesData[1])) {
						$invoice->plan_id = Account::getPlanBySystemId($linesData[1]['plan']['id'])->id;
						$invoice->amount = $linesData[1]['amount'];
						$invoice->start_date = Carbon::createFromTimestamp($linesData[1]['period']['start'])->toDateTimeString();
						$invoice->end_date = Carbon::createFromTimestamp($linesData[1]['period']['end'])->toDateTimeString();
						$invoice->is_recuring = 0;
					} else {
						$invoice->plan_id = Account::getPlanBySystemId($linesData[0]['plan']['id'])->id;
						$invoice->amount = $linesData[0]['amount'];
						$invoice->start_date = Carbon::createFromTimestamp($linesData[0]['period']['start'])->toDateTimeString();
						$invoice->end_date = Carbon::createFromTimestamp($linesData[0]['period']['end'])->toDateTimeString();
						$invoice->is_recuring = 1;
					}
				} else {
					$invoice->plan_id = Account::getPlanBySystemId($linesData[0]['plan']['id'])->id;
					$invoice->amount = $linesData[0]['amount'];
					$invoice->start_date = Carbon::createFromTimestamp($linesData[0]['period']['start'])->toDateTimeString();
					$invoice->end_date = Carbon::createFromTimestamp($linesData[0]['period']['end'])->toDateTimeString();
					$invoice->is_recuring = 0;
				}

				$invoice->status = 'paid';
				$invoice->save();

				/*
				* Now the user paied the money successfully!
				* Check if There is a Delay for Downgrade his plan
				* If there is a delay for his plan! remove it to not be downgraded
				*/
				FailedPayment::where('user_id', $invoice->user_id)
											->where('downgraded', false)
											->where('created_at', '<', Carbon::now()->subDays(3)->toDateTimeString() )
											->delete();

				return new Response('Webhook Handled handleInvoicePaymentSucceeded - 2', 200);
			}
		} catch (\Exception $e) {
			StripeLogger::error($e->getMessage());
			StripeLogger::error($e);
			return new Response('Webhook Handled handleInvoicePaymentSucceeded Error', 477);
		}

		return new Response('Webhook Handled handleInvoicePaymentSucceeded', 200);
	}

	protected function handleInvoicePaymentFailed(array $payload)
	{
		$billable = $this->getBillable($payload['data']['object']['customer']);
		$invoice = Invoice::where('invoice_id', $payload['data']['object']['id'])->first();

		/*
		* Add failed payment data to the failed_payments table to be handeled later
		*/
		$failed = new FailedPayment;
		$failed->gateway    = 'stripe';
		$failed->user_id    = $invoice->user_id;
		$failed->invoice_id = $payload['data']['object']['id'];
		$failed->customer   = $payload['data']['object']['customer'];
		$failed->save();

		$user = User::find($invoice->user_id);

		$data = [
				'name' => $user->name,
				'canceldate' => Carbon::now()->addDays(3)->format('d/m/Y')
		];
		/*
		* Send an email to the user
		* to let him know that his account will be downgraded after 3 days
		* 3 Days from the failed payment date
		*/
		Mail::send('emails.subscription.delaydowngrade', $data, function ($message) use($user) {
				$message->to($user->email,$user->name)->subject('Keyword Revealer | Account Downgrade After 3 Days');
		});

		return new Response('Webhook Handled handleInvoicePaymentFailed', 200);
	}

	public function DelayHandleInvoicePaymentFailed($data){
		$billable = $this->getBillable($data['customer']);
		$invoice = Invoice::where('invoice_id', $data['invoice_id'])->first();

		if ($invoice) {
			$invoice->status = 'failed';
			$invoice->save();
		}

		if ($billable) {
			$billable->stripe_active = 0;
			$billable->account_id = Account::FreeAccountId();
			$billable->save();
			$freeRole = Role::where('name', 'free')->first();
			if (!empty($freeRole)) {
				$billable->roles()->sync([$freeRole->id]);
			}
		}

		if ($this->userIsSubscribedWithoutACard($billable)) {
			$billable->subscription->cancel(false);
		}
	}
}
