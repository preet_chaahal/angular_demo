<?php

namespace App\Http\Controllers\Auth;

use App;
use Validator;
use App\Account;
use App\EmailTemplate;

use App\CancellationReason;
use App\userCancellationReason;
use App\userSubscriptionCancellation;

use App\Events\DownGradePlanRemoveBrainStormData;
use App\Events\DownGradePlanRemoveRankCheckData;
use App\Http\Controllers\Admin\EmailTemplateManagementController;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Auth\IntercomController as IntercomController;
use App\Http\Requests;
use App\Libraries\Logger\PaypalLogger;
use App\Payment;
use App\Role;
use App\RoleLimit;
use App\Permission;   
use App\User;
use App\Cbinvoice;
use App\FailedPayment;
use App\Utilities\Paypal;
use Auth;
use Carbon\Carbon;
use DB;
use Doctrine\DBAL\Query\QueryException;
use Event;
use Illuminate\Http\Request;
use Input;
use Log;
use Mail;
use Redirect;
use Session;

use ChargeBee_Environment;
use ChargeBee_Plan;
use ChargeBee_HostedPage;
use ChargeBee_Subscription;
use ChargeBee_Customer;
use ChargeBee_Card;
use ChargeBee_Invoice;
use ChargeBee_Estimate;
use ChargeBee_InvalidRequestException;
class SubscriptionController extends Controller
{
    protected $user;
    protected $url;
    private $chargebee_plan_limit = 40;
    public function __construct()
    {
        $this->url = env('PAYPAL_SANDBOX') ? '.sandbox.' : '.';

       // Log::info('PayPal URL');
       // Log::info($this->url);

        //$this->middleware('auth', ['except' => 'handlePaypalIPN']);
        $this->middleware('auth');
    }

    public function getIndex()
    {
        ChargeBee_Environment::configure(getenv('CHARGEBEE_SITE'), getenv('CHARGEBEE_KEY'));
        $all = ChargeBee_Plan::all(array(
            "limit" => $this->chargebee_plan_limit, 
            "status[is]" => "active",
            "periodUnit[is]" => "month",
        ));

        $dataArr  = array();
        $dataArrY = array();
        $dataArrT = array();
        $responseArr = $this->getChargebeeplans($all);
        $dataArr  = $responseArr["dataArr"];
        $dataArrY = $responseArr["dataArrY"];
        // trial account
        $dataArrT = $this->getTrialPlanInformation();
        $invoices = $this->getInvoiceList();
        $user_plan= $this->getCurrentPlan();
        $coupon_ID= $this->getCouponID();
        //return Redirect::route('subscription-change');
        return view('backend.subscription.index', [
            'data' => $dataArr,
            'records_y' => $dataArrY, 
            'data_trial'=> $dataArrT, 
            'invoices'  => $invoices,   
            'user_plan' => $user_plan,     
            'user' => Auth::user(),
            'cb_coupon' => $coupon_ID,
            'cancellation_reasons' => CancellationReason::get()
        ]);
    }

    public function getCouponID() {
        $customerID    = Auth::user()->chargebee_customer_id;
        $couponCode    = Cbinvoice::where('cb_customer_id',$customerID)->orderBy('id', 'DESC')->first();
        return (isset($couponCode->cb_coupon_id) ? $couponCode->cb_coupon_id : "");
    }

    public function getInvoiceList() {
        $userId             = Auth::user()->id;
        $userInvoiceList    = Cbinvoice::where('user_id',$userId)->orderBy('id', 'DESC')->get();
        return $userInvoiceList;
    }

    public function getCurrentPlan() {
        $accountid= Auth::user()->account_id;
        $plan     = Account::where('id', $accountid)->first();
        return      Role::where('name',$plan->name)->first();      
    }

    public function getTrialPlanInformation() {
        $dataArrT = array();
        $roleT    = Role::where('status', '1')->where('is_trial',1)->first();
        if(isset($roleT) && !empty($roleT)) {
            $rolePermissionsT = $roleT->perms()->get();
            $permissionsT     = array_pluck($rolePermissionsT->toArray(), 'name');
            
            $roleLimitT       = RoleLimit::where('role_id', $roleT->id)->first()->toArray();
            $allPermissionsT  = Permission::all();
            $dataT = array_pluck($allPermissionsT->toArray(), 'name');

            foreach ($dataT as $permT) {
                $dataT[$permT] = false;
            }

            foreach ($permissionsT as $permT) {
                $dataT[$permT] = true;
            }
            $dataArrT['role']  = $roleT;
            $dataArrT['role_limits'] = array_merge($roleLimitT, $dataT);
        } else {
            $roleT    = Role::where('name', 'free')->where('status','1')->first();
            $rolePermissionsT = $roleT->perms()->get();
            $permissionsT     = array_pluck($rolePermissionsT->toArray(), 'name');
            
            $roleLimitT       = RoleLimit::where('role_id', $roleT->id)->first()->toArray();
            $allPermissionsT  = Permission::all();
            $dataT = array_pluck($allPermissionsT->toArray(), 'name');

            foreach ($dataT as $permT) {
                $dataT[$permT] = false;
            }

            foreach ($permissionsT as $permT) {
                $dataT[$permT] = true;
            }
            $dataArrT['role']  = $roleT;
            $dataArrT['role_limits'] = array_merge($roleLimitT, $dataT);            
        }
        return $dataArrT;
    }

    public function getChargebeeplans($all) {
        if(isset($all) && !empty($all)) {
            $i = 0;
            $j = 0;
            $dataArr  = array();
            $dataArrY = array();
            foreach ($all as $entry) {
                $ChargeBee_Plan = $entry->plan();
                if(isset($ChargeBee_Plan->periodUnit) && $ChargeBee_Plan->periodUnit == "year" && $j < 4) {
                    $role = Role::where('status', '1')->where('chargebee_plan_id', $ChargeBee_Plan->id)->where('is_trial',0)->first();
                    if(isset($role) && !empty($role)) {
                        $rolePermissions = $role->perms()->get();
                        $permissions = array_pluck($rolePermissions->toArray(), 'name');
                        
                        $roleLimit = RoleLimit::where('role_id', $role->id)->first()->toArray();
                        $allPermissions = Permission::all();
                        $data = array_pluck($allPermissions->toArray(), 'name');

                        foreach ($data as $perm) {
                            $data[$perm] = false;
                        }

                        foreach ($permissions as $perm) {
                            $data[$perm] = true;
                        }
                        $dataArrY[$j]['sorting_order'] = (isset($role->sorting_order) ? $role->sorting_order : '');                        
                        $dataArrY[$j]['chargebee']['id'] = (isset($ChargeBee_Plan->id) ? $ChargeBee_Plan->id : '');
                        $dataArrY[$j]['chargebee']['invoice_name'] = (isset($ChargeBee_Plan->invoice_name) ? $ChargeBee_Plan->invoice_name : '');
                        $dataArrY[$j]['chargebee']['price'] = (isset($ChargeBee_Plan->price) ? $ChargeBee_Plan->price : '');
                        $dataArrY[$j]['chargebee']['period'] = (isset($ChargeBee_Plan->period) ? $ChargeBee_Plan->period : '');
                        $dataArrY[$j]['chargebee']['period_unit'] = (isset($ChargeBee_Plan->period_unit) ? $ChargeBee_Plan->period_unit : '');
                        $dataArrY[$j]['chargebee']['trial_period'] = (isset($ChargeBee_Plan->trial_period) ? $ChargeBee_Plan->trial_period : '');
                        $dataArrY[$j]['chargebee']['trial_period_unit'] = (isset($ChargeBee_Plan->trial_period_unit) ? $ChargeBee_Plan->trial_period_unit : '');
                        $dataArrY[$j]['chargebee']['charge_model'] = (isset($ChargeBee_Plan->charge_model) ? $ChargeBee_Plan->charge_model : '');
                        $dataArrY[$j]['chargebee']['free_quantity'] = (isset($ChargeBee_Plan->free_quantity) ? $ChargeBee_Plan->free_quantity : '');
                        $dataArrY[$j]['chargebee']['status'] = (isset($ChargeBee_Plan->status) ? $ChargeBee_Plan->status : '');
                        $dataArrY[$j]['chargebee']['enabled_in_hosted_pages'] = (isset($ChargeBee_Plan->enabled_in_hosted_pages) ? $ChargeBee_Plan->enabled_in_hosted_pages : '');
                        $dataArrY[$j]['chargebee']['enabled_in_portal'] = (isset($ChargeBee_Plan->enabled_in_portal) ? $ChargeBee_Plan->enabled_in_portal : '');
                        $dataArrY[$j]['chargebee']['taxable'] = (isset($ChargeBee_Plan->taxable) ? $ChargeBee_Plan->taxable : '');
                        $dataArrY[$j]['role'] = $role;
                        $dataArrY[$j]['role_limits'] = array_merge($roleLimit, $data);
                        $j++;
                    }
                } else {
                    if($i < 4) {
                    $role = Role::where('status', '1')->where('chargebee_plan_id', $ChargeBee_Plan->id)->where('is_trial',0)->first();
                    if(isset($role) && !empty($role)) {
                        $rolePermissions = $role->perms()->get();
                        $permissions = array_pluck($rolePermissions->toArray(), 'name');
                        
                        $roleLimit = RoleLimit::where('role_id', $role->id)->first()->toArray();
                        $allPermissions = Permission::all();
                        $data = array_pluck($allPermissions->toArray(), 'name');

                        foreach ($data as $perm) {
                            $data[$perm] = false;
                        }

                        foreach ($permissions as $perm) {
                            $data[$perm] = true;
                        }
                        $dataArr[$i]['sorting_order'] = (isset($role->sorting_order) ? $role->sorting_order : '');
                        $dataArr[$i]['chargebee']['id'] = (isset($ChargeBee_Plan->id) ? $ChargeBee_Plan->id : '');
                        $dataArr[$i]['chargebee']['invoice_name'] = (isset($ChargeBee_Plan->invoice_name) ? $ChargeBee_Plan->invoice_name : '');
                        $dataArr[$i]['chargebee']['price'] = (isset($ChargeBee_Plan->price) ? $ChargeBee_Plan->price : '');
                        $dataArr[$i]['chargebee']['period'] = (isset($ChargeBee_Plan->period) ? $ChargeBee_Plan->period : '');
                        $dataArr[$i]['chargebee']['period_unit'] = (isset($ChargeBee_Plan->period_unit) ? $ChargeBee_Plan->period_unit : '');
                        $dataArr[$i]['chargebee']['trial_period'] = (isset($ChargeBee_Plan->trial_period) ? $ChargeBee_Plan->trial_period : '');
                        $dataArr[$i]['chargebee']['trial_period_unit'] = (isset($ChargeBee_Plan->trial_period_unit) ? $ChargeBee_Plan->trial_period_unit : '');
                        $dataArr[$i]['chargebee']['charge_model'] = (isset($ChargeBee_Plan->charge_model) ? $ChargeBee_Plan->charge_model : '');
                        $dataArr[$i]['chargebee']['free_quantity'] = (isset($ChargeBee_Plan->free_quantity) ? $ChargeBee_Plan->free_quantity : '');
                        $dataArr[$i]['chargebee']['status'] = (isset($ChargeBee_Plan->status) ? $ChargeBee_Plan->status : '');
                        $dataArr[$i]['chargebee']['enabled_in_hosted_pages'] = (isset($ChargeBee_Plan->enabled_in_hosted_pages) ? $ChargeBee_Plan->enabled_in_hosted_pages : '');
                        $dataArr[$i]['chargebee']['enabled_in_portal'] = (isset($ChargeBee_Plan->enabled_in_portal) ? $ChargeBee_Plan->enabled_in_portal : '');
                        $dataArr[$i]['chargebee']['taxable'] = (isset($ChargeBee_Plan->taxable) ? $ChargeBee_Plan->taxable : '');
                        $dataArr[$i]['role'] = $role;
                        $dataArr[$i]['role_limits'] = array_merge($roleLimit, $data);
                        $i++;
                    }
                    }
                }
            }
        }

        array_multisort(array_column($dataArr, 'sorting_order'), SORT_ASC, $dataArr);
        array_multisort(array_column($dataArrY, 'sorting_order'), SORT_ASC, $dataArrY);
        return array("dataArr" => $dataArr, "dataArrY" => $dataArrY);
    }

    public function getUserInvoice() {
        $invoices = $this->getInvoiceList();
        $user_plan= $this->getCurrentPlan();
        return view('backend.subscription.userinvoice', [
            'invoices'  => $invoices,   
            'user_plan' => $user_plan,     
            'user' => Auth::user(),
        ]);
    }

    public function getInvoices()
    {
        $this->user = Auth::user();
        $invoices = $this->user->invoices();

        return View('backend.subscription.invoices', ['invoices' => $invoices]);
    }

    public function getJoin(Request $request)
    {
        //free or trial account condition
        $id   = $request->get('plan');
        $plan = Account::where('name', $id)->where('disabled', 0)->first();
        $roleData = Role::where('name',$id)->first();
        if (!$plan) {
            return redirect()->back();
        }

        if(isset($roleData['is_trial']) && $roleData['is_trial'] == 1) {
            return $this->trialPlanSubscription($request);
        }

        if($id == 'free') {
            return $this->freePlanSubscription($request);
        }
        
        if(!isset($roleData['chargebee_plan_id']) && empty($roleData['chargebee_plan_id'])) {
            return redirect()->back();
        }

        $flag       = $request->input('payment_gateway');
        if($flag == "stripe") {
            return $this->getJoinstoken($request);
        } else if($flag == "paypal") {
            $url = $this->payByPaypal($request);
            return Redirect::to($url);
        }


        ChargeBee_Environment::configure(getenv('CHARGEBEE_SITE'), getenv('CHARGEBEE_KEY'));

        $chargebeeData  = ChargeBee_Plan::retrieve($roleData['chargebee_plan_id']);
        $planInfo       = $chargebeeData->plan();
        //print_r($planInfo->price); exit();

        $price = (isset($planInfo->price) ? number_format(($planInfo->price /100), 2, '.', ' ') : 0);
        $setupCost = (isset($planInfo->setupCost) ? number_format(($planInfo->setupCost /100), 2, '.', ' ') : 0);
        $periodUnit   = (isset($planInfo->periodUnit) ? $planInfo->periodUnit.'ly' : '');
        $TotalCharges = $price + $setupCost;
        $year = Carbon::now()->year;
        $user = Auth::user();
        $user_email = $user->email;
        $first_name = $user->name;
        $screen_name= (isset($roleData['screen_name']) ? $roleData['screen_name'] : '');

        return View('backend.subscription.join', [
            'selectedPlan'  => $id,
            'user_email'    => $user_email,
            'first_name'    => $first_name,
            'amt'           => $price,
            'setupCost'     => $setupCost,
            'TotalCharges'  => $TotalCharges,
            'periodUnit'    => $periodUnit,
            'year'          => $year,
            'screen_name'   => $screen_name,
            'plans' => Account::where('id', '!=', Account::FreeAccountId())
                ->where('disabled', '!=', 1)
                ->get()
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postJoin(Request $request)
    {
        /** @var SubscriptionController $gateway */
        $user    = Auth::user();
        $gateway = $request->gateway;
        $id = $request->get('plan');
        if ($gateway != 'stripe') {
            Session::flash('nosuccess', array('please select payment gateway'));
            return Redirect::route('subscription');
        }

        // cancel plan
        if($user->payment_type == getenv('CHARGEBEE_TYPE') && $user->chargebee_customer_id != '' && $user->chargebee_subscription_id != '' && $user->chargebee_active == 1) {
            $this->cancelChargebeeSubscription(1);
        }

        $plan       = Account::getPlanBySystemId($request->get('plan'));
        //$plan       = Account::getPlanBySystemId('basic');
        $role       = $plan->getRole();
        $plan_type  = 'recurring';
        $planId     = $role->chargebee_plan_id;

        $postData     = $request->input();
        $amt          = (isset($postData['Price']) ? $postData['Price'] : '');
        $card_number  = (isset($postData['card_number']) ? $postData['card_number'] : '');
        $cc_exp_month = (isset($postData['exp_month']) ? $postData['exp_month'] : '');
        $cc_exp_year  = (isset($postData['exp_year']) ? $postData['exp_year'] : '');
        $cvc          = (isset($postData['cvc']) ? $postData['cvc'] : '');
        $phone        = (isset($postData['phone']) ? $postData['phone'] : '');
        $address_line1= (isset($postData['address_line1']) ? $postData['address_line1'] : '');
        $address_zip  = (isset($postData['address_zip']) ? $postData['address_zip'] : '');
        $address_city = (isset($postData['address_city']) ? $postData['address_city'] : '');
        $address_state= (isset($postData['address_state']) ? $postData['address_state'] : '');   
        $address_country= (isset($postData['address_country']) ? $postData['address_country'] : '');

        ####chargebee subscription
        ChargeBee_Environment::configure(getenv('CHARGEBEE_SITE'), getenv('CHARGEBEE_KEY'));
            try {
                $result = ChargeBee_Subscription::create(array(
                    "planId" => $planId, 
                    "customer" => array(
                        "email" => $user->email, 
                        "firstName" => $user->name, 
                        "lastName" => $user->name, 
                        "locale" => $address_city, 
                        "phone" => $phone,
                        "auto_collection"=> "off"
                    ), 
                    "billingAddress" => array(
                        "firstName" => $user->name, 
                        "lastName" => $user->name, 
                        "line1" => $address_line1, 
                        "city" => $address_city, 
                        "state" => $address_state, 
                        "zip" => $address_zip, 
                        "country" => $address_country
                    )
                ));
            } catch (\Exception $e) {
                Log::info(json_encode($e));
                Session::flash('nosuccess', array("failed to purchase " . (isset($role->screen_name) ? $role->screen_name : $plan->description) . " please try again later or contact our support"));
                return Redirect::route('subscription');
            }
                    if(isset($result->customer()->id) && !empty($result->customer()->id)) {
                        $custID = $result->customer()->id;
            try {
                        $result2 = ChargeBee_Card::updateCardForCustomer($custID, array(
                          "gateway"     => "chargebee", 
                          "firstName"   => $user->name, 
                          "lastName"    => $user->name, 
                          "number"      => $card_number, 
                          "expiryMonth" => $cc_exp_month, 
                          "expiryYear"  => $cc_exp_year, 
                          "cvv"     => $cvc));
                        $customer   = $result->customer();
                        $card       = $result->card();
            } catch (\Exception $e) {
                $this->cancelNewSubscription($custID);
                Log::info(json_encode($e));
                Session::flash('nosuccess', array("failed to purchase " . (isset($role->screen_name) ? $role->screen_name : $plan->description) . " please try again later or contact our support"));
                return Redirect::route('subscription');
            }

            $subscriptionArr = $result->subscription();

            $ch_startDate  =  (isset($subscriptionArr->currentTermStart) ? date("Y-m-d",$subscriptionArr->currentTermStart) : '');
            $ch_expiryDate = (isset($subscriptionArr->currentTermEnd) ? date("Y-m-d",$subscriptionArr->currentTermEnd) : '');

            $this->user             = $user;
            $plan_type              = $plan->id;
            $sale_params = [
                'email'  => $this->user->email,
                'first_name' => $this->user->name,
                'amount'     => 100,
                'event_id'   => $result->subscription()->id,
                'tid'  => isset($_COOKIE['_fprom_track']) ? $_COOKIE['_fprom_track'] : '',
                'plan' => 'chargebee'
            ];
            $tdays = (isset($role->trial_days) ? $role->trial_days : 14);
            $user->trial_ends_at       = Carbon::now()->addDay($tdays);
            $user->stripe_active       = 1;
            $user->chargebee_customer_id       = $custID;
            $user->chargebee_subscription_id   = $result->subscription()->id;
            //$user->last_four         = $charge['source']['last4'];
            $user->chargebee_plan      = $role->name;
            $user->chargebee_active    = 1;

            $user->chargebee_subscription_start_date  = $ch_startDate;
            $user->chargebee_subscription_end_date    = $ch_expiryDate;

            $order_id           = 'chargebee subscription';
            $user->account_id   = $plan->id;
            $user->toggle_free  = 0;
            $user->payment_type = getenv('CHARGEBEE_TYPE');
            $user->current_plan_type = $plan_type;
            $user->is_trial     = 0;
            //$user->trial_expire_date = "0000-00-00";
            $user->payment_mode = getenv('PAYMENT_MODE_CHARGEBEE');
            $user->roles()->sync([$role->id]);
            $user->save();

            //trial subcription updates in intercom
            $intercomclient = new IntercomController();
            $intercomclient->updateIntercomUser($user);
        
            /** @var EmailTemplateManagementController $emailTemplate */
            $emailTemplate = App::make(EmailTemplateManagementController::class);
            $template = $emailTemplate->getEmailTemplateByName(EmailTemplate::NEW_SUBSCRIBER_EMAIL_TEMPLATE);
            if (!empty($template)) {
                $body = $template->body;
                $body = str_replace('{USER_NAME}', $this->user->name, $body);
                $body = str_replace('{USER_EMAIL}', $user->email, $body);
                $mailData = ['body' => $body];
                $email = $this->user->email;
                $subject = $template->subject;
                Mail::send('emails.blank', $mailData, function ($message) use ($email, $subject) {
                    $message->to($email)->subject($subject);
                });
            }

            //tell user
            Session::flash('success', array('Thank you. You are now subscribed to ' . (isset($role->screen_name) ? $role->screen_name : $plan->description)));
            Session::flash('ga_track_event', ['plan_type' => 'chargebee', 'plan_name' => $plan->name, 'order_id' => $order_id, 'plan_amount' => intval($amt)]);
            return Redirect::route('subscription');
        } else {
            Session::flash('nosuccess', array("failed to purchase " . (isset($role->screen_name) ? $role->screen_name : $plan->description) . " please try again later or contact our support"));
            return Redirect::route('subscription');
        }
    }

    public function getJoinstoken(Request $request)
    {
        //free or trial account condition
        $id   = $request->get('plan');
        $plan = Account::where('name', $id)->where('disabled', 0)->first();
        $roleData = Role::where('name',$id)->first();
        if (!$plan) {
            return redirect()->back();
        }

        if(isset($roleData['is_trial']) && $roleData['is_trial'] == 1) {
            return $this->trialPlanSubscription($request);
        }

        if($id == 'free') {
            return $this->freePlanSubscription($request);
        }

        if(!isset($roleData['chargebee_plan_id']) && empty($roleData['chargebee_plan_id'])) {
            return redirect()->back();
        }

        ChargeBee_Environment::configure(getenv('CHARGEBEE_SITE'), getenv('CHARGEBEE_KEY'));

        $chargebeeData  = ChargeBee_Plan::retrieve($roleData['chargebee_plan_id']);
        $planInfo       = $chargebeeData->plan();
        //print_r($planInfo->price); exit();

        $price = (isset($planInfo->price) ? number_format(($planInfo->price /100), 2, '.', ' ') : 0);
        $setupCost = (isset($planInfo->setupCost) ? number_format(($planInfo->setupCost /100), 2, '.', ' ') : 0);
        $periodUnit   = (isset($planInfo->periodUnit) ? $planInfo->periodUnit.'ly' : '');
        $TotalCharges = $price;
        $year = Carbon::now()->year;
        $user = Auth::user();
        $user_email = $user->email;
        $first_name = $user->name;
        $screen_name= (isset($roleData['screen_name']) ? $roleData['screen_name'] : '');

        return View('backend.subscription.stripejoin', [
            'selectedPlan'  => $id,
            'user_email'    => $user_email,
            'first_name'    => $first_name,
            'amt'           => $price,
            'setupCost'     => $setupCost,
            'TotalCharges'  => $TotalCharges,
            'periodUnit'    => $periodUnit,
            'year'          => $year,
            'screen_name'   => $screen_name,
            'plans' => Account::where('id', '!=', Account::FreeAccountId())
                ->where('disabled', '!=', 1)
                ->get()
        ]);
    }

    /**
    * @param Request $request
    * @return \Illuminate\Http\RedirectResponse
    */
    public function postJoinstoken(Request $request)
    {
        $postData   = $request->input();
        $stripeToken= $request->get('stripeTmpToken');
        /** @var SubscriptionController $gateway */
        $user    = Auth::user();
        $gateway = $request->gateway;
        $id = $request->get('plan');
        if ($gateway != 'stripe') {
            Session::flash('nosuccess', array('please select payment gateway'));
            return Redirect::route('subscription');
        }

        // cancel plan
        if($user->payment_type == getenv('CHARGEBEE_TYPE') && $user->chargebee_customer_id != '' && $user->chargebee_subscription_id != '' && $user->chargebee_active == 1) {
            //$this->cancelChargebeeSubscription(1);
        }

        $plan       = Account::getPlanBySystemId($request->get('plan'));
        //$plan       = Account::getPlanBySystemId('basic');
        $role       = $plan->getRole();
        $plan_type  = 'recurring';
        $planId     = $role->chargebee_plan_id;

        $postData     = $request->input();
        $amt          = (isset($postData['Price']) ? $postData['Price'] : '');
        $card_number  = (isset($postData['card_number']) ? $postData['card_number'] : '');
        $cc_exp_month = (isset($postData['exp_month']) ? $postData['exp_month'] : '');
        $cc_exp_year  = (isset($postData['exp_year']) ? $postData['exp_year'] : '');
        $cvc          = (isset($postData['cvc']) ? $postData['cvc'] : '');
        //$phone        = (isset($postData['phone']) ? $postData['phone'] : '');
        $address_line1= (isset($postData['address_line1']) ? $postData['address_line1'] : '');
        $address_zip  = (isset($postData['address_zip']) ? $postData['address_zip'] : '');
        $address_city = (isset($postData['address_city']) ? $postData['address_city'] : '');
        $address_state= (isset($postData['address_state']) ? $postData['address_state'] : '');   
        $address_country= (isset($postData['st_country']) ? $postData['st_country'] : '');

        if($postData['coupon_response'] == 1) {
            $couponId = $postData['coupon_code'];
        }

        ####chargebee subscription
        ChargeBee_Environment::configure(getenv('CHARGEBEE_SITE'), getenv('CHARGEBEE_KEY'));

        if($user->payment_type == getenv('CHARGEBEE_TYPE') && $user->chargebee_customer_id != '' && $user->chargebee_subscription_id != '' && $user->chargebee_active == 1) {
                $custID = $user->chargebee_customer_id;
                try {
                    $result2 = ChargeBee_Card::updateCardForCustomer($custID, array(
                        "gateway"   => "stripe",
                        "type"      => "card",
                        "tmp_token" => $stripeToken,
                    ));
                    //$customer   = $result->customer();
                    //$card       = $result->card();
                    //$invoice    = $result->invoice();
                } catch (\Exception $e) {
                    //$this->cancelNewSubscription($custID);
                    Log::info(json_encode($e));
                    Session::flash('nosuccess', array("failed to purchase " . (isset($role->screen_name) ? $role->screen_name : $plan->description) . " please try again later or contact our support"));
                    return Redirect::route('subscription');
                }

            try {
                $result = ChargeBee_Subscription::update($user->chargebee_subscription_id, array(
                    "planId" => $planId,
                    "coupon" => (isset($couponId) ? $couponId : ""), 
                    "customer" => array(
                        "email" => $user->email, 
                        "firstName" => $user->name, 
                        "lastName" => $user->name, 
                        "locale" => $address_city, 
                        "phone" => '',
                        "auto_collection"=> "off"
                    ), 
                    "billingAddress" => array(
                        "firstName" => $user->name, 
                        "lastName" => $user->name, 
                        "line1" => $address_line1, 
                        "city" => $address_city, 
                        "state" => $address_state, 
                        "zip" => $address_zip, 
                        "country" => $address_country
                    ),
                ));
            } catch (\Exception $e) {
                Log::info(json_encode($e));
                Session::flash('nosuccess', array("failed to purchase " . (isset($role->screen_name) ? $role->screen_name : $plan->description) . " please try again later or contact our support"));
                return Redirect::route('subscription');
            }            
        } else {
            try {
                $result = ChargeBee_Subscription::create(array(
                    "planId" => $planId,
                    "coupon" => (isset($couponId) ? $couponId : ""), 
                    "customer" => array(
                        "email" => $user->email, 
                        "firstName" => $user->name, 
                        "lastName" => $user->name, 
                        "locale" => $address_city, 
                        "phone" => '',
                        "auto_collection"=> "off"
                    ), 
                    "billingAddress" => array(
                        "firstName" => $user->name, 
                        "lastName" => $user->name, 
                        "line1" => $address_line1, 
                        "city" => $address_city, 
                        "state" => $address_state, 
                        "zip" => $address_zip, 
                        "country" => $address_country
                    ),

                ));
            } catch (\Exception $e) {
                Log::info(json_encode($e));
                Session::flash('nosuccess', array("failed to purchase " . (isset($role->screen_name) ? $role->screen_name : $plan->description) . " please try again later or contact our support"));
                return Redirect::route('subscription');
            }

            if(isset($result->customer()->id) && !empty($result->customer()->id)) {
                $custID = $result->customer()->id;
                try {
                    $result2 = ChargeBee_Card::updateCardForCustomer($custID, array(
                        "gateway"   => "stripe",
                        "type"      => "card",
                        "tmp_token" => $stripeToken,
                    ));
                    //$customer   = $result->customer();
                    //$card       = $result->card();
                    //$invoice    = $result->invoice();
                } catch (\Exception $e) {
                    $this->cancelNewSubscription($custID);
                    Log::info(json_encode($e));
                    Session::flash('nosuccess', array("failed to purchase " . (isset($role->screen_name) ? $role->screen_name : $plan->description) . " please try again later or contact our support"));
                    return Redirect::route('subscription');
                }
            }            
        }

            if(isset($result->customer()->id) && !empty($result->customer()->id)) {
                $custID = $result->customer()->id;
                $customer   = $result->customer();
                $card       = $result->card();
                $invoice    = $result->invoice();

            $subscriptionArr = $result->subscription();
            $ch_startDate    =  (isset($subscriptionArr->currentTermStart) ? date("Y-m-d",$subscriptionArr->currentTermStart) : '');
            $ch_expiryDate   = (isset($subscriptionArr->currentTermEnd) ? date("Y-m-d",$subscriptionArr->currentTermEnd) : '');
            $last_four       = substr($card_number, -4);
            $this->user      = $user;
            $plan_type       = $plan->id;

            $tdays = (isset($role->trial_days) ? $role->trial_days : 14);
            $user->trial_ends_at       = Carbon::now()->addDay($tdays);
            $user->stripe_active       = 1;
            $user->chargebee_customer_id       = $custID;
            $user->chargebee_subscription_id   = $result->subscription()->id;
            $user->last_four           = $last_four;
            $user->chargebee_plan      = $role->name;
            $user->chargebee_active    = 1;

            $user->chargebee_subscription_start_date  = $ch_startDate;
            $user->chargebee_subscription_end_date    = $ch_expiryDate;

            $order_id           = 'chargebee subscription';
            $user->account_id   = $plan->id;
            $user->toggle_free  = 0;
            $user->payment_type = getenv('CHARGEBEE_TYPE');
            $user->current_plan_type = $plan_type;
            $user->is_trial     = 0;
            //$user->trial_expire_date = "0000-00-00";
            $user->payment_mode = getenv('PAYMENT_MODE_STRIPE');
            $user->roles()->sync([$role->id]);
            $user->save();

            if(isset($invoice->id) && !empty($invoice->id)) {
                $resultIN     = ChargeBee_Invoice::retrieve($invoice->id);
                $cbinvoice    = $resultIN->invoice();

                $cb_invoice   = array();
                $cb_invoice['invoice_id'] = $invoice->id;
                $cb_invoice['status']     = $cbinvoice->status;
                $cb_invoice['amount']     = (isset($cbinvoice->amountPaid) ? $cbinvoice->amountPaid : 0);
                $cb_invoice['amount_due'] = (isset($cbinvoice->amountDue) ? $cbinvoice->amountDue : 0);
                $cb_invoice['trans_id']   = "";
                $cb_invoice['role_id']    = (isset($role->id) ? $role->id : 0);
                
                $cb_invoice['cb_coupon_id']    = (isset($cbinvoice->discounts[0]->entityId) ? $cbinvoice->discounts[0]->entityId : "");
                $cb_invoice['cb_coupon_amount']  = (isset($cbinvoice->discounts[0]->amount) ? $cbinvoice->discounts[0]->amount : "");
                $cb_invoice['cb_coupon_type']    = (isset($cbinvoice->discounts[0]->object) ? $cbinvoice->discounts[0]->object : "");
                $this->saveInvoiceData($user, $cb_invoice);
            }

            if(isset($cb_invoice['amount']) && $cb_invoice['amount'] > 0) {
                $fp_amt = number_format(($cb_invoice['amount'] /100), 2, '.', ' ');
            } else {
                $subscriptionFP = $result->subscription();
                $fp_sub_amt = (isset($subscriptionFP->planUnitPrice) ? $subscriptionFP->planUnitPrice : 0);
                if($fp_sub_amt > 0) {
                    $fp_amt = number_format(($fp_sub_amt /100), 2, '.', ' ');
                } else {
                    $fp_amt = 10;
                }
            }

            //trial subcription updates in intercom
            $intercomclient = new IntercomController();
            $intercomclient->updateIntercomUser($user);

            $sale_params = [
                'email'      => (isset($user->email) ? $user->email : ''),
                'first_name' => (isset($user->name) ? $user->name : ''),
                'event_id'   => (isset($user->chargebee_subscription_id) ? $user->chargebee_subscription_id : ''),
                'amount'     => $fp_amt,
                'tid'        => (isset($_COOKIE['_fprom_track']) ? $_COOKIE['_fprom_track'] : ''),
                'plan'       => $planId
            ];

            $paramsFP = json_encode($sale_params);
            $ch     = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://firstpromoter.com/api/v1/track/sale?api_key=' . env('FP_API_KEY') . '&wid=' . env('FP_WTID') . '');
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $paramsFP);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json; charset=utf-8',
                    'Content-Length: ' . strlen(($paramsFP)))
            );
            $output = curl_exec($ch);
            $result = json_encode($output);

            /** @var EmailTemplateManagementController $emailTemplate */
            $emailTemplate = App::make(EmailTemplateManagementController::class);
            $template = $emailTemplate->getEmailTemplateByName(EmailTemplate::NEW_SUBSCRIBER_EMAIL_TEMPLATE);
            if (!empty($template)) {
                $body = $template->body;
                $body = str_replace('{USER_NAME}', $this->user->name, $body);
                $body = str_replace('{USER_EMAIL}', $user->email, $body);
                $mailData = ['body' => $body];
                $email = $this->user->email;
                $subject = $template->subject;
                Mail::send('emails.blank', $mailData, function ($message) use ($email, $subject) {
                    $message->to($email)->subject($subject);
                });
            }

            //tell user
            Session::flash('success', array('Thank you. You are now subscribed to ' . (isset($role->screen_name) ? $role->screen_name : $plan->description)));
            Session::flash('ga_track_event', ['plan_type' => 'chargebee', 'plan_name' => $plan->name, 'order_id' => $order_id, 'plan_amount' => intval($amt)]);
            return Redirect::route('subscription');
        } else {
            Session::flash('nosuccess', array("failed to purchase " . (isset($role->screen_name) ? $role->screen_name : $plan->description) . " please try again later or contact our support"));
            return Redirect::route('subscription');
        }
    }

    /**
    * @param $post
    */
    public function payByStripe()
    {
        $plan = Account::getPlanBySystemId(Input::get('plan'));
        $this->user = Auth::user();
        $this->user->subscription($plan->stripe_plan)
            ->create(Input::get('token'), [
                'email' => $this->user->email//,
                //'description' => $_POST['pap_cookie'] // Post_Affiliate_Pro /var/www/kr-project-cw/vendor/laravel/cashier/src/Laravel/Cashier/StripeGateway.php
            ]);
    }

    public function saveInvoiceData($user,$invoice) {
        $CBInvoice = new CBInvoice();
        $CBInvoice->user_id           = (isset($user->id) ? $user->id : 0);
        $CBInvoice->user_plan_id      = (isset($invoice['role_id']) ? $invoice['role_id'] : 0);
        $CBInvoice->cb_subscription_id= (isset($user->chargebee_subscription_id) ? $user->chargebee_subscription_id : 0);
        $CBInvoice->cb_customer_id    = (isset($user->chargebee_customer_id) ? $user->chargebee_customer_id : '');
        $CBInvoice->cb_transaction_id = (isset($invoice['trans_id']) ? $invoice['trans_id'] : '');
        $CBInvoice->cb_invoice_id     = (isset($invoice['invoice_id']) ? $invoice['invoice_id'] : 0);
        $CBInvoice->amount            = (isset($invoice['amount']) ? number_format(($invoice['amount'] /100), 2, '.', ' ') : 0);
        $CBInvoice->amount_due        = (isset($invoice['amount_due']) ? number_format(($invoice['amount_due'] /100), 2, '.', ' ') : 0);
        $CBInvoice->cb_invoice_status = (isset($invoice['status']) ? $invoice['status'] : 0);
        $CBInvoice->date_created      = Carbon::now();
        $CBInvoice->start_date        = (isset($user->chargebee_subscription_start_date) ? $user->chargebee_subscription_start_date : 0);
        $CBInvoice->end_date          = (isset($user->chargebee_subscription_end_date) ? $user->chargebee_subscription_end_date : 0);
        if(isset($invoice['cb_coupon_amount']) && $invoice['cb_coupon_amount'] > 0) {
        $CBInvoice->cb_coupon_id    = (isset($invoice['cb_coupon_id']) ? $invoice['cb_coupon_id'] : '');
        $CBInvoice->cb_coupon_amount= (isset($invoice['cb_coupon_amount']) ? number_format(($invoice['cb_coupon_amount'] /100), 2, '.', ' ') : '');
        $CBInvoice->cb_coupon_type  = (isset($invoice['cb_coupon_type']) ? $invoice['cb_coupon_type'] : '');
        }   
        return $CBInvoice->save();     
    }

    public function payByPaypal($request) {
        $inputs  = $request->all();
        $roleData= Role::where('name', $inputs['plan'])->first();
        ChargeBee_Environment::configure(getenv('CHARGEBEE_SITE'), getenv('CHARGEBEE_KEY'));
        $result  = ChargeBee_Plan::retrieve($roleData['chargebee_plan_id']);
        $cb_plan        = $result->plan();   
        $cb_plan_price  = (isset($cb_plan->price) ? number_format(($cb_plan->price /100), 2, '.', ' ') : '');
        $amt = 25;
        $form_fields = array(
            'USER' => env('PAYPAL_USER'),
            'PWD' => env('PAYPAL_USER_PWD'),
            'SIGNATURE' => env('PAYPAL_SIGNATURE'),
            'VERSION' => '86',
            'METHOD' => 'SetExpressCheckout',
            'PAYMENTREQUEST_0_PAYMENTACTION' => 'AUTHORIZATION',
            'PAYMENTREQUEST_0_AMT' => $cb_plan_price,
            'PAYMENTREQUEST_0_CURRENCYCODE' => 'USD',
            'L_BILLINGTYPE0' => 'MerchantInitiatedBilling',
            'L_BILLINGAGREEMENTDESCRIPTION0' => 'ClubUsage',
            'RETURNURL' => url('/auth/subscription/success'),
            'CANCELURL' => url('/auth/subscription/cancel'),
        );       

        $postACT = "";
        foreach ($form_fields as $key => $value) {
            $postACT .= "&" . $key . "=" . urlencode(html_entity_decode($value, ENT_QUOTES));
        }
        $tokenUrl = 'https://api-3t'. $this->url .'paypal.com/nvp';
        Log::info('PayPal Token URL');
        Log::info($tokenUrl);
        Log::info($postACT);

        $reply = $this->curlCall($tokenUrl, $postACT);
        parse_str($reply, $status);
        
        Log::info('PayPal Token Response');
        Log::info($reply);
        Log::info($status);
        if (!isset($status['TOKEN'])) {
            //return redirect()->back()->withErrors('failed to connect with paypal. Time Out');
            Session::flash('nosuccess', array("failed to connect with paypal. Time Out"));            
            return '/auth/profile';
        }

        if (isset($status['TOKEN'])) {
            $payment = Payment::where('token', $status['TOKEN'])->first();
            if (empty($payment)) {
                $payment = new Payment();
            }
            $payment->customer_id   = Auth::user()->id;
            $payment->token         = $status['TOKEN'];
            $payment->amount        = $cb_plan_price;
            $payment->payment_date  = Carbon::now();
            $payment->start_date    = Carbon::now();
            $payment->payment_type  = Payment::PAYMENT_TYPE_PAYPAL;
            $payment->plan_type     = $inputs['plan'];
            $payment->save();
        }

        //https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token=InsertTokenHere
        $url = 'https://www'. $this->url .'paypal.com/cgi-bin/webscr?cmd=_express-checkout&token=' . $status['TOKEN'];
        return $url;
    }

    /**
     * @param $id
     * @return Redirect
     */
    /*
    public function payByPaypal($id)
    {
        $plan = Account::getPlanBySystemId($id);
        //$status = $this->paypalPaymentProcess($id, $papCookie); // /Post Affiliate Pro integration snippet - added $papCookie

         $status = $this->paypalPaymentProcess($id); // Post_Affiliate_Pro integration snippet - removed $papCookie

        if (!isset($status['TOKEN'])) {
            return redirect()->back()->withErrors('failed to connect with paypal. Time Out');
        }

        $amt = $plan->getPrice();
        $itemName = urlencode($plan->paypal_plan);
        $itemNumber = urlencode(strtoupper('KR-' . $plan->name));

       $url = 'https://www' . $this->url . 'paypal.com/cgi-BIN/webscr?cmd=_express-checkout&token=' . $status['TOKEN'] . '&business=' . env('PAYPAL_BUSINESS_ID') . '&lc=CA&item_name=' . $itemName . '&item_number=' . $itemNumber . '&no_note=1&no_shipping=1&rm=1&return_url=' . url('/subscription-paypal-success') . '&cancel_url=' . env('PAYPAL_CANCEL_URL') .
            '&notify_url=' . url('/subscription-paypal-success') . // '?pap_custom='.$papCookie. Post_Affiliate_Pro
            '&src=1&a3=' . $amt . '&p3=1&t3=M&currency_code=USD&bn=PP%2dSubscriptionsBF%3abtn_subscribeCC_LG%2egif%3aNonHosted&custom=' . Auth::user()->id;

        Log::info('PayPal Redirect URL');
        Log::info($url);

        return redirect($url);
    }
    */
    //public function paypalPaymentProcess($id, $custom = '') /* Post Affiliate Pro integration snippet - added $custom */
    public function paypalPaymentProcess($id) /* Post_Affiliate_Pro integration snippet - removed $papCookie */
    {
        $plan = Account::getPlanBySystemId($id);

        if (!$plan) {
            return redirect()
                ->back()
                ->withErrors('please select proper subscription plan');
        }

        $amt = $plan->getPrice();

        $form_fields = array(
            'USER' => env('PAYPAL_USER'),
            'PWD' => env('PAYPAL_USER_PWD'),
            'SIGNATURE' => env('PAYPAL_SIGNATURE'),
            'VERSION' => '95',
            'METHOD' => 'SetExpressCheckout',
            'PAYMENTREQUEST_0_PAYMENTACTION' => 'SALE',
            'PAYMENTREQUEST_0_AMT' => $amt,
            'L_BILLINGAGREEMENTDESCRIPTION0' => $plan->paypal_plan,
            'PAYMENTREQUEST_0_CURRENCYCODE' => 'USD',
            'RETURNURL' => url('/auth/subscription/success'), // SubscriptionController@getSuccess
            'CANCELURL' => url('/auth/subscription/cancel') // SubscriptionController@cancelUrl
        );


        /*
        * Check if it's a lifetime plan or not
        * if not lifetime plan then it's recurring
        */
        if (!$plan->lifetime) {
            $form_fields['L_BILLINGTYPE0'] = 'RecurringPayments';
        }

        $plan = $plan->paypal_plan;

        /* Post_Affiliate_Pro integration snippet 
        if ($custom != '') {
            $form_fields['RETURNURL'] = url('/auth/subscription/success').'?pap_custom='.$custom;
        }
        */
        $postACT = "";
        foreach ($form_fields as $key => $value) {
            $postACT .= "&" . $key . "=" . urlencode(html_entity_decode($value, ENT_QUOTES));
        }

        $tokenUrl = 'https://api-3t' . $this->url . 'paypal.com/nvp';
        Log::info('PayPal Token URL');
        Log::info($tokenUrl);
        Log::info($postACT);

        $reply = $this->curlCall($tokenUrl, $postACT);
        parse_str($reply, $status);

        Log::info('PayPal Token Response');
        Log::info($reply);
        Log::info($status);

        if (isset($status['TOKEN'])) {
            $payment = Payment::where('token', $status['TOKEN'])->first();
            if (empty($payment)) {
                $payment = new Payment();
            }
            $payment->customer_id = Auth::user()->id;
            $payment->token = $status['TOKEN'];
            $payment->amount = $amt;
            $payment->payment_date = Carbon::now();
            $payment->start_date = Carbon::now();
            $payment->payment_type = Payment::PAYMENT_TYPE_PAYPAL;
            $payment->plan_type = $plan;
            $payment->save();
        }
        return $status;
    }

    public function getSuccess(Request $request) {
        $inputs  = $request->all();
        $payment = Payment::where('token', $inputs['token'])->first();
        $roleData= Role::where('name', $payment['plan_type'])->first();
        if (isset($payment) && !empty($payment)) {
            $form_fields = array(
                'USER'      => env('PAYPAL_USER'),
                'PWD'       => env('PAYPAL_USER_PWD'),
                'SIGNATURE' => env('PAYPAL_SIGNATURE'),
                'VERSION'   => '86',
                'METHOD'    => 'CreateBillingAgreement',
                'TOKEN'     => $inputs['token'],
            );

            $postACT = "";
            foreach ($form_fields as $key => $value) {
                $postACT .= "&" . $key . "=" . urlencode(html_entity_decode($value, ENT_QUOTES));
            }

            $reply  = $this->curlCall('https://api-3t'. $this->url .'paypal.com/nvp', $postACT);
            parse_str($reply, $status);
            if ($status['ACK'] == 'Success') {
                $payment->profile_id     = $status['BILLINGAGREEMENTID'];;
                $payment->payment_status = 'paid';
                $payment->token          = $inputs['token'];
                $payment->payment_data   = $reply;
                $payment->save();

                $sessArr   = array();
                $sessArr['cb_coupon'] = $request->session()->get('cb_coupon');
                $sessArr['cb_plan']   = $request->session()->get('cb_plan');

                $request->session()->put('cb_coupon', '');
                $request->session()->put('cb_plan', '');

                $data = $this->chargebeeSubscriptionWithBAID($status,$roleData,$payment,$sessArr);
               return Redirect::route('subscription');
            }
            Session::flash('nosuccess', array("failed to purchase " . (isset($roleData->screen_name) ? $roleData->screen_name : "--") . " please try again later or contact our support"));            
            return redirect('/auth/profile');
        }
    }

    public function chargebeeSubscriptionWithBAID($BAID,$roleData,$payment,$sessArr) {
        $user    = Auth::user();
        $id      = $roleData['name'];
        //check if already subscription is valid or not
        //cancel plan
        if($user->payment_type == getenv('CHARGEBEE_TYPE') && $user->chargebee_customer_id != '' && $user->chargebee_subscription_id != '' && $user->chargebee_active == 1) {
            //$this->cancelChargebeeSubscription(1);
        }

        $plan         = Account::getPlanBySystemId($id);
        $role         = $plan->getRole();
        $plan_type    = 'recurring';
        $planId       = $role->chargebee_plan_id;

        //$postData     = $request->input();
        $amt          = (isset($payment['amount']) ? $payment['amount'] : '');
        $phone        = (isset($postData['phone']) ? $postData['phone'] : '');
        $address_line1= (isset($postData['address_line1']) ? $postData['address_line1'] : '');
        $address_zip  = (isset($postData['address_zip']) ? $postData['address_zip'] : '');
        $address_city = (isset($postData['address_city']) ? $postData['address_city'] : '');
        $address_state= (isset($postData['address_state']) ? $postData['address_state'] : '');   
        $address_country= (isset($postData['address_country']) ? $postData['address_country'] : '');

        $cb_coupon = $sessArr['cb_coupon'];
        $cb_plan   = $sessArr['cb_plan'];

        if($cb_coupon != "") {
            $couponId = $cb_coupon;
        }

        ####chargebee subscription
        ChargeBee_Environment::configure(getenv('CHARGEBEE_SITE'), getenv('CHARGEBEE_KEY'));
        if($user->payment_type == getenv('CHARGEBEE_TYPE') && $user->chargebee_customer_id != '' && $user->chargebee_subscription_id != '' && $user->chargebee_active == 1) {
            $custID = $user->chargebee_customer_id;
            try {
                $result2 = ChargeBee_Customer::updatePaymentMethod($custID, array(
                        "payment_method" => array(
                        "type" => "paypal_express_checkout", 
                        "reference_id" => $BAID['BILLINGAGREEMENTID'],
                    )));
                //$invoice    = $result->invoice();
            } catch (\Exception $e) {
                //$this->cancelNewSubscription($custID);
                Log::info(json_encode($e));
                Session::flash('nosuccess', array("failed to purchase " . (isset($role->screen_name) ? $role->screen_name : $plan->description) . " please try again later or contact our support"));
                return Redirect::route('subscription');
            }

            try {
                $result = ChargeBee_Subscription::update($user->chargebee_subscription_id, array(
                    "planId" => $planId,
                    "coupon" => (isset($couponId) ? $couponId : ""), 
                    "customer" => array(
                        "email" => $user->email, 
                        "firstName" => $user->name, 
                        "lastName" => $user->name, 
                        "locale" => $address_city, 
                        "phone" => $phone,
                        "auto_collection"=> "off"
                    ), 
                    "billingAddress" => array(
                        "firstName" => $user->name, 
                        "lastName" => $user->name, 
                        "line1" => $address_line1, 
                        "city" => $address_city, 
                        "state" => $address_state, 
                        "zip" => $address_zip, 
                        "country" => $address_country
                    ),
                ));
            } catch (\Exception $e) {
                Log::info(json_encode($e));
                Session::flash('nosuccess', array("failed to purchase " . (isset($role->screen_name) ? $role->screen_name : $plan->description) . " please try again later or contact our support"));
                return Redirect::route('subscription');
            }
        } else {
            try {
                $result = ChargeBee_Subscription::create(array(
                    "planId" => $planId,
                    "coupon" => (isset($couponId) ? $couponId : ""), 
                    "customer" => array(
                        "email" => $user->email, 
                        "firstName" => $user->name, 
                        "lastName" => $user->name, 
                        "locale" => $address_city, 
                        "phone" => $phone,
                        "auto_collection"=> "off"
                    ), 
                    "billingAddress" => array(
                        "firstName" => $user->name, 
                        "lastName" => $user->name, 
                        "line1" => $address_line1, 
                        "city" => $address_city, 
                        "state" => $address_state, 
                        "zip" => $address_zip, 
                        "country" => $address_country
                    ),
                ));
            } catch (\Exception $e) {
                Log::info(json_encode($e));
                Session::flash('nosuccess', array("failed to purchase " . (isset($role->screen_name) ? $role->screen_name : $plan->description) . " please try again later or contact our support"));
                return Redirect::route('subscription');
            }

            if(isset($result->customer()->id) && !empty($result->customer()->id)) {
                $custID = $result->customer()->id;
                try {
                    $result2 = ChargeBee_Customer::updatePaymentMethod($custID, array(
                            "payment_method" => array(
                            "type" => "paypal_express_checkout", 
                            "reference_id" => $BAID['BILLINGAGREEMENTID'],
                        )));
                    //$invoice    = $result->invoice();
                } catch (\Exception $e) {
                    $this->cancelNewSubscription($custID);
                    Log::info(json_encode($e));
                    Session::flash('nosuccess', array("failed to purchase " . (isset($role->screen_name) ? $role->screen_name : $plan->description) . " please try again later or contact our support"));
                    return Redirect::route('subscription');
                }
            }
        }
            if(isset($result->customer()->id) && !empty($result->customer()->id)) {
            $custID = $result->customer()->id;
            $invoice    = $result->invoice();
            $subscriptionArr = $result->subscription();
            $ch_startDate  =  (isset($subscriptionArr->currentTermStart) ? date("Y-m-d",$subscriptionArr->currentTermStart) : '');
            $ch_expiryDate = (isset($subscriptionArr->currentTermEnd) ? date("Y-m-d",$subscriptionArr->currentTermEnd) : '');

            $this->user             = $user;
            $plan_type              = $plan->id;
            
            $tdays = (isset($role->trial_days) ? $role->trial_days : 14);
            $user->trial_ends_at       = Carbon::now()->addDay($tdays);
            $user->stripe_active       = 1;
            $user->chargebee_customer_id       = $custID;
            $user->chargebee_subscription_id   = $result->subscription()->id;
            //$user->last_four         = $charge['source']['last4'];
            $user->chargebee_plan      = $role->name;
            $user->chargebee_active    = 1;

            $user->chargebee_subscription_start_date  = $ch_startDate;
            $user->chargebee_subscription_end_date    = $ch_expiryDate;

            $order_id           = 'chargebee subscription';
            $user->account_id   = $plan->id;
            $user->toggle_free  = 0;
            $user->payment_type = getenv('CHARGEBEE_TYPE');
            $user->current_plan_type = $plan_type;
            $user->is_trial     = 0;
            //$user->trial_expire_date = "0000-00-00";
            $user->payment_mode = getenv('PAYMENT_MODE_PAYPAL');
            $profile_id = (isset($BAID['PROFILEID']) ? $BAID['PROFILEID'] : '');
            $user->stripe_id           = $profile_id;
            $user->stripe_subscription = $profile_id;
            $user->pending_payment  = 0;
            $user->campaign         = (isset($roleData->name) ? $roleData->name : '');
            $user->roles()->sync([$role->id]);
            $user->save();

            if(isset($invoice->id) && !empty($invoice->id)) {
                $resultIN   = ChargeBee_Invoice::retrieve($invoice->id);
                $cbinvoice  = $resultIN->invoice();
                $cb_invoice = array();
                $cb_invoice['invoice_id'] = $invoice->id;
                $cb_invoice['status']     = $cbinvoice->status;
                $cb_invoice['amount']     = (isset($cbinvoice->amountPaid) ? $cbinvoice->amountPaid : 0);
                $cb_invoice['amount_due'] = (isset($cbinvoice->amountDue) ? $cbinvoice->amountDue : 0);
                $cb_invoice['trans_id']   = "";
                $cb_invoice['role_id']    = (isset($role->id) ? $role->id : 0);

                $cb_invoice['cb_coupon_id']    = (isset($cbinvoice->discounts[0]->entityId) ? $cbinvoice->discounts[0]->entityId : "");
                $cb_invoice['cb_coupon_amount']= (isset($cbinvoice->discounts[0]->amount) ? $cbinvoice->discounts[0]->amount : "");
                $cb_invoice['cb_coupon_type']  = (isset($cbinvoice->discounts[0]->object) ? $cbinvoice->discounts[0]->object : "");
                $this->saveInvoiceData($user, $cb_invoice);
            }

            //trial subcription updates in intercom
            $intercomclient = new IntercomController();
            $intercomclient->updateIntercomUser($user);

            /** @var EmailTemplateManagementController $emailTemplate */
            $emailTemplate = App::make(EmailTemplateManagementController::class);
            $template = $emailTemplate->getEmailTemplateByName(EmailTemplate::NEW_SUBSCRIBER_EMAIL_TEMPLATE);
            if (!empty($template)) {
                $body = $template->body;
                $body = str_replace('{USER_NAME}', $this->user->name, $body);
                $body = str_replace('{USER_EMAIL}', $user->email, $body);
                $mailData = ['body' => $body];
                $email = $this->user->email;
                $subject = $template->subject;
                Mail::send('emails.blank', $mailData, function ($message) use ($email, $subject) {
                    $message->to($email)->subject($subject);
                });
            }

            if(isset($cb_invoice['amount']) && $cb_invoice['amount'] > 0) {
                $fp_amt = number_format(($cb_invoice['amount'] /100), 2, '.', ' ');
            } else {
                $subscriptionFP = $result->subscription();
                $fp_sub_amt = (isset($subscriptionFP->planUnitPrice) ? $subscriptionFP->planUnitPrice : 0);
                if($fp_sub_amt > 0) {
                    $fp_amt = number_format(($fp_sub_amt /100), 2, '.', ' ');
                } else {
                    $fp_amt = 10;
                }
            }

            $sale_params = [
                'email'      => (isset($user->email) ? $user->email : ''),
                'first_name' => (isset($user->name) ? $user->name : ''),
                'event_id'   => (isset($user->chargebee_subscription_id) ? $user->chargebee_subscription_id : ''),
                'amount'     => $fp_amt,
                'tid'        => (isset($_COOKIE['_fprom_track']) ? $_COOKIE['_fprom_track'] : ''),
                'plan'       => $planId
            ];

            $paramsFP = json_encode($sale_params);
            $ch     = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://firstpromoter.com/api/v1/track/sale?api_key=' . env('FP_API_KEY') . '&wid=' . env('FP_WTID') . '');
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $paramsFP);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json; charset=utf-8',
                    'Content-Length: ' . strlen(($paramsFP)))
            );
            $output = curl_exec($ch);
            $result = json_encode($output);

            //tell user
            Session::flash('success', array('Thank you. You are now subscribed to ' . (isset($roleData->screen_name) ? $roleData->screen_name : $plan->description)));
                 Redirect::route('subscription');
        } else {
            Session::flash('nosuccess', array("failed to purchase " . (isset($roleData->screen_name) ? $roleData->screen_name : $plan->description) . " please try again later or contact our support"));
             Redirect::route('subscription');
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    /*
    public function getSuccess(Request $request)
    {
        $inputs = $request->all();
        $payment = Payment::where('token', $inputs['token'])->first();
        $plan = Account::getByPaypalPlanName($payment->plan_type);
        $days = $payment->remain_usage;
        $day = Carbon::now('UTC')->addDay($days)->toDateTimeString();

        $user = User::where('id', $payment->customer_id)->first();
        $month = $plan->getPeriod();
        $freq = $plan->getFrequency();

        if (isset($payment) && !empty($payment)) {
            $form_fields = array(
                'USER' => env('PAYPAL_USER'),
                'PWD' => env('PAYPAL_USER_PWD'),
                'SIGNATURE' => env('PAYPAL_SIGNATURE'),
                'VERSION' => '95',
                'TOKEN' => $inputs['token'],
                'PAYERID' => $inputs['PayerID'],
                'PROFILESTARTDATE' => $day,
                'PAYMENTREQUEST_0_PAYMENTACTION' => 'SALE',
                'DESC' => $payment->plan_type,    #Profile description - same as billing agreement description
                'AMT' => $payment->amount,    #The amount the buyer will pay in a payment period
                'CURRENCYCODE' => 'USD',    #The currency, e.g. US dollars
                'COUNTRYCODE' => 'US',    #The country code, e.g. US
                'MAXFAILEDPAYMENTS' => 3,
            );


             //check if the plan is a one time payment or a lifetime plan

            if ($plan->lifetime) {
                $form_fields['METHOD'] = 'DoExpressCheckoutPayment';
            } else {
                $form_fields['METHOD'] = 'CreateRecurringPaymentsProfile';
                $form_fields['BILLINGPERIOD'] = ucfirst(strtolower($month));    #Period of time between billings
                $form_fields['BILLINGFREQUENCY'] = (int)$freq;    #Frequency of charges
            }

            $postACT = "";
            foreach ($form_fields as $key => $value) {
                $postACT .= "&" . $key . "=" . urlencode(html_entity_decode($value, ENT_QUOTES));
            }

            $reply = $this->curlCall('https://api-3t' . $this->url . 'paypal.com/nvp', $postACT);

            parse_str($reply, $status);

            if ($status['ACK'] == 'Success') {
                $profile_id = isset($status['PROFILEID']) ? $status['PROFILEID'] : $status['TRANSACTIONID'];

                $user->stripe_id = $inputs['PayerID'];
                $user->stripe_subscription = $profile_id;

                // Update plan even before paypal payment is confirmed ========
                $user->pending_payment = 1;
                $user->payment_type = 'paypal';
                $user->stripe_active = 1;
                $user->stripe_plan = $payment->plan_type;
                $user->account_id = $plan->id;
                $user->campaign = $plan->name;

                if ($plan->lifetime == 1) {
                    $user->current_plan_type = 'lifetime';
                    $user->trial_ends_at = Carbon::now()->addDay(14);
                    $user->save();
                } else {
                    $user->current_plan_type = 'recurring';
                    $user->save();
                }

//                $user->save();

                // ============================================================


                $payment->profile_id = $profile_id;
                $payment->payment_status = 'pending';
                $payment->token = $inputs['token'];
                $payment->payment_data = $reply;

                $payment->save();


                Session::flash('ga_track_event', ['plan_type' => 'paypal', 'plan_name' => $payment->plan_type, 'order_id' => $profile_id, 'plan_amount' => intval($payment->amount)]);


                // firstpromoter track/sale api
                $array_monthly = ['new_basic_monthly', 'new_pro_monthly', 'new_elite_monthly'];
                $array_yearly = ['new_basic_yearly', 'new_pro_yearly', 'new_elite_yearly'];


                if ($plan->lifetime) {
                    $sale_params = [
                        'email' => $user->email,
                        'first_name' => $user->name,
                        'event_id' => $user->stripe_subscription,
                        'amount' => ($payment->amount * 100),
                        'tid' => isset($_COOKIE['_fprom_track']) ? $_COOKIE['_fprom_track'] : '',
                        'plan' => $plan->paypal_plan
                    ];
                } else {
                    if (in_array($plan->name, $array_monthly)) {
                        $sale_params = [
                            'email' => $user->email,
                            'first_name' => $user->name,
                            'amount' => ($payment->amount * 100),
                            'event_id' => $user->stripe_subscription,
                            'mrr' => ($payment->amount * 100),
                            'tid' => isset($_COOKIE['_fprom_track']) ? $_COOKIE['_fprom_track'] : '',
                            'plan' => $plan->paypal_plan
                        ];
                    } else if (in_array($plan->name, $array_yearly)) {
                        $sale_params = [
                            'email' => $user->email,
                            'first_name' => $user->name,
                            'event_id' => $user->stripe_subscription,
                            'amount' => ($payment->amount * 100),
                            'mrr' => ($payment->amount * 100 / 12),
                            'tid' => isset($_COOKIE['_fprom_track']) ? $_COOKIE['_fprom_track'] : '',
                            'plan' => $plan->paypal_plan
                        ];
                    } else {

                    }

                }

                $params = json_encode($sale_params);

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, 'https://firstpromoter.com/api/v1/track/sale?api_key=' . env('FP_API_KEY') . '&wid=' . env('FP_WTID') . '');
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                        'Content-Type: application/json; charset=utf-8',
                        'Content-Length: ' . strlen(($params)))
                );

                $output = curl_exec($ch);
                $result = json_encode($output);

            }

            if (!empty($days)) {
                Session::flash('success', [
                    'Your subscription has been changed. Now ' . $payment->plan_type .
                    'Plan activated in your account. Your next Payment date is ' . Carbon::now()->addDay($days)
                ]);

                // $user->subscription_ends_at = Carbon::now()->addDay($days);
                // $user->save();

                return Redirect::route('subscription');
            }

            return redirect('/auth/profile');
        }
    }
    */
    public function cancelUrl()
    {
        return View('backend.subscription.success');
    }

    public function calculateRemainUsage($id)
    {
        $plan = Account::getPlanBySystemId($id);
        $user = Auth::user();
        $payment = Payment::where('profile_id', $user->stripe_subscription)->where('payment_date', '!=', 'Recurring')->first();
        $amt = $payment->amount;
        $endDate = $user->subscription_ends_at ?: Carbon::now();
        $daysOfMonth = Carbon::now()->daysInMonth;
        $difference = $endDate->diff(Carbon::now())->days;
        $perCurrentday = $amt / $daysOfMonth;
        $remainUsage = $difference * $perCurrentday;
        $newPerDay = $plan->getPrice() / $daysOfMonth;
        $trail = $remainUsage / $newPerDay;
        $newDay = round($trail);
        $payment->remain_usage = $newDay;
        $payment->save();

        $user->subscription_ends_at = Carbon::now()->addDay($newDay);
        $user->save();

        return $newDay;
    }

    public function changeToPlan(User $user, $planName)
    {
        $plan = Account::getPlanBySystemId($planName);
        $role = $plan->getRole();
        if ($planName === 'free') {
            $user->subscription()->cancel();
            $this->changeToFree();
        } else {
            $user->subscription($plan->stripe_plan)->swap();

            $user->account_id = $plan->id;
            $user->toggle_free = 0;

            $user->save();

            $proRole = Role::where('name', 'pro')->first();
            if (!empty($proRole)) {
                $user->roles()->sync([$role->id]);
                Session::flash('success', [
                    "Your subscription has been changed. Now {$plan->description} activated in your account."
                ]);
            }

            if (!$user->can('brainstorm_tool')) {
                Event::fire(new DownGradePlanRemoveBrainStormData($user));
            }

            if (!$user->can('rank_checker')) {
                Event::fire(new DownGradePlanRemoveRankCheckData($user));
            }
        }
    }

    public function trialPlanSubscription($request)
    {
        $user = User::where('id', Auth::id())->first();
        if(isset($user->used_trial_period) && $user->used_trial_period == 1) {
            Session::flash('nosuccess', array("You are not allowed to acquire trial plan because you have used your initial trial subscription. Please try to purchase any paid plan."));
            return Redirect::route('subscription');
        }

        // cancel plan
        if($user->payment_type == getenv('CHARGEBEE_TYPE') && $user->chargebee_customer_id != '' && $user->chargebee_subscription_id != '' && $user->chargebee_active == 1) {
            $this->cancelChargebeeSubscription(1);
        }

        $plan       = Account::getPlanBySystemId($request->get('plan'));
        $role       = $plan->getRole();

        /** @var EmailTemplateManagementController $emailTemplate */
        $emailTemplate = App::make(EmailTemplateManagementController::class);

        $template = $emailTemplate->getEmailTemplateByName(EmailTemplate::SUBSCRIPTION_CANClE_EMAIL_TEMPLATE);
       /* if (!empty($template)) {
            $body = $template->body;
            $body = str_replace('{USER_NAME}', $user->name, $body);
            $body = str_replace('{CANCEL_DATE}', $user->subscription_ends_at->format('D M d Y'), $body);
            $mailData = ['body' => $body];

            $email = $this->user->email;
            $subject = $template->subject;

            Mail::send('emails.blank', $mailData, function ($message) use ($email, $subject) {
                $message->to($email)->subject($subject);
            });
        }*/

        //get trial plan detials
        $trial_days = (isset($role['trial_days']) ? $role['trial_days'] : 0);
        $cDate      = date("Y-m-d");
        $trial_expire_date = date('Y-m-d', strtotime($cDate. ' + '.$trial_days.' days'));

        Session::flash('success', array('Your subscription has been changed.'));
        $user->trial_ends_at    = Carbon::now()->addDay($trial_days);
        $user->account_id       = (isset($plan->id) ? $plan->id : 1);
        $user->stripe_active    = 1;
        $user->toggle_free      = 0;
        $user->chargebee_customer_id     = '';
        $user->chargebee_subscription_id = '';
        $user->chargebee_plan   = '';
        $user->chargebee_active = 0;
        $user->is_trial         = 1;
        $user->trial_expire_date= $trial_expire_date;
        $user->used_trial_period= 1;
        $user->payment_mode = getenv('PAYMENT_MODE_EMPTY');
        $user->save();

        //trial subcription updates in intercom
        $intercomclient = new IntercomController();
        $intercomclient->updateIntercomUser($user);

        //$freeRole = Role::where('name', 'free')->first();
        if (!empty($role)) {
            $user->roles()->sync([$role->id]);
            Session::flash('success',
                array('Your subscription has been changed. Now Free Trial Plan activated in your account.'));
        }

        if (!Auth::user()->can('brainstorm_tool')) {
            Event::fire(new DownGradePlanRemoveBrainStormData(Auth::user()));
        }
        if (!Auth::user()->can('rank_checker')) {
            Event::fire(new DownGradePlanRemoveRankCheckData(Auth::user()));
        }
    
        return Redirect::route('subscription');   
    }

    public function freePlanSubscription($request)
    {
        $user = User::where('id', Auth::id())->first();
        // cancel plan
        if($user->payment_type == getenv('CHARGEBEE_TYPE') && $user->chargebee_customer_id != '' && $user->chargebee_subscription_id != '' && $user->chargebee_active == 1) {
            $this->cancelChargebeeSubscription(1);
        }

        $plan       = Account::getPlanBySystemId($request->get('plan'));
        $role       = $plan->getRole();

        /** @var EmailTemplateManagementController $emailTemplate */
        $emailTemplate = App::make(EmailTemplateManagementController::class);

        $template = $emailTemplate->getEmailTemplateByName(EmailTemplate::SUBSCRIPTION_CANClE_EMAIL_TEMPLATE);

        Session::flash('success', array('Your subscription has been changed.'));
        $user->account_id       = (isset($plan->id) ? $plan->id : 1);
        $user->stripe_active    = 1;
        $user->toggle_free      = 0;
        $user->chargebee_customer_id     = '';
        $user->chargebee_subscription_id = '';
        $user->chargebee_plan   = '';
        $user->chargebee_active = 0;
        $user->payment_mode     = getenv('PAYMENT_MODE_EMPTY');
        $user->save();

        //trial subcription updates in intercom
        $intercomclient = new IntercomController();
        $intercomclient->updateIntercomUser($user);

        //$freeRole = Role::where('name', 'free')->first();
        if (!empty($role)) {
            $user->roles()->sync([$role->id]);
            Session::flash('success',
                array('Your subscription has been changed. Now Free Plan activated in your account.'));
        }

        if (!Auth::user()->can('brainstorm_tool')) {
            Event::fire(new DownGradePlanRemoveBrainStormData(Auth::user()));
        }
        if (!Auth::user()->can('rank_checker')) {
            Event::fire(new DownGradePlanRemoveRankCheckData(Auth::user()));
        }
    
        return Redirect::route('subscription');   
    }

    public function changeToFree()
    {
        $user = User::where('id', Auth::id())->first();
        /** @var EmailTemplateManagementController $emailTemplate */
        $emailTemplate = App::make(EmailTemplateManagementController::class);

        $template = $emailTemplate->getEmailTemplateByName(EmailTemplate::SUBSCRIPTION_CANClE_EMAIL_TEMPLATE);
        if (!empty($template)) {
            $body = $template->body;
            $body = str_replace('{USER_NAME}', $this->user->name, $body);
            $body = str_replace('{CANCEL_DATE}', $this->user->subscription_ends_at->format('D M d Y'), $body);
            $mailData = ['body' => $body];

            $email = $this->user->email;
            $subject = $template->subject;

            Mail::send('emails.blank', $mailData, function ($message) use ($email, $subject) {
                $message->to($email)->subject($subject);
            });
        }
        Session::flash('success', array('Your subscription has been changed.'));
        $user->account_id = 1;
        $user->stripe_active = 0;
        $user->toggle_free = 0;
        $user->save();
        $freeRole = Role::where('name', 'free')->first();
        if (!empty($freeRole)) {
            $user->roles()->sync([$freeRole->id]);
            Session::flash('success',
                array('Your subscription has been changed. Now Free Plan activated in your account.'));
        }

        if (!Auth::user()->can('brainstorm_tool')) {
            Event::fire(new DownGradePlanRemoveBrainStormData(Auth::user()));
        }
        if (!Auth::user()->can('rank_checker')) {
            Event::fire(new DownGradePlanRemoveRankCheckData(Auth::user()));
        }

        return true;
    }

    public function changeToBasic()
    {
        $user = User::where('id', Auth::id())->first();
        $user->account_id = 2;
        $user->toggle_free = 0;
        $user->save();

        $basicRole = Role::where('name', 'basic')->first();
        if (!empty($basicRole)) {
            $user->roles()->sync([$basicRole->id]);
            Session::flash('success',
                array('Your subscription has been changed. now Monthly Basic activated in your account.'));
        }

        if (!Auth::user()->can('brainstorm_tool')) {
            Event::fire(new DownGradePlanRemoveBrainStormData(Auth::user()));
        }
        if (!Auth::user()->can('rank_checker')) {
            Event::fire(new DownGradePlanRemoveRankCheckData(Auth::user()));
        }

        return true;
    }

    public function changeToPro()
    {
        $user = User::where('id', Auth::id())->first();
        $user->account_id = 3;
        $user->toggle_free = 0;
        $user->save();
        $proRole = Role::where('name', 'pro')->first();
        if (!empty($proRole)) {
            $user->roles()->sync([$proRole->id]);
            Session::flash('success',
                array('Your subscription has been changed. Now Monthly Pro Plan activated in your account.'));
        }

        if (!Auth::user()->can('brainstorm_tool')) {
            Event::fire(new DownGradePlanRemoveBrainStormData(Auth::user()));
        }
        if (!Auth::user()->can('rank_checker')) {
            Event::fire(new DownGradePlanRemoveRankCheckData(Auth::user()));
        }

        return true;
    }

    public function changeToProYearly()
    {
        $user = User::where('id', Auth::id())->first();
        $user->account_id = 4;
        $user->toggle_free = 0;
        $user->save();
        $proRole = Role::where('name', 'pro')->first();
        if (!empty($proRole)) {
            $user->roles()->sync([$proRole->id]);
            Session::flash('success',
                array('Your subscription has been changed. Now Yearly Pro Plan activated in your account.'));
        }

        if (!Auth::user()->can('brainstorm_tool')) {
            Event::fire(new DownGradePlanRemoveBrainStormData(Auth::user()));
        }
        if (!Auth::user()->can('rank_checker')) {
            Event::fire(new DownGradePlanRemoveRankCheckData(Auth::user()));
        }

        return true;
    }

    public function changePaypalPlan($postCPP)
    {
        return $this->curlCall('https://api-3t' . $this->url . 'paypal.com/nvp', $postCPP);
    }

    /**
     * @param Request $request
     */
    public function curlCall($url, $postFields, $curlOpts = array())
    {
        if (!array_key_exists("CURLOPT_TIMEOUT", $curlOpts)) {
            $curlOpts['CURLOPT_TIMEOUT'] = 100;
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);

        if ($postFields) {
            $fieldString = $postFields;

            if (is_array($fieldString)) {
                $fieldString = "";
                foreach ($postFields as $k => $v) {
                    $fieldString .= "" . $k . "=" . urlencode($v) . "&";
                }
            }

            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fieldString);
        }

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, $curlOpts['CURLOPT_TIMEOUT']);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        if (array_key_exists("HEADER", $curlOpts)) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $curlOpts['HEADER']);
        }

        $res = curl_exec($ch);

        if (curl_errno($ch)) {
            $res = "CURL Error: " . curl_errno($ch) . " - " . curl_error($ch);
        }

        curl_close($ch);

        return $res;
    }

    public function handlePaypalIPN(Request $request)
    {
        $inputs = $request->all();

        PaypalLogger::info('========================Invoice Paypal Started========================');
        PaypalLogger::info('IPN Response:');
        PaypalLogger::info(print_r($inputs, true));
        Log::info('Invoice Paypal Started');

        if (
            isset($inputs['txn_type']) && $inputs['txn_type'] === 'recurring_payment_profile_cancel' &&
            $inputs['profile_status'] != 'Active'
        ) {
            $user = User::where('stripe_subscription', $inputs['recurring_payment_id'])->first();

            if (!empty($user)) {
                $this->user = $user;

                if ($user->subscription_ends_at) {
                    $user->subscription_ends_at->format('D M d Y');
                }

                $payment = Payment::where('profile_id', $request->get('recurring_payment_id'))->first();

                if (!empty($payment)) {
                    if ($payment->payment_status !== 'Completed') {
                        $user->account_id = 1;
                        $user->stripe_active = 0;

                        $freeRole = Role::where('name', 'free')->first();

                        if (!empty($freeRole)) {
                            $user->roles()->sync([$freeRole->id]);
                            Session::flash('success',
                                array('Your subscription has been changed. Now Free Plan activated in your account.'));
                        }
                    } else {
                        $start_date = Carbon::createFromFormat('Y-m-d H:i:s', $payment->start_date);

                        if ($request->get('payment_cycle') === 'Monthly') {
                            $user->subscription_ends_at = $start_date->addMonth()->toDateTimeString();
                        } elseif ($request->get('payment_cycle') === 'Yearly') {
                            $user->subscription_ends_at = $start_date->addYear()->toDateTimeString();
                        }
                    }
                    $user->toggle_free = 0;
                    $user->pending_payment = 3;
                    $user->save();
                }
            } else {
                PaypalLogger::info('Paypal profile id not found');
            }

        } elseif (
            isset($inputs['txn_type']) && $inputs['txn_type'] === 'recurring_payment_profile_created' &&
            !isset($inputs['txn_id'])
        ) {
            $user = User::where('stripe_subscription', $inputs['recurring_payment_id'])->first();
            $plan = Account::getPlanBySystemId($inputs['product_name']);
            $role = $plan->getRole();

            $user->account_id = $plan->id;
            $user->toggle_free = 0;
            $user->stripe_active = 1;
            $user->pending_payment = 4;

            // if ($plan->getPeriod() === Account::PERIOD_MONTH) {
            //     $user->subscription_ends_at = Carbon::now()->addMonth();
            // } else if ($plan->getPeriod() === Account::PERIOD_YEAR) {
            //     $user->subscription_ends_at = Carbon::now()->addYear();
            // }

            $user->payment_type = Payment::PAYMENT_TYPE_PAYPAL;
            $user->stripe_plan = $inputs['product_name'];
            $user->save();

            $user->roles()->sync([$role->id]);
        } else {
            $receiverEmail = $request->get('receiver_email', '');

            // Build the required acknowledgement message out of the notification just received
            $postIPN = "cmd=_notify-validate";
            $orgIPN = "";

            foreach ($request->all() as $key => $value) {
                $orgIPN .= ("" . $key . " => " . $value . "\r\n");
                $postIPN .= "&" . $key . "=" . urlencode(html_entity_decode($value, ENT_QUOTES));
            }

            $reply = $this->curlCall('https://www' . $this->url . 'paypal.com/cgi-bin/webscr', $postIPN);
            PaypalLogger::info('reply:');
            PaypalLogger::info($reply);

            if (!strcmp($reply, "VERIFIED")) {

                if ($request->get('txn_type') === 'subscr_payment') {
                    PaypalLogger::info('Recurring Payment Completed');
                    try {
                        $plan = Account::getPlanBySystemId($request->get('transaction_subject', ''));
                        $date = Carbon::createFromFormat('M d, Y H:i:s', substr($inputs['payment_date'], 9, 12) . ' ' . substr($inputs['payment_date'], 0, 8))->toDateTimeString();
                        $user = User::where('stripe_subscription', $request->get('subscr_id'))->first();
                        $role = $plan->getRole();

                        $payment = new Payment();
                        $payment->customer_id = $user->id;
                        $payment->transaction_id = $request->get('txn_id', '');
                        $payment->profile_id = $request->get('subscr_id', '');
                        $payment->payment_data = 'Recurring';
                        $payment->payment_status = $request->get('payment_status', '');
                        $payment->amount = $request->get('mc_gross', '');
                        $payment->payment_date = $date;
                        $payment->start_date = $date;
                        $payment->payment_type = Payment::PAYMENT_TYPE_PAYPAL;
                        $payment->plan_type = $plan->paypal_plan;
                        $payment->save();

                        $user->account_id = $plan->id;
                        $user->toggle_free = 0;
                        $user->stripe_active = 1;
                        $user->pending_payment = 7;
                        $user->payment_type = Payment::PAYMENT_TYPE_PAYPAL;
                        $user->stripe_plan = $inputs['product_name'];
                        $user->save();
                        $user->roles()->sync([$role->id]);

                        /*
                        * Now the user paied the money successfully!
                        * Check if There is a Delay for Downgrade his plan
                        * If there is a delay for his plan! remove it to not be downgraded
                        */
                        FailedPayment::where('user_id', $user->id)
                            ->where('downgraded', false)
                            ->where('created_at', '<', Carbon::now()->subDays(3)->toDateTimeString())
                            ->delete();

                    } catch (\Exception $e) {
                        PaypalLogger::error($e->getMessage());
                        PaypalLogger::error($e->getTraceAsString());
                    }
                    PaypalLogger::info('Recurring Payment Stored');
                } elseif ($request->get('txn_type') === 'express_checkout') {
                    PaypalLogger::info('Express Checkout Payment Completed');
                    try {
                        $plan = Account::getPlanBySystemId($request->get('transaction_subject', ''));
                        $date = Carbon::createFromFormat('M d, Y H:i:s', substr($inputs['payment_date'], 9, 12) . ' ' . substr($inputs['payment_date'], 0, 8))->toDateTimeString();
                        $user = User::where('stripe_subscription', $request->get('txn_id'))->first();
                        $role = $plan->getRole();

                        $payment = new Payment();
                        $payment->customer_id = $user->id;
                        $payment->transaction_id = $request->get('txn_id', '');
                        $payment->profile_id = $request->get('txn_id', '');
                        $payment->payment_data = 'instant';
                        $payment->payment_status = $request->get('payment_status', '');
                        $payment->amount = $request->get('mc_gross', '');
                        $payment->payment_date = $date;
                        $payment->start_date = $date;
                        $payment->payment_type = Payment::PAYMENT_TYPE_PAYPAL;
                        $payment->plan_type = $plan->paypal_plan;
                        $payment->save();

                        $user->account_id = $plan->id;
                        $user->toggle_free = 0;
                        $user->stripe_active = 1;
                        $user->pending_payment = 7;
                        $user->payment_type = Payment::PAYMENT_TYPE_PAYPAL;
                        $user->stripe_plan = $inputs['transaction_subject'];
                        $user->save();
                        $user->roles()->sync([$role->id]);


                    } catch (\Exception $e) {
                        PaypalLogger::error($e->getMessage());
                        PaypalLogger::error($e->getTraceAsString());
                    }
                    PaypalLogger::info('Instant Payment Stored');
                } elseif (
                    $request->get('txn_type') === 'recurring_payment_suspended_due_to_max_failed_payment' ||
                    $request->get('txn_type') === 'recurring_payment_failed' ||
                    $request->get('txn_type') === 'subscr_failed' ||
                    $request->get('txn_type') === 'recurring_payment_skipped' ||
                    $request->get('txn_type') === 'subscr_cancel' ||
                    $request->get('txn_type') === 'subscr_eot'
                ) {
                    PaypalLogger::info('Recurring Payment Subscription Ended Or Canceled');
                    try {
                        $subscriptionId = $request->get('subscr_id') ?: $request->get('recurring_payment_id');
                        $productName = $request->get('item_name') ?: $request->get('product_name');
                        $user = User::where('stripe_subscription', $subscriptionId)->first();

                        /*
                        * Delay downgrade for later, it will be handeled with Cron job
                        * The user will be downgraded after 3 days
                        * Send an email to The User to let him know he has a 3 days left
                        */
                        if ($request->get('txn_type') === 'recurring_payment_failed' ||
                            $request->get('txn_type') === 'recurring_payment_skipped' ||
                            $request->get('txn_type') === 'subscr_failed'
                        ) {
                            $failed = new FailedPayment;
                            $failed->user_id = $user->id;
                            $failed->gateway = 'paypal';
                            $failed->save();

                            $data = [
                                'name' => $user->name,
                                'canceldate' => Carbon::now()->addDays(3)->format('d/m/Y')
                            ];
                            /*
                            * Send an email to the user
                            * to let him know that his account will be downgraded after 3 days
                            * 3 Days from the failed payment date
                            */
                            Mail::send('emails.subscription.delaydowngrade', $data, function ($message) use ($user) {
                                $message->to($user->email, $user->name)->subject('Keyword Revealer | Payment Failed');
                            });
                        } else {
                            $plan = Account::getPlanBySystemId($productName);
                            $role = $plan->getRole();
                            $user->toggle_free = 0;
                            $user->stripe_active = 0;
                            $user->pending_payment = 6;
                            $user->account_id = 1;
                            $user->save();

                            $freeRole = Role::where('name', 'free')->first();
                            if (!empty($freeRole)) {
                                $user->roles()->sync([$freeRole->id]);
                            }
                        }
                    } catch (\Exception $e) {
                        PaypalLogger::error($e->getMessage());
                        PaypalLogger::error($e->getTraceAsString());
                    }
                    PaypalLogger::info('User was unsubscribed due to Subscription Ended Or Canceled');
                } elseif (isset($inputs['payer_id']) && isset($inputs['payment_date']) && isset($inputs['recurring_payment_id']) && isset($inputs['payment_status']) && isset($inputs['product_name']) && isset($inputs['transaction_subject'])) {
                    PaypalLogger::info('Handling IPN type:' . $request->get('txn_type', 'no txn_type'));
                    try {
                        $inputs = $request->all();
                        //$user = User::where('stripe_id', $inputs['payer_id'])->first(); // Should be ('stripe_subscription', $inputs['recurring_payment_id']), exception from log paypal-2017-06-06.log, paypal-2018-01-06.log
                        $user = User::where('stripe_subscription', $inputs['recurring_payment_id'])->first();
                        $date = substr($inputs['payment_date'], 9, 12);
                        $time = substr($inputs['payment_date'], 0, 8);
                        $sub_start_date = $date . ' ' . $time;
                        $payment_date = Carbon::createFromFormat('M d, Y H:i:s', $sub_start_date)->toDateTimeString();
                        $enddate = Carbon::createFromFormat('M d, Y H:i:s', $sub_start_date);
                        $payment = Payment::where('profile_id', $inputs['recurring_payment_id'])->where('payment_date', '!=', 'Recurring')->first();
                        $payment->transaction_id = $request->get('txn_id', '');
                        $payment->payment_status = $inputs['payment_status'];
                        $payment->save();

                        $plan = Account::getPlanBySystemId($inputs['product_name']);
                        $role = $plan->getRole();

                        if ($plan->getPeriod() === Account::PERIOD_MONTH) {
                            $enddate = $enddate->addMonth()->format('Y-m-d');
                        } else if ($plan->getPeriod() === Account::PERIOD_YEAR) {
                            $enddate = $enddate->addYear()->format('Y-m-d');
                        }

                        $user->account_id = $plan->id;
                        $user->toggle_free = 0;
                        $user->stripe_active = 1;
                        // $user->subscription_ends_at = $enddate;
                        $user->payment_type = Payment::PAYMENT_TYPE_PAYPAL;
                        $user->stripe_plan = $inputs['product_name'];
                        $user->pending_payment = 5;

                        $user->save();

                        $user->roles()->sync([$role->id]);

                        /* Post_Affiliate_Pro integration snippet 
                        Log::info('Calling (recurring) https://keywordrevealer.postaffiliatepro.com/scripts/sale.php?data2='.$inputs['txn_id'].'&data1='.$inputs['payer_id'].'&TotalCost='.$payment->amount.'&ProductID='.$plan->paypal_plan);
                        if (isset($inputs['payment_status']) and $inputs['payment_status'] == 'Refunded')
                        {
                            //$this->curlCall('https://keywordrevealer.postaffiliatepro.com/scripts/sale.php?data2='.$inputs['recurring_payment_id'].'&data1='.$inputs['payer_id'].'&TotalCost='.(($payment->amount)*-1).'&ProductID='.urlencode($plan->paypal_plan).'&OrderID='.$inputs['payer_id'].'-'.$inputs['txn_id'], array());
                            Log::info('No record for PAP: Refunded');
                        }else{
                            $this->curlCall('https://keywordrevealer.postaffiliatepro.com/scripts/sale.php?data2='.$inputs['recurring_payment_id'].'&data1='.$inputs['payer_id'].'&TotalCost='.$payment->amount.'&ProductID='.urlencode($plan->paypal_plan).'&OrderID='.$inputs['payer_id'].'-'.$inputs['txn_id'], array());
                        }

                        */
                    } catch (\Exception $e) {
                        PaypalLogger::error($e->getMessage());
                        PaypalLogger::error($e->getTraceAsString());
                    }
                    PaypalLogger::info('End Of Handling IPN type:' . $request->get('txn_type', 'no txn_type'));
                } else {
                    /*if (isset($inputs['txn_type']) && $inputs['txn_type'] != 'recurring_payment') // small hack as recuring paypal payments not handlel probably causing paypal ipn to alwayes fail,  Hassan Amir 16-May17
                    {
                        PaypalLogger::info('txn_type = ' . $inputs['txn_type']);
                        //PaypalLogger::info('Invoice Paypal Completed');
                        //return;

                    }else{
                        PaypalLogger::info('Invoice Invalid IPN Error, reply VERIFIED however (payer_id,payment_date,recurring_payment_id,payment_status,product_name,transaction_subject,txn_type) not found');
                        return response(null, 406);
                    }*/
                    PaypalLogger::info('Invoice IPN Error, reply VERIFIED however (payer_id,payment_date,recurring_payment_id,payment_status,product_name,transaction_subject) not found');
                    // some of these IPN data have txn_type and some dont ....
                }

            } else {
                if (!strcmp($reply, "INVALID")) {
                    PaypalLogger::error('Invoice Invalid IPN Error 2');
                    PaypalLogger::error(json_encode($request->all()));
                    PaypalLogger::info('========================Invoice Paypal Not Completed========================');

                    return response(null, 406);
                } else {
                    PaypalLogger::error('Invoice Invalid IPN Error 3');
                    PaypalLogger::error(json_encode($request->all()));
                    PaypalLogger::info('========================Invoice Paypal Not Completed========================');

                    return response(null, 406);
                }
            }

        }
        PaypalLogger::info('========================Invoice Paypal Completed========================');

        return;
    }

    public function getCancel()
    {
        /*
        * check if the reasons that user selected is exist in the cancellation_reasons or not
        */

        $request = request();
        $return = is_string($request->other) && strlen($request->other) > 10 ? false : (is_array($request->reasons) && count($request->reasons) > 0 ? false : true);
        if ($return) {

            return back()->withErrors(['reasons' => 'The reasons field is required.']);
        }

        if ($request->reasons) {
            /*
            * Extend new method to Validator to check the id's of the reasons
            */
            Validator::extend('exist_reason', function ($attribute, $values) {
                foreach ($values as $v) {
                    if (!CancellationReason::find($v)) return false;
                }
                return true;
            });
        }
        $val = Validator::make($request->all(), [
            'reasons' => 'array|min:1|exist_reason',
            'other' => 'string|min:10|max:100'
        ]);

        if ($val->fails()) {
            return back()->withErrors($val);
        }

        /*
        * Save that the user cancelled his plan
        * Save reasonse why he cancelled it
        */

        //cancel the trial account subscription
        $this->user = Auth::user();
        if($this->user->is_trial == 1) {
            $planU       = Account::where(["id" => $this->user->account_id])->first();
            if(isset($planU->name) && !empty($planU->name)) {
                $roleU = Role::where('name', $planU->name)->first();
                if(isset($roleU->is_trial) && $roleU->is_trial == 1) {
                    $user = User::where('id', Auth::id())->first();
                    $user->account_id        = 1;                
                    $user->is_trial          = 0;
                    //$user->trial_expire_date = "";
                    $user->payment_mode = getenv('PAYMENT_MODE_EMPTY');
                    $user->save();
                    $freeRole = Role::where('name', 'free')->first();
                    if (!empty($freeRole)) {
                        $user->roles()->sync([$freeRole->id]);
                    }
                    Session::flash('success', array('your subscription is cancelled successfully.'));
                    return Redirect::route('subscription');
                }
            }
        }

        $account= Account::findOrFail($this->user->account_id);
        $amount = $account->getPrice($this->user->campaign);

        $cancel = new userSubscriptionCancellation;
        $cancel->user_id = $this->user->id;
        $cancel->plan_id = $this->user->account_id;
        $cancel->save();

        if ($request->reasons) {
            foreach ($request->reasons as $reason) {
                $cancellationReason = new userCancellationReason;
                $cancellationReason->reason_id = $reason;
                $cancellationReason->cancellation_id = $cancel->id;
                $cancellationReason->save();
            }
        }
        if ($request->other) {
            $reason = new userCancellationReason;
            $reason->other = $request->other;
            $reason->cancellation_id = $cancel->id;
            $reason->save();
        }


        if ($this->user->payment_type == getenv('CHARGEBEE_TYPE')) {
            $cancel_response = $this->cancelChargebeeSubscription();
            if($cancel_response == false) {
                return Redirect::route('subscription');
            }

            $user = User::where('id', Auth::id())->first();
            $trial_end_date = $user->trial_expire_date;
            $current_date   = date("Y-m-d");

            if(strtotime($trial_end_date) > strtotime($current_date)) {
                $freeRole   = Role::where('name', 'trial')->first();
                $trial_plan = Account::where('name', 'trial')->first();
                $assign_plan= (isset($trial_plan->id) ? $trial_plan->id : 1);
                $isTrial    = 1;
            } else {
                $freeRole   = Role::where('name', 'free')->first();
                $trial_plan = Account::where('name', 'free')->first();
                $assign_plan= (isset($trial_plan->id) ? $trial_plan->id : 1);
                $isTrial    = 0;
            }

            //$plan = Account::getByPaypalPlanName($user->stripe_plan);
            //$paypal_amount = $plan->getPrice($user->campaign);

            $user->chargebee_active = 0;
            $user->chargebee_customer_id = '';
            $user->chargebee_subscription_id = '';
            $user->stripe_active= 0;
            $user->account_id   = $assign_plan;
            $user->is_trial     = $isTrial;
            $user->payment_mode = getenv('PAYMENT_MODE_EMPTY');
            $user->save();

            if (!empty($freeRole)) {
                $user->roles()->sync([$freeRole->id]);
            }

            //trial subcription updates in intercom
            $intercomclient = new IntercomController();
            $intercomclient->updateIntercomUser($user);

            $paramsF = json_encode(['email' => $this->user->email]);
            // User Cancellation in FirstPromoter
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://firstpromoter.com/api/v1/track/cancellation?api_key=' . env('FP_API_KEY') . '&wid=' . env('FP_WTID') . '');
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $paramsF);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json; charset=utf-8',
                    'Content-Length: ' . strlen(($paramsF)))
            );

            $output = curl_exec($ch);
            $result = json_encode($output);

            if ($this->user->current_plan_type == 'lifetime') {
                $dt = Carbon::now();
                $trial_date = Auth::user()->trial_ends_at;
                $current_date = $dt->toDateString();

                $ltd_purchase_date = $this->user->created_at;
                $cancel_day_count = $ltd_purchase_date->diffInDays($current_date);
                $data = array(
                    'name' => $this->user->name,
                    'canceldate' => $current_date,
                    'email' => $this->user->email,
                    //'plan' => $account->stripe_plan,
                    'payment_type' => $this->user->payment_type,
                    'amount' => $amount,
                    'cancel_day_count' => $cancel_day_count
                );

                if ($trial_date > $current_date) {
                    Mail::send('emails.subscription.ltd_cancel_subscription', $data, function ($message) {
                        $message->to($this->user->email, 'KeywordRevealer')->subject('User Cancellation LTD Under 14 Day');
                    });
                }
                Session::flash('success', array('Sorry to see you go. You can subscribe any time'));
            } else {
                $data = array(
                    'name' => $this->user->name,
                    'canceldate' => Carbon::now()->toDateString()
                );

                Mail::send('emails.subscription.cancel', $data, function ($message) {
                    $message->to($this->user->email, $this->user->name)->subject('Keyword Revealer Cancellation');
                });

                //tell user
                Session::flash('success', array('your subscription is cancelled successfully.'));
            }
        } else if ($this->user->payment_type == 'stripe') {
            $params = json_encode(['email' => $this->user->email]);
            // User Cancellation in FirstPromoter
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://firstpromoter.com/api/v1/track/cancellation?api_key=' . env('FP_API_KEY') . '&wid=' . env('FP_WTID') . '');
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json; charset=utf-8',
                    'Content-Length: ' . strlen(($params)))
            );

            $output = curl_exec($ch);
            $result = json_encode($output);

            // User Refunds in FirstPromoter
            $refund_param = json_encode(['email' => $this->user->email, 'amount' => ($amount * 100),'event_id' => $this->user->stripe_id]);

            $refunds_ch = curl_init();
            curl_setopt($refunds_ch, CURLOPT_URL, 'https://firstpromoter.com/api/v1/track/refund?api_key=' . env('FP_API_KEY') . '&wid=' . env('FP_WTID') . '');
            curl_setopt($refunds_ch, CURLOPT_POST, 1);
            curl_setopt($refunds_ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($refunds_ch, CURLOPT_POSTFIELDS, $refund_param);
            curl_setopt($refunds_ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($refunds_ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json; charset=utf-8',
                    'Content-Length: ' . strlen(($refund_param)))
            );

            $refunds_output = curl_exec($refunds_ch);
            $refunds_result = json_encode($refunds_output);

            $user = User::where('id', Auth::id())->first();

            $user->toggle_free = 0;
            $user->account_id = 1;
            $freeRole = Role::where('name', 'free')->first();
            $user->roles()->sync([$freeRole->id]);
            $user->save();

            if ($this->user->current_plan_type == 'lifetime') {

                $dt = Carbon::now();
                $trial_date = Auth::user()->trial_ends_at;
                $current_date = $dt->toDateString();

                $ltd_purchase_date = $this->user->created_at;
                $cancel_day_count = $ltd_purchase_date->diffInDays($current_date);

                $data = array(
                    'name' => $this->user->name,
                    'canceldate' => $current_date,
                    'email' => $this->user->email,
                    'plan' => $account->stripe_plan,
                    'payment_type' => $this->user->payment_type,
                    'amount' => $amount,
                    'cancel_day_count' => $cancel_day_count
                );

                if ($trial_date > $current_date) {
                    Mail::send('emails.subscription.ltd_cancel_subscription', $data, function ($message) {
                        $message->to($this->user->email, 'KeywordRevealer')->subject('User Cancellation LTD Under 14 Day');
                    });
                }

                $user = User::where('id', Auth::id())->first();
                $user->trial_ends_at = null;
                $user->stripe_active = 0;
                $user->stripe_id = null;
                $user->payment_mode = getenv('PAYMENT_MODE_EMPTY');
                $user->save();

                // tell user
                Session::flash('success', array('your subscription is cancelled successfully.'));

            } else {
                $this->user->subscription()->cancel();

                $data = array(
                    'name' => $this->user->name,
                    'canceldate' => $this->user->subscription_ends_at->format('D M d Y')
                );

                Mail::send('emails.subscription.cancel', $data, function ($message) {
                    $message->to($this->user->email, $this->user->name)->subject('Keyword Revealer Cancellation');
                });

                // tell user
                Session::flash('success', array('your subscription is cancelled successfully.'));
            }

        } else {

            PaypalLogger::info('cencel request for User: ' . $this->user->id);

            $user = User::where('id', Auth::id())->first();
            $plan = Account::getByPaypalPlanName($user->stripe_plan);
            $paypal_amount = $plan->getPrice($user->campaign);
            $user->toggle_free = 0;
            $user->stripe_active = 0;
            $user->account_id = 1;
            $user->payment_mode = getenv('PAYMENT_MODE_EMPTY');
//            $user->current_plan_type = null;
//            $user->trial_ends_at = null;


            if ($this->user->current_plan_type != 'lifetime') {
                $reply = $this->cancelPaypal();

                PaypalLogger::info('Paypal Cancel Response');
                PaypalLogger::info($reply);

                parse_str($reply, $status);
                if ($status['ACK'] == 'Failure') {
                    Session::flash('nosuccess',
                        array('Sorry!. Your subscription is not cancel due to' . isset($status['L_LONGMESSAGE0'])));
                    return Redirect::route('subscription');
                }

            }

            $user->save();

            $params = json_encode(['email' => $user->email]);

            // User Cancellation in FirstPromoter
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://firstpromoter.com/api/v1/track/cancellation?api_key=' . env('FP_API_KEY') . '&wid=' . env('FP_WTID') . '');
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json; charset=utf-8',
                    'Content-Length: ' . strlen(($params)))
            );

            $output = curl_exec($ch);
            $result = json_encode($output);


            // User Refunds in FirstPromoter
            $paypal_refund_param = json_encode(['email' => $user->email, 'amount' => ($paypal_amount * 100), 'event_id' => $user->stripe_subscription]);
            $paypal_refunds_ch = curl_init();
            curl_setopt($paypal_refunds_ch, CURLOPT_URL, 'https://firstpromoter.com/api/v1/track/refund?api_key=' . env('FP_API_KEY') . '&wid=' . env('FP_WTID') . '');
            curl_setopt($paypal_refunds_ch, CURLOPT_POST, 1);
            curl_setopt($paypal_refunds_ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($paypal_refunds_ch, CURLOPT_POSTFIELDS, $paypal_refund_param);
            curl_setopt($paypal_refunds_ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($paypal_refunds_ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json; charset=utf-8',
                    'Content-Length: ' . strlen(($paypal_refund_param)))
            );

            $paypal_refunds_output = curl_exec($paypal_refunds_ch);
            $refunds_result = json_encode($paypal_refunds_output);


            $freeRole = Role::where('name', 'free')->first();
            if (!empty($freeRole)) {
                $user->roles()->sync([$freeRole->id]);
            }

            if ($this->user->current_plan_type == 'lifetime') {

                $dt = Carbon::now();
                $trial_date = Auth::user()->trial_ends_at;
                $current_date = $dt->toDateString();

                $ltd_purchase_date = $this->user->created_at;
                $cancel_day_count = $ltd_purchase_date->diffInDays($current_date);

                $data = array(
                    'name' => $this->user->name,
                    'canceldate' => $current_date,
                    'email' => $this->user->email,
                    'plan' => $account->stripe_plan,
                    'payment_type' => $this->user->payment_type,
                    'amount' => $amount,
                    'cancel_day_count' => $cancel_day_count

                );

                if ($trial_date > $current_date) {
                    Mail::send('emails.subscription.ltd_cancel_subscription', $data, function ($message) {
                        $message->to($this->user->email, 'KeywordRevealer')->subject('User Cancellation LTD Under 14 Day');
                    });
                }
                Session::flash('success', array('Sorry to see you go. You can subscribe any time'));

            } else {
                $data = array(
                    'name' => $this->user->name,
                    'canceldate' => Carbon::now()->toDateString()
                );

                Mail::send('emails.subscription.cancel', $data, function ($message) {
                    $message->to($this->user->email, $this->user->name)->subject('Keyword Revealer Cancellation');
                });

                // tell user
                Session::flash('success', array('your subscription is cancelled successfully.'));
            }


            //  echo $output;
            //$request = Request::create($reply);
            //  $params = $request->all();
        }

        return Redirect::route('subscription');
    }

    public function cancelNewSubscription($cb_sub_id) {
        ChargeBee_Environment::configure(getenv('CHARGEBEE_SITE'), getenv('CHARGEBEE_KEY'));
        try {
            $this->user = Auth::user();
            $result = ChargeBee_Subscription::retrieve($cb_sub_id);
            $subscription = $result->subscription();

            if(isset($subscription->status) && $subscription->status == "cancelled") {
                return true;
            }
            if(isset($cb_sub_id) && !empty($cb_sub_id)) {
                $result = ChargeBee_Subscription::cancel($cb_sub_id);
                if(isset($result->subscription()->status) && $result->subscription()->status == "cancelled") {
                    $subscription   = $result->subscription();
                    $customer       = $result->customer();
                    return $result->subscription()->status;
                }
                return true;
            }
        } catch (\Exception $e) {
            return true;
        }
    }

    public function cancelChargebeeSubscription($t=0) {
        ChargeBee_Environment::configure(getenv('CHARGEBEE_SITE'), getenv('CHARGEBEE_KEY'));
        try {
            $this->user = Auth::user();
             $result = ChargeBee_Subscription::retrieve($this->user->chargebee_subscription_id);
            $subscription = $result->subscription();

            if(isset($subscription->status) && $subscription->status == "cancelled") {
                return true;
            }
            if(isset($this->user->chargebee_subscription_id) && !empty($this->user->chargebee_subscription_id)) {
                $result = ChargeBee_Subscription::cancel($this->user->chargebee_subscription_id);
                if(isset($result->subscription()->status) && $result->subscription()->status == "cancelled") {
                    $subscription   = $result->subscription();
                    $customer       = $result->customer();
                    $card           = $result->card();
                    $invoice        = $result->invoice();
                    return $result->subscription()->status;
                }
                $subscription   = $result->subscription();
                $customer       = $result->customer();
                $card           = $result->card();
                $invoice        = $result->invoice();
                return true;
            }
        } catch (\Exception $e) {
            if($t == 0) {
                Log::info(json_encode($e));
                Session::flash('nosuccess', array("failded to cancel. please try again later or contact our support."));
            }
        }
    }

    public function getDownloadInvoicePDF($cb_sub_id=0) {
        ChargeBee_Environment::configure(getenv('CHARGEBEE_SITE'), getenv('CHARGEBEE_KEY'));        
        $result = ChargeBee_Invoice::pdf($cb_sub_id);
        $download = $result->download();
        echo (isset($download->downloadUrl) ? $download->downloadUrl : '');
    }

    public function validateCouponCode(Request $request) {
        $cb_coupon  = $request->input("promo_code");
        $cb_plan    = $request->input("plan");
        $postData   = $request->input();

        $roleData   = Role::where('name',$cb_plan)->first();
        $cb_plan_id = (isset($roleData['chargebee_plan_id']) ? $roleData['chargebee_plan_id'] : '');

        ####verify coupon code
        $postArr = "";
        try {
            $subParams["planId"] = $cb_plan_id;
            $subParams["coupon"] = $cb_coupon;
            $params = array("subscription" => $subParams);
            ChargeBee_Environment::configure(getenv('CHARGEBEE_SITE'), getenv('CHARGEBEE_KEY'));
            $result = ChargeBee_Estimate::createSubscription($params);
            $response = array();
            $response['discount_amount'] = number_format(($result->estimate()->discounts[0]->amount/100), 2, '.', ' ');
            $response['total_amount'] = number_format(($result->estimate()->amount/100), 2, '.', ' ');

            if(isset($postData["pay_request"]) && $postData["pay_request"] == "paypal") {
                $request->session()->put('cb_coupon', $cb_coupon);
                $request->session()->put('cb_plan', $cb_plan);
            }

            return json_encode(array("status" => "success", "response" => $response));
            //return $invoiceEstimate = $result->estimate()->invoiceEstimate;
        } catch (ChargeBee_InvalidRequestException $e) {
            //Checking whether the error is due to coupon param. If the error is due to
            //coupon param then reason for error can be identified through "api_error_code" attribute.
            if ($e->getParam() == "subscription[coupon]") {
                 return json_encode(array("status" => "error", "response" => $e->getMessage()));
            }
            return;
        } catch (Exception $e) {
           return;
        }
    }

    public function cancelPaypal()
    {
        $this->user = Auth::user();
        $form_fields = array(
            'USER' => env('PAYPAL_USER_OLD'),
            'PWD' => env('PAYPAL_USER_PWD_OLD'),
            'SIGNATURE' => env('PAYPAL_SIGNATURE_OLD'),
            'VERSION' => '95',
            'METHOD' => 'ManageRecurringPaymentsProfileStatus',
            'PROFILEID' => $this->user->stripe_subscription,
            'ACTION' => 'cancel',
            'NOTE' => 'Profile cancelled at store'
        );
        $postCNL = "cmd=_notify-validate";
        $orgCNl = "";
        foreach ($form_fields as $key => $value) {
            $orgCNl .= ("" . $key . " => " . $value . "\r\n");
            $postCNL .= "&" . $key . "=" . urlencode(html_entity_decode($value, ENT_QUOTES));
        }
        return $this->curlCall('https://api-3t' . $this->url . 'paypal.com/nvp', $postCNL);
    }

    public function getResume()
    {
        $this->user = Auth::user();
        $this->user->subscription($this->user->stripe_plan)->resume();

        $user = User::where('id', Auth::id())->first();

        $user->toggle_free = 0;

        $user->save();

        // tell user
        Session::flash('success', array('Thank you for resuming your subscription.'));

        return Redirect::route('subscription');
    }

    public function getCard()
    {
        ChargeBee_Environment::configure(getenv('CHARGEBEE_SITE'), getenv('CHARGEBEE_KEY'));

        $this->user = Auth::user();
        if($this->user->chargebee_subscription_id == '') {
            Session::flash('nosuccess', array("You have not subscribed to chargebee plan. Therefore, your are not allowed to update your card details."));
            return Redirect::route('subscription');
        }

        $result = ChargeBee_Subscription::retrieve($this->user->chargebee_subscription_id);

        $subscription = $result->subscription();
        $customer     = $result->customer();
        $card         = $result->card();

        try {
            if(isset($subscription->status) && $subscription->status == "cancelled") {
                Session::flash('nosuccess', array("There are some errors while proceessing. please try again later or contact our support"));
                return Redirect::route('subscription');
            }
        } catch (\Exception $e) {
            Log::info(json_encode($e));
            Session::flash('nosuccess', array("There are some errors while proceessing. please try again later or contact our support"));
            return Redirect::route('subscription');
        }

        $year = Carbon::now()->year;
        return View('backend.subscription.card', [
            'year' => $year,
            'subscription'  => $subscription,
            'customer'      => $customer,
            'card'          => $card,
        ]);
    }

    public function postCard(Request $request)
    {
        $postData     = $request->input();
        $this->user   = Auth::user();
        if($this->user->payment_type == 'chargebee') {
            $postData     = $request->input();
            $card_number  = (isset($postData['card_number']) ? $postData['card_number'] : '');
            $cc_exp_month = (isset($postData['exp_month']) ? $postData['exp_month'] : '');
            $cc_exp_year  = (isset($postData['exp_year']) ? $postData['exp_year'] : '');
            $cvc          = (isset($postData['cvc']) ? $postData['cvc'] : '');
            $phone        = (isset($postData['phone']) ? $postData['phone'] : '');
            ChargeBee_Environment::configure(getenv('CHARGEBEE_SITE'), getenv('CHARGEBEE_KEY'));
            try {
                $custID = $this->user->chargebee_customer_id;
                $stripeToken= $request->get('stripeTmpToken');
                $custID = $this->user->chargebee_customer_id;
                $result = ChargeBee_Card::updateCardForCustomer($custID, array(
                    "gateway"   => "stripe",
                    "type"      => "card",
                    "tmp_token" => $stripeToken,
                ));
/*
                $result2 = ChargeBee_Card::updateCardForCustomer($custID, array(
                  "gateway"     => "chargebee", 
                  "firstName"   => $this->user->name, 
                  "lastName"    => $this->user->name, 
                  "number"      => $card_number, 
                  "expiryMonth" => $cc_exp_month, 
                  "expiryYear"  => $cc_exp_year, 
                  "cvv"     => $cvc));
*/                  
                Session::flash('success', array('Your card has been updated and subscription as Your old plan (' . Auth::user()->account->description . ').'));                 
            } catch (\Exception $e) {
                Log::info(json_encode($e));
                Session::flash('nosuccess', array("Card details are not being updated. Please try again later or contact our support"));
                return Redirect::route('subscription');
            }
            $last_four = substr($card_number, -4);
            $user = User::where('id', Auth::id())->first();
            $user->last_four = $last_four;
            $user->save();
        } else {
            $this->user->updateCard(Input::get('token'));
            $this->user->subscription($this->user->stripe_plan)->resume();
            $this->user->toggle_free = 0;
            $this->user->save();

            // tell user
            Session::flash('success', array('Your card has been updated and subscription as Your old plan (' . Auth::user()->account->description . ').'));
        }
        return Redirect::route('auth.profile');
    }

    public function getChange()
    {
        ChargeBee_Environment::configure(getenv('CHARGEBEE_SITE'), getenv('CHARGEBEE_KEY'));
        $all = ChargeBee_Plan::all(array(
            "limit" => $this->chargebee_plan_limit, 
            "status[is]" => "active",
            "periodUnit[is]" => "month",
        ));

        $dataArr  = array();
        $dataArrY = array();
        $dataArrT = array();
        $responseArr = $this->getChargebeeplans($all);
        $dataArr  = $responseArr["dataArr"];
        $dataArrY = $responseArr["dataArrY"];
        // trial account
        $dataArrT = $this->getTrialPlanInformation();
        $invoices = $this->getInvoiceList();
        $user_plan= $this->getCurrentPlan();
        $coupon_ID= $this->getCouponID();
        //echo "<pre>"; print_r($invoices); exit();
        return view('backend.subscription.change', [
            'user' => Auth::user(),
            'data' => $dataArr,
            'records_y' => $dataArrY,
            'data_trial'=> $dataArrT,
            'invoices'  => $invoices,
            'user_plan' => $user_plan,
            'cb_coupon' => $coupon_ID,
            'cancellation_reasons' => CancellationReason::get(),
        ]);
    }

    public function postChange(Request $request) {
        $user       = Auth::user();
        $this->user = $user;

        $id   = $request->get('plan');
        $plan = Account::where('name', $id)->where('disabled', 0)->first();
        $roleData = Role::where('name',$id)->first();
        if (!$plan) {
            return redirect()->back();
        }

        if(isset($roleData['is_trial']) && $roleData['is_trial'] == 1) {
            return $this->trialPlanSubscription($request);
        }

        if($id == 'free') {
            return $this->freePlanSubscription($request);
        }

        if(!isset($roleData['chargebee_plan_id']) && empty($roleData['chargebee_plan_id'])) {
            return redirect()->back();
        }

        $flag       = $request->input('payment_gateway');
        if($flag == "stripe") {
            return $this->getJoinstoken($request);
        } else if($flag == "paypal") {
            $url = $this->payByPaypal($request);
            return Redirect::to($url);
        } else {
            return $this->getJoin($request);            
        }
        return Redirect::route('subscription');
        die('=========');
        //pervious method
        if ($user->payment_type === Payment::PAYMENT_TYPE_STRIPE) {
            $this->changeToPlan($user, $request->input('plan'));
        } else if ($user->payment_type === Payment::PAYMENT_TYPE_PAYPAL) {
            if ($request->input('plan') == 'free') {
                $reply = $this->cancelPaypal();
                PaypalLogger::info('Paypal Cancel Response');
                PaypalLogger::info($reply);
                parse_str($reply, $status);
                if ($status['ACK'] == 'Failure') {
                    Session::flash('nosuccess', [
                        'Sorry!. Your subscription plan is not changed due to' .
                        isset($status['L_LONGMESSAGE0']) ? $status['L_LONGMESSAGE0'] : ' UNKNOWN_ERROR'
                    ]);
                    return Redirect::route('subscription');
                }
                $this->changeToFree();
            } else {
                try {
                    DB::beginTransaction();
                    $plan = Account::getPlanBySystemId($request->input('plan'));
                    $newDay = $this->calculateRemainUsage($request->input('plan'));
                    $reply = $this->cancelPaypal();
                    PaypalLogger::info('Paypal Cancel Response');
                    PaypalLogger::info($reply);
                    parse_str($reply, $status);

                    if ($status['ACK'] == 'Success') {
                        $payment = Payment::where('profile_id', $user->stripe_subscription)->where('payment_date', '!=', 'Recurring')->first();
                        $payment->payment_status = 'Canceled';
                        $payment->save();

                        /* Post_Affiliate_Pro integration snippet
                        $papCookie = '';
                        if (isset($_GET['pap_cookie'])) {
                            $papCookie = $_GET['pap_cookie'];
                        }
                        */

                        ///$new = $this->paypalPaymentProcess($request->input('plan'), $papCookie); // /Post Affiliate Pro integration snippet - added $papCookie
                        $new = $this->paypalPaymentProcess($request->input('plan')); // Post_Affiliate_Pro integration snippet - removed $papCookie

                        $payment = Payment::where('token', $new['TOKEN'])->first();
                        $payment->remain_usage = $newDay;

                        $payment->save();

                        $this->changeToPlan($user, $request->input('plan'));
                        DB::commit();

                        $amt = $plan->getPrice();
                        $itemName = urlencode($plan->paypal_plan);
                        $itemNumber = urlencode(strtoupper('KR-' . $plan->name));

                        return redirect(
                            'https://www' . $this->url . 'paypal.com/cgi-BIN/webscr?cmd=_express-checkout&token=' .
                            $new['TOKEN'] . '&business=' . env('PAYPAL_BUSINESS_ID') .
                            '&lc=CA&item_name=' . $itemName . '&item_number=' . $itemNumber . '&no_note=1' .
                            '&no_shipping=1&rm=1&return_url=' . env('PAYPAL_SUCCESS_URL') . '&cancel_url=' .
                            env('PAYPAL_CANCEL_URL') . '&src=1&a3=' . $amt .
                            '&p3=1&t3=M&currency_code=USD&' .
                            'bn=PP%2dSubscriptionsBF%3abtn_subscribeCC_LG%2egif%3aNonHosted&custom=' . $user->id
                        );
                    }

                } catch (\Exception $e) {
                    DB::rollBack();

                    return redirect()
                        ->back()
                        ->withErrors($e->getMessage());
                }
            }
        } else {
            Session::flash('nosuccess', ['you have not subscribed.']);
            return Redirect::route('subscription');
        }

        // tell user
        return Redirect::route('subscription');
    }

    // test route

    public function createPlan()
    {
        $Paypal = new Paypal;

        // basic
        // $Paypal->createPlan([
        //     'name'        => 'Basic',
        //     'description' => 'Keyword Revealer Basic Account',
        //     'type'        => 'fixed',
        //     'price'       => '9.97',
        //     'frequency'   => 'MONTH',
        //     'cycles'      => '12',
        // ]);
        // pro
        // $Paypal->createPlan([
        //     'name'        => 'Pro',
        //     'description' => 'Keyword Revealer Pro Account',
        //     'type'        => 'fixed',
        //     'price'       => '29.97',
        //     'frequency'   => 'MONTH',
        //     'cycles'      => '12',
        // ]);
        // pro yearly
        // $Paypal->createPlan([
        //     'name'        => 'ProYearly',
        //     'description' => 'Keyword Revealer Pro Yearly Account',
        //     'type'        => 'fixed',
        //     'price'       => '247',
        //     'frequency'   => 'YEAR',
        //     'cycles'      => '3',
        // ]);
        die;

        // take the id and then activate them
        //$Paypal->activatePlan('P-27U660600P662080TJINML6Q');

    }


    public function basic()
    {
        $Paypal = new Paypal;

        $Paypal->createPlan([
            'name' => 'Basic',
            'description' => 'Keyword Revealer Basic Account',
            'type' => 'fixed',
            'price' => '9.97'
        ]);
        $Paypal->getPlan('P-45Y00102S1751650NIUQRKIQ');

        //$Paypal->getPlans();

        return view('front.subscription.basic', [
            'account' => Account::BasicAccount()
        ]);
    }

    public function paypal($accountId)
    {
        // for now account id = 2 (basic)
        $account = Account::BasicAccount();
    }


}
