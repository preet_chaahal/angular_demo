<?php
namespace App\Http\Controllers\Auth;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Config;
use Illuminate\Http\Request;
use Hash;
use Redirect;
use Session;
use App\User;
use App;
use App\Http\Controllers\Admin\EmailTemplateManagementController;
use App\EmailTemplate;
use Illuminate\Support\Facades\URL;
use Mail;

class UserController extends Controller
{
    public function __construct()
    {
        // Must be signed in to access anything in here
        $this->middleware('auth');
    }

    public function index()
    {
    	
        $user = Auth::user();
      
        $searchEngines = Config::get('searchengines');
        $languages = Config::get('languages');

        usort($searchEngines, function($a,$b) {
            if ($a['name'] == 'All Locations') {
                return -1;
            } else if ($b['name'] == 'All Locations') {
                return 1;
            }
            return strcmp($a['name'], $b['name']);
        });

        $selectedEngine = 'All Locations';

        if ($user->default_location) {
            foreach ($searchEngines as $engine) {
                if ($engine['short_form'] == $user->default_location) {
                    $selectedEngine = $engine['name'];
                    break;
                }
            }
        } else {
            foreach ($searchEngines as $engine) {
                if ($engine['engine'] == $user->defaultSearchEngine) {
                    $selectedEngine = $engine['name'];
                    break;
                }
            }
        }

        //echo "<pre>";print_r($searchEngines);die;
        return view('backend.user.index', [
            'user'          => $user,
            'searchEngines' => json_encode($searchEngines),
            'selectedEngine' => $selectedEngine,
            'searchLanguages' => json_encode($languages),
            'selectedLanguage' => $user->defaultSearchLanguage
        ]);
    }

    public function update(Request $request)
    {

        $id = $request->input('id');

        $rules = [
            'name'     => 'required',
            'email'    => 'required|email|unique:users,email,' . $id,
        ];

        $this->validate($request, $rules);

        // lets see if we are changing our email
        if($request->input('email') != Auth::user()->email)
        {
            $rules = ['email' => 'required|unique:users,email'];
            $this->validate($request, $rules);
        }

        $searchEngines = Config::get('searchengines');
        $defaultSearchEngine = $request->input('defaultSearchEngine');
        foreach ($searchEngines as $engine) {
            if ($engine['name'] == $defaultSearchEngine) {
                $selectedEngine = $engine['engine'];
                $selectedLocation = $engine['short_form'];
                break;
            }
        }

        // update the user profile
        $user = \App\User::find(Auth::id());

        if($request->input('email') != $user->email ){
            
            $user->is_registered  = false;  
            // tell user
            Session::flash('success', array('Your email has been recently updated, please check your email for activation link.'));
        }else{
                // tell user
           Session::flash('success', array('Your profile was updated'));
        }

        if(empty($user->register_token)) {
            $user->register_token  = md5(date('Y-m-d H:i:s'));
        }
        $user->username            = $request->input('email');
        $user->name                = $request->input('name');
        $user->email               = $request->input('email');
        $user->defaultSearchEngine = $selectedEngine;
        $user->default_location    = $selectedLocation;
        $user->defaultSearchLanguage = $request->input('defaultSearchLanguage');
        $user->save();

        $this->sendActivationEmail($user->id);
        
        return Redirect::route('auth.profile');
    }

    /**
     * allow the user to update their password
     * @return [type] [description]
     */
    public function password()
    {
        $user = Auth::user();

        return view('backend.user.password', [
            'user' => $user
        ]);
    }

    public function getAccountDelete(){
        $user = Auth::user();

        return view('backend.user.delete_account', [
            'user' => $user
        ]);
    }

    public function postAccountDelete(Request $request){


        $user = Auth::user();

        if($user->account_id != 1){

            $planInfo = App\Account::findOrFail($user->account_id);

            $str = "you are currently subscribed to a <strong>$planInfo->stripe_plan</strong> plan, deleteing the account will cancel your current subscribtion and we will not be able to re-activate your account";

            return view('backend.user.delete_account', [
                'user' => $user
            ])->withErrors($str);

        }

        \DB::table('has_tables')->insert([
            'email'=>Hash::make($user->email),
            'user_id'=>$user->id
        ]);

        $user->email = null;
        $user->username = null;
        $user->ip = null;
        $user->last_four = null;
        $user->save();

        Auth::logout();

        Session::flash('success', ['Your Account Delete Permanently']);

        return Redirect::route('auth.login');

//        return view('backend.user.delete_account', [
//            'user' => $user
//        ])->with(['success'=>"Your Account Delete Permanently"]);

    }

    public function passwordUpdate(Request $request)
    {
        $user = Auth::user();
        $rules = [
            'current_password' => 'required',
            'newpassword'      => 'required|confirmed'
        ];

        $messages = [
            'newpassword.required' => 'Your new password is required',
            'newpassword.confirmed' => 'Your new password does not match'
        ];

        $this->validate($request, $rules, $messages);

        // check the current password
        if ($user->checkPassword($request->input('current_password')) === false) {
            return Redirect::back()->withErrors(['msg' => 'Current password is incorrect']);
        }

        // we are here because everything is good, lets update the password
        $user->password = Hash::make($request->input('newpassword'));
        $user->save();
        
        $this->changePasswordEmail($user->id);
        // tell user
        Session::flash('success', array('Your password was updated'));

        return Redirect::route('auth.profile.password');
    }


    public function sendActivationEmail($user_id)
    {
        /** @var User $user */

        $user = User::where('id', $user_id)->first();

        /** @var EmailTemplateManagementController $emailTemplate */
        $emailTemplate = App::make(EmailTemplateManagementController::class);

        $template = $emailTemplate->getEmailTemplateByName(EmailTemplate::CONFORM_EMAIL_ACTIVATE_ACCOUNT_TEMPLATE);
        if (!empty($template)) {
            $body = $template->body;
            $body = str_replace('{USER_NAME}', $user->username, $body);
            $body = str_replace('{URL}', URL::route('auth.hash.index', $user->register_token), $body);
            $mailData = ['body' => $body];

            $email = $user->email;
            $subject = $template->subject;

            Mail::send('emails.blank', $mailData, function ($message) use ($email, $subject) {
                $message->to($email)->subject($subject);
            });

        }

    }

    public function changePasswordEmail($user_id)
    {
        /** @var User $user */
        $user = User::where('id', $user_id)->first();

        /** @var EmailTemplateManagementController $emailTemplate */
        $emailTemplate = App::make(EmailTemplateManagementController::class);

        $template = $emailTemplate->getEmailTemplateByName(EmailTemplate::CHANGE_PASSWORD_EMAIL_TEMPLATE);
        if (!empty($template)) {
            $body = $template->body;
            $body = str_replace('{USER_NAME}', $user->username, $body);
            $body = str_replace('{URL}', URL::route('auth.hash.index', $user->register_token), $body);
            $mailData = ['body' => $body];

            //$email = $user->email;
            $email = "adityachw.uve@gmail.com";
            $subject = $template->subject;

            Mail::send('emails.blank', $mailData, function ($message) use ($email, $subject) {
                $message->to($email)->subject($subject);
            });

        }

    }    
}