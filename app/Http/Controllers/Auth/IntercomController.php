<?php

namespace App\Http\Controllers\Auth;

use App;
use Validator;
use App\Account;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Role;
use Auth;
use Carbon\Carbon;
use DB;
use Doctrine\DBAL\Query\QueryException;
use Event;
use Illuminate\Http\Request;
use Input;
use Log;
use Mail;
use Redirect;
use Session;
use Intercom\IntercomClient;

class IntercomController extends Controller
{
    protected $user;
    protected $url;
    public function __construct()
    {
        $this->middleware('auth');
    }

    /** Create a user in intercom */
    public function createIntercomUser($user='') {
        $client = new IntercomClient(getenv('INTERCOM_API_KEY'), null);

        if(isset($user->is_trial) && $user->is_trial == 1) {
            $plan              = "trial";
            $trial_start_date  = date("Y-m-d");
            $trial_expire_date = (isset($user->trial_expire_date) ? $user->trial_expire_date : '');
            $trail_active      = "active";
            $chargebee_active  = "";
        } else {
            $plan              = "free";
            $trial_start_date  = "";
            $trial_expire_date = "";
            $trail_active      = "";
            $chargebee_active  = "";
        }

        $client->users->create([
          "user_id" => (isset($user->id) ? $user->id : ''),
          "email"   => (isset($user->email) ? $user->email : ''),
          "name"    => (isset($user->name) ? $user->name : ''),
          "custom_attributes" => [
            'subscription_plan'   => $plan,
            'chargebee_active'    => $chargebee_active,
            //'trail_active'        => $trail_active,
            'trial_active'        => $trail_active,
            'trial_start_date'    => $trial_start_date,
            'trial_expire_date'   => $trial_expire_date,
            ]
        ]);
    }

    /** Update a user intercom **/
    public function updateIntercomUser($user='',$free=0) {
        $client = new IntercomClient(getenv('INTERCOM_API_KEY'), null); 
        if(isset($user->account_id) && !empty($user->account_id)) {
            $plan     = Account::where('id', $user->account_id)->first();
            $roleData = Role::where('name',$plan['name'])->first();

            if(isset($user->chargebee_subscription_id) && !empty($user->chargebee_subscription_id)) {
                $plan = (isset($roleData['screen_name']) ? $roleData['screen_name'] : '');
                $trial_start_date  = "";
                $trial_expire_date = "";
                $trail_active      = "";
                $chargebee_active  = "active";
                $chargebee_plan_id = (isset($roleData['chargebee_plan_id']) ? $roleData['chargebee_plan_id'] : '');
            } else {
                 if(isset($roleData['name']) && $roleData['name'] == 'trial') {
                    $plan = (isset($roleData['screen_name']) ? $roleData['screen_name'] : '');
                    $trial_start_date  = date("Y-m-d");
                    $trial_expire_date = (isset($user->trial_expire_date) ? $user->trial_expire_date : '');
                    $trail_active      = "active";
                    $chargebee_active  = "";
                    $chargebee_plan_id = "";
                } else if(isset($roleData['name']) && $roleData['name'] == 'free') {
                    $plan = (isset($roleData['screen_name']) ? $roleData['screen_name'] : '');
                    $trial_start_date  = "";
                    $trial_expire_date = "";
                    $trail_active      = "";
                    $chargebee_active  = "";
                    $chargebee_plan_id = "";
                } else {
                    $plan = (isset($roleData['screen_name']) ? $roleData['screen_name'] : '');
                    $trial_start_date  = "";
                    $trial_expire_date = "";
                    $trail_active      = "";
                    $chargebee_active  = "";
                    $chargebee_plan_id = "";
                }
            }

            $client->users->update([
              "user_id" => (isset($user->id) ? $user->id : ''),
              "email"   => (isset($user->email) ? $user->email : ''),
              "name"    => (isset($user->name) ? $user->name : ''),
              "custom_attributes" => [
                'subscription_plan'   => $plan,
                'chargebee_active'    => $chargebee_active,
                'chargebee_plan_id'   => $chargebee_plan_id,
                //'trail_active'        => $trail_active,
                'trial_active'        => $trail_active,
                'trial_start_date'    => $trial_start_date,
                'trial_expire_date'   => $trial_expire_date,
                ]
            ]);
        }
    }

    /** Delete a user by intercom ID */
    public function deleteIntercomeUser($intercomeId) {
        $client->users->deleteUser($intercomeId);        
    }

    /** Get a user by ID */
    public function getIntercomeUserById($intercomeId) {
        /** Get a user by ID */
        $client->users->getUser($intercomeId);        
    }

    /** Find a single user by email */
    public function getIntercomeUserByEmail($email) {
        $client->users->getUsers(["email" => $email]);  
    }
}
