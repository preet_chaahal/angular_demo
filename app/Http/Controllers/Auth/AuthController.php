<?php

namespace App\Http\Controllers\Auth;


use App;
use App\EmailTemplate;
use App\Http\Controllers\Admin\EmailTemplateManagementController;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\User;
use Auth;
use Carbon\Carbon;
use Hash;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Http\Request;
use Illuminate\Mail\Message;
use Mail;
use Redirect;
use Session;
use Validator;
use MailWizzApi_Autoloader;
use MailWizzApi_Endpoint_ListSubscribers;



/**
 * Class AuthController
 * @package App\Http\Controllers\Auth
 */
class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * @var string
     */
    protected $redirectTo = '/tools/keywordresearch';

    /**
     * AuthController constructor.
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => ['getLogout','hashIndex']]);
    }

    /**
     * @param $hash
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function hashIndex($hash)
    {
        //echo "<pre>";print_r($hash);die;
        /** @var User $user */
        $user = User::where('register_token', $hash)->first();



        if (User::where('register_token', $hash)->first()) {
            Session::flash('success', ['Your account has been verified']);

            Session::flash('ga_track_event', true);

            if($user->email_status == 'deliverable'){
                if(preg_replace('/www\./i', '', $_SERVER['SERVER_NAME']) == 'keywordrevealer.com') {
                    //require the Mailer first and set it's stuff before change the user status
                    require_once '/var/www/vhosts/keywordrevealer.com/httpdocs/vendor/twisted1919/mailwizz-php-sdk/examples/setup.php';
                    // CREATE THE ENDPOINT
                    $endpoint = new MailWizzApi_Endpoint_ListSubscribers();
                    $response = $endpoint->create('hv528krmtg927', array(
                        'EMAIL' => $user->email,
                        'FNAME' => $user->name,
                        'USERNAME' => $user->username,
                        'USER_LEVEL' => 'Free',
                        'USER_ID' => $user->id
                    ));

                }
            }

//            $user->register_token = '';
            $user->is_registered  = true;
            $user->save();


            // login
            Auth::login($user);
            $emailTemplate = App::make(EmailTemplateManagementController::class);

            $template = $emailTemplate->getEmailTemplateByName(EmailTemplate::WELCOME_EMAIL_TEMPLATE);
            if (!empty($template)) {
                $body = $template->body;
                $body = str_replace('{USER_NAME}', $user->username, $body);
                $body = str_replace('{EMAIL}', $user->email, $body);
                $mailData = ['body' => $body];
                $email = $user->email;
                $subject = $template->subject;

                Mail::send('emails.blank', $mailData, function ($message) use ($email, $subject) {
                    $message->to($email)->subject($subject);
                });
            }
            return Redirect::route('auth.profile');
        }

        Session::flash('nosuccess', ['Your account cannot be verified']);

        return Redirect::route('auth.profile');
        /*return view('front.subscription.verify', [
            'hash' => $hash
        ]);*/
    }


    /**
     * @param Request $request
     *
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function hashVerify(Request $request)
    {
        /** @var User $user */
        $user = User::where('register_token', $request->input('hash'))
                    ->where('username', $request->input('username'))
                    ->first();

        if ( ! $user) {
            // we didn't find a user with that criterial
            return redirect()->back()->withInput()->withErrors(['Invalid credentials']);
        }

        // we are here because everything is good
        // activate their account
//        $user->register_token = '';
        $user->is_registered  = true;

        $user->save();

        $emailTemplate = App::make(EmailTemplateManagementController::class);

        $template = $emailTemplate->getEmailTemplateByName(EmailTemplate::WELCOME_EMAIL_TEMPLATE);
        if (!empty($template)) {
            $body = $template->body;
            $body = str_replace('{USER_NAME}', $user->username, $body);
            $body = str_replace('{EMAIL}', $user->email, $body);
            $mailData = ['body' => $body];
            $email = $user->email;
            $subject = $template->subject;

            Mail::send('emails.blank', $mailData, function ($message) use ($email, $subject) {
                $message->to($email)->subject($subject);
            });
        }
        if ($user->disabled) {
            // flash user
            Session::flash('nosuccess', ['Your account has been disabled. Please contact support.']);

            return Redirect::route('auth.login');
        }

        // login
        Auth::login($user);


        return Redirect::route('backend.tools.keywordresearch');
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function attemptLogin(Request $request)
    {
        $errorMessage = null;

        if(\Illuminate\Support\Facades\Session::get('loginAttempts') >= 2 ){
            $rules = [
                'username' => 'required',
                'password' => 'required',
            'g-recaptcha-response' => 'required|captcha',
            ];
        }else{
            $rules = [
                'username' => 'required',
                'password' => 'required',
//            'g-recaptcha-response' => 'required|recaptcha',
            ];
        }

        $this->validate($request, $rules);
        $remeber_me = $request->input('remember');
        /** @var User $user */
        $user = User::where('username', $request->input('username'))->orWhere('email', $request->input('username'))->first();
        $notRegistered = false;

        $loginAttempts = 0;
        $login_ip = request()->ip();
        if ( ! $user ) {

            if($login_ip == request()->ip()){
                $loginAttempts = \Illuminate\Support\Facades\Session::get('loginAttempts');
                \Illuminate\Support\Facades\Session::put('loginAttempts', $loginAttempts + 1);
                    Session::put('loginAttemptTime', Carbon::now());

            }
            $errorMessage = 'Invalid credentials';
        } else if ( $this->checkPasswordForUser($request->input('password'), $user) === false ) {

            if($login_ip == request()->ip()){
                $loginAttempts = \Illuminate\Support\Facades\Session::get('loginAttempts');
                \Illuminate\Support\Facades\Session::put('loginAttempts', $loginAttempts + 1);
                    Session::put('loginAttemptTime', Carbon::now());
            }

            $errorMessage = 'Invalid credentials';

        } else if ( ! $user->is_registered) {
            $notRegistered = true;
            Session::flash('nosuccess',['Your account is not verified yet, Click here To <a href="'.url('auth/resend-email').'">resend activation email</a>']);
            return redirect()
                ->back();
        } else if ( $user->disabled ) {
            $errorMessage = 'Your account has been suspended, possibly due to account sharing, creating multiple accounts or the use of automated software. If you believe you\'re seeing this message in error, please contact our support team.';
        }

        /*
        * If the user is registered from the Campaign Login In him
        * Redirect him to the Checkout Page
        * He will be redirect to checkout because he didn't purchase any plan yet!
        */
        if (($notRegistered && $user && $user->campaign && $user->account_id == 1) || (!$errorMessage && $user && $user->campaign && $user->account_id == 1 && !$user->is_registered)) {
          $user->last_login = date('Y-m-d H:i:s');
          $user->save();

          Auth::login($user,$remeber_me);
          \Illuminate\Support\Facades\Session::forget('loginAttemptTime');
          \Illuminate\Support\Facades\Session::forget('loginAttempts');

          return Redirect::route('front.pages.landing.pricing.checkout', ['plan' => $user->campaign,'_token' => csrf_token()]);
        }

        if ($errorMessage) {
            return redirect()
                ->back()
                ->withInput()
                ->withErrors([$errorMessage]);
        }

        // if we are here everything is ok!
        // update the user
        $user->last_login = date('Y-m-d H:i:s');
        $user->save();

        // now log the user in
        Auth::login($user,$remeber_me);

        \Illuminate\Support\Facades\Session
            ::forget('loginAttempts');

        // redirect them
        return Redirect::route('backend.tools.keywordresearch');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name'     => 'required|max:255',
            'email'    => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     *
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name'     => $data['name'],
            'email'    => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    /**
     * Compares given password with User's password hash with several hashing algorithms
     *
     * @param $password
     * @param User $user
     * @return bool
     */
    protected function checkPasswordForUser($password, User $user)
    {
        return $user->checkPassword($password);
    }

}
