<?php
    Route::group(['middleware' => 'admin'], function ()
    {
        Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function ()
        {
            Route::get('/dashboard', array(
                    'as'   => 'admin.dashboard',
                    'uses' => 'AdminController@dashboard')
            );

            Route::get('/registration-management', array(
                'as'   => 'admin.registration.management',
                'uses' => 'AdminController@registrationManagement'
            ));

            Route::get('/registration-management/toggle-registration', array(
                'as'   => 'admin.registration.management.toggle',
                'uses' => 'AdminController@registrationManagementToggle'
            ));
            Route::get('/email-template', array(
                    'as'   => 'admin.email-template',
                    'uses' => 'EmailTemplateManagementController@index')
            );
            Route::get('/email-template/new', array(
                    'as'   => 'admin.email.new',
                    'uses' => 'EmailTemplateManagementController@add')
            );
            Route::post('/email-template/new', array(
                    'as'   => 'admin.email.store',
                    'uses' => 'EmailTemplateManagementController@store')
            );
            Route::get('/email-template/edit/{id}', array(
                    'as'   => 'admin.email.edit',
                    'uses' => 'EmailTemplateManagementController@edit')
            );
            Route::post('/email-template/edit', array(
                    'as'   => 'admin.email.edit.post',
                    'uses' => 'EmailTemplateManagementController@update')
            );
            Route::get('/email-template/delete/{id}', array(
                    'as'   => 'admin.email.delete',
                    'uses' => 'EmailTemplateManagementController@delete')
            );
            Route::post('/email-template', array(
                    'as'   => 'admin.email-template',
                    'uses' => 'EmailTemplateManagementController@update')
            );
            Route::post('/registration-management/toggle-registration', array(
                'as'   => 'admin.registration.management.toggle.post',
                'uses' => 'AdminController@registrationManagementTogglePost'
            ));

            Route::get('/registration-management/disable/ip', array(
                'as'   => 'admin.registration.management.disable.ip',
                'uses' => 'AdminController@disableIP'
            ));

            Route::post('/registration-management/disable/ip', array(
                'as'   => 'admin.registration.management.disable.ip.post',
                'uses' => 'AdminController@disableIPPost'
            ));

            Route::get('/registration-management/delete/ip/{id}', array(
                    'as'   => 'admin.registration.management.disable.ip.delete',
                    'uses' => 'AdminController@disableIPDelete')
            );

            Route::get('/registration-management/disable/email', array(
                'as'   => 'admin.registration.management.disable.email',
                'uses' => 'AdminController@disableEmail'
            ));

            Route::post('/registration-management/disable/email', array(
                'as'   => 'admin.registration.management.disable.email.post',
                'uses' => 'AdminController@disableEmailPost'
            ));

            Route::get('/registration-management/delete/email/{id}', array(
                    'as'   => 'admin.registration.management.disable.email.delete',
                    'uses' => 'AdminController@disableEmailDelete')
            );

            //User Management
            Route::group(['prefix' => 'users'], function ()
            {
                Route::get('/', array(
                        'as'   => 'admin.users',
                        'uses' => 'UserManagementController@index')
                );

                Route::get('/edit/{id}', array(
                        'as'   => 'admin.users.edit',
                        'uses' => 'UserManagementController@edit')
                );

                Route::post('/update', array(
                        'as'   => 'admin.users.update',
                        'uses' => 'UserManagementController@update')
                );

                Route::get('/delete/{id}', array(
                        'as'   => 'admin.users.delete',
                        'uses' => 'UserManagementController@destroy')
                );

                Route::get('/add', array(
                        'as'   => 'admin.users.add',
                        'uses' => 'UserManagementController@create')
                );

                Route::post('/store', array(
                        'as'   => 'admin.users.store',
                        'uses' => 'UserManagementController@store')
                );

                Route::get('/get', array(
                        'as'   => 'admin.users.get',
                        'uses' => 'UserManagementController@get')
                );
            });

            // Proxy Management
            Route::group(['prefix' => 'proxies'], function ()
            {
                Route::get('/{module}', array(
                        'as'   => 'admin.proxies',
                        'uses' => 'AdminController@proxies')
                );

                Route::get('/add/{module}', array(
                        'as'   => 'admin.proxies.add',
                        'uses' => 'AdminController@proxiesAdd')
                );
                Route::get('/access-log/{ip}', array(
                        'as'   => 'admin.proxies.log',
                        'uses' => 'AdminController@proxiesLog')
                );
                Route::post('/insert/post', array(
                        'as'   => 'admin.proxies.insert.post',
                        'uses' => 'AdminController@proxiesInsert')
                );

                Route::get('/import/{module}', array(
                        'as'   => 'admin.proxies.import',
                        'uses' => 'AdminController@proxiesImport')
                );
                Route::post('/import/post', array(
                        'as'   => 'admin.proxies.import.post',
                        'uses' => 'AdminController@proxiesImportPost')
                );

                Route::get('/delete/{id}', array(
                        'as'   => 'admin.proxies.delete',
                        'uses' => 'AdminController@proxiesDelete')
                );

                Route::get('/edit/{id}', array(
                        'as'   => 'admin.proxies.edit',
                        'uses' => 'AdminController@proxiesEdit')
                );
                Route::post('/update', array(
                        'as'   => 'admin.proxies.update',
                        'uses' => 'AdminController@proxiesUpdate')
                );

            });

            // Tools management
            Route::group(['prefix' => 'tools'], function ()
            {
                Route::group(['prefix' => 'ranktracker'], function ()
                {
                    Route::get('/', array(
                            'as'   => 'admin.tools.ranktracker.index',
                            'uses' => 'AdminRankController@index')
                    );

                    Route::get('/view/{id}', array(
                            'as'   => 'admin.tools.ranktracker.view',
                            'uses' => 'AdminRankController@view')
                    );

                    Route::get('/download/{id}', array(
                            'as'   => 'admin.tools.ranktracker.download',
                            'uses' => 'AdminRankController@download')
                    );

                    Route::post('/event/insert', array(
                            'as'   => 'admin.tools.ranktracker.events.insert',
                            'uses' => 'AdminRankController@eventInsert')
                    );

                    Route::get('/add', array(
                            'as'   => 'admin.tools.ranktracker.add',
                            'uses' => 'AdminRankController@add')
                    );
                    Route::post('/insert', array(
                            'as'   => 'admin.tools.ranktracker.insert',
                            'uses' => 'AdminRankController@insert')
                    );

                    Route::get('/edit/{id}', array(
                            'as'   => 'admin.tools.ranktracker.edit',
                            'uses' => 'AdminRankController@edit')
                    );
                    Route::post('/update', array(
                            'as'   => 'admin.tools.ranktracker.update',
                            'uses' => 'AdminRankController@update')
                    );

                    Route::get('/delete/{id}', array(
                            'as'   => 'admin.tools.ranktracker.delete',
                            'uses' => 'AdminRankController@delete')
                    );

                    Route::group(['prefix' => 'settings'], function ()
                    {
                        Route::get('/', array(
                                'as'   => 'admin.tools.ranktracker.settings',
                                'uses' => 'AdminRankController@settings')
                        );

                        Route::get('/logs', array(
                                'as'   => 'admin.tools.ranktracker.logs',
                                'uses' => 'AdminRankController@logs')
                        );

                        Route::get('/logs/view/{id}', array(
                                'as'   => 'admin.tools.ranktracker.viewlogs',
                                'uses' => 'AdminRankController@viewLogs')
                        );

                        Route::get('/logs/delete/{id}', array(
                                'as'   => 'admin.tools.ranktracker.logs.delete',
                                'uses' => 'AdminRankController@deleteLog')
                        );

                        Route::group(['prefix' => 'options'], function ()
                        {
                            Route::get('/', array(
                                    'as'   => 'admin.tools.ranktracker.options',
                                    'uses' => 'AdminRankController@options')
                            );
                            Route::post('/update', array(
                                    'as'   => 'admin.tools.ranktracker.options.update',
                                    'uses' => 'AdminRankController@optionsUpdate')
                            );
                        });

                    });
                });

                // brainstorm
                Route::group(['prefix' => 'brainstorm'], function ()
                {
                    Route::get('/', array(
                            'as'   => 'admin.tools.brainstorm.index',
                            'uses' => 'AdminBrainstormController@index')
                    );
                    Route::get('/view/{id}', array(
                            'as'   => 'admin.tools.brainstorm.view',
                            'uses' => 'AdminBrainstormController@view')
                    );
                    Route::get('/refresh/{id}', array(
                            'as'   => 'admin.tools.brainstorm.refresh',
                            'uses' => 'AdminBrainstormController@refresh')
                    );
                    Route::get('/add', array(
                            'as'   => 'admin.tools.brainstorm.add',
                            'uses' => 'AdminBrainstormController@add')
                    );
                    Route::post('/insert', array(
                            'as'   => 'admin.tools.brainstorm.insert',
                            'uses' => 'AdminBrainstormController@insert')
                    );
                    Route::get('/delete/{id}', array(
                            'as'   => 'admin.tools.brainstorm.delete',
                            'uses' => 'AdminBrainstormController@delete')
                    );
                });


            });

            // Reports Management
            Route::group(['prefix' => 'reports'], function ()
            {
                Route::get('/', array(
                        'as'   => 'admin.reports',
                        'uses' => 'ReportController@index')
                );

                Route::get('/users', array(
                        'as'   => 'admin.report.users',
                        'uses' => 'ReportController@users')
                );
                Route::post('/users/download', array(
                        'as'   => 'admin.report.users.download',
                        'uses' => 'ReportController@usersDownload')
                );
                Route::post('/users/report', array(
                        'as' => 'admin.report.users.report',
                        'uses' => 'ReportController@userReport')
                );


                Route::get('/evaluation', array(
                        'as'   => 'admin.report.evaluation',
                        'uses' => 'ReportController@evaluation')
                );
                Route::post('/evaluation/download', array(
                        'as'   => 'admin.report.evaluation.download',
                        'uses' => 'ReportController@evaluationDownload')
                );
            });
            //Plan Management
            Route::group(['prefix' => 'plan'], function ()
            {
                Route::get('/{role_name}', array(
                        'as'   => 'admin.plan',
                        'uses' => 'RoleManagementController@index')
                );
                Route::post('/{role_name}', array(
                        'as'   => 'admin.plan.update',
                        'uses' => 'RoleManagementController@planUpdate')
                );
            });
            Route::group(['prefix' => 'newplan'], function ()
            {
                Route::get('/add', array(
                        'as'   => 'admin.newplan.add',
                        'uses' => 'RoleManagementController@planAdd')
                );
                Route::post('/add', array(
                        'as'   => 'admin.newplan.create',
                        'uses' => 'RoleManagementController@planCreate')
                );
            });
        });
    });