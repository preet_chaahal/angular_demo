<?php

Route::group(['prefix' => 'tools', 'namespace' => 'Backend\Tools' , 'middleware' => 'verified'], function() {

    Route::group(['prefix' => 'keywordresearch','middleware' => ['permission:keyword_searches']], function() {
        Route::get('/', array(
            'as'   => 'backend.tools.keywordresearch',
            'uses' => 'KeywordResearchController@index')
        );
        Route::get('/index/data', array(
            'as'   => 'backend.tools.keywordresearch.index.data',
            'uses' => 'KeywordResearchController@indexData')
        );
        Route::get('/ang/{template}', array(
            'as'   => 'backend.tools.keywordresearch.ang.template',
            'uses' => 'KeywordResearchController@angularTemplates')
        );
        Route::get('/add', array(
            'as'   => 'backend.tools.keywordresearch.add',
            'uses' => 'KeywordResearchController@add')
        );
        Route::any('/insert', array(
            'as'   => 'backend.tools.keywordresearch.insert',
            'uses' => 'KeywordResearchController@insert')
        );
        Route::get('/insert/data', array(
            'as'   => 'backend.tools.keywordresearch.insert',
            'uses' => 'KeywordResearchController@insertData')
        );
        Route::post('/bulk/delete', array(
            'as'   => 'backend.tools.keywordresearch.bulkDelete',
            'uses' => 'KeywordResearchController@bulkDeleteKeywords')
        );
        Route::get('/findResearch/{id}', array(
            'as'   => 'backend.tools.keywordresearch.findResearch',
            'uses' => 'KeywordResearchController@findResearch')
        );
        Route::post('/postDomainAvailability', array(
            'as'   => 'backend.tools.keywordresearch.postDomainAvailability',
            'uses' => 'KeywordResearchController@postDomainAvailability')
        );
        Route::get('/deleteResearch/{id}', array(
            'as'   => 'backend.tools.keywordresearch.deleteResearch',
            'uses' => 'KeywordResearchController@deleteResearch')
        );
        Route::get('/deleteEvaluate/{id}', array(
            'as'   => 'backend.tools.keywordresearch.deleteEvaluate',
            'uses' => 'KeywordResearchController@deleteEvaluate')
        );
        Route::post('/importkeyword', array(
            'as'   => 'backend.tools.keywordresearch.importkeyword',
            'uses' => 'KeywordResearchController@importkeyword')
        );
        Route::post('/storefilteredkeyword', array(
            'as'   => 'backend.tools.keywordresearch.storeFilteredKeyword',
            'uses' => 'KeywordResearchController@storeFilteredKeyword')
        );        
        Route::post('/store', array(
            'middleware' => ['permission:keyword_save'],
            'as'   => 'backend.tools.keywordresearch.store',
            'uses' => 'KeywordResearchController@store')
        );
        Route::get('/saved/{id}/{key_index_list}', array(
            'as'   => 'backend.tools.keywordresearch.saved',
            'uses' => 'KeywordResearchController@saved')
        );
        Route::get('/notSaved/{id}', array(
            'as'   => 'backend.tools.keywordresearch.notSaved',
            'uses' => 'KeywordResearchController@notSaved')
        );
        Route::get('/download', array(
            'as'   => 'backend.tools.keywordresearch.download',
            'uses' => 'KeywordResearchController@download')
        );
        Route::get('/downloadPreview/{id}', array(
            'as'   => 'backend.tools.keywordresearch.downloadPreview',
            'uses' => 'KeywordResearchController@downloadPreview')
        );
        Route::post('/downloadEvaluated/{id}/{downloadEvaluated}', array(
            'as'   => 'backend.tools.keywordresearch.downloadEvaluated',
            'uses' => 'KeywordResearchController@downloadEvaluated')
        );
        Route::get('/downloadEvaluated/file', array(
            'as'   => 'backend.tools.keywordresearch.downloadEvaluated.file',
            'uses' => 'KeywordResearchController@downloadSearchedData')
        );
        Route::get('/evaluate/{keyword}/{keyword_id}/{searches}/{cpc}/{profit}/{word_count}/{location}/{language}/{search_keyword}/{isEvaluated}/{is_reevaluate}/{is_unsaved}/{project_name}/{tmp_id}', array(
            'as'   => 'backend.tools.keywordresearch.evaluate',
            'uses' => 'KeywordResearchController@evaluate')
        );
        /*Route::post('/evaluate/{keyword}/{keyword_id}/{searches}/{cpc}/{profit}/{word_count}/{location}/{language}/{search_keyword}/{isEvaluated}/{is_reevaluate}/{is_unsaved}/{project_name}/{tmp_id}', array(
            'as'   => 'backend.tools.keywordresearch.evaluate',
            'uses' => 'KeywordResearchController@evaluate')
        );*/        
        Route::get('/evaluated/{id}', array(
            'as'   => 'backend.tools.keywordresearch.evaluated',
            'uses' => 'KeywordResearchController@evaluated')
        );
        Route::get('/google/trends/{keyword}', array(
            'as'   => 'backend.tools.keywordresearch.google.trends',
            'uses' => 'KeywordResearchController@googleTrends')
        );
        Route::get('/destroy/{id}', array(
            'as'   => 'backend.tools.keywordresearch.destroy',
            'uses' => 'KeywordResearchController@destroy')
        );
        Route::get('/destroyAjax', array(
            'as'   => 'backend.tools.keywordresearch.destroyAjax',
            'uses' => 'KeywordResearchController@destroyAjax')
        );
        Route::post('/bulk/delete', array(
            'as'   => 'backend.tools.keywordresearch.bulkDelete',
            'uses' => 'KeywordResearchController@bulkDelete')
        );
        Route::get('/destroyResearchAjax', array(
            'as'   => 'backend.tools.keywordresearch.destroyResearach',
            'uses' => 'KeywordResearchController@destroyResearach')
        );
        Route::post('/saveKeyword',array(
            'as'    =>  'backend.tools.keywordresearch.saveKeyword',
            'uses'  =>  'KeywordResearchController@saveKeyword'
        ));
        Route::post('/updateKeyword', array(
            'as' => 'backend.tools.keywordresearch.updateKeyword',
            'uses' => 'KeywordResearchController@updateEvaluation'
        ));
        Route::get('/userCountsAndLimits', array(
            'as'   => 'backend.tools.keywordresearch.userCountsAndLimits',
            'uses' => 'KeywordResearchController@userCountsAndLimits')
        );
    });

    Route::group(['prefix' => 'ranktracker','middleware' => ['permission:rank_checker']], function() {
        Route::get('/', array(
            'as'   => 'backend.tools.ranktracker',
            'uses' => 'RankTrackerController@index')
        );
        Route::get('/add', array(
            'as'   => 'backend.tools.ranktracker.add',
            'uses' => 'RankTrackerController@add')
        );
        Route::post('/insert', array(
            'as'   => 'backend.tools.ranktracker.insert',
            'uses' => 'RankTrackerController@insert')
        );
        Route::post('/date', array(
            'as'   => 'backend.tools.ranktracker.setDate',
            'uses' => 'RankTrackerController@setDate')
        );
        Route::get('/edit/{id}', array(
            'as'   => 'backend.tools.ranktracker.edit',
            'uses' => 'RankTrackerController@edit')
        );
        Route::post('/update', array(
            'as'   => 'backend.tools.ranktracker.update',
            'uses' => 'RankTrackerController@update')
        );
        Route::get('/view/{id}', array(
            'as'   => 'backend.tools.ranktracker.view',
            'uses' => 'RankTrackerController@view')
        );
        Route::get('/delete/{id}', array(
            'as'   => 'backend.tools.ranktracker.delete',
            'uses' => 'RankTrackerController@delete')
        );
        Route::get('/download/{id}', array(
            'as'   => 'backend.tools.ranktracker.download',
            'uses' => 'RankTrackerController@download')
        );
        Route::post('/event/insert', array(
            'as'   => 'backend.tools.ranktracker.events.insert',
            'uses' => 'RankTrackerController@eventInsert')
        );
        Route::get('/casper/generator/{id}', array(
            'as'   => 'backend.tools.ranktracker.casper.generator',
            'uses' => 'RankTrackerController@casperGenerator')
        );
        Route::post('/bulk/delete', array(
            'as'   => 'backend.tools.ranktracker.bulkDelete',
            'uses' => 'RankTrackerController@bulkDelete')
        );
    });

    Route::group(['prefix' => 'brainstorm','middleware' => ['permission:brainstorm_tool']], function() {
        Route::any('/', array(
            'as'   => 'backend.tools.brainstorm',
            'uses' => 'BrainstormController@index')
        );
        Route::post('/storeCpcData',array(
            'as'=>'backend.tools.brainstorm.storeCpcData',
            'uses'=>'BrainstormController@storeCpcData')
        );
        Route::get('/downloadSavedResearch/{id}',array(
            'as'=>'backend.tools.brainstorm.downloadSavedResearch',
            'uses'=>'BrainstormController@downloadSavedResearch')
        );
        Route::get('/add', array(
            'as'   => 'backend.tools.brainstorm.add',
            'uses' => 'BrainstormController@add')
        );
        Route::post('/insert', array(
            'as'   => 'backend.tools.brainstorm.insert',
            'uses' => 'BrainstormController@save')
        );
        Route::get('/search', array(
            'as' => 'backend.tools.brainstorm.search',
            'uses' => 'BrainstormController@search')
        );
        Route::post('/apiData', array(
            'as'   => 'backend.tools.brainstorm.apiData',
            'uses' => 'BrainstormController@apiData')
        );
        Route::get('/delete', array(
            'as'   => 'backend.tools.brainstorm.delete',
            'uses' => 'BrainstormController@delete')
        );
        Route::get('/view/{id}', array(
            'as'   => 'backend.tools.brainstorm.view',
            'uses' => 'BrainstormController@view')
        );
        Route::get('/refresh/{id}', array(
            'as'   => 'backend.tools.brainstorm.refresh',
            'uses' => 'BrainstormController@refresh')
        );
        Route::post('/bulk/delete', array(
            'as'   => 'admin.tools.brainstorm.bulkDelete',
            'uses' => 'BrainstormController@bulkDelete')
        );
    });
});
