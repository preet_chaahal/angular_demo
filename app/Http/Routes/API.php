<?php

Route::group(['prefix' => 'api', 'namespace' => 'Backend\API'], function() {
    Route::group(['prefix' => 'seomoz'], function() {
        Route::get('/get/{key}/{urls}', array(
            'as'   => 'backend.api.seomoz.get',
            'uses' => 'SeomozController@get')
        );
    });
});