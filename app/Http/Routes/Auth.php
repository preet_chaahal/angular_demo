<?php

/**
 * LOG IN / LOG OUT
 */
Route::group(['prefix' => 'auth'], function() {

    // user profile
    Route::get('/profile', array(
        'as'   => 'auth.profile',
        'uses' => 'Auth\UserController@index')
    );
    Route::post('/profile/update', array(
        'as'   => 'auth.profile.update',
        'uses' => 'Auth\UserController@update')
    );
    Route::get('/profile/password', array(
        'as'   => 'auth.profile.password',
        'uses' => 'Auth\UserController@password')
    );
    Route::post('/profile/password/update', array(
        'as'   => 'auth.profile.password.update',
        'uses' => 'Auth\UserController@passwordUpdate')
    );

    Route::get('/profile/delete-account',array(
       'as'=>'auth.profile.account.delete',
       'uses'=>'Auth\UserController@getAccountDelete'
    ));

    Route::post('/profile/delete-account',array(
        'as'=>'auth.profile.account.delete',
        'uses'=>'Auth\UserController@postAccountDelete'
    ));

    // New Authentication Routes
    Route::get('/hash/{hash}', array(
        'as'   => 'auth.hash.index',
        'uses' => 'Auth\AuthController@hashIndex')
    );
    Route::post('/hash/verify', array(
        'as'   => 'auth.hash.verify',
        'uses' => 'Auth\AuthController@hashVerify')
    );

    // Resend Verification Email
    Route::get('resend-email','Front\SubscriptionController@getResendVerifyEmail');
    Route::post('resend-email','Front\SubscriptionController@PostResendVerifyEmail')->name('resend-email');

    // Authentication routes...
    Route::get('login', [
        'as'   => 'auth.login',
        'uses' => 'Auth\AuthController@getLogin'
    ]);
    Route::post('login', [
        'as'   => 'auth.login',
        'uses' => 'Auth\AuthController@attemptLogin'
    ]);

    Route::get('logout', [
        'as'   => 'auth.logout',
        'uses' => 'Auth\AuthController@getLogout'
    ]);

    //Subscription Routes
    Route::group(['prefix' => 'subscription'], function()
    {
        Route::get('/', [
            'as' => 'subscription',
            'uses' => 'Auth\SubscriptionController@getIndex'
        ]);
        Route::any('success', [
            'as' => 'success',
            'uses' => 'Auth\SubscriptionController@getSuccess'
        ]);
        Route::any('cancel', [
            'as' => 'cancel',
            'uses' => 'Auth\SubscriptionController@cancelUrl'
        ]);
        Route::get('invoices', [
            'as' => 'invoices',
            'uses' => 'Auth\SubscriptionController@getInvoices'
        ]);

        Route::get('userinvoice', [
            'as' => 'userinvoice',
            'uses' => 'Auth\SubscriptionController@getUserInvoice'
        ]);

        Route::get('downloadinvoice/{id}', [
            'as' => 'subscription-downloadinvoice',
            'uses' => 'Auth\SubscriptionController@getDownloadInvoicePDF'
        ]);

        Route::get('card', [
            'as' => 'subscription-card',
            'uses' => 'Auth\SubscriptionController@getCard'
        ]);

        Route::post('card', [
            'as' => 'subscription-card',
            'uses' => 'Auth\SubscriptionController@postCard'
        ]);

        Route::get('join', [
            'as' => 'subscription-join',
            'uses' => 'Auth\SubscriptionController@getJoin'
        ])->middleware('available_plan');

        Route::post('join', [
            'as' => 'subscription-post',
            'uses' => 'Auth\SubscriptionController@postJoin'
        ])->middleware('available_plan');

        Route::get('joinstoken', [
            'as' => 'subscription-joinstoken',
            'uses' => 'Auth\SubscriptionController@getJoinstoken'
        ])->middleware('available_plan');

        Route::post('joinstoken', [
            'as' => 'subscription-poststoken',
            'uses' => 'Auth\SubscriptionController@postJoinstoken'
        ])->middleware('available_plan');

        Route::get('payment/{id}', [
            'as' => 'subscription-get',

            'uses' => 'Auth\SubscriptionController@payByPaypal'
        ]);

        Route::get('validatecoupon/{id}', [
            'as' => 'subscription-validatecoupon',
            'uses' => 'Auth\SubscriptionController@validateCouponCode'
        ]);

        //Not Subscribed
        Route::group(['middleware' => 'notsubscribed'], function()
        {


        });
        //End Not Subscribed
        //Subscribed
        Route::group(['middleware' => 'subscribed'], function ()
        {
            Route::group(['middleware' => 'notcancelled'], function()
            {
                Route::get('cancel', [
                    'as' => 'subscription-cancel',
                    'uses' => 'Auth\SubscriptionController@getCancel'
                ]);

                Route::get('change', [
                    'as' => 'subscription-change',
                    'uses' => 'Auth\SubscriptionController@getChange'
                ]);

                Route::post('change', [
                    'as' => 'subscription-change-post',
                    'uses' => 'Auth\SubscriptionController@postChange'
                ]);
            });
            //Cancelled
            Route::group(['middleware' => 'cancelled'], function()
            {
                Route::get('resume', [
                    'as' => 'subscription-resume',
                    'uses' => 'Auth\SubscriptionController@getResume'
                ]);
            });
            //End Cancelled
        });
        //End Subscribed
        // TEST ROUTES
        Route::get('/createPlan', array(
            'as'   => 'subscription.createPlan',
            'uses' => 'Auth\SubscriptionController@createPlan')
        );
    });
    //End Subscription Routes
});

/**
 * PASSWORD RESET
 */
Route::group(['prefix' => 'password'], function() {
    Route::get('email', [
        'as'   => 'password.email',
        'uses' => 'Auth\PasswordController@getEmail'
    ]);
    Route::post('email', [
        'as'   => 'password.email',
        'uses' => 'Auth\PasswordController@postEmail'
    ]);

    Route::get('reset/{token}', [
        'as'   => 'password.reset',
        'uses' => 'Auth\PasswordController@getReset'
    ]);
    Route::post('reset', [
        'as'   => 'password.reset',
        'uses' => 'Auth\PasswordController@postReset'
    ]);
});

Route::any('payment/ppl_lstnr.php', [
   'as' => 'subscription-paypal-success',
   'uses' => 'Auth\SubscriptionController@handlePaypalIPN'
]);
Route::any('payment/paypal_listener.php', [
   'as' => 'subscription-paypal-success',
   'uses' => 'Auth\SubscriptionController@handlePaypalIPN'
]);
Route::any('subscription-paypal-success', [
    'as' => 'subscription-paypal-success',
    'uses' => 'Auth\SubscriptionController@handlePaypalIPN'
]);
Route::get('/login.php', function(){
    return Redirect::to('/auth/login', 301);
});


