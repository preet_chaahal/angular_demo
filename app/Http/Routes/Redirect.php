<?php

Route::get('/blog/sample-page/',function (){
    return Redirect::to('https://www.keywordrevealer.com/blog/',301);
});

Route::get('/blog/keyword-brainstorming-and-grouper-tool/',function (){
    return Redirect::to('https://www.keywordrevealer.com/blog/tutorial-keyword-brainstorming-tool/',301);
});

Route::get(' /blog/keyword-tool-getting-started/',function (){
    return Redirect::to('https://www.keywordrevealer.com/blog/keyword-research-tool-tutorial/',301);
});

Route::get(' /blog/category/tutorials/',function (){
    return Redirect::to('https://www.keywordrevealer.com/blog/category/tutorial/',301);
});

Route::get('/blog/tag/keyword-brainstorming-tool/',function (){
    return Redirect::to('https://www.keywordrevealer.com/blog/tutorial-keyword-brainstorming-tool/',301);
});

Route::get('/blog/affiliate-program/',function (){
    return Redirect::to('https://keywordrevealer.postaffiliatepro.com/affiliates/',301);
});

Route::get('/blog/latest-news/',function (){
    return Redirect::to('https://www.keywordrevealer.com/blog/',301);
});

Route::get('/blog/tag/brainstorm/',function (){
    return Redirect::to('https://www.keywordrevealer.com/blog/tutorial-keyword-brainstorming-tool/',301);
});

Route::get('/sign_up.php',function (){
    return Redirect::to('https://www.keywordrevealer.com/subscription/free',301);
});

Route::get('/contact-form.php',function (){
    return Redirect::to('https://www.keywordrevealer.com/contact',301);
});

Route::get(' /home.php',function (){
    return Redirect::to('https://www.keywordrevealer.com',301);
});

Route::get('/faq.php',function (){
    return Redirect::to('https://www.keywordrevealer.com',301);
});

Route::get('/plans.php',function (){
    return Redirect::to('https://www.keywordrevealer.com/#pricing',301);
});

Route::get('/keyword_brainstorming.php',function (){
    return Redirect::to('https://www.keywordrevealer.com/tools/brainstorm',301);
});

Route::get('/login',function (){
    return Redirect::to('https://www.keywordrevealer.com/auth/login',301);
});

Route::get('/sign',function (){
    return Redirect::to('https://www.keywordrevealer.com/auth/login',301);
});

Route::get('/privacy.php',function (){
    return Redirect::to('https://www.keywordrevealer.com/privacy',301);
});

Route::get('/terms.php',function (){
    return Redirect::to('https://www.keywordrevealer.com/terms',301);
});

Route::get('/blog/knowledgebase/how-do-i-cancel-my-account',function (){
    return Redirect::to('https://docs.keywordrevealer.com/faqs/cancel-monthly-subscription',301);
});

Route::get('/blog/affiliate-program',function (){
    return Redirect::to('https://keywordrevealer.firstpromoter.com/',301);
});

