<?php namespace App\Libraries\Logger;

use Monolog\Handler\RotatingFileHandler;
use Monolog\Logger;

/**
 * Created by PhpStorm.
 * User: Mitul
 * Date: 19/05/15
 * Time: 12:44 PM
 */
class PaypalLogger
{
    /** @var  \Monolog\Logger */
    public $monoLog;

    function __construct()
    {
        $this->monoLog = new Logger('log');
        $logFile = storage_path('logs/paypal.log');
        $this->monoLog->pushHandler(new RotatingFileHandler($logFile));
    }

    public static function info($message, array $context = array())
    {
        $logger = new PaypalLogger();
        $logger->monoLog->info($message, $context);
    }

    public static function error($message, array $context = array())
    {
        $logger = new PaypalLogger();
        $logger->monoLog->error($message, $context);
    }

    public static function debug($message, array $context = array())
    {
        $logger = new PaypalLogger();
        $logger->monoLog->debug($message, $context);
    }

    public static function notice($message, array $context = array())
    {
        $logger = new PaypalLogger();
        $logger->monoLog->notice($message, $context);
    }

    public static function warning($message, array $context = array())
    {
        $logger = new PaypalLogger();
        $logger->monoLog->warning($message, $context);
    }

    public static function critical($message, array $context = array())
    {
        $logger = new PaypalLogger();
        $logger->monoLog->critical($message, $context);
    }

    public static function alert($message, array $context = array())
    {
        $logger = new PaypalLogger();
        $logger->monoLog->alert($message, $context);
    }
}