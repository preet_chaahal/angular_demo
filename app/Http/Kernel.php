<?php

namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
        \App\Http\Middleware\EncryptCookies::class,
        \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
        \Illuminate\Session\Middleware\StartSession::class,
        \Illuminate\View\Middleware\ShareErrorsFromSession::class,
//        \App\Http\Middleware\VerifyCsrfToken::class,
    ];

    /**
     * The application's route middleware.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth'          => \App\Http\Middleware\Authenticate::class,
        'auth.basic'    => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'guest'         => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'admin'         => \App\Http\Middleware\Admin::class,
        'subscribed'    => \App\Http\Middleware\Subscribed::class,
        'notsubscribed' => \App\Http\Middleware\NotSubscribed::class,
        'notcancelled'  => \App\Http\Middleware\NotCancelled::class,
        'cancelled'     => \App\Http\Middleware\Cancelled::class,
        'available_plan'=> \App\Http\Middleware\AvailablePlan::class,
        'verified'      => \App\Http\Middleware\VerifiedEmail::class,
    ];
}
