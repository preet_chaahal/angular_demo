<?php
Route::get('/', array(
    'as'   => 'front.home',
    'uses' => 'Front\PagesController@index')
);
Route::get('/terms', array(
    'as'   => 'front.terms',
    'uses' => 'Front\PagesController@terms')
);
Route::get('/contact', array(
    'as'   => 'front.contact',
    'uses' => 'Front\PagesController@contact')
);
Route::post('/contact/post', array(
    'as'   => 'front.contact.post',
    'uses' => 'Front\PagesController@contactPost'
));
Route::get('/privacy', array(
    'as'   => 'front.privacy',
    'uses' => 'Front\PagesController@privacy')
);
Route::get('/create', array(
    'as'   => 'front.csubscription.create',
    'uses' => 'Front\CsubscriptionController@create')
);
Route::get('/success', array(
    'as'   => 'front.csubscription.handlecallback',
    'uses' => 'Front\CsubscriptionController@handleCallback')
);
Route::post('/', array(
    'as'   => 'front.home',
    'uses' => 'Front\PagesController@index')
);
Route::group(['prefix' => 'subscription'], function() {
    Route::get('/free', array(
        'as'   => 'subscription.free',
        'uses' => 'Front\SubscriptionController@free')
    );
    Route::post('/free', array(
        'as'   => 'subscription.free',
        'uses' => 'Front\SubscriptionController@postFree')
    );
    Route::get('/thanks', array(
        'as'   => 'subscription.thanks',
        'uses' => 'Front\SubscriptionController@thanks')
    );
});

Route::get('/dashboard', array(
    'as'   => 'dashboard',
    'uses' => 'DashboardController@index')
);

Route::get('email',function (){
   return view('emails.subscription.ltd_cancel_subscription');
});
// Route::post('stripe/webhook', '\Laravel\Cashier\WebhookController@handleWebhook');
Route::post('stripe/webhook', 'Auth\StripeWebhookController@handleWebhook');

Route::group(['middleware' => 'auth'], function () {
    include 'Routes/API.php';
    include 'Routes/Tools.php';
    include 'Routes/Admin.php';
});

include 'Routes/Auth.php';
include 'Routes/Redirect.php';

Route::group(['prefix' => 'LTD', 'namespace' => 'Front'], function(){
    Route::get('',                  'LandingPageController@index'   )->name('front.pages.landing');
    Route::get('/Pricing',          'LandingPageController@pricing' )->name('front.pages.landing.pricing');
    Route::get('/Pricing/Checkout', 'LandingPageController@checkout')
         ->name('front.pages.landing.pricing.checkout')
         ->middleware('available_plan');

    Route::get('verify/email',      'LandingPageController@verify'  )->name('front.pages.landing.verify');
});


