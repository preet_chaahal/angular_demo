<?php

namespace App\Services;

use App\User;
use Exception;
use App\ApiLog;
use GuzzleHttp\Exception\BadResponseException;

/**
 * @property User $user
 */
class ApiLoggerService
{
    const ERROR_EMPTY_RESPONSE = 'ERROR_EMPTY_RESPONSE';

    const API_TYPE_KEYWORDS_SEARCH = 'API_TYPE_KEYWORDS_SEARCH';
    const API_TYPE_KEYWORDS_ADDITIONAL_SEARCH = 'API_TYPE_KEYWORDS_ADDITIONAL_SEARCH';
    const API_TYPE_SOCIAL_NETWORKS = 'API_TYPE_SOCIAL_NETWORKS';
    const API_TYPE_GOOGLE_MOZ = 'API_TYPE_GOOGLE_MOZ';

    protected $user;

    /**
     * @param User $user
     * @return $this
     */
    public function setUser(User $user)
    {
        $this->user = $user;

        return $user;
    }

    /**
     * @param Exception $error
     * @param array $params
     * @return ApiLog
     */
    public function logException(Exception $error, array $params = [])
    {
        $logModel = new ApiLog();
        $apiType = isset($params['apiType']) ? $params['apiType'] : null;
        $url = isset($params['url']) ? $params['url'] : null;

        if ($error instanceof BadResponseException) {
            $request = $error->getRequest();
            $response = $error->getResponse();

            if ( $request->getBody() ) {
                $request_body = (string) $request->getBody();
            }
            else {
                $parts = parse_url($url);
                parse_str($parts['query'], $query);
                $request_body = json_encode($query);
            }

            $logModel->fill([
                'url' => (string) $url,
                'user_id' => (int) $this->user->id,
                'request_body' => $request_body,
                'response_http_code' => (int) $response->getStatusCode(),
                'response_body' => (string) $response->getBody(),
                'system_error_code' => (string) ApiLog::API_INTERNAL_ERROR,
                'api_type' => (string) $apiType ?: null,
            ]);
        } else {
            $logModel->fill([
                'url' => $url,
                'user_id' => (int) $this->user->id,
                'response_body' => (string) $error->getMessage(),
                'api_type' => (string) $apiType ?: null,
                'system_error_code' => (string) ApiLog::API_DOES_NOT_RESPONDS,
            ]);
        }

        $logModel->save();

        return $logModel;
    }

    public function log(array $data = [])
    {
        $url = isset($data['url']) ? (string) $data['url'] : null;
        $error = isset($data['error']) ? json_encode($data['error']) : null;
        $apiType = isset($data['api_type']) ? (string) $data['api_type'] : null;
        $systemErrorCode = isset($data['systemErrorCode']) ?
            (string) $data['systemErrorCode'] : ApiLog::API_VALIDATION_ERROR;

        return ApiLog::create([
            'url' => $url,
            'api_type' => $apiType,
            'response_body' => $error,
            'user_id' => $this->user->id,
            'system_error_code' => $systemErrorCode,
            'request_body' => isset($data['request_body']) ? json_encode($data['request_body']) : null
        ]);
    }
}