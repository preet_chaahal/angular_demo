<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\TblBrainstormStatus
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $keyword
 * @property string $google_engine
 * @property integer $user_current_level
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\TblBrainstormStatus whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\TblBrainstormStatus whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\TblBrainstormStatus whereKeyword($value)
 * @method static \Illuminate\Database\Query\Builder|\App\TblBrainstormStatus whereGoogleEngine($value)
 * @method static \Illuminate\Database\Query\Builder|\App\TblBrainstormStatus whereUserCurrentLevel($value)
 * @method static \Illuminate\Database\Query\Builder|\App\TblBrainstormStatus whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\TblBrainstormStatus whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class TblBrainstormStatus extends Model
{
    protected $primaryKey = 'id';

    protected $table = 'tbl_brainstorm_status';

    protected $fillable = [
        'user_id',
        'keyword',
        'google_engine',
        'user_current_level',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */

}
