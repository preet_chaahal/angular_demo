<?php

namespace App;

/**
 * App\ResearchTmp
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $serialized_data
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read User $user
 * @method static \Illuminate\Database\Query\Builder|\App\ResearchTmp whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\ResearchTmp whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\ResearchTmp whereSerializedData($value)
 * @method static \Illuminate\Database\Query\Builder|\App\ResearchTmp whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\ResearchTmp whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ResearchTmp extends BaseModel
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'research_tmp';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'serialized_data',
        'project_name'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];


    /**
     * the keyword belongs to a user
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
