<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Seomoz
 *
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property string $ip_address
 * @property \Carbon\Carbon $last_accessed
 * @property boolean $active
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Seomoz whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Seomoz whereUsername($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Seomoz wherePassword($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Seomoz whereIpAddress($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Seomoz whereLastAccessed($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Seomoz whereActive($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Seomoz whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Seomoz whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Seomoz extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'seomoz';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username',
        'password',
        'ip_address',
        'last_accessed',
        'active',
    ];

    /**
     * dates
     * @var [type]
     */
    protected $dates = [
        'last_accessed'
    ];
}