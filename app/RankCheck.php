<?php

namespace App;

/**
 * App\RankCheck
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $group_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read User $user
 * @property-read SGroup $group
 * @property-read Casper $casper
 * @method static \Illuminate\Database\Query\Builder|\App\RankCheck whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\RankCheck whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\RankCheck whereGroupId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\RankCheck whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\RankCheck whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class RankCheck extends BaseModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'rankchecker';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'user_id', 'group_id'];

    /**
     * a rankcheck (tool) belongs to a user
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function group()
    {
        return $this->hasOne(SGroup::class, 'idGroup', 'group_id');
    }

    /**
     * the rank can have many casper events
     */
    public function casper()
    {
        // return Casper::where('group_id', $this->group_id);
        return $this->belongsTo(Casper::class, 'group_id', 'group_id');
    }
}
