<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Payment
 *
 * @property integer $id
 * @property integer $customer_id
 * @property string $transaction_id
 * @property string $token
 * @property string $profile_id
 * @property string $payment_data
 * @property string $payment_status
 * @property string $remain_usage
 * @property float $amount
 * @property string $payment_date
 * @property string $start_date
 * @property string $payment_type
 * @property string $plan_type
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Payment whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Payment whereCustomerId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Payment whereTransactionId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Payment whereToken($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Payment whereProfileId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Payment wherePaymentData($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Payment wherePaymentStatus($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Payment whereRemainUsage($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Payment whereAmount($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Payment wherePaymentDate($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Payment whereStartDate($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Payment wherePaymentType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Payment wherePlanType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Payment whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Payment whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Payment extends Model
{
    const PAYMENT_TYPE_STRIPE = 'stripe';
    const PAYMENT_TYPE_PAYPAL = 'paypal';

    protected $table = 'payment';
    protected $primaryKey = 'id';
}
