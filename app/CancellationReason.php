<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CancellationReason extends Model
{
    public $table = 'cancellation_reasons';

    protected $fillable = ['reason'];
    /*
    * We don't need timestamps in this table
    */
    public $timestamps = false;
}
