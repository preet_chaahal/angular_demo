<?php

namespace App;

/**
 * App\SLogs
 *
 * @property integer $idRun
 * @property string $dateStart
 * @property string $dateStop
 * @property string $logs
 * @property integer $pid
 * @property boolean $haveError
 * @method static \Illuminate\Database\Query\Builder|\App\SLogs whereIdRun($value)
 * @method static \Illuminate\Database\Query\Builder|\App\SLogs whereDateStart($value)
 * @method static \Illuminate\Database\Query\Builder|\App\SLogs whereDateStop($value)
 * @method static \Illuminate\Database\Query\Builder|\App\SLogs whereLogs($value)
 * @method static \Illuminate\Database\Query\Builder|\App\SLogs wherePid($value)
 * @method static \Illuminate\Database\Query\Builder|\App\SLogs whereHaveError($value)
 * @mixin \Eloquent
 */
class SLogs extends BaseModel
{
    public $timestamps = false;

    protected $connection = 'mysql_serposcope';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'run';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['idRun', 'dateStart', 'dateStop', 'logs', 'pid', 'haveError'];
}
