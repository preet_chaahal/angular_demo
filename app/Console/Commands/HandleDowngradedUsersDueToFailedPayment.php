<?php

namespace App\Console\Commands;

use Mail;
use App\Role;
use App\User;
use Carbon\Carbon;
use App\FailedPayment;
use Illuminate\Console\Command;
use App\Http\Controllers\Auth\StripeWebhookController;

class HandleDowngradedUsersDueToFailedPayment extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'HandleDowngradedUsersDueToFailedPayment:launch';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Downgrade Users with status "recurring_payment_suspended_due_to_max_failed_payment"';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $failedPayments = FailedPayment::where('created_at', '<', Carbon::now()->subDays(3)->toDateTimeString() )
                              ->where('downgraded', false)
                              ->get();

      $stripeWebhook = new StripeWebhookController;

      foreach ($failedPayments as $failed) {
        $user = $failed->user;
        if ($failed->gateway == 'paypal') {
          $user->toggle_free = 0;
          $user->stripe_active = 0;
          $user->pending_payment = 6;
          $user->account_id = 1;
          //$user->failed_payment_date = null;
          $user->save();
          $freeRole = Role::where('name', 'free')->first();
          if (!empty($freeRole)) {
            $user->roles()->sync([$freeRole->id]);
          }
        }

        /*
        * Downgrade Stripe User
        */
        if ($failed->gateway == 'stripe') {
          $stripeWebhook->DelayHandleInvoicePaymentFailed([
            'customer'   => $failed->customer,
            'invoice_id' => $failed->invoice_id
          ]);
        }

        /*
        * Save That the user has downgraded successfully.
        */
        $failed->downgraded = true;
        $failed->save();
      }
    }
}
