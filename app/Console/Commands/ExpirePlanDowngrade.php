<?php

namespace App\Console\Commands;

use App\Events\DownGradePlanRemoveBrainStormData;
use App\Events\DownGradePlanRemoveRankCheckData;
use App\Role;
use App\User;
use Carbon\Carbon;
use Event;
use Illuminate\Console\Command;

class ExpirePlanDowngrade extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ExpirePlanDowngrade:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';


    /**
     * ExpirePlanDowngrade constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
//        Log::info('------------# CHECKING EXPIRE PLAN DOWNGRADE AND UPDATE USERS ACCOUNT #-----------------');
//        Log::info(Carbon::now()->toDateTimeString());
        $Users = User::where('subscription_ends_at', '<', Carbon::now())->where('account_id', '!=', 1)->get();
        $freeRole = Role::where('name', 'free')->first();
        if (!empty($Users) && !empty($freeRole)) {

            foreach ($Users as $User) {

                $User->account_id = 1;
                $User->stripe_active = 0;
                $User->subscription_ends_at = null;
                $User->save();
                $User->roles()->sync([$freeRole->id]);

                if (!$User->can('brainstorm_tool')) {
                    //
                    Event::fire(new DownGradePlanRemoveBrainStormData($User));
                }
                if (!$User->can('rank_checker')) {
                    Event::fire(new DownGradePlanRemoveRankCheckData($User));
                }
            }
        }
//        Log::info('------------# CHECKING EXPIRE PLAN DOWNGRADE RESULT #-----------------');
    }
}
