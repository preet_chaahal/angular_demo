<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\Inspire::class,
        \App\Console\Commands\ExpirePlanDowngrade::class,
        \App\Console\Commands\HandleDowngradedUsersDueToFailedPayment::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('inspire')
                 ->hourly();
        $schedule->command('ExpirePlanDowngrade:check')->daily();

        /*
        * Downgrade Users due to failed payment after three days from their failed date
        * this task will run daily at midnight
        */
        $schedule->command('HandleDowngradedUsersDueToFailedPayment:launch')
                 ->daily();
    }
}
