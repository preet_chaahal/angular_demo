<?php

namespace App;

/**
 * App\STarget
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\SEvent[] $events
 * @property-read \App\SGroup $group
 * @property integer $idTarget
 * @property integer $idGroup
 * @property string $name
 * @property string $info
 * @method static \Illuminate\Database\Query\Builder|\App\STarget whereIdTarget($value)
 * @method static \Illuminate\Database\Query\Builder|\App\STarget whereIdGroup($value)
 * @method static \Illuminate\Database\Query\Builder|\App\STarget whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\STarget whereInfo($value)
 * @mixin \Eloquent
 */
class STarget extends BaseModel
{

    public $timestamps = false;
    protected $primaryKey = 'idTarget';

    protected $connection = 'mysql_serposcope';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'target';

    /**
     * the dates on the table
     */
    protected $dates = [];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['idGroup', 'name'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];


    public function events()
    {
        return $this->hasMany(SEvent::class, 'idTarget');
    }

    public function group()
    {
        return $this->belongsTo(SGroup::class, 'idGroup');
    }

}
