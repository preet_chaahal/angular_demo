<?php

namespace App\Events;

use App\User;
use Illuminate\Queue\SerializesModels;
use PayPal\Api\Plan;

class DownGradePlanRemoveBrainStormData extends Event
{
    use SerializesModels;
    public $user;


    /**
     * DownGradePlanRemoveBrainStormData constructor.
     */
    public function __construct(User $user)
    {
        $this->user=$user;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}

