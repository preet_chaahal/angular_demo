<?php

namespace App\Events;

use App\Events\Event;
use App\User;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class DownGradePlanRemoveRankCheckData extends Event
{
    use SerializesModels;
    public $user;

    /**
     * DownGradePlanRemoveRankCheckData constructor.
     */
    public function __construct(User $user)
    {
        $this->user=$user;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
