<?php

namespace App;

use Hash;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Laravel\Cashier\Billable;
use Laravel\Cashier\Contracts\Billable as BillableContract;
use PayPal\Api\Plan;
use Zizaco\Entrust\Contracts\EntrustUserInterface;
use Zizaco\Entrust\Traits\EntrustUserTrait;
//use TijmenWierenga\LaravelChargebee\Billable as BillableChargebee;
//use TijmenWierenga\LaravelChargebee\HandlesWebhooks;
/**
 * App\User
 *
 * @property integer $id
 * @property string $name
 * @property string $username
 * @property string $email
 * @property string $password
 * @property string $is_admin
 * @property string $remember_token
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property boolean $stripe_active
 * @property string $stripe_id
 * @property string $stripe_subscription
 * @property string $stripe_plan
 * @property string $last_four
 * @property \Carbon\Carbon $trial_ends_at
 * @property \Carbon\Carbon $subscription_ends_at
 * @property string $defaultSearchEngine
 * @property boolean $is_registered
 * @property string $register_token
 * @property integer $account_id
 * @property \Carbon\Carbon $last_login
 * @property string $ip
 * @property string $disabled
 * @property string $toggle_free
 * @property-read \Illuminate\Database\Eloquent\Collection|RankCheck[] $toolRankChecks
 * @property-read \Illuminate\Database\Eloquent\Collection|Brainstorm[] $brainstorms
 * @property-read \Illuminate\Database\Eloquent\Collection|Research[] $research
 * @property-read \Illuminate\Database\Eloquent\Collection|TblKeywordStatus[] $keywordStatus
 * @property-read \Illuminate\Database\Eloquent\Collection|TblEvaluationStatus[] $evaluationStatus
 * @property-read \Illuminate\Database\Eloquent\Collection|Evaluation[] $evaluation
 * @property-read mixed $gravatar
 * @property-read Account $account
 * @property string $payment_type
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Role[] $roles
 * @method static \Illuminate\Database\Query\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereUsername($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereIsAdmin($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereRememberToken($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereStripeActive($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereStripeId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereStripeSubscription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereStripePlan($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereLastFour($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereTrialEndsAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereSubscriptionEndsAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereDefaultSearchEngine($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereIsRegistered($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereRegisterToken($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereAccountId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereLastLogin($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereIp($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereDisabled($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereToggleFree($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User wherePaymentType($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\ResearchKeyword[] $researchKeyword
 */
class User extends BaseModel implements AuthenticatableContract,
    AuthorizableContract,
    CanResetPasswordContract,
    BillableContract, EntrustUserInterface
{
    use Authenticatable, Authorizable, CanResetPassword, EntrustUserTrait {
        EntrustUserTrait::can insteadof Authorizable;
    }
    use Billable;
    
    protected $dates = ['trial_ends_at', 'subscription_ends_at', 'last_login'];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'username',
        'email',
        'password',
        'defaultSearchEngine',
        'defaultSearchLanguage',
        'default_location',
        'is_registered',
        'is_admin',
        'register_token',
        'account_id',
        'last_login',
        'ip',
        'disabled',
        'account_id',
        'toggle_free',
        'payment_type',
        'campaign',
        'email_status',
        'is_trial',
        'trial_expire_date',
        'used_trial_period'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'is_registered' => 'boolean',
        'is_admin'      => 'boolean',
        'disabled'      => 'boolean'
    ];


    /**
     * a user has many ranktracker items
     */
    public function toolRankChecks()
    {
        return $this->hasMany(RankCheck::class);
    }

    /**
     * a user has many evaluation items
     */
    public function evaluation()
    {
        return $this->hasMany(Evaluation::class);
    }


    /**
     * a user has many keyword status items
     */
    public function keywordStatus()
    {
        return $this->hasMany(TblKeywordStatus::class);
    }
    public function researchKeyword()
    {
        return $this->hasMany(ResearchKeyword::class);
    }

    /**
     * a user has many evaluation status items
     */
    public function evaluationStatus()
    {
        return $this->hasMany(TblEvaluationStatus::class);
    }

    /**
     * the user has many brainstorm items
     */
    public function brainstorms()
    {
        return $this->hasMany(Brainstorm::class);
    }

    /**
     * the user can have many research (keywords)
     */
    public function research()
    {
        return $this->hasMany(Research::class);
    }

    /**
     * the user's gravatar image
     */
    public function getGravatarAttribute()
    {
        $hash = md5(strtolower(trim($this->attributes['email'])));

        return "https://secure.gravatar.com/avatar/$hash?d=mm";
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function account()
    {
        return $this->belongsTo(Account::class);
    }

    public function isCurrentPlan($name)
    {
        if($this->is_trial == 1) {
            //$comparedPlan = Account::getPlanBySystemId($name);
            $currentPlan = Account::getPlanBySystemId($name);
            if (!isset($currentPlan['id']) || !isset($this->account_id)) {
                return false;
            }
            return $currentPlan->id === $this->account_id;
        } else if($this->payment_type == getenv('CHARGEBEE_TYPE') && $this->chargebee_active == 1) {
            $comparedPlan = Account::getPlanBySystemId($this->chargebee_plan);
            $currentPlan = Account::getPlanBySystemId($name);

            if (in_array(trim($name), ['', 'free']) && !$this->chargebee_plan) {
                return true;
            }
            if (!$currentPlan || !$comparedPlan) {
                return false;
            }
            return $currentPlan->id === $comparedPlan->id;
        } else {
            $comparedPlan = Account::getPlanBySystemId($this->stripe_plan);
            $currentPlan = Account::getPlanBySystemId($name);

            if (in_array(trim($name), ['', 'free']) && !$this->stripe_plan) {
                return true;
            }
            if (!$currentPlan || !$comparedPlan) {
                return false;
            }
            return $currentPlan->id === $comparedPlan->id;
        }
    }

    /**
     * Compares given password with User's password hash with several hashing algorithms
     *
     * @param $password
     * @return bool
     */
    public function checkPassword($password)
    {
        $checkerFunctions = [
            function ($password, $hash) {
                return Hash::check($password, $hash);
            },

            function ($password, $hash) {
                return hash_equals(md5($password), $hash);
            }
        ];

        foreach ($checkerFunctions as $checkerFunction) {
            if ($checkerFunction($password, $this->password) === true) {
                return true;
            }
        }

        return false;
    }
}
