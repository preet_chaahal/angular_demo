<?php

namespace App;

/**
 * App\Evaluation
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $keyword
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\User $user
 * @property integer $keyword_search_id
 * @method static \Illuminate\Database\Query\Builder|\App\Evaluation whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Evaluation whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Evaluation whereKeyword($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Evaluation whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Evaluation whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Evaluation whereKeywordSearchId($value)
 * @mixin \Eloquent
 * @property string $location
 * @property integer $monthly_searches
 * @property float $keyword_cpc
 * @property float $estimated_profile
 * @property integer $word_count
 * @property string $google_engine
 * @property integer $user_current_level
 * @property integer $difficulty
 * @method static \Illuminate\Database\Query\Builder|\App\Evaluation whereLocation($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Evaluation whereMonthlySearches($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Evaluation whereKeywordCpc($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Evaluation whereEstimatedProfile($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Evaluation whereWordCount($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Evaluation whereGoogleEngine($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Evaluation whereUserCurrentLevel($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Evaluation whereDifficulty($value)
 */
class Evaluation extends BaseModel
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'evaluations';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'keyword',
        'monthly_searches',
        'keyword_cpc',
        'estimated_profile',
        'word_count',
        'location',
        'difficulty',
        'keyword_search_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];


    /**
     * the evaluation belongs to a user
     */
    public function user()
    {
        return $this->belongsTo(\App\User::class);
    }

}
