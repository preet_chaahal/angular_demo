<?php

namespace App;

/**
 * App\SEvent
 *
 * @property integer $idEvent
 * @property integer $idTarget
 * @property string $date
 * @property string $event
 * @method static \Illuminate\Database\Query\Builder|\App\SEvent whereIdEvent($value)
 * @method static \Illuminate\Database\Query\Builder|\App\SEvent whereIdTarget($value)
 * @method static \Illuminate\Database\Query\Builder|\App\SEvent whereDate($value)
 * @method static \Illuminate\Database\Query\Builder|\App\SEvent whereEvent($value)
 * @mixin \Eloquent
 */
class SEvent extends BaseModel
{

    public $timestamps = false;
    protected $primaryKey = 'idEvent';

    protected $connection = 'mysql_serposcope';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'event';

    /**
     * the dates on the table
     */
    protected $dates = [];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['idTarget', 'date', 'event'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

}
