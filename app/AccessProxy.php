<?php
/**
 * Created by PhpStorm.
 * User: harshadvala
 * Date: 1/20/2016
 * Time: 6:42 PM
 */

namespace App;


/**
 * App\AccessProxy
 *
 * @property integer $id
 * @property string $url
 * @property float $access_time
 * @property boolean $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\SProxy $proxy
 * @method static \Illuminate\Database\Query\Builder|\App\AccessProxy whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\AccessProxy whereUrl($value)
 * @method static \Illuminate\Database\Query\Builder|\App\AccessProxy whereAccessTime($value)
 * @method static \Illuminate\Database\Query\Builder|\App\AccessProxy whereStatus($value)
 * @method static \Illuminate\Database\Query\Builder|\App\AccessProxy whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\AccessProxy whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string $type
 * @property string $port
 * @property string $user
 * @property string $password
 * @property integer $module 1 = keyword Research, 2 = Brainstorm, 3 = Rank Tracker
 * @method static \Illuminate\Database\Query\Builder|\App\AccessProxy whereType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\AccessProxy wherePort($value)
 * @method static \Illuminate\Database\Query\Builder|\App\AccessProxy whereUser($value)
 * @method static \Illuminate\Database\Query\Builder|\App\AccessProxy wherePassword($value)
 * @method static \Illuminate\Database\Query\Builder|\App\AccessProxy whereModule($value)
 * @property string $ip
 * @method static \Illuminate\Database\Query\Builder|\App\AccessProxy whereIp($value)
 */
class AccessProxy extends BaseModel
{
    protected $table = 'tbl_access_proxy';


    protected $fillable = ['ip', 'access_time', 'status','type', 'port', 'user', 'password','module'];

    public function proxy()
    {
        return $this->belongsTo(SProxy::class, 'url', 'ip');
    }

}