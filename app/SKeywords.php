<?php

namespace App;

/**
 * App\SKeywords
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\SRank[] $rank
 * @property integer $idKeyword
 * @property integer $idGroup
 * @property string $name
 * @property integer $user_id
 * @method static \Illuminate\Database\Query\Builder|\App\SKeywords whereIdKeyword($value)
 * @method static \Illuminate\Database\Query\Builder|\App\SKeywords whereIdGroup($value)
 * @method static \Illuminate\Database\Query\Builder|\App\SKeywords whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\SKeywords whereUserId($value)
 * @mixin \Eloquent
 */
class SKeywords extends BaseModel
{

    public $timestamps = false;
    protected $primaryKey = 'idKeyword';

    protected $connection = 'mysql_serposcope';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'keyword';

    /**
     * the dates on the table
     */
    protected $dates = [];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['idGroup', 'name','user_id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];


    public function rank()
    {
        return $this->hasMany(SRank::class, 'idKeyword');
    }

}
