<?php

namespace App;

/**
 * App\SCheck
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|SRank[] $ranks
 * @property integer $id
 * @property integer $role_id
 * @property integer $research
 * @property integer $evolution
 * @property integer $brainstorm
 * @property integer $tracking
 * @property integer $can_save
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\RoleLimit whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\RoleLimit whereRoleId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\RoleLimit whereResearch($value)
 * @method static \Illuminate\Database\Query\Builder|\App\RoleLimit whereEvolution($value)
 * @method static \Illuminate\Database\Query\Builder|\App\RoleLimit whereBrainstorm($value)
 * @method static \Illuminate\Database\Query\Builder|\App\RoleLimit whereTracking($value)
 * @method static \Illuminate\Database\Query\Builder|\App\RoleLimit whereCanSave($value)
 * @method static \Illuminate\Database\Query\Builder|\App\RoleLimit whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\RoleLimit whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class RoleLimit extends BaseModel
{

    protected $primaryKey = 'id';


    protected $table = 'role_limit';


}
