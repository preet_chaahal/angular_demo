<?php

namespace App;

/**
 * App\Research
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $keyword
 * @property integer $monthly_searches
 * @property float $keyword_cpc
 * @property float $estimated_profile
 * @property integer $word_count
 * @property string $location
 * @property integer $difficulty
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read User $user
 * @property integer $keyword_search_id
 * @property-read \App\TblKeywordStatus $keyword_status
 * @method static \Illuminate\Database\Query\Builder|\App\Research whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Research whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Research whereKeyword($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Research whereMonthlySearches($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Research whereKeywordCpc($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Research whereEstimatedProfile($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Research whereWordCount($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Research whereLocation($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Research whereDifficulty($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Research whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Research whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Research whereKeywordSearchId($value)
 * @mixin \Eloquent
 * @property string $google_engine
 * @property integer $user_current_level
 * @method static \Illuminate\Database\Query\Builder|\App\Research whereGoogleEngine($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Research whereUserCurrentLevel($value)
 */
class Research extends BaseModel
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'research';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'keyword',
        'monthly_searches',
        'keyword_cpc',
        'estimated_profile',
        'word_count',
        'location',
        'language',
        'difficulty',
        'keyword_search_id',
        'user_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];


    /**
     * the keyword belongs to a user
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function keyword_status()
    {
        return $this->belongsTo(ResearchKeyword::class,'keyword_search_id');
    }

}
