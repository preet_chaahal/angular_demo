<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cbinvoice extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'cb_invoices';
    public $timestamps = false;
    protected $fillable = [
		'user_id',
		'user_plan_id',
		'cb_subscription_id',
		'cb_customer_id',
		'cb_transaction_id',
		'invoice_id',
		'amount',
		'date_created',
		'start_date',
		'end_date',
		'status',
    ];
}
