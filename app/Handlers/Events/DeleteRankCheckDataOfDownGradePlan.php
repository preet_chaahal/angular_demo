<?php

namespace App\Handlers\Events;

use App\Brainstorm;
use App\Events\DownGradePlanRemoveRankCheckData;
use App\Exceptions\ApiOperationFailedException;
use App\RankCheck;
use App\SGroup;
use App\SKeywords;
use App\STarget;
use App\TblBrainstormStatus;
use Auth;
use DB;
use Illuminate\Database\QueryException;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class DeleteRankCheckDataOfDownGradePlan
{

    /**
     * DeleteRankCheckDataOfDownGradePlan constructor.
     */
    public function __construct()
    {
        //
    }


    /**
     * @param DownGradePlanRemoveRankCheckData $event
     * @throws ApiOperationFailedException
     * @throws \Exception
     */
    public function handle(DownGradePlanRemoveRankCheckData $event)
    {
        try {
            DB::beginTransaction();
            $user_id = $event->user->id;
            $group=RankCheck::whereUserId($user_id)->get();
            foreach ($group as $k)
            {
                $group_id= $k->group_id;
                $target=STarget::where('IdGroup',$group_id)->delete();
                $group=SGroup::where('IdGroup',$group_id)->delete();
            }
            $keyword=SKeywords::whereUserId($user_id);
            $keyword->delete();
            $page_rank = RankCheck::whereUserId($user_id);
            $page_rank->delete();
            DB::commit();

        } catch (QueryException $e) {
            DB::rollBack();
            throw new ApiOperationFailedException($e->getMessage());
        }

    }
}
