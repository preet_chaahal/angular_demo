<?php

namespace App\Handlers\Events;

use App\Brainstorm;
use App\Events\DownGradePlanRemoveBrainStormData;
use App\Exceptions\ApiOperationFailedException;
use App\RankCheck;
use App\SGroup;
use App\SKeywords;
use App\STarget;
use App\TblBrainstormStatus;
use Auth;
use DB;
use Illuminate\Database\QueryException;


class DeleteBrainStormDataOfDownGradePlan
{

    /**
     * DeleteBrainStormDataOfDownGradePlan constructor.
     */
    public function __construct()
    {
        //
    }


    /**
     * @param DownGradePlanRemoveBrainStormData $event
     * @throws ApiOperationFailedException
     */
    public function handle(DownGradePlanRemoveBrainStormData $event)
    {
        try {
            DB::beginTransaction();
            $user_id = $event->user->id;
            $brainstorm_detail = Brainstorm::whereUserId($user_id);
            $brainstorm_detail->delete();
            $brainstorm_status = TblBrainstormStatus::whereUserId($user_id);
            $brainstorm_status->delete();
            DB::commit();

        } catch (QueryException $e) {
            DB::rollBack();
            throw new ApiOperationFailedException($e->getMessage());
        }

    }
}
