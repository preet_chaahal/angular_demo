<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $primaryKey = 'id';

    protected $table = 'invoices';

    protected $fillable = [
		'user_id',
		'customer_id',
		'subscription_id',
		'invoice_id',
		'is_recuring',
		'plan_id',
		'status',
		'amount',
		'date',
		'start_date',
		'end_date'
    ];
}
