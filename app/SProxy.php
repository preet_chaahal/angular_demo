<?php

namespace App;

/**
 * App\SProxy
 *
 * @property integer $id
 * @property string $type
 * @property string $ip
 * @property string $port
 * @property string $user
 * @property string $password
 * @property integer $module 1 = keyword Research, 2 = Brainstorm, 3 = Rank Tracker
 * @property-read \App\AccessProxy $accessProxy
 * @method static \Illuminate\Database\Query\Builder|\App\SProxy whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\SProxy whereType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\SProxy whereIp($value)
 * @method static \Illuminate\Database\Query\Builder|\App\SProxy wherePort($value)
 * @method static \Illuminate\Database\Query\Builder|\App\SProxy whereUser($value)
 * @method static \Illuminate\Database\Query\Builder|\App\SProxy wherePassword($value)
 * @method static \Illuminate\Database\Query\Builder|\App\SProxy whereModule($value)
 * @mixin \Eloquent
 */
class SProxy extends BaseModel
{
    public $timestamps = false;
    protected $primaryKey = 'id';

    protected $connection = 'mysql_serposcope';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'proxy';

    /**
     * the dates on the table
     */
    protected $dates = [];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['type', 'ip', 'port', 'user', 'password','module'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    public function accessProxy()
    {
        return $this->hasOne(AccessProxy::class, 'url', 'ip');
    }
}
