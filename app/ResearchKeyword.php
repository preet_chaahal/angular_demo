<?php

namespace App;


/**
 * App\ResearchKeyword
 *
 * @property integer $id
 * @property string $keyword
 * @property integer $user_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Query\Builder|\App\ResearchKeyword whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\ResearchKeyword whereKeyword($value)
 * @method static \Illuminate\Database\Query\Builder|\App\ResearchKeyword whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\ResearchKeyword whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\ResearchKeyword whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Research[] $research
 * @property string $google_engine
 * @property integer $user_current_level
 * @method static \Illuminate\Database\Query\Builder|\App\ResearchKeyword whereGoogleEngine($value)
 * @method static \Illuminate\Database\Query\Builder|\App\ResearchKeyword whereUserCurrentLevel($value)
 */
class ResearchKeyword extends BaseModel
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'research_keywords';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'keyword',
        'google_engine',
        'language',
        'user_current_level'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];


    /**
     * the keyword belongs to a user
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function research()
    {
        return $this->hasMany(Research::class, 'keyword_search_id');
    }
}
