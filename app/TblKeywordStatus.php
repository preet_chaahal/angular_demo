<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\TblKeywordStatus
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $keyword
 * @property string $google_engine
 * @property integer $user_current_level
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $location
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\TblEvaluationStatus[] $evalute
 * @method static \Illuminate\Database\Query\Builder|\App\TblKeywordStatus whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\TblKeywordStatus whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\TblKeywordStatus whereKeyword($value)
 * @method static \Illuminate\Database\Query\Builder|\App\TblKeywordStatus whereGoogleEngine($value)
 * @method static \Illuminate\Database\Query\Builder|\App\TblKeywordStatus whereUserCurrentLevel($value)
 * @method static \Illuminate\Database\Query\Builder|\App\TblKeywordStatus whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\TblKeywordStatus whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\TblKeywordStatus whereLocation($value)
 * @mixin \Eloquent
 */
class TblKeywordStatus extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'tbl_keyword_search_status';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function evalute()
    {
        return $this->hasMany(TblEvaluationStatus::class, 'keyword_search_id');
    }
   /* public function research()
    {
        return $this->hasMany(Research::class, 'keyword_search_id');
    }*/
}
