<?php

namespace App;

/**
 * App\CasperResult
 *
 * @property integer $id
 * @property integer $casper_id
 * @property string $ad_group
 * @property string $keyword
 * @property string $currency
 * @property string $avg_month_searches
 * @property string $competition
 * @property string $suggested_bid
 * @property string $impression_share
 * @property string $organic_impression_share
 * @property string $organic_avg_position
 * @property string $in_account
 * @property string $in_plan
 * @property string $extracted_from
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read Casper $casper
 * @method static \Illuminate\Database\Query\Builder|\App\CasperResult whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\CasperResult whereCasperId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\CasperResult whereAdGroup($value)
 * @method static \Illuminate\Database\Query\Builder|\App\CasperResult whereKeyword($value)
 * @method static \Illuminate\Database\Query\Builder|\App\CasperResult whereCurrency($value)
 * @method static \Illuminate\Database\Query\Builder|\App\CasperResult whereAvgMonthSearches($value)
 * @method static \Illuminate\Database\Query\Builder|\App\CasperResult whereCompetition($value)
 * @method static \Illuminate\Database\Query\Builder|\App\CasperResult whereSuggestedBid($value)
 * @method static \Illuminate\Database\Query\Builder|\App\CasperResult whereImpressionShare($value)
 * @method static \Illuminate\Database\Query\Builder|\App\CasperResult whereOrganicImpressionShare($value)
 * @method static \Illuminate\Database\Query\Builder|\App\CasperResult whereOrganicAvgPosition($value)
 * @method static \Illuminate\Database\Query\Builder|\App\CasperResult whereInAccount($value)
 * @method static \Illuminate\Database\Query\Builder|\App\CasperResult whereInPlan($value)
 * @method static \Illuminate\Database\Query\Builder|\App\CasperResult whereExtractedFrom($value)
 * @method static \Illuminate\Database\Query\Builder|\App\CasperResult whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\CasperResult whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class CasperResult extends BaseModel
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'casper_results';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'casper_id',
        'ad_group',
        'keyword',
        'currency',
        'avg_month_searches',
        'competition',
        'suggested_bid',
        'impression_share',
        'organic_impression_share',
        'organic_avg_position',
        'in_account',
        'in_plan',
        'extracted_from'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];


    /**
     * the casper result belongs to a casper object
     */
    public function casper()
    {
        return $this->belongsTo(Casper::class);
    }

}
