<?php

namespace App;

/**
 * App\RegistrationManagement
 *
 * @property integer $id
 * @property string $ip
 * @property string $email_domain
 * @property string $disabled
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\RegistrationManagement whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\RegistrationManagement whereIp($value)
 * @method static \Illuminate\Database\Query\Builder|\App\RegistrationManagement whereEmailDomain($value)
 * @method static \Illuminate\Database\Query\Builder|\App\RegistrationManagement whereDisabled($value)
 * @method static \Illuminate\Database\Query\Builder|\App\RegistrationManagement whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\RegistrationManagement whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class RegistrationManagement extends BaseModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'registration';

    /**
     * the dates on the table
     */
    protected $dates = [];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['ip', 'email_domain', 'disabled'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

}
