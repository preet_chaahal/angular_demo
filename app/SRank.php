<?php

namespace App;

/**
 * App\SRank
 *
 * @method static \Illuminate\Database\Query\Builder|\App\SRank keyword()
 * @property integer $idCheck
 * @property integer $idTarget
 * @property integer $idKeyword
 * @property integer $position
 * @property string $url
 * @method static \Illuminate\Database\Query\Builder|\App\SRank whereIdCheck($value)
 * @method static \Illuminate\Database\Query\Builder|\App\SRank whereIdTarget($value)
 * @method static \Illuminate\Database\Query\Builder|\App\SRank whereIdKeyword($value)
 * @method static \Illuminate\Database\Query\Builder|\App\SRank wherePosition($value)
 * @method static \Illuminate\Database\Query\Builder|\App\SRank whereUrl($value)
 * @mixin \Eloquent
 */
class SRank extends BaseModel
{

    public $timestamps = false;
    protected $primaryKey = 'idRank';

    protected $connection = 'mysql_serposcope';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'rank';

    /**
     * the dates on the table
     */
    protected $dates = [];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['idTarget', 'idKeyword', 'position', 'url'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    public function scopeKeyword()
    {
        return \App\SKeywords::where('idKeyword', $this->idKeyword);
        // return $this->belongsTo(\App\SKeywords::class, 'idKeyword');
    }

}
