<?php

namespace App;

/**
 * App\Casper
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $group_id
 * @property integer $brainstorm_id
 * @property string $upload_file
 * @property string $download_file
 * @property boolean $is_uploaded
 * @property boolean $is_downloaded
 * @property string $uploaded_date
 * @property string $downloaded_date
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property boolean $is_processing
 * @property-read User $user
 * @property-read RankCheck $ranktracker
 * @property-read Brainstorm $brainstorm
 * @property-read \Illuminate\Database\Eloquent\Collection|CasperResult[] $results
 * @method static \Illuminate\Database\Query\Builder|\App\Casper whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Casper whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Casper whereGroupId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Casper whereBrainstormId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Casper whereUploadFile($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Casper whereDownloadFile($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Casper whereIsUploaded($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Casper whereIsDownloaded($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Casper whereUploadedDate($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Casper whereDownloadedDate($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Casper whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Casper whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Casper whereIsProcessing($value)
 * @mixin \Eloquent
 */
class Casper extends BaseModel
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'casper';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'group_id',
        'brainstorm_id',
        'upload_file',
        'download_file',
        'is_uploaded',
        'is_downloaded',
        'uploaded_date',
        'downloaded_date'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];


    /**
     * the casper item belongs to a user
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * the casper item can have a rank tracker
     */
    public function ranktracker()
    {
        return $this->belongsTo(RankCheck::class);
    }

    /**
     * the casper item can have a brainstorm
     */
    public function brainstorm()
    {
        return $this->belongsTo(Brainstorm::class);
    }

    /**
     * the casper item has many results
     */
    public function results()
    {
        return $this->hasMany(CasperResult::class);
    }
}
