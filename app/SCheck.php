<?php

namespace App;

/**
 * App\SCheck
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|SRank[] $ranks
 * @property integer $idCheck
 * @property integer $idGroup
 * @property integer $idRun
 * @property \Carbon\Carbon $date
 * @method static \Illuminate\Database\Query\Builder|\App\SCheck whereIdCheck($value)
 * @method static \Illuminate\Database\Query\Builder|\App\SCheck whereIdGroup($value)
 * @method static \Illuminate\Database\Query\Builder|\App\SCheck whereIdRun($value)
 * @method static \Illuminate\Database\Query\Builder|\App\SCheck whereDate($value)
 * @mixin \Eloquent
 */
class SCheck extends BaseModel
{

    public $timestamps = false;
    protected $primaryKey = 'idCheck';

    protected $connection = 'mysql_serposcope';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'check';

    /**
     * the dates on the table
     */
    protected $dates = ['date'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['idGroup', 'idRun', 'date'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    public function ranks()
    {
        return $this->hasMany(SRank::class, 'idCheck');
    }

}
