<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\UnsavedResearch
 *
 * @mixin \Eloquent
 */
class UnsavedResearch extends Model
{
    protected $primaryKey = 'id';

    protected $table = 'unsaved_research';

    protected $fillable = [
        'user_id',
        'keyword',
        'project_name',
        'monthly_searches',
        'keyword_cpc',
        'estimated_profile',
        'word_count',
        'location',
        'language',
        'difficulty',
        'google_engine'
    ];
    
}
