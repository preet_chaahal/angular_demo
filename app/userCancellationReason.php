<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class userCancellationReason extends Model
{
    protected $table = 'user_cancellation_reasons';

    protected $fillable = ['cancellation_id', 'reason_id', 'other'];

    public $timestamps = false;
}
