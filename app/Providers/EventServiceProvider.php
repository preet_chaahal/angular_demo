<?php

namespace App\Providers;

use App\Events\DownGradePlanRemoveBrainStormData;
use App\Events\DownGradePlanRemoveRankCheckData;
use App\Handlers\Events\DeleteBrainStormDataOfDownGradePlan;
use App\Handlers\Events\DeleteRankCheckDataOfDownGradePlan;
use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
//    protected $listen = [
//        'App\Events\DownGradePlan' => [
//            'App\Listeners\DeleteDataOfDownGradePlan',
//        ],
//    ];
    protected $listen = [
        DownGradePlanRemoveBrainStormData::class => [
            DeleteBrainStormDataOfDownGradePlan::class,

        ],
        DownGradePlanRemoveRankCheckData::class => [
            DeleteRankCheckDataOfDownGradePlan::class,

        ]
    ];

    /**
     * Register any other events for your application.
     *
     * @param  \Illuminate\Contracts\Events\Dispatcher $events
     * @return void
     */
    public function boot(DispatcherContract $events)
    {
        parent::boot($events);

        //
    }
}
