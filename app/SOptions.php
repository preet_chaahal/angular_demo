<?php

namespace App;

/**
 * App\SOptions
 *
 * @property string $name
 * @property string $value
 * @method static \Illuminate\Database\Query\Builder|\App\SOptions whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\SOptions whereValue($value)
 * @mixin \Eloquent
 */
class SOptions extends BaseModel
{
    public $timestamps = false;

    protected $connection = 'mysql_serposcope';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'option';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'value'];
}
