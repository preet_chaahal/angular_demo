<?php

namespace App;

/**
 * App\Account
 *
 * @property integer $id
 * @property string $name
 * @property string $stripe_plan
 * @property string $paypal_plan
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $description
 * @property-read \Illuminate\Database\Eloquent\Collection|User[] $users
 * @method static \Illuminate\Database\Query\Builder|Account basicAccount()
 * @method static \Illuminate\Database\Query\Builder|\App\Account whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Account whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Account whereStripePlan($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Account wherePaypalPlan($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Account whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Account whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Account whereDescription($value)
 * @mixin \Eloquent
 */
class Account extends BaseModel
{
    const PERIOD_YEAR = 'YEAR';
    const PERIOD_MONTH = 'Month';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'accounts';

    /**
     * the dates on the table
     */
    protected $dates = [];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'stripe_plan', 'paypal_plan', 'description'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * return the free account id
     */
    public static function FreeAccountId()
    {
        return Account::where('name', 'free')->first()->id;
    }

    public function users()
    {
        return $this->hasMany(User::class);
    }

    public function scopeBasicAccount($query)
    {
        return $query->where('name', 'basic')->first();
    }

    /**
     * @param $planName
     * @return self
     */
    public static function getByPlanName($planName)
    {
        return self::where('stripe_plan', $planName)->firstOrFail();
    }

    /**
     * @param $planName
     * @return self
     */
    public static function getByPaypalPlanName($planName)
    {
        return self::where('paypal_plan', $planName)->where('disabled',0)->firstOrFail();
    }

    /**
     * @param $accountName
     * @return self
     */
    public static function getByAccountName($accountName)
    {
        return self::where('name', $accountName)->firstOrFail();
    }

    /**
     * @return Role|null
     */
    public function getRole()
    {
        $roles = Role::all();

        foreach ($roles as $role) {
            //if (strpos($this->name, $role->name) === 0) {
            if ($this->name == $role->name) {
                return $role;
            }
        }
        return null;
    }

    /**
     * @param $systemId
     * @return self
     */
    public static function getPlanBySystemId($systemId)
    {
        switch ($systemId) {
            case 'basic':
                return self::whereName('basic')->first();
            case 'pro-monthly':
                return self::whereName('pro')->first();
            case 'pro-yearly':
                return self::whereName('proyearly')->first();
            default:
                return self::where('name', $systemId)
                    ->orWhere('stripe_plan', $systemId)
                    ->orWhere('paypal_plan', $systemId)
                    ->orWhere('description', $systemId)
                    ->first();
        }
    }

    public static function getPlans()
    {
        $accounts = Account::all();
        foreach ($accounts as $account) {
            $accountsData[$account->id] = $account->description;
        }
        return $accountsData;
    }

    public static function getStripePlanById($id)
    {
        return self::where('id', $id)->first()->stripe_plan;
    }

    public function getPrice()
    {
        switch ($this->name) {
            case 'basic':
                return 19.0;
            case 'pro':
                return 39.0;
            case 'proyearly':
                return 234.0;
            case 'elite':
                return 69.0;
            case 'agency':
                return 150.0;
            case 'basic_yearly':
                return 114.0;
            case 'elite_yearly':
                return 414.0;
            case 'agency_yearly':
                return 900.0;

            /*
            * New plans prices
            */
            case 'new_basic_monthly':
                return 9.97;
            case 'new_basic_yearly':
                return 90.0;
            case 'new_pro_monthly':
                return 27.97;
            case 'new_pro_yearly':
                return 210.0;
            case 'new_elite_monthly':
                return 47.97;
            case 'new_elite_yearly':
                return 354.0;

            /*
            * New Campaign plans prices
            */
            case 'lifetime_marketer':
                return 69;
            case 'lifetime_agency':
                return 199;
            case 'lifetime_consultant':
                 return 39;
            default:
                return 0.0;
        }
    }

    public function getPeriod()
    {
        if (strpos($this->name, 'yearly')) {
            return self::PERIOD_YEAR;
        }

        return self::PERIOD_MONTH;
    }

    /**
     * 😓😓😓😓😓
     * @return int
     */
    public function getFrequency()
    {
        return 1;
    }
}
