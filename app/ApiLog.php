<?php

namespace App;

use Carbon\Carbon;

/**
 * @property string $url
 * @property integer $user_id
 * @property string $request_body
 * @property int $response_http_code
 * @property string $response_body
 * @property string $system_error_code
 * @property Carbon|string $created_at
 */
class ApiLog extends BaseModel
{
    const API_INTERNAL_ERROR = 'API_INTERNAL_ERROR';
    const API_DOES_NOT_RESPONDS = 'API_DOES_NOT_RESPONDS';
    const API_VALIDATION_ERROR = 'API_VALIDATION_ERROR';

    public $timestamps = false;
    protected $fillable = [
        'url', 'user_id', 'request_body',
        'response_http_code', 'response_body', 'system_error_code',
        'api_type'
    ];

    public static function boot()
    {
        parent::boot();

        static::creating(function (ApiLog $model) {
            $model->created_at = $model->freshTimestamp();
        });
    }
}
