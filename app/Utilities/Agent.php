<?php
namespace App\Utilities;

class Agent
{

    public static function build()
    {
        $agents = array();
        $netclr = array();
        $sysntv = array();

        $ras1 = rand(0,9);
        $ras2 = rand(0,255);

        $date = date("YmdHis").$ras2;

        $netclr[0] = ".NET CLR 2.0.50727";
        $netclr[1] = ".NET CLR 1.1.4322";
        $netclr[2] = ".NET CLR 4.0.30319";
        $netclr[3] = ".NET CLR 3.5.2644";
        $netclr[4] = ".NET CLR 1.0.10322";
        $netclr[5] = ".NET CLR 3.5.11952";
        $netclr[6] = ".NET CLR 4.0.30319";
        $netclr[7] = ".NET CLR 2.0.65263";
        $netclr[8] = ".NET CLR 1.1.4322; .NET CLR 4.0.30319";
        $netclr[9] = ".NET CLR 4.0.30319; .NET CLR 2.0.50727";

        $sysntv[0] = "Windows NT 6.1; WOW64";
        $sysntv[1] = "Windows NT 5.1; rv:10.1";
        $sysntv[2] = "Windows NT 5.1; U; en";
        $sysntv[3] = "compatible; MSIE 10.0; Windows NT 6.2";
        $sysntv[4] = "Windows NT 6.1; U; en; OneNote.2; ";
        $sysntv[5] = "compatible; Windows NT 6.2; WOW64; en-US";
        $sysntv[6] = "compatible; MSIE 10.0; Windows NT 6.2; Trident/5.0; WOW64";
        $sysntv[7] = "Windows NT 5.1; en; FDM";
        $sysntv[8] = "Windows NT 6.2; WOW64; MediaBox 1.1";
        $sysntv[9] = "compatible; MSIE 11.0; Windows NT 6.2; WOW64";

        $agents[0] = "Opera/9.80 (".$sysntv[$ras1].";".$netclr[$ras1].") Presto/2.10.".rand(0,999)." Version/11.62";
        $agents[1] = "Mozilla/5.0 (".$sysntv[$ras1].";".$netclr[$ras1].") Gecko/".$date." Firefox/23.0.".$ras1;
        $agents[2] = "Mozilla/5.0 (".$sysntv[$ras1].";".$netclr[$ras1].") AppleWebKit/535.2 (KHTML, like Gecko) Chrome/20.0.".rand(0,9999).".".rand(0,99)." Safari/535.2";
        $agents[3] = "Mozilla/5.0 (".$sysntv[$ras1].";".$netclr[$ras1].")";
        $agents[4] = "Mozilla/5.0 (".$sysntv[$ras1].") AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.".rand(0,9999).".".rand(0,99)." Safari/537.36)";
        $agents[5] = "Mozilla/5.0 (".$sysntv[$ras1].";".$netclr[$ras1].")";
        $agents[6] = "Opera/9.80 (".$sysntv[$ras1].") Presto/2.9.".$ras2." Version/12.50";
        $agents[7] = "Mozilla/5.0 (".$sysntv[$ras1].";".$netclr[$ras1].")";
        $agents[8] = "Mozilla/5.0 (".$sysntv[$ras1].") Gecko/".$date." Firefox/17.0";
        $agents[9] = "Mozilla/5.0 (".$sysntv[$ras1].";".$netclr[$ras1].")";

        return $agents[$ras1];
    }

}