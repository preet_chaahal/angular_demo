<?php
namespace App\Utilities;

class Research
{
    var $curlOptions;

    public function __construct()
    {
        $this->chUrl = curl_init();
        $proxy = $this->selectProxy();

        // setup CURL options
        if(! empty($proxy))
        {
            if(strtolower($proxy->type) == 'http')
            {
                $proxy_address = $proxy->ip;
                if(count(trim($proxy->port)) > 0)
                {
                    $proxy_address .= ':'. $proxy->port;
                }

                $this->curlOptions[CURLOPT_PROXY] = $proxy_address;
            }
            elseif(strtolower($proxy->type) == 'iface')
            {
                $this->curlOptions[CURLOPT_INTERFACE] = $proxy->ip;
            }
        }
    }

    public function curlUrl($url, $options, $userAgent)
    {
        $content = array(
            'valid' => false
        );

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        // referrer
        curl_setopt($curl, CURLOPT_USERAGENT, $userAgent);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        // extra options
        curl_setopt_array($curl, $options);

        // execute the curl and return the data (if there is some)
        $html        = curl_exec($curl);
        $errorNumber = curl_errno($curl);
        $error       = curl_error($curl);
        curl_close($curl);

        if(! $errorNumber)
        {
            $content['valid']    = true;
            $content['response'] = $html;
            $content['error']    = null;
        }
        else
        {
            // could log the error number and error itself
            $content['error'] = $error;
            $content['options']    = $this->curlOptions;
        }

        return $content;
    }

    public function selectProxy()
    {
        $proxy = \App\SProxy::orderByRaw('RAND()')->first();

        return $proxy;
    }

    public function buildAgentString()
    {
        $agents = array();
        $netclr = array();
        $sysntv = array();

        $ras1 = rand(0,9);
        $ras2 = rand(0,255);

        $date = date("YmdHis").$ras2;

        $netclr[0] = ".NET CLR 2.0.50727";
        $netclr[1] = ".NET CLR 1.1.4322";
        $netclr[2] = ".NET CLR 4.0.30319";
        $netclr[3] = ".NET CLR 3.5.2644";
        $netclr[4] = ".NET CLR 1.0.10322";
        $netclr[5] = ".NET CLR 3.5.11952";
        $netclr[6] = ".NET CLR 4.0.30319";
        $netclr[7] = ".NET CLR 2.0.65263";
        $netclr[8] = ".NET CLR 1.1.4322; .NET CLR 4.0.30319";
        $netclr[9] = ".NET CLR 4.0.30319; .NET CLR 2.0.50727";

        $sysntv[0] = "Windows NT 6.1; WOW64";
        $sysntv[1] = "Windows NT 5.1; rv:10.1";
        $sysntv[2] = "Windows NT 5.1; U; en";
        $sysntv[3] = "compatible; MSIE 10.0; Windows NT 6.2";
        $sysntv[4] = "Windows NT 6.1; U; en; OneNote.2; ";
        $sysntv[5] = "compatible; Windows NT 6.2; WOW64; en-US";
        $sysntv[6] = "compatible; MSIE 10.0; Windows NT 6.2; Trident/5.0; WOW64";
        $sysntv[7] = "Windows NT 5.1; en; FDM";
        $sysntv[8] = "Windows NT 6.2; WOW64; MediaBox 1.1";
        $sysntv[9] = "compatible; MSIE 11.0; Windows NT 6.2; WOW64";

        $agents[0] = "Opera/9.80 (".$sysntv[$ras1].";".$netclr[$ras1].") Presto/2.10.".rand(0,999)." Version/11.62";
        $agents[1] = "Mozilla/5.0 (".$sysntv[$ras1].";".$netclr[$ras1].") Gecko/".$date." Firefox/23.0.".$ras1;
        $agents[2] = "Mozilla/5.0 (".$sysntv[$ras1].";".$netclr[$ras1].") AppleWebKit/535.2 (KHTML, like Gecko) Chrome/20.0.".rand(0,9999).".".rand(0,99)." Safari/535.2";
        $agents[3] = "Mozilla/5.0 (".$sysntv[$ras1].";".$netclr[$ras1].")";
        $agents[4] = "Mozilla/5.0 (".$sysntv[$ras1].") AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.".rand(0,9999).".".rand(0,99)." Safari/537.36)";
        $agents[5] = "Mozilla/5.0 (".$sysntv[$ras1].";".$netclr[$ras1].")";
        $agents[6] = "Opera/9.80 (".$sysntv[$ras1].") Presto/2.9.".$ras2." Version/12.50";
        $agents[7] = "Mozilla/5.0 (".$sysntv[$ras1].";".$netclr[$ras1].")";
        $agents[8] = "Mozilla/5.0 (".$sysntv[$ras1].") Gecko/".$date." Firefox/17.0";
        $agents[9] = "Mozilla/5.0 (".$sysntv[$ras1].";".$netclr[$ras1].")";

        return $agents[$ras1];
    }

    public function researchKeyword($searchString, $engine, $country)
    {
        $hits_per_page = 10;

        // make the $searchString safe to pass to google
        $searchString = trim($searchString);
        $searchString = str_replace('+', ' ', $searchString);
        $searchString = str_replace('[', '', $searchString);
        $searchString = str_replace(']', '', $searchString);
        $searchString = str_replace('"', '', $searchString);

        if(strlen($searchString) <= 0)
        {
            // cannot search nothing
            return [];
        }

        $searchString = str_replace(" ", "+", $searchString);
        $searchString = str_replace("%26", "&", $searchString);
        $count = 0;
        $urllist = array();

        $searchQuery = "https://www.$engine/search?pws=0&gl=$country&q=$searchString&num=20";

        $http_agent = $this->buildAgentString();

        $results = $this->curlUrl($searchQuery, $this->curlOptions, $http_agent);

        if($results['valid'] == true)
        {
            if (strpos($results['response'], '302 Moved')
                || preg_match('/sorry.google.com/', $results['response'])
                || strstr($results['response'], 'computer virus or spyware application')
                || strstr($results['response'], 'entire network is affected')
                || strstr($results['response'], 'http://www.download.com/Antivirus'))
            {
                // we change the results to false
                $results['valid'] = false;
                $results['response'] = 'Server Busy';
            }

            // check again to see if everything is valid
            if($results['valid'] == true)
            {
                // return all H3 elements
                preg_match_all('@<h3\s*class="r">\s*<a[^<>]*href="([^<>]*)"[^<>]*>(.*)</a>\s*</h3>@siU', $results['response'], $matches);

                // loop through all matches
                for ($i = 0; $i < count($matches[1]); $i++)
                {
                    $start = '/url?q=';
                    $end = '&amp;';
                    $matches[1][$i] = $this->get_string($matches[1][$i], $start, $end);
                    $matches[1][$i] = urldecode($matches[1][$i]);
                }

                $results['response'] = $matches[1];
            }
        }

        dd($results);
    }

    // find out what this is doing
    public function get_string($string, $start, $end){
 $string = " ".$string;
 $pos = strpos($string,$start);
 if ($pos == 0) return "";
 $pos += strlen($start);
 $len = strpos($string,$end,$pos) - $pos;
 return substr($string,$pos,$len);
}
}