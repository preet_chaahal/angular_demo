<?php
namespace App\Utilities;
// will this work better if we didn't use rolling curl?
// yes

// also max tries could be moved to the .env

use App\AccessProxy;
use App\AccessProxyLog;
use App\Exceptions\ApiOperationFailedException;
use Illuminate\Support\Facades\DB;
use App\SProxy;
use Carbon\Carbon;
use Html;
use Log;

class Google
{
    var $curlOptions;
    var $resultCount;

    public function __construct($resultCount = 10)
    {
    }

    public function curlUrl($url,$module)
    {
        $maxTries = 3;

        $content = array(
            'valid' => false
        );

        $urlList = [];
        while($maxTries > 0)
        {
          
            Log::info("TRY TO GET PAGE CONTENT :" .$maxTries);
            Log::info($url);

            /** @var SProxy $proxy */
            $proxy = Proxy::select($module);
            $this->curlOptions = [];
            // setup CURL options
            if(! empty($proxy))
            {
                if(strtolower($proxy->type) == 'http')
                {
                    $proxy_address = $proxy->ip;
                    if(count(trim($proxy->port)) > 0)
                    {
                        $this->curlOptions[CURLOPT_PROXYPORT] = $proxy->port;
                        // $proxy_address .= ':'. $proxy->port;
                    }

                    if(!empty($proxy->user) && !empty($proxy->password)){
                        $this->curlOptions[CURLOPT_PROXYUSERPWD] = $proxy->user . ':' . $proxy->password;
                    }

                    $this->curlOptions[CURLOPT_PROXY]= $proxy_address;
                } elseif (strtolower($proxy->type) == 'iface') {
                    $this->curlOptions [CURLOPT_INTERFACE] = $proxy->ip;
                }
            } else {
                throw new ApiOperationFailedException('No any active proxy available');
            }
            // proxy


            Log::info('Trying to fetch results from google');
//            $userAgent = \App\Utilities\Agent::build()
            $userAgent= "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0";
            $results = $this->execute($url, $this->curlOptions, $userAgent);
            Log::info("Ip :------------------->".$proxy->ip);
            Log::info($results);
            $results['error'] = $userAgent . PHP_EOL . PHP_EOL . $results['error'];
            if($results['valid'] == true)
            {
                Log::info('Google Content Found');

                $content['valid']    = true;
                $content['response'] = $results['response']; // we only expect one

                // maybe in here we need to look for 302 moved?
                if(strpos($content['response'], '302 Moved') === false and strpos($content['response'], '503 Service Unavailable') === false)
                {
                    

                    Log::info('Good Google Content');


                    preg_match_all('@<h3\s*class="r">\s*<a[^<>]*href="([^<>]*)"[^<>]*>(.*)</a>\s*</h3>@siU', $content['response'], $matches);

                    $urlList = [];
                    for ($i = 0; $i < count($matches[1]); $i++) {
                        //$matches[2][$i] = strip_tags($matches[2][$i]);
                        //$start = '/url?q=';
                        //$end = '&amp;';
                        //$matches[1][$i] = $this->get_string($matches[1][$i], $start, $end);
                        $matches[1][$i] =$url= urldecode($matches[1][$i]);

                        if (!empty($url) and filter_var($url, FILTER_VALIDATE_URL)) {
                            $urlList[] = preg_replace('/^https(?=:\/\/)/i', 'http', $url);
                        }
                    }

                    Log::info("--------URL LIST-----------");
                    Log::info($urlList);

                    if(empty($urlList))
                    {
                        $log = ['ip' => $proxy->ip, 'id'=>$proxy->id,'error_code' => $results['error_code'], 'error' => HTML::entities($content['response'])];
                        AccessProxyLog::create($log);
                        $proxy->status = 0;
                        $proxy->save();
                        DB::commit();
                        usleep(rand(50000, 100000));
                    }

                    $maxTries--;

                    // we got a good response, lets ensure we do not keep looping
                    if (!empty($urlList)) {
                        $maxTries = -99;
                    }
                }
                else
                {
                    Log::info('Bad Google Content (302 moved), trying again');

                    $log = ['ip' => $proxy->ip,'id'=>$proxy->id, 'error_code' => $results['error_code'], 'error' => $results['error']];
                    AccessProxyLog::create($log);
                    $proxy->status = 0;
                    $proxy->save();
                    DB::commit();
                    usleep(rand(50000, 100000));
                    $maxTries--;
                }
            }
            else
            {
                // store proxy log here
                $log = ['ip' => $proxy->ip, 'error_code' => $results['error_code'], 'error' => $results['error']];
                AccessProxyLog::create($log);
                /** @var AccessProxy $proxy */
                $proxy->status = 0;
                $proxy->save();
                DB::commit();
                Log::info('No Google Content');
                Log::info(print_r($results['error'], true));
                //Log::info($this->curlOptions);//[CURLOPT_PROXY]);

                // we didn't get a good response, so try again
                // let's not get banned so much!
                usleep(rand(50000, 100000));
                $maxTries--;
            }
        }

        if($maxTries != -99)
        {
            Log::info('Hit maximum google tries');
        }

        return $urlList;
    }

    public function getScrapeUrl($engine, $country, $value)
    {
        $searchquery = $value;
        $searchquery = str_replace('+', ' ', $searchquery);
        $searchquery = str_replace('[', '', $searchquery);
        $searchquery = str_replace(']', '', $searchquery);
        $searchquery = str_replace('"', '', $searchquery);

        $getit = str_replace(" ", "+", $searchquery);
        $getit = str_replace("%26", "&", $getit);

        return "https://www.{$engine}/search?pws=0&gl={$country}&q={$getit}&num=20";
    }

    public function scrape($engine, $country, $value, $module)
    {
        Log::info(__FUNCTION__ .'  START '. $value);
        Log::info(Carbon::now()->toDateTimeString());

        $content = $this->curlUrl(
            $this->getScrapeUrl($engine, $country, $value),
            $module
        );

        Log::info(__FUNCTION__ .'  END '. $value);
        Log::info(Carbon::now()->toDateTimeString());

        return $content;
    }

    // find out what this is doing
    public function get_string($string, $start, $end)
    {
        $string = " " . $string;
        $pos = strpos($string, $start);
        if ($pos == 0) return "";
        $pos += strlen($start);
        $len = strpos($string, $end, $pos) - $pos;
        return substr($string, $pos, $len);

    }


    public function execute($url, $options, $userAgent)
    {
        $content = array(
            'valid' => false
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, true); // no headers in the output
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // output to variable
        curl_setopt($ch, CURLOPT_USERAGENT, $userAgent);        // Spoof the User-Agent header
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_FAILONERROR, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        if (isset($options[CURLOPT_PROXY])) {
            if (isset($options[CURLOPT_PROXYPORT])) {
                curl_setopt($ch, CURLOPT_PROXYPORT, $options[CURLOPT_PROXYPORT]);
            }
            curl_setopt($ch, CURLOPT_PROXYTYPE, 'HTTP');
            curl_setopt($ch, CURLOPT_PROXY, $options[CURLOPT_PROXY]);
            if (isset($options[CURLOPT_PROXYUSERPWD])) {
                curl_setopt($ch, CURLOPT_PROXYUSERPWD, $options[CURLOPT_PROXYUSERPWD]);
                curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            }
        } else if (isset($options[CURLOPT_INTERFACE])) {
            curl_setopt($ch, CURLOPT_INTERFACE, $options[CURLOPT_INTERFACE]);
        }


        curl_setopt($ch, CURLOPT_REFERER, $url);
        $html        = curl_exec($ch);
        $errorNumber = curl_errno($ch);
        $error       = curl_error($ch);
        curl_close($ch);

        if (!$errorNumber) {
            $content['valid'] = true;
            $content['response'] = $html;
            $content['error'] = $error;
            $content['error_code'] = $errorNumber;
        } else {
            // could log the error number and error itself

            Log::info($error);

            $content['response'] = $html;
            $content['error']   = $error;
            $content['error_code']   = $errorNumber;
            $content['options'] = $this->curlOptions;
        }

        return $content;
    }

}