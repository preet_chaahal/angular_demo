<?php
/**
 * Created by PhpStorm.
 * User: abc
 * Date: 2/6/2016
 * Time: 4:50 PM
 */

namespace App\Utilities;


class StringValidator
{
    public static function validate($keyword){
        $remove = array(
            "`",
            "~",
            "!",
            "@",
            "%",
            "^",
            "*",
            "(",
            ")",
            "=",
            "|",
            "{",
            "}",
            "?",
            "\?",
            "\\",
            "/",
            "<",
            ">",
            ";",
            ","
        );
        $replace = array('');
        $keyword = str_replace($remove, $replace, $keyword);

        //$remove = array(" ");
        //$replace = array(' ');
        $remove = array("�");
        $replace = array(' ');
        $keyword = str_replace($remove, $replace , $keyword);

        return $keyword;
    }
}