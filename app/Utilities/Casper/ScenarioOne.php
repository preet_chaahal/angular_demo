<?php
namespace App\Utilities\Casper;

use Config;

class ScenarioOne
{
    // our writable paths
    var $paths;
    // this variable is only set with the setDebugFile (won't delete the file every time)
    var $debugDelete;
    // the keywords we want to research
    var $keywords;
    // our user id
    var $userId;
    // the unique id we are going to assign the file
    var $fileId;

    // this is information about the file we are requesting
    var $file;

    public function __construct($userId, $keywords)
    {
        $this->userId   = $userId;
        $this->keywords = $keywords;
        $this->fileId   = uniqid();

        // our configurable paths
        $this->paths['download'] = Config::get('casper.download');
    }

    public function sendAPIRequest()
    {
        error_log('Sending CasperJS API Request');
        $this->file = 'user_'. $this->userId .'_'. $this->fileId .'.csv';

        // put together the URL for the API
        $url = \Config::get('casper.scenario_one_url');
        $url .= implode(',', str_replace(' ', '%20', $this->keywords));
        $url .= '|'. $this->file;

        error_log('URL: '. $url);

        $content = file_get_contents($url);

        error_log('Content Received: '. $content);
    }

    public function processFile($timeOut = 120)
    {
        error_log('Processing file, trying for '. $timeOut .' seconds');
        $casperData     = array();
        $fileToLookFile = $this->paths['download'] . $this->file;
        $timeOutLeft    = $timeOut;

        // Loop until we time out
        while ($timeOutLeft > 1)
        {
            // look to see if the file exists
            if(file_exists($fileToLookFile))
            {
                // if the file exists, then process it
                error_log('Found CasperJS File');

                // start processing the file
                $foundHeaders = false;
                $handle = fopen($fileToLookFile, 'r');
                while(($row = fgetcsv($handle, 10000, "\r")) !== false)
                {
                    if($foundHeaders)
                    {
                        // we found the headers continue processing
                        $data = explode("\t", $row[0]);

                        $casperData[] = $data;
                    }
                    else
                    {
                        $foundHeaders = true;
                        // just skip them
                    }
                }
                fclose($handle);

                // remove the file(s)
                if(! $this->debugDelete)
                {
                    @unlink($this->paths['download'] . $this->file);
                }

                // we need to skip out of here
                $timeOutLeft = 0;
                break;
            }

            // sleep for one second
            sleep(1);

            // take away one second
            $timeOutLeft -= 1;
        }

        // finally return the data
        return $casperData;
    }

    public function setDebugFile($filename)
    {
        error_log('Setting Debug File');
        $this->file = $filename;
        $this->debugDelete = true;
    }
}
