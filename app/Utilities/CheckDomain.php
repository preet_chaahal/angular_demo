<?php

use App\Utilities\Domain;
use App\Utilities\Utility;

class CheckDomain
{
    function getDns()
    {
        return [
            '209.244.0.4',
            '8.8.4.4',
            '84.200.70.40',
            '8.20.247.20',
            '208.67.220.220',
            '156.154.71.1',
            '199.85.127.10',
            '209.88.198.133',
            '195.46.39.40',
            '50.116.23.211',
            '208.76.51.51',
            '216.146.36.36',
            '37.235.1.177',
            '23.253.163.53',
            '91.239.100.100',
            '209.244.0.3',
            '8.8.8.8',
            '84.200.69.80',
            '8.26.56.26',
            '208.67.222.222',
            '156.154.70.1',
            '199.85.126.10',
            '81.218.119.11',
            '195.46.39.39',
            '50.116.40.226',
            '208.76.50.50',
            '216.146.35.35',
            '37.235.1.174',
            '198.101.242.72',
            '89.233.43.71',
            '74.82.42.42',
            '109.69.8.51'
        ];
    }

    function doCheck($checkKeywords)
    {
        if (!is_array($checkKeywords)) {
            $checkKeywords = [strtolower($checkKeywords)];
        }

        Log::info("=================== TO CHECK RESULT =====================");
        Log::info($checkKeywords);

        $rdns = new RDNS;

        $dns = $this->getDns();

        $index = 0;
        $timeout = 3;
//        foreach ($dns as $key) {
//            $rdns->addServer($key, 53, $index);
//            $index++;
//        }
        $dnsIp = $dns[array_rand($dns)];
        $rdns->addServer($dnsIp);

        $keywords_json_cleared = array();
        $keywords_domains = array();
        $domainKeywordAssocArr = [];
        foreach ($checkKeywords as $key) {
            try {
                $keyWord = Utility::filterKeyword($key);
                $keywords_json_cleared[$key] = $keyWord;

                $keywords_domains[] = $checkDomain = $keyWord . '.com';
                $rdns->addRequest($checkDomain, RDNS_NS, $timeout);
                $domainKeywordAssocArr[$checkDomain] = $key;

                $keywords_domains[] = $checkDomain = $keyWord . '.net';
                $rdns->addRequest($checkDomain, RDNS_NS, $timeout);
                $domainKeywordAssocArr[$checkDomain] = $key;

                $keywords_domains[] = $checkDomain = $keyWord . '.org';
                $rdns->addRequest($checkDomain, RDNS_NS, $timeout);
                $domainKeywordAssocArr[$checkDomain] = $key;

            } catch (Exception $e) {
                Log::info('--------------ERROR START---------------');
                Log::info($key);
                Log::info($e->getMessage());
                Log::info('--------------ERROR END---------------');
            }
        }

        // blocking
        $replies = $rdns->getReplies();
        $result_NS = array();
        $result_RDNS = array();
        foreach ($replies as $key) {
            foreach ($key as $value) {
                $result_NS[] = $value['host'];
            }
        }

        Log::info("=================== DOMAIN LIST =====================");
        Log::info($domainKeywordAssocArr);

        foreach ($replies as $key) {
            foreach ($key as $value) {
                $result_RDNS[strtolower($value['host'])] = $value['host'];
            }
        }

        $keywordResult = [];
        foreach ($keywords_json_cleared as $preWord => $postWord) {

            if (isset($result_RDNS[$postWord . '.com'])) {
                if (!isset($keywordResult[$preWord])) {
                    $keywordResult[$preWord] = [];
                }
                $keywordResult[$preWord]['com'] = false;
            }

            if (isset($result_RDNS[$postWord . '.net'])) {
                if (!isset($keywordResult[$preWord])) {
                    $keywordResult[$preWord] = [];
                }
                $keywordResult[$preWord]['net'] = false;
            }

            if (isset($result_RDNS[$postWord . '.org'])) {
                if (!isset($keywordResult[$preWord])) {
                    $keywordResult[$preWord] = [];
                }
                $keywordResult[$preWord]['org'] = false;
            }
        }

        Log::info("============== REGISTERED DOMAINS ==============");
        Log::info($keywordResult);


        Log::info("============== NS RESULT ==============");
        Log::info($result_RDNS);

        $domainObj = new Domain('');

        $needAPICheck = array_diff($keywords_domains, $result_NS);

        Log::info("============== NEED TO CHECK API ==============");
        Log::info($needAPICheck);

        if (!empty($needAPICheck)) {
            foreach ($needAPICheck as $domain) {

                $preWord = $postWord = $domain;
                $checkDomainNameArr = explode('.', $domain);
                if (count($checkDomainNameArr) > 1) {
                    $index = array_search($checkDomainNameArr[0], $keywords_json_cleared);
                    if (isset($keywords_json_cleared[$index])) {
                        $postWord = $keywords_json_cleared[$index];
                        $preWord = $index;
                    }

                    if (isset($keywordResult[$index][$checkDomainNameArr[1]])) {
                        continue;
                    }
                }

                $domainBoxResult = $domainObj->checkit($postWord);
                Log::info("============== DOMAINBOX RESULT ==============");
                Log::info($domainBoxResult);
                Log::info($keywords_json_cleared);

                $keywordResult[$preWord] = $domainBoxResult;
            }
        }

        return $keywordResult;
    }

}
