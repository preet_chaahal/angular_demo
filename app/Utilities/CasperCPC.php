<?php
namespace App\Utilities;

use Config;
use GuzzleHttp\Client;

class CasperCPC
{
    // our writable path
    var $paths;

    // what keywords do we want to send to the API?
    var $keywords;
    // who is the user that is requesting them?
    var $userId;
    // group id
    var $groupId;
    // brainstorm id
    var $brainstormId;
    // our end casper object
    var $casper;

    public function __construct($userId)
    {
        $this->userId = $userId;

        // our configurable paths
        $this->paths['upload'] = Config::get('casper.upload');
        $this->paths['download'] = Config::get('casper.download');
    }

    /**
     * set the keywords we want
     */
    public function setKeywords($keywords = [])
    {
        $this->keywords = $keywords;
    }

    public function setLanguage($language = 'all')
    {
        $this->language = $language;
    }

    public function setLocation($location = 'all')
    {
        $this->location = $location;
    }

    /**
     * used to find the rank tracker item
     */
    public function setRanktrackerId($groupId)
    {
        $this->groupId = $groupId;
        $this->brainstormId = 0;
    }

    /**
     * used to find the brainstorm item
     */
    public function setBrainstormId($brainstormId)
    {
        $this->brainstormId = $brainstormId;
        $this->groupId = 0;
    }

    public function generateKeywords()
    {
        // Upload Path and File
        $fileUploadPath = $this->paths['upload'];
        $fileUploadFilename = 'user_' . $this->userId;

        // Download Path and File
        $fileDownloadPath = $this->paths['download'];
        $fileDownloadFilename = 'user_' . $this->userId;


        // append either group id, or brainstorm
        if ($this->groupId != 0) {
            $fileUploadFilename .= '_group_' . $this->groupId;
            $fileDownloadFilename .= '_group_' . $this->groupId;
        } else {
            $fileUploadFilename .= '_brainstorm_' . $this->brainstormId;
            $fileDownloadFilename .= '_brainstorm_' . $this->brainstormId;
        }

        // add final extension
        $fileUploadFilename .= '_upload.csv';
        $fileDownloadFilename .= '_download.csv';

        // save the job
        $casper = \App\Casper::create([
            'user_id' => $this->userId,
            'group_id' => $this->groupId,
            'brainstorm_id' => $this->brainstormId,
            'upload_file' => $fileUploadFilename,
            'download_file' => $fileDownloadFilename,
            'is_uploaded' => true,
            'is_downloaded' => false,
            'is_processing' => false
        ]);

        // resave the files, now that we have an id
        $casper->upload_file = $casper->id . '_' . $casper->upload_file;
        $casper->download_file = $casper->id . '_' . $casper->download_file;
        $casper->save();

        // write the file
//        $fileHandle = fopen($fileUploadPath . $casper->upload_file, 'w+');
        // write the keywords
        $keywordString = '';
        foreach ($this->keywords as $keyword) {
            $keyword .= "\n";
//            fwrite($fileHandle, $keyword);
            if (!empty($keywordString)) {
                $keywordString .= ',';
            }
            $keywordString .= $keyword;
        }
        // close the file
//        fclose($fileHandle);
        $casper->upload_file = $keywordString;
        $casper->save();
        // save the capser object so that we can send it through the API
        $this->casper = $casper;

        return $casper->id;
    }

    /**
     * this function will generate the Casper File but not write the information into the database
     */
    public function generateKeywordsManual()
    {
        // Upload Path and File
        $fileUploadPath = $this->paths['upload'];
        $fileUploadFilename = 'user_' . $this->userId;

        // Download Path and File
        $fileDownloadPath = $this->paths['download'];
        $fileDownloadFilename = 'user_' . $this->userId;


        // append either group id, or brainstorm
        if ($this->groupId != 0) {
            $fileUploadFilename .= '_group_' . $this->groupId;
            $fileDownloadFilename .= '_group_' . $this->groupId;
        } else {
            $fileUploadFilename .= '_brainstorm_' . $this->brainstormId;
            $fileDownloadFilename .= '_brainstorm_' . $this->brainstormId;
        }

        // add final extension
        $fileUploadFilename .= '_upload.csv';
        $fileDownloadFilename .= '_download.csv';

        // save the job
        $casper = new \App\Casper();
        $casper->user_id = $this->userId;
        $casper->group_id = $this->groupId;
        $casper->brainstorm_id = $this->brainstormId;
        $casper->upload_file = $fileUploadFilename;
        $casper->download_file = $fileDownloadFilename;
        $casper->is_uploaded = true;
        $casper->is_downloaded = false;
        $casper->is_processing = false;

        // write the file
        $fileHandle = fopen($fileUploadPath . $casper->upload_file, 'w+');
        // write the keywords
        foreach ($this->keywords as $keyword) {
            $keyword .= "\n";
            fwrite($fileHandle, $keyword);
        }
        // close the file
        fclose($fileHandle);

        // save the capser object so that we can send it through the API
        $this->casper = $casper;
    }

    /**
     * sends the API call to the web
     */
    public function sendAPIRequest()
    {   
        $currentAPI = env('CURRENT_ACTIVE_API');
        $arr = explode(",", str_replace(array("\n"), array(''), $this->casper->upload_file));

        if ($currentAPI == 1) { 
            $params = array(
                'apikey' => env('API_KEY'),
                'keyword' => json_encode($arr),
                'metrics_network' => 'googlesearch',
                'metrics' => 'true',
                'metrics_location' => $this->location,
                'metrics_language' => $this->language,
                'metrics_currency' => 'USD',
                'output' => 'json',
            );

            $url = \Config::get('casper.scenario2_two_url');

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url . http_build_query($params));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

            try {
                $content = curl_exec($ch);
            }
            catch(Exception $e){
                \Session::flash('nosuccess', $e->getMessage());
                return redirect()->back();
            }

            $response = json_decode($content, TRUE);

            if (!empty($response['error'])) {
                return redirect()->back()->withErrors([
                    'message' => 'API error! ' . $response['error']['message'],
                ]);
            }
            // $response = Config::get('mocked_api_data_2');
            $response = $response['results'];

            if (!empty($response)) {
                foreach ($response as $data) {
                    \App\CasperResult::create([
                        'casper_id' => $this->casper->id,
                        'ad_group' => '',
                        'keyword' => $data['string'],
                        'currency' => (is_numeric($data['cpc']) ? $data['cpc'] : 0),
                        'avg_month_searches' => $data['volume'],
                        'competition' => $data['cmp'],
                        'suggested_bid' => '$'.(is_numeric($data['cpc']) ? $data['cpc'] : 0),
                        'impression_share' => '',
                        'organic_impression_share' => '',
                        'organic_avg_position' => '',
                        'in_account' => '',
                        'in_plan' => '',
                        'extracted_from' => '',
                    ]);
                }
            }
        }
        else {
            $params = array(
                'key' => env('API_KEY2'),
            );

            $url = \Config::get('casper.scenario3_two_url');
            $client = new Client();
            
            try {
                $response = $client->post( $url . http_build_query($params), [
                    'json' => [
                        "keywords" => $arr,
                        "language" => $this->language,
                        "country" => $this->location
                    ]
                ]);
            }
            catch(Exception $e){
                \Session::flash('nosuccess', $e->getMessage());
                return redirect()->back();
            }

            $content = $response->getBody()->getContents();

            $response = json_decode($content, TRUE);

            $response = $response['stats'];

            if (!empty($response)) {
                foreach ($response as $data) {
                    $cpc = is_numeric($data['cpc']) ? round($data['cpc'] / 1000000, 2) : 0;
                    \App\CasperResult::create([
                        'casper_id' => $this->casper->id,
                        'ad_group' => '',
                        'keyword' => $data['keyword'],
                        'currency' => $cpc,
                        'avg_month_searches' => $data['volume'],
                        'competition' => $data['ads_competition'],
                        'suggested_bid' => '$'.$cpc,
                        'impression_share' => '',
                        'organic_impression_share' => '',
                        'organic_avg_position' => '',
                        'in_account' => '',
                        'in_plan' => '',
                        'extracted_from' => '',
                    ]);
                }
            }
        }

        $this->casper->is_downloaded = true;
        $this->casper->save();
        return $content;
    }

    /**
     * really only used for debugging
     * it will set the casper object
     * @param integer $id casper.id
     */
    public function setCasper($id)
    {
        $this->casper = \App\Casper::find($id);
    }


    /**
     * * will look for a file to process returning the results
     * @param  integer $timeOut the number in seconds it will wait until it kicks itself out
     * @return array an array of the latest results
     */
    public function processFile($timeOut = 120)
    {


        $casperData = array();
        $fileToLookFile = $this->paths['download'] . $this->casper->download_file;
        $timeOutLeft = $timeOut;

        // Loop until we time out
        while ($timeOutLeft > 1) {
            // look to see if the file exists
            if (file_exists($fileToLookFile)) {
                // if the file exists, then process it
                // grab it as fast as possible so that we know that no other process can grab it
                $this->casper->is_processing = true;
                $this->casper->save();

                // start processing the file
                $foundHeaders = false;
                $handle = fopen($fileToLookFile, 'r');
                while (($row = fgetcsv($handle, 10000, "\r")) !== false) {
                    if ($foundHeaders) {
                        // we found the headers continue processing
                        $data = explode("\t", $row[0]);

                        \App\CasperResult::create([
                            'casper_id' => $this->casper->id,
                            'ad_group' => $data[0],
                            'keyword' => $data[1],
                            'currency' => $data[2],
                            'avg_month_searches' => $data[3],
                            'competition' => $data[4],
                            'suggested_bid' => $data[5],
                            'impression_share' => $data[6],
                            'organic_impression_share' => $data[7],
                            'organic_avg_position' => $data[8],
                            'in_account' => $data[9],
                            'in_plan' => $data[10],
                            'extracted_from' => $data[11],
                        ]);
                    } else {
                        $foundHeaders = true;
                        // just skip them
                    }
                }
                fclose($handle);

                // tell the program we are complete
                $this->casper->is_downloaded = true;
                $this->casper->save();

                // remove the file(s)
                @unlink($this->paths['upload'] . $this->casper->upload_file);
                @unlink($this->paths['download'] . $this->casper->download_file);

                // we need to skip out of here
                $timeOutLeft = 0;
                break;
            }

            // sleep for one second
            sleep(1);

            // take away one second
            $timeOutLeft -= 1;
        }

        // now lets see if we processed the file
        if ($this->casper->is_downloaded) {
            // send data back
            $casperData = $this->casper->results;
        }

        // finally return the data
        return $casperData;
    }

    public function streamFile($timeOut = 120)
    {
        $casperData = array();
        $fileToLookFile = $this->paths['download'] . $this->casper->download_file;
        $timeOutLeft = $timeOut;

        // Loop until we time out
        while ($timeOutLeft > 1) {
            // look to see if the file exists
            if (file_exists($fileToLookFile)) {
                // if the file exists, then process it

                // start processing the file
                $foundHeaders = false;
                $handle = fopen($fileToLookFile, 'r');
                while (($row = fgetcsv($handle, 10000, "\r")) !== false) {
                    if ($foundHeaders) {
                        // we found the headers continue processing
                        $data = explode("\t", $row[0]);

                        $casperData[] = $data;
                    } else {
                        $foundHeaders = true;
                        // just skip them
                    }
                }
                fclose($handle);

                // remove the file(s)
                @unlink($this->paths['upload'] . $this->casper->upload_file);
                @unlink($this->paths['download'] . $this->casper->download_file);

                // we need to skip out of here
                $timeOutLeft = 0;
                break;
            }

            // sleep for one second
            sleep(1);

            // take away one second
            $timeOutLeft -= 1;
        }

        // finally return the data
        return $casperData;
    }

    function setDebugFiles($filename)
    {
        $this->casper->upload_file = $filename . 'upload.csv';
        $this->casper->download_file = $filename . 'download.csv';
    }

}
