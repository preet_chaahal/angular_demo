<?php
namespace App\Utilities;

use Log;
use SoapClient;

include "CheckDomain.php";

class Domain
{
    var $domain;

    public function __construct($domain)
    {
        $this->domain = $domain;
    }

    public function check()
    {
        $checkObj = new \CheckDomain();
        Log::info('Check Domain: ');
        Log::info($this->domain);
        $results = $checkObj->doCheck($this->domain);
        Log::info("--------------- DOMAIN RESULT ------------------");
        Log::info($results);
        Log::info("--------------- DOMAIN RESULT END ------------------");
        return $results;

    }

    function checkit($domain, $tld = '.com,.net,.org')
    {

        $checkDomainNameArr = explode('.', $domain);
        if (count($checkDomainNameArr) < 2) {
            $domain = $domain . '.com';
        }

        Log::info("--------------- CHeck FOr $domain------------------");

        $client = new SoapClient('https://live.domainbox.net/?WSDL', array('trace' => true, 'soap_version' => SOAP_1_2));
        $params = array(
            'AuthenticationParameters' => array(
                'Reseller' => 'wela',
                'Username' => 'hsnamir',
                'Password' => 'P@ssw0rd!@'
            ),
            'CommandParameters' => array(
                'DomainName' => $domain,
                'TLDs' => $tld,
                'Limit' => '10',
                'CheckAtRegistry' => true,
                'DomainCheck' => array(
                    'Include' => true
                ),
                'NameSuggestions' => array(
                    'Include' => false
                ),
                'PremiumDomains' => array(
                    'Include' => false
                ),
            )
        );
        $result = $domainBoxResult = $client->CheckDomainAvailabilityPlus($params);

        /*Log::info("--------------- DOMAINBOX RESULT ------------------");
        Log::info((array)$domainBoxResult);
        Log::info("---------------  DOMAINBOX RESULT END ------------------");*/

        $ar = array('com' => 0, 'net' => 0, 'org' => 0);
        $counter = substr_count($tld, ".");
        for ($x = 0; $x < $counter; $x++) {
            if ($counter == 1) {
                if (isset($result->CheckDomainAvailabilityPlusResult->DomainCheck->Domains->DomainAvailabilityResult->AvailabilityStatus)) {
                    $domainNameArr = explode('.', $result->CheckDomainAvailabilityPlusResult->DomainCheck->Domains->DomainAvailabilityResult->DomainName);
                    if (isset($domainNameArr[1])) {
                        if ($result->CheckDomainAvailabilityPlusResult->DomainCheck->Domains->DomainAvailabilityResult->AvailabilityStatusDescr == 'Available') {
                            $ar[$domainNameArr[1]] = 1;
                        }
                    }
                }
            } else {
                if (isset($result->CheckDomainAvailabilityPlusResult->DomainCheck->Domains->DomainAvailabilityResult[$x]->AvailabilityStatusDescr)) {
                    $domainNameArr = explode('.', $result->CheckDomainAvailabilityPlusResult->DomainCheck->Domains->DomainAvailabilityResult[$x]->DomainName);
                    if (isset($domainNameArr[1])) {
                        if ($result->CheckDomainAvailabilityPlusResult->DomainCheck->Domains->DomainAvailabilityResult[$x]->AvailabilityStatusDescr == 'Available') {
                            $ar[$domainNameArr[1]] = 1;
                        }
                    }
                }
            }
        }

        return $ar;

    }
}