<?php
/**
 * Created by PhpStorm.
 * User: harshadvala
 * Date: 2/6/2016
 * Time: 9:28 AM
 */

namespace App\Utilities;


class Utility
{

    public static function filterKeyword($key)
    {
        $key = preg_replace('/[^\p{L}\p{N}\s]/u', '', $key);
        $key = str_replace(' ', '', $key);
        if (ctype_alnum($key)) {
            return $key;
        } else {
            $key = "";

            return $key;
        }
    }


    public function setKeywordPosition($valueArr, $rank)
    {
        $rank = intval($rank);
        switch ($rank) {
            case ($rank) <= 3:
                $valueArr[3] += 1;
            case $rank <= 10:
                $valueArr[10] += 1;
            case $rank <= 20:
                $valueArr[20] += 1;
            case $rank <= 300:
                $valueArr[30] += 1;
            case $rank <= 100:
                $valueArr[100] += 1;

        }

        return $valueArr;
    }

    public function getKeywordVisibilityPointByPosition($position)
    {
        if ($position < 0 || $position > 30) return 0;

        switch ($position) {
            case 1:
                $points = 30;
                break;
            case 2:
                $points = 29;
                break;
            case 3:
                $points = 28;
                break;
            case 4:
                $points = 27;
                break;
            case 5:
                $points = 26;
                break;
            case 6:
                $points = 25;
                break;
            case 7:
                $points = 24;
                break;
            case 8:
                $points = 23;
                break;
            case 9:
                $points = 22;
                break;
            case 10:
                $points = 21;
                break;
            case 11:
                $points = 20;
                break;
            case 12:
                $points = 19;
                break;
            case 13:
                $points = 18;
                break;
            case 14:
                $points = 17;
                break;
            case 15:
                $points = 16;
                break;
            case 16:
                $points = 15;
                break;
            case 17:
                $points = 14;
                break;
            case 18:
                $points = 13;
                break;
            case 19:
                $points = 12;
                break;
            case 20:
                $points = 11;
                break;
            case 21:
                $points = 10;
                break;
            case 22:
                $points = 9;
                break;
            case 23:
                $points = 8;
                break;
            case 24:
                $points = 7;
                break;
            case 25:
                $points = 6;
                break;
            case 26:
                $points = 5;
                break;
            case 27:
                $points = 4;
                break;
            case 28:
                $points = 3;
                break;
            case 29:
                $points = 2;
                break;
            case 30:
                $points = 1;
                break;
            default:
                $points = 0;
                break;

        }

        return $points;
    }

}