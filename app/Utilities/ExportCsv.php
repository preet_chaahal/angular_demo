<?php
namespace App\Utilities;

class ExportCsv
{
    var $debug;
    var $data;
    var $filename;

    public function __construct($data, $filename, $debug = false)
    {
        $this->data     = $data;
        $this->filename = $filename;
        $this->debug    = $debug;
    }

    public function printHeaders()
    {
        // get the headers from the data

        $headers = array_keys(array_values($this->data)[0]);

        foreach($headers as $header)
        {
            print $header .',';
        }

        print "\n";
    }

    public function send()
    {
        // check to see if we are in debug mode
        if(! $this->debug)
        {
            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private", false);
            header("Content-Type: application/octet-stream");
            header('Content-Disposition: attachment; filename="'. $this->filename .'";' );
            header("Content-Transfer-Encoding: binary");
        }

        if(count($this->data) <= 0)
        {
            die;
        }

        print "\xEF\xBB\xBF";

        // print out headers
        $this->printHeaders();

        // start printing out our data
        foreach($this->data as $record)
        {
            foreach($record as $key => $value)
            {
                // try to escape any double quotes
                $value = str_replace('"', '\"', $value);

                // quote all values (in case of commma's ,)
                $value = '"'. $value .'"';

                print $value .',';
            }
            print "\n";
        }

        // kill the rest because our file is complete
        die;
    }

}