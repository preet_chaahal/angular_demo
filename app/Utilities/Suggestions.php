<?php
namespace App\Utilities;

use App\AccessProxy;
use App\AccessProxyLog;
use Log;
use Session;
use Config;
class Suggestions
{
    var $curlOptions;
    var $proxy;


    public function __construct()
    {
      $module = 2;
        $this->chUrl = curl_init();
        $this->proxy = $proxy = Proxy::select($module);

        // setup CURL options
        if (!empty($proxy)) {
            if (strtolower($proxy->type) == 'http' || strtolower($proxy->type) == 'socks') {
                $proxy_address = $proxy->ip;
                if (count(trim($proxy->port)) > 0) {
                    $proxy_address .= ':' . $proxy->port;
                }

                $this->curlOptions[CURLOPT_PROXY] = $proxy_address;
            } elseif (strtolower($proxy->type) == 'iface') {
                $this->curlOptions[CURLOPT_INTERFACE] = $proxy->ip;
            }

        }
    }

    public function curlUrl($url, $options, $userAgent)
    { $content = array(
        'valid' => false
    );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // output to variable
        curl_setopt($ch, CURLOPT_USERAGENT, $userAgent);        // Spoof the User-Agent header


        $proxy = Proxy::select(2);
            //$this->curlOptions = [];
            // setup CURL options
            if(! empty($proxy))
            {
                if(strtolower($proxy->type) == 'http')
                {
                    $proxy_address = $proxy->ip;
                    if(count(trim($proxy->port)) > 0)
                    {
//                        $this->curlOptions[CURLOPT_PROXYPORT] = $proxy->port;
                        curl_setopt($ch, CURLOPT_PROXYPORT, $proxy->port);
                    }

                    if(!empty($proxy->user) && !empty($proxy->password)){
                        //$this->curlOptions[CURLOPT_PROXYUSERPWD] = $proxy->user . ':' . $proxy->password;
                        curl_setopt($ch, CURLOPT_PROXYUSERPWD, $proxy->user . ':' . $proxy->password);
                    }

                    //$this->curlOptions[CURLOPT_PROXY]= $proxy_address;
                    curl_setopt($ch, CURLOPT_PROXY, $proxy_address);
                } elseif (strtolower($proxy->type) == 'iface') {
                    $this->curlOptions [CURLOPT_INTERFACE] = $proxy->ip;
                }
            } else {
                throw new ApiOperationFailedException('No any active proxy available');
            }
            
        curl_setopt($ch, CURLOPT_REFERER, $url);
        $html        = curl_exec($ch);
        $errorNumber = curl_errno($ch);
        $error       = curl_error($ch);
        curl_close($ch);

        $xmlResponse = simplexml_load_string($html);
        $json        = json_encode($xmlResponse);
        $jsonResponse= json_decode($json, true);
        $newArray    = array();
        $data        = array();

        if(isset($jsonResponse['CompleteSuggestion']) && !empty($jsonResponse['CompleteSuggestion'])) {
            foreach($jsonResponse['CompleteSuggestion'] as $row) {
                if(isset($row['suggestion']['@attributes']['data'])) {
                    $newArray[] = $row['suggestion']['@attributes']['data'];
                }
            }
        }
        //echo "<pre>"; print_r($jsonResponse); exit();
        $data[0] = (isset($newArray[0]) ? $newArray[0] : '');
        $data[1] = $newArray;
        if (!$errorNumber) {
            $content['valid'] = true;
            $content['response'] = json_encode($data);
            $content['error'] = $error;
            $content['error_code'] = $errorNumber;
        } else {
            // could log the error number and error itself

            Log::info($error);

            $content['response'] = $html;
            $content['error']   = $error;
            $content['error_code']   = $errorNumber;
            $content['options'] = $this->curlOptions;
        }

        return $content;
    }

    public function selectProxy()
    {
        $proxy =AccessProxy::orderByRaw('RAND()')->where('module', 2)->get();

        return $proxy;
    }

    public function getLocation($inputName = null){
        $engines = Config::get('searchengines');
        $location = 'all';
        foreach ($engines as $engine){
            if (!is_null($inputName)) {
                if ($engine['name'] == $inputName) {
                    $location = $engine['short_form'];
                    break;
                }
            }
        }
        return $location;
    }

    /**
     * [scrap description]
     * @param  string $keyword keyword to look for
     * @return array          [description]
     */
    public function scrape($keyword, $depth = 1, $firstSleep = true,$lang=false,$location=false)
    {
        if ($firstSleep) {
            usleep(rand(50000, 100000));
        }
        // we are in this function, so let's decrease our depth
        $depth -= 1;
        $keywords 	= array();
        $content 	= array();
		$suggestLang= 'en';
        $geoLocation= '';

		if($lang != false && $lang != 'all') {
			$suggestLang = $lang;
		}
        $lochortCode  = $this->getLocation($location);
        if($lochortCode != false && $lochortCode != 'all') {
            $geoLocation = $lochortCode;
            $geoLocation = "&gl=".urlencode($geoLocation);
        }

        if ($depth >= 0) {
            // their code goes 1st without a space, then adds a space on to "keyword "
            // url to parse
           //$url = "http://suggestqueries.google.com/complete/search?output=firefox&client=firefox&hl=".urlencode($suggestLang)."&q=" . urlencode($keyword);
            $url = "http://suggestqueries.google.com/complete/search?output=toolbar&hl=".urlencode($suggestLang)."&q=" . urlencode($keyword).$geoLocation;

            $userAgent='Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0';
            $content = $this->curlUrl($url,$this->curlOptions,$userAgent);

            if ($content['valid'] == true) {
                // we know that our response is in JSON format, so lets decode it first
                $content['response'] = json_decode($content['response']);
                $keywords[$content['response'][0]] = $content['response'][1];
                if (!isset($content['response'][1])) {
                    return array();
                }
                $keywordskey = (isset($content['response'][0]) ? $content['response'][0] : '');
                // our response is a weird array now, lets fix that

                // going through the group of keywords that we search for we can use the depth to do more searching
                if(isset($keywords[$keyword])) {
                    foreach ($keywords[$keyword] as $nextKeyword) {

                        // ensure this keyword isn't the one that we are currently on (makes a little loop)
                        if (strtolower(trim($nextKeyword)) != strtolower(trim($keyword))) {
                            // just grab the next set of keywords
                            $keywords[trim($nextKeyword)] = $this->scrape($nextKeyword, $depth, false);
                        } else {
                            $keywords[trim($nextKeyword)] = array();
                        }
                    }
                } else if(isset($keywords[strtolower($keyword)])) {
					$keywordlw = strtolower($keyword);
                    foreach ($keywords[$keywordlw] as $nextKeyword) {
                        // ensure this keyword isn't the one that we are currently on (makes a little loop)
                        if (strtolower(trim($nextKeyword)) != strtolower(trim($keywordlw))) {
                            // just grab the next set of keywords
                            $keywords[trim($nextKeyword)] = $this->scrape($nextKeyword, $depth, false);
                        } else {
                            $keywords[trim($nextKeyword)] = array();
                        }
                    }
				} else if(isset($keywords[$keywordskey])) {
                    foreach ($keywords[$keywordskey] as $nextKeyword) {

                        // ensure this keyword isn't the one that we are currently on (makes a little loop)
                        if (strtolower(trim($nextKeyword)) != strtolower(trim($keyword))) {
                            // just grab the next set of keywords
                            $keywords[trim($nextKeyword)] = $this->scrape($nextKeyword, $depth, false);
                        } else {
                            $keywords[trim($nextKeyword)] = array();
                        }
                    }
                } else if(isset($keywords[strtolower($keywordskey)])) {
                    $keywordlw = strtolower($keywordskey);
                    foreach ($keywords[$keywordlw] as $nextKeyword) {
                        // ensure this keyword isn't the one that we are currently on (makes a little loop)
                        if (strtolower(trim($nextKeyword)) != strtolower(trim($keywordlw))) {
                            // just grab the next set of keywords
                            $keywords[trim($nextKeyword)] = $this->scrape($nextKeyword, $depth, false);
                        } else {
                            $keywords[trim($nextKeyword)] = array();
                        }
                    }
                }
            } else {
                $exp = explode(':', $content['error']);
                $log = ['ip' => $this->proxy->ip, 'error' => $exp[0]];
                AccessProxyLog::create($log);
                $accessProxy = $this->proxy->accessProxy;
                $accessProxy->status = 0;
                $accessProxy->save();
                \DB::commit();
                return array();
            }
        }
        return $keywords;
    }

    public function toD3JSON($data)
    {
        $tmp = array();

        if (count($data) <= 0 || !is_array($data)) {
            return $tmp;
        }

        foreach ($data as $key => $values) {
            $tmp[] = [
                'name' => str_replace("'", "", str_replace('"', '', $key)),
//                'name' => stripslashes($key),
                'children' => $this->toD3JSON($values)
            ];
        }

        return $tmp;
    }
}
