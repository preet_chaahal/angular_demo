<?php

namespace App\Utilities;

use App\Account;

use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;

use PayPal\Api\ChargeModel;
use PayPal\Api\Currency;
use PayPal\Api\MerchantPreferences;
use PayPal\Api\PaymentDefinition;
use PayPal\Api\Plan;

use PayPal\Api\Agreement;
use PayPal\Api\CreditCard;
use PayPal\Api\FundingInstrument;
use PayPal\Api\Payer;
use PayPal\Api\PayerInfo;
use PayPal\Api\ShippingAddress;

use PayPal\Api\Patch;
use PayPal\Api\PatchRequest;
use PayPal\Common\PayPalModel;

class Paypal
{

    var $apiContext;
    var $curl;
    var $baseUrl;

    public function __construct()
    {
        if(env('PAYPAL_SANDBOX'))
        {
            $this->baseUrl = 'https://api.sandbox.paypal.com/nvp';
        }
        else
        {
            $this->baseUrl = 'https://api.paypal.com/v1/';
        }
    }

    public function getAccessToken()
    {
        $this->apiContext = new ApiContext(
            new OAuthTokenCredential(
                env('PAYPAL_CLIENT_ID'),
                env('PAYPAL_SECRET')
            )
        );

        $this->apiContext->setConfig(
            array(
                'mode' => env('PAYPAL_SANDBOX') ? 'sandbox' : 'live',
                'log.LogEnabled' => true,
                'log.FileName' => storage_path('logs') .'/paypal.log',
                'log.LogLevel' => 'DEBUG', // PLEASE USE `FINE` LEVEL FOR LOGGING IN LIVE ENVIRONMENTS
                'cache.enabled' => false,
            )
        );
    }

    public function createPlan($planDetails)
    {
        $this->getAccessToken();

        // Create a new instance of Plan object
        $plan = new Plan();
        // # Basic Information
        // Fill up the basic information that is required for the plan
        $plan->setName($planDetails['name'])
            ->setDescription($planDetails['description'])
            ->setType($planDetails['type']);

        // # Payment definitions for this billing plan.
        $paymentDefinition = new PaymentDefinition();

        // The possible values for such setters are mentioned in the setter method documentation.
        // Just open the class file. e.g. lib/PayPal/Api/PaymentDefinition.php and look for setFrequency method.
        // You should be able to see the acceptable values in the comments.
        $paymentDefinition->setName('Payment Schedule')
            ->setType('REGULAR')
            ->setFrequency($planDetails['frequency'])
            ->setFrequencyInterval("1")
            ->setCycles($planDetails['cycles'])
            ->setAmount(new Currency(array('value' => $planDetails['price'], 'currency' => 'USD')));

        // Charge Models
        $chargeModel = new ChargeModel();
        $chargeModel->setType('SHIPPING')
            ->setAmount(new Currency(array('value' => 0, 'currency' => 'USD')));
        $paymentDefinition->setChargeModels(array($chargeModel));

        $merchantPreferences = new MerchantPreferences();
        // ReturnURL and CancelURL are not required and used when creating billing agreement with payment_method as "credit_card".
        // However, it is generally a good idea to set these values, in case you plan to create billing agreements which accepts "paypal" as payment_method.
        // This will keep your plan compatible with both the possible scenarios on how it is being used in agreement.
        $merchantPreferences->setReturnUrl(env('PAYPAL_URL_SUCCESS'))
            ->setCancelUrl(env('PAYPAL_URL_CANCEL'))
            ->setAutoBillAmount("yes")
            ->setInitialFailAmountAction("CONTINUE")
            ->setMaxFailAttempts("0");

        $plan->setPaymentDefinitions(array($paymentDefinition));
        $plan->setMerchantPreferences($merchantPreferences);

        // create the plan
        $output = $plan->create($this->apiContext);

        // we need to store the id into mysql
        dd($output);
    }

    public function getPlan($planToken)
    {
        $this->getAccessToken();

        $plan = Plan::get($planToken, $this->apiContext);

        return $plan;
    }

    public function activatePlan($planToken)
    {
        $this->getAccessToken();

        $plan = $this->getPlan($planToken);

        $patch = new Patch();
        $value = new PayPalModel('{
               "state":"ACTIVE"
             }');
        $patch->setOp('replace')
            ->setPath('/')
            ->setValue($value);
        $patchRequest = new PatchRequest();
        $patchRequest->addPatch($patch);
        $plan->update($patchRequest, $this->apiContext);
    }

    public function createCustomer($customerDetails, $planId)
    {
        $this->getAccessToken();

        // gran plan
        $Account = Account::where('paypal_plan', $planId)->first();


        $agreement = new Agreement();
        $agreement->setName('KWRPlan')
            ->setDescription($Account->description)
            ->setStartDate(date('Y-m-d', strtotime('+1 day')) .'T'. date('H:i:s') .'Z');

        $plan = new Plan();
        $plan->setId($Account->paypal_plan);
        $agreement->setPlan($plan);

        // Add Payer
        $payer = new Payer();
        $payer->setPaymentMethod('credit_card')
            ->setPayerInfo(new PayerInfo(['email' => $customerDetails['email']]));

        // Add Credit Card to Funding Instruments
        $creditCard = new CreditCard();
        $creditCard->setType($customerDetails['card']['type'])
            ->setNumber($customerDetails['card']['number'])
            ->setExpireMonth($customerDetails['card']['month'])
            ->setExpireYear($customerDetails['card']['year'])
            ->setCvv2($customerDetails['card']['cvv']);

        $fundingInstrument = new FundingInstrument();
        $fundingInstrument->setCreditCard($creditCard);
        $payer->setFundingInstruments(array($fundingInstrument));
        //Add Payer to Agreement
        $agreement->setPayer($payer);

        // set it up
        try {
            $agreement = $agreement->create($this->apiContext);
        } catch (Exception $e) {
            return $e->getMessage();
        }

        return $agreement;
    }
}