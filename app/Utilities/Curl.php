<?php
namespace App\Utilities;

class Curl
{
    var $options;
    var $limit = 3;
    var $rollingCurl;
    var $content;

    public function __construct()
    {
        $this->rollingCurl = new \RollingCurl\RollingCurl();
    }

    public function setProxy($proxy)
    {
        if(! empty($proxy))
        {
            if(strtolower($proxy->type) == 'http')
            {
                $proxy_address = $proxy->ip;
                if(count(trim($proxy->port)) > 0)
                {
                    $proxy_address .= ':'. $proxy->port;
                }
                $this->addOption([CURLOPT_PROXY => $proxy_address]);
            }
            elseif(strtolower($proxy->type) == 'iface')
            {
                $this->addOption([CURLOPT_INTERFACE => $proxy->ip]);
            }
        }
    }

    public function addOption($optionsArray)
    {
        $this->rollingCurl->addOptions($optionsArray);
    }

    public function setLimit($limit = 3)
    {
        $this->limit = $limit;
    }

    public function addUrl($url)
    {
        error_log('adding url: '. $url);

        $this->rollingCurl->get($url);
    }

    public function execute()
    {
        // let's not get banned so much!
        usleep(rand(50000,100000));

        $results = array();

        $this->rollingCurl
            ->setCallback(function(\RollingCurl\Request $request, \RollingCurl\RollingCurl $rollingCurl) use(&$results) {
                $results[] = $request->getResponseText();

                // just send the output to the log for now to view
                //error_log($request->getResponseText());
            })
            ->setSimultaneousLimit($this->limit)
            ->execute();

        // append the results
        if(count($results) > 0)
        {
            foreach($results as $result)
            {
                $this->content[] = $result;
            }
        }
    }
}