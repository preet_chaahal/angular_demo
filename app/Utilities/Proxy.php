<?php
namespace App\Utilities;

use App\AccessProxy;
use Illuminate\Database\QueryException;
use Mockery\Exception;

class Proxy
{
    public static function select($module)
    {
        try {
            $accessProxy = AccessProxy::where('module', $module)->where('status', 1)->orderBy('access_time')->first();
            if (!empty($accessProxy)) {
                list($usec, $sec) = explode(" ", microtime());
                $accessProxy->access_time = date('YmdHis', $sec) . substr($usec, 1, 4);
                $accessProxy->save();
                return $accessProxy;
            } else {
                throw new \PhpSpec\Exception\Exception('Server is overloaded right now. Adding projects is temporarily unavailable, try again later.');
            }

        } catch (QueryException $e) {
            throw new \PhpSpec\Exception\Exception('Server is overloaded right now. Adding projects is temporarily unavailable, try again later.');
        }
    }
}