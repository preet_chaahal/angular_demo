<?php
namespace App\Utilities;

use App\AccessProxyLog;
use App\SProxy;
use Log;

class Social
{

    var $keyword;
    var $curl;
    var $facebook = [];
    var $googlePlus = [];
    var $limit = 3;
    var $twitter = [];
    var $pageRank = [];
    var $metaResults = [];
    var $proxy = '';
    var $urlArr = [];

    public function __construct()
    {
        // Defaults

        $this->facebook = [];

        $this->googlePlus = [];
        $this->twitter = [];
    }

    public function getSocialInformation($urls, $module)
    {
        if (empty($urls)) {
            return;
        }
//        $proxy = $this->proxy = \App\Utilities\Proxy::select($module);
        $this->curl = new \RollingCurl\RollingCurl();

        if (!is_array($urls)) {
            $urls = [$urls];
        }

        Log::info('Get Social Informations');
        Log::info($urls);

        // set the proxy
//        $this->setProxy($proxy);

        $this->limit = count($urls) * 5;
        if ($this->limit < 2) {
            $this->limit = 2;
        }

        $this->setLimit($this->limit);

        foreach ($urls as $url) {
            $this->urlArr[$url] = [];
            $fb = 'https://api.sharedcount.com/v1.0/?apikey=e4ed316b422ce6c6175d18e13940143ef6282b7d&url=' . $url;
            $this->addUrl($fb);
            $twitter = 'http://public.newsharecounts.com/count.json?url=' . $url;
            $this->addUrl($twitter);
            //$pinterest = 'http://api.pinterest.com/v1/urls/count.json?callback=receiveCount&url=' . $url;
            //$this->addUrl($pinterest);
            //$googlePlus = 'https://plusone.google.com/_/+1/fastbutton?url=' . $url;
            //$this->addUrl($googlePlus);
            $this->addUrl($url);


            $this->urlArr[$fb] = $url;
            $this->urlArr[$twitter] = $url;
            //$this->urlArr[$pinterest] = $url;
            //$this->urlArr[$googlePlus] = $url;
            $this->urlArr[$url] = $url;

            $this->facebook[$url] = 0;
            $this->googlePlus[$url] = 0;
            $this->pinterest[$url] = 0;
            $this->twitter[$url] = 0;
            $this->metaResults[$url] = array(
                'url' => false,
                'title' => false,
                'desc' => false,
                'h1' => false,
            );

        }

        $this->execute();

        Log::info('---------------Social Results-------------------');
        Log::info($this->facebook);
        //Log::info($this->googlePlus);
        Log::info($this->twitter);
        //Log::info($this->pinterest);
        Log::info($this->metaResults);

    }

    public function setProxy($proxy)
    {
        $options = [];
        if (!empty($proxy)) {
            if (strtolower($proxy->type) == 'http') {
                $proxy_address = $proxy->ip;
                if (count(trim($proxy->port)) > 0) {
                    $proxy_address .= ':' . str_replace('\r', '', $proxy->port);
                }

                $options [CURLOPT_PROXY] = $proxy_address;
                $options [CURLOPT_PROXYTYPE] = 'HTTP';

                if (!empty($proxy->user) && !empty($proxy->password)) {
                    $options[CURLOPT_PROXYUSERPWD] = $proxy->user . ':' . $proxy->password;
                }

            } elseif (strtolower($proxy->type) == 'iface') {
                $options[CURLOPT_INTERFACE] = $proxy->ip;
            }

            $agent = \App\Utilities\Agent::build();

            $options[CURLOPT_USERAGENT] = $agent;        // Spoof the User-Agent header
            $options[CURLOPT_FOLLOWLOCATION] = true;
            $options[CURLOPT_SSL_VERIFYPEER] = 0;

            $this->addOption($options);
        }
    }

    public function addOption($optionsArray)
    {
        $this->curl->addOptions($optionsArray);
    }

    public function setLimit($limit = 3)
    {
        $this->limit = $limit;
    }

    public function addUrl($url)
    {
        $this->curl->get($url);
    }

    public function execute()
    {
        $this->curl
            ->setCallback(function (\RollingCurl\Request $request, \RollingCurl\RollingCurl $rollingCurl) use (&$results) {
                //$results[] = $request->getResponseText();

                $url = $request->getUrl();

                Log::info('-------------------------------------------------------------');
                Log::info($request->getResponseErrno());
                Log::info($error=$request->getResponseError());
                Log::info($this->proxy);
                Log::info('-------------------------------------------------------------');

//                if ($error == $error && !empty($request->getResponseErrno()) && !empty($this->proxy)) {
//                    $log = ['ip' => $this->proxy, 'error_code' => $request->getResponseErrno(), 'error' => $error];
//                    AccessProxyLog::create($log);
//                    /** @var SProxy $this ->proxy */
//                    $accessProxy = $this->proxy->accessProxy;
//                    $accessProxy->status = 0;
//                    $accessProxy->save();
//                }

                $this->url = $this->urlArr[$url];
                //error_log('Running Check for url: '. $url);

                $fb = 'https://api.sharedcount.com/v1.0/?apikey=e4ed316b422ce6c6175d18e13940143ef6282b7d&url=';
                $twitter = 'http://public.newsharecounts.com/count.json?url=';
                //$pinterest = 'http://api.pinterest.com/v1/urls/count.json?callback=receiveCount&url=';
                //$googlePlus = 'https://plusone.google.com/_/+1/fastbutton?url=';

                if (substr($url, 0, strlen($fb)) == $fb)// Facebook
                {
                    $fbResult = $this->parseFacebook($request->getResponseText());
                    if (!empty($fbResult)) {
                        $this->facebook[$this->url] = $fbResult[0][0];
                        $this->googlePlus[$this->url] = $fbResult[0][1];
                        $this->pinterest[$this->url] = $fbResult[0][2];
                    }
                } /*elseif (substr($url, 0, strlen($googlePlus)) == $googlePlus)// Google Plus
                {
                    $googlePlusResult = $this->parseGooglePlus($request->getResponseText());
                    if (!empty($googlePlusResult)) {
                        $this->googlePlus[$this->url] = $googlePlusResult;
                    }
                }*/ elseif (substr($url, 0, strlen($twitter)) == $twitter)// Twitter
                {
                    $twitterResult = $this->parseTwitter($request->getResponseText());
                    if (!empty($twitterResult)) {
                        $this->twitter[$this->url] = $twitterResult;
                    }
                } /*elseif (substr($url, 0, strlen($pinterest)) == $pinterest)// Page Rank
                {
                    $pinterestResult = $this->parsePinterest($request->getResponseText());
                    if (!empty($pinterestResult)) {
                        $this->pinterest[$this->url] = $pinterestResult;
                    }
                } */else {
                    $this->metaResults[$this->url] = $this->parseMeta($request->getResponseText());
                }

            })
            ->setSimultaneousLimit($this->limit)
            ->setOptions(array(
                                CURLOPT_RETURNTRANSFER => 1,
                                CURLOPT_FOLLOWLOCATION => 1,
                                CURLOPT_MAXREDIRS      => 2,
                                CURLOPT_CONNECTTIMEOUT => 3,
                                CURLOPT_TIMEOUT        => 3,
                            ))
            ->execute();

        $resp = 1;

        return $resp;
    }


    // Parse Facebook (content is passed through as XML)
    public function parseFacebook($content)
    {
    	$api_data = array();
        $facebook_shares = 0;
        $facebook_comments = 0;
        $pinterest = 0;

        $fbData = json_decode($content);
        if (!empty($fbData->Facebook->share_count)) {
            $facebook_shares = isset($fbData->Facebook->share_count) ? $fbData->Facebook->share_count : 0;
        }
        if (!empty($fbData->Facebook->comment_count)) {
            $facebook_comments = isset($fbData->Facebook->comment_count) ? $fbData->Facebook->comment_count : 0;
        }
        if (!empty($fbData->Pinterest)) {
            $pinterest = isset($fbData->Pinterest) ? $fbData->Pinterest : 0;
        }

        $api_data[] = array($facebook_shares,$facebook_comments,$pinterest);
        return $api_data;
    }


    // Google Plus
    public function parseGooglePlus($content)
    {
        $final = "";
        $pos = 0;

        if (preg_match('/window.__SSR/', $content)) {
            $pos = strpos($content, "window.__SSR", $pos);
            $subst = substr($content, $pos, 30);

            if (strpos($this->get_string($subst, ':', ','), 'bubble')) {
                $final = "0";
            } else {
                $final = intval($this->get_string($subst, ':', ','));
            }// end else
        }// end if preg_match

        return $final;
    }

    // Twitter
    public function parseTwitter($content)
    {
        $rank = 0;
        $tmp = json_decode($content);

        if (@property_exists($tmp, 'count')) {
            $rank = $tmp->count;
        }

        return $rank;
    }

    // Page Rank
    public function parsePinterest($content)
    {
        $pincount = 0;

        if ( !(strpos($content, "receiveCount(") === false) ) {
            $content = str_replace("receiveCount(", "", $content); //remove the function call the data is wrapped in
            $content = substr($content, 0, -1);
            $pinterest = json_decode($content);
            if (@property_exists($pinterest, 'count')) {
                $pincount = $pinterest->count;
            }
        }

        return $pincount;
    }

    public function isKeywordsInString($keywords, $string)
    {   
        $kwInStr = true;
        for ($i=0; $i < count($keywords); $i++) { 
            if (stripos($string, $keywords[$i]) === false) {
                $kwInStr = false;
            }
        }
        return $kwInStr;
    }


    public function parseMeta($content)
    {
        $metaResults = array(
            'url' => false,
            'title' => false,
            'desc' => false,
            'h1' => false,
        );


        $keywordParts = explode(" ", $this->keyword);

        // let's check the URL with the keyword no matter what
        $metaResults['url'] = $this->isKeywordsInString($keywordParts, $this->url);

        // we have good content, so now we need to parse it
        $dom = new \DOMDocument();
        @$dom->loadHTML($content);

        $title = '';
        $h1 = '';

        $title = $dom->getElementsByTagName('title');
        $h1 = $dom->getElementsByTagName('h1');

        // Title
        if (($title->length) > 0) {
            $metaResults['title'] = $this->isKeywordsInString($keywordParts, $title->item(0)->nodeValue);
        }

        // Header
        if (($h1->length) > 0) {
            $metaResults['h1'] = $this->isKeywordsInString($keywordParts, $h1->item(0)->nodeValue);
        }

        // Description
        $description = "";
        $metas = $dom->getElementsByTagName('meta');
        for ($j = 0; $j < $metas->length; $j++) {
            $meta = $metas->item($j);
            if ($meta->getAttribute('name') == 'description') {
                $metaResults['desc'] = $this->isKeywordsInString($keywordParts, $meta->getAttribute('content'));
            }
        }


        return $metaResults;
    }

    public function seomoz($urls)
    {
        error_log('SEOMOZ start');

        $semos = new \App\Http\Controllers\Backend\API\SeomozController;
        $results = $semos->get($urls);

        return $results;
    }


    // these functions are used for the google page rank only

    function get_string($string, $start, $end)
    {
        $string = " " . $string;
        $pos = strpos($string, $start);
        if ($pos == 0) return "";
        $pos += strlen($start);
        $len = strpos($string, $end, $pos) - $pos;
        return substr($string, $pos, $len);
    }


    function StrToNum($Str, $Check, $Magic)
    {
        $Int32Unit = 4294967296;  // 2^32

        $length = strlen($Str);
        for ($i = 0; $i < $length; $i++) {
            $Check *= $Magic;
//If the float is beyond the boundaries of integer (usually +/- 2.15e+9 = 2^31),
//  the result of converting to integer is undefined
//  refer to http://www.php.net/manual/en/language.types.integer.php
            if ($Check >= $Int32Unit) {
                $Check = ($Check - $Int32Unit * (int)($Check / $Int32Unit));
//if the check less than -2^31
                $Check = ($Check < -2147483648) ? ($Check + $Int32Unit) : $Check;
            }
            $Check += ord($Str{$i});
        }
        return $Check;
    }

//--> for google pagerank
    /*
    * Genearate a hash for a url
    */
    function HashURL($String)
    {
        $Check1 = $this->StrToNum($String, 0x1505, 0x21);
        $Check2 = $this->StrToNum($String, 0, 0x1003F);

        $Check1 >>= 2;
        $Check1 = (($Check1 >> 4) & 0x3FFFFC0) | ($Check1 & 0x3F);
        $Check1 = (($Check1 >> 4) & 0x3FFC00) | ($Check1 & 0x3FF);
        $Check1 = (($Check1 >> 4) & 0x3C000) | ($Check1 & 0x3FFF);

        $T1 = (((($Check1 & 0x3C0) << 4) | ($Check1 & 0x3C)) << 2) | ($Check2 & 0xF0F);
        $T2 = (((($Check1 & 0xFFFFC000) << 4) | ($Check1 & 0x3C00)) << 0xA) | ($Check2 & 0xF0F0000);

        return ($T1 | $T2);
    }
//--> for google pagerank
    /*
    * genearate a checksum for the hash string
    */
    function CheckHash($Hashnum)
    {
        $CheckByte = 0;
        $Flag = 0;

        $HashStr = sprintf('%u', $Hashnum);
        $length = strlen($HashStr);

        for ($i = $length - 1; $i >= 0; $i--) {
            $Re = $HashStr{$i};
            if (1 === ($Flag % 2)) {
                $Re += $Re;
                $Re = (int)($Re / 10) + ($Re % 10);
            }
            $CheckByte += $Re;
            $Flag++;
        }

        $CheckByte %= 10;
        if (0 !== $CheckByte) {
            $CheckByte = 10 - $CheckByte;
            if (1 === ($Flag % 2)) {
                if (1 === ($CheckByte % 2)) {
                    $CheckByte += 9;
                }
                $CheckByte >>= 1;
            }
        }

        return '7' . $CheckByte . $HashStr;
    }
}