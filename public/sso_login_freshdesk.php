<?php

require __DIR__.'/../bootstrap/autoload.php';

$app = require_once __DIR__.'/../bootstrap/app.php';

$kernel = $app->make(Illuminate\Contracts\Http\Kernel::class);
$response = $kernel->handle(
   $request = Illuminate\Http\Request::capture()
);
$kernel->terminate($request, $response);

$user = $request->user();

$email = $user ? $user->email : 'NULL';
$username = $user ? $user->username : 'NULL';

define('FRESHDESK_SHARED_SECRET','637a766073d96a2b0987a0b43d31721b');
define('FRESHDESK_BASE_URL','http://keywordrevealer.freshdesk.com/');   //With Trailing slashes

function getSSOUrl($strName, $strEmail) {
	$timestamp = time();
	$to_be_hashed = $strName . FRESHDESK_SHARED_SECRET . $strEmail . $timestamp;
	$hash = hash_hmac('md5', $to_be_hashed, FRESHDESK_SHARED_SECRET);
	return FRESHDESK_BASE_URL."login/sso/?name=".urlencode($strName)."&email=".urlencode($strEmail)."&timestamp=".$timestamp."&hash=".$hash;
}

if ($user == 'NULL') {
	header("Location: ".FRESHDESK_BASE_URL);
}
else {
	header("Location: ".getSSOUrl($username, $email));
}