$(function () {

	


	$('.slider').on('click', function(e) {
		
		alert($('#switcher').val());

		var id = $(this).attr('id');



		$('#pricing .button-switcher').removeClass('active');

		$(this).addClass('active');



		$('#pricing .price-wrapper .pti-cell-price').removeClass('active');

		$('#pricing .price-wrapper .pti-cell-price.' + id).addClass('active');

	});

});

$(document).ready(function() {
    var $window = $(window);
    function checkWidth() {
      var windowsize = $window.width();
      if ($('.plans_for_month').length > 0) {
        if (windowsize < 768) {
          $("#plans_for_month").css("display","none");
          $(".plans_for_month").css("display","none");
          $(".plans_for_year").css("display","none");
        } else {
          $("#plans_for_month").css("display","block");
          $(".plans_for_month").css("display","block"); 
        }
      }
    }
    checkWidth();
    $(window).resize(checkWidth);
});

$.fn["minitoggle"] = function(options) {
  options = options || {};
  opts = $.extend({
    on: false
  }, options);
  doToggle = function(toggle) {
    toggle = toggle.find(".minitoggle");
    active = toggle.toggleClass("active").hasClass("active");
    handle = toggle.find(".toggle-handle");
    handlePosition = handle.position()
    offset = (active ? toggle.width() - handle.width() - handlePosition.left * 2 : 0);
	if(offset<0){
		offset=37;
		
	}
    handle.css({
      "transform": "translate3d(" + offset + "px,0,0)",
	   '-webkit-transform':"translate3d(" + offset + "px,0,0)",
    });
    return toggle.trigger({
      type: "toggle",
      isActive: active
    });
  };
  this.each(function() {
    var self = $(this);
    self.html("<div class=\"minitoggle\"><div class=\"toggle-handle\"></div></div>");
    self.click(function() {
      doToggle(self);
    });
    if (opts["on"]) {
      doToggle(self);
    }
  });
};

$('.toggle').minitoggle();
$('.toggle').on("toggle", function(e){
  if (e.isActive){
    $('.annually').addClass('active');
    $('.monthly').removeClass('active');
    $('.plans_for_month').css('display','none');
    $('.plans_for_year').css('display','block');
    if($('#tabY').length) {
      var tabY = $("#tabY").val();
      var tabM = $("#tabM").val();
      $("#plan_tab").removeClass("col-md-"+tabM+" col-sm-"+tabM);
      $("#plan_tab").addClass("col-md-"+tabY+" col-sm-"+tabY);
    }
  } else {
    $('.monthly').addClass('active');
    $('.annually').removeClass('active');
    $('.plans_for_year').css('display','none');
    $('.plans_for_month').css('display','block');
    if($('#tabM').length) {
      var tabM = $("#tabM").val();
      var tabY = $("#tabY").val();
      $("#plan_tab").removeClass("col-md-"+tabY+" col-sm-"+tabY);
      $("#plan_tab").addClass("col-md-"+tabM+" col-sm-"+tabM);
    }
  }
});
