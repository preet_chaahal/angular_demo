function selectFeature(element) {
	var id = element.attr('id');

	$('#details .custom-img-button').removeClass('active');
	element.addClass('active');
	$('#details .detail-text').hide();
	$('#details .detail-img').hide();
	$('#' + id + '-text').show();
	$('#' + id + '-img').show();
}

function changeFeature() {
	var featuresIds = ['detail-1', 'detail-2', 'detail-3'];
	var featureButtons = $('#details .detail-links .custom-button.custom-img-button');
	var nextId = 0;
	for (var i = 0; i < featureButtons.length; i++) {
		if ( $(featureButtons[i]).hasClass('active')) {
			nextId = i + 1 === 3 ? 0 : i + 1;
			break;
		}
	}
	selectFeature( $('#detail-' + nextId) );
}

$(function () {
	$('.carousel').carousel();

	$('#details .custom-button').on('click', function(e) {
		var element = $(this);
		selectFeature(element);
		clearInterval(intervalID);
	});

	var intervalID = setInterval(changeFeature, 3000);

	$('.plan-switcher .button-switcher').on('click', function(e) {
		var id = $(this).attr('id');

		$('#pricing .button-switcher').removeClass('active');
		$(this).addClass('active');

		$('#pricing .price-wrapper .pti-cell-price').removeClass('active');
		$('#pricing .price-wrapper .pti-cell-price.' + id).addClass('active');
	});
});

var modal = $('#myModal');
$('.detail-img').on('click', function(e) {
	modal.show();
	$('#modal-img').attr("src", $(this).attr("src"));
	$('#caption').html( $(this).attr("alt") );
});

modal.on('click', function(e) {
	modal.hide();
});