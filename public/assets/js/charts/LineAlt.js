Chart.types.Line.extend({
    name: "LineAlt",
    draw: function () {
        Chart.types.Line.prototype.draw.apply(this, arguments);

        // now draw the segments
        var ctx = this.chart.ctx
        this.datasets.forEach(function (dataset) {
            ctx.strokeStyle = dataset.strokeColor

            var previousPoint = {
                value: null
            };
            dataset.points.forEach(function (point) {
                if (previousPoint.value !== null && point.value !== null) {
                    ctx.beginPath();
                    ctx.moveTo(previousPoint.x, previousPoint.y);
                    ctx.lineTo(point.x, point.y);
                    ctx.stroke();
                }
                previousPoint = point;
            })
        })
    }
});