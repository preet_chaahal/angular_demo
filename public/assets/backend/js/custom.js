function swapElement(elementToHide, elementToShow)
{
    $(elementToHide).hide();
    $(elementToShow).fadeIn();
}