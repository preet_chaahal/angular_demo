/*hide and show function while click on domain availability*/

function openCheckDomainModal() {
	document.getElementById('modal').style.display = 'block';
	document.getElementById('fade').style.display = 'block';
}
function closeCheckDomainModal() {
	document.getElementById('modal').style.display = 'none';
	document.getElementById('fade').style.display = 'none';
}
// var deb=['ngSanitize','ngRoute','ui.bootstrap','ngMaterial','ngAria','ngMessages','ngAnimate','ngFileUpload','highcharts-ng','ui.mention','mentio','nvd3'];
function initiation(res, scope, loading = false){
  if (res.status == 200) {
    angular.forEach(res.data,function(v,k) {
      if (angular.isObject(v)) {
        if (scope.hasOwnProperty(k)){
          angular.forEach(v,function(x,y) {
            scope[k][y]=x;
          });
        }else{
          scope[k]=v;
        }
      }else{
        scope[k]=v;
      }
    });
  }
  scope.loading_icon=loading;
}
function handleErrors(res){
    $("#errMsg").html("");$("#error").show();$("#error ul").html("");
    if (res.status == 500){ $("#error ul").append("<li>please try again</li>")}
    $("#errMsg").hide();showLoader(false);
	if (res.status == 422) {
        $("#error ul").append(function(){
            var txt = '';
            if(typeof res.data=='string'){return res.data;}
            for(x in res.data){
            	txt += '<li><b>'+x+'</b> : ';
            	txt += res.data[x].join(', ');
            	txt += '</li>';
            }
            return txt;
        });
	}
    setTimeout(function () {
        $('#error').fadeOut('fast');
    }, 5000);
}
function showError(txt){
	handleErrors({data:txt, status: 422})
}
var App = angular.module('keywordRevealer',['ui.bootstrap','easypiechart','chart.js','ngAnimate','ngSanitize','angular.morris'],['$interpolateProvider',function($interpolateProvider) {

  $interpolateProvider.startSymbol('[[');
  $interpolateProvider.endSymbol(']]');

}]).controller('keywordResearch', ['$scope','$http','$timeout','$window',function($scope,$http,$timeout,$window){
	$scope.initialValues = function(){
		$http.get('/tools/keywordresearch/index/data').then(function (res) {
			initiation(res, $scope);
			// console.log("Cheheehhee",)
			$scope.search.searchOn = keyword;
		},function(res){

		});
	}

    $scope.downloadInvoice = function(id) {
        $scope.invoice_id = id;
        $http.get('/auth/subscription/downloadinvoice/'+id).then(function (res) {
			if(res.data != "") {
				$window.open(res.data, '_blank');
			}
		},function(res){

		});
    }


    $scope.validatePromoCode = function() {
    	if($("#coupon_code").val() != "" ) {
    		$("#coupon_code_success").text("");
    		$("#coupon_code_error").text("");
    		$('#subscribe-form-button').prop("disabled", true);
    		$('#apply_promo_code').prop("disabled", true);

			var data = {
				promo_code: $scope.promo_code,
				plan: $("#plan").val(),
			}
			$scope.cb_amt = $("#cb_total_amount").val();
			$http({
			     url: '/auth/subscription/validatecoupon/'+$scope.promo_code,
			     method: 'GET',
			     params: data,
			}).then(function (res) {
				$('#subscribe-form-button').prop("disabled", false);
				$scope.cb_result = res.data;
				if($scope.cb_result.status == "success") {
					//alert("coupon code has been applied successfully.");
					$("#coupon_code_success").text("Coupon code has been applied successfully.");
					$("#coupon_response").val("1");
					$("#discount_amount").css("display","block");
					$("#discount_amount").text("Discount: $" + $scope.cb_result.response.discount_amount);
					$("#total_amount").text("$"+$scope.cb_result.response.total_amount);
				} else {
					//alert("Error");
					$("#coupon_code_error").text("Coupon code is not valid.");
					$("#coupon_response").val("0");
					$("#discount_amount").css("display","none");
					$("#discount_amount").val("");
					$("#total_amount").text($scope.cb_amt);
				}
			},function(res) {
				$('#subscribe-form-button').prop("disabled", false);
			 	//alert("error t2");
			 	$("#coupon_code_error").text("Coupon code is not valid.");
			 	$("#coupon_response").val("0");
				$("#discount_amount").css("display","none");
				$("#discount_amount").val("");
				$("#total_amount").text($scope.cb_amt);  	
			});
    	} else {
    		$('#subscribe-form-button').prop("disabled", false);
    		//alert("Empty");
    		$("#coupon_code_error").text("Please enter valid coupon code.");
    		$("#coupon_response").val("0");
			$("#discount_amount").css("display","none");
			$("#discount_amount").val("");
			$("#total_amount").text($scope.cb_amt);  	
    	}
    }

    $scope.updateCoupon = function() {
    	$('#apply_promo_code').prop("disabled", false);
    }

    $scope.validatePromoCodePP = function() {
    	if($("#coupon_code").val() != "" && $('input:radio[name=plan]:checked').val() != "trial") {
    		$("#coupon_code_success").text("");
    		$("#coupon_code_error").text("");
    		$('#submitPP').prop("disabled", true);
    		$('#apply_promo_code').prop("disabled", true);
			var data = {
				promo_code: $("#coupon_code").val(),
				plan: $('input:radio[name=plan]:checked').val(),
				pay_request: "paypal",
			}
			$scope.cb_amt = $("#cb_total_amount").val();
			$scope.cb_cop = $("#coupon_code").val();
			$http({
			     url: '/auth/subscription/validatecoupon/'+$scope.cb_cop,
			     method: 'GET',
			     params: data,
			}).then(function (res) {
				$('#apply_promo_code').prop("disabled", false);
				$('#submitPP').prop("disabled", false);
				$scope.cb_result = res.data;
				if($scope.cb_result.status == "success") {
					//alert("coupon code has been applied successfully.");
					$("#coupon_code_success").text("Coupon code has been applied successfully.");
					//$("#discount_amount").css("display","block");
					//$("#discount_amount").text("Discount: $" + $scope.cb_result.response.discount_amount);
					//$("#total_amount").text("$"+$scope.cb_result.response.total_amount);
				} else {
					//alert("Error");
					$("#coupon_code_error").text("Coupon code is not valid.");
					//$("#discount_amount").css("display","none");
					//$("#discount_amount").val("");
					//$("#total_amount").text($scope.cb_amt);
				}
			},function(res) {
				$('#apply_promo_code').prop("disabled", false);
				$('#submitPP').prop("disabled", false);
			 	//alert("error t2");
			 	$("#coupon_code_error").text("Coupon code is not valid.");
				//$("#discount_amount").css("display","none");
				//$("#discount_amount").val("");
				//$("#total_amount").text($scope.cb_amt);  	
			});
    	} else {
			$('#apply_promo_code').prop("disabled", false);
			$('#submitPP').prop("disabled", false);
    		$("#coupon_code_error").text("Please enter valid coupon code.");
			//$("#discount_amount").css("display","none");
			//$("#discount_amount").val("");
			//$("#total_amount").text($scope.cb_amt);  	
    	}
    }

	$scope.main_keyword="";
    $scope.main_keyword_id="";
    $scope.evaluate_keyword="";
    $scope.projectSaved = false;

	$scope.initialValues();
    $scope.keywordId;
	$scope.openSubTable = function(x){
		if (!x.child && x.research.length > 0){
			$scope.currentOpenSubTable = $scope.currentOpenSubTable==x.index ? null : x.index;
		}
	}

	$scope.search = {searchOnLines: [], searchOn:''};
	$scope.evaluatedKeywords = [];
	$scope.filterSearch = {}
	$scope.filterSearch = {}
	$scope.defaultPagination = function(){
		$scope.pagination = {amount:10, page:1, options: [10,25,50,100]}
	}
	$scope.defaultPagination();

	/*
	* Reset search to default values
	*/
	$scope.resetSearch = function(res){
		$scope.researchData = [];
		$scope.defaultPagination();

		//clear tabs to default
		$scope.defaultTabs();

		//restore default filters for search
		$scope.filterSearch = {};

		$scope.evaluatedKeywords = [];
	  	initiation(res,$scope);
	}

	$scope.newSearch = function(){
		var data = {
			keyword: $scope.search.searchOn,
			tld: Selected.location,
			lang: Selected.language,
			is_ajax_view: 1
		}

		$scope.search.keyword = data.keyword;
		$scope.search.location = data.tld;
		$scope.search.language = data.lang;


		var url    = '/tools/keywordresearch/'+( $scope.importKeyword && $scope.userCanImportKeywords ? 'importkeyword' : 'insert/data');
		var method = $scope.importKeyword && $scope.userCanImportKeywords ? 'POST' : 'GET';

		if ($scope.importKeyword && $scope.search.searchOnLines.length > 700){
			showError('Sorry, you can\'t import more than <b>700</b> word.');
			return;
		}
		$scope.search.searching = true;
		if($scope.importKeyword == true && $scope.userCanImportKeywords == true) {
			$http.post(
			     url,
			     data  
			).then(function (res) {
				$scope.search.searching = false;
				$scope.resetSearch(res);
			},function(res){
				$scope.search.searching = false;
				handleErrors(res);
			});
		} else {
			$http({
			     url: url,
			     method: method,
			     params: data  
			}).then(function (res) {
				$scope.search.searching = false;
				$scope.resetSearch(res);
			},function(res){
				$scope.search.searching = false;
				handleErrors(res);
			});
		}
	}

	// $scope.search.searchOn = 'gifft';
	// $scope.importKeyword = true;
	// $scope.userCanImportKeywords = true;
	// $scope.newSearch();

	$scope.searchOnTodaySearchKeyword = function(x){
	  	x.searching = true;
		var data = {
			keyword: x.keyword,
			lang: x.language,
			tld: x.location,
			is_ajax_view: 1
		}
		$scope.search.keyword = data.keyword;
		$scope.search.location = data.tld;
		$scope.search.language = data.lang;

		$http({
		     url: '/tools/keywordresearch/insert/data',
		     method: 'GET',
		     params: data  
		}).then(function (res) {
			$scope.importKeyword = false;
			$scope.search= {};
			$("#newSearchBtn a").trigger('click');
		  	x.searching = false;
			$scope.resetSearch(res);
			$scope.search.searchOn = data.keyword;
		},function(res){
		  	x.searching = false;
			handleErrors(res);
		});
	}


    /* ------------------------------------ */
    $scope.searchOnSavedKeyword = function(x){
	  	x.getSearched = true;
        $scope.main_keyword_id = x.id;
        $scope.main_keyword = x.keyword;
        $scope.projectSaved = true;
        // $http.get('/tools/keywordresearch/evaluated/'+x.id).then(function (res) {
		$http.get('/tools/keywordresearch/findResearch/'+x.id).then(function (res) {
		  	x.getSearched = false;
			$scope.resetSearch(res);
			$("#newSearchBtn a").trigger('click');
		},function(res){
		  	x.getSearched = false;
			handleErrors(res);
		});
    }
    /* ------------------------------------ */

	/* start of fltration */
	$scope.handleComma = function(x,y=0){
		var parts = x.toString().split(".");
		parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		return parts.join(".");
		// return x.toFixed(y).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
	}
	$scope.handleImportKeyword = function(){
		$scope.importKeyword = !$scope.importKeyword;
	}


	$scope.socialPresence = {
		// colors: ["#3B5998", "#1DA1F2","#EC5F52","#BD081C"],
		colors:[{
			backgroundColor:"#3B5998",
			// hoverBackgroundColor:"#3B5998",
			borderColor:'transparent',
		},{
			backgroundColor:"#1DA1F2",
			borderColor:'transparent',
		},{
			backgroundColor:"#EC5F52",
			borderColor:'transparent',
		},{
			backgroundColor:"#BD081C",
			borderColor:'transparent',
		}],
		series: ['Facebook','Twitter','Google+','Pinterest'],
		lables: [1,2,3,4,5,6,7,8,9,10],
		options :{
			legend: {display: true},
	        scales: {
			    xAxes: [{gridLines: {display:false}}],
			    yAxes: [{gridLines: {display:false}}]
		    }
		},
		responsive: true,
		maintainAspectRatio: false,
		tooltips: { mode : 'label' }
	}

	$scope.domainStrength = {
		colors:[{
			backgroundColor:"#3B5998",
			borderColor:'transparent',
		}],
		lables: [1,2,3,4,5,6,7,8,9,10],
		series: ['Moz Domains'],
		options :{
			legend: {display: true},
	        scales: {
			    xAxes: [{gridLines: {display:false}, stacked: true}],
			    yAxes: [{gridLines: {display:false}}]
		    },
            responsive: true,
            maintainAspectRatio: false,
            tooltips: { mode : 'label' }
		}
	}

    $scope.googleGraphResults = [
	    {name: 'Url', class: 'primary', key: 'meta_url'},
	    {name: 'Title', class: 'pink', key: 'meta_title'},
	    {name: 'Desc', class: 'warning', key: 'meta_desc'},
	    {name: 'H1', class: 'success', key: 'meta_h1'},
    ];
	var options = { Word: 'word_count',Average: 'searches',Estimated: 'profit',Cost: 'cpc'}
	$scope.customFilterForSearch = function(target){
	    return function(item) {
		    var res = parseFloat(item[options[target]]);
		    var min = parseFloat($scope.filterSearch['min'+target]);
		    var max = parseFloat($scope.filterSearch['max'+target]);
		    
		    if (!res && res !=0){ return false; }
		    // if (isNaN(res)){ return false; }
		    if(min && res < min){ return false; }
		    if(max && res > max){ return false; }
		  
		    return true;
	    }
	}
	$scope.customFilterExclude = function(item){
		if (!$scope.filterSearch.exclude){ return true;}
		return item.keyword.search($scope.filterSearch.exclude) !== -1 ? false : true;
	}
	/* end of fltration */

	$scope.importLines = function(e){
		if (typeof $scope.search.searchOn != 'undefined') {
	        $scope.search.searchOnLines = $scope.search.searchOn.split('\n').filter(String);
		}else{
			$scope.search.searchOnLines = []
		}
	}
	$scope.handleSingleKeywordOrImported = function(keyword){
		var keywords = keyword.split('\n');
		return keywords ? keywords[0] : keyword;
	}

	$scope.research = [];
	$scope.currentEvaluateItems = 0;
	$scope.evaluate = function(x, reEvaluating = false,reEvaluatingAgain = false){
		var loader = reEvaluating ? 'reEvaluate' : 'evaluating';

		if ($scope.currentEvaluateItems == 3) {
			showError('Sorry you can\'t evaluate more than 3 keywords at once.');
			return;
		}
		$scope.currentEvaluateItems += 1;
		x[loader] = true;
		x.evaluated_id = reEvaluating ? true :  false;

		x.is_evaluated = reEvaluatingAgain ;
		x.is_unsaved   = $scope.isSaved ? false : true;

		if(!$scope.projectSaved && $scope.main_keyword == ""){
            $scope.main_keyword = $scope.search.keyword;
		}

        $scope.evaluate_keyword = x.keyword;
        //alert($scope.evaluate_keyword);
			var url = "/tools/keywordresearch/evaluate/{keyword}/"+($scope.isSaved?'12':$scope.evaluate_keyword)+"/{searches}/{cpc}/{profit}/{word_count}/{location}/{language}/{keyword}/{evaluated_id}/{is_evaluated}/{is_unsaved}/{keyword}/"+$scope.tmp_id
		for (var i in x) {
			var regex = new RegExp('{'+i+'}','g');
			url = url.search('{'+i+'}') != -1 ? url.replace(regex,x[i]):url;
		}
		$http.get(url).then(function (res) {
			$scope.currentEvaluateItems -=1;
			x[loader] = false;
			res.data.googleTrends = '/tools/keywordresearch/google/trends/'+x.keyword;
    	    angular.forEach($scope.researchData, function(v,k) {
    	    	if (v.keyword == res.data.keyword){
		    	    angular.forEach(res.data, function(vv,kk) {
	    	    		$scope.researchData[k][kk] = vv;
				    });
					$scope.evaluatedKeywords.push($scope.researchData[k]);
			        $scope.addTab($scope.researchData[k]);
    	    	}
		    });


    		// $http.post('/tools/keywordresearch/update/evaluated/'+);
		},function(res){
			$scope.currentEvaluateItems -=1;
			x[loader] = false;
			handleErrors(res);
		});
	}
	$scope.saveProject = function(){
		$scope.search.savingProject = true;
        var data = {
            search_keyword: $scope.search.keyword,
            location: $scope.search.location,
            tmp_id: $scope.tmp_id,
            language: $scope.search.language
        };
		$http.post('/tools/keywordresearch/saveKeyword', data).then(function (res) {
            $scope.main_keyword=res.data.keyword;
            $scope.main_keyword_id=res.data.id;
            $scope.projectSaved = true;
			$scope.search.savingProject = false;
			initiation(res,$scope)
		},function(res){
			$scope.search.savingProject = false;
			handleErrors(res);
		});
	}
	$scope.saveKeyword = function(x){
		x.savingKeyword = true;
		x.search_keyword = $scope.search.keyword;
        x.main_keyword = $scope.main_keyword;
        x.main_keyword_id = $scope.main_keyword_id;
        x.evaluate_keyword = $scope.evaluate_keyword;
		x.tmp_id = $scope.tmp_id;
		$http.post('/tools/keywordresearch/store', x).then(function (res) {
			x.savingKeyword = false;
			x.savedKeyword = true;
            // x.evaluate_keyword = true;
			//make the whole project saved
			$scope.isSaved = true;

			$scope.savedSearches = res.data
		},function(res){
			x.savingKeyword = false;
			handleErrors(res);
		});
	}

	$scope.saveKeywordFilter = function(x,keyword,tmpId,id,language){
		var data = {
			keyword_data : x,
			main_keyword : keyword,
			main_keyword_id : id,
			tmp_id 		 : tmpId,
			lang 		 : language,
			};

		var url = '/tools/keywordresearch/storefilteredkeyword';
			$http.post(
			     url,
			     data  
			).then(function (res) {
				$scope.savedSearches = res.data
			},function(res){
				x.savingKeyword = false;
				handleErrors(res);
			});
	}

	$scope.defaultTabs = function(){
		$scope.tabs = [
			{ title: 'Search Results'}
		];
		$scope.activePanelForSearchResults = 0;
	}
	$scope.defaultTabs();

	$scope.addTab = function(e) {
		var added = false;
	    angular.forEach($scope.tabs, function(v,k) {
    		if (e.keyword == v.keyword && k >0) {
				$scope.tabs[k] = e;
				added = true;
    		}
	    });
	    if (!added){$scope.tabs.push(e);}
		$timeout(function() {
			var nv = e.keyword.split(' ').join('');
			$('#'+nv).trigger('click');		
			//$scope.activePanelForSearchResults = ($scope.tabs.length - 1);
		});
	};

	$scope.countDomainFinder = 0;
	$scope.getDomain = function(x) {
		if ($scope.countDomainFinder == 3) {
			showError('Sorry you can\'t process more than 3 Domain Finder at once.');
			return;
		}
		$scope.countDomainFinder += 1;
		x.findDomain = true;
		$http.post('/tools/keywordresearch/postDomainAvailability',{keyword: [x.keyword]}).then(function (res) {
			x.findDomain = false;
			x.domains = res.data[x.keyword];
			$scope.countDomainFinder -= 1;
		},function(res){
			$scope.countDomainFinder -= 1;
			x.findDomain = false;
			handleErrors(res);
		});
	};
	// $scope.getDomain({keyword : 'hello'})

	$scope.checkAllDomains = function(domainKeywords){

		if($scope.pagination.amount > 10){
			$.alert({
				title:'Warning !',
                content: 'Domain Check will not work for more than 10 keywords, Please select 10 from the dropdown below Filter button.'
			});
		}else{
            $scope.checkDomains = true;
            var keywords = [];
            angular.forEach(domainKeywords, function(v,k) {
                v.findDomain = true;
                keywords.push(v.keyword);
            });
            openCheckDomainModal();
            $http.post('/tools/keywordresearch/postDomainAvailability',{keyword: keywords}).then(function (res) {
                closeCheckDomainModal();
                $scope.checkDomains = false;
                angular.forEach($scope.researchData,function(v,k) {
                    if (!res.data[v.keyword]) {return;}
                    v.findDomain = false;
                    v.domains = res.data[v.keyword];
                });
            },function(res){
                closeCheckDomainModal();
                $scope.checkDomains = false;
                handleErrors(res);
            });
		}

	}

	$scope.changeTab = function(x){
		$scope.activePanelForSearchResults = x;
	}
	$scope.removeEvalauatedKeyword = function(index){
		$scope.tabs.splice(index, 1);
		if (index == $scope.activePanelForSearchResults) {
			$scope.activePanelForSearchResults = $scope.activePanelForSearchResults-1;
		}else{
			$scope.activePanelForSearchResults = 0;
		}
	} 
	$scope.changeCurrentEvalauatedKeyword = function(x){
		$scope.currentEvaluate = x;
	}
	$scope.seeMore = function(x,length = 32){
		return x.replace("http://", "").length > length ? x.replace("http://", "").slice(0, length)+' ...' : x.replace("http://", "");
	}
	$scope.easypiechartOptions = {
		animate:{
			duration:1000,
			enabled:true
		},
		barColor: function (percent) {
			return (percent >= 0 && percent <= 30 ? '#5fbeaa' : percent > 30 && percent <= 60 ? '#36a2eb' : '#ff6384');
		},
		scaleColor:false,
		size: 180,
		lineHeight:'170px',
		lineWidth:20,
		lineCap:'butt'
	};
    $scope.difficultyText = function(difficulty){
    	/* Make sure that difficulty is a number */
    	difficulty = Number(difficulty);

        if (difficulty >= 0 && difficulty <= 10) {
            return 'No competition';
        }
        else if (difficulty >= 11 && difficulty <= 20) {
           return 'Extremely low competition';
        }
        else if (difficulty >= 21 && difficulty <= 30) {
           return 'Low competition';
        }
        else if (difficulty >= 31 && difficulty <= 40) {
           return 'Moderate Competition';
        }
        else if (difficulty >= 41 && difficulty <= 50) {
           return 'Somewhat High Competition';
        }
        else if (difficulty >= 51 && difficulty <= 65) {
           return 'Very High Competition';
        }
        else {
           return 'Do not even think about it!';
        }
    }


    $scope.downloadEvaluated = function(evaluated) {
        var data = evaluated ? $scope.evaluatedKeywords : $scope.researchData;
        $('#DownloadCsvData').modal('toggle');
		$http.post('/tools/keywordresearch/downloadEvaluated/'+$scope.tmp_id+'/'+evaluated,{researches: data}).then(function (res) {
	        window.location.href = '/tools/keywordresearch/downloadEvaluated/file';
		},function(res){
			handleErrors(res);
		});
    }
    /* Delete an Item from SavedProjects */
    $scope.$on('ITEM_IS_COMNFIRMED_AND_DELETED', function(ev, item) {
				$("#parent_"+item.data.id).remove();
                $scope.savedSearches.splice(item.index, 1);
                $scope.initialValues();
	});

	/* Re-Evaluate evaluated Item */
    $scope.$on('RE_EVALUATE_RESEARCH_KEYWORD', function(ev, item) {
	    angular.forEach($scope.researchData,function(v,k) {
	    	if (v.keyword == item.data.keyword) {
		    	$scope.evaluate(v,true,item.reEvaluatingAgain)
	    	}
	    });

	});
}]).directive('uibTabButton', function() {
	return {
		restrict: 'EA',
		scope: {
			handler: '&',
			text:'@'
		},
		template: '<li class="uib-tab nav-item">' +
		'<a href="javascript:;" ng-click="handler()" class="nav-link" ng-bind="text"></a></li>',
		replace: true
	}
}).directive('revearlerSearch', function(){
  return {
	    restrict: 'A',
	    templateUrl:'/tools/keywordresearch/ang/search'
	};
}).directive('elasticTextarea', [
    '$timeout',
    function($timeout) {
        return {
            restrict: 'A',
            link: function($scope, element) {
                $scope.initialHeight = $scope.initialHeight || element[0].style.height;
                var resize = function() {
                    element[0].style.height = $scope.initialHeight;
                    element[0].style.height = "" + element[0].scrollHeight + "px";
                };
                element.on("input change", resize);
                $timeout(resize, 0);
            }
        };
    }
]).directive('orderTable', function($timeout){
  return {
    restrict: 'A',
    scope:{
	    target: '@orderTable'
    },
    link: function(scope, element, attrs){
        $(element).click(function() {
        	scope.$parent.defaultPagination();
        	$('.dataTable th[order-table]').removeClass('sorting_asc sorting_desc').addClass('sorting');
			scope.$parent.filterSearch.orderReverse = !scope.$parent.filterSearch.orderReverse;
        	$(this).removeClass('sorting').addClass( scope.$parent.filterSearch.orderReverse ? 'sorting_asc' : 'sorting_desc');
			scope.$parent.filterSearch.orderBy = scope.target;
			$timeout(function(){
				scope.toggleState = !scope.toggleState;
			});
        });

    }
  };
})
//We already have a limitTo filter built-in to angular,
//let's make a startFrom filter
.filter('startFrom', function() {
    return function(input, start) {
        start = +start; //parse to int
        return input.slice(start);
    }
})
.directive('evaluateItem', function(){
  return {
	    restrict: 'A',
	    templateUrl:'/tools/keywordresearch/ang/evaluate'
	};
}).controller('ModalCtrl', function ($uibModal, $log, $document,$rootScope) {
  var $ctrl = this;
  $ctrl.item = {};

  $ctrl.open = function (item, index,reEvaluating=false) {
  	$ctrl.item  = {data: item, index: index,reEvaluating: reEvaluating};
    var modalInstance = $uibModal.open({
      animation: true,
      ariaLabelledBy: 'modal-title',
      ariaDescribedBy: 'modal-body',
      templateUrl: 'myModalContent.html',
      controller: 'ModalInstanceCtrl',
      controllerAs: '$ctrl',
      resolve: {
        items: function () {
          return $ctrl.item;
        }
      }
    });
    modalInstance.result.then(function (selectedItem) {
      $ctrl.selected = selectedItem;
    }, function () {
      // $log.info('Modal dismissed at: ' + new Date());
    });
  };

  $ctrl.openComponentModal = function () {
    var modalInstance = $uibModal.open({
      animation: true,
      component: 'modalComponent',
      resolve: {
        items: function () {
          return $ctrl.item;
        }
      }
    });

    modalInstance.result.then(function (selectedItem) {
      $ctrl.selected = selectedItem;
    }, function () {
      // $log.info('modal-component dismissed at: ' + new Date());
    });
  };

});
App.controller('ModalInstanceCtrl', function ($uibModalInstance, items, $http,$rootScope) {
	var $ctrl = this;
	$ctrl.item = items;
	$ctrl.reEvaluating = $ctrl.item.reEvaluating;

	$ctrl.ok = function (reEvaluatingAgain=true) {
		if ($ctrl.item.reEvaluating){
			$ctrl.item.reEvaluatingAgain = reEvaluatingAgain;
			/* Emit re-evaluating to parent to handle it */
		  	$rootScope.$broadcast('RE_EVALUATE_RESEARCH_KEYWORD', $ctrl.item);
		    $uibModalInstance.dismiss('cancel');
		}else{
			/* Handle deleting an item from saved keywords */
		  	$ctrl.deleting = true;

		  	$http({
				url: '/tools/keywordresearch/destroyAjax',
				method: 'get',
                params: {id : $ctrl.item.data.id,keyword:$ctrl.item.data.keyword}
			}).then(function (res) {
				var d = $ctrl.item.data.id;
			  	$ctrl.deleting = false;
			    $uibModalInstance.dismiss('cancel');
				//var subBlock01 = angular.element( document.querySelector( '#'+'child_'+$ctrl.item.data.id ) );
				//subBlock01.remove();
			  	$rootScope.$broadcast('ITEM_IS_COMNFIRMED_AND_DELETED', $ctrl.item);
			},function(res){
			  	$ctrl.deleting = false;
			    $uibModalInstance.dismiss('cancel');
				handleErrors(res);
			});
		}
	};
	$ctrl.cancel = function () {
		$uibModalInstance.dismiss('cancel');
	};
	$ctrl.loadSaved = function () {
		if ($ctrl.item.data.savedKeyword){
			$ctrl.notSavedKeyword = false;
			$ctrl.ok(false);
		}else{
			$ctrl.notSavedKeyword = true;
		}
	};
}).component('modalComponent', {
  templateUrl: 'myModalContent.html',
  bindings: {
    resolve: '<',
    close: '&',
    dismiss: '&'
  },
  controller: function () {
    var $ctrl = this;

    $ctrl.$onInit = function () {
      $ctrl.items = $ctrl.resolve.items;
      $ctrl.selected = {
        item: $ctrl.items[0]
      };
    };

    $ctrl.ok = function () {
      $ctrl.close({$value: $ctrl.selected.item});
    };

    $ctrl.cancel = function () {
      // $ctrl.dismiss({$value: 'cancel'});
    };
  }
})
.filter('paginationfilter', function () {
    return function (items, scope) {
    	var start = (scope.pagination.page-1)*scope.pagination.amount;
    	var end = (scope.pagination.page*scope.pagination.amount)-1;
	    var filteredItems = items.filter(function(item, index, array) {
	    	if(index >= start && index <= end){
	    		return true;
	    	}
	    	return false;
	    });
	    scope.filteredItemsLength = items.length;
	    return filteredItems;
    };
})
