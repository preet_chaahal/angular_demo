<?php
require_once(dirname(__FILE__) . '/../../../vendor/chargebee/chargebee-php/lib/ChargeBee.php');

define("FP_API_KEY_CRON", "79a28ce9fb4c056ddc68753cbf83d455");
define("FP_WTID_CRON", "d99e1f2fc321a8f2e20e");
define("CHARGEBEE_SITE_CRON", "kwrev-test");
define("CHARGEBEE_KEY_CRON", "test_4L8RJtYBvUCwWkWNBR6bQ2rq3PWEpCNf");

ChargeBee_Environment::configure("kwrev-test","test_4L8RJtYBvUCwWkWNBR6bQ2rq3PWEpCNf");
//ChargeBee_Environment::configure(CHARGEBEE_SITE_CRON, CHARGEBEE_KEY_CRON);

$downgradeUnpaidUsers = function ($host, $dbName, $user, $pass) {
    $pdo = new PDO("mysql:host={$host};dbname={$dbName}", $user, $pass);
    $pdo->prepare("set names 'utf8'")->execute();

    $usersResource = $pdo->prepare(
        'SELECT u.`stripe_subscription` as stripe_subscription, u.`id` as uid, p.`id` as payment_id ' .
        'FROM `users` as u ' .
        'INNER JOIN `payment` as p ' .
        'ON p.`profile_id` = u.`stripe_subscription` ' .
        'WHERE u.`pending_payment` in (:paymentStatus_1, :paymentStatus_2) AND p.`payment_status` = :pendingStatus'
    );

    $usersResource->execute([
        'paymentStatus_1' => 1,
        'paymentStatus_2' => 4,
        'pendingStatus'   => 'pending'
    ]);

    foreach ($usersResource as $user) {
        $updateResources[] = $pdo->prepare(
            'UPDATE `users` SET `pending_payment` = 2, `stripe_active` = 0, `account_id` = 1 WHERE `id` = :uid'
        );

        $updateResources[] = $pdo->prepare(
            'UPDATE `role_user` SET `role_id` = 1 WHERE `user_id` = :uid'
        );

        foreach($updateResources as $updateResource) {
            $updateResult = $updateResource->execute(['uid' => $user['uid']]);
            if (!$updateResult) {
                e('Cron', "Could not change Users account for pending payment (User #{$user['uid']})");
            }
        }
    }
};

$downgradeChargebeeCustomers = function ($host, $dbName, $user, $pass) {
    $pdo = new PDO("mysql:host={$host};dbname={$dbName}", $user, $pass);
    $pdo->prepare("set names 'utf8'")->execute();

    $usersResource = $pdo->prepare(
        'SELECT id,email,chargebee_subscription_id,chargebee_subscription_start_date,chargebee_subscription_end_date,account_id,payment_mode FROM users WHERE chargebee_subscription_end_date < :currentDate'
    );

    $usersResource->execute([
        'currentDate' => date("Y-m-d"),
    ]);
    $response = array();
    foreach ($usersResource as $user) {
        $subscription     = array();
        try {
            $result       = ChargeBee_Subscription::retrieve($user['chargebee_subscription_id']);
            $subscription = $result->subscription();
            //$invoice      = $result->invoice();
        } catch (\Exception $e) {
            //Log::info(json_encode($e));
        }

        if(!empty($subscription)) {
            $updateResources = array();
            if(isset($subscription->dueInvoicesCount) && $subscription->dueInvoicesCount == 0) {
                //$paymentDue    = (isset($subscription->dueInvoicesCount) ? $subscription->dueInvoicesCount : "NA");
                $ch_expiryDate = (isset($subscription->currentTermEnd) ? date("Y-m-d",$subscription->currentTermEnd) : '');

                //update subscription end date 
                //add new row in invoice table
                $updateResources[] = $pdo->prepare(
                    'UPDATE `users` SET `chargebee_subscription_end_date` = "'.$ch_expiryDate.'" WHERE `id` = :uid'
                );
            } else {
                //update plan to downgrade
                $cb_subscription_status = cancelNewSubscription($user['chargebee_subscription_id']);
                if($cb_subscription_status == true) {
                    $updateResources[] = $pdo->prepare(
                        'UPDATE `users` SET `pending_payment` = 2, `stripe_active` = 0, `account_id` = 1, `chargebee_active` = 0, `chargebee_customer_id` = 0, `chargebee_subscription_id` = 0, `stripe_active` = 0, `payment_mode` = "0" WHERE `id` = :uid'
                    );

                    $updateResources[] = $pdo->prepare(
                        'UPDATE `role_user` SET `role_id` = 1 WHERE `user_id` = :uid'
                    );
                    user_cancellation_firstpromoter($user['email']);
                }
            }

            if(!empty($updateResources)) {
                foreach($updateResources as $updateResource) {
                    $updateResult = $updateResource->execute(['uid' => $user['id']]);
                    if (!$updateResult) {
                        e('Cron', "Could not change Users account for pending payment (User #{$user['uid']})");
                    }
                }
            }
        }
    }
};

$updateAffiliateTrialCustomers = function ($host, $dbName, $user, $pass) {
    $pdo = new PDO("mysql:host={$host};dbname={$dbName}", $user, $pass);
    $pdo->prepare("set names 'utf8'")->execute();

    $usersResource = $pdo->prepare(
        'SELECT id,email,trial_expire_date,account_id FROM users WHERE is_trial = :isTrial AND trial_expire_date < :currentDate'
    );

    $usersResource->execute([
        'currentDate' => date("Y-m-d"),
        'isTrial'     => 1,
    ]);

    $response = array();
    foreach ($usersResource as $user) {
        //user_cancellation_firstpromoter($user['email']);
        trackExpriringTrialAccount($user['email']);
        /*$updateResources = array();
        if(isset($user['email']) && !empty($user['email'])) {
            $updateResources[] = $pdo->prepare(
                'UPDATE `users` SET `pending_payment` = 2, `stripe_active` = 0, `account_id` = 1, `chargebee_active` = 0, `is_trial` = 0, `stripe_active` = 0, `payment_mode` = "0" WHERE `id` = :uid'
            );

            $updateResources[] = $pdo->prepare(
                'UPDATE `role_user` SET `role_id` = 1 WHERE `user_id` = :uid'
            );
        }
        if(!empty($updateResources)) {
            foreach($updateResources as $updateResource) {
                $updateResult = $updateResource->execute(['uid' => $user['id']]);
                if (!$updateResult) {
                    e('Cron', "Could not change Users account for pending payment (User #{$user['uid']})");
                }
            }
        }*/
    }
};

$downgradeUnpaidUsers(DB_HOST, DB_DATABASE, DB_USERNAME, DB_PASSWORD);
$downgradeChargebeeCustomers(DB_HOST, DB_DATABASE, DB_USERNAME, DB_PASSWORD);
$updateAffiliateTrialCustomers(DB_HOST, DB_DATABASE, DB_USERNAME, DB_PASSWORD);

function cancelNewSubscription($cb_sub_id) {
    try {
        $result = ChargeBee_Subscription::retrieve($cb_sub_id);
        $subscription = $result->subscription();
        if(isset($subscription->status) && $subscription->status == "cancelled") {
            return true;
        }

        $result = ChargeBee_Subscription::cancel($cb_sub_id);
        if(isset($result->subscription()->status) && $result->subscription()->status == "cancelled") {
            return true;
        }
    } catch (\Exception $e) {
        return false;
    }
}

function user_cancellation_firstpromoter($email) {
    $params = json_encode(['email' => $email]);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://firstpromoter.com/api/v1/track/cancellation?api_key=' . FP_API_KEY_CRON . '&wid=' . FP_WTID_CRON . '');
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json; charset=utf-8',
            'Content-Length: ' . strlen(($params)))
    );

    $output = curl_exec($ch);
    $result = json_encode($output);
}

function trackExpriringTrialAccount($email) {
    $sale_params = [
        'email'      => (isset($email) ? $email : ''),
        'first_name' => '',
        'event_id'   => '',
        'uid'        => 'trial',
        'amount'     => '12',
        'tid'        => (isset($_COOKIE['_fprom_track']) ? $_COOKIE['_fprom_track'] : ''),
        'plan'       => 'trial plan'
    ];

    $params = json_encode($sale_params);
    $ch     = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://firstpromoter.com/api/v1/track/sale?api_key=' . FP_API_KEY_CRON . '&wid=' . FP_WTID_CRON . '');
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json; charset=utf-8',
            'Content-Length: ' . strlen(($params)))
    );
    $output = curl_exec($ch);
    $result = json_encode($output);
}