<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCasperResultTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('casper_results', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('casper_id');
            $table->string('ad_group');
            $table->string('keyword');
            $table->string('currency');
            $table->string('avg_month_searches');
            $table->string('competition');
            $table->string('suggested_bid');
            $table->string('impression_share');
            $table->string('organic_impression_share');
            $table->string('organic_avg_position');
            $table->string('in_account');
            $table->string('in_plan');
            $table->string('extracted_from');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('casper_results');
    }
}
