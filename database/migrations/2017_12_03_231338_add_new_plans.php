<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewPlans extends Migration
{
    /*
    * New plans for Stripe and PayPal
    */
    protected $newplans = [
      ['name' => 'new_basic_monthly', 'id' => 10, 'description' => 'KeywordRevealer Basic - Monthly'],
      ['name' => 'new_basic_yearly',  'id' => 11, 'description' => 'KeywordRevealer Basic - Yearly'],
      ['name' => 'new_pro_monthly',   'id' => 12, 'description' => 'KeywordRevealer Pro - Monthly'],
      ['name' => 'new_pro_yearly',    'id' => 13, 'description' => 'KeywordRevealer Pro - Yearly'],
      ['name' => 'new_elite_monthly', 'id' => 14, 'description' => 'KeywordRevealer Elite - Monthly'],
      ['name' => 'new_elite_yearly',  'id' => 15, 'description' => 'KeywordRevealer Elite - Yearly'],
    ];

     /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      /*
      * Disable Old plans
      */
      DB::table('accounts')->whereIn('id',[2,3,4,5,6,7,8,9])->update(['disabled' => 1]);

      /*
      * Save new Plans in accounts table
      */
      foreach ($this->newplans as $plan) {
        $plan['stripe_plan'] = $plan['description'];
        $plan['paypal_plan'] = $plan['description'];
        $plan['updated_at']  = \Carbon\Carbon::now();
        $plan['created_at']  = \Carbon\Carbon::now();

        DB::table('accounts')->insert($plan);
      }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      /*
      * enable old plans
      */
      DB::table('accounts')->whereIn('id',[2,3,4,5,6,7,8,9])->update(['disabled' => 0]);

      /*
      * Delete New plans
      */
      DB::table('accounts')->whereIn('id', collect($this->newplans)->pluck('id'))->delete();
    }
}
