<?php

use App\ResearchKeyword;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMigrationChangeDataIntoKeywordSearch extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $engines = Config::get('searchengines');
        $keywordResearch = ResearchKeyword::all();
        foreach ($engines as $engine) {
            foreach ($keywordResearch as $keyword)
                if ($engine['name'] == $keyword->google_engine) {
                    ResearchKeyword::whereId($keyword->id)->update(['google_engine' => $engine['engine']]);
                }

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
    }
}
