<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewRolesToRolesTable extends Migration
{

    protected $newRoles = [
      [
        'id'           => 7,
        'name'         => 'new_basic',
        'display_name' => 'New Basic Plan User',
        'description'  => 'New Basic Plan User',
      ],[
        'id'           => 8,
        'name'         => 'new_pro',
        'display_name' => 'New Pro Plan User',
        'description'  => 'New Pro Plan User',
      ],[
        'id'           => 9,
        'name'         => 'new_elite',
        'display_name' => 'New Elite Plan User',
        'description'  => 'New Elite Plan User',
      ],
    ];
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      foreach ($this->newRoles as $row) {
        $row['created_at'] = \Carbon\Carbon::now();
        $row['updated_at'] = \Carbon\Carbon::now();
        DB::table('roles')->insert($row);
      }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      DB::table('roles')->whereIn('id', [7,8,9])->delete();
    }
}
