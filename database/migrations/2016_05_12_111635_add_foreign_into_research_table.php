<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignIntoResearchTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \App\Research::whereNotNull('keyword_search_id')->update(['keyword_search_id'=>null]);

        Schema::table('research', function (Blueprint $table) {
            $table->integer('keyword_search_id')->unsigned()->nullable()->change();
            $table->foreign('keyword_search_id')->references('id')->on('research_keywords')->onUpdate('NO ACTION')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('research', function (Blueprint $table) {
            $table->dropForeign('research_keyword_search_id_foreign');
        });
    }
}
