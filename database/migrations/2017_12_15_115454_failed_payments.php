<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FailedPayments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('failed_payments', function (Blueprint $table) {
          $table->increments('id');
          $table->string('gateway');
          if (Schema::hasTable('users') && Schema::hasColumn('users','id')) {
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users');
          }
          $table->string('invoice_id')->nullable();
          $table->string('customer')->nullable();
          $table->boolean('downgraded')->default(false);
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('failed_payments');
    }
}
