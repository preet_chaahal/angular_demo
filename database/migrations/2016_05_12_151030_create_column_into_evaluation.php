<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateColumnIntoEvaluation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('evaluations', function (Blueprint $table) {
            $table->integer('monthly_searches')->after('keyword');
            $table->decimal('keyword_cpc', 12, 2)->after('monthly_searches');
            $table->decimal('estimated_profile', 12, 2)->after('keyword_cpc');
            $table->integer('word_count')->after('estimated_profile');
            $table->string('google_engine')->after('word_count');
            $table->integer('user_current_level')->after('google_engine');
            $table->integer('difficulty')->after('user_current_level');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('evaluations', function (Blueprint $table) {
            $table->dropColumn('monthly_searches');
            $table->dropColumn('keyword_cpc');
            $table->dropColumn('estimated_profile');
            $table->dropColumn('word_count');
            $table->dropColumn('google_engine');
            $table->dropColumn('user_current_level');
            $table->dropColumn('difficulty');
        });
    }
}
