<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddResearchTmpIdColumnIntoResearchKeywordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('research_keywords', function (Blueprint $table) {
            $table->integer('research_tmp_id')->nullable()->unsigned()->after('keyword');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('research_keywords', function (Blueprint $table) {
            $table->dropColumn('research_tmp_id');
        });
    }
}
