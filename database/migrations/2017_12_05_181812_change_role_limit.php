<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeRoleLimit extends Migration
{

    protected $oldLimits = [
      [
        'id'         => 1,
        'role_id'    => 1,
        'research'   => 3,
        'evolution'  => 3,
        'brainstorm' => 3,
        'tracking'   => 1,
        'can_save'   => 0,
        'project'    => 0
      ],[
        'research'   => 15,
        'evolution'  => 25,
        'brainstorm' => 25,
        'tracking'   => 50,
        'can_save'   => 30,
        'project'    => 3
      ],[
        'research'   => 50,
        'evolution'  => 50,
        'brainstorm' => 50,
        'tracking'   => 100,
        'can_save'   => 100,
        'project'    => 10
      ],[
        'research'   => 100,
        'evolution'  => 100,
        'brainstorm' => 100,
        'tracking'   => 200,
        'can_save'   => 500,
        'project'    => 50
      ]
    ];

    protected $newLimits = [
      [
        'id'          => 1,
        'research'    => 3,
        'role_id'     => 1,
        'evolution'   => 7,
        'brainstorm'  => 3,
        'tracking'    => 0,
        'can_save'    => 0,
        'project'     => 0,
        'description' => 'new free'
      ],[
        'id'          => 7,
        'research'    => 25,
        'role_id'     => 7,
        'evolution'   => 50,
        'brainstorm'  => 25,
        'tracking'    => 25,
        'can_save'    => 25,
        'project'     => 3,
        'description' => 'new basic'
      ],[
        'id'          => 8,
        'research'    => 150,
        'role_id'     => 8,
        'evolution'   => 200,
        'brainstorm'  => 150,
        'tracking'    => 150,
        'can_save'    => 150,
        'project'     => 50,
        'description' => 'new pro'
      ],[
        'id'          => 9,
        'research'    => 400,
        'role_id'     => 9,
        'evolution'   => 450,
        'brainstorm'  => 400,
        'tracking'    => 400,
        'can_save'    => 400,
        'project'     => 100,
        'description' => 'new elite'
      ]
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      if (Schema::hasTable('role_limit') && !Schema::hasColumn('role_limit', 'description')) {
        Schema::table('role_limit', function(Blueprint $table){
          $table->string('description')->nullable();
        });
      }
      DB::table('role_limit')->where('id', 1)->delete();
      foreach ($this->newLimits as $plan) {
        $plan['updated_at']  = \Carbon\Carbon::now();
        $plan['created_at']  = \Carbon\Carbon::now();

        DB::table('role_limit')->insert($plan);
      }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      DB::table('role_limit')->whereIn('role_id', [1,7,8,9])->delete();
      DB::table('role_limit')->insert($this->oldLimits[0]);

      if (Schema::hasTable('role_limit') && Schema::hasColumn('role_limit', 'description')) {
        Schema::table('role_limit', function(Blueprint $table){
          $table->dropColumn('description');
        });
      }
    }
}
