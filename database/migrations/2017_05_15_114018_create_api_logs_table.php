<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApiLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('api_logs', function (Blueprint $table) {
            $table->engine = 'MyISAM';

            $table->unsignedBigInteger('id', true);
            $table->string('url')->nullable()->default(null);
            $table->unsignedInteger('user_id')->nullable()->default(null);
            $table->longText('request_body')->nullable()->default(null);
            $table->string('response_http_code')->nullable()->default(null);
            $table->longText('response_body')->nullable()->default(null);
            $table->string('system_error_code')->nullable()->default(null);
            $table->timestamp('created_at');

            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('set null')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('api_logs');
    }
}
