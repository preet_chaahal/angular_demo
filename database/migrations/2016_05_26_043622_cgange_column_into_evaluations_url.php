<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CgangeColumnIntoEvaluationsUrl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('evaluations_url', function (Blueprint $table) {
            $table->dropForeign('evaluations_url_keyword_search_id_foreign');
        });
        Schema::table('evaluations_url', function (Blueprint $table) {
            $table->renameColumn('keyword_search_id', 'keyword_research_id');
        });
        Schema::table('evaluations_url', function (Blueprint $table) {
            $table->foreign('keyword_research_id')->references('id')->on('research')->onUpdate('NO ACTION')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('evaluations_url', function (Blueprint $table) {
            $table->dropForeign('evaluations_url_keyword_research_id_foreign');
        });
        Schema::table('evaluations_url', function (Blueprint $table) {
            $table->renameColumn('keyword_research_id', 'keyword_search_id');
        });
        Schema::table('evaluations_url', function (Blueprint $table) {
            $table->foreign('keyword_search_id')->references('id')->on('research')->onUpdate('NO ACTION')->onDelete('CASCADE');
        });
    }
}
