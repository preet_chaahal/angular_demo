<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FillInBlankCellsLanguageFieldResearchTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $researches = DB::table('research')->get();

        foreach ($researches as $research) {
            if ($research->language == '') {
                DB::table('research')->where('id', $research->id)->update([
                    'language' => 'all'
                ]);
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
