<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewPermisssionsForNewRoles extends Migration
{

    protected $newPermissions = [
      //new basic permissions
      ['role_id' => 7, 'permission_id' => [1,2,3,4,7,8,9,10]],

      //new pro permissions
      ['role_id' => 8, 'permission_id' => [1,2,3,4,7,8,9,10]],

      //new elite permissions
      ['role_id' => 9, 'permission_id' => [1,2,3,4,7,8,9,10]]
    ];
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      foreach ($this->newPermissions as $row){
        $rows = [];
        foreach ($row['permission_id'] as $id) {
          $rows[] = [
            'role_id'       => $row['role_id'],
            'permission_id' => $id,
          ];
        }
        DB::table('permission_role')->insert($rows);
      }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      DB::table('permission_role')->whereIn('role_id', collect($this->newPermissions)->pluck('role_id'))->delete();
    }
}
