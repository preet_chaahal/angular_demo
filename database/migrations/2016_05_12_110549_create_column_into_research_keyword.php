<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateColumnIntoResearchKeyword extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('research_keywords', function (Blueprint $table) {
            $table->string('keyword')->after('id');
            $table->integer('user_id')->unsigned()->nullable()->after('keyword');
        });
        Schema::table('research_keywords', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('NO ACTION')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('research_keywords', function (Blueprint $table) {
            $table->dropForeign('research_keywords_user_id_foreign');
        });
        Schema::table('research_keywords', function (Blueprint $table) {
            $table->dropColumn('keyword');
            $table->dropColumn('user_id');
        });
    }
}
