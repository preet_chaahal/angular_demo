<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewPlansForTheCampaign extends Migration
{
    /*
    * New plans for Stripe and PayPal
    */
    protected $newplans = [
        ['name' => 'lifetime_starter', 'id' => 16, 'description' => 'KeywordRevealer Lifetime - Starter'],
        ['name' => 'lifetime_pro',     'id' => 17, 'description' => 'KeywordRevealer Lifetime - Pro']
    ];

    /*
    * This is the new plan limit
    */
    protected $newPlansLimit = [
      [
        'id'          => 10,
        'role_id'     => 10,
        'research'    => 20,
        'evolution'   => 30,
        'brainstorm'  => 20,
        'tracking'    => 20,
        'can_save'    => 20,
        'project'     => 3,
        'description' => 'new lifetime starter'
      ],[
        'id'          => 11,
        'role_id'     => 11,
        'research'    => 60,
        'evolution'   => 100,
        'brainstorm'  => 50,
        'tracking'    => 100,
        'can_save'    => 150,
        'project'     => 100,
        'description' => 'new lifetime pro'
      ]
    ];

    /*
    * New roles 
    */
    protected $newRoles = [
        [
            'id'           => 10,
            'name'         => 'lifetime_starter',
            'display_name' => 'Lifetime Starter Plan User',
            'description'  => 'Lifetime Starter Plan User'
        ],[
            'id'           => 11,
            'name'         => 'lifetime_pro',
            'display_name' => 'Lifetime Pro Plan User',
            'description'  => 'Lifetime Pro Plan User'
        ]
    ];

    /*
    * New Permissions
    */
    protected $newPermissions = [
        /*
        * New Campaign Starter
        */
        ['role_id' => 10, 'permission_id' => [1,2,3,4,7,8,9,10]],

        /*
        * New Campaign Pro
        */
        ['role_id' => 11, 'permission_id' => [1,2,3,4,7,8,9,10]]
    ];

    /**
    * Run the migrations.
    *
    * @return void
    */
    public function up()
    {

        /*
        * Save new Plans in the accounts table
        */
        foreach ($this->newplans as $plan) {
            $plan['stripe_plan'] = $plan['description'];
            $plan['paypal_plan'] = $plan['description'];
            $plan['updated_at']  = \Carbon\Carbon::now();
            $plan['created_at']  = \Carbon\Carbon::now();

            DB::table('accounts')->insert($plan);
        }

        /*
        * Save new Plans Limit in the role_limit table
        */
        foreach ($this->newPlansLimit as $plan) {
            $plan['updated_at']  = \Carbon\Carbon::now();
            $plan['created_at']  = \Carbon\Carbon::now();

            DB::table('role_limit')->insert($plan);
        }

        /*
        * Save new Roles to the roles table
        */
        foreach ($this->newRoles as $row) {
            $row['created_at'] = \Carbon\Carbon::now();
            $row['updated_at'] = \Carbon\Carbon::now();
            DB::table('roles')->insert($row);
        }

        /*
        * Save new Permissions Roles to the permission_role table
        */
        foreach ($this->newPermissions as $row){
            $rows = [];
            foreach ($row['permission_id'] as $id) {
              $rows[] = [
                'role_id'       => $row['role_id'],
                'permission_id' => $id,
              ];
            }
            DB::table('permission_role')->insert($rows);
        }
    }

    /**
    * Reverse the migrations.
    *
    * @return void
    */
    public function down()
    {
        /*
        * Delete New plans
        */
        DB::table('accounts')->whereIn('id', collect($this->newplans)->pluck('id'))->delete();

        /*
        * Delete the added roles limit
        */
        DB::table('role_limit')->whereIn('id', collect($this->newPlansLimit)->pluck('id'))->delete();

        /*
        * Delete the added roles
        */
        DB::table('roles')->whereIn('id', collect($this->newRoles)->pluck('id'))->delete();

        /*
        * Delete the added Permission Roles
        */
        DB::table('permission_role')->whereIn('role_id', collect($this->newPermissions)->pluck('role_id'))->delete();
    }
}



