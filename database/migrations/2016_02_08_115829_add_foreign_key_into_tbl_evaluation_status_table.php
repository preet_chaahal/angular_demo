<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeyIntoTblEvaluationStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tbl_evaluate_status', function (Blueprint $table) {
            $table->integer('keyword_search_id')->nullable()->unsigned()->change();
            $table->foreign('keyword_search_id')->references('id')->on('tbl_keyword_search_status')->onUpdate('NO ACTION')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tbl_evaluate_status', function (Blueprint $table) {
            $table->dropForeign('tbl_evaluate_status_keyword_search_id_foreign');
        });
    }
}
