<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnIntoEvaluationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('evaluations', function (Blueprint $table) {
            $table->integer('keyword_search_id')->nullable()->unsigned();
        });
        Schema::table('evaluations', function (Blueprint $table) {
            $table->foreign('keyword_search_id')->references('id')->on('tbl_keyword_search_status')->onUpdate('NO ACTION')->onDelete('CASCADE');
        });
        Schema::table('tbl_evaluate_status', function (Blueprint $table) {
            $table->integer('evaluate_id')->nullable()->unsigned()->change();
            $table->foreign('evaluate_id')->references('id')->on('evaluations')->onUpdate('NO ACTION')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('evaluations', function (Blueprint $table) {
            $table->dropForeign('evaluations_keyword_search_id_foreign');
        });
        Schema::table('evaluations', function (Blueprint $table) {
            $table->dropColumn('keyword_search_id');
        });

        Schema::table('tbl_evaluate_status', function (Blueprint $table) {
            $table->dropForeign('tbl_evaluate_status_evaluate_id_foreign');
        });
    }
}
