<?php

use App\Role;
use Illuminate\Database\Migrations\Migration;

class PopulatePlanData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Role::whereName('elite')->exists() === false) {
            DB::table('roles')
                ->insert([
                    'id' => 5,
                    'name' => 'elite',
                    'display_name' => 'Elite Plan User',
                    'description' => 'Elite Plan User',
                    'created_at' => \Carbon\Carbon::now(),
                    'updated_at' => \Carbon\Carbon::now()
                ]);
        }

        if (Role::whereName('agency')->exists() === false) {
            DB::table('roles')
                ->insert([
                    'id' => 6,
                    'name' => 'agency',
                    'display_name' => 'Agency Plan User',
                    'description' => 'Agency Plan User',
                    'created_at' => \Carbon\Carbon::now(),
                    'updated_at' => \Carbon\Carbon::now()
                ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('roles')
            ->where('name', 'elite')
            ->orWhere('name', 'agency')
            ->delete();
    }
}
