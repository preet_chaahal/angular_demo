<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewColumnsIntoResearchKeywordTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('research_keywords', function (Blueprint $table) {
            $table->string('google_engine')->after('user_id');
            $table->integer('user_current_level')->after('google_engine');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('research_keywords', function (Blueprint $table) {
            $table->dropColumn('google_engine');
            $table->dropColumn('user_current_level');
        });
    }
}
