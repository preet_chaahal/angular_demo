<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateColumnCpcDataIntoBrainstorm extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('brainstorm', function (Blueprint $table) {
            $table->text('cpc_data')->after('data');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('brainstorm', function (Blueprint $table) {
           $table->dropColumn('cpc_data');
        });
    }
}
