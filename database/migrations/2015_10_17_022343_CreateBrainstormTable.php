<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBrainstormTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brainstorm', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('user_id');
            $table->text('keyword');
            $table->integer('group_count')->default(0);
            $table->integer('keyword_count')->default(0);
            $table->longText('data');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('brainstorm');
    }
}
