<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResearchTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('research', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('user_id');
            $table->string('keyword');
            $table->integer('monthly_searches');
            $table->decimal('adword_cpc', 12, 2);
            $table->decimal('estimated_profile', 12, 2);
            $table->integer('word_count');
            $table->string('location');
            $table->integer('difficulty');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('research');
    }
}
