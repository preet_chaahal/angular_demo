<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateColumnIntoResearch extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Schema::table('research', function (Blueprint $table) {
            Schema::table('research', function (Blueprint $table) {
                $table->integer('keyword_search_id')->nullable();
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('research', function (Blueprint $table) {
            Schema::table('research', function (Blueprint $table) {
                $table->dropColumn('keyword_search_id');
            });
        });
    }
}
