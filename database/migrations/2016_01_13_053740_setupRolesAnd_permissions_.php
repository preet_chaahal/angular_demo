<?php

use App\Role;
use App\Permission;
use Illuminate\Database\Migrations\Migration;

class SetupRolesAndPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $freeRole               = new Role();
        $freeRole->name         = 'free';
        $freeRole->display_name = 'Free Plan User'; // optional
        $freeRole->description  = 'Free Plan User'; // optional
        $freeRole->save();

        $basicRole               = new Role();
        $basicRole->name         = 'basic';
        $basicRole->display_name = 'Basic Plan User'; // optional
        $basicRole->description  = 'Basic Plan User'; // optional
        $basicRole->save();


        $proRole               = new Role();
        $proRole->name         = 'pro';
        $proRole->display_name = 'Pro Plan User'; // optional
        $proRole->description  = 'Pro Plan User'; // optional
        $proRole->save();


        $permissions = [
            [
                'name'         => 'keyword_searches',
                'display_name' => 'Can Search Keyword',
                'description'  => null
            ],
            [
                'name'         => 'keyword_evaluate',
                'display_name' => 'Can Evaluate Keyword',
                'description'  => null
            ],
            [
                'name'         => 'keyword_save',
                'display_name' => 'Can Save Keyword',
                'description'  => null
            ],
            [
                'name'         => 'domain_finder',
                'display_name' => 'Exact Match Domain Finder',
                'description'  => null
            ],
            [
                'name'         => 'filter_keyword_data',
                'display_name' => 'Can Filter Keyword Data',
                'description'  => null
            ],
            [
                'name'         => 'sortable_columns',
                'display_name' => 'Can Column Sort',
                'description'  => null
            ],
            [
                'name'         => 'export_data',
                'display_name' => 'Can Export Data',
                'description'  => null
            ],
            [
                'name'         => 'import_keywords',
                'display_name' => 'Can Import Keywords',
                'description'  => null
            ],
            [
                'name'         => 'brainstorm_tool',
                'display_name' => 'Can Use Brainstorm Tool',
                'description'  => null
            ],
            [
                'name'         => 'rank_checker',
                'display_name' => 'Can Use Rank Checker',
                'description'  => null
            ]
        ];

        $freePermissions = $basicPermissions = $proPermissions = [];
        $index           = 0;
        foreach ($permissions as $permission) {
            $permissionObj               = new Permission();
            $permissionObj->name         = $permission['name'];
            $permissionObj->display_name = $permission['display_name'];
            $permissionObj->save();
            if ($index <= 5) {
                $freePermissions[] = $permissionObj;
            }
            if ($index <= 6) {
                $basicPermissions[] = $permissionObj;
            }
            $proPermissions[] = $permissionObj;
            $index++;
        }

        $freeRole->attachPermissions($freePermissions);
        $basicRole->attachPermissions($basicPermissions);
        $proRole->attachPermissions($proPermissions);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Permission::whereNotNull('id')->delete();
        Role::whereNotNull('id')->delete();
    }
}
