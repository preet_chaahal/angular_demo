<?php

use App\Evaluation;
use App\TblEvaluationStatus;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeForignIntoEvaluation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('evaluations', function (Blueprint $table) {
            $table->dropForeign('evaluations_keyword_search_id_foreign');
        });
        Schema::table('evaluations', function (Blueprint $table) {
            $table->foreign('keyword_search_id')->references('id')->on('research_keywords')->onUpdate('NO ACTION')->onDelete('CASCADE');
        });
        Schema::table('tbl_evaluate_status', function (Blueprint $table) {
            $table->dropForeign('tbl_evaluate_status_evaluate_id_foreign');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $TblEvaluation=TblEvaluationStatus::all();
        $evaluation=Evaluation::all();
        $TblEvaluateId=array_pluck($TblEvaluation->toArray(),'evaluate_id');
        $evaluateId=array_pluck($evaluation->toArray(),'id');
        foreach ($TblEvaluateId as $id) {
            if (!in_array($id, $evaluateId)) {
                \App\TblEvaluationStatus::whereEvaluateId($id)->update(['evaluate_id' => Null]);
            }
        }
        Schema::table('tbl_evaluate_status', function (Blueprint $table) {
            $table->foreign('evaluate_id')->references('id')->on('evaluations')->onUpdate('NO ACTION')->onDelete('CASCADE');
        });
        Schema::table('evaluations', function (Blueprint $table) {
            $table->dropForeign('evaluations_keyword_search_id_foreign');
        });
        Schema::table('evaluations', function (Blueprint $table) {
            $table->foreign('keyword_search_id')->references('id')->on('tbl_keyword_search_status')->onUpdate('NO ACTION')->onDelete('CASCADE');
        });
    }
}
