<?php

use App\User;
use Illuminate\Database\Migrations\Migration;

class AssignRoleToUserAsPerPlan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /** @var User $users */
        $users = User::with('account')->get();
        foreach ($users as $user) {
            $planName = strtolower($user->account->name);
            if ($planName == 'Proyearly') {
                $planName = 'pro';
            }
            $role = \App\Role::where('name', $planName)->first();
            if ( ! empty($role)) {
                $user->attachRole($role);
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
