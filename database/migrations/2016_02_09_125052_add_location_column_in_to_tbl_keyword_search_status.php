<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLocationColumnInToTblKeywordSearchStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tbl_keyword_search_status', function (Blueprint $table) {
            $table->string('location')->nullable();
        });

        Schema::table('tbl_keyword_search_status', function (Blueprint $table) {
            \App\TblKeywordStatus::where('location',null)->update(['location'=>'United States']);
        });

        Schema::table('tbl_evaluate_status', function (Blueprint $table) {
            $table->string('location')->nullable()->after('difficulty');
        });

        Schema::table('tbl_evaluate_status', function (Blueprint $table) {
            \App\TblEvaluationStatus::where('location',null)->update(['location'=>'United States']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tbl_keyword_search_status', function (Blueprint $table) {
            $table->dropColumn('location');
        });
        Schema::table('tbl_evaluate_status', function (Blueprint $table) {
            $table->dropColumn('location');
        });
    }
}
