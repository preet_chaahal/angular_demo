<?php

use App\RoleLimit;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PopulateLimitData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('role_limit')->delete();
        $data = [
            [
                'id' => 1,
                'role_id' => 1,
                'research' => 3,
                'evolution' => 3,
                'brainstorm' => 3,
                'tracking' => 1,
                'can_save' => 1,
                'project' => 1,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ], [
                'id' => 2,
                'role_id' => 2,
                'research' => 15,
                'evolution' => 25,
                'brainstorm' => 25,
                'tracking' => 50,
                'can_save' => 30,
                'project' => 3,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ], [
                'id' => 3,
                'role_id' => 3,
                'research' => 50,
                'evolution' => 50,
                'brainstorm' => 50,
                'tracking' => 100,
                'can_save' => 100,
                'project' => 10,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ], [
                'id' => 5,
                'role_id' => 5,
                'research' => 100,
                'evolution' => 100,
                'brainstorm' => 100,
                'tracking' => 200,
                'can_save' => 500,
                'project' => 50,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ], [
                'id' => 6,
                'role_id' => 6,
                'research' => 200,
                'evolution' => 200,
                'brainstorm' => 200,
                'tracking' => 500,
                'can_save' => 1000,
                'project' => 100,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
        ];

        foreach ($data as $datum) {
            DB::table('role_limit')
                ->insert($datum);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
