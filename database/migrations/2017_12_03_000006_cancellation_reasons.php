<?php

use App\CancellationReason;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CancellationReasons extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('cancellation_reasons', function (Blueprint $table) {
          $table->increments('id');
          $table->string('reason');
      });

      /*
      * Create the default Cancellation Reasons
      */
      if (Schema::hasTable('cancellation_reasons')) {
        $reasons = [
          "This is only temporary. I’ll be back!",
          "I no longer needed it.",
          "It didn’t meet my needs.",
          "I found an alternative.",
          "Quality was less than expected.",
          "Ease of use was less than expected.",
          "Access to the service was less than expected.",
          "Customer service was less than expected.",
          "Subscription fees are too high."
        ];
        foreach ($reasons as $reason) {
          CancellationReason::create(['reason' => $reason]);
        }
      }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cancellation_reasons');
    }
}
