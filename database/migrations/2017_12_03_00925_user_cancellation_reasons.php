<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserCancellationReasons extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('user_cancellation_reasons', function (Blueprint $table) {
          $table->increments('id');
          if (Schema::hasTable('user_subscription_cancellations') && Schema::hasColumn('user_subscription_cancellations','id')) {
            $table->integer('cancellation_id')->unsigned();
            $table->foreign('cancellation_id')->references('id')->on('user_subscription_cancellations');
          }
          $table->string('reason_id')->nullable();
          $table->string('other')->nullable();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('user_cancellation_reasons');
    }
}
