<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class RemoveKeywordIdColumnIntoTblEvaluateStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tbl_evaluate_status', function (Blueprint $table) {
            $table->dropForeign('tbl_evaluate_status_keyword_search_id_foreign');
        });
        Schema::table('tbl_evaluate_status', function (Blueprint $table) {
            $table->dropColumn('keyword_search_id');
        });
        Schema::table('tbl_evaluate_status', function (Blueprint $table) {
            $table->dropColumn('evaluate_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tbl_evaluate_status', function (Blueprint $table) {
            $table->integer('keyword_search_id')->nullable()->unsigned()->after('user_id');
        });
        Schema::table('tbl_evaluate_status', function (Blueprint $table) {
            $table->integer('evaluate_id')->nullable()->unsigned()->after('keyword_search_id');
        });
        Schema::table('tbl_evaluate_status', function (Blueprint $table) {
            $table->foreign('keyword_search_id')->references('id')->on('tbl_keyword_search_status')->onUpdate('NO ACTION')->onDelete('CASCADE');
        });
    }
}
