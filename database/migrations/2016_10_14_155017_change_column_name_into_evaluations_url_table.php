<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeColumnNameIntoEvaluationsUrlTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('evaluations_url', function (Blueprint $table) {
           $table->renameColumn('page_rank','pinterest_share');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('evaluations_url', function (Blueprint $table) {
            $table->renameColumn('pinterest_share','page_rank');
        });
    }
}
