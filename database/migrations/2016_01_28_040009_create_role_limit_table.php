<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoleLimitTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('role_limit', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('role_id')->unique();
            $table->integer('research');
            $table->integer('evolution');
            $table->integer('brainstorm');
            $table->integer('tracking');
            $table->integer('can_save');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('role_limit');
    }
}
