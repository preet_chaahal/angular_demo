<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class InsertDataIntoTblProxyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tbl_access_proxy', function (Blueprint $table) {
            $proxies = DB::table('serposcope.proxy')->get();

            foreach ($proxies as $proxy) {
                DB::table('tbl_access_proxy')->where('url',$proxy->ip)->update([
                    'type' => $proxy->type,
                    'port' => $proxy->port,
                    'user' => $proxy->user,
                    'password' => $proxy->password,
                    'module' => $proxy->module
                ]);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tbl_access_proxy', function (Blueprint $table) {
            //
        });
    }
}
