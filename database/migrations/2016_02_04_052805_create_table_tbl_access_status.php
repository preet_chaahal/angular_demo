<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTblAccessStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_evaluate_status', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('user_id')->nullable()->unsigned();
            $table->bigInteger('keyword_search_id');
            $table->bigInteger('evaluate_id');
            $table->string('keyword');
            $table->integer('monthly_searches');
            $table->decimal('keyword_cpc', 12, 2);
            $table->decimal('estimated_profile', 12, 2);
            $table->integer('word_count');
            $table->string('google_engine');
            $table->integer('user_current_level');
            $table->integer('difficulty');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tbl_evaluate_status');
    }
}
