<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignInToEvaluationUrlTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('evaluations_url', function (Blueprint $table) {
            $table->foreign('keyword_search_id')->references('id')->on('research_keywords')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('evaluations_url', function (Blueprint $table) {
            $table->dropForeign('evaluations_url_keyword_search_id_foreign');
        });
    }
}
