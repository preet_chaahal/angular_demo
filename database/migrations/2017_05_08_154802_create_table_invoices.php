<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableInvoices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('user_id')->nullable()->unsigned();
            $table->string('customer_id', 20)->nullable();
            $table->string('subscription_id', 20)->nullable();
            $table->string('invoice_id', 30)->nullable();
            $table->tinyInteger('is_recuring')->default(0);
            $table->string('plan_id', 100)->nullable();
            $table->string('status', 20)->nullable();
            $table->decimal('amount', 8, 2);
            $table->timestamp('date');
            $table->timestamp('start_date');
            $table->timestamp('end_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('invoices');
    }
}
