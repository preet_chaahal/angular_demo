<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateColumnIntoAccessProxyLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tbl_access_proxy_logs', function (Blueprint $table) {
            $table->integer('proxy_id')->unsigned()->nullable()->after('ip');
        });
        Schema::table('tbl_access_proxy_logs', function (Blueprint $table) {
            $table->foreign('proxy_id')->references('id')->on('tbl_access_proxy')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tbl_access_proxy_logs', function (Blueprint $table) {
            $table->dropForeign('tbl_access_proxy_logs_proxy_id_foreign');
        });
        Schema::table('tbl_access_proxy_logs', function (Blueprint $table) {
            $table->dropColumn('proxy_id');
        });
    }
}
