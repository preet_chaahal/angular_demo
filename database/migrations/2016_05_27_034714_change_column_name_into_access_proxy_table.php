<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeColumnNameIntoAccessProxyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tbl_access_proxy', function (Blueprint $table) {
           $table->renameColumn('url','ip');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tbl_access_proxy', function (Blueprint $table) {
            $table->renameColumn('ip','url');
        });
    }
}
