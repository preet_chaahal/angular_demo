<?php

use App\RegistrationDisable;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DefaultSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        RegistrationDisable::where('id',1)->update(['disabled' => 0]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        RegistrationDisable::where('id',1)->update(['disabled' => 1]);
    }
}
