<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeColumnNameIntoAccessProxy extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tbl_access_proxy', function (Blueprint $table) {
            $table->dropColumn('access_time');
        });
        Schema::table('tbl_access_proxy', function (Blueprint $table) {
            $table->timestamp('access_time')->after('url');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tbl_access_proxy', function (Blueprint $table) {
            
            $table->decimal('access_time');
        });
    }
}
