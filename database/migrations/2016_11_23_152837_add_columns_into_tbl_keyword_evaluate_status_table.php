<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsIntoTblKeywordEvaluateStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tbl_evaluate_status', function (Blueprint $table) {
            $table->string('language');
            $table->string('project_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tbl_evaluate_status', function (Blueprint $table) {
            $table->dropColumn('language');
            $table->dropColumn('project_name');
        });
    }
}
