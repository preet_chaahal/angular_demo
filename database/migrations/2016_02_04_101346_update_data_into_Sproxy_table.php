<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateDataIntoSproxyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('serposcope.proxy', function (Blueprint $table) {
            $module=\App\SProxy::where('module',0)->update(['module'=>3]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('serposcope.proxy', function (Blueprint $table) {
            //
        });
    }
}
