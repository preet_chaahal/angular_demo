<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewColumnIntoProxy extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tbl_access_proxy', function (Blueprint $table) {
            $table->string('type');
            $table->string('port')->nullable();
            $table->string('user')->nullable();
            $table->string('password')->nullable();
            $table->integer('module')->comment = '1 = keyword Research, 2 = Brainstorm, 3 = Rank Tracker';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tbl_access_proxy', function (Blueprint $table) {
            $table->dropColumn('type');
            $table->dropColumn('port');
            $table->dropColumn('user');
            $table->dropColumn('password');
            $table->dropColumn('module');
        });
    }
}
