<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('has_tables',function (Blueprint $table){
           $table->increments('id');
           $table->unsignedInteger('user_id')->nullable();
           $table->foreign('user_id')->references('id')->on('users')->onDelete('CASCADE');
           $table->text('email')->nullable();
           $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('has_tables');
    }
}
