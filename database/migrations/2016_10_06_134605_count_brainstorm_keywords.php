<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CountBrainstormKeywords extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $brainstorms = DB::table('brainstorm')->get();

        foreach ($brainstorms as $brainstorm) {
            $data = unserialize(base64_decode($brainstorm->data));

            DB::table('brainstorm')->where('id', $brainstorm->id)->update([
                'keyword_count' => $this->countKeywordNumber($data)
            ]);
        }
    }

    public function countKeywordNumber($data)
    {
        $keywordNumber = 0;

        if (is_array($data)) {
            $presentNumber = count( $data['children'] );
        } elseif (is_object($data)) {
            $presentNumber = count( $data->children );
        }

        if ($presentNumber > 0) {
            $keywordNumber += $presentNumber;
            for ($i=0; $i < $presentNumber; $i++) {
                if (is_array($data)) {
                    $childData = $data['children'][$i];
                } elseif (is_object($data)) {
                    $childData = $data->children[$i];
                }
                $keywordNumber += $this->countKeywordNumber($childData);
            }
        }

        return $keywordNumber;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
