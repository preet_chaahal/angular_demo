<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCasperTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('casper', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('user_id');
            $table->bigInteger('group_id');
            $table->bigInteger('brainstorm_id');
            $table->string('upload_file');
            $table->string('download_file');
            $table->boolean('is_uploaded')->default(false);
            $table->boolean('is_downloaded')->default(false);
            $table->date('uploaded_date');
            $table->date('downloaded_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('casper');
    }
}
