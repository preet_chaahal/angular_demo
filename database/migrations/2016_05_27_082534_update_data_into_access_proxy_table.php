<?php

use App\AccessProxy;
use App\AccessProxyLog;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateDataIntoAccessProxyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $proxies=AccessProxy::all();
        foreach ($proxies as $proxy){
            AccessProxyLog::whereIp($proxy->ip)->update(['proxy_id'=>$proxy->id]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tbl_access_proxy_logs', function (Blueprint $table) {
            //
        });
    }
}
