<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewColumnIntoSerposcopeProxyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('serposcope.proxy', function (Blueprint $table) {
            $table->integer('module')->comment = '1 = keyword Research, 2 = Brainstorm, 3 = Rank Tracker';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('serposcope.proxy', function (Blueprint $table) {
            $table->dropColumn('module');

        });
    }
}
