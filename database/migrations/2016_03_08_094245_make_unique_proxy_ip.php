<?php

use App\AccessProxy;
use App\AccessProxyLog;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class MakeUniqueProxyIp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('serposcope.proxy', 'serposcope.proxy_old');
        Schema::create('serposcope.proxy', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type');
            $table->string('ip')->unique();
            $table->string('port')->nullable();
            $table->string('user')->nullable();
            $table->string('password')->nullable();
            $table->integer('module')->comment = '1 = keyword Research, 2 = Brainstorm, 3 = Rank Tracker';
        });

        $proxies = DB::table('serposcope.proxy_old')->get();

        foreach ($proxies as $proxy) {

            $alreadyExistProxies = DB::table('serposcope.proxy')->where('ip', $proxy->ip)->first();
            if (empty($alreadyExistProxies)) {
                DB::table('serposcope.proxy')->insert([
                    "id" => $proxy->id,
                    'type' => $proxy->type,
                    'ip' => $proxy->ip,
                    'port' => $proxy->port,
                    'user' => $proxy->user,
                    'password' => $proxy->password,
                    'module' => $proxy->module
                ]);
            } else {
                AccessProxy::where('url', $proxy->ip)->delete();
                AccessProxyLog::where('ip', $proxy->ip)->delete();
            }
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('serposcope.proxy');
        Schema::rename('serposcope.proxy_old', 'serposcope.proxy');
    }
}
