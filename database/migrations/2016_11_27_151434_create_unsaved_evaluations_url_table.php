<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnsavedEvaluationsUrlTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('unsaved_evaluations_url', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('keyword_unsaved_research_id')->unsigned()->nullable();
            $table->string('top_ten_url');
            $table->integer('page_authority');
            $table->integer('domain_authority');
            $table->integer('back_links');
            $table->integer('moz_rank');
            $table->integer('page_rank');
            $table->integer('google_plus_1s');
            $table->integer('fb_share');
            $table->integer('fb_like');
            $table->integer('tweets');
            $table->integer('pinterest_share');
            $table->tinyInteger('url');
            $table->tinyInteger('title');
            $table->tinyInteger('desc');
            $table->tinyInteger('h1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('unsaved_evaluations_url');
    }
}
