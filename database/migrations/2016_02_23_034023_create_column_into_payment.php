<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateColumnIntoPayment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payment', function (Blueprint $table) {
            $table->string('token')->after('transaction_id')->nullable();
            $table->string('profile_id')->after('token')->nullable();
            $table->string('payment_data')->after('profile_id')->nullable();
            $table->string('payment_status')->after('payment_data')->nullable();
            $table->string('remain_usage')->after('payment_status')->nullable();

        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payment', function (Blueprint $table) {
            $table->dropColumn('token');
            $table->dropColumn('profile_id');
            $table->dropColumn('payment_data');
            $table->dropColumn('payment_status');
            $table->dropColumn('remain_usage');
        });
    }
}
