<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserSubscriptionCancellations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('user_subscription_cancellations', function (Blueprint $table) {
          $table->increments('id');
          if (Schema::hasTable('users') && Schema::hasColumn('users','id')) {
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
          }
          $table->string('plan_id');
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('user_subscription_cancellations');
    }
}
