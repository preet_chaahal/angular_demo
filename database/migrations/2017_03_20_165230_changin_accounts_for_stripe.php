<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChanginAccountsForStripe extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $data = [
            'basic' => 'Keyword Revealer Basic - Monthly',
            'basic_yearly' => 'Keyword Revealer Basic - Yearly',
            'pro' => 'Keyword Revealer Pro - Monthly',
            'proyearly' => 'Keyword Revealer Pro - Yearly',
            'elite' => 'Keyword Revealer Elite - Monthly',
            'elite_yearly' => 'Keyword Revealer Elite - Yearly',
            'agency' => 'Keyword Revealer Agency - Monthly',
            'agency_yearly' => 'Keyword Revealer Agency - Yearly',
        ];

        foreach ($data as $name => $stripePlan) {
            \App\Account::where(['name' => $name])->update([ 'stripe_plan' => $stripePlan ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
