<?php

use App\Permission;
use App\Role;
use Illuminate\Database\Migrations\Migration;

class CreateAdminRole extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $adminRole               = new Role();
        $adminRole->name         = 'admin';
        $adminRole->display_name = 'Admin User'; // optional
        $adminRole->description  = 'Admin User'; // optional
        $adminRole->save();

        $permission     = Permission::all();
        $permissionsArr = [];
        foreach ($permission as $per) {
            $permissionsArr[] = $per;
        }
        $adminRole->attachPermissions($permissionsArr);

        $users = \App\User::where('is_admin', 1)->get();
        foreach($users as $user)
        {
            $user->roles()->sync([$adminRole->id]);
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Role::where('name', 'admin')->delete();
    }
}
