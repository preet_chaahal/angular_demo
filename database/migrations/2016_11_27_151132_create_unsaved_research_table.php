<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnsavedResearchTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('unsaved_research', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('user_id')->nullable()->unsigned();
            $table->string('keyword');
            $table->string('project_name');
            $table->integer('monthly_searches');
            $table->decimal('keyword_cpc', 12, 2);
            $table->decimal('estimated_profile', 12, 2);
            $table->integer('word_count');
            $table->string('location');
            $table->string('language');
            $table->string('google_engine');
            $table->integer('difficulty');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('unsaved_research');
    }
}
