<?php

use App\Account;
use Illuminate\Database\Migrations\Migration;

class PopulateDataIntoAccountsTable extends Migration
{
    private $accounts = [
        [
            'name' => 'elite',
            'plan' => 'Keyword Revealer Monthly Elite',
        ], [
            'name' => 'agency',
            'plan' => 'Keyword Revealer Monthly Agency',
        ], [
            'name' => 'basic_yearly',
            'plan' => 'Keyword Revealer Yearly Basic',
        ], [
            'name' => 'elite_yearly',
            'plan' => 'Keyword Revealer Yearly Elite',
        ], [
            'name' => 'agency_yearly',
            'plan' => 'Keyword Revealer Yearly Agency',
        ]
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach ($this->accounts as $account) {
            Account::create([
                'name' => $account['name'],
                'stripe_plan' => $account['plan'],
                'paypal_plan' => $account['plan'],
                'description' => $account['plan'],
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach ($this->accounts as $account) {
            Account::where('name', $account['name'])->delete();
        }
    }
}
