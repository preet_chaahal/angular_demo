<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProjectColumnIntoRoleLimitTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('role_limit', function (Blueprint $table) {
            $table->integer('project')->after('can_save')->unsigned()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('role_limit', function (Blueprint $table) {
            $table->dropColumn('project');
        });
    }
}
