<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertRoleLimit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('role_limit', function (Blueprint $table){
                $role_id1 = ['role_id'=>'1','research' => '3','evolution' => '10','brainstorm' => '0','tracking'=>'0','can_save'=>'0'];
                $role_id2 = ['role_id'=>'2','research' => '25','evolution' => '300','brainstorm' => '0','tracking'=>'0','can_save'=>'0'];
                $role_id3 = ['role_id'=>'3','research' => '1000','evolution' => '500000','brainstorm' => '500000','tracking'=>'0','can_save'=>'0'];
                $db = DB::table('role_limit')->insert($role_id1);
                $db = DB::table('role_limit')->insert($role_id2);
                $db = DB::table('role_limit')->insert($role_id3);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('role_limit', function (Blueprint $table) {
            //
        });
    }
}
