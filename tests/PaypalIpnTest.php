<?php

use App\User;
use App\Account;
use App\Payment;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\Http\Controllers\Auth\SubscriptionController;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PaypalIpnTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * A basic functional test example.
     *
     * @return void
     */
    public function test_change_to_free()
    {
        $testUser = $this->getTestUser();

        $input = [
            'txn_type' => 'recurring_payment_profile_cancel',
            'profile_status' => 'Anything except Active',
            'recurring_payment_id' => $testUser->stripe_subscription,
        ];

        $this->post('/subscription-paypal-success', $input)
            ->seeStatusCode(200);

        $this->seeInDatabase('users', [
            'id' => $testUser->id,
            'account_id' => 1,
            'stripe_active' => false,
            'toggle_free' => false,
        ]);

        $this->assertTrue(
            $this->getTestUser()->hasRole('free')
        );
    }

    public function test_handle_without_txn_id()
    {
        $testUser = $this->getTestUser();

        $plans = Account::all();
        $now = Carbon::now();

        foreach ($plans as $plan) {
            /** @var Account $plan */

            $input = [
                'txn_type' => 'recurring_payment_profile_created',
                'recurring_payment_id' => $testUser->stripe_subscription,
                'product_name' => $plan->paypal_plan
            ];

            $this->post('/subscription-paypal-success', $input)
                ->seeStatusCode(200);

            $this->seeInDatabase('users', [
                'id' => $testUser->id,
                'account_id' => $plan->id,
                'toggle_free' => false,
                'stripe_active' => true,
                'stripe_plan' => $plan->paypal_plan,
            ]);

            $diffInDays = $this->getTestUser()
                ->subscription_ends_at
                ->diffInDays( $now );

            if ($plan->getPeriod() === Account::PERIOD_MONTH) {
                $this->assertTrue($diffInDays > 27);
                $this->assertTrue($diffInDays <= 31);
            } else if ($plan->getPeriod() === Account::PERIOD_YEAR) {
                $this->assertTrue($diffInDays >= 364);
                $this->assertTrue($diffInDays <= 365);
            } else {
                throw new ErrorException('Wrong Plan period');
            }

            $this->assertTrue(
                $this->getTestUser()->hasRole( $plan->getRole()->name )
            );
        }
    }

    public function test_handle_with_txn_id_valid_case()
    {
        $this->mockMethod('curlCall', 'VERIFIED');

        $plans = Account::all();
        $testUser = $this->getTestUser();
        $payment = Payment::where('customer_id', $testUser->id)
            ->orderBy('id', 'DESC')
            ->first();
        $now = Carbon::now();

        foreach ($plans as $plan) {
            /** @var Account $plan */
            $input = [
                'payer_id' => $testUser->stripe_id,
                'txn_id' => str_random(10),
                'product_name' => $plan->paypal_plan,
                'receiver_email' => str_random(10),
                'payment_date' => Carbon::now()->format('H:m:i M d, Y PST'),
                'recurring_payment_id' => $payment->profile_id,
                'payment_status' => 'Completed',
                'transaction_subject' => $plan->paypal_plan
            ];

            $this->post('/subscription-paypal-success', $input)
                ->seeStatusCode(200);

            $this->seeInDatabase('users', [
                'id' => $testUser->id,
                'account_id' => $plan->id,
                'toggle_free' => false,
                'stripe_active' => true,
                'payment_type' => Payment::PAYMENT_TYPE_PAYPAL,
                'stripe_plan' => $plan->paypal_plan,
            ]);

            $diffInDays = $this->getTestUser()
                ->subscription_ends_at
                ->diffInDays( $now );

            if ($plan->getPeriod() === Account::PERIOD_MONTH) {
                $this->assertTrue($diffInDays > 27);
                $this->assertTrue($diffInDays <= 31);
            } else if ($plan->getPeriod() === Account::PERIOD_YEAR) {
                $this->assertTrue($diffInDays >= 364);
                $this->assertTrue($diffInDays <= 365);
            } else {
                throw new ErrorException('Wrong Plan period');
            }

            $this->assertTrue(
                $this->getTestUser()->hasRole( $plan->getRole()->name )
            );
        }
    }

    public function test_handle_with_txn_id_invalid_case()
    {
        foreach (['INVALID', 'SOMETHING_ELSE'] as $returnStatus) {
            $this->mockMethod('curlCall', $returnStatus);

            $input = [
                'txn_id' => str_random(10)
            ];

            $this->post('/subscription-paypal-success', $input)
                ->seeStatusCode(406);
        }
    }

    /**
     * @return User
     */
    private function getTestUser()
    {
        return User::where('username', 'palnstest@test.com')->firstOrFail();
    }

    private function mockMethod($method = 'curlCall', $returnStatus)
    {
        $mock = Mockery::mock(SubscriptionController::class)->makePartial();
        $mock->shouldReceive($method)
            ->andReturn($returnStatus);

        $this->app->instance(SubscriptionController::class, $mock);

        return $mock;
    }
}
