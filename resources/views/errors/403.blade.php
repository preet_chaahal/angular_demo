@extends('layouts.backend.master')

@section('content')
    <div style="text-align: center;vertical-align: middle">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="error-template">
                        <h1>
                            Oops!</h1>
                        <h2>
                            403 Access Denied</h2>
                        <div class="error-details">
                            Sorry, an error has occurred, You have not access right!
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
