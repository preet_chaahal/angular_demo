@extends('layouts.backend.master')

@section('style')
@stop


@section('content')
    <div class="page page-dashboard">
        <div class="pageheader">
            <h2>Disable IP</h2>
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <a href="{{ URL::route('admin.dashboard') }}"><i class="fa fa-home"></i> Admin Dashboard</a>
                    </li>
                    <li>
                        <a href="{{ URL::route('admin.registration.management') }}">Registration Management</a>
                    </li>
                </ul>
            </div>
        </div>

        @include('layouts.backend.partials.messages')

                <!-- row -->
        <div class="row">
            <!-- col -->
            <div class="col-md-12">
                <!-- tile -->
                <section class="tile tile-simple">
                    <!-- tile body -->
                    <div class="tile-body p-0">
                        <div role="tabpanel">
                            <ul class="nav nav-tabs tabs-dark tabs-right" role="tablist">
                                <li class="tabs-title pull-left">Registration<strong> Management</strong></li>
                                <li class="{{ Request::is('admin/registration-management/toggle-registration') ? 'active' : '' }}"><a href="{{ URL::route('admin.registration.management.toggle') }}">Toggle Registration</a></li>
                                <li class="{{ Request::is('admin/registration-management/disable/ip') ? 'active' : '' }}"><a href="{{ URL::route('admin.registration.management.disable.ip') }}">Disable IP</a></li>
                                <li class="{{ Request::is('admin/registration-management/disable/email') ? 'active' : '' }}"><a href="{{ URL::route('admin.registration.management.disable.email') }}">Disable Email</a></li>
                            </ul>


                            <!-- Tab panes -->
                            <div class="tile-body">

                                <form method="POST" action="{{ URL::route('admin.registration.management.disable.ip.post') }}">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                    <!-- ip -->
                                    <div class="form-group">
                                        <label for="ip">IP</label>
                                        <input type="text" class="form-control" id="ip" name="ip">
                                    </div>
                                    <!-- ip -->

                                    <!-- actions -->
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-8 col-md-offset-2">
                                                <input type="submit" class="btn btn-success btn-lg btn-block" value="Ban IP Address">
                                            </div>
                                        </div>
                                    </div> <!-- actions -->

                                </form>
                            </div>
                            <!-- /Tab panes -->
                        </div>
                    </div>
                    <!-- /tile body -->
                </section>
                <!-- / tile -->
            </div>
            <!-- / col -->
        </div>
        <!-- / row -->

    </div>

@stop


@section('scripts')
@stop