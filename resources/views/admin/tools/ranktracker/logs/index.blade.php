@extends('layouts.backend.master')

@section('style')
@stop


@section('content')
<div class="page page-dashboard">
        <div class="pageheader">
            <h2>Rank Tracker Admin <span>// Manage saved domains set by users</span></h2>
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <a href="{{ URL::route('admin.dashboard') }}"><i class="fa fa-home"></i> Admin Dashboard</a>
                    </li>
                    <li>
                        <a href="{{ URL::route('admin.tools.ranktracker.index') }}">Rank Tracker Administration</a>
                    </li>
                </ul>
            </div>
        </div>

        @include('layouts.backend.partials.messages')

        <!-- row -->
        <div class="row">
            <!-- col -->
            <div class="col-md-12">
                <!-- tile -->
                <section class="tile tile-simple">
                    <!-- tile body -->
                    <div class="tile-body p-0">
                        <div role="tabpanel">
                            <!-- Nav tabs -->
                            @include('admin.tools.ranktracker.partials.adminsubnav')

                            <!-- Tab panes -->
                            <div class="tile-body">
                                @include('admin.tools.ranktracker.partials.settingsmenu')
                                <table class="table">
                                <thead>
                                    <tr>
                                        <th>
                                            #
                                        </th>
                                        <th>
                                            Start
                                        </th>
                                        <th>
                                            Stop
                                        </th>
                                        <th>
                                            Error
                                        </th>
                                        <th>
                                            Logs
                                        </th>
                                        <th>
                                            Delete
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($logitems as $logitem)
                                        @if ($logitem->logs != null)
                                        <tr>
                                            <td>
                                                {{ $logitem->idRun }}
                                            </td>
                                            <td>
                                                {{ $from_time = date('m-d-Y h:i A',strtotime($logitem->dateStart)) }}
                                            </td>
                                            <td>
                                                {{ $to_time = !empty($logitem->dateStop)?date('m-d-Y h:i A',strtotime($logitem->dateStop)):null }}
                                            </td>
                                            <td>
                                                {{ $logitem->haveError }}
                                            </td>
                                            <td>
                                                <a href="{{ URL::route('admin.tools.ranktracker.viewlogs', $logitem->idRun) }}">View</a>
                                            </td>
                                            <td>
                                                <a href="{{ URL::route('admin.tools.ranktracker.logs.delete', $logitem->idRun) }}" onclick="return confirm('Are you sure you wish to delete this group?');">Delete</a>
                                            </td>
                                        </tr>
                                        @endif
                                    @endforeach
                                </tbody>
                            </table>
                            {!! $logitems->render() !!}
                            </div>
                            <!-- /Tab panes -->
                        </div>
                    </div>
                    <!-- /tile body -->
                </section>
                <!-- / tile -->
            </div>
            <!-- / col -->
        </div>
        <!-- / row -->

    </div>

@stop


@section('scripts')
@stop