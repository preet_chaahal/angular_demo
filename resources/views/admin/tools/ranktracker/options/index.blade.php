@extends('layouts.backend.master')

@section('style')
@stop


@section('content')
<div class="page page-dashboard">
        <div class="pageheader">
            <h2>Rank Tracker Admin <span>// Manage saved domains set by users</span></h2>
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <a href="{{ URL::route('admin.dashboard') }}"><i class="fa fa-home"></i> Admin Dashboard</a>
                    </li>
                    <li>
                        <a href="{{ URL::route('admin.tools.ranktracker.index') }}">Rank Tracker Administration</a>
                    </li>
                </ul>
            </div>
        </div>

        @include('layouts.backend.partials.messages')

        <!-- row -->
        <div class="row">
            <!-- col -->
            <div class="col-md-12">
                <!-- tile -->
                <section class="tile tile-simple">
                    <!-- tile body -->
                    <div class="tile-body p-0">
                        <div role="tabpanel">
                            <!-- Nav tabs -->
                            @include('admin.tools.ranktracker.partials.adminsubnav')

                            <!-- Tab panes -->
                            <div class="tile-body">
                                @include('admin.tools.ranktracker.partials.settingsmenu')
                                <form method="POST" action="{{ URL::route('admin.tools.ranktracker.options.update') }}">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                    <!-- timeout -->
                                    <div class="form-group">
                                        <label for="general_timeout">Timeout</label>
                                        @if ($timeout == "")
                                        <input type="text" class="form-control" id="general_timeout" name="general_timeout" value="{{ old('general_timeout')}}">
                                        @else
                                        <input type="text" class="form-control" id="general_timeout" name="general_timeout" value="{{ old('general_timeout', $timeout->value) }}">
                                        @endif
                                        <p class="help-block">Maximum HTTP request execution time</p>
                                    </div>
                                    <!-- timeout -->

                                    <!-- fetch retry -->
                                    <div class="form-group">
                                        <label for="general_fetch_retry">Fetch Retry</label>
                                        @if ($retry == "")
                                        <input type="text" class="form-control" id="general_fetch_retry" name="general_fetch_retry" value="{{ old('general_fetch_retry') }}">
                                        @else
                                        <input type="text" class="form-control" id="general_fetch_retry" name="general_fetch_retry" value="{{ old('general_fetch_retry', $retry->value) }}">
                                        @endif
                                        <p class="help-block">Maximum GET retry on HTTP error / captcha</p>
                                    </div>
                                    <!-- fetch retry -->

                                    <!-- bad proxies -->
                                    <div class="form-group">
                                        <label for="general_rm_bad_proxies">Remove Bad Proxies</label>
                                        @if ($badproxies == "")
                                        <input type="text" class="form-control" id="general_rm_bad_proxies" name="general_rm_bad_proxies" value="{{ old('general_rm_bad_proxies') }}">
                                        @else
                                        <input type="text" class="form-control" id="general_rm_bad_proxies" name="general_rm_bad_proxies" value="{{ old('general_rm_bad_proxies', $badproxies->value) }}">
                                        @endif
                                        <p class="help-block">Remove bad proxies after X fails, 0 to never remove bad proxy </p>
                                    </div>
                                    <!-- bad proxies -->

                                    <!-- proxy auto rotate -->
                                    <div class="form-group">
                                        <label for="general_proxy_auto_rotate">Proxy Auto Rotate</label>
                                        @if ($proxy_auto == "")
                                            <input type="radio" name="general_proxy_auto_rotate" value="yes" checked>Yes
                                            <input type="radio" name="general_proxy_auto_rotate" value="no">No
                                        @else
                                            <input type="radio" name="general_proxy_auto_rotate" value="yes">Yes
                                            <input type="radio" name="general_proxy_auto_rotate" value="no" checked>No
                                        @endif                                        
                                        <p class="help-block">Rotate the proxy on new keyword.</p>
                                    </div>
                                    <!-- bad proxies -->

                                    <!-- general rendering -->
                                    <div class="form-group">
                                        <label for="general_rendering">Rendering</label>
                                        @if ($gen_rend == "")
                                        <input type="text" class="form-control" id="general_rendering" name="general_rendering" value="{{ old('general_rendering') }}">
                                        @else
                                        <input type="text" class="form-control" id="general_rendering" name="general_rendering" value="{{ old('general_rendering', $gen_rend->value) }}">
                                        @endif                                        
                                        <p class="help-block">Possible values : highcharts,table</p>
                                    </div>
                                    <!-- general rendering -->

                                    <!-- dbc user -->
                                    <div class="form-group">
                                        <label for="general_dbc_user">DBC User</label>
                                        @if ($dbc_user == "")
                                        <input type="text" class="form-control" id="general_dbc_user" name="general_dbc_user" value="{{ old('general_dbc_user') }}">
                                        @else
                                        <input type="text" class="form-control" id="general_dbc_user" name="general_dbc_user" value="{{ old('general_dbc_user', $dbc_user->value) }}">
                                        @endif                                        
                                        <p class="help-block">DeathByCaptcha username</p>
                                    </div>
                                    <!-- dbc user -->

                                    <!-- dbc pass -->
                                    <div class="form-group">
                                        <label for="general_dbc_pass">DBC Pass</label>
                                        @if ($dbc_pass == "")
                                        <input type="text" class="form-control" id="general_dbc_pass" name="general_dbc_pass" value="{{ old('general_dbc_pass') }}">
                                        @else
                                        <input type="text" class="form-control" id="general_dbc_pass" name="general_dbc_pass" value="{{ old('general_dbc_pass', $dbc_pass->value) }}">
                                        @endif                                        
                                        <p class="help-block">DeathByCaptcha password</p>
                                    </div>
                                    <!-- dbc pass -->

                                    <h3>Google</h3>

                                    <!-- page sleep -->
                                    <div class="form-group">
                                        <label for="Google_page_sleep">Page Sleep</label>
                                        @if ($sleep == "")
                                        <input type="text" class="form-control" id="Google_page_sleep" name="Google_page_sleep" value="{{ old('Google_page_sleep') }}">
                                        @else
                                        <input type="text" class="form-control" id="Google_page_sleep" name="Google_page_sleep" value="{{ old('Google_page_sleep', $sleep->value) }}">
                                        @endif                                        
                                        <p class="help-block">Pause in seconds between request to google.</p>
                                    </div>
                                    <!-- page sleep -->

                                    <!-- captcha base sleep -->
                                    <div class="form-group">
                                        <label for="Google_captcha_basesleep">Captcha Base Sleep</label>
                                        @if ($captcha == "")
                                        <input type="text" class="form-control" id="Google_captcha_basesleep" name="Google_captcha_basesleep" value="{{ old('Google_captcha_basesleep') }}">
                                        @else
                                        <input type="text" class="form-control" id="Google_captcha_basesleep" name="Google_captcha_basesleep" value="{{ old('Google_captcha_basesleep', $captcha->value) }}">
                                        @endif                                        
                                        <p class="help-block">Pause in seconds if captcha.</p>
                                    </div>
                                    <!-- captcha base sleep -->

                                    <!-- tld -->
                                    <div class="form-group">
                                        <label for="Google_tld">TLD</label>
                                        @if ($tld == "")
                                        <input type="text" class="form-control" id="Google_tld" name="Google_tld" value="{{ old('Google_tld') }}">
                                        @else
                                        <input type="text" class="form-control" id="Google_tld" name="Google_tld" value="{{ old('Google_tld', $tld->value) }}">
                                        @endif                                        
                                        <p class="help-block">The google search engine top level domain: google.com, google.co.uk.</p>
                                    </div>
                                    <!-- tld -->

                                    <!-- actions -->
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-8 col-md-offset-2">
                                                <input type="submit" class="btn btn-success btn-lg btn-block" value="Update Options">
                                            </div>
                                        </div>
                                    </div> <!-- actions -->

                                </form>
                            </div>
                            <!-- /Tab panes -->
                        </div>
                    </div>
                    <!-- /tile body -->
                </section>
                <!-- / tile -->
            </div>
            <!-- / col -->
        </div>
        <!-- / row -->

    </div>

@stop


@section('scripts')
@stop