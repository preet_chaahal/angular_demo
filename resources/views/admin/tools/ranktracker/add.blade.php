@extends('layouts.backend.master')

@section('style')
    <link rel="stylesheet" href="{{ asset('assets/backend/css/vendor/selectize.default.css') }}">

    <style>
        .selectItem >span { color: #000; }
    </style>
@stop


@section('content')

    <div class="page page-dashboard">
        <div class="pageheader">
            <h2>Rank Tracker Admin <span>// Save time and money by automating the process of checking your keyword rankings</span></h2>
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <a href="{{ URL::route('admin.dashboard') }}"><i class="fa fa-home"></i> Admin Dashboard</a>
                    </li>
                    <li>
                        <a href="{{ URL::route('admin.tools.ranktracker.index') }}">Rank Tracker Admin</a>
                    </li>
                </ul>
            </div>
        </div>

        @include('layouts.backend.partials.messages')

        <!-- row -->
        <div class="row">
            <!-- col -->
            <div class="col-md-12">
                <!-- tile -->
                <section class="tile tile-simple">
                    <!-- tile body -->
                    <div class="tile-body p-0">
                        <div role="tabpanel">
                            <!-- Nav tabs -->
                            @include('admin.tools.ranktracker.partials.adminsubnav')

                            <!-- Tab panes -->
                            <div class="tile-body">

                                <form method="POST" action="{{ URL::route('admin.tools.ranktracker.insert') }}">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                    <!-- user -->
                                    <div class="form-group">
                                        <label for="useraccount">User</label>
                                        <select id="useraccount" name="useraccount" class="form-control">
                                                <option>Select</option>
                                            @foreach ($allUsers as $selectUser)
                                                <option>{{$selectUser->username}}</option>
                                            @endforeach
                                        </select>                                        
                                    </div>

                                    <!-- name -->
                                    <div class="form-group">
                                        <label for="name">Name</label>
                                        <input type="text" class="form-control" id="name" name="name">
                                    </div> <!-- name -->

                                    <!-- keywords -->
                                    <div class="form-group">
                                        <label for="keywords">Keywords</label>
                                        <textarea name="keywords" id="keywords" rows="10" class="form-control"></textarea>
                                        <p class="help-block">One keyword per line</p>
                                    </div> <!-- keywords -->

                                    <!-- domain -->
                                    <div class="form-group">
                                        <label for="domain">Domain</label>
                                        <input type="text" class="form-control" id="domain" name="domain">
                                        <p class="help-block">*.domain.com or www.domain.com</p>
                                    </div>
                                    <!-- domain -->


                                    <!-- module -->
                                    <div class="form-group">
                                        <label for="module">Module</label>
                                        <select class="form-control" name="module" id="module">
                                            <option value="google">Google</option>
                                        </select>
                                    </div> <!-- module -->

                                    <!-- tld -->
                                    <div class="form-group">
                                        <label for="tld">tld</label>
                                        <select id="selectize" name="tld"></select>
                                        <p class="help-block">The google search engine top level domain: google.com, google.co.uk</p>
                                    </div> <!-- tld -->

                                    {{--<!-- datacenter -->--}}
                                    {{--<div class="form-group">--}}
                                        {{--<label for="datacenter">Data Center</label>--}}
                                        {{--<input type="text" class="form-control" id="datacenter" name="datacenter">--}}
                                        {{--<p class="help-block">A specific datacenter. Leave empty to use standard google.tld</p>--}}
                                    {{--</div> <!-- datacenter -->--}}

                                    {{--<!-- parameters -->--}}
                                    {{--<div class="form-group">--}}
                                        {{--<label for="parameters">Parameters</label>--}}
                                        {{--<input type="text" class="form-control" id="parameters" name="parameters">--}}
                                        {{--<p class="help-block">Additional parameters in the request (like hl=fr&tbs=qdr:d)</p>--}}
                                    {{--</div> <!-- parameters -->--}}

                                    {{--<!-- local -->--}}
                                    {{--<div class="form-group">--}}
                                        {{--<label for="local">Local</label>--}}
                                        {{--<input type="text" class="form-control" id="local" name="local">--}}
                                        {{--<p class="help-block">City or place for local search, should be in the country of the tld</p>--}}
                                    {{--</div> <!-- local -->--}}

                                    <!-- actions -->
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-8 col-md-offset-2">
                                                <input type="submit" class="btn btn-success btn-lg btn-block" value="Add New Search">
                                            </div>
                                        </div>
                                    </div> <!-- actions -->

                                </form>

                            </div>
                            <!-- /Tab panes -->
                        </div>
                    </div>
                    <!-- /tile body -->
                </section>
                <!-- / tile -->
            </div>
            <!-- / col -->
        </div>
        <!-- / row -->

    </div>
@stop


@section('scripts')
    <script src="{{ asset('assets/backend/js/vendor/selectize/selectize.js') }}"></script>

    <script>
        var options = {!! $tldOptions !!};

        $('#selectize').selectize({
            plugins: ['remove_button'],
            valueField: 'engine',
            labelField: 'name',
            searchField: ['name', 'engine'],
            delimiter: ',',
            persist: false,
            hideSelected: true,
            openOnFocus: true,
            items: ['.com'],
            render: {
                item: function (item, escape) {
                    var name = escape(item.name).match(/\(.+\)/g);
                    name = name !== null ? name[0].substring(1, name[0].length - 1) : '';
                    return '<div>' +
                            '<span>' + name + '</span>' +
                            (item.engine ? ' <span> ‒ ' + escape(item.engine) + '</span>' : '') +
                            '</div>';
                },
                option: function (item, escape) {
                    var name = escape(item.name).match(/\(.+\)/g);
                    name = name !== null ? name[0].substring(1, name[0].length - 1) : '';
                    return '<div class="selectItem">' +
                            '<span>' + name + '</span>' +
                            (item.engine ? ' <span> ‒ ' + escape(item.engine) + '</span>' : '') +
                            '</div>';
                }
            },
            options: options
        });
    </script>
@stop