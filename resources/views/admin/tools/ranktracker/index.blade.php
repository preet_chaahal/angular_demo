@extends('layouts.backend.master')

@section('style')
@stop


@section('content')
<div class="page page-dashboard">
        <div class="pageheader">
            <h2>Rank Tracker Admin <span>// Manage saved domains set by users</span></h2>
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <a href="{{ URL::route('admin.dashboard') }}"><i class="fa fa-home"></i> Admin Dashboard</a>
                    </li>
                    <li>
                        <a href="{{ URL::route('admin.tools.ranktracker.index') }}">Rank Tracker Administration</a>
                    </li>
                </ul>
            </div>
        </div>

        @include('layouts.backend.partials.messages')

        <!-- row -->
        <div class="row">
            <!-- col -->
            <div class="col-md-12">
                <!-- tile -->
                <section class="tile tile-simple">
                    <!-- tile body -->
                    <div class="tile-body p-0">
                        <div role="tabpanel">
                            <!-- Nav tabs -->
                            @include('admin.tools.ranktracker.partials.adminsubnav')

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <table class="table">
                                <thead>
                                    <tr>
                                        <th>
                                            Group Name
                                        </th>
                                        <th>
                                            Username
                                        </th>
                                        <th>
                                            Domain
                                        </th>
                                        <th>
                                            Keywords
                                        </th>
                                        <th>
                                            Last Checked
                                        </th>
                                        <th>
                                            Controls
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if($domains->count() > 0)
                                        @foreach($domains as $domain)
                                            @if(isset($domain->group->idGroup))
                                            <tr>
                                                <td>{{$id=isset($domain->group->idGroup)?$domain->group->idGroup:''}}
                                                    <a href="{{ URL::route('admin.tools.ranktracker.view',$id)}}">
                                                        {{ isset($domain->group->name)?$domain->group->name:'' }}
                                                    </a>
                                                </td>
                                                <td>
                                                    {{  isset($domain->user->username)?$domain->user->username:'' }}
                                                </td>
                                                <td>
                                                    {{ isset($domain->group->target->name)?$domain->group->target->name:'' }}
                                                </td>
                                                <td>
                                                    <span class="label label-default">{{ isset($domain->group->keywords)?$domain->group->keywords->count():'' }} Keywords</span>
                                                </td>
                                                <td>
                                                    @if(isset($domain->group->lastRan) && $domain->group->lastRan->count() <= 0)
                                                        Never
                                                    @else
                                                        {{ isset($domain->group->lastRan)?$domain->group->lastRan->last()->date->format('F d, Y @ g:i A'):'' }}
                                                    @endif
                                                </td>
                                                <td>
                                                    <a href="{{ URL::route('admin.tools.ranktracker.edit', $id=isset($domain->group->idGroup)?$domain->group->idGroup:'') }}">Edit</a> | <a href="{{ URL::route('admin.tools.ranktracker.delete',$id=isset($domain->group->idGroup)?$domain->group->idGroup:'') }}" onclick="return confirm('Are you sure you wish to delete this group?');">Delete</a>
                                                </td>
                                            </tr>
                                            @endif
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                            </div>
                            <!-- /Tab panes -->
                        </div>
                    </div>
                    <!-- /tile body -->
                </section>
                <!-- / tile -->
            </div>
            <!-- / col -->
        </div>
        <!-- / row -->

    </div>

@stop


@section('scripts')
@stop