<ul class="nav nav-tabs tabs-dark" role="tablist">
    <li class="{{ Request::is('admin/tools/ranktracker') ? 'active' : '' }}"><a href="{{ URL::route('admin.tools.ranktracker.index') }}">Listing</a></li>
    <li class="{{ Request::is('admin/tools/ranktracker/add*') ? 'active' : '' }}"><a href="{{ URL::route('admin.tools.ranktracker.add') }}">New Domain</a></li>
    <li class="{{ Request::is('admin/tools/ranktracker/settings*') ? 'active' : '' }}"><a href="{{ URL::route('admin.tools.ranktracker.settings') }}"><i class="fa fa-cog"></i> Settings</a></li>
    <li class="tabs-title pull-right" style="padding-right: 15px;">Rank Tracker <strong>Settings</strong></li>
</ul>