<ul class="nav nav-tabs nav-justified tabs-dark">
    <li role="presentation" class="{{ Request::is('admin/tools/ranktracker/settings/options') ? 'active' : '' }}"><a href="{{ URL::route('admin.tools.ranktracker.options') }}">Options</a></li>
    <li role="presentation" class="{{ Request::is('admin/tools/ranktracker/settings/logs*') ? 'active' : '' }}"><a href="{{ URL::route('admin.tools.ranktracker.logs') }}">Logs</a></li>
</ul>
<br/>