@extends('layouts.backend.master')

@section('style')
@stop


@section('content')
<div class="page page-dashboard">
        <div class="pageheader">
            <h2>Rank Tracker <span>// Save time and money by automating the process of checking your keyword rankings</span></h2>
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <a href="{{ URL::route('dashboard') }}"><i class="fa fa-home"></i> Admin Dashboard</a>
                    </li>
                    <li>
                        <a href="{{ URL::route('admin.tools.ranktracker.index') }}">Rank Tracker Administration - Edit</a>
                    </li>
                </ul>
            </div>
        </div>

        @include('layouts.backend.partials.messages')

        <!-- row -->
        <div class="row">
            <!-- col -->
            <div class="col-md-12">
                <!-- tile -->
                <section class="tile tile-simple">
                    <!-- tile body -->
                    <div class="tile-body p-0">
                        <div role="tabpanel">
                            <!-- Nav tabs -->
                            @include('admin.tools.ranktracker.partials.adminsubnav')

                            <!-- Tab panes -->
                            <div class="tile-body">

                                <form method="POST" action="{{ URL::route('admin.tools.ranktracker.update') }}">
                                <input type="hidden" name="id" value="{{ $group->idGroup }}">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                <!-- name -->
                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <input type="text" class="form-control" id="name" name="name" value="{{ old('name', $group->name) }}">
                                </div> <!-- name -->

                                <!-- keywords -->
                                <div class="form-group">
                                    <label for="keywords">Keywords</label>
                                    <textarea name="keywords" id="keywords" rows="10" class="form-control">{{ old('keywords', $keywordString) }}</textarea>
                                    <p class="help-block">One keyword per line</p>
                                </div> <!-- keywords -->

                                <!-- domain -->
                                <div class="form-group">
                                    <label for="domain">Domain</label>
                                    <input type="text" class="form-control" id="domain" name="domain" value="{{ old('domain', $group->target->name) }}">
                                    <p class="help-block">*.domain.com or www.domain.com</p>
                                </div>
                                <!-- domain -->


                                <!-- module -->
                                <div class="form-group">
                                    <label for="module">Module</label>
                                    <select name="module" id="module">
                                        <option value="google">Google</option>
                                    </select>
                                </div> <!-- module -->

                                <!-- tld -->
                                <div class="form-group">
                                    <label for="tld">tld</label>
                                    <input type="text" class="form-control" id="tld" name="tld" value="{{ old('tld', (isset($group->options['tld']) ?$group->options['tld']: 'com')) }}">
                                    <p class="help-block">The google search engine top level domain: google.com, google.co.uk</p>
                                </div> <!-- tld -->

                                {{--<!-- datacenter -->--}}
                                {{--<div class="form-group">--}}
                                    {{--<label for="datacenter">Data Center</label>--}}
                                    {{--<input type="text" class="form-control" id="datacenter" name="datacenter" value="{{ old('datacenter', (isset($group->options['datacenter']) ?$group->options['datacenter']: '')) }}">--}}
                                    {{--<p class="help-block">A specific datacenter. Leave empty to use standard google.tld</p>--}}
                                {{--</div> <!-- datacenter -->--}}

                                {{--<!-- parameters -->--}}
                                {{--<div class="form-group">--}}
                                    {{--<label for="parameters">Parameters</label>--}}
                                    {{--<input type="text" class="form-control" id="parameters" name="parameters" value="{{ old('parameters', (isset($group->options['parameters']) ?$group->options['parameters']: '')) }}">--}}
                                    {{--<p class="help-block">Additional parameters in the request (like hl=fr&tbs=qdr:d)</p>--}}
                                {{--</div> <!-- parameters -->--}}

                                {{--<!-- local -->--}}
                                {{--<div class="form-group">--}}
                                    {{--<label for="local">Local</label>--}}
                                    {{--<input type="text" class="form-control" id="local" name="local" value="{{ old('local', (isset($group->options['local']) ?$group->options['local']: '')) }}">--}}
                                    {{--<p class="help-block">City or place for local search, should be in the country of the tld</p>--}}
                                {{--</div> <!-- local -->--}}

                                    <div class="row">
                                        <!-- actions -->
                                        <div class="col-md-6 col-md-offset-3">
                                            <input type="submit" class="btn btn-success btn-block" value="Update Group">
                                        </div> <!-- actions -->
                                    </div>

                                </form>

                            </div>
                            <!-- /Tab panes -->
                        </div>
                    </div>
                    <!-- /tile body -->
                </section>
                <!-- / tile -->
            </div>
            <!-- / col -->
        </div>
        <!-- / row -->

    </div>
    

@stop


@section('scripts')
@stop