@extends('layouts.backend.master')

@section('style')
    <style>
        /* basic positioning */
        .legend { list-style: none; margin: 0px; padding: 0px;}
        .legend li { margin-right: 10px; }
        .legend span { border: 1px solid #ccc; float: left; width: 12px; height: 12px; margin: 2px; }

        canvas
        {
            width: 100% !important;
            max-width: 945px;
            height: auto !important;
        }
    </style>
@stop


@section('content')

    <div class="page page-dashboard">
        <div class="pageheader">
            <h2>Rank Tracker <span>// Save time and money by automating the process of checking your keyword rankings</span></h2>
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <a href="{{ URL::route('dashboard') }}"><i class="fa fa-home"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href="{{ URL::route('backend.tools.ranktracker') }}">Rank Tracker</a>
                    </li>
                </ul>
            </div>
        </div>

        @include('layouts.backend.partials.messages')

        <div class="row">
            <div class="col-md-8">
                <section class="tile">
                    <!-- tile header -->
                    <div class="tile-header bg-slategray text-center">
                        <h4 class="custom-font text-uppercase">Viewing <strong>{{ $group->name }}</strong></h4>
                        <ul class="controls">
                            <li>
                                <a role="button" tabindex="0" class="pickDate2" id="datePicker">
                                    <span id="dateSpan">{{ date('M d, Y', strtotime($dates['start'])) }} - {{ date('M d, Y', strtotime($dates['stop'])) }}</span>&nbsp;&nbsp;<i class="fa fa-angle-down"></i>
                                    <div class="hidden">
                                        <form id="dateForm" action="{{ URL::route('backend.tools.ranktracker.setDate') }}" method="post">
                                            <input type="hidden" name="id" value="{{ $group->idGroup }}">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <input type="hidden" name="start_date" id="start_date" value="">
                                            <input type="hidden" name="stop_date" id="stop_date" value="">
                                        </form>
                                    </div>
                                </a>
                            </li>
                            <li class="dropdown">
                                <a role="button" tabindex="0" class="dropdown-toggle settings" data-toggle="dropdown">
                                    <i class="fa fa-cog"></i>
                                    <i class="fa fa-spinner fa-spin"></i>
                                </a>
                                <ul class="dropdown-menu pull-right with-arrow animated littleFadeInUp">
                                    <li>
                                        <a role="button" tabindex="0" class="tile-toggle">
                                            <span class="minimize"><i class="fa fa-angle-down"></i>&nbsp;&nbsp;&nbsp;Minimize</span>
                                            <span class="expand"><i class="fa fa-angle-up"></i>&nbsp;&nbsp;&nbsp;Expand</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ URL::route('backend.tools.ranktracker.edit', $group->idGroup) }}" tabindex="0" class="">
                                            <i class="fa fa-pencil"></i> Edit
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ URL::route('backend.tools.ranktracker.download', $group->idGroup) }}" tabindex="0" class="">
                                            <i class="fa fa-cloud-download"></i> Download Data
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ URL::route('backend.tools.ranktracker.delete', $group->idGroup) }}" tabindex="0" class="" onclick="return confirm('Are you sure you wish to delete this group?');">
                                            <i class="fa fa-trash-o"></i> Delete
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div> <!-- /tile header -->
                    <!-- tile body -->
                    <div class="tile-body">
                        <canvas id="chart" style="width: 100%; height: 500px;"></canvas>
                    </div> <!-- /tile body -->

                </section>
                <!-- search volume and cpc -->
                <!-- /search volume and cpc -->
            </div> <!-- col-md-8 -->
            <div class="col-md-4">

                <!-- Visibility Score -->

                <section class="tile">
                    <!-- tile header -->
                    <div class="tile-header bg-slategray text-center">
                        <h4 class="custom-font text-uppercase"><strong>Visibility Score</strong></h4>
                    </div> <!-- /tile header -->
                    <!-- tile body -->
                    <div class="tile-body">
                        <ul class="legend">
                            <li>
                                <span><b>Visibility Score:</b> {{$visibilityScore}}</span>
                            </li>
                        </ul>
                        <ul class="legend">
                            <li>
                                <span><b>Visibility Percent:</b> {{$visibilityPercentage}}%</span>
                            </li>
                        </ul>
                    </div> <!-- /tile body -->
                </section> <!--  Visibility Score -->

                <!-- Legend -->
                <section class="tile">
                    <!-- tile header -->
                    <div class="tile-header bg-slategray text-center">
                        <h4 class="custom-font text-uppercase"><strong>Legend</strong></h4>
                    </div> <!-- /tile header -->
                    <!-- tile body -->
                    <div class="tile-body">
                        @foreach($chartJs['legend'] as $legend)
                            <ul class="legend">
                                <li>
                                    <span style="background: {{ isset($legend['fillColor'])?$legend['fillColor']:'' }}; border: 1px solid {{ isset($legend['strokeColor'])?$legend['strokeColor']:'' }};"></span> {{ isset($legend['label'])?$legend['label']:''}}
                                </li>
                            </ul>
                        @endforeach
                    </div> <!-- /tile body -->
                </section> <!-- Legend -->

                <!-- Events -->
                <section class="tile">
                    <!-- tile header -->
                    <div class="tile-header bg-slategray text-center">
                        <h4 class="custom-font text-uppercase">
                            <strong>Events</strong>
                        </h4>
                        <ul class="controls">
                            <li>
                                <a href="#" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i> Add Event</a>
                            </li>
                        </ul>
                    </div> <!-- /tile header -->
                    <!-- tile body -->
                    <div class="tile-body">
                        @if($group->target->events->count() > 0)
                            <table class="table table-bordered table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th width="25%">
                                            Date
                                        </th>
                                        <th>
                                            Description
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($group->target->events->sortBy('date') as $event)
                                        <tr>
                                            <td>
                                                {{ date('M d, Y', strtotime($event->date)) }}
                                            </td>
                                            <td>
                                                {{ $event->event }}
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="alert alert-warning alert-dismissable">
                                <strong>No Events Found!</strong>
                            </div>
                        @endif
                    </div> <!-- /tile body -->
                </section> <!-- Events -->

                <section class="tile">
                    <!-- tile header -->
                    <div class="tile-header bg-slategray text-center">
                        <h4 class="custom-font text-uppercase"><strong>Keyword Position</strong></h4>
                    </div> <!-- /tile header -->
                    <!-- tile body -->
                    <div class="tile-body">
                        <table>
                            <tr>
                                <td>
                                    <div id="container"
                                         style="min-width: 210px; height: 145px; max-width: 250px; margin-top: -14px; margin-bottom: -10px">
                                    </div>
                                </td>
                                <td style="vertical-align: top">
                                    <table>
                                        <tr>
                                            <td>
                                                <span class="legend">Top 3: </span>
                                            </td>
                                            <td>
                                                <span class="legend"
                                                      style="color: #00dfdf;">{{$keywordPositionChartTableData[3]}}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="legend">Top 10: </span>
                                            </td>
                                            <td>
                                                <span class="legend"
                                                      style="color: #00dfdf;">{{$keywordPositionChartTableData[10]}}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="legend">Top 20: </span>
                                            </td>
                                            <td>
                                                <span class="legend"
                                                      style="color: #00dfdf;">{{$keywordPositionChartTableData[20]}}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="legend">Top 30: </span>
                                            </td>
                                            <td>
                                                <span class="legend"
                                                      style="color: #00dfdf;">{{$keywordPositionChartTableData[30]}}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="legend">Top 100: </span>
                                            </td>
                                            <td>
                                                <span class="legend"
                                                      style="color: #00dfdf;">{{$keywordPositionChartTableData[100]}}</span>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                        </table>

                    </div> <!-- /tile body -->
                </section>

            </div> <!-- col-md-4 -->

            <div class="col-md-12">
                <section class="tile">
                    <!-- tile header -->
                    <div class="tile-header bg-slategray text-center">
                        <h4 class="custom-font text-uppercase">Search Volume &amp; CPC</h4>
                        <ul class="controls">
                            <li class="dropdown">
                                <a role="button" tabindex="0" class="dropdown-toggle settings" data-toggle="dropdown">
                                    <i class="fa fa-cog"></i>
                                    <i class="fa fa-spinner fa-spin"></i>
                                </a>
                                <ul class="dropdown-menu pull-right with-arrow animated littleFadeInUp">
                                    <li>
                                        <a role="button" tabindex="0" class="tile-toggle">
                                            <span class="minimize"><i class="fa fa-angle-down"></i>&nbsp;&nbsp;&nbsp;Minimize</span>
                                            <span class="expand"><i class="fa fa-angle-up"></i>&nbsp;&nbsp;&nbsp;Expand</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ URL::route('backend.tools.ranktracker.casper.generator', $group->idGroup) }}" tabindex="0" class="" onclick="return canRefreshData();">
                                            <i class="fa fa-refresh"></i> Refresh
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div> <!-- /tile header -->
                    <!-- tile body -->
                    <div class="tile-body" id="cpc_results">
                        <div class="table-responsive">
                            @if($casperResults)
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>
                                            Keyword
                                        </th>
                                        <th class="text-center">
                                            Currency
                                        </th>
                                        <th class="text-center">
                                            Avg Monthly Searches
                                        </th>
                                        <th class="text-center">
                                            Competition
                                        </th>
                                        <th class="text-center">
                                            Suggested Bid
                                        </th>
                                        <th class="text-center">
                                            Old
                                        </th>
                                        <th class="text-center">
                                            Current
                                        </th>
                                        <th>
                                            +/-
                                        </th>
                                        <th>
                                            Best
                                        </th>
                                        <th class="text-center">
                                            Updated at
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($casperResults as $result)
                                        @if(isset($result->keyword) && !empty($result->keyword))
                                            <tr>
                                                <td>
                                                    {{ isset($result->keyword)?$result->keyword:'' }}
                                                </td>
                                                <td class="text-center">
                                                    {{ isset($result->currency)?$result->currency:'' }}
                                                </td>
                                                <td class="text-center">
                                                    {{ isset($result->avg_month_searches)?$result->avg_month_searches:'' }}
                                                </td>
                                                <td class="text-center">
                                                    {{ isset($result->competition)?$result->competition:'' }}
                                                </td>
                                                <td class="text-center">
                                                    {{ isset($result->suggested_bid)?$result->suggested_bid:'' }}
                                                </td>
                                                <td class="text-center">
                                                    {{isset($result->oldRank)?$result->oldRank:'N/A'}}
                                                </td>
                                                <td class="text-center">
                                                    {{isset($result->newRank)?$result->newRank:'N/A'}}
                                                </td>
                                                <td class="text-center">
                                                    {{isset($result->change)?$result->change:''}}
                                                </td>
                                                <td class="text-center">
                                                    {{isset($result->change)?$result->bestRank:''}}
                                                </td>
                                                <td class="text-center">
                                                    {{ isset($result->updated_at)?$result->updated_at:'' }}
                                                </td>
                                            </tr>
                                        @endif
                                    @endforeach
                                    </tbody>
                                </table>
                            @else
                                <div class="alert alert-warning">
                                    <strong>Sorry</strong>
                                    you will need to refresh the data by clicking here.
                                </div>
                            @endif
                        </div>
                    </div> <!-- /tile body -->
                </section>
            </div>
        </div>

    </div>
@stop

@section('modals')
    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form method="post" action="{{ URL::route('backend.tools.ranktracker.events.insert') }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="target_id" value="{{ $group->target->idTarget }}">

                    <div class="modal-header">
                        <h3 class="modal-title custom-font">
                            Add New Event
                        </h3>
                    </div>
                    <div class="modal-body">
                        <label for="dtp_eventDate">Event Date</label>
                        <div class='input-group date datepicker' id='dtp_eventDate' data-format="L">
                            <input type='text' class="form-control" name="date" />
                            <span class="input-group-addon">
                                <span class="fa fa-calendar"></span>
                            </span>
                        </div>

                        <div class="form-group">
                            <label for="description" style="display: block;">Description of Event</label>
                            <textarea name="description" cols="60" rows="10" class="form-control"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="submit" class="btn btn-success btn-ef btn-ef-3 btn-ef-3c" value="Save">
                        <button class="btn btn-lightred btn-ef btn-ef-4 btn-ef-4c" data-dismiss="modal"><i class="fa fa-arrow-left"></i> Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop


@section('scripts')

    <script src="{{ asset('assets/js/chart.min.js') }}"></script>
    <script src="{{ asset('assets/js/charts/LineAlt.js') }}"></script>

    <script>
        var data = {
            labels: {!! $chartJs['labels'] !!},
            datasets: {!! $chartJs['datasets'] !!}
        };

        $(function(){
            var ctx = $("#chart").get(0).getContext("2d");

            $('#datePicker').daterangepicker(
            {
                format: 'MM/DD/YYYY',
                startDate: {{ $dates['start'] }},
                endDate: {{ $dates['stop'] }},
                dateLimit: { days: 60 },
                showDropdowns: true,
                showWeekNumbers: true,
                timePicker: false,
                timePickerIncrement: 1,
                timePicker12Hour: true,
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
                opens: 'left',
                drops: 'down',
                buttonClasses: ['btn', 'btn-sm'],
                applyClass: 'btn-success',
                cancelClass: 'btn-default',
                separator: ' to ',
                locale: {
                applyLabel: 'Submit',
                cancelLabel: 'Cancel',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: 'Custom',
                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr','Sa'],
                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                firstDay: 1
                }
            }, function(start, end, label) {
                    $('#dateSpan').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                    $("#start_date").val(start.format('YYYY-MM-DD'));
                    $("#stop_date").val(end.format('YYYY-MM-DD'));
                    $("#dateForm").submit();
                }
            );

            $(function () {
                $('#container').highcharts({
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: 0,
                        plotShadow: false
                    },
                    title: {
                        text: '',
                        align: 'center',
                        verticalAlign: 'middle',
                        y: 40
                    },
                    colors: ['#00dfdf', '#ccffff', '#3399ff', '#9999ff', '#8cb3d9'],
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    credits: {
                        enabled: false
                    },
                    plotOptions: {
                        pie: {
                            dataLabels: {
                                enabled: true,
                                distance: -50,
                                style: {
                                    fontWeight: 'bold',
                                    color: 'white',
                                    textShadow: '0px 1px 2px black'
                                }
                            },
                            startAngle: 0,
                            endAngle: 360,

                        }
                    },
                    series: [{
                        type: 'pie',
                        name: 'Keyword Position',
                        innerSize: '50%',
                        data: [
                            {!!$keywordPositionChartData!!},
                            {
                                name: 'Proprietary or Undetectable',
                                y: 0.2,
                                dataLabels: {
                                    enabled: true
                                }
                            }
                        ]
                    }]
                });
            });

            var chartjs = new Chart(ctx).LineAlt(data, {
                responsive: false,
                maintainAspectRatio: false,
                showScale: true,
                ///Boolean - Whether grid lines are shown across the chart
                scaleShowGridLines : true,
                //String - Colour of the grid lines
                scaleGridLineColor : "rgba(0,0,0,0.05)",
                //Number - Width of the grid lines
                scaleGridLineWidth : 3,
                //Boolean - Whether to show horizontal lines (except X axis)
                scaleShowHorizontalLines: true,
                //Boolean - Whether to show vertical lines (except Y axis)
                scaleShowVerticalLines: true,
                //Boolean - Whether the line is curved between points
                bezierCurve : true,
                //Number - Tension of the bezier curve between points
                bezierCurveTension : 1,
                //Boolean - Whether to show a dot for each point
                pointDot : true,
                //Number - Radius of each point dot in pixels
                pointDotRadius : 8,
                //Number - Pixel width of point dot stroke
                pointDotStrokeWidth : 4,
                //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
                pointHitDetectionRadius : 8,
                //Boolean - Whether to show a stroke for datasets
                datasetStroke : true,
                //Number - Pixel width of dataset stroke
                datasetStrokeWidth : 2,
                //Boolean - Whether to fill the dataset with a colour
                datasetFill : false,
                //Set y axis
                scaleOverride: true,
                scaleSteps: 11,
                scaleStepWidth: -10,
                scaleStartValue: 100,
                // String - Template string for single tooltips
                multiTooltipTemplate: function(chartData){
                    //console.log(chartData);
                    return chartData.datasetLabel + " : " + chartData.value;
                }
            });
        });
    </script>
@stop