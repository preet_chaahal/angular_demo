@extends('layouts.backend.master')

@section('style')
    <style>
        .node { cursor: pointer; }
        .node circle { fill: #fff; stroke: steelblue; stroke-width: 1.5px; }
        .node text { font: 10px sans-serif; }
        .link { fill: none; stroke: #ccc; stroke-width: 1.5px; }
    </style>
@stop


@section('content')

    <div class="page page-dashboard">
        <div class="pageheader">
            <h2>Brainstorm <span>// Discover uncovered long tail keywords</span></h2>
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <a href="{{ URL::route('admin.dashboard') }}"><i class="fa fa-home"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href="{{ URL::route('admin.tools.brainstorm.index') }}">Brainstorm Admin</a>
                    </li>
                </ul>
            </div>
        </div>

        @include('layouts.backend.partials.messages')

        <!-- row -->
        <div class="row">
            <!-- col -->
            <div class="col-md-12">
                <!-- tile -->
                <section class="tile">
                    <div class="tile-header bg-slategray text-center">
                        <h4 class="custom-font text-uppercase">Viewing Brainstorm: <strong>{{ $brainstorm->keyword }}</strong></h4>
                        <ul class="controls">
                            <li class="dropdown">
                                <a role="button" tabindex="0" class="dropdown-toggle settings" data-toggle="dropdown">
                                    <i class="fa fa-cog"></i>
                                    <i class="fa fa-spinner fa-spin"></i>
                                </a>
                                <ul class="dropdown-menu pull-right with-arrow animated littleFadeInUp">
                                    <li>
                                        <a role="button" tabindex="0" class="tile-toggle">
                                            <span class="minimize"><i class="fa fa-angle-down"></i>&nbsp;&nbsp;&nbsp;Minimize</span>
                                            <span class="expand"><i class="fa fa-angle-up"></i>&nbsp;&nbsp;&nbsp;Expand</span>
                                        </a>
                                    </li>
                                    <li>
                                        @if($hasData)
                                            <a href="{{ URL::route('admin.tools.brainstorm.refresh', $brainstorm->id) }}" tabindex="0" onclick="swapElement('#dthree', '#ajax-refresh');">
                                        @else
                                            <a href="{{ URL::route('admin.tools.brainstorm.refresh', $brainstorm->id) }}" tabindex="0" onclick="swapElement('#alert-nodata', '#ajax-refresh');">
                                        @endif
                                            <i class="fa fa-refresh"></i> Refresh Data
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ URL::route('admin.tools.ranktracker.delete', $brainstorm->id) }}" tabindex="0" class="" onclick="return confirm('Are you sure you wish to delete this group?');">
                                            <i class="fa fa-trash-o"></i> Delete
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div> <!-- tile-header -->
                    <!-- tile body -->
                    <div class="tile-body">
                        @if($hasData)
                            <div id="dthree"></div>
                        @else
                            <div id="alert-nodata" class="alert alert-big alert-lightred alert-dismissable fade in">
                                <h4><strong>Heads up!</strong></h4>
                                <p>
                                    There was a problem quering for suggestions, please try again
                                </p>
                                <p>
                                    <a href="{{ URL::route('admin.tools.brainstorm.refresh', $brainstorm->id) }}" class="btn btn-default" onclick="swapElement('#alert-nodata', '#ajax-refresh');">Try Again</a>
                                </p>
                            </div>
                        @endif

                        <div id="ajax-refresh" class="ajax-hidden ajax-success-wait text-center" style="padding: 20px;">
                                <img src="{{ asset('assets/backend/images/success-loader.gif') }}" alt="">
                                <strong>
                                    Please Wait, Refreshing Data ...
                                </strong>
                            </div>
                    </div> <!-- tile body -->
                </section>
                <!-- / tile -->
            </div>
            <!-- / col -->
        </div>
        <!-- / row -->
    </div>

@stop


@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.5/d3.min.js"></script>

    <script>
        var jsonData = '{!! $keywords !!}';

        var margin = {top: 20, right: 120, bottom: 20, left: 120},
            width = 960 - margin.right - margin.left,
            height = 600 - margin.top - margin.bottom;
        var i = 0,
            duration = 750,
            root;
        var tree = d3.layout.tree().size([height, width]);
        var diagonal = d3.svg.diagonal().projection(function(d) { return [d.y, d.x]; });
        var svg = d3.select("#dthree")
            .append("svg")
            .attr("width", width + margin.right + margin.left)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

        root = JSON.parse(jsonData);
        root.x0 = height / 2;
        root.y0 = 0;
        function collapse(d) {
            if (d.children) {
                d._children = d.children;
                d._children.forEach(collapse);
                d.children = null;
            }
        }

        root.children.forEach(collapse);
        update(root);

        function update(source) {
            // Compute the new tree layout.
            var nodes = tree.nodes(root).reverse(), links = tree.links(nodes);
            // Normalize for fixed-depth.
            nodes.forEach(function(d) { d.y = d.depth * 180; });
            // Update the nodes…
            var node = svg.selectAll("g.node")
                .data(nodes, function(d) { return d.id || (d.id = ++i); });
            // Enter any new nodes at the parent's previous position.
            var nodeEnter = node.enter().append("g")
                .attr("class", "node")
                .attr("transform", function(d) { return "translate(" + source.y0 + "," + source.x0 + ")"; })
                .on("click", click);
            nodeEnter.append("circle")
                .attr("r", 1e-6)
                .style("fill", function(d) { return d._children ? "lightsteelblue" : "#fff"; });
            nodeEnter.append("text")
                .attr("x", function(d) { return d.children || d._children ? -10 : 10; })
                .attr("dy", ".35em")
                .attr("text-anchor", function(d) { return d.children || d._children ? "end" : "start"; })
                .text(function(d) { return d.name; })
                .style("fill-opacity", 1e-6);
            // Transition nodes to their new position.
            var nodeUpdate = node.transition()
                .duration(duration)
                .attr("transform", function(d) { return "translate(" + d.y + "," + d.x + ")"; });
            nodeUpdate.select("circle")
                .attr("r", 4.5)
                .style("fill", function(d) { return d._children ? "lightsteelblue" : "#fff"; });
            nodeUpdate.select("text")
                .style("fill-opacity", 1);
            // Transition exiting nodes to the parent's new position.
            var nodeExit = node.exit().transition()
                .duration(duration)
                .attr("transform", function(d) { return "translate(" + source.y + "," + source.x + ")"; })
                .remove();
            nodeExit.select("circle")
                .attr("r", 1e-6);
            nodeExit.select("text")
                .style("fill-opacity", 1e-6);
            // Update the links…
            var link = svg.selectAll("path.link")
                .data(links, function(d) { return d.target.id; });
            // Enter any new links at the parent's previous position.
            link.enter().insert("path", "g")
                .attr("class", "link")
                .attr("d", function(d) {
                    var o = {x: source.x0, y: source.y0};
                    return diagonal({source: o, target: o});
                });
            // Transition links to their new position.
            link.transition()
                .duration(duration)
                .attr("d", diagonal);
            // Transition exiting nodes to the parent's new position.
            link.exit().transition()
                .duration(duration)
                .attr("d", function(d) {
                    var o = {x: source.x, y: source.y};
                    return diagonal({source: o, target: o});
                })
                .remove();
            // Stash the old positions for transition.
            nodes.forEach(function(d) {
                d.x0 = d.x;
                d.y0 = d.y;
            });
        }

        // Toggle children on click.
        function click(d) {
            if (d.children) {
                d._children = d.children;
                d.children = null;
            } else {
                d.children = d._children;
                d._children = null;
            }
            update(d);
        }
    </script>
@stop