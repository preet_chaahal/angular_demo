@extends('layouts.backend.master')

@section('style')
@stop


@section('content')

    <div class="page page-dashboard">
        <div class="pageheader">
            <h2>Brainstorm <span>// Discover uncovered long tail keywords</span></h2>
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <a href="{{ URL::route('admin.dashboard') }}"><i class="fa fa-home"></i> Admin Dashboard</a>
                    </li>
                    <li>
                        <a href="{{ URL::route('backend.tools.brainstorm') }}">Brainstorm Admin</a>
                    </li>
                </ul>
            </div>
        </div>

        @include('layouts.backend.partials.messages')

        <!-- row -->
        <div class="row">
            <!-- col -->
            <div class="col-md-12">
                <!-- tile -->
                <section class="tile tile-simple">
                    <!-- tile body -->
                    <div class="tile-body p-0">
                        <div role="tabpanel">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs tabs-dark" role="tablist">
                                <li class="active"><a href="{{ URL::route('admin.tools.brainstorm.index') }}">Listing</a></li>
                                <li><a href="{{ URL::route('admin.tools.brainstorm.add') }}">New Brainstorm</a></li>
                                <li class="tabs-title pull-right" style="padding-right: 15px;">Recent <strong>Brainstorms</strong></li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>
                                                Keyword
                                            </th>
                                            <th>
                                                User
                                            </th>
                                            <th>
                                                Controls
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if($brainstorms->count() > 0)
                                            @foreach($brainstorms as $brainstorm)
                                                <tr>
                                                    <td>
                                                        <a href="{{ URL::route('admin.tools.brainstorm.view', $brainstorm->id) }}">
                                                            {{ isset($brainstorm->keyword)?$brainstorm->keyword:'' }}
                                                        </a>
                                                    </td>
                                                    <td>
                                                        {{ isset($brainstorm->user->username)?$brainstorm->user->username:'' }}
                                                    </td>
                                                    <td>
                                                       <a href="{{ URL::route('admin.tools.brainstorm.delete', $brainstorm->id) }}" onclick="return confirm('Are you sure you wish to delete this group?');">Delete</a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="1">
                                                    <div class="alert alert-warning">
                                                        <strong>Oh No!</strong>
                                                        You do not have any brainstorming happening
                                                    </div>
                                                </td>
                                            </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                            <!-- /Tab panes -->
                        </div>
                    </div>
                    <!-- /tile body -->
                </section>
                <!-- / tile -->
            </div>
            <!-- / col -->
        </div>
        <!-- / row -->

    </div>
@stop


@section('scripts')
@stop