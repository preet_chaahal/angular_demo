@extends('layouts.backend.master')

@section('style')
@stop


@section('content')
    <div class="page page-dashboard">
        <div class="pageheader">
            <h2>Proxy Admin <span>// Manage saved domains set by users</span></h2>
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <a href="{{ URL::route('admin.dashboard') }}"><i class="fa fa-home"></i> Admin Dashboard</a>
                    </li>
                    <li>
                        <a href="{{ URL::route('admin.proxies','rank-tool-proxy') }}">Proxy Administration</a>
                    </li>
                </ul>
            </div>
        </div>

        @include('layouts.backend.partials.messages')

                <!-- row -->
        <div class="row">
            <!-- col -->
            <div class="col-md-12">
                <!-- tile -->
                <section class="tile tile-simple">
                    <!-- tile body -->
                    <div class="tile-body p-0">
                        <div role="tabpanel">


                            <!-- Tab panes -->
                            <div class="tile-body">

                                <form method="POST" action="{{ URL::route('admin.proxies.import.post') }}">
                                    <input type="hidden" name="module" value="{{ $module }}">

                                    <!-- type -->
                                    <div class="form-group">
                                        <label for="type">Proxy Type</label>
                                        <select id="type" name="type" class="form-control">
                                            <option>http</option>
                                            <option>socks</option>
                                            <option>iface</option>
                                        </select>
                                    </div>
                                    <!-- type -->

                                    <!-- proxy -->
                                    <div class="form-group">
                                        <label for="bulkProxy">Bulk Import</label>
                                        <textarea name="bulkProxy" id="bulkProxy" cols="30" rows="10" placeholder="ip:port:username:password" class="form-control"></textarea>
                                    </div>
                                    <!-- ip -->

                                    <!-- actions -->
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-8 col-md-offset-2">
                                                <input type="submit" class="btn btn-success btn-lg btn-block" value="Add New Proxy">
                                            </div>
                                        </div>
                                    </div> <!-- actions -->

                                </form>
                            </div>
                            <!-- /Tab panes -->
                        </div>
                    </div>
                    <!-- /tile body -->
                </section>
                <!-- / tile -->
            </div>
            <!-- / col -->
        </div>
        <!-- / row -->

    </div>

@stop


@section('scripts')
@stop