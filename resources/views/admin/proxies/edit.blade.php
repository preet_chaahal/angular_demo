@extends('layouts.backend.master')

@section('style')
@stop


@section('content')
    <div class="page page-dashboard">
        <div class="pageheader">
            <h2>Proxy Admin <span>// Manage saved domains set by users</span></h2>
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <a href="{{ URL::route('admin.dashboard') }}"><i class="fa fa-home"></i> Admin Dashboard</a>
                    </li>
                    <li>
                        <a href="{{ URL::route('admin.proxies','rank-tool-proxy') }}">Proxy Administration</a>
                    </li>
                </ul>
            </div>
        </div>

        @include('layouts.backend.partials.messages')

                <!-- row -->
        <div class="row">
            <!-- col -->
            <div class="col-md-12">
                <!-- tile -->
                <section class="tile tile-simple">
                    <!-- tile body -->
                    <div class="tile-body p-0">
                        <div role="tabpanel">


                            <!-- Tab panes -->
                            <div class="tile-body">

                                <form method="POST" action="{{ URL::route('admin.proxies.update') }}">
                                    {{--   <input type="hidden" name="_token" value="{{ csrf_token() }}">--}}
                                    <input type="hidden" name="id" value="{{ $proxy->id }}">
                                    <input type="hidden" name="module" value="{{ $proxy->module }}">

                                    <!-- type -->
                                    <div class="form-group">
                                        <label for="type">Proxy Type</label>
                                        <select id="type" name="type" class="form-control">
                                            @if($proxy->type == 'http')
                                                <option selected="selected">http</option>
                                            @else
                                                <option>http</option>
                                            @endif
                                            @if($proxy->type == 'socks')
                                                <option selected="selected">socks</option>
                                            @else
                                                <option>socks</option>
                                            @endif
                                            @if($proxy->type == 'iface')
                                                <option selected="selected">iface</option>
                                            @else
                                                <option>iface</option>
                                            @endif

                                        </select>
                                    </div>
                                    <!-- type -->

                                    <!-- ip -->
                                    <div class="form-group">
                                        <label for="ip">IP</label>
                                        <input type="text" class="form-control" id="ip" name="ip"
                                               value="{{ old('ip', $proxy->ip) }}">
                                    </div>
                                    <!-- ip -->

                                    <!-- port -->
                                    <div class="form-group">
                                        <label for="port">Port</label>
                                        <input type="text" class="form-control" id="port" name="port"
                                               value="{{ old('port', $proxy->port) }}">
                                    </div>
                                    <!-- port -->

                                    <!-- username -->
                                    <div class="form-group">
                                        <label for="username">Username</label>
                                        <input type="text" class="form-control" id="username" name="username"
                                               value="{{ old('username', $proxy->user) }}">
                                    </div>
                                    <!-- username -->

                                    <!-- password -->
                                    <div class="form-group">
                                        <label for="password">Password</label>
                                        <input type="text" class="form-control" id="password" name="password"
                                               value="{{ old('password', $proxy->password) }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="password">Status</label>
                                        <select name="status" class="form-control">
                                                <option value="1" <?php if($proxy->status == 1){echo "selected='selected'";}?> >Active</option>
                                                <option value="0" <?php if($proxy->status == 0){echo "selected='selected'";}?>>Inactive</option>
                                        </select>

                                    </div>
                                    <!-- password -->

                                    <!-- actions -->
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-8 col-md-offset-2">
                                                <input type="submit" class="btn btn-success btn-lg btn-block"
                                                       value="Add New Proxy">
                                            </div>
                                        </div>
                                    </div> <!-- actions -->

                                </form>
                            </div>
                            <!-- /Tab panes -->
                        </div>
                    </div>
                    <!-- /tile body -->
                </section>
                <!-- / tile -->
            </div>
            <!-- / col -->
        </div>
        <!-- / row -->

    </div>

@stop


@section('scripts')
@stop