@extends('layouts.backend.master')

@section('style')
@stop


@section('content')
<div class="page page-dashboard">
        <div class="pageheader">
            <h2>Proxy Admin <span>// Manage saved domains set by users</span></h2>
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <a href="{{ URL::route('admin.dashboard') }}"><i class="fa fa-home"></i> Admin Dashboard</a>
                    </li>
                    <li>
                        <a href="{{ URL::route('admin.proxies','rank-tool-proxy') }}">Proxy Administration</a>
                    </li>
                </ul>
            </div>
        </div>

        @include('layouts.backend.partials.messages')

        <!-- row -->
        <div class="row">
            <!-- col -->
            <div class="col-md-12">
                <!-- tile -->
                <section class="tile tile-simple">
                    <!-- tile body -->
                    <div class="tile-body p-0">
                        <div role="tabpanel">

                            <ul class="nav nav-tabs tabs-dark" role="tablist">
                                <li class="{{ Request::is('admin/proxies/rank-tool-proxy') ? 'active' : '' }}"><a href="{{ URL::route('admin.proxies','rank-tool-proxy') }}">Rank Tool Proxy Setup</a></li>
                                <li class="{{ Request::is('admin/proxies/keyword-evolution-proxy') ? 'active' : '' }}"><a href="{{ URL::route('admin.proxies','keyword-evolution-proxy') }}">Keyword Evaluation Proxy Setup</a></li>
                                <li class="{{ Request::is('admin/proxies/keyword-brainstorming-proxy') ? 'active' : '' }}"><a href="{{ URL::route('admin.proxies','keyword-brainstorming-proxy') }}">Keyword Brainstorming Proxy Setup</a></li>
                                {{--<li class="{{ Request::is('registration-management/disable/email') ? 'active' : '' }}"><a href="{{ URL::route('admin.proxies') }}">Page Rank Proxy Setup</a></li>--}}
                            </ul>

                            <!-- Tab panes -->
                            <div class="tile-body">

                                <div class="proxy-table">
                                <div class="col-md-12">
                                    <div class="pull-right">
                                        <a href="{{ URL::route('admin.proxies.import',$module) }}" class="btn btn-primary">Import Proxies</a>
                                        <a href="{{ URL::route('admin.proxies.add',$module) }}" class="btn btn-success">Add New Proxy</a>
                                    </div>
                                </div>
                                <table class="table">
                                <thead>
                                    <tr>
                                        <th>
                                            #
                                        </th>
                                        <th>
                                            Type
                                        </th>
                                        <th>
                                            IP
                                        </th>
                                        <th>
                                            Port
                                        </th>
                                        <th>
                                            User
                                        </th>
                                        <th>
                                            Password
                                        </th>
                                        <th>
                                            Last Access Time
                                        </th>
                                        <th>
                                           Status
                                        </th>

                                        <th>
                                            Controls
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($proxies as $proxie)
                                        <tr>
                                            <td>

                                                    {{ $proxie->id }}

                                            </td>
                                            <td>
                                                {{ $proxie->type }}
                                            </td>
                                            <td>
                                                <a href="{{ URL::route('admin.proxies.log',[$proxie->ip,'id'=>$proxie->id]) }}" type="button" class="btn btn-primary btn-xs mb-10">{{ $proxie->ip }}</a>
                                            </td>
                                            <td>
                                                {{ $proxie->port }}
                                            </td>
                                            <td>
                                                {{ $proxie->user }}
                                            </td>
                                            <td>
                                                {{ $proxie->password }}
                                            </td>
                                            <td>
                                                {{ $proxie->access_time }}
                                            </td>
                                            <td>
                                                {{ ($proxie->status==1)?'Active':'Inactive' }}
                                            </td>
                                            <td>
                                                <a href="{{ URL::route('admin.proxies.edit', $proxie->id) }}">Edit</a> | <a href="{{ URL::route('admin.proxies.delete', $proxie->id) }}" onclick="return confirm('Are you sure you wish to delete this group?');">Delete</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <!-- /Tab panes -->
                        </div>
                    </div>
                    <!-- /tile body -->
                </section>
                <!-- / tile -->
            </div>
            <!-- / col -->
        </div>
        <!-- / row -->

    </div>

@stop


@section('scripts')
    <script>
        function onClickIp(ip){
            $.ajax({
                url: 'http://' + window.location.hostname + '/admin/proxies/access-log/'+ip,
                type: "GET",
                success: function (data) {
                    var ipTable = document.getElementById("ipTable");
                    $("#ipTable").empty();
                    for (i = 0; i < data.length; i++) {
                        var row = ipTable.insertRow(0);
                        var cell1 = row.insertCell(0);
                        var cell2 = row.insertCell(1);
                        var cell3 = row.insertCell(2);
                        var cell4 = row.insertCell(3);
                        cell1.innerHTML = data[i].ip;
                        cell2.innerHTML = data[i].error_code;
                        cell3.innerHTML = data[i].error;
                        cell4.innerHTML = data[i].updated_at;
                    }
                },
                error: function (result) {
                    var resText = JSON.parse(data.responseText);
                    $("#errorsBox").html("");
                    $("#errorsBox").append('<div><ul><li>'+resText.message+'</li></ul></div>');
                    $("#errorsBox").show();
                    setTimeout(function () {
                        $('div.alert').fadeOut('fast');
                    }, 3000);
                }
            })
        }
    </script>
@stop