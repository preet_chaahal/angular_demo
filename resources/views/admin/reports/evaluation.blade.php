@extends('layouts.backend.master')

@section('style')
@stop


@section('content')

    <div class="page page-dashboard">
        <div class="pageheader">
            <h2>Administration</h2>

            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <a href="{{ URL::route('admin.dashboard') }}"><i class="fa fa-home"></i> Admin Dashboard</a>
                    </li>
                    <li>
                        <a href="{{ URL::route('admin.reports') }}">Reports Admin</a>
                    </li>
                </ul>
            </div>
        </div>

        @include('layouts.backend.partials.messages')

                <!-- row -->
        <div class="row">
            <!-- col -->
            <div class="col-md-12">
                <!-- tile -->
                <section class="tile tile-simple">

                    <div class="tile-body p-0">
                        <div role="tabpanel">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs tabs-dark tabs-right" role="tablist">
                                <li class="tabs-title pull-left">Report <strong>Evaluation</strong></li>
                            </ul>
                            <div class="tile-body">
                                <div class="tab-content">
                                    <form action="{{ URL::route('admin.report.evaluation.download') }}" method="POST">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                        <div class="form-group">
                                            <label for="">Date Range</label>
                                            <input type="text" id="date_range" name="date_range" class="form-control">
                                        </div>

                                        <div class="form-group">
                                            <input type="button" id="btnSubmit" class="btn btn-success"
                                                   value="Generate Report">
                                        </div>
                                    </form>

                                    <div id="reportContainer">
                                        <div class="form-group row" style="margin-left: 10px" id="keyResearchContainer">
                                            <div class="report-header">Keyword Researches</div>
                                            <div class="col-md-2 form-group">
                                                <div class="text-center report-count">Last 10 Days</div>
                                            </div>
                                            <div id="container" class="col-md-8">
                                            </div>
                                        </div>

                                        <div class="form-group row" style="margin-left: 10px"
                                             id="keyEvaluationContainer">
                                            <div class="report-header">Keyword Evaluation</div>
                                            <div class="col-md-2 form-group">
                                                <div class="text-center report-count">Last 10 Days</div>
                                            </div>
                                            <div id="evalContainer" class="col-md-8">
                                            </div>
                                        </div>

                                        <div class="form-group row" style="margin-left: 10px" id="errorContainer">
                                            <div class="report-header">Error</div>
                                            <div class="col-md-2 form-group">
                                                <div class="text-center report-count">Last 10 Days</div>
                                                <div class="text-center report-count">Proxy Status</div>
                                            </div>
                                            <div id="errContainer" class="col-md-8">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- / tile -->
            </div>
            <!-- / col -->
        </div>
        <!-- / row -->

    </div>
@stop


@section('scripts')
    <script>
        $('#reportContainer').hide();
        $('#btnSubmit').click(function () {
            $.ajax({
                url: 'http://' + window.location.hostname + '/admin/reports/evaluation/report',
                dataType: 'json',
                data: {
                    date:$('#date_range').val()
                },
                type: "POST",
                success: function (data) {
                    renderChartData(data);
                },
                error: function (result) {
                    $("#errorsBox").html("");
                    result.responseJSON.message.forEach(function (item) {
                        $("#errorsBox").append('<div><ul><li>' + item + '</li></ul></div>');
                    });
                    $("#errorsBox").show();
                    setTimeout(function () {
                        $('div.alert').fadeOut('fast');
                    }, 3000);
                }
            })
            $('#reportContainer').show();
        });
        $(function () {
            $("#date_range").daterangepicker({
                locale: {
                    format: 'YYYY-MM-DD'
                }
            });
        });

        function renderChartData(data) {
            $('#container').highcharts({
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Keyword Researches'
                },
//            subtitle: {
//                text: 'Keyword Researches'
//            },
                xAxis: {
                    categories: data.researchDateArr,
                    crosshair: true
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Researches'
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                series: [{
                    name: 'Researches',
                    data: data.researchCountArr

                }]
            });
            $('#evalContainer').highcharts({
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Keyword Evaluation'
                },
//            subtitle: {
//                text: 'Keyword Evaluation'
//            },
                xAxis: {
                    categories: data.evalDateArr,
                    crosshair: true
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Evaluations'
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                series: [{
                    name: 'Evaluations',
                    data: data.evalCountArr

                }]
            });
            $('#errContainer').highcharts({
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Error'
                },
//            subtitle: {
//                text: 'Error'
//            },
                xAxis: {
                    categories: data.resErrorDateArr,
                    crosshair: true
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Errors'
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                series: [{
                    name: 'Errors',
                    data: data.resErrorCountArr

                }]
            });
        }
    </script>
@stop