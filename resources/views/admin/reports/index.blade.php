@extends('layouts.backend.master')

@section('style')
@stop


@section('content')

    <div class="page page-dashboard">
        <div class="pageheader">
            <h2>Administration</h2>

            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <a href="{{ URL::route('admin.dashboard') }}"><i class="fa fa-home"></i> Admin Dashboard</a>
                    </li>
                    <li>
                        <a href="{{ URL::route('admin.reports') }}">Reports Admin</a>
                    </li>
                </ul>
            </div>
        </div>

        @include('layouts.backend.partials.messages')

                <!-- row -->

        <!-- / row -->
        <div class="row">
            <!-- col -->
            <div class="col-md-12">
                <!-- tile -->
                <section class="tile tile-simple">

                    <div class="tile-body p-0">
                        <div role="tabpanel">
                            <!-- Nav tabs -->

                            <div class="tile-body">
                                <div class="tab-content">
                                    <form>
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                        <div class="form-group">
                                            <label for="">Date Range</label>
                                            <?php
                                            $iniStartDate = \Carbon\Carbon::now()->startOfMonth()->toDateString();
                                            $iniStartDate = Carbon\Carbon::parse($iniStartDate)->format('m/d/Y');
                                            $iniEndDate = \Carbon\Carbon::now()->endOfDay()->toDateString();
                                            $iniEndDate = Carbon\Carbon::parse($iniEndDate)->format('m/d/Y');

                                            $dateStr = $iniStartDate . ' - ' . $iniEndDate;
                                            ?>
                                            <input type="text" id="date_range" value="{{$dateStr}}" name="date_range"
                                                   class="form-control">
                                        </div>

                                        <div class="form-group">
                                            <input type="button" id="btnSubmit" class="btn btn-success"
                                                   value="Generate Report">
                                        </div>
                                    </form>
                                    <div id="reportContainer">
                                        <div class="form-group row" style="margin-left: 10px" id="regUserContainer">
                                            <div class="report-header">Registered User</div>
                                            <div class="col-md-3 form-group">
                                                <div class="report-count">
                                                    <div style="font-size: 16px; margin: 5px 5px">Total</div>
                                                    <div style="margin: 15px 15px">
                                                        <div id="totalUser" class="cleanable"></div>
                                                    </div>
                                                </div>
                                                <div class="report-count">
                                                    <div style="font-size: 16px; margin: 5px 5px">Range</div>
                                                    <div style="margin: 15px 15px">
                                                        <div id="rangeUser" class="cleanable"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="container" class="col-md-8">
                                            </div>
                                        </div>

                                        <div class="form-group row" style="margin-left: 10px"
                                             id="subScribedUserContainer">
                                            <div class="report-header">User Subscriptions</div>
                                            <div class="col-md-3 form-group">
                                                <table class="table table-responsive">
                                                    <thead>
                                                        <tr>
                                                            <th>Plans</th>
                                                            <th>Today</th>
                                                            <th>Range</th>
                                                            <th>Canceled</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>Basic</td>
                                                            <td><div id="todayBasicPlan" class="cleanable"></div></td>
                                                            <td><div id="rangeBasicPlan" class="cleanable"></div></td>
                                                            <td><div id="canceledBasicPlan" class="cleanable"></div></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Pro</td>
                                                            <td><div id="todayProPlan" class="cleanable"></div></td>
                                                            <td><div id="rangeProPlan" class="cleanable"></div></td>
                                                            <td><div id="canceledProPlan" class="cleanable"></div></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Elite</td>
                                                            <td><div id="todayElitePlan" class="cleanable"></div></td>
                                                            <td><div id="rangeElitePlan" class="cleanable"></div></td>
                                                            <td><div id="canceledElitePlan" class="cleanable"></div></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Agency</td>
                                                            <td><div id="todayAgencyPlan" class="cleanable"></div></td>
                                                            <td><div id="rangeAgencyPlan" class="cleanable"></div></td>
                                                            <td><div id="canceledAgencyPlan" class="cleanable"></div></td>
                                                        </tr>
                                                        <tr>
                                                            <td>BasicYearly</td>
                                                            <td><div id="todayBasicYearlyPlan" class="cleanable"></div></td>
                                                            <td><div id="rangeBasicYearlyPlan" class="cleanable"></div></td>
                                                            <td><div id="canceledBasicYearlyPlan" class="cleanable"></div></td>
                                                        </tr>
                                                        <tr>
                                                            <td>ProYearly</td>
                                                            <td><div id="todayProYearlyPlan" class="cleanable"></div></td>
                                                            <td><div id="rangeProYearlyPlan" class="cleanable"></div></td>
                                                            <td><div id="canceledProYearlyPlan" class="cleanable"></div></td>
                                                        </tr>
                                                        <tr>
                                                            <td>EliteYearly</td>
                                                            <td><div id="todayEliteYearlyPlan" class="cleanable"></div></td>
                                                            <td><div id="rangeEliteYearlyPlan" class="cleanable"></div></td>
                                                            <td><div id="canceledEliteYearlyPlan" class="cleanable"></div></td>
                                                        </tr>
                                                        <tr>
                                                            <td>AgencyYearly</td>
                                                            <td><div id="todayAgencyYearlyPlan" class="cleanable"></div></td>
                                                            <td><div id="rangeAgencyYearlyPlan" class="cleanable"></div></td>
                                                            <td><div id="canceledAgencyYearlyPlan" class="cleanable"></div></td>
                                                        </tr>
                                                    </tbody>
                                                </table>

                                                <!-- <div class="report-count">
                                                    <div style="font-size: 16px; margin: 5px 5px">Today</div>
                                                    <div style="margin: 15px 15px">
                                                        <div id="todayBasicPlan" class="cleanable"></div>
                                                        <div id="todayProPlan" class="cleanable"></div>
                                                        <div id="todayElitePlan" class="cleanable"></div>
                                                        <div id="todayAgencyPlan" class="cleanable"></div>
                                                        <div id="todayProYearlyPlan" class="cleanable"></div>
                                                        <div id="todayBasicYearlyPlan" class="cleanable"></div>
                                                        <div id="todayEliteYearlyPlan" class="cleanable"></div>
                                                        <div id="todayAgencyYearlyPlan" class="cleanable"></div>
                                                    </div>
                                                </div>
                                                <div class="report-count">
                                                    <div style="font-size: 16px; margin: 5px 5px">Range</div>
                                                    <div style="margin: 15px 15px">
                                                        <div id="rangeBasicPlan" class="cleanable"></div>
                                                        <div id="rangeProPlan" class="cleanable"></div>
                                                        <div id="rangeElitePlan" class="cleanable"></div>
                                                        <div id="rangeAgencyPlan" class="cleanable"></div>
                                                        <div id="rangeProYearlyPlan" class="cleanable"></div>
                                                        <div id="rangeBasicYearlyPlan" class="cleanable"></div>
                                                        <div id="rangeEliteYearlyPlan" class="cleanable"></div>
                                                        <div id="rangeAgencyYearlyPlan" class="cleanable"></div>
                                                    </div>
                                                </div>
                                                <div class="report-count">
                                                    <div style="font-size: 16px; margin: 5px 5px">Cancelled</div>
                                                    <div style="margin: 15px 15px">
                                                        <div id="rangeCancelCount" class="cleanable"></div>
                                                    </div>
                                                </div> -->
                                            </div>
                                            <div id="container1" class="col-md-8">
                                            </div>
                                        </div>

                                        <div class="form-group row" style="margin-left: 10px" id="keyResearchContainer">
                                            <div class="report-header">Keyword Researches</div>
                                            <div id="container3" class="col-md-12"></div>
                                            <div class="col-md-12 form-group">
                                                <div class="report-count">
                                                    <div style="font-size: 16px; margin: 5px 5px">Last 5 days</div>
                                                    <div style="margin: 15px 15px">
                                                        <div>
                                                            <table class="table table-responsive">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Days</th>
                                                                        <th>Free</th>
                                                                        <th>Basic</th>
                                                                        <th>Pro</th>
                                                                        <th>Elite</th>
                                                                        <th>Agency</th>
                                                                        <th>BasicYearly</th>
                                                                        <th>ProYearly</th>
                                                                        <th>EliteYearly</th>
                                                                        <th>AgencyYearly</th>
                                                                        <th>Total</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody id="rTable" class="cleanable"></tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group row" style="margin-left: 10px" id="keyEvaluationContainer">
                                            <div class="report-header">Keyword Evaluation</div>
                                            <div id="evalContainer" class="col-md-12"></div>
                                            <div class="col-md-12 form-group">
                                                <div class="report-count">
                                                    <div style="font-size: 16px; margin: 5px 5px">Last 5 days</div>
                                                    <div style="margin: 15px 15px">
                                                        <div>
                                                            <table class="table table-responsive">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Days</th>
                                                                        <th>Free</th>
                                                                        <th>Basic</th>
                                                                        <th>Pro</th>
                                                                        <th>Elite</th>
                                                                        <th>Agency</th>
                                                                        <th>BasicYearly</th>
                                                                        <th>ProYearly</th>
                                                                        <th>EliteYearly</th>
                                                                        <th>AgencyYearly</th>
                                                                        <th>Total</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody id="eTable" class="cleanable"></tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group row" style="margin-left: 10px">
                                            <div class="report-header">Keyword Brainstorming</div>
                                            <div id="brainstormsContainer" class="col-md-12"></div>
                                            <div class="col-md-12 form-group">
                                                <div class="report-count">
                                                    <div style="font-size: 16px; margin: 5px 5px">Last 5 days</div>
                                                    <div style="margin: 15px 15px">
                                                        <div>
                                                            <table class="table table-responsive">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Days</th>
                                                                        <th>Free</th>
                                                                        <th>Basic</th>
                                                                        <th>Pro</th>
                                                                        <th>Elite</th>
                                                                        <th>Agency</th>
                                                                        <th>BasicYearly</th>
                                                                        <th>ProYearly</th>
                                                                        <th>EliteYearly</th>
                                                                        <th>AgencyYearly</th>
                                                                        <th>Total</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody id="bTable" class="cleanable"></tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group row" style="margin-left: 10px" id="errorContainer">
                                            <div class="report-header">Errors</div>
                                            <div class="col-md-3 form-group">
                                                <div class="report-count">
                                                    <div style="font-size: 16px; margin: 5px 5px">Proxy Status</div>
                                                    <div style="margin: 15px 15px">
                                                        <table class="table table-responsive ">
                                                            <thead>
                                                                <tr>
                                                                    <th>Module</th>
                                                                    <th>In-Active Proxies</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td>Evaluation</td>
                                                                    <td id="evaluationBadProxy" class="cleanable"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Rank Tracker</td>
                                                                    <td id="brainstormBadProxy" class="cleanable"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Brainstorming</td>
                                                                    <td id="rankBadProxy" class="cleanable"></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="errContainer" class="col-md-8"></div>
                                            <div class="col-md-12 form-group">
                                                <div class="report-count">
                                                    <div style="font-size: 16px; margin: 5px 5px">Last 5 days</div>
                                                    <div style="margin: 15px 15px">
                                                        <div>
                                                            <table class="table table-responsive">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Days</th>
                                                                        <th>Empty Response</th>
                                                                        <th>Keywords Search</th>
                                                                        <th>Keywords Additional Search</th>
                                                                        <th>Social Networks</th>
                                                                        <th>Google Moz</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody id="errTable" class="cleanable"></tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </div>
                </section>
                <!-- / tile -->
            </div>
            <!-- / col -->
        </div>
    </div>
@stop


@section('scripts')
    <script>


        $('#btnSubmit').click(function () {
            $.ajax({
                url: '{{ URL::route('admin.report.users.report') }}',
                dataType: 'json',
                data: {
                    date: $('#date_range').val()
                },
                type: "POST",
                success: function (data) {
                    console.log(data);
                    renderChartData(data);
                },
                error: function (result) {
                    $("#errorsBox").html("");
                    result.responseJSON.message.forEach(function (item) {
                        $("#errorsBox").append('<div><ul><li>' + item + '</li></ul></div>');
                    });
                    $("#errorsBox").show();
                    setTimeout(function () {
                        $('div.alert').fadeOut('fast');
                    }, 3000);
                }
            })
            $('#reportContainer').show();
        });
        $(window).load(function loadReportData() {
            $.ajax({
                url: '{{ URL::route('admin.report.users.report') }}',
                dataType: 'json',
                data: {
                    date: $('#date_range').val()
                },
                type: "POST",
                success: function (data) {
                    console.log(data);
                    renderChartData(data);
                },
                error: function (result) {
                    $("#errorsBox").html("");
                    result.responseJSON.message.forEach(function (item) {
                        $("#errorsBox").append('<div><ul><li>' + item + '</li></ul></div>');
                    });
                    $("#errorsBox").show();
                    setTimeout(function () {
                        $('div.alert').fadeOut('fast');
                    }, 3000);
                }
            })
            $('#reportContainer').show();
        });

        $(function () {
            $("#date_range").daterangepicker({
                locale: {
                    format: 'YYYY-MM-DD'
                }
            });
        });

        function renderChartData(data) {
            $(".cleanable").empty();

            $("#totalUser").append('Registered : ' + data.totalRegUser);
            $('#rangeUser').append('Registered : ' + data.rangeUser);

            $('#todayBasicPlan').append(data.todayBasicPlan);
            $('#todayProPlan').append(data.todayProPlan);
            $('#todayElitePlan').append(data.todayElitePlan);
            $('#todayAgencyPlan').append(data.todayAgencyPlan);
            $('#todayBasicYearlyPlan').append(data.todayBasicYearlyPlan);
            $('#todayProYearlyPlan').append(data.todayProYearlyPlan);
            $('#todayEliteYearlyPlan').append(data.todayEliteYearlyPlan);
            $('#todayAgencyYearlyPlan').append(data.todayAgencyYearlyPlan);

            $('#rangeBasicPlan').append(data.rangeBasicPlan);
            $('#rangeProPlan').append(data.rangeProPlan);
            $('#rangeElitePlan').append(data.rangeElitePlan);
            $('#rangeAgencyPlan').append(data.rangeAgencyPlan);
            $('#rangeBasicYearlyPlan').append(data.rangeBasicYearlyPlan);
            $('#rangeProYearlyPlan').append(data.rangeProYearlyPlan);
            $('#rangeEliteYearlyPlan').append(data.rangeEliteYearlyPlan);
            $('#rangeAgencyYearlyPlan').append(data.rangeAgencyYearlyPlan);

            $('#canceledBasicPlan').append(data.canceledBasicPlan);
            $('#canceledProPlan').append(data.canceledProPlan);
            $('#canceledElitePlan').append(data.canceledElitePlan);
            $('#canceledAgencyPlan').append(data.canceledAgencyPlan);
            $('#canceledBasicYearlyPlan').append(data.canceledBasicYearlyPlan);
            $('#canceledProYearlyPlan').append(data.canceledProYearlyPlan);
            $('#canceledEliteYearlyPlan').append(data.canceledEliteYearlyPlan);
            $('#canceledAgencyYearlyPlan').append(data.canceledAgencyYearlyPlan);

            $('#rtCancelled').append('Cancelled : ' + data.totalCancelCount);

            $('#evaluationBadProxy').append(data.evaluationBadProxy);
            $('#brainstormBadProxy').append(data.brainstormBadProxy);
            $('#rankBadProxy').append(data.rankBadProxy);

            var rTable = document.getElementById("rTable");
            for (i = 0; i < data.researchCountArr.length; i++) {
                var row = rTable.insertRow(0);
                row.insertCell(0).innerHTML = data.researchDateArr[i];
                row.insertCell(1).innerHTML = data.researchesFreePlan[i];
                row.insertCell(2).innerHTML = data.researchesBasicPlan[i];
                row.insertCell(3).innerHTML = data.researchesProPlan[i];
                row.insertCell(4).innerHTML = data.researchesElitePlan[i];
                row.insertCell(5).innerHTML = data.researchesAgencyPlan[i];
                row.insertCell(6).innerHTML = data.researchesBasicYearlyPlan[i];
                row.insertCell(7).innerHTML = data.researchesProYearlyPlan[i];
                row.insertCell(8).innerHTML = data.researchesEliteYearlyPlan[i];
                row.insertCell(9).innerHTML = data.researchesAgencyYearlyPlan[i];
                row.insertCell(10).innerHTML = data.researchCountArr[i]; 
            }

            var eTable = document.getElementById("eTable");
            for (i = 0; i < data.evaluationsCountArr.length; i++) {
                var row = eTable.insertRow(0);
                row.insertCell(0).innerHTML = data.evaluationsDateArr[i];
                row.insertCell(1).innerHTML = data.evaluationsFreePlan[i];
                row.insertCell(2).innerHTML = data.evaluationsBasicPlan[i];
                row.insertCell(3).innerHTML = data.evaluationsProPlan[i];
                row.insertCell(4).innerHTML = data.evaluationsElitePlan[i];
                row.insertCell(5).innerHTML = data.evaluationsAgencyPlan[i];
                row.insertCell(6).innerHTML = data.evaluationsBasicYearlyPlan[i];
                row.insertCell(7).innerHTML = data.evaluationsProYearlyPlan[i];
                row.insertCell(8).innerHTML = data.evaluationsEliteYearlyPlan[i];
                row.insertCell(9).innerHTML = data.evaluationsAgencyYearlyPlan[i];
                row.insertCell(10).innerHTML = data.evaluationsCountArr[i];
            }

            var bTable = document.getElementById("bTable");
            for (i = 0; i < data.brainstormsCountArr.length; i++) {
                var row = bTable.insertRow(0);
                row.insertCell(0).innerHTML = data.brainstormsDateArr[i];
                row.insertCell(1).innerHTML = data.brainstormsFreePlan[i];
                row.insertCell(2).innerHTML = data.brainstormsBasicPlan[i];
                row.insertCell(3).innerHTML = data.brainstormsProPlan[i];
                row.insertCell(4).innerHTML = data.brainstormsElitePlan[i];
                row.insertCell(5).innerHTML = data.brainstormsAgencyPlan[i];
                row.insertCell(6).innerHTML = data.brainstormsBasicYearlyPlan[i];
                row.insertCell(7).innerHTML = data.brainstormsProYearlyPlan[i];
                row.insertCell(8).innerHTML = data.brainstormsEliteYearlyPlan[i];
                row.insertCell(9).innerHTML = data.brainstormsAgencyYearlyPlan[i];
                row.insertCell(10).innerHTML = data.brainstormsCountArr[i];
            }

            var errTable = document.getElementById("errTable");
            for (i = 0; i < data.resErrorDateArr.length; i++) {
                var row = errTable.insertRow(0);
                row.insertCell(0).innerHTML = data.resErrorDateArr[i];
                row.insertCell(1).innerHTML = data.errorEmptyResponse[i];
                row.insertCell(2).innerHTML = data.errorKeywordsSearch[i];
                row.insertCell(3).innerHTML = data.errorKeywordsAdditionalSearch[i];
                row.insertCell(4).innerHTML = data.errorSocialNetworks[i];
                row.insertCell(5).innerHTML = data.errorGoogleMoz[i];
            }

            $('#container').highcharts({
                title: {
                    text: 'Registered User',
                    x: -20 //center
                },
                xAxis: {
                    categories: data.userDateArr
                },
                yAxis: {
                    title: {
                        text: 'Users'
                    },
                    plotLines: [{
                        value: 0,
                        width: 1,
                        color: '#808080'
                    }]
                },
                tooltip: {
                    valueSuffix: ''
                },
                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'middle',
                    borderWidth: 0
                },
                series: [{
                    name: 'New User',
                    data: data.regUserCountArr
                }]
            });

            $('#container1').highcharts({
                title: {
                    text: 'User Subscriptions',
                    x: -20 //center
                },
                xAxis: {
                    categories: data.userDateArr
                },
                yAxis: {
                    title: {
                        text: 'Users'
                    },
                    plotLines: [{
                        value: 0,
                        width: 1,
                        color: '#808080'
                    }]
                },
                tooltip: {
                    valueSuffix: ''
                },
                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'middle',
                    borderWidth: 0
                },
                series: [{
                    name: 'User Subscriptions',
                    data: data.sameMonthSubCountArr
                }, {
                    name: 'User Subscriptions LM',
                    data: data.prevMonthSubCountArr
                }]
            });
            $('#container3').highcharts({
                chart: {
                    type: 'column'
                },
                title: {
                    text: ''
                },
                xAxis: {
                    categories: data.researchDateArr,
                    crosshair: true
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Researches'
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y}</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                series: [{
                    name: 'Researches',
                    data: data.researchCountArr

                }]
            });

            $('#evalContainer').highcharts({
                chart: {
                    type: 'column'
                },
                title: {
                    text: ''
                },
                xAxis: {
                    categories: data.evaluationsDateArr,
                    crosshair: true
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Evaluations'
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y}</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                series: [{
                    name: 'Evaluations',
                    data: data.evaluationsCountArr

                }]
            });

            $('#brainstormsContainer').highcharts({
                chart: {
                    type: 'column'
                },
                title: {
                    text: ''
                },
                xAxis: {
                    categories: data.brainstormsDateArr,
                    crosshair: true
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Brainstorms'
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y}</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                series: [{
                    name: 'Brainstorms',
                    data: data.brainstormsCountArr

                }]
            });

            $('#errContainer').highcharts({
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Error'
                },
                xAxis: {
                    categories: data.resErrorDateArr,
                    crosshair: true
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Errors'
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y}</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                series: [{
                    name: 'Errors',
                    data: data.resErrorCountArr

                }]
            });
        }
    </script>
@stop