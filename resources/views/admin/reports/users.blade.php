@extends('layouts.backend.master')

@section('style')
@stop


@section('content')

    <div class="page page-dashboard">
        <div class="pageheader">
            <h2>Administration</h2>

            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <a href="{{ URL::route('admin.dashboard') }}"><i class="fa fa-home"></i> Admin Dashboard</a>
                    </li>
                    <li>
                        <a href="{{ URL::route('admin.reports') }}">Reports Admin</a>
                    </li>
                </ul>
            </div>
        </div>

        @include('layouts.backend.partials.messages')

                <!-- row -->
        <div class="row">
            <!-- col -->
            <div class="col-md-12">
                <!-- tile -->
                <section class="tile tile-simple">

                    <div class="tile-body p-0">
                        <div role="tabpanel">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs tabs-dark tabs-right" role="tablist">
                                <li class="tabs-title pull-left">Report <strong>Users</strong></li>
                            </ul>
                            <div class="tile-body">
                                <div class="tab-content">
                                    <form>
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                        <div class="form-group">
                                            <label for="">Date Range</label>
                                            <input type="text" id="date_range" name="date_range" class="form-control">
                                        </div>

                                        {{--<div class="form-group">--}}
                                        {{--<label for="">Type</label>--}}
                                        {{--<select name="type" id="type" class="form-control">--}}
                                        {{--<option value="1">Users who logged on that day</option>--}}
                                        {{--<option value="2">New Users for that time period</option>--}}
                                        {{--</select>--}}
                                        {{--</div>--}}

                                        <div class="form-group">
                                            <input type="button" id="btnSubmit" class="btn btn-success"
                                                   value="Generate Report">
                                        </div>
                                    </form>
                                    <div id="reportContainer">
                                        <div class="form-group row" style="margin-left: 10px" id="regUserContainer">
                                            <div class="report-header">Registered User</div>
                                            <div class="col-md-2 form-group">
                                                <div class="text-center report-count">Total</div>
                                                <div class="text-center report-count">Range</div>
                                            </div>
                                            <div id="container" class="col-md-8">
                                            </div>
                                        </div>
                                        <div class="form-group row" style="margin-left: 10px"
                                             id="subScribedUserContainer">
                                            <div class="report-header">Subscribed User</div>
                                            <div class="col-md-2 form-group">
                                                <div class="text-center report-count">Today</div>
                                                <div class="text-center report-count">Range</div>
                                                <div class="text-center report-count">Canceled</div>
                                            </div>
                                            <div id="container1" class="col-md-8">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- / tile -->
            </div>
            <!-- / col -->
        </div>
        <!-- / row -->

    </div>
@stop


@section('scripts')
    <script>

        $('#reportContainer').hide();
        $('#btnSubmit').click(function(){
            $.ajax({
                url: 'http://' + window.location.hostname + '/admin/reports/users/report',
                dataType: 'json',
                data: {
                    date:$('#date_range').val()
                },
                type: "POST",
                success: function (data) {
                    renderChartData(data);
                },
                error: function (result) {
                    $("#errorsBox").html("");
                    result.responseJSON.message.forEach(function (item) {
                        $("#errorsBox").append('<div><ul><li>' + item + '</li></ul></div>');
                    });
                    $("#errorsBox").show();
                    setTimeout(function () {
                        $('div.alert').fadeOut('fast');
                    }, 3000);
                }
            })
            $('#reportContainer').show();
        });

        $(function () {
            $("#date_range").daterangepicker({
                locale: {
                    format: 'YYYY-MM-DD'
                }
            });
        });

        function renderChartData(data){
            $('#container').highcharts({
                title: {
                    text: 'Registered User',
                    x: -20 //center
                },
//                subtitle: {
//                    text: 'Registered User',
//                    x: -20
//                },
                xAxis: {
                    categories: data.userDateArr
                },
                yAxis: {
                    title: {
                        text: 'Users'
                    },
                    plotLines: [{
                        value: 0,
                        width: 1,
                        color: '#808080'
                    }]
                },
                tooltip: {
                    valueSuffix: ''
                },
                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'middle',
                    borderWidth: 0
                },
                series: [{
                    name: 'New User',
                    data: data.regUserCountArr
                }]
            });

            $('#container1').highcharts({
                title: {
                    text: 'Subscribed User',
                    x: -20 //center
                },
//                subtitle: {
//                    text: 'Subscribed User',
//                    x: -20
//                },
                xAxis: {
                    categories: data.subDateArr
                },
                yAxis: {
                    title: {
                        text: 'Users'
                    },
                    plotLines: [{
                        value: 0,
                        width: 1,
                        color: '#808080'
                    }]
                },
                tooltip: {
                    valueSuffix: ''
                },
                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'middle',
                    borderWidth: 0
                },
                series: [{
                    name: 'Subscribed Users',
                    data: data.sameMonthSubCountArr
                }, {
                    name: 'Subscribed Users LM',
                    data: data.prevMonthSubCountArr
                }]
            });
        };
    </script>
@stop

