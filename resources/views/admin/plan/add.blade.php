@extends('layouts.backend.master')

@section('style')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.10/css/dataTables.bootstrap.min.css">
@stop


@section('content')
    <div class="page page-dashboard">
        <div class="pageheader">
            <h2>Plan Administration <span>// Manage Plan Role</span></h2>
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <a href="{{ URL::route('admin.dashboard') }}"><i class="fa fa-home"></i> Admin Dashboard</a>
                    </li>
                    <li>
                        <a href="">Plan Administration</a>
                    </li>
                    <li>
                        <a href="">Plan Management</a>
                    </li>                    
                </ul>
            </div>
        </div>
        @include('layouts.backend.partials.messages')
        <!-- row -->
        <div class="row">
            <!-- col -->
            <div class="col-md-12">
                <!-- tile -->
                <section class="tile tile-simple">
                    <!-- tile body -->
                    <div class="tile-body p-0">
                        <div role="tabpanel">
                            <!-- Nav tabs -->
                            @include('admin.plan.partials.plansubnav')
                            <!-- Tab panes -->
                            <div class="row">                            
                                <div class="col-md-3 col-xs-12 col-sm-3" style="float:right;">
                                    <div class="form-group mb-0">
                                        <label class="control-label">&nbsp;</label>
                                        <a href="javascript:history.back(-1);" class="btn btn-large btn-primary btn-block projectsubmit" title="Back">Back</a>
                                    </div>
                                </div>
                            </div>                            
                            <div class="tab-content">
                                <div class="clearfix">&nbsp;</div>
                                <table class="table">
                                    <div class="col-md-8 col-md-offset-1">
                                        {!! Form::open(array('route'=>['admin.newplan.create'], 'method' => 'post')) !!}

                                        {!! Form::hidden('keyword_searches','true') !!}
                                        {!! Form::hidden('keyword_evaluate','true') !!}
                                        <div class="form-group row" id="chargebee_plans_list">
                                            {!! Form::label('research', 'Chargebee Plans', array('class' => 'col-sm-2')) !!}
                                            <div class="col-sm-4">
                                                <select id="chargebee_plan_id" name="chargebee_plan_id" class="form-control">
                                                    @foreach($chargebee_plan as $c_plan)
                                                    <option value="{{ $c_plan['id'] }}">{{ $c_plan['name'] ." (". $c_plan['period']."ly)"}}</option>
                                                @endforeach    
                                                </select>
                                            </div>
                                        </div>


                                        <div class="form-group row">
                                            {!! Form::label('research', 'Screen name', array('class' => 'col-sm-2')) !!}
                                            <div class="col-sm-4">
                                                {!! Form::text('screen_name','', array('id'=>'screen_name','class' => 'form-control')) !!}
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            {!! Form::label('research', 'Searches', array('class' => 'col-sm-2')) !!}
                                            <div class="col-sm-4">
                                                {!! Form::text('research','', array('class' => 'form-control')) !!}
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            {!! Form::label('evolution', 'Evaluation', array('class' => 'col-sm-2')) !!}
                                            <div class="col-sm-4">
                                                {!! Form::text('evolution','', array('class' => 'form-control' )) !!}
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-2">
                                                <div>
                                                    <label class="checkbox checkbox-custom-alt">
                                                        {!! Form::checkbox('keyword_save', 'true', '', ['id'=>'can_save']) !!}
                                                        <i></i>Can save keywords
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                {!! Form::text('can_save','', array('id'=>'can_save_text','class' => 'form-control')) !!}
                                            </div>
                                        </div>
                                        <div id="project-groupe" class="form-group row">
                                            {!! Form::label('project', 'Projects', array('class' => 'col-sm-2')) !!}
                                            <div class="col-sm-4">
                                                {!! Form::text('project', '', array('class' => 'form-control')) !!}
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-2">
                                                <div>
                                                    <label class="checkbox checkbox-custom-alt">
                                                        {!! Form::checkbox('domain_finder','true', '') !!}
                                                        <i></i>Domain finder
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-2">
                                                <div>
                                                    <label class="checkbox checkbox-custom-alt">
                                                        {!! Form::checkbox('export_data','true', '') !!} <i></i>Export
                                                       Data
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-2">
                                                <div>
                                                    <label class="checkbox checkbox-custom-alt">
                                                        {!! Form::checkbox('import_keywords', 'true','') !!}
                                                        <i></i>Import Keywords
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-2">
                                                <div>
                                                    <label class="checkbox checkbox-custom-alt">
                                                        {!! Form::checkbox('brainstorm_tool', 'true','', ['id'=>'brainstorm_tool']) !!}
                                                        <i></i>Brain storming tool
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                {!! Form::text('brainstorm','', array('class' => 'form-control','id'=>'brainstorm_tool_text')) !!}
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-2">
                                                <div>
                                                    <label class="checkbox checkbox-custom-alt">
                                                        {!! Form::checkbox('rank_checker',  'true', '', ['id'=>'rank_checker']) !!}
                                                        <i></i>Rank Checker
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                {!! Form::text('tracking','', array('class' => 'form-control','id'=>'rank_checker_text')) !!}
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            {!! Form::label('research', 'Plan Status', array('class' => 'col-sm-2')) !!}
                                            <div class="col-sm-4">
                                                <select id="plan_status" name="plan_status" class="form-control">
                                                    <option value="1">Active</option> 
                                                    <option value="0" selected>Inactive</option>  
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            {!! Form::label('research', 'Sorting Order', array('class' => 'col-sm-2')) !!}
                                            <div class="col-sm-4">
                                                <select id="sorting_order" name="sorting_order" class="form-control">
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-offset-1 col-sm-4">
                                                <button type="submit" class="btn btn-success btn-rounded">Add
                                                </button>
                                            </div>
                                        </div>
                                        {!! Form::close() !!}
                                    </div>
                                </table>
                            </div>
                            <!-- /Tab panes -->
                        </div>
                    </div>
                    <!-- /tile body -->
                </section>
                <!-- / tile -->
            </div>
            <!-- / col -->
        </div>
        <!-- / row -->
    </div>
@stop

@section('scripts')
    <script src="//cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
    <script src="//cdn.datatables.net/1.10.10/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(document).ready(function () {
            $("#can_save").load(function () {
                $("#can_save_text")[$(this).is(":checked") ? 'show' : 'hide']("fast");
                $("#project-groupe")[$(this).is(":checked") ? 'show' : 'hide']("fast");
            }).load();
            $("#brainstorm_tool").load(function () {
                $("#brainstorm_tool_text")[$(this).is(":checked") ? 'show' : 'hide']("fast")
            }).load()
            $("#rank_checker").load(function () {
                $("#rank_checker_text")[$(this).is(":checked") ? 'show' : 'hide']("fast")
            }).load()
        });
        $(document).ready(function () {
            $('#can_save').change(function () {
                if (this.checked) {
                    $('#can_save_text').show();
                    $("#project-groupe").show();
                }
                else {
                    $("#can_save_text").hide();
                    $("#project-groupe").hide();
                }

            });
            $('#brainstorm_tool').change(function () {
                if (this.checked)
                    $('#brainstorm_tool_text').show();
                else
                    $("#brainstorm_tool_text").hide();

            });
            $('#rank_checker').change(function () {
                if (this.checked)
                    $('#rank_checker_text').show();
                else
                    $("#rank_checker_text").hide();

            });
        });
    </script>
@stop
