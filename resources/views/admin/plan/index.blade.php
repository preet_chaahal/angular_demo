@extends('layouts.backend.master')

@section('style')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.10/css/dataTables.bootstrap.min.css">
@stop


@section('content')
    <div class="page page-dashboard">
        <div class="pageheader">
            <h2>Plan Administration <span>// Manage Plan Role</span></h2>
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <a href="{{ URL::route('admin.dashboard') }}"><i class="fa fa-home"></i> Admin Dashboard</a>
                    </li>
                    <li>
                        <a href="">Plan Administration</a>
                    </li>
                </ul>
            </div>
        </div>

        @include('layouts.backend.partials.messages')

        <!-- row -->
        <div class="row">
            <!-- col -->
            <div class="col-md-12">
                <!-- tile -->
                <section class="tile tile-simple">
                    <!-- tile body -->
                    <div class="tile-body p-0">
                        <div role="tabpanel">
                            <!-- Nav tabs -->
                            @include('admin.plan.partials.plansubnav')
                            <div class="row">                            
                                <div class="col-md-3 col-xs-12 col-sm-3" style="float:right;">
                                    <div class="form-group mb-0">
                                        <label class="control-label">&nbsp;</label>
                                        <a href="{{ URL::route('admin.newplan.add') }}" class="btn btn-large btn-primary btn-block projectsubmit" title="Add New Plan">Add New Plan</a>
                                    </div>
                                </div>
                            </div>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="clearfix">&nbsp;</div>
                                <table class="table">
                                    <div class="col-md-8 col-md-offset-1">
                                        {!! Form::open(array('route'=>['admin.plan.update', $role], 'method' => 'post')) !!}

                                        {!! Form::hidden('role',$role) !!}
                                        {!! Form::hidden('keyword_searches','true') !!}
                                        {!! Form::hidden('keyword_evaluate','true') !!}

                                        @if($roleName == 'trial')
                                        <div class="form-group row">
                                            <div class="col-sm-4">
                                                <div>
                                                    <label class="checkbox checkbox-custom-alt">
                                                        {!! Form::checkbox('is_trial',  1, $is_trial, ['id'=>'is_trial']) !!}
                                                        <i></i>Trial Account
                                                    </label>
                                                </div>
                                            </div>
                                        </div>    
                                        @endif
                                        <div class="form-group row" id="trial_days" style="display:none;">
                                            {!! Form::label('evolution', 'Trail days', array('class' => 'col-sm-2')) !!}
                                            <div class="col-sm-4">
                                                {!! Form::text('trial_days',$trial_days, array('class' => 'form-control' )) !!}
                                            </div>
                                        </div>
                                        @if($default_plan != 1)
                                        <div class="form-group row" id="chargebee_plans_list">
                                            {!! Form::label('research', 'Chargebee Plans', array('class' => 'col-sm-2')) !!}
                                            <div class="col-sm-4">
                                                <select id="chargebee_plan_id" name="chargebee_plan_id" class="form-control" disabled="true">
                                                    <?php $cbst = 0; ?>
                                                    @foreach($chargebee_plan as $c_plan)
                                                    <?php
                                                        if($chargebeeplan_id == $c_plan['id']) {
                                                            $cbst = 1;
                                                        }
                                                    ?>
                                                    <option @if($chargebeeplan_id == $c_plan['id'])  selected @endif value="{{ $c_plan['id'] }}">{{ $c_plan['name'] ." (". $c_plan['period']."ly)"}}</option>
                                                @endforeach
                                                @if($cbst == 0)
                                                    <option value="" selected>No Plan Exist</option>
                                                @endif
                                                </select>
                                            </div>
                                        </div>
                                        @endif

                                        <div class="form-group row">
                                            {!! Form::label('research', 'Screen name', array('class' => 'col-sm-2')) !!}
                                            <div class="col-sm-4">
                                                {!! Form::text('screen_name',$screen_name, array('id'=>'screen_name','class' => 'form-control')) !!}
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            {!! Form::label('research', 'Searches', array('class' => 'col-sm-2')) !!}
                                            <div class="col-sm-4">
                                                {!! Form::text('research',$research, array('class' => 'form-control')) !!}
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            {!! Form::label('evolution', 'Evaluation', array('class' => 'col-sm-2')) !!}
                                            <div class="col-sm-4">
                                                {!! Form::text('evolution',$evolution, array('class' => 'form-control' )) !!}
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-2">
                                                <div>
                                                    <label class="checkbox checkbox-custom-alt">
                                                        {!! Form::checkbox('keyword_save', 'true', $keyword_save, ['id'=>'can_save']) !!}
                                                        <i></i>Can save keywords
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                {!! Form::text('can_save',$can_save, array('id'=>'can_save_text','class' => 'form-control')) !!}
                                            </div>
                                        </div>
                                        <div id="project-groupe" class="form-group row">
                                            {!! Form::label('project', 'Projects', array('class' => 'col-sm-2')) !!}
                                            <div class="col-sm-4">
                                                {!! Form::text('project', $project, array('class' => 'form-control')) !!}
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-2">
                                                <div>
                                                    <label class="checkbox checkbox-custom-alt">
                                                        {!! Form::checkbox('domain_finder','true', $domain_finder) !!}
                                                        <i></i>Domain finder
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-2">
                                                <div>
                                                    <label class="checkbox checkbox-custom-alt">
                                                        {!! Form::checkbox('export_data','true', $export_data) !!} <i></i>Export
                                                       Data
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-2">
                                                <div>
                                                    <label class="checkbox checkbox-custom-alt">
                                                        {!! Form::checkbox('import_keywords', 'true',$import_keywords) !!}
                                                        <i></i>Import Keywords
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-2">
                                                <div>
                                                    <label class="checkbox checkbox-custom-alt">
                                                        {!! Form::checkbox('brainstorm_tool', 'true', $brainstorm_tool, ['id'=>'brainstorm_tool']) !!}
                                                        <i></i>Brain storming tool
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                {!! Form::text('brainstorm',$brainstorm, array('class' => 'form-control','id'=>'brainstorm_tool_text')) !!}
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-2">
                                                <div>
                                                    <label class="checkbox checkbox-custom-alt">
                                                        {!! Form::checkbox('rank_checker',  'true', $rank_checker, ['id'=>'rank_checker']) !!}
                                                        <i></i>Rank Checker
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                {!! Form::text('tracking',$tracking, array('class' => 'form-control','id'=>'rank_checker_text')) !!}
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            {!! Form::label('research', 'Plan Status', array('class' => 'col-sm-2')) !!}
                                            <div class="col-sm-4">
                                                <select id="plan_status" name="plan_status" class="form-control">
                                                    <option @if($status == 1)  selected @endif value="1">Active</option> 
                                                    <option @if($status == 0)  selected @endif value="0">Inactive</option>  
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            {!! Form::label('research', 'Sorting Order', array('class' => 'col-sm-2')) !!}
                                            <div class="col-sm-4">
                                                <select id="sorting_order" name="sorting_order" class="form-control">
                                                    <option @if($sorting_order == 1)  selected @endif value="1">1</option>
                                                    <option @if($sorting_order == 2)  selected @endif value="2">2</option>
                                                    <option @if($sorting_order == 3)  selected @endif value="3">3</option>
                                                    <option @if($sorting_order == 4)  selected @endif value="4">4</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-offset-1 col-sm-4">
                                                <button type="submit" class="btn btn-success btn-rounded">Update
                                                </button>
                                            </div>
                                        </div>
                                        {!! Form::close() !!}
                                    </div>
                                </table>
                            </div>
                            <!-- /Tab panes -->
                        </div>
                    </div>
                    <!-- /tile body -->
                </section>
                <!-- / tile -->
            </div>
            <!-- / col -->
        </div>
        <!-- / row -->
    </div>
@stop

@section('scripts')
    <script src="//cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
    <script src="//cdn.datatables.net/1.10.10/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(document).ready(function () {
            $("#can_save").load(function () {
                $("#can_save_text")[$(this).is(":checked") ? 'show' : 'hide']("fast");
                $("#project-groupe")[$(this).is(":checked") ? 'show' : 'hide']("fast");
            }).load();
            $("#brainstorm_tool").load(function () {
                $("#brainstorm_tool_text")[$(this).is(":checked") ? 'show' : 'hide']("fast")
            }).load()
            $("#rank_checker").load(function () {
                $("#rank_checker_text")[$(this).is(":checked") ? 'show' : 'hide']("fast")
            }).load()
        });
        $(document).ready(function () {
            $('#can_save').change(function () {
                if (this.checked) {
                    $('#can_save_text').show();
                    $("#project-groupe").show();
                }
                else {
                    $("#can_save_text").hide();
                    $("#project-groupe").hide();
                }

            });
            $('#brainstorm_tool').change(function () {
                if (this.checked)
                    $('#brainstorm_tool_text').show();
                else
                    $("#brainstorm_tool_text").hide();

            });
            $('#rank_checker').change(function () {
                if (this.checked)
                    $('#rank_checker_text').show();
                else
                    $("#rank_checker_text").hide();

            });
/*
            $('#is_trial').click(function(){
                if($(this).prop("checked") == true){
                    $("#trial_days").css("display","block");
                    $("#chargebee_plans_list").css("display","none");
                } else if($(this).prop("checked") == false) {
                    $("#trial_days").css("display","none");
                    $("#chargebee_plans_list").css("display","block");
                }
            });
*/            

            if($('#is_trial').prop("checked") == true){
                $("#trial_days").css("display","block");
                $("#chargebee_plans_list").css("display","none");
            } else if($(this).prop("checked") == false) {
                $("#trial_days").css("display","none");
                $("#chargebee_plans_list").css("display","block");
            }
       
        });
    </script>
@stop
