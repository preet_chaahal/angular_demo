<ul class="nav nav-tabs tabs-dark" role="tablist">
    @foreach($accounts as $account)
    	<?php
	    	$plancl = "plan-heigh-light";
	    	if($account->status == 0) {
	    		$plancl = "plan-inactive";
	    	}
    	?>
        <li class="{{ Request::is("admin/plan/{$account->name}") ? 'active' : $plancl }}">
            <a href="{{ URL::route('admin.plan', $account->name) }}">{{ $account->name }}</a>
        </li>
    @endforeach
    <li class="tabs-title pull-right" style="padding-right: 15px;">
        User<strong> Administration</strong>
    </li>
</ul>