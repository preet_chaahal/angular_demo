@extends('layouts.backend.master')

@section('style')
@stop


@section('content')
    <div class="page page-dashboard">
        <div class="pageheader">
            <h2>Registration Administration</h2>
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <a href="{{ URL::route('admin.dashboard') }}"><i class="fa fa-home"></i> Admin Dashboard</a>
                    </li>
                    <li>
                        <a href="{{ URL::route('admin.registration.management') }}">Registration Management</a>
                    </li>
                </ul>
            </div>
        </div>

        @include('layouts.backend.partials.messages')

                <!-- row -->
        <div class="row">
            <!-- col -->
            <div class="col-md-12">
                <!-- tile -->
                <section class="tile tile-simple">
                    <!-- tile body -->
                    <div class="tile-body p-0">
                        <div role="tabpanel">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs tabs-dark" role="tablist">
                                <li class="{{ Request::is('admin/registration-management') ? 'active' : '' }}"><a href="{{ URL::route('admin.registration.management') }}">List</a></li>
                                <li class="{{ Request::is('registration-management/toggle-registration') ? 'active' : '' }}"><a href="{{ URL::route('admin.registration.management.toggle') }}">Toggle Registration</a></li>
                                <li class="{{ Request::is('registration-management/disable/ip') ? 'active' : '' }}"><a href="{{ URL::route('admin.registration.management.disable.ip') }}">Disable IP</a></li>
                                <li class="{{ Request::is('registration-management/disable/email') ? 'active' : '' }}"><a href="{{ URL::route('admin.registration.management.disable.email') }}">Disable Email</a></li>
                                <li class="tabs-title pull-right" style="padding-right: 15px;">Registration<strong> Management</strong></li>
                            </ul>

                                    <!-- Tab panes -->
                            <div class="tile-body">

                                <div class="proxy-table">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>
                                                IP
                                            </th>
                                            <th>
                                                Email
                                            </th>
                                            <th>
                                                Controls
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($regs as $reg)
                                            <tr>
                                                <td>
                                                    @if ($reg->ip != null)
                                                    {{ $reg->ip }}
                                                    @else
                                                     <div>N/A</div>
                                                    @endif
                                                </td>
                                                <td>
                                                    @if ($reg->email_domain != null)
                                                        {{ $reg->email_domain }}
                                                    @else
                                                        <div>N/A</div>
                                                    @endif
                                                </td>
                                                <td>
                                                    <a href="{{ URL::route('admin.registration.management.disable.ip.delete', $reg->id) }}" onclick="return confirm('Are you sure you wish to delete this group?');">Delete</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /Tab panes -->
                        </div>
                    </div>
                    <!-- /tile body -->
                </section>
                <!-- / tile -->
            </div>
            <!-- / col -->
        </div>
        <!-- / row -->

    </div>

@stop


@section('scripts')
@stop