@extends('layouts.backend.master')

@section('style')
@stop


@section('content')
    <div class="page page-dashboard">
        <div class="pageheader">
            <h2>Registration Administration</h2>
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <a href="{{ URL::route('admin.dashboard') }}"><i class="fa fa-home"></i> Admin Dashboard</a>
                    </li>
                    <li>
                        <a href="{{ URL::route('admin.registration.management') }}">Registration Management</a>
                    </li>
                </ul>
            </div>
        </div>

        @include('layouts.backend.partials.messages')

                <!-- row -->
        <div class="row">
            <!-- col -->
            <div class="col-md-12">
                <!-- tile -->
                <section class="tile tile-simple">
                    <!-- tile body -->
                    <div class="tile-body p-0">
                        <div role="tabpanel">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs tabs-dark" role="tablist">
                                <li class="{{ Request::is('admin/registration-management') ? 'active' : '' }}"><a href="{{ URL::route('admin.registration.management') }}">List</a></li>
                                <li class="{{ Request::is('admin/registration-management/toggle-registration') ? 'active' : '' }}"><a href="{{ URL::route('admin.registration.management.toggle') }}">Toggle Registration</a></li>
                                <li class="{{ Request::is('registration-management/disable/ip') ? 'active' : '' }}"><a href="{{ URL::route('admin.registration.management.disable.ip') }}">Disable IP</a></li>
                                <li class="{{ Request::is('registration-management/disable/email') ? 'active' : '' }}"><a href="{{ URL::route('admin.registration.management.disable.email') }}">Disable Email</a></li>
                                <li class="tabs-title pull-right" style="padding-right: 15px;">Registration<strong> Management</strong></li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tile-body">
                                <form method="POST" action="{{ URL::route('admin.registration.management.toggle.post') }}">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                    <!--Disabled-->
                                    <div class="form-group">
                                        <label for="disabled">Disable Registration</label>
                                        @if ($reg->disabled)
                                            <select name="disabled" id="disabled" class="form-control">
                                                <option value="1" selected="selected">Yes</option>
                                                <option value="0">No</option>
                                            </select>
                                        @else
                                            <select name="disabled" id="disabled" class="form-control">
                                                <option value="1">Yes</option>
                                                <option value="0" selected="selected">No</option>
                                            </select>
                                        @endif
                                    </div>

                                    <!-- actions -->
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-8 col-md-offset-2">
                                                <input type="submit" class="btn btn-success btn-lg btn-block" value="Update User">
                                            </div>
                                        </div>
                                    </div> <!-- actions -->

                                </form>
                            </div>
                            <!-- /Tab panes -->
                        </div>
                    </div>
                    <!-- /tile body -->
                </section>
                <!-- / tile -->
            </div>
            <!-- / col -->
        </div>
        <!-- / row -->

    </div>

@stop


@section('scripts')
@stop