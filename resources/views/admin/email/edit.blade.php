@extends('layouts.backend.master')

@section('style')
@stop


@section('content')
    <div class="page page-dashboard">
        <div class="pageheader">
            <h2>Email Templates Admin <span>// Manage Email Templates</span></h2>
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <a href="{{ URL::route('admin.dashboard') }}"><i class="fa fa-home"></i> Admin Dashboard</a>
                    </li>
                    <li>
                        <a href="{{ URL::route('admin.email-template') }}">Email Templates Administration</a>
                    </li>
                </ul>
            </div>
        </div>

        @include('layouts.backend.partials.messages')

                <!-- row -->
        <div class="row">
            <!-- col -->
            <div class="col-md-12">
                <!-- tile -->
                <section class="tile tile-simple">
                    <!-- tile body -->
                    <div class="tile-body p-0">
                        <div role="tabpanel">

                            <ul class="nav nav-tabs tabs-dark" role="tablist">
                                <li class="{{ Request::is('admin/email-template') ? 'active' : '' }}"><a href="{{ URL::route('admin.email-template')  }}">Listing</a></li>
                                <li class="{{ Request::is('admin/email-template/edit/*') ? 'active' : '' }}"><a>Edit {{ucwords($email->name)}} Email Template</a></li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tile-body">

                                <form method="POST" action="{{ URL::route('admin.email.edit.post') }}">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" name="id" value="{{ $email->id }}">
                                    <!-- type -->
                                    <div class="form-group">
                                        {!! Form::label('Name') !!}<b style="color:red;padding: 0 0 0 4px">*</b>
                                        {!! Form::text('name',$email->name,['class'=>"form-control"]) !!}
                                    </div>
                                    <!-- type -->

                                    <!-- ip -->
                                    <div class="form-group">
                                        {!! Form::label('Subject') !!}<b style="color:red;padding: 0 0 0 4px">*</b>
                                        {!! Form::text('subject',$email->subject,['class'=>"form-control"]) !!}
                                    </div>
                                    <!-- ip -->

                                    <!-- port -->
                                    <div class="form-group">
                                        {!! Form::label('Body') !!}<b style="color:red;padding: 0 0 0 4px">*</b>
                                        {!! Form::textarea('body',$email->body,['class'=>"form-control"]) !!}
                                    </div>
                                    <!-- port -->

                                    <!-- actions -->
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-8 col-md-offset-2">
                                                <input type="submit" class="btn btn-success btn-lg btn-block" value="Update Email Template">
                                            </div>
                                        </div>
                                    </div> <!-- actions -->

                                </form>
                            </div>
                            <!-- /Tab panes -->
                        </div>
                    </div>
                    <!-- /tile body -->
                </section>
                <!-- / tile -->
            </div>
            <!-- / col -->
        </div>
        <!-- / row -->

    </div>

@stop


@section('scripts')
@stop