@extends('layouts.backend.master')

@section('style')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.10/css/dataTables.bootstrap.min.css">
@stop


@section('content')
    <div class="page page-dashboard">
        <div class="pageheader">
            <h2>Email Templates Administration <span>// Manage Email Templates</span></h2>
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <a href="{{ URL::route('admin.dashboard') }}"><i class="fa fa-home"></i> Admin Dashboard</a>
                    </li>
                    <li>
                        <a href="{{ URL::route('admin.email-template') }}">Email Templates Administration</a>
                    </li>
                </ul>
            </div>
        </div>

        @include('layouts.backend.partials.messages')

                <!-- row -->
        <div class="row">
            <!-- col -->
            <div class="col-md-12">
                <!-- tile -->
                <section class="tile tile-simple">
                    <!-- tile body -->
                    <div class="tile-body p-0">
                        <div role="tabpanel">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs tabs-dark" role="tablist">
                                <li class="{{ Request::is('admin/email-template') ? 'active' : '' }}"><a>Listing</a></li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="col-md-12" style="margin-top: 10px">
                                    <div class="pull-right">
                                        <a href="{{ URL::route('admin.email.new') }}" class="btn btn-success">Add
                                            New Template</a>
                                    </div>
                                </div>
                                <table class="table" id="saveTbl">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Subject</th>
                                        <th>Action</th>
                                    </tr>
                                    @foreach($emails as $email)
                                        <tr>
                                            <td>
                                                {{ isset($email->name)?strtoupper($email->name):'' }}
                                            </td>
                                            <td>
                                                {{ isset($email->subject)?$email->subject:'' }}
                                            </td>
                                            <td>
                                                <a href="{{ URL::route('admin.email.edit', $email->id) }}">Edit</a> |
                                                <a href="{{ URL::route('admin.email.delete', $email->id) }}"
                                                   onclick="return confirm('Are you sure you wish to delete this Template?');">Delete</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                            <!-- /Tab panes -->
                        </div>
                    </div>
                    <!-- /tile body -->
                </section>
                <!-- / tile -->
            </div>
            <!-- / col -->
        </div>
        <!-- / row -->

    </div>

@stop
@section('scripts')
    <script src="//cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
    <script src="//cdn.datatables.net/1.10.10/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(document).ready(function () {

            $('#saveTbl').DataTable({
                "paging": false,
                "info": false,
                "searching": false
                columnDefs: [
                    {type: 'numeric-comma', targets: 0},
                    {"bSortable": false, targets: 7}
                ]
            })
        });
    </script>
@stop