<ul class="nav nav-tabs tabs-dark" role="tablist">
    <li class="{{ Request::is('admin/users') ? 'active' : '' }}"><a href="{{ URL::route('admin.users') }}">Listing</a></li>
    <li class="{{ Request::is('admin/users/create/*') ? 'active' : '' }}"><a href="{{ URL::route('admin.users.add') }}">Add User</a></li>
    <li class="tabs-title pull-right" style="padding-right: 15px;">User<strong> Administration</strong></li>
</ul>