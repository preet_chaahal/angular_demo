@extends('layouts.backend.master')

@section('style')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.10/css/dataTables.bootstrap.min.css">
@stop


@section('content')
<div class="page page-dashboard">
        <div class="pageheader">
            <h2>User Administration <span>// Manage users</span></h2>
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <a href="{{ URL::route('admin.dashboard') }}"><i class="fa fa-home"></i> Admin Dashboard</a>
                    </li>
                    <li>
                        <a href="{{ URL::route('admin.users') }}">User Administration</a>
                    </li>
                </ul>
            </div>
        </div>

        @include('layouts.backend.partials.messages')

        <!-- row -->
        <div class="row">
            <!-- col -->
            <div class="col-md-12">
                <!-- tile -->
                <section class="tile tile-simple">
                    <!-- tile body -->
                    <div class="tile-body p-0">
                        <div role="tabpanel">
                            <!-- Nav tabs -->
                            @include('admin.user.partials.adminsubnav')

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <table class="table" id="user-table">
                                <thead>
                                    <tr>
                                        <th>
                                            Name
                                        </th>
                                        <th>
                                            Email
                                        </th>
                                        <th>
                                            Username
                                        </th>
                                        <th>
                                            Plan Type
                                        </th>
                                        <th>
                                            Payment Type
                                        </th>
                                        <th>
                                            Payment Sys. ID
                                        </th>
                                        <th>
                                            Subscription ID
                                        </th>
                                        <th>
                                            Start Date
                                        </th>
                                        <th>
                                            Expiry Date
                                        </th>                                                                                
                                        <th>
                                            Admin
                                        </th>
                                        <th>
                                            Disabled
                                        </th>
                                        <th>
                                            Manually Upgraded
                                        </th>
                                        <th>
                                            Created
                                        </th>
                                        <th>
                                            Controls
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($users as $user)
                                        <tr>
                                            <td>
                                                {{ $user->name }}
                                            </td>
                                            <td>
                                                {{ $user->email }}
                                            </td>
                                            <td>
                                                {{ $user->username }}
                                            </td>
                                            <td>
                                                {{ substr($plans[$user->account_id], 17) }}
                                            </td>
                                            <td>
                                                @if ($user->payment_type != '')
                                                    {{$user->payment_type}}
                                                @else
                                                    N/A
                                                @endif
                                            </td>
                                            <td>
                                                @if ($user->payment_type == 'chargebee')
                                                    {{ $user->chargebee_subscription_id }}
                                                @else
                                                    {{ $user->stripe_id }}
                                                @endif
                                            </td>
                                            <td>
                                                @if ($user->payment_type == 'chargebee')
                                                    {{ $user->chargebee_subscription_id }}
                                                @else
                                                    {{$user->stripe_subscription}}
                                                @endif        
                                            </td>
                                            <td>
                                            {{$user->chargebee_subscription_start_date}}
                                            </td>
                                            <td>
                                            {{$user->chargebee_subscription_end_date}}
                                            </td>
                                            <td>
                                                {{ $user->is_admin ? 'Yes' : 'No' }}
                                            </td>
                                            <td>
                                                {{ $user->disabled ? 'Yes' : 'No' }}
                                            </td>
                                            <td>
                                                {{ $user->toggle_free ? 'Yes' : 'No' }}
                                            </td>
                                            <td>
                                                {{ $user->created_at->format('m/d/Y') }}
                                            </td>
                                            <td>
                                                <a href="{{ URL::route('admin.users.edit', $user->id) }}">Edit</a> | <a href="{{ URL::route('admin.users.delete', $user->id) }}" onclick="return confirm('Are you sure you wish to delete this group?');">Delete</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            </div>
                            <!-- /Tab panes -->
                        </div>
                    </div>
                    <!-- /tile body -->
                </section>
                <!-- / tile -->
            </div>
            <!-- / col -->
        </div>
        <!-- / row -->

    </div>

@stop


@section('scripts')
    <script src="//cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
    <script src="//cdn.datatables.net/1.10.10/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(document).ready(function(){
            $('#user-table').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": "{{ URL::route('admin.users.get') }}",
                "deferLoading": {{$deferLoading}}
            });

        });
    </script>
@stop
