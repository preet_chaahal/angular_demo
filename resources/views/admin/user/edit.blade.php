@extends('layouts.backend.master')

@section('style')
@stop


@section('content')
<div class="page page-dashboard">
        <div class="pageheader">
            <h2>User Administration <span>// Manage saved users</span></h2>
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <a href="{{ URL::route('admin.dashboard') }}"><i class="fa fa-home"></i> Admin Dashboard</a>
                    </li>
                    <li>
                        <a href="{{ URL::route('admin.users') }}">User Administration</a>
                    </li>
                </ul>
            </div>
        </div>

        @include('layouts.backend.partials.messages')

        <!-- row -->
        <div class="row">
            <!-- col -->
            <div class="col-md-12">
                <!-- tile -->
                <section class="tile tile-simple">
                    <!-- tile body -->
                    <div class="tile-body p-0">
                        <div role="tabpanel">


                            <!-- Tab panes -->
                            <div class="tile-body">

                                <form method="POST" action="{{ URL::route('admin.users.update') }}">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" name="id" value="{{ $user->id }}">



                                    <!-- name -->
                                    <div class="form-group">
                                        <label for="name">Name</label>
                                        <input type="text" class="form-control" id="name" name="name" value="{{ old('name', $user->name) }}">
                                    </div>
                                    <!-- name -->

                                    <!-- username -->
                                    <div class="form-group">
                                        <label for="username">Username</label>
                                        <input type="text" class="form-control" id="username" name="username" value="{{ old('username', $user->username) }}">
                                    </div>
                                    <!-- username -->

                                    <!-- username -->
                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input type="text" class="form-control" id="email" name="email" value="{{ old('email', $user->email) }}">
                                    </div>
                                    <!-- username -->

                                    <!-- password -->
                                    <div class="form-group">
                                        <label for="password">New Password</label>
                                        <input type="text" class="form-control" id="password" name="password" value="">
                                    </div>
                                    <!-- password -->

                                    <!--admin-->
                                    <div class="form-group">
                                        <label for="togglefree">Manual Upgrade</label>
                                        @if ($user->toggle_free == 0)
                                        <select name="togglefree" id="togglefree" class="form-control">
                                            <option value="0" selected="selected">No</option>
                                            <option value="1">Yes</option>
                                        </select>
                                        @else
                                        <select name="togglefree" id="togglefree" class="form-control">
                                            <option value="0">No</option>
                                            <option value="1" selected="selected">Yes</option>
                                        </select>
                                        @endif
                                    </div>

                                    <!--toggle free-->
                                    <div class="form-group">
                                        <label for="admin">Admin</label>
                                        @if ($user->is_admin == 0)
                                            <select name="admin" id="admin" class="form-control">
                                                <option value="0" selected="selected">No</option>
                                                <option value="1">Yes</option>
                                            </select>
                                        @else
                                            <select name="admin" id="admin" class="form-control">
                                                <option value="0">No</option>
                                                <option value="1" selected="selected">Yes</option>
                                            </select>
                                        @endif
                                    </div>

                                    <!--account-->
                                    <div class="form-group">
                                        <label for="account">Account</label>
                                        <select name="account" id="account" class="form-control">
                                            @foreach(App\Account::where('name', 'trial')->orWhere('name', 'free')->get() as $account)
                                                @if($user->account_id == $account->id)
                                                    <option value="{{ $account->id }}" selected="selected">{{ $account->description }}</option>
                                                @else
                                                    <option value="{{ $account->id }}">{{ $account->description }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>

                                    <!--Disabled-->
                                    <div class="form-group">
                                        <label for="disabled">Disabled</label>
                                        @if ($user->disabled)
                                        <select name="disabled" id="disabled" class="form-control">
                                            <option value="1" selected="selected">Yes</option>
                                            <option value="0">No</option>
                                        </select>
                                        @else
                                            <select name="disabled" id="disabled" class="form-control">
                                                <option value="1">Yes</option>
                                                <option value="0" selected="selected">No</option>
                                            </select>
                                        @endif
                                    </div>

                                    <!-- actions -->
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-8 col-md-offset-2">
                                                <input type="submit" class="btn btn-success btn-lg btn-block" value="Update User">
                                            </div>
                                        </div>
                                    </div> <!-- actions -->

                                </form>
                            </div>
                            <!-- /Tab panes -->
                        </div>
                    </div>
                    <!-- /tile body -->
                </section>
                <!-- / tile -->
            </div>
            <!-- / col -->
        </div>
        <!-- / row -->

    </div>

@stop


@section('scripts')
@stop