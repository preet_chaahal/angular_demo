@extends('layouts.front.master')

@section('content')
    @include('front.pages.sections.navigation')
<div id="wrap">
    <div id="main" style="margin-top: 50px;">
        <div class="container">
            <div class="row">

                <div class="col-md-12">
                    <form class="login-form" action="{{ URL::route('password.reset') }}" method="post">
                        {!! csrf_field() !!}
                        <input type="hidden" name="token" value="{{ $token }}">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Password Reset</h3>
                            </div>
                            <div class="panel-body">
                                @include('layouts.partials.messages')

                                <div class="form-group">
                                    <label class="control-label">Email Address</label>
                                    <input type="email" name="email" value="{{ old('email') }}" class="form-control form-control-solid placeholder-no-fix" />
                                </div>
                                <div class="form-group">
                                    <label class="control-label">New Password</label>
                                    <input type="password" name="password" class="form-control form-control-solid placeholder-no-fix" />
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Confirm Password</label>
                                    <input type="password" name="password_confirmation" class="form-control form-control-solid placeholder-no-fix" />
                                </div>
                            </div>
                            <div class="panel-footer">
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <button type="submit" id="forget-password" class="btn btn-primary btn-block uppercase">Reset Password</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop