@extends('layouts.front.master')

@section('content')
    @include('front.pages.new_sections.navigation')
	<div class="sign_up_main recovery">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-2"></div>
            <div class="col-md-6 col-sm-8">
				
					 @include('layouts.partials.messages')
					
                <form class="login-form form-custom" action="{{ URL::route('password.email') }}" method="post">
				  <h3 class="panel-title">Password Recovery</h3>
					<p>
						No worries! Link on resetting your password will be sent to your email. 
					</p>
                    {!! csrf_field() !!}
                   
                    <b>Email or user name</b> <i class="fa fa-envelope-o"></i>
					<input type="email" name="email" value="{{ old('email') }}" class="form-control box_1" placeholder="Type Your Email"/>             
                    <center>
					 <button type="submit" id="forget-password" class="create_btn">Reset Password</button>
					</center>
                </form><!-- form end here -->
			
                <p><a href="{{ URL::route('auth.login') }}" class="">Cancel</a></p>
            </div>
            <!-- col end here -->
        </div>
        <!-- row end here -->
    </div>
    <!-- container end here -->
</div>
<!-- sign_up_main end here -->

@stop