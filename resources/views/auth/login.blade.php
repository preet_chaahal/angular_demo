@extends('layouts.front.master')

@section('content')
    @include('front.pages.new_sections.navigation')
    <div class="sign_up_main">
        <div class="sign-in">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-2"></div>

                    <div class="col-md-6 col-sm-8">
                        <img src="{{ asset('assets/frontend/new_html/images/logo_2.png') }}" class="img-responsive">
                        <h3>SIGN IN</h3>

                        @include('layouts.partials.messages')
                        <form action="{{ URL::route('auth.login') }}" method="POST">
                            {!! csrf_field() !!}

                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <!-- Post Affiliate Pro START -->

                            <input type="hidden" name="pap_cookie" id="pap_cookie" value="">

                            {{--<script type="text/javascript" id="pap_x2s6df8d" src="https://keywordrevealer.postaffiliatepro.com/scripts/vdzfn3n4jm8"></script>--}}

                            {{--<script type="text/javascript">--}}

                            {{--PostAffTracker.setAccountId('default1');--}}

                            {{--try {--}}

                            {{--PostAffTracker.track();--}}

                            {{--PostAffTracker.writeCookieToCustomField('pap_cookie');--}}

                            {{--} catch (err) { }--}}

                            {{--</script>--}}

                        <!-- Post Affiliate Pro END -->
                            {{--<p>Attempt Count {{\Illuminate\Support\Facades\Session::get('loginAttempts')}}</p>--}}
                            {{--<p>Attempt Time {{\Illuminate\Support\Facades\Session::get('loginAttemptTime')}}</p>--}}
                            {{--<p>Attempt End Time {{\Carbon\Carbon::now()->diffForHumans(\Illuminate\Support\Facades\Session::get('loginAttemptTime'))}}</p>--}}
                            <p>Email</p>
                            <i class="fa fa-envelope-o"></i>
                            <input type="username" name="username" value="{{ old('username') }}"
                                   class="form-control box_1" placeholder="Type your E-mail">


                            <p>Password</p>
                            <i class="fa fa-lock"></i>
                            <input type="password" name="password" id="password" class="form-control box_1"
                                   placeholder="Password">

                            <center>
                                @if(\Illuminate\Support\Facades\Session::get('loginAttempts') >= 2 &&  \Carbon\Carbon::now()->diffForHumans(\Illuminate\Support\Facades\Session::get('loginAttemptTime')) <= '24 hour after')
                                    {!! app('captcha')->display() !!}
                                @else

                                @endif
                                <div class="form-group">
                                    <label for="remember">
                                        <input type="checkbox" name="remember" id="remember" value="1">Remember Me
                                    </label>
                                </div>
                                <div class="forgot-pass">

                                    <a href="{{ URL::route('password.email') }}" class="">Forgot your password?</a>
                                </div>
                                <input type="submit" class="create_btn" value="Sign In"/>

                            </center>
                        </form><!-- form end here -->


                        <p>Don't have an account? <a href="{{ URL::route('subscription.free') }}?fp_ref=testpl80">Sign UP</a></p>
                    </div><!-- col end here -->
                </div><!-- row end here -->
            </div><!-- container end here -->
        </div>
    </div><!-- sign_up_main end here -->
@stop