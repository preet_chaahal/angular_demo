
<div class="card-box" style="border: none">
    <div class="panel-group" id="accordion">
        <div class="panel panel-default">
            <div class="row">
                <div class=" serching-results col-xs-3">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" class="collapsed btn btn-primary">
                                Filter
                            </a>
                        </h4>
                    </div>
                </div>
                <div class="col-xs-9 pull-right pl-0">
                    <!-- <a href=" URL::route('backend.tools.keywordresearch.downloadPreview', $tmp_id) "
                        class="btn btn-default waves-effect waves-light pull-right ml-20">Download CSV
                        <span class="btn-label btn-label-right"><i class="fa fa-file-excel-o"></i></span>
                    </a> -->
                    <a href="#" data-toggle="modal" data-target=".confirm-modal" class="btn btn-default waves-effect waves-light pull-right ml-20">
                        Download CSV
                        <span class="btn-label btn-label-right">
                            <i class="fa fa-file-excel-o"></i>
                        </span>
                    </a>

                    <button class="btn btn-default pull-right ml-20" style="min-width:125px;" ng-if="userCanFindDomain" ng-disabled="checkDomains" ng-click="checkAllDomains(filteredResearchData)">
                        <span ng-if="!checkDomains">Check Domains</span>
                        <img src="/assets/backend/images/loading-icon.gif" ng-if="checkDomains">
                    </button>

                    <button ng-if="userCanSaveKeyword" ng-disabled="isSaved || search.savingProject" ng-style="search.savingProject?{'width':'105px'}:{}" class="btn btn-default pull-right" ng-click="saveProject();">
                        <span ng-if="!search.savingProject">Save Project</span>
                        <img src="/assets/backend/images/loading-icon.gif" ng-if="search.savingProject">
                    </button>

                    <!--<span class="btn btn-default pull-right" ng-click="saveKeywordFilter(researchData,keyword,tmp_id,id,language)">
                        <span>Save</span>
                        <img src="/assets/backend/images/loading-icon.gif" ng-if="search.savingKeyword">
                    </span>-->
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div id="collapseOne" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="row">

                                <div class="col-md-4">
                                    <div class="row">
                                        <div class="col-md-11">
                                            <label class="control-label">Keyword Search</label>
                                            <div class="form-group">
                                                <input type="text" ng-model="filterSearch.keyword" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-11">
                                            <div class="form-group">
                                                <label class="control-label">Keyword Exclude</label>
                                                <input type="text" ng-model="filterSearch.exclude" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <label class="control-label">Number Of Words</label>
                                    <div class="row">
                                        <div class="col-xs-5">
                                            <div class="form-group">
                                                <input type="number" ng-model="filterSearch.minWord" class="form-control" placeholder="min">
                                            </div>
                                        </div>
                                        <div class="col-md-1 col-xs-2 text-center">-</div>
                                        <div class="col-xs-5">
                                            <div class="form-group">
                                                <input type="number" ng-model="filterSearch.maxWord" class="form-control" placeholder="max">
                                            </div>
                                        </div>
                                    </div>

                                    <label class="control-label">Cost per Click</label>
                                    <div class="row">
                                        <div class="col-xs-5">
                                            <div class="form-group">
                                                <input type="number" ng-model="filterSearch.minCost" class="form-control" placeholder="min">
                                            </div>
                                        </div>
                                        <div class="col-md-1 col-xs-2 text-center">-</div>
                                        <div class="col-xs-5">
                                            <div class="form-group">
                                                <input type="number" ng-model="filterSearch.maxCost" class="form-control" placeholder="max">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <label class="control-label">Average Monthly Searches</label>
                                    <div class="row">
                                        <div class="col-xs-5">
                                            <div class="form-group">
                                                <input type="number" ng-model="filterSearch.minAverage" class="form-control" placeholder="min">
                                            </div>
                                        </div>
                                        <div class="col-md-1 col-xs-2 text-center">-</div>
                                        <div class="col-xs-5">
                                            <div class="form-group">
                                                <input type="number" ng-model="filterSearch.maxAverage" class="form-control" placeholder="max">
                                            </div>
                                        </div>
                                    </div>

                                    <label class="control-label">Estimated Potential Earning</label>
                                    <div class="row">
                                        <div class="col-xs-5">
                                            <div class="form-group">
                                                <input type="text" ng-model="filterSearch.minEstimated" class="form-control" placeholder="min">
                                            </div>
                                        </div>
                                        <div class="col-md-1 col-xs-2 text-center">-</div>
                                        <div class="col-xs-5">
                                            <div class="form-group">
                                                <input type="text" ng-model="filterSearch.maxEstimated" class="form-control" placeholder="max">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="modal">
        <div class="loader-panel panel-default" style="border-radius:5px;">
            <div class="model-header text-left">
                <h4 class="loader-panel-text">Processing...</h4>
            </div>
            <hr style="margin-top:0px;margin-bottom:20px;">
            <div class="panel-body">
                <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:100%;margin-top:-20px;">
                    &nbsp;
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-12dom-horizontal-class">
        <div>
            <div class="dataTables_length" >
                <label>
                    <select aria-controls="researchTable" class="form-control input-sm" ng-model="pagination.amount" ng-options="amount for amount in pagination.options"></select>
                </label>
                <span class="pull-left" style="margin:5px 0 0 15px">
                    Showing 1 to [[pagination.amount]] of [[filteredItemsLength]] entries
                    <span ng-if="filteredItemsLength < researchData.length"> ( filtered from [[researchData.length]] total entries )</span>
                </span>
            </div>
        </div>
        <div class="dom-left-margin">
            <div class="dataTables_info" id="researchTable_info" role="status" aria-live="polite"></div>
        </div>
    </div>

    <div class="table-responsive">
        <table class="table dataTable" id="researchTable">
            <thead>
            <tr>
                <th class="sorting" tooltip-append-to-body="true" rowspan="1" colspan="1" style="width:304px" uib-tooltip="Generated Keyword Ideas" order-table="keyword">Keyword Ideas</th>
                <th class="text-center sorting" tooltip-append-to-body="true" style="width: 150px;min-width: 150px" uib-tooltip="Monthly Searches" order-table="searches">Monthly Searches</th>
                <th class="text-center sorting" tooltip-append-to-body="true" style="width: 150px;min-width: 150px" uib-tooltip="The Approximate Cost Per Click"  order-table="cpc">Cost Per Click</th>
                <th class="text-center sorting" tooltip-append-to-body="true" uib-tooltip="Potential Monthly Earning from Ads Clicks" order-table="profit" style="width: 150px">Profit</th>
                <th class="text-center sorting" style="width: 150px" tooltip-append-to-body="true" uib-tooltip="Number of Words" order-table="word_count">Words</th>
                <th class="text-center" tooltip-append-to-body="true" uib-tooltip="Exact Match Domain Availability" ng-if="userCanFindDomain" style="width: 150px;min-width: 120px">Domain</th>
                <th class="text-center sorting" style="width: 150px" tooltip-append-to-body="true" uib-tooltip="Calculate the Ranking Difficulty and Opportunities, the Lower the Better" order-table="difficulty">Difficulty</th>
            </tr>
            </thead>
            <tbody>
                <!-- | filter : customFilterAverage | filter : customFilterExclude | filter : customFilterCost | filter : customFilterEstimated  -->
                <tr ng-repeat="(k,x) in researchData {{--| ilterme : this --}} | filter : {keyword: filterSearch.keyword} | filter : customFilterForSearch('Word') | filter : customFilterForSearch('Average') | filter : customFilterForSearch('Estimated') | filter : customFilterForSearch('Cost') | filter : customFilterExclude | orderBy : filterSearch.orderBy : filterSearch.orderReverse | {{--limitTo: pagination.amount : (pagination.page-1)*pagination.amount --}} paginationfilter : this as filteredResearchData track by $index">
                    <td ng-style="x.keyword == keyword ? {'color':'#428bca'}:{}" style="width:304px;min-width:304px">[[ x.keyword ]]</td>
                    <td class="text-center" style="width: 150px;min-width: 150px">[[ handleComma(x.searches) ]]</td>
                    <td class="text-center" style="width: 150px;min-width: 150px">$[[ handleComma(x.cpc,2) ]]</td>
                    <td class="text-center" style="width: 150px">$[[ handleComma(x.profit,2) ]]</td>
                    <td class="text-center" style="width: 150px">[[ x.word_count ]]</td>
                    <td ng-if="userCanFindDomain && ! x.domains" style="width: 150px" class="text-center domain" ng-click="getDomain(x)">
                        {{--
                        <span ng-disabled="x.findDomain" style="width:50px" class="btn btn-primary">
                            <span ng-if="!x.findDomain">Get</span>
                        </span>
                        --}}
                        <img src="/assets/backend/images/loading-icon.gif" ng-if="x.findDomain">

                    </td>
                    <td ng-if="userCanFindDomain && x.domains" class="text-center domain" style="width:130px;">
                        <span ng-repeat="d in ['com','net','org']">
                            <span ng-if="!x.domains[d] && !x.domains.flag" class="label label-danger domain-margin no-animate">.[[d]]</span>

                            <a ng-if="x.domains[d] && !x.domains.flag" class="no-animate" href="https://www.namecheap.com/domains/registration/results.aspx?domain=[[x.keyword]].[[d]]" target="_blank"><span class="label label-success domain-margin">.[[d]]</span></a>

                            <a ng-if="x.domains.flag" class="no-animate" href="https://www.namecheap.com/domains/registration/results.aspx?domain=[[x.keyword]].[[d]]" target="_blank"><span class="label label-success domain-margin" style="background-color: #08c!important;">.[[d]]</span></a>
                        </span>
                    </td>

                    <td class="text-center" style="color:#1693A5;font-size:20px;">

                        <span style="width: 95px;" class="btn btn-primary" ng-if="!x.difficulty && !x.reEvaluate" ng-disabled="x.evaluating" ng-click="evaluate(x)">
                            <span ng-if="!x.evaluating">Evaluate <i class="icon-action-redo"></i></span>
                            <img src="/assets/backend/images/loading-icon.gif" ng-if="x.evaluating">
                        </span>

                        <span style="cursor:pointer;" ng-if="x.difficulty && !x.reEvaluate" ng-click="$ctrl.open(x,$index,true)">[[x.difficulty]]</span>

                        <img src="/assets/backend/images/loading-icon.gif" ng-if="x.reEvaluate">

                    </td>
                </tr>
            </tbody>
        </table>
    </div>

    <div class="pb-50">
        <ul uib-pagination total-items="filteredItemsLength" ng-model="pagination.page" max-size="7" items-per-page="pagination.amount" class="pagination-sm pull-right" boundary-link-numbers="true"></ul>
    </div>
</div>
