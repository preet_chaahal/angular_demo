
<div class="row">
    <div class="col-md-4 col-sm-12">
        <div class="card-box" style="height: 329px;position:relative;">
            <div style="position:absolute;top:150px;left:45%;font-size:30px;color:#888">
                [[e.difficulty]]%
            </div>
            <h4 class="m-t-0 header-title">Keyword Difficulty</h4>
            <div class="tile-body text-center">
                <h4><span>[[difficultyText(e.difficulty)]]</span></h4>
                <div easypiechart options="easypiechartOptions" percent="e.difficulty"></div>
                <h4> [[e.keyword]] </h4>
            </div>
        </div>
    </div>

    <div class="col-md-8 col-sm-12">
        <div class="card-box" style="height:329px; overflow:hidden;">
            <h4 class="m-t-0 header-title">
                Keyword Search Trend

                <span ng-if="e.userCanSaveKeyword" ng-disabled="e.savingKeyword || e.savedKeyword || e.re_evaluate_keyword" style="width:85px;position:relative;z-index:50" class="btn btn-default pull-right" ng-if="e.userCanSaveKeyword" ng-click="saveKeyword(e)">
                        <span ng-if="!e.savingKeyword">Save</span>
                        <img src="/assets/backend/images/loading-icon.gif" ng-if="e.savingKeyword">
                </span>
                
            </h4>

            <div>
                <span style="display: inline-block;width:10px;height:10px;background:rgb(33, 150, 243);border-radius:50%"></span>
                [[e.keyword]]
            </div>
            <div style="position:absolute; background:#fff; height:260px; width:20px; bottom:45px; right:16px;"></div>

            <div style="overflow: hidden;height:329px">
                <iframe ng-src="[[e.googleTrends]]"
                        scrolling="no" style="border:none; margin:0;position:relative;top:-110px;margin-left:-2%" height="360" width="105%">
                </iframe>
            </div>

            <div style="position:relative; background:#fff; height:20px; width:120%; bottom:45px;z-index: -1"></div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-4">
        <div class="card-box pb-10">
            <h4 class="text-dark  header-title m-t-0">Social Presence</h4>
            <div class="widget-chart text-center">
                <div style="min-height: 275px">
                    <canvas id="bar"  style="margin: 0;max-height: 275px" class="chart chart-bar" chart-data="e.graphResults.socialChartData" chart-labels="socialPresence.lables" chart-options="socialPresence.options" chart-series="socialPresence.series" chart-colors="socialPresence.colors">
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="card-box pb-10">
            <h4 class="text-dark header-title m-t-0">Domain Strength</h4>
            <div class="widget-chart text-center">
                <div>
                    <canvas style="min-height: 275px;" id="base" class="chart-horizontal-bar" chart-data="e.graphResults.domainStrengthData" chart-labels="domainStrength.lables" chart-options="domainStrength.options" chart-series="domainStrength.series" chart-colors="domainStrength.colors">
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="card-box">
            <div ng-repeat="bar in googleGraphResults track by $index">
                <p class="font-600">[[bar.name]]
                    <span class="text-[[bar.class]] pull-right">
                        [[e.graphResults[bar.key].positive * 10]]%
                    </span>
                </p>
                <div class="progress progress-lg m-b-20">
                    <div class="progress-bar progress-bar-[[bar.class]] progress-animated wow animated" role="progressbar" aria-valuenow="[[e.graphResults[bar.key].positive * 10]]" aria-valuemin="0" aria-valuemax="100" ng-style="{'width' : '[[(e.graphResults[bar.key].positive * 10)]]%' }">
                        <span>Yes</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="table-responsive mb-20 p-20" style="background: #fff">
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>Keyword</th>
            <th class="text-center">Monthly Searches</th>
            <th class="text-center">Cost Per Click</th>
            <th class="text-center">Profit</th>
            <th class="text-center">Words</th>
            <th class="text-center">Location</th>
            <th class="text-center">Difficulty</th>
            {{--
            <th class="text-center" ng-if="e.userCanSaveKeyword">Action</th>
            --}}
        </tr>
        </thead>
        <tbody>
            <tr>
                <td style="background:#fff">[[ e.keyword ]]</td>
                <td class="text-center" style="background:#fff">[[ handleComma(e.searches) ]]</td>
                <td class="text-center" style="background:#fff">$[[ handleComma(e.cpc,2) ]]</td>
                <td class="text-center" style="background:#fff">$[[ handleComma(e.profit,2) ]]</td>
                <td class="text-center" style="background:#fff">[[ e.word_count ]]</td>
                <td class="text-center" style="background:#fff">[[ e.location ]]</td>
                <td class="text-center" style="color:#1693A5;font-size:20px;background:#fff">     
                    <span>[[e.difficulty]]</span>
                </td>
                {{--
                <td class="text-center" style="background:#fff" ng-if="e.userCanSaveKeyword">
                    <span ng-disabled="e.savingKeyword || e.savedKeyword" style="width:85px;" class="btn btn-default" ng-click="saveKeyword(e)">
                        <span ng-if="!e.savingKeyword">Save</span>
                        <img src="/assets/backend/images/loading-icon.gif" ng-if="e.savingKeyword">
                    </span>
                </td>
                --}}
            </tr>
        </tbody>
    </table>
</div>

<div class="table-responsive p-20" style="background:#fff;">
    <table id="rankingTable" class="table table-bordered">
        <thead>
        <tr>
            <th class="text-center">#</th>
            <th class="text-left" tooltip-append-to-body="true" uib-tooltip="Top Ten URLS on Google Search Engine Results">Google Top Ten Ranking</th>
            <th class="text-center" data-multiple="110" tooltip-append-to-body="true" uib-tooltip="Moz Page Authority">Page Aut..</th>
            <th class="text-center" data-multiple="130" tooltip-append-to-body="true" uib-tooltip="Moz Domain Authority">Domain Aut..</th>
            <th class="text-center" data-multiple="110" tooltip-append-to-body="true" uib-tooltip="External Links">External Li..</th>
            <th class="text-center" tooltip-append-to-body="true" uib-tooltip="MozRank">MozRank</th>
            <th class="text-center" data-multiple="120" tooltip-append-to-body="true" uib-tooltip="Pinterest shares">Pinterest</th>
            <th class="text-center" data-multiple="110" tooltip-append-to-body="true" uib-tooltip="FB Comments">FB Comment</th>
            <th class="text-center" data-multiple="100" tooltip-append-to-body="true" uib-tooltip="FB Shares">FB Shares</th>
            <th class="text-center" tooltip-append-to-body="true" uib-tooltip="Tweets">Tweets</th>
            <th class="text-center" tooltip-append-to-body="true" uib-tooltip="Keyword Found in URL">Url</th>
            <th class="text-center" tooltip-append-to-body="true" uib-tooltip="Keyword Found in Title">Title</th>
            <th class="text-center" tooltip-append-to-body="true" uib-tooltip="Keyword Found in Description">Desc</th>
            <th class="text-center" tooltip-append-to-body="true" uib-tooltip="Keyword Found in Header Tag">H1</th>
        </tr>
        </thead>
        <tbody>
            <tr ng-if="e.googleResults" ng-repeat="result in e.googleResults track by $index">
                <td class="text-center"> [[ $index + 1 ]] </td>
                <td class="text-left"><a href="[[result.url]]" target="_blank">
                    [[seeMore(result.url)]]
                </a></td>
                <td class="text-center">[[result.page_authority]]</td>
                <td class="text-center">[[result.domain_authority]]</td>
                <td class="text-center">[[result.backlinks]]</td>
                <td class="text-center">[[result.score.toFixed(2)]]</td>
                <td class="text-center">[[result.pinterest]]</td>
                <td class="text-center">[[result.googlePlus]]</td>
                <td class="text-center">[[result.facebook_shares]]</td>
                <td class="text-center">[[result.tweets]]</td>
                <td class="text-center">[[result.meta.url && result.meta.url==true ? 'YES' : 'NO' ]]</td>
                <td class="text-center">[[result.meta.title && result.meta.title==true ? 'YES' : 'NO' ]]</td>
                <td class="text-center">[[result.meta.desc && result.meta.desc==true ? 'YES' : 'NO' ]]</td>
                <td class="text-center">[[result.meta.h1 && result.meta.h1==true ? 'YES' : 'NO' ]]</td>
            </tr>
            <tr ng-if="!e.googleResults">
                <td colspan="15" class="alert alert-warning">
                    <h5>Sorry, we could not process any google search results. Please try again later</h5>
                </td>
            </tr>
        </tbody>
    </table>
</div>
