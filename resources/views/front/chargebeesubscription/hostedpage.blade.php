@extends('layouts.front.master')

@section('content')
@include('front.pages.new_sections.navigation')
    <div class="success_main">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-12">
					<iframe src="{{ $url }}" width="100%" height="400px"></iframe>
                </div>
            </div>
        </div>
    </div>
	  @include('front.pages.new_sections.footer-up')
@stop