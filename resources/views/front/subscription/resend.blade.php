@extends('layouts.front.master')
@section('content')
    @include('front.pages.new_sections.navigation')
    <div class="success_main sign_up_main">">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-2"></div>
                <div class="col-md-6 col-sm-8">
                @include('layouts.partials.messages')
                    <form action="{{ URL::route('resend-email') }}" method="POST" autocomplete="off"><img src="{{ asset('assets/frontend/new_html/images/success_icon.png')}}"
                               class="img-responsive">
                        <h3>If you didn't receive your verification email, please add your email and click resend. </h3>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <!--Post_Affiliate_Pro START 		<input type="hidden" name="pap_cookie" id="pap_cookie" value="">		<script type="text/javascript" id="pap_x2s6df8d" src="https://keywordrevealer.postaffiliatepro.com/scripts/vdzfn3n4jm8"></script>		<script type="text/javascript">		PostAffTracker.setAccountId('default1');		try {		PostAffTracker.track();		PostAffTracker.writeCookieToCustomField('pap_cookie');		} catch (err) { }		</script>		 Post Affiliate Pro END -->
                            <p>Email</p><i class="fa fa-envelope-o"></i> <input type="text" id="email"
                                                                                          name="email"
                                                                                          class="form-control box_1"
                                                                                          value="{{ app('request')->input('email') }}"
                                                                                          placeholder="E-mail">
                            <center><input type="submit" id="resend" class="create_btn" value="Resend"></center>
                    </form>
                    <!-- form end here -->   </div><!-- col end here -->  </div><!-- row end here --> </div>
        <!-- container end here --></div><!-- success_main end here -->
@stop
