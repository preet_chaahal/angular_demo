@extends('layouts.front.master')

@section('content')
    @include('front.pages.sections.navigation')
<div id="wrap">
    <div id="main" style="padding-top: 50px; padding-bottom: 25px;">
        <div class="container">

            <div class="row">
                <div class="col-sm-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                Account Information
                            </h3>
                        </div>
                        <div class="panel-body">

                            @include('layouts.partials.messages')

                            <form action="{{ URL::route('auth.hash.verify') }}" method="POST" autocomplete="off">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                <div class="form-group">
                                    <label for="username">Username</label>
                                    <input type="text" id="username" name="username" class="form-control" value="{{ old('email') }}">
                                </div>

                                <div class="form-group">
                                    <label for="hash">Token</label>
                                    <input type="text" id="hash" name="hash" class="form-control" value="{{ old('hash', $hash) }}">
                                </div>

                                <div class="row">
                                    <div class="col-xs-8 col-xs-offset-2">
                                        <input type="submit" class="btn btn-success btn-lg btn-block" value="Activate My Account">
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div> <!-- /panel -->
                </div> <!-- /col -->

                <div class="col-sm-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                Why do I need to activate my Account?
                            </h3>
                        </div>
                        <div class="panel-body">
                            <p>
                                KR TO ADD WORDS HERE
                            </p>
                        </div>
                    </div> <!-- /panel -->

                </div> <!-- /col -->
            </div> <!-- /row -->

        </div>
    </div>
</div>
@stop