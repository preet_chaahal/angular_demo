@extends('layouts.front.master')

@section('content')
    @include('front.pages.sections.navigation')
<div id="wrap">
    <div id="main" style="padding-top: 50px; padding-bottom: 25px;">
        <div class="container">

            <div class="row">
                <div class="col-sm-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                Account Information
                            </h3>
                        </div>
                        <div class="panel-body">

                            @include('layouts.partials.messages')

                            <form action="{{ URL::route('subscription.free') }}" method="POST" autocomplete="off">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                <div class="form-group">
                                    <label for="name">Your Name</label>
                                    <input type="text" id="name" name="name" class="form-control" value="{{ old('name') }}">
                                </div>

                                <div class="form-group">
                                    <label for="email">Email Address</label>
                                    <input type="text" id="email" name="email" class="form-control" value="{{ old('email') }}">
                                </div>

                                <div class="form-group">
                                    <label for="password">Password</label>
                                    <input type="password" id="password" name="password" class="form-control">
                                </div>

                                <div class="form-group">
                                    <label for="password_confirmation">Password Confirmed</label>
                                    <input type="password" id="password_confirmation" name="password_confirmation" class="form-control">
                                </div>

                                <div class="row">
                                    <div class="col-xs-8 col-xs-offset-2">
                                        <input type="submit" class="btn btn-success btn-lg btn-block" value="Create My Account">
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div> <!-- /panel -->
                </div> <!-- /col -->

                <div class="col-sm-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                Credit Card Information
                            </h3>
                        </div>
                        <div class="panel-body">
                            <h3>
                                Paypal
                            </h3>
                        </div>
                    </div> <!-- /panel -->
                </div> <!-- /col -->
            </div> <!-- /row -->

        </div>
    </div>
</div>
@stop

@section('script')

@stop