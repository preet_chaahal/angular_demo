@extends('layouts.front.master')

@section('content')
    @include('front.pages.new_sections.navigation')
    <div class="sign_up_main">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-2"></div>
                <div class="col-md-6 col-sm-8"><img src="{{ asset('assets/frontend/new_html/images/logo_2.png') }}"
                                                    class="img-responsive">
                    <h3>SIGN UP</h3> @if ($registration->disabled == 1)
                        <div class="alert alert-info" role="alert">Registration is currently disabled. Please try again
                            later.
                        </div>    @else     @include('layouts.partials.messages')
                        <form action="{{ URL::route('subscription.free') }}" method="POST" autocomplete="off"><input
                                    type="hidden" name="_token" value="{{ csrf_token() }}">
                            <!--Post_Affiliate_Pro START 		<input type="hidden" name="pap_cookie" id="pap_cookie" value="">		<script type="text/javascript" id="pap_x2s6df8d" src="https://keywordrevealer.postaffiliatepro.com/scripts/vdzfn3n4jm8"></script>		<script type="text/javascript">		PostAffTracker.setAccountId('default1');		try {		PostAffTracker.track();		PostAffTracker.writeCookieToCustomField('pap_cookie');		} catch (err) { }		</script>		 Post Affiliate Pro END -->
                            <p>Email</p>          <i class="fa fa-envelope-o"></i> <input type="text" id="email"
                                                                                          name="email"
                                                                                          class="form-control box_1"
                                                                                          value="{{ app('request')->input('email') }}"
                                                                                          placeholder="E-mail">
                            <p>Password</p>     <i class="fa fa-lock"></i> <input type="password" id="password"
                                                                                  name="password"
                                                                                  class="form-control box_1"
                                                                                  placeholder="Password">
                            <p>Confirm Password</p>     <i class="fa fa-lock"></i> <input type="password"
                                                                                          id="password_confirmation"
                                                                                          name="password_confirmation"
                                                                                          class="form-control box_1"
                                                                                          placeholder="Repeat Password">
                            <center><input type="submit" id="signup" class="create_btn" value="Create an Account"></center>
                        </form><!-- form end here -->     @endif <p>Already have an account? <a
                                href="{{ URL::route('auth.login') }}">Sign In</a></p></div><!-- col end here -->  </div>
            <!-- row end here --> </div><!-- container end here -->
    </div><!-- sign_up_main end here -->@include('front.pages.new_sections.testimonial')@include('front.pages.new_sections.footer-up')

@stop