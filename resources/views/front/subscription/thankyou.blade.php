@extends('layouts.front.master')

@section('content')
    @include('front.pages.new_sections.navigation')
    <div class="success_main">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-2"></div>
                <div class="col-md-6 col-sm-8">
                    <form><img src="{{ asset('assets/frontend/new_html/images/success_icon.png')}}"
                               class="img-responsive">
                        <h3>Thank You for registering your account</h3>
                        <p>Please check your email for details on how to activate your new account</p>
                        <p><a href="{{url('auth/resend-email')}}">Click Here</a> to resend verification email.</p>                     
                        </form>
                    <!-- form end here -->   </div><!-- col end here -->  </div><!-- row end here --> </div>
        <!-- container end here --></div><!-- success_main end here -->
@stop

@section('head-script')

    <script type="text/javascript">
        _fprom = window._fprom || [];
        window._fprom = _fprom;
        _fprom.push(["event", "signup"]);
        _fprom.push(["email", "{{$user_email}}"]);
        _fprom.push(["first_name", "{{$first_name}}"]);
        _fprom.push(["wid", "{{env('FP_WTID_2')}}"]);
        _fprom.push(["api_key","{{env('FP_API_KEY_2')}}"]);
        @if(isset($_COOKIE['_fprom_track']))
        _fprom.push(["tid", "{{$_COOKIE['_fprom_track']}}"]);
        @else
        _fprom.push(["tid", ""]);
        @endif
    </script>
@endsection