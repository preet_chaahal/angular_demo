@extends('layouts.front.master')

@section('content')
   {{--@include('front.pages.sections.navigation')
   @include('front.pages.sections.hero') --}}
   
 
   {{--@include('front.pages.sections.hero') --}}
    <!--<div id="main">
          {{--@include('front.pages.sections.main')
        @include('front.pages.sections.pricing') --}}
    </div>-->
	
	  @include('front.pages.new_sections.navigation')
	  @include('front.pages.new_sections.main')
	  @include('front.pages.new_sections.testimonial')
	  @include('front.pages.new_sections.pricing')
	  @include('front.pages.new_sections.faq')
	  @include('front.pages.new_sections.footer-up')
@stop