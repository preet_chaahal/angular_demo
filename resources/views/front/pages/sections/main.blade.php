<section id="about" class="light">
    <div class="test almost-white">
        <div class="container features">
            <div class="row mb-100">
                <div class="col-sm-12">
                    <div class="container-header">
                        <h2>Discover low-competition keywords Guaranteed!</h2>
                        <span>Start spending less time on keywords that are simply out of reach!</span>
                    </div>
                </div>
            </div>
            <div class="row mb-xs-0">
                <div class="col-sm-6 col-xs-12">
                    <div class="info-wrapper">
                        <div class="info-img">
                            <img src="{{ asset('assets/frontend/img/new_design/keywords.png') }}" alt="keyword research tool" class="img-responsive center-block">
                        </div>
                        <div class="info-text">
                            <h3 class="info-text-header">Keyword Research</h3>
                            <p class="info-text-body">Find hundreds of long tail keyword ideas in seconds with the amazing keyword research tool.</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-xs-12">
                    <div class="info-wrapper">
                        <div class="info-img">
                            <img src="{{ asset('assets/frontend/img/new_design/search.png') }}" alt="keyword difficulty tool" class="img-responsive center-block">
                        </div>
                        <div class="info-text">
                            <h3 class="info-text-header">Discover Keywords</h3>
                            <p class="info-text-body">Identify keywords with high search volume and low competition, you can also check domain availability for each term as well.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mb-100">
                <div class="col-sm-6 col-xs-12">
                    <div class="info-wrapper">
                        <div class="info-img">
                            <img src="{{ asset('assets/frontend/img/new_design/stats.png') }}" alt="keyword evaluation tool" class="img-responsive center-block">
                        </div>
                        <div class="info-text">
                            <h3 class="info-text-header">Evaluate Keywords</h3>
                            <p class="info-text-body">Instantly reveal the ranking difficulty and metrics for hundreds of keywords!</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-xs-12">
                    <div class="info-wrapper">
                        <div class="info-img">
                            <img src="{{ asset('assets/frontend/img/new_design/ranking.png') }}" alt="keyword ranking tool" class="img-responsive center-block">
                        </div>
                        <div class="info-text">
                            <h3 class="info-text-header">Rank Keywords</h3>
                            <p class="info-text-body">Apply the best keyword terms to your SEO strategy & start ranking on page one!</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="blue-bar">
            <div class="container">
                <div class="row">
                    <div class="col-sm-8 col-xs-12">
                        <div class="text-center">
                            <h2>Reveal low competition keywords in no time at all.</h2>
                        </div>
                    </div>
                    <div class="col-sm-4 col-xs-12">
                        <a href="{{ URL::route('subscription.free') }}" class="button custom-button button-rounded button-white">Start Free Trial</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="details" class="test white">
        <div class="container details">
            <div class="row">
                <div class="col-sm-6">
                    <div class="detail-wrapper">
                        <div class="detail-links">
                            <a href="javascript:void(0)" id="detail-0" class="button custom-button custom-img-button button-rounded active">
                                <img src="{{ asset('assets/frontend/img/new_design/keywords-sm.png') }}" alt="keyword research tool" class="">
                                Keyword Research
                            </a>
                            <a href="javascript:void(0)" id="detail-1" class="button custom-button custom-img-button button-rounded">
                                <img src="{{ asset('assets/frontend/img/new_design/cloud-sm.png') }}" alt="keyword brainstorming tool" class="">
                                Keyword Brainstorming
                            </a>
                            <a href="javascript:void(0)" id="detail-2" class="button custom-button custom-img-button button-rounded">
                                <img src="{{ asset('assets/frontend/img/new_design/speedometer-sm.png') }}" alt="rank tracker tool" class="">
                                Rank Tracker
                            </a>
                        </div>
                        <div class="detail-container">
                            <div id="detail-0-text" class="detail-text">
                                <h3 class="detail-header">Keyword Research</h3>
                                <div class="detail-body">
                                    <p>Keyword Revealer is a huge time saver for anyone looking to uncover low-competition keywords. Start focusing your SEO efforts on easy-to-rank terms with low difficulty scores.</p>
                                    <p>Our Keyword tool will uncover thousands of untapped long-tail keywords for any niche imaginable. Once keywords are discovered you will be able to view the difficulty score for each phrase and analyze the competition currently ranking on the first page of Google.</p>
                                </div>
                            </div>
                            <div id="detail-1-text" class="detail-text">
                                <h3 class="detail-header">Keyword Brainstorming</h3>
                                <div class="detail-body">
                                    <p>One of the most difficult aspects of keyword research is discovering new keyword phrases based on searcher intent. Our Keyword Brainstorming tool will allow you to discover the specific context in which your keywords are being used within the search engines.</p>
                                    <p>Use the Keyword Brainstorming tool to explore new niche ideas and group them in the correct silo structure. </p>
                                </div>
                            </div>
                            <div id="detail-2-text" class="detail-text">
                                <h3 class="detail-header">Rank Tracker</h3>
                                <div class="detail-body">
                                    <p>Save time and money by automating the process of checking your keyword rankings. Our Rank tracking tool will automatically check your keyword position in Google every day!</p>
                                    <p>View your current rank and measure changes in rankings since yesterday, last week, last month, or since you started tracking the keyword. It's the rank tracker you've been waiting for.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-5 col-sm-offset-1">
                    <div class="screenshot img-overflow">
                        <img src="{{ asset('assets/frontend/img/new_design/main-scr2.png') }}" alt="Keyword Research" class="detail-img" id="detail-0-img"> 
                        <img src="{{ asset('assets/frontend/img/new_design/main-scr3.png') }}" alt="Keyword Brainstorming" class="detail-img" id="detail-1-img"> 
                        <img src="{{ asset('assets/frontend/img/new_design/main-scr4.png') }}" alt="Rank Tracker" class="detail-img" id="detail-2-img"> 
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- The Image Modal -->
    <div id="myModal" class="modal">
        <img class="modal-content" id="modal-img">
        <div id="caption"></div>
    </div>

</section>

<section id="feedback" class="light">
    <div class="test">

        <div class="container feedback">
            <div class="container-header">
                <h2>What Our Clients Say</h2>
            </div>
            <div id="carousel-feedback" class="carousel slide text-center">
                <ol class="carousel-indicators">
                    <li data-target="#carousel-feedback" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-feedback" data-slide-to="1" class=""></li>
                    <li data-target="#carousel-feedback" data-slide-to="2" class=""></li>
                    <li data-target="#carousel-feedback" data-slide-to="3" class=""></li>
                    <li data-target="#carousel-feedback" data-slide-to="4" class=""></li>
                </ol>
                <!-- Carousel items -->
                <div class="carousel-inner">
                    <div class="item active">
                        <div class="carousel-content">
                            <blockquote>
                                <p>"The Keyword Revealer is an Amazing keyword Research tool for every Internet Marketer. With Great User Interface and the fantastic Keyword Research results, it helped me to build up my Niche site and make it big. In the times when Google Adwords Keyword Planner" tool stopped showing exact search volume to the Non-Advertisers, Keyword Revealer Helped me to come up with my Keyword Research."</p>
                            </blockquote>
                            <div class="author-info">
                                <div class="author-img">
                                    <img src="{{ asset('assets/frontend/img/new_design/client_1.png') }}">
                                </div>
                                <div class="author">
                                    <strong>Swaraj Nandedkar</strong>
                                    <p>SEO Specialist</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="carousel-content">
                            <blockquote>
                                <p>"I love keyword revealer. It's so easy to use and accurate! I recommend it to all my friends and coworkers as the best keyword research tool!"</p>
                            </blockquote>
                            <div class="author-info">
                                <div class="author-img">
                                    <img src="{{ asset('assets/frontend/img/new_design/client_2.png') }}">
                                </div>
                                <div class="author">
                                    <strong>John Zoro</strong>
                                    <p>Business Owner</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="carousel-content">
                            <blockquote>
                                <p>"I have been using keyword revealer for quite a few months and I must say, its my go to software to find and examine keywords! Whenever I think I found a good seed keyword, I head over to keywordrevealer, as I know ill be able to find a lot more keywords with the seed keyword! This tool is great for finding the difficulty of keywords as well, which helps me determine if I should build a site on the keyword or not, which makes keyword revealer one of the best sites I have used!"</p>
                            </blockquote>
                            <div class="author-info">
                                <div class="author-img">
                                    <img src="{{ asset('assets/frontend/img/new_design/client_3.png') }}">
                                </div>
                                <div class="author">
                                    <strong>John Green</strong>
                                    <p>SEO Agency</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="carousel-content">
                            <blockquote>
                                <p>"I used Keyword Revealer above all other keyword research tools out there for quick and extensive results. I rarely needed other tools for my extensive keyword research tasks. Thanks for this wonderful tool."</p>
                            </blockquote>
                            <div class="author-info">
                                <div class="author-img">
                                    <img src="{{ asset('assets/frontend/img/new_design/client_4.png') }}">
                                </div>
                                <div class="author">
                                    <strong>David Kim</strong>
                                    <p>SEO Consultant</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="carousel-content">
                            <blockquote>
                                <p>"I have been using KeywordRevealer for quite a while now, and it saves me so much time when it comes to identifying primary and secondary keywords to target. I often recommend it to others, especially to members of our Facebook SEO group."</p>
                            </blockquote>
                            <div class="author-info">
                                <div class="author-img">
                                    <img src="{{ asset('assets/frontend/img/new_design/client_5.png') }}">
                                </div>
                                <div class="author">
                                    <strong>Earl Gardner</strong>
                                    <p>Business Owner</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>