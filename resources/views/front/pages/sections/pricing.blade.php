<section id="pricing" class="light">
    <div class="test white molecule">
    </div>
    <div class="test green-purple">
        <div class="">
            <div class="row">
                <div class="col-sm-12">
                    <div class="plan-switcher">
                        <a id="monthly" href="javascript:void(0)" class="button button-rounded button-switcher pull-left">
                            <div class="text-center">Monthly</div>
                        </a>
                        <a id="annual" href="javascript:void(0)" class="button button-rounded button-switcher pull-right active">
                            <div class="switcher-price-type text-center">Annual</div>
                            <div class="switcher-discount text-center">-40% OFF</div>
                        </a>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="plan-table">
                        <div class="pt-item pt-item-label pt-item-first">
                            <div class="pti-cell pti-cell-header"></div>
                            <div class="pti-cell pti-cell-price">Price</div>
                            <div class="pti-cell">Daily Searches</div>
                            <div class="pti-cell">Daily Evaluations</div>
                            <div class="pti-cell">Brainstorming Usage</div>
                            <div class="pti-cell">Rank Tracker (keywords)</div>
                            <div class="pti-cell">Save Projects</div>
                            <div class="pti-cell">Save Keywords</div>
                            <div class="pti-cell">Exact Match Domain Finder</div>
                            <div class="pti-cell">Bulk Keyword Upload</div>
                            <div class="pti-cell pti-cell-btn"></div>
                        </div>
                        <div class="pt-item">
                            <div class="pti-cell pti-cell-header">Free</div>
                            <div class="pti-cell pti-cell-price">
                                <div class="price-free">
                                    Free
                                </div>
                            </div>
                            <div class="pti-cell">3</div>
                            <div class="pti-cell">7</div>
                            <div class="pti-cell">3</div>
                            <div class="pti-cell">0</div>
                            <div class="pti-cell"><i class="fa fa-times-circle" aria-hidden="true"></i></div>
                            <div class="pti-cell"><i class="fa fa-times-circle" aria-hidden="true"></i></div>
                            <div class="pti-cell"><i class="fa fa-times-circle" aria-hidden="true"></i></div>
                            <div class="pti-cell"><i class="fa fa-times-circle" aria-hidden="true"></i></div>
                            <div class="pti-cell pti-cell-btn">
                                @if (Auth::check())
                                    <a href="{{ URL::route('subscription-change') }}" class="button button-rounded button-default">Get Started</a>
                                @else
                                    <a href="{{ URL::route('subscription.free') }}" class="button button-rounded button-default">Get Started</a>
                                @endif
                            </div>
                        </div>
                        <div class="price-wrapper">
                            <div class="pt-item">
                                <div class="pti-cell pti-cell-header">Basic</div>
                                <div class="pti-cell pti-cell-price annual active">
                                    <div class="price-line">
                                        <div class="price-value">$7.50/</div>
                                        <div class="price-period">
                                            <span class="period-full">month</span>
                                            <span class="period-short">mo.</span>
                                        </div>
                                    </div>
                                    <div class="price-type">$90 Paid Annually<br> <span class="badge"> Save 25%</span></div>
                                </div>
                                <div class="pti-cell pti-cell-price monthly">
                                    <div class="price-line">
                                        <div class="price-value">$9.97/</div>
                                        <div class="price-period">
                                            <span class="period-full">month</span>
                                            <span class="period-short">mo.</span>
                                        </div>
                                    </div>
                                    <div class="price-type">Paid Monthly</div>
                                </div>
                                <div class="pti-cell">25</div>
                                <div class="pti-cell">50</div>
                                <div class="pti-cell">25</div>
                                <div class="pti-cell">25</div>
                                <div class="pti-cell">3</div>
                                <div class="pti-cell">25</div>
                                <div class="pti-cell"><i class="fa fa-times-circle" aria-hidden="true"></i></div>
                                <div class="pti-cell"><i class="fa fa-times-circle" aria-hidden="true"></i></div>
                                {{-- <div class="pti-cell"><i class="fa fa-check-circle" aria-hidden="true"></i></div> --}}

                                <div class="pti-cell pti-cell-btn">
                                    @if (Auth::check())
                                        <a href="{{ URL::route('subscription-change') }}" class="button button-rounded button-default">Sign Up</a>
                                    @else
                                        <a href="{{ URL::route('subscription.free') }}" class="button button-rounded button-default">Sign Up</a>
                                    @endif
                                </div>
                            </div>
                            <div class="pt-item pt-item-main">
                                <div class="pti-cell pti-cell-header">Pro</div>
                                <div class="pti-cell pti-cell-price annual active">
                                    <div class="price-line">
                                        <div class="price-value">$17.50/</div>
                                        <div class="price-period">
                                            <span class="period-full">month</span>
                                            <span class="period-short">mo.</span>
                                        </div>
                                    </div>
                                    <div class="price-type">$210 Paid Annually<br> <span class="badge">Save 37%</span></div>
                                </div>
                                <div class="pti-cell pti-cell-price monthly">
                                    <div class="price-line">
                                        <div class="price-value">$27.97/</div>
                                        <div class="price-period">
                                            <span class="period-full">month</span>
                                            <span class="period-short">mo.</span>
                                        </div>
                                    </div>
                                    <div class="price-type">Paid Monthly</div>
                                </div>
                                <div class="pti-cell">150</div>
                                <div class="pti-cell">200</div>
                                <div class="pti-cell">150</div>
                                <div class="pti-cell">150</div>
                                <div class="pti-cell">50</div>
                                <div class="pti-cell">150</div>
                                <div class="pti-cell"><i class="fa fa-times-circle" aria-hidden="true"></i></div>
                                <div class="pti-cell"><i class="fa fa-times-circle" aria-hidden="true"></i></div>

                                <div class="pti-cell pti-cell-btn">
                                    @if (Auth::check())
                                        <a href="{{ URL::route('subscription-change') }}" class="button button-rounded button-default">Sign Up</a>
                                    @else
                                        <a href="{{ URL::route('subscription.free') }}" class="button button-rounded button-default">Sign Up</a>
                                    @endif
                                </div>
                            </div>
                            <div class="pt-item  pt-item-last">
                                <div class="pti-cell pti-cell-header">Elite</div>
                                <div class="pti-cell pti-cell-price annual active">
                                    <div class="price-line">
                                        <div class="price-value">$29.50/</div>
                                        <div class="price-period">
                                            <span class="period-full">month</span>
                                            <span class="period-short">mo.</span>
                                        </div>
                                    </div>
                                    <div class="price-type">
                                        $354 Paid Annually
                                        <br><span class="badge">Save 39%</span>
                                    </div>
                                </div>
                                <div class="pti-cell pti-cell-price monthly">
                                    <div class="price-line">
                                        <div class="price-value">$47.97/</div>
                                        <div class="price-period">
                                            <span class="period-full">month</span>
                                            <span class="period-short">mo.</span>
                                        </div>
                                    </div>
                                    <div class="price-type">Paid Monthly</div>
                                </div>
                                <div class="pti-cell">400</div>
                                <div class="pti-cell">450</div>
                                <div class="pti-cell">400</div>
                                <div class="pti-cell">400</div>
                                <div class="pti-cell">100</div>
                                <div class="pti-cell">400</div>
                                <div class="pti-cell"><i class="fa fa-times-circle" aria-hidden="true"></i></div>
                                <div class="pti-cell"><i class="fa fa-times-circle" aria-hidden="true"></i></div>
                                <div class="pti-cell pti-cell-btn">
                                    @if (Auth::check())
                                        <a href="{{ URL::route('subscription-change') }}" class="button button-rounded button-default">Sign Up</a>
                                    @else
                                        <a href="{{ URL::route('subscription.free') }}" class="button button-rounded button-default">Sign Up</a>
                                    @endif
                                </div>
                            </div>

                            {{-- <div class="pt-item">
                                <div class="pti-cell pti-cell-header">Agency</div>
                                <div class="pti-cell pti-cell-price annual active">
                                    <div class="price-line">
                                        <div class="price-value">$75/</div>
                                        <div class="price-period">
                                            <span class="period-full">month</span>
                                            <span class="period-short">mo.</span>
                                        </div>
                                    </div>
                                    <div class="price-type">$900 Paid Annually</div>
                                </div>
                                <div class="pti-cell pti-cell-price monthly">
                                    <div class="price-line">
                                        <div class="price-value">$150/</div>
                                        <div class="price-period">
                                            <span class="period-full">month</span>
                                            <span class="period-short">mo.</span>
                                        </div>
                                    </div>
                                    <div class="price-type">Paid Monthly</div>
                                </div>
                                <div class="pti-cell">1000</div>
                                <div class="pti-cell">1000</div>
                                <div class="pti-cell">1000</div>
                                <div class="pti-cell">500</div>
                                <div class="pti-cell">100</div>
                                <div class="pti-cell">1000</div>
                                <div class="pti-cell"><i class="fa fa-check-circle" aria-hidden="true"></i></div>
                                <div class="pti-cell"><i class="fa fa-check-circle" aria-hidden="true"></i></div>
                                <div class="pti-cell pti-cell-btn">
                                    @if (Auth::check())
                                        <a href="{{ URL::route('subscription-change') }}" class="button button-rounded button-default">Sign Up</a>
                                    @else
                                        <a href="{{ URL::route('subscription.free') }}" class="button button-rounded button-default">Sign Up</a>
                                    @endif
                                </div>
                            </div> --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('front.pages.sections.faq')
    </div>
</section>
