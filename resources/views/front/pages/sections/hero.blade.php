<div id="hero" class="dark">
    <div class="test substrate">
        <div class="test main-bg">
            <div class="container text-center">
                <h1>The Ultimate Keyword Intelligence Tool</h1>
                <div class="main-label">
                    <span>Get Your FREE KeywordRevealer Trial Account!</span>
                </div>
                <div class="main-form">
                    <form action="{{ URL::route('subscription.free') }}" method="get" autocomplete="off">
                        <div class="form-group">
                            <input type="text" id="email" name="email" class="form-control" placeholder="Enter your E-mail">
                        </div>
                        <button type="submit" class="button custom-button button-rounded button-info">Start your FREE trial now</button>
                    </form>
                </div>
                <div id="screenshot-1" class="screenshot">
                    <img src="{{ asset('assets/frontend/img/new_design/main-scr1.png') }}" alt="" class="center-block img-responsive">
                </div>
            </div>
            <!-- <div class="main-cover">
                <img src="{{ asset('assets/frontend/img/new_design/cover.png') }}" alt="" class="center-block img-responsive">
            </div> -->
        </div>
    </div>
</div>