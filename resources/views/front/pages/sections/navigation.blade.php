<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="{{ URL::route('front.home') }}"><img src="{{ asset('assets/frontend/img/new_design/logo-white-blue.png') }}" alt="Keyword Revealer" class="img-responsive"></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        @if (Auth::check())
        <ul class="nav navbar-nav pull-right navbar-login hidden-xs hidden-sm">
            <li class="login-btn signed-in-btn">
                <a href="{{ URL::route('backend.tools.keywordresearch') }}" class="button custom-button button-rounded button-info">Research</a>
            </li>
        </ul>
        <!-- <ul class="nav navbar-nav pull-right navbar-login hidden-xs hidden-sm">
            <li>
                <div class="btn-group">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img src="{{ Auth::user()->gravatar }}" alt="{{ Auth::user()->username }}" style="height:25px; width: 25px;"> {{ Auth::user()->username }} <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu">
                        <li><a href="{{ URL::route('auth.profile') }}"><i class="fa fa-user"></i>&nbsp;My Account</a></li>
                        <li><a href="{{ URL::route('backend.tools.keywordresearch') }}"><i class="fa fa-search"></i>&nbsp;Keyword Research</a></li>
                        <li><a href="{{ URL::route('backend.tools.ranktracker') }}"><i class="fa fa-arrow-up"></i>&nbsp;Rank Tracker</a></li>
                        <li><a href="{{ URL::route('backend.tools.brainstorm') }}"><i class="fa fa-group"></i>&nbsp;Keyword Brainstorming</a></li>
                        <li><a href="{{ URL::route('front.contact') }}"><i class="fa fa-info-circle"></i>&nbsp;Help</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="{{ URL::route('auth.logout') }}"><i class="fa fa-sign-out"></i>&nbsp;Logout</a></li>
                    </ul>
                </div>
            </li>
        </ul> -->
        @else
        <ul class="nav navbar-nav pull-right navbar-login hidden-xs hidden-sm">
            <li class="login-btn sign-in-btn">
                <a href="{{ URL::route('auth.login') }}" class="button custom-button button-rounded button-info">Sign In</a>
            </li>
        </ul>
        @endif
        <ul class="nav navbar-nav pull-right">
            <li><a href="#pricing">Pricing</a></li>
            <li><a href="http://www.keywordrevealer.com/blog/">Blog</a></li>
            <li><a href="https://keywordrevealer.freshdesk.com/login/normal" target="_blank">Support</a></li>            <!--<li><a href="/sso_login_freshdesk.php?host_url=keywordrevealer.freshdesk.com">Support</a></li>-->
            <li><a href="{{ URL::route('front.contact') }}">Contact Us</a></li>
        </ul>
        <ul class="nav navbar-nav pull-right navbar-login visible-xs visible-sm">
            <li><a href="{{ URL::route('auth.login') }}" class="button custom-button button-rounded button-info">Sign In</a></li>
        </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>