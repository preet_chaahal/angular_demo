<div class="container baners">
    <div class="row">
        <div class="col-md-6 col-sm-12">
            <div class="secure-container">
                <img class="media-object img-responsive"
                     src="{{ asset('assets/frontend/img/new_design/secure.png') }}" alt="Image">
            </div>
        </div>
        <div class="col-md-6 col-sm-12">
        <div class="money-back-container">
            <img class="media-object img-responsive"
                 src="{{ asset('assets/frontend/img/new_design/money-back.png') }}" alt="Image">
        </div>
        </div>
    </div>
</div>

<div class="container faq">
    <div class="row">
        <div class="col-md-10 col-md-offset-1 col-sm-12">
            <div class="container-header">
                <h2>Frequently Asked Questions</h2>
            </div>
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                1. Can I cancel anytime or change my subscription plan?
                            </a>
                        </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body">
                            <p>Yes. KeywordRevealer is a pay-as-you-go service. There are no long term contracts or commitments to worry about.</p>
                            <p>You can change or cancel your subscription plan at anytime right from your <a href="https://www.keywordrevealer.com/auth/profile">account dashboard</a>. Just <a href="https://www.keywordrevealer.com/contact">contact us</a> if you have any questions and our support team will be happy to help. </p>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingTwo">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                2. How does the money-back guarantee work?
                            </a>
                        </h4>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                        <div class="panel-body">
                            <p>We offer a full money-back guarantee within 30-days of signing up. Just <a href="https://www.keywordrevealer.com/contact">contact our</a> support team and we will refund you 100% of your initial payment. </p>
                            <p>We stand behind this guarantee because we're that confident you'll love KeywordRevealer and we want you to keep your membership active so you enjoy all of the ongoing benefits! </p>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingThree">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                3. Will I receive free updates to Keyword Revealer?
                            </a>
                        </h4>
                    </div>
                    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                        <div class="panel-body">
                            <p>Yes. We are always working to improve our service by adding more features and functionality. All active customers will have access to all features within their plan, without any additional fees. </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container contact">
    <div class="row">
        <div class="col-sm-12">
            <div class="container-header">
                <h2>Still Have Questions?</h2>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-6">
            <div class="pull-right">
                <h3>Feel free to</h3>
            </div>
        </div>
        <div class="col-xs-6">
            <div class="pull-left">
                <a href="{{ URL::route('front.contact') }}" class="button custom-button button-rounded">Contact Us</a>
            </div>
        </div>
    </div>
</div>

