@extends('layouts.front.master')
@section('robots', 'noindex, nofollow')

@section('content')
    @include('front.pages.new_sections.navigation')

    <div id='main' class='all_colors' data-scroll-offset='88'>

        <div  class='main_color container_wrap_first container_wrap fullsize'   ><div class='container' ><main  role="main" itemprop="mainContentOfPage"  class='template-page content  av-content-full alpha units' style="margin-top: 100px;"><div class='post-entry post-entry-type-page post-entry-1179'><div class='entry-content-wrapper clearfix'><div style='padding-bottom:0px;font-size:30px;' class='av-special-heading av-special-heading-h4  blockquote modern-quote  avia-builder-el-0  el_before_av_textblock  avia-builder-el-first   av-inherit-size'><h4 class='av-special-heading-tag'  itemprop="headline"  >KeywordRevealer™ Terms of Service</h4><div class='special-heading-border'><div class='special-heading-inner-border' ></div></div></div>
<section class="av_textblock_section"  itemscope="itemscope" itemtype="https://schema.org/CreativeWork" ><div class='avia_textblock '   itemprop="text" ><p><span style="font-weight: 400;">We thank you for choosing to subscribe to our services. These Terms of Service (the “ToS”) governs your relationship with KeywordRevealer. (“we”), and set forth the terms and conditions under which KeywordRevealer makes available its paid Service.</span></p>
</div></section>
<div class='hr hr-invisible  avia-builder-el-2  el_after_av_textblock  el_before_av_one_full  '><span class='hr-inner ' ><span class='hr-inner-style'></span></span></div>
<div class="flex_column av_one_full  flex_column_div av-zero-column-padding first  avia-builder-el-3  el_after_av_hr  avia-builder-el-last   " style='border-radius:0px; '><section class="av_textblock_section"  itemscope="itemscope" itemtype="https://schema.org/CreativeWork" ><div class='avia_textblock '  style='font-size:16px; '  itemprop="text" ><p><span style="font-weight: 400;">By signing up and subscribing to our Services you (the “User”) hereby agree to be bound by these TOS, as may be amended from time to time. Therefore please read these TOS carefully and visit this pages regularly for updates and changes.</span></p>
<ol>
<li style="font-weight: 400;"><strong>Subscribing to the Services</strong>
<ul>
<li style="font-weight: 400;"><span style="font-weight: 400;">KeywordRevealer’s Services are strictly business to business. By the customer approval to this ToS s/he is approving that s/he represent a a business entity, and that s/he is at least 18 years old, and has the authority to bind the entity to these ToS.</span></li>
<li style="font-weight: 400;"><span style="font-weight: 400;">By registering and placing an order to acquire Services, you warrant and represent and that any information provided by you at the time of registration is complete, truthful and accurate, and you agree to ensure that such information is kept up to date.</span></li>
<li style="font-weight: 400;"><span style="font-weight: 400;">Upon registering for an account, you will receive a link to your account, a username and a password. You are entirely responsible for maintaining the confidentiality of your password. You agree not to use the account, username, or password of another user at any time, or to disclose your password to any third party, except to your employees actually using the service, subject to the package that you purchased (the “</span><strong>Approved Personnel</strong><span style="font-weight: 400;">”). You agree to notify us immediately if you suspect any unauthorized use of your account or access to your password. You are solely responsible for any and all use of your account.</span></li>
<li style="font-weight: 400;"><span style="font-weight: 400;">You may not transfer, assign or sell your KeywordRevealer’s account and/or User ID to another party.</span></li>
<li style="font-weight: 400;"><span style="font-weight: 400;">Only Approved Personnel may use the Services.</span></li>
</ul>
</li>
</ol>
<ul>
<li style="font-weight: 400;"><span style="font-weight: 400;">By your approval to this ToS you herby agree to receive digital invoices, receipts, credit notes etc. (hereinafter: &#8220;Digital Billing Documents&#8221;). </span></li>
</ul>
<ul>
<li style="list-style-type: none;"></li>
<li style="font-weight: 400;"><strong>Billing and Payment</strong>
<ul>
<li style="font-weight: 400;"><span style="font-weight: 400;">Subscriptions to the KeywordRevealer’s Services may be made available in free or paid versions and/or in different service levels. Not all of the features and functionality of the KeywordRevealer’s Services may be available in each version or service level. The features and functionality of each version or service level may be changed from time to time at KeywordRevealer’s discretion.</span></li>
<li style="font-weight: 400;"><span style="font-weight: 400;">If your billing information and payment source is invalid, if charges billed to you are declined or not paid or if you fail to pay charges for a paid version of KeywordRevealer’s Services when due, your account may be downgraded, suspended or cancelled, at KeywordRevealer’s discretion.</span></li>
<li style="font-weight: 400;"><span style="font-weight: 400;">Failure to Charge – If the client’s payment method is declined for any reason, or if the payment has not been submitted. KeywordRevealer reserves the right to suspend the account until all payments are resolved.</span></li>
</ul>
</li>
<li style="font-weight: 400;"><strong>Rebilling statement</strong>
<ul>
<li style="font-weight: 400;"><span style="font-weight: 400;">Your KeywordRevealer™ subscription fee is recurring, with automatic periodic renewals, typically 30 days. Subscription fees are non-refundable under any circumstance, however, you are allowed to cancel your recurring subscription prior to each renewal date.</span></li>
<li style="font-weight: 400;"><span style="font-weight: 400;">For your subscription fee you are entitled to the agreed number of Usages, as defined in your Plan, as credit, to be consumed within each subscription period. At the end of each period your credit balance will be reloaded to the full periodic credit.</span></li>
</ul>
</li>
</ul>
<ul>
<li style="font-weight: 400;"><span style="font-weight: 400;">Failure to Charge – If the client’s payment method is declined for any reason, or if the payment has not been submitted. KeywordRevealer reserves the right to suspend the account until all payments are resolved.</span></li>
</ul>
<ul>
<li style="font-weight: 400;"><span style="font-weight: 400;">If a User account is suspended, KeywordRevealer may, but is not obligated to, maintain your account and/or related information, in order to allow the User to pay the past-due charges and restore its account. If the charges are not paid, such account may be cancelled.</span></li>
<li style="font-weight: 400;"><span style="font-weight: 400;">Information regarding the different packages and services we offer, as well as their pricing, billing method, etc., can be found here:</span><em><span style="font-weight: 400;"> https://www.keywordrevealer.com/#pricing</span></em></li>
</ul>
<ul>
<li style="list-style-type: none;">
<ul>
<li style="font-weight: 400;"><span style="font-weight: 400;">Service Privilege KeywordRevealer grants the User, commencing on the effective date of the User subscription to access KeywordRevealer’s Services (the “</span><strong>Effective Date</strong><span style="font-weight: 400;">“) and continuing thereafter for the term of the subscription (unless terminated earlier pursuant to these ToS), a non-exclusive, non-transferable, privilege to use the current versions of the KeywordRevealer’s Services to which such User has subscribed, solely for the User’s own use, in connection with its own business, and for the purposes of utilizing KeywordRevealer’s site security solution. The access to KeywordRevealer Service can be used by a privilege , not sold.</span></li>
<li style="font-weight: 400;"><span style="font-weight: 400;">Any software or code available on or for download through our site or Services (“</span><strong>Software</strong><span style="font-weight: 400;">“) is protected by intellectual property rights and is owned by KeywordRevealer or any of its affiliates. Except as expressly set forth herein regarding your right to access the Software in accordance with these ToS, you are granted no rights, in or to the Software or any intellectual property rights related thereto, in any way whatsoever, including by implication, estoppel, or otherwise.</span></li>
<li style="font-weight: 400;"><span style="font-weight: 400;">You may not modify, reproduce, perform, display, create derivative works from, republish, post, transmit, participate in the transfer or sale of, distribute, or in any way exploit any portion of the Software without the prior written permission of KeywordRevealer.</span></li>
<li style="font-weight: 400;"><span style="font-weight: 400;">The access to the Services may include services developed, provided or maintained by third-party service providers (“</span><strong>Third Party Services</strong><span style="font-weight: 400;">“). KeywordRevealer may change, modify or discontinue any Third Party Service at any time and without notice to you. Please note, that your access to and use of any Third Party Services is also subject to any other agreement separate from these ToS that you may enter into (or may have entered into) relating to those Third Party Services. Third Party Services may be subject to additional Fees as set forth on the site.</span></li>
</ul>
</li>
<li style="font-weight: 400;"><strong>Restrictions</strong>
<ul>
<li style="font-weight: 400;"><span style="font-weight: 400;">In connection with your use of the Services, and without limiting any of the other obligations under these ToS or applicable law, User shall not, and shall not permit others to:</span>
<ul>
<li style="font-weight: 400;"><span style="font-weight: 400;">Use the KeywordRevealer’s Services for any purposes other than those expressly set forth in these ToS;</span></li>
<li style="font-weight: 400;"><span style="font-weight: 400;">Modify the KeywordRevealer’s Services, or decompile, reverse-engineer, disassemble, or otherwise attempt, directly or indirectly, to obtain or create source code for KeywordRevealer ‘s Services;</span></li>
<li style="font-weight: 400;"><span style="font-weight: 400;">Use, sell, distribute, copy, duplicate, or otherwise reproduce all or any part of KeywordRevealer’s Services;</span></li>
<li style="font-weight: 400;"><span style="font-weight: 400;">Attempt to circumvent or overcome any technological protection measures intended to restrict access to any portion of our site, Services, Technology, or any Software;</span></li>
<li style="font-weight: 400;"><span style="font-weight: 400;">Engage in any deceptive, misleading, illegal or unethical practices that may be detrimental to KeywordRevealer or the KeywordRevealer’s Services, and any such representation, warranty or guarantee concerning KeywordRevealer made by User shall be considered a material breach of these ToS; and make any agreements, covenants, representations, warranties or guarantees concerning KeywordRevealer or the KeywordRevealer’s Services that are inconsistent with or in addition to those contained in these ToS;</span></li>
<li style="font-weight: 400;"><span style="font-weight: 400;">Allow any access to or use of the Services by anyone other than your authorized users.</span></li>
</ul>
</li>
</ul>
</li>
</ul>
<ol>
<li style="font-weight: 400;"><strong>User’s Content</strong>
<ul>
<li style="font-weight: 400;"><span style="font-weight: 400;">You are solely responsible for any content uploaded, submitted or transmitted by you through the site or the Service, including, without limitation, all personally identifiable information relating to you or any of your users, customers, service providers, employees, contractors or agents. KeywordRevealer expressly disclaims any and all liability in connection with such content.</span></li>
<li style="font-weight: 400;"><span style="font-weight: 400;">KeywordRevealer may access, use, store, disclose and/or share any information stored or transmitted on or through the Services which KeywordRevealer considers necessary or appropriate, including, without limitation, User’s content, IP address and traffic information, volumes, frequencies, recipients, bounce rates, usage history, and any other type of content or data, in order to, among others, comply with applicable laws and lawful governmental requests, to protect KeywordRevealer’s systems and users, to ensure the integrity and operation of KeywordRevealer’s business and systems, and to allow KeywordRevealer to provide and improve its services. KeywordRevealer also reserves the right to report any activity that it suspects violates any law, rule or regulation to appropriate law enforcement officials, regulators or other appropriate third parties.</span></li>
</ul>
</li>
<li style="font-weight: 400;"><strong>Provision of Services</strong>
<ul>
<li style="font-weight: 400;"><span style="font-weight: 400;">The User Subscription Effective Date definition is according to the customer confirmation of the digital documents as follows, whichever is the latest approval: </span>
<ol>
<li style="font-weight: 400;"><span style="font-weight: 400;">Approval of This TOS.</span></li>
<li style="font-weight: 400;"><span style="font-weight: 400;">Confirming the service price.</span></li>
</ol>
</li>
<li style="font-weight: 400;"><span style="font-weight: 400;">, Unless otherwise stated in writing, The User Subscription   shall continue indefinitely until User’s account had been terminated.</span></li>
<li style="font-weight: 400;"><span style="font-weight: 400;">You are entitled to cancel your account and registration at any time, in accordance to the terms of your subscription. After cancellation or termination of your account for any reason, you will no longer have access to the account and all information and content in such account may be, but is not required to be, deleted by KeywordRevealer. Moreover, the service privilege that was granted by KeywordRevealer, if any, shall immediately terminate.</span></li>
</ul>
</li>
</ol>
<ul>
<li style="list-style-type: none;">
<ul>
<li style="font-weight: 400;"><span style="font-weight: 400;">KeywordRevealer’s services are not available to temporarily or indefinitely suspended accounts and KeywordRevealer reserves the right, in KeywordRevealer’s sole discretion, to cancel unconfirmed or inactive accounts. KeywordRevealer reserves the right at any time and from time to time to modify, suspend, or discontinue, temporarily or permanently, the Services or any part thereof, or any User’s access thereto, and to modify, suspend or delete the site or any part thereof. You acknowledge and agree that KeywordRevealer shall not be liable to you or to any third party, for any modification, suspension or discontinuance of the Services.</span></li>
<li style="font-weight: 400;"><span style="font-weight: 400;">Without limiting any other remedies, KeywordRevealer may, without notice, and without refunding any fees, temporarily suspend a User, temporarily or indefinitely suspend a User’s account privileges, terminate a User’s account, and prohibit access to the Services, if such User has breached these ToS or any other KeywordRevealer’s policies, or has engaged in improper or fraudulent activity in connection with KeywordRevealer, or has preformed any other acts that may cause legal liability or financial loss to KeywordRevealer’s Users or to KeywordRevealer.</span></li>
</ul>
</li>
</ul>
<p><span style="font-weight: 400;">KeywordRevealer may recover from a User any losses, damages, costs or expenses incurred by KeywordRevealer resulting from or arising out of User’s non-compliance with any provision of these ToS.</span></p>
<ol>
<li style="font-weight: 400;"><strong>Warranty Disclaimer</strong></li>
</ol>
<p><span style="font-weight: 400;">USER ACKNOWLEDGES AND AGREES THAT THE SITE, THE SERVICES AND ANY OF ITS CONTENT, ARE PROVIDED “AS IS,” “AS AVAILABLE,” AND “WITH ALL FAULTS,” ARE USED ONLY AT YOUR SOLE RISK, TO THE FULLEST EXTENT PERMISSIBLE BY ANY APPLICABLE, LOCAL OR OTHERWISE, LAW. KeywordRevealer DISCLAIMS ALL WARRANTIES, EXPRESS OR IMPLIED, OF ANY KIND, REGARDING THE SERVICES, INCLUDING ANY IMPLIED WARRANTIES AS TO FITNESS FOR A PARTICULAR PURPOSE, MERCHANTABILITY, TITLE, NON-INFRINGEMENT, RESULTS, ACCURACY, COMPLETENESS, ACCESSIBILITY, COMPATIBILITY, SUITABILITY, RELIABILITY, AVAILABILITY, TIMELINESS, QUALITY, OR LACK OF VIRUSES. IF APPLICABLE LAW DOES NOT ALLOW THE EXCLUSION OF SOME OR ALL OF THE ABOVE IMPLIED WARRANTIES TO APPLY TO YOU, THE ABOVE EXCLUSIONS WILL APPLY TO YOU TO THE FULLEST EXTENT PERMITTED BY APPLICABLE LAW.</span></p>
<ol>
<li style="font-weight: 400;"><strong>Limitations on Liability and Remedies</strong></li>
</ol>
<p><span style="font-weight: 400;">KeywordRevealer ENTIRE LIABILITY AND YOUR EXCLUSIVE REMEDY WITH RESPECT TO ANY DISPUTE WITH KeywordRevealer (INCLUDING WITHOUT LIMITATION YOUR USE OF THE SITE AND/OR SERVICES) IS TO DISCONTINUE YOUR USE OF THE SERVICES. KeywordRevealer AND ITS AFFILIATES, OR THEIR RESPECTIVE DIRECTORS, OFFICERS, EMPLOYEES, SERVANTS OR AGENTS SHALL NOT BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL, CONSEQUENTIAL OR EXEMPLARY DAMAGE ARISING FROM YOUR USE OF THE SITE OR SERVICES OR FOR ANY OTHER CLAIM RELATED IN ANY WAY TO YOUR USE OR REGISTRATION WITH KeywordRevealer’S SITE OR SERVICES. THESE EXCLUSIONS FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, CONSEQUENTIAL OR EXEMPLARY DAMAGES INCLUDE, WITHOUT LIMITATION, DAMAGES FOR LOST PROFITS, LOST DATA, LOSS OF GOODWILL, WORK STOPPAGE, COMPUTER FAILURE OR MALFUNCTION, OR ANY OTHER COMMERCIAL DAMAGES OR LOSSES, EVEN IF KeywordRevealer   HAD BEEN ADVISED OF THE POSSIBILITY THEREOF AND REGARDLESS OF THE LEGAL OR EQUITABLE THEORY UPON WHICH THE CLAIM IS BASED. KeywordRevealer LIABILITY, AND (AS APPLICABLE) THE LIABILITY OF KeywordRevealer’S SUBSIDIARIES, OFFICERS, DIRECTORS, EMPLOYEES, AND SUPPLIERS, TO YOU OR ANY THIRD PARTIES IN ANY CIRCUMSTANCE IS LIMITED TO THE GREATER OF (A) THE AMOUNT OF FEES YOU PAY TO KeywordRevealer IN THE 1 MONTH PRIOR TO THE ACTION GIVING RISE TO LIABILITY, AND (B) $100. BECAUSE SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR THE LIMITATION OF LIABILITY FOR CONSEQUENTIAL OR INCIDENTAL DAMAGES, IN SUCH JURISDICTIONS, KeywordRevealer’S LIABILITY SHALL BE LIMITED TO THE EXTENT PERMITTED BY LAW. USER ACKNOWLEDGES AND AGREES THAT WITHOUT THE FOREGOING EXCLUSIONS AND LIMITATIONS OF LIABILITY, KeywordRevealer WOULD NOT BE ABLE TO OFFER THE SITE OR THE SERVICES.</span></p>
<ol>
<li style="font-weight: 400;"><strong>Indemnity</strong></li>
</ol>
<p><span style="font-weight: 400;">YOU AGREE TO INDEMNIFY AND HOLD KeywordRevealer AND ITS DIRECTORS, OFFICERS, EMPLOYEES, ADVISORS, SUBSIDIARIES, AFFILIATES AND AGENTS, HARMLESS FROM AND AGAINST ALL LOSS, DAMAGES, EXPENSES, CLAIMS, DEMANDS AND LIABILITIES INCURRED OR SUFFERED BY KeywordRevealer ARISING OUT OF (A) ANY REPRESENTATION MADE BY YOU TO THIRD PARTIES CREATING ANY OBLIGATION OR LIABILITY REGARDING KeywordRevealer’S SERVICES WHICH KeywordRevealer HAS NOT SPECIFICALLY ASSUMED OR APPROVED UNDER THESE TERMS, (B) YOUR’S BREACH OF ANY TERM OR CONDITION OF THESE TOS OR THE DOCUMENTS IT INCORPORATES BY REFERENCE, OR (C) YOUR FAILURE TO COMPLY WITH ALL APPLICABLE LAWS, REGULATIONS, ORDINANCES AND TREATY REQUIREMENTS, RELATING, AMONG OTHERS, TO DATA PROTECTION, PRIVACY RIGHTS, AND COPYRIGHTS.</span></p>
<ol>
<li style="font-weight: 400;"><strong>Miscellaneous</strong>
<ul>
<li style="font-weight: 400;"><strong>No Guarantee</strong><span style="font-weight: 400;">. KeywordRevealer does not guarantee continuous, uninterrupted access to the site, and operation of the site and/or Services may be interfered with by numerous factors outside KeywordRevealer ‘s control. KeywordRevealer does not guarantee your ability to connect to specific internet sites.</span></li>
<li style="font-weight: 400;"><span style="font-weight: 400;">If any provision of these ToS is held unenforceable, then such provision will be modified to reflect the parties’ intention. All remaining provisions of these Terms shall remain in full force and effect.</span></li>
<li style="font-weight: 400;"><strong>Assignment</strong><span style="font-weight: 400;">. KeywordRevealer may assign this agreement at any time to the successor in interest in connection with a merger, consolidation or other corporate reorganization.</span></li>
<li style="font-weight: 400;"><span style="font-weight: 400;">User may give notices to KeywordRevealer by email to [support@KeywordRevealer.com]</span></li>
</ul>
</li>
</ol>
<p><strong><em>KeywordRevealer – Privacy Policy</em></strong></p>
<p>Ca be found here: https://www.keywordrevealer.com/privacy</p>
<div class="main_color container_wrap_first container_wrap fullsize">
<div class="container">
<div class="post-entry post-entry-type-page post-entry-991">
<div class="entry-content-wrapper clearfix">
<div class="flex_column av_one_full flex_column_div av-zero-column-padding first avia-builder-el-9 el_after_av_hr avia-builder-el-last ">
<section class="av_textblock_section">
<div class="avia_textblock ">
<p>Last updated: May 24, 2018.</p>
</div>
</section>
</div>
</div>
</div>
</div>
</div>
</div></section></div>
</div></div></main><!-- close content main element --> <!-- section close by builder template -->       </div><!--end builder template--></div><!-- close default .container_wrap element -->
 <style>

            #footer .container {
                display: flex;
            }

            .footer_menu_wrap {
                padding: 20px 0;
            }

            .footer_menu {
                display: grid;
                grid-template-columns: repeat(3, 250px);
            }

            .menu_heading {
                margin-bottom: 12px;
            }
            
            .menu_heading > a {
                text-decoration: none;
                color: #919191;
                font-size: 18px;
            }

            .menu_heading .sub-menu {
                margin-left: 0;
            }

            .menu_heading .sub-menu a {
                font-size: 14px;
            }





            /* hubspot form styles */

            .footer_contact_form {
                padding: 20px 0;
                flex-grow: 1;
            }

            .footer_form label span {
                color: #fff !important;
            }

            .footer_form div {
                position: relative;
            }

            .footer_form .error_message {
                display: block;
                position: absolute;
                top: 0;
                right: 0;
                color: #b23737;
            }

            .footer_form input.submit_btn {
                background: #4e9fd5;
                color: #fff;
                border-radius: 3px;
                border: 1px solid #4e9fd5;
                width: 100%;
            }

            .footer_form input.submit_btn:hover {
                background: transparent;
                border-color: #4e9fd5;
                color: #4e9fd5;
            }

            .footer_form div.input input {
                color: #fff !important;
                margin-bottom: 10px !important;
            }

            @media (max-width: 767px) {
                .footer_menu {
                    grid-template-columns: repeat(1, 250px);
                }
            }

            .hs-form-booleancheckbox-display span{
                display: inline-block;
                width: 90%;
                margin-left: 10px;
            }

            .hs-form-booleancheckbox-display input{
                position: relative;
                top: -16px;
            }
            
        </style>
@stop