@extends('layouts.front.master')
@section('robots', 'noindex, nofollow')

@section('content')
    @include('front.pages.new_sections.navigation')

    <div id='main' class='all_colors' data-scroll-offset='88'>

        <div  class='main_color container_wrap_first container_wrap fullsize'   ><div class='container' ><main  role="main" itemprop="mainContentOfPage"  class='template-page content  av-content-full alpha units'><div class='post-entry post-entry-type-page post-entry-306'><div class='entry-content-wrapper clearfix'><div class="flex_column av_one_full  flex_column_div first  avia-builder-el-0  el_before_av_hr  avia-builder-el-first  " ><div style='padding-bottom:10px;color:#348fce;font-size:70px;' class='av-special-heading av-special-heading-h1 custom-color-heading blockquote modern-quote modern-centered  avia-builder-el-1  avia-builder-el-no-sibling  av-inherit-size'><h1 class='av-special-heading-tag'  itemprop="headline"  >keywordrevealer Privacy Policy</h1><div class='special-heading-border'><div class='special-heading-inner-border' style='border-color:#348fce'></div></div></div></div>
<div style='height:30px' class='hr hr-invisible  avia-builder-el-2  el_after_av_one_full  el_before_av_one_full '><span class='hr-inner ' ><span class='hr-inner-style'></span></span></div>
<div class="flex_column av_one_full  flex_column_div first  avia-builder-el-3  el_after_av_hr  avia-builder-el-last  " ><section class="av_textblock_section"  itemscope="itemscope" itemtype="https://schema.org/CreativeWork" ><div class='avia_textblock '   itemprop="text" ><p class="Default">
<p class="Default"><span style="mso-spacerun: yes;"> </span><strong><span style="font-size: 14.0pt;">Privacy Policy </span></strong></p>
<p class="Default"><span style="font-size: 11.5pt;color: blue;">1. Introduction </span></p>
<p class="Default"><span style="font-size: 11.5pt;color: blue;">2. What Personal Data we collect and how we collect It </span></p>
<p class="Default"><span style="font-size: 11.5pt;color: blue;">3. The way we use Personal Data and who we share it with </span></p>
<p class="Default"><span style="font-size: 11.5pt;color: blue;">4. How we protect your data </span></p>
<p class="Default"><span style="font-size: 11.5pt;color: blue;">5. Your rights as a “Data Subject” </span></p>
<p class="Default"><span style="font-size: 11.5pt;color: blue;">6. Additional notices </span></p>
<p class="Default"><span style="font-size: 11.5pt;color: blue;">7. Questions, concerns or complaints</span></p>
<p class="Default"><span style="font-size: 11.5pt;"><span style="mso-spacerun: yes;"> </span></span></p>
<p class="Default"><span style="font-size: 10.0pt;">This is the privacy policy (“<strong>Privacy Policy</strong>”) that governs how we, keywordrevealer . (“<strong>KeywordRevealer</strong>”, “<strong>we</strong>”, “<strong>our</strong>” or “<strong>us</strong>”), use Personal Data (defined below) that we collect, receive and store about individuals in connection with the use of: (i) the websites at www.keywordrevealer.com, </span><a href="http://www.keywordrevealer.com/"><span style="font-size: 10.0pt;">www.keywordrevealer.com</span></a><span style="font-size: 10.0pt;">, www.keywordrevealer.com and any other website that we operate (collectively, together with their sub-domains, content and services, the “<strong>Sites</strong>”); and (ii) our services, available via the Sites and our web and mobile applications (the “<strong>Service</strong>”). </span></p>
<p class="Default"><span style="font-size: 10.0pt;">We ourselves do not host any of the Sites and Service. All hosting is done by third-party service providers that we engage. This means that data you provide us or that we collect from you (including any Personal Data, as defined below) – as further described in this Privacy Policy – is hosted with such third-party service providers on servers that they own or control. Regardless of where such third-party service providers are located, their servers may be located anywhere in the world. Your data may even be replicated across multiple servers located in multiple countries. </span></p>
<p class="Default"><strong><span style="font-size: 10.0pt; font-family: 'Times New Roman',serif;"> </span></strong></p>



<p class="Default" style="page-break-before: always;"><strong><span style="font-size: 10.0pt; font-family: 'Times New Roman',serif;">1. </span></strong><strong><span style="font-size: 10.0pt;">Introduction. </span></strong></p>


<p class="Default" style="margin-bottom: 8.45pt;"><span style="font-size: 10.0pt; font-family: 'Times New Roman',serif;">1.1. </span><span style="font-size: 10.0pt;">The privacy of our users is important to us. This Privacy Policy explains our online data practices and the choices you can make about the way your Personal Data is collected and used in connection with the Sites and the Service. “<strong>Personal Data</strong>” means any information or data that may be used, either alone or in combination with other data, to personally identify an individual, including a person&#8217;s name, personal profile, e-mail address, physical address or other contact information. </span></p>
<p class="Default" style="margin-bottom: 8.45pt;"><span style="font-size: 10.0pt; font-family: 'Times New Roman',serif;">1.2. </span><span style="font-size: 10.0pt;">This Privacy Policy forms part of our Terms of Service which is available at www.keywordrevealer.com/terms (“<strong>Terms</strong>”). Any capitalized but undefined term in this Privacy Policy shall have the meaning given to it in the Terms. </span></p>
<p class="Default" style="margin-bottom: 8.45pt;"><span style="font-size: 10.0pt; font-family: 'Times New Roman',serif;">1.3. </span><span style="font-size: 10.0pt;">We strongly urge you to read this Privacy Policy and make sure that you fully understand and agree with it. If you do not agree to this Privacy Policy, please discontinue and avoid using the Sites or Service. </span></p>
<p class="Default"><span style="font-size: 10.0pt; font-family: 'Times New Roman',serif;">1.4. </span><span style="font-size: 10.0pt;">You are not legally obligated to provide us Personal Data and may do so at your own free will. If you do not agree to provide us with such data or have it processed by us or any of our service providers, please simply do not enter our Sites or use our Service. We reserve the right to change this Privacy Policy at any time at our discretion. Such change will be effective ten (10) days following posting of the revised Privacy Policy on the Sites, and your continued use of the Service thereafter means that you accept those changes. </span></p>
<p class="Default"><span style="font-size: 10.0pt;"> </span></p>

<p class="Default" style="page-break-before: always;"><strong><span style="font-size: 10.0pt; font-family: 'Times New Roman',serif;">2. </span></strong><strong><span style="font-size: 10.0pt;">What Personal Data We Collect and How We Collect It. </span></strong></p>

<p class="Default"><span style="font-size: 10.0pt;">We do not require you to provide us your Personal Data in order to access general information available on the Sites, although such activities may still be logged. However, we do receive and collect Personal Data from you in the ways listed below. We receive and collect this data as it is necessary for the adequate facilitation and performance of the contract between you and us, to provide you the Service, to secure our legitimate interests in analyzing the performance of our campaigns and Service, and to allow us to comply with our legal obligations.</span></p>
<p class="Default"><strong><span style="font-size: 10.0pt;"> </span></strong></p>
<p class="Default"><span style="font-size: 10.0pt; font-family: 'Times New Roman',serif;">2.1. </span><strong><u><span style="font-size: 10.0pt;">User Account</span></u></strong><span style="font-size: 10.0pt;">. In order to use the Service, or certain areas on the Sites, you are required to create an account (“User Account”) and provide your name and e-mail address. We may send you a text message to your mobile phone or an e-mail to the e-mail address you provide, to confirm your registration. </span></p>
<p class="Default" style="margin-bottom: 8.45pt;"><span style="font-size: 10.0pt; font-family: 'Times New Roman',serif;"> </span></p>
<p class="Default" style="margin-bottom: 8.45pt;"><span style="font-size: 10.0pt; font-family: 'Times New Roman',serif;">2.2. </span><strong><u><span style="font-size: 10.0pt;">‘Contact Us’</span></u></strong><span style="font-size: 10.0pt;"> Information. If you send us a “Contact Us” request, whether by submitting an online form on our Sites or by sending an e-mail to an e-mail address that we display, you will be asked to provide us with your contact information (name, e-mail address and phone number). </span></p>
<p class="Default" style="margin-bottom: 8.45pt;"><span style="font-size: 10.0pt; font-family: 'Times New Roman',serif;">2.3. </span><strong><u><span style="font-size: 10.0pt;">Usage Data</span></u></strong><span style="font-size: 10.0pt;">. We collect data about how you are accessing and using the Sites and Service, which include administrative and support communications with us and information about the services you interact with, and what third-party integrations you use (if any). Such information may include Personal Data. </span></p>
<p class="Default" style="margin-bottom: 8.45pt;"><span style="font-size: 10.0pt; font-family: 'Times New Roman',serif;">2.4. </span><strong><u><span style="font-size: 10.0pt;">Data We Receive from Third Parties</span></u></strong><span style="font-size: 10.0pt;">. We may receive certain data, including Personal Data, from third parties, such as service providers, ad networks and marketing agencies, which all relate to your preferences and your use of the Sites and Service. </span></p>
<p class="Default"><span style="font-size: 10.0pt; font-family: 'Times New Roman',serif;">2.5. </span><strong><u><span style="font-size: 10.0pt;">Purchases</span></u></strong><span style="font-size: 10.0pt;">. The Service includes the option to purchase certain services from us. If you choose to make a purchase, we will require certain information from you as necessary to complete the transaction. Such information could include a credit card number and related account and billing information, invoice related information, and other data required to process the order. We will also update such data should you grant us permission to bill you or your credit card for recurring charges, such as monthly or other types of periodic payments. We use third-party service providers to process transactions, in which case </span></p>
<p class="Default"><span style="font-size: 10.0pt;"> </span></p>
<p class="Default" style="margin-bottom: 8.45pt;"><span style="font-size: 10.0pt; font-family: 'Times New Roman',serif;">2.6. </span><strong><u><span style="font-size: 10.0pt;">Log Files</span></u></strong><span style="font-size: 10.0pt;">. We make use of log files. The data inside the log files includes internet protocol (IP) addresses, type of browser, your device’s type, Internet Service Provider (ISP), date/time stamp, referring/exit pages, clicked pages and any other information your browser send to us. We use such data to analyze trends, administer the Sites and Service, track user movement in the Sites and Service and gather statistical data. </span></p>
<p class="Default" style="margin-bottom: 8.45pt;"><span style="font-size: 10.0pt; font-family: 'Times New Roman',serif;">2.7. </span><strong><u><span style="font-size: 10.0pt;">Cookies and Other Tracking Technologies</span></u></strong><span style="font-size: 10.0pt;">. Our Sites and Service utilize “cookies”, anonymous identifiers and other tracking technologies in order for us to provide our Service, analyze our performance and personalize your experience. A “cookie” is a small text file that is used, for example, to collect data about activity on our Sites. Certain cookies and other technologies serve to recall Personal Data, such as an IP address, previously indicated by a user. Please note that we do not change our practices in response to a “Do Not Track” signal in the HTTP header from a browser or mobile application, however, most browsers allow you to control cookies, including whether or not to accept them and how to remove them. You may set most browsers to notify you if you receive a cookie, or you may choose to block cookies with your browser. </span></p>
<p class="Default" style="margin-bottom: 8.45pt;"><span style="font-size: 10.0pt; font-family: 'Times New Roman',serif;">2.8. </span><strong><span style="font-size: 10.0pt;">Services Integrations</span></strong><span style="font-size: 10.0pt;">. If, when using the Service, you integrate with a third-party service, we will connect that third-party service to ours. The third-party provider of the integration shares certain relevant data about your account with us. </span></p>
<p class="Default" style="margin-bottom: 8.45pt;"><span style="font-size: 10.0pt; font-family: 'Times New Roman',serif;">2.9. </span><strong><u><span style="font-size: 10.0pt;">Analytics</span></u></strong><span style="font-size: 10.0pt;">. We use analytics tools (e.g. Google Analytics) to collect data about the use of the Sites and Service. Analytics tools collect data such as on how often users visit the Sites or Service, what pages they visit when they do so and what website they directly came from to the Sites or Service. We use the data we get from these tools to maintain and improve the Sites and Service and our other products. </span></p>
<p class="Default"><span style="font-size: 10.0pt;"> </span></p>
<p class="Default"><strong><span style="font-size: 10.0pt; font-family: 'Times New Roman',serif;">3. </span></strong><strong><span style="font-size: 10.0pt;">The Way We Use Personal Data and Who We Share It With. </span></strong></p>
<p class="Default"><span style="font-size: 10.0pt;">If you submit or we collect Personal Data through the Sites and Service, then we will use such Personal Data in the following ways, via any communication channel available to us, including e-mail, SMS, etc.</span></p>
<p class="Default"><span style="font-size: 10.0pt;"> </span></p>
<p class="Default" style="margin-bottom: 8.45pt;"><span style="font-size: 10.0pt; font-family: 'Times New Roman',serif;">3.1. </span><span style="font-size: 10.0pt;">We will use your Personal Data to provide and improve our Service, to identify and authenticate your access to certain non-public parts of the Sites and Service, </span></p>
<p class="Default" style="margin-bottom: 8.45pt;"><span style="font-size: 10.0pt; font-family: 'Times New Roman',serif;">3.2. </span><span style="font-size: 10.0pt;">We will use your designated e-mail address to: (i) send you updates and news regarding the Service and our products; or (ii) to send you marketing/advertisement communications that we believe may interest you. You may opt-out of such communications at any time, as further explained in Section 5.2 below. </span></p>
<p class="Default" style="margin-bottom: 8.45pt;"><span style="font-size: 10.0pt; font-family: 'Times New Roman',serif;">3.3. </span><span style="font-size: 10.0pt;">We will send you service and administrative e-mails and messages, including to inform you about changes in our Sites or Service, to provide you with important Service related notices, such as security notices, and to respond to a &#8220;Contact Us&#8221; or administrative request (for example, to change your password). These e-mails and messages are considered an integral part of the Service and you may not opt-out of them. </span></p>
<p class="Default" style="margin-bottom: 8.45pt;"><span style="font-size: 10.0pt; font-family: 'Times New Roman',serif;">3.4. </span><span style="font-size: 10.0pt;">If you choose to send others an e-mail or message inviting them to use the Service, we will use the contact information you provide us to automatically send such invitation e-mail or message. Your name and e-mail address may be included in the invitation e-mail or message. </span></p>
<p class="Default" style="margin-bottom: 8.45pt;"><span style="font-size: 10.0pt; font-family: 'Times New Roman',serif;">3.5. </span><span style="font-size: 10.0pt;">We will transfer your Personal Data to our local or foreign subsidiaries and affiliated companies for the purpose of storing and processing such data on our behalf. Such data may be transferred to other countries. These parties are committed to process such data in accordance with this Privacy Policy. </span></p>
<p class="Default" style="margin-bottom: 8.45pt;"><span style="font-size: 10.0pt; font-family: 'Times New Roman',serif;">3.6. </span><span style="font-size: 10.0pt;">We will share or provide access to your Personal Data to our selected third-party service providers and partners, some of which are situated outside of the EEA, but only to assist us with our business operations and to provide our Service to you and other users. Such service providers include hosting and server co-location services, communications and content delivery networks (CDNs), data and cyber security services, DDoS prevention services, billing and payment processing services, domain name registrars, fraud detection and prevention services, web analytics, e-mail distribution and monitoring services, session recording and remote access services, performance measurement data optimization and marketing services, content providers, e-mail, voicemails, support and customer relation management systems (CRM), and our legal and financial advisors. This means that your data may be transferred to other countries. We will typically engage only with such third-party service providers and partners that post a privacy policy governing their processing of Personal Data. </span></p>
<p class="Default" style="margin-bottom: 8.45pt;"><span style="font-size: 10.0pt; font-family: 'Times New Roman',serif;">3.7. </span><span style="font-size: 10.0pt;">We may disclose your Personal Data or any information you submit via the Sites and Service per your specific instructions or requests. </span></p>
<p class="Default" style="margin-bottom: 8.45pt;"><span style="font-size: 10.0pt; font-family: 'Times New Roman',serif;">3.8. </span><span style="font-size: 10.0pt;">We may share your contact details with our business partners and affiliates, so they can contact you in order to offer or promote our Service. </span></p>
<p class="Default" style="margin-bottom: 8.45pt;"><span style="font-size: 10.0pt; font-family: 'Times New Roman',serif;">3.9. </span><span style="font-size: 10.0pt;">We may disclose your Personal Data if we have a good faith belief that disclosure of such data is reasonably necessary to: (i) comply with any applicable law, regulation, legal process or governmental request; (ii) enforce our Terms, including investigations of potential violations thereof; (iii) detect, prevent, or otherwise address fraud or security issues; or (iv) protect against harm to the rights, property or safety of keywordrevealer.com, our users, yourself or the public. </span></p>
<p class="Default" style="margin-bottom: 8.45pt;"><span style="font-size: 10.0pt; font-family: 'Times New Roman',serif;">3.10. </span><span style="font-size: 10.0pt;">If you contact us for help in resolving an issue specific to a team of which you are a member (and which is managed by your Organization), then given our relationship with your Organization we may share your concern with them. </span></p>
<p class="Default"><span style="font-size: 10.0pt; font-family: 'Times New Roman',serif;">3.11. </span><span style="font-size: 10.0pt;">We use Anonymous Data (as defined below) and disclose it to third-party service providers in order to improve our Sites and Service, to perform random tests for additional features and to enhance your user experience. We also disclose Anonymous Data (with or without compensation) to third parties, including advertisers and partners. “<strong>Anonymous Data</strong>” means data which does not enable the identification of an individual user, such as aggregated data about the use of our Sites and Service. This Privacy Policy is not intended to place any limits on what we do with data that is aggregated and de-identified, so it is no longer associated with an identifiable person. </span></p>
<p class="Default"><span style="font-size: 10.0pt;"> </span></p>
<p class="Default"><span style="font-size: 10.0pt; font-family: 'Times New Roman',serif;">3.12. </span><span style="font-size: 10.0pt;">In the event that we are acquired by or merged with a third-party entity, or in the event of bankruptcy or a comparable event, we reserve the right to transfer or assign Personal Data and any other information you provided us in connection with such events. </span></p>
<p class="Default"><span style="font-size: 10.0pt;"> </span></p>
<p class="Default"><span style="font-size: 10.0pt;"> </span></p>
<p class="Default"><strong><span style="font-size: 10.0pt; font-family: 'Times New Roman',serif;">4. How we Protect your Data </span></strong></p>
<p class="Default"><span style="font-size: 10.0pt; font-family: 'Times New Roman',serif;">4.1. </span><span style="font-size: 10.0pt;">Security. The security of your Personal Data is important to us. We follow generally accepted industry standards to protect the Personal Data submitted to us. However, no method of transmission over the Internet or method of electronic storage is 100% secure. Therefore, we cannot guarantee its absolute security or confidentiality. If you have any questions about security on the Sites or Service, you can contact us at </span><a href="mailto:support@keywordrevealer.com"><span style="font-size: 10.0pt;">support@keywordrevealer.com</span></a><span style="font-size: 10.0pt;">.</span></p>
<p class="Default"><span style="font-size: 10.0pt;"> </span></p>
<p class="Default"><strong><span style="font-size: 10.0pt; font-family: 'Times New Roman',serif;">5. Your Rights as a “Data Subject” </span></strong></p>
<p class="Default" style="margin-bottom: 8.45pt;"><span style="font-size: 10.0pt; font-family: 'Times New Roman',serif;">5.1. </span><span style="font-size: 10.0pt;">Data Subject Rights To the extent that you wish to exercise your rights under applicable law (e.g. the EU GDPR) to request access to and rectification or erasure of your Personal Data held with keywordrevealer.com, or to restrict or object to such Personal Data&#8217;s processing, or to port such Personal Data &#8211; please contact your Organization’s Admin who will typically have control of your Personal Data. You can also contact our support team at support@keywordrevealer.com. </span></p>
<p class="Default"><span style="font-size: 10.0pt;"> </span></p>
<p class="Default"><span style="font-size: 10.0pt; font-family: 'Times New Roman',serif;">5.2. </span><strong><u><span style="font-size: 10.0pt;">Opting Out</span></u></strong><span style="font-size: 10.0pt;">. You may choose not to receive future promotional or advertising, or certain Service-related e-mails from us by clicking the &#8220;unsubscribe&#8221; or &#8220;change email preferences&#8221; link at the bottom of each e-mail that we send and changing such preferences in your account. Please note that even if you opt out of receiving the foregoing e-mails, we will still send you a response to any “Contact Us” request as well as administrative e-mails (for example, in connection with a password reset request) that are necessary to facilitate your use of the Sites and Service, as explained in Section 3.3 above. </span></p>
<p class="Default"><span style="font-size: 10.0pt; font-family: 'Times New Roman',serif;">5.3. </span><strong><u><span style="font-size: 10.0pt;">Choice</span></u></strong><span style="font-size: 10.0pt;">. At all times, you may choose whether or not to provide or disclose Personal Data. If you choose not to provide Personal Data which is mandatory in order to fully use our Service, you may still visit parts of the Sites and Service which are available to the general public, but you will not be able to access certain options, programs, and services that require authentication or involve our interaction with you. </span></p>
<p class="Default"><span style="font-size: 10.0pt;"> </span></p>
<p class="Default"><strong><span style="font-size: 10.0pt; font-family: 'Times New Roman',serif;">6. Additional notices </span></strong></p>
<p class="Default" style="margin-bottom: 8.45pt;"><span style="font-size: 10.0pt; font-family: 'Times New Roman',serif;">6.1. </span><span style="font-size: 10.0pt;">Links to and Interaction with Third Party Products. The Sites and Service may enable you to interact with or contain links to your user accounts with other third-party websites, mobile software applications and services that are not owned or controlled by us (each a “Third Party Service”). We are not responsible for the privacy practices or the content of such Third Party Services. Please be aware that Third Party Services may collect Personal Data from you. Accordingly, we encourage you to read the terms and conditions and privacy policy of each Third Party Service that you choose to use or interact with. </span></p>
<p class="Default" style="margin-bottom: 8.45pt;"><span style="font-size: 10.0pt; font-family: 'Times New Roman',serif;">6.2. </span><span style="font-size: 10.0pt;">Storage and deletion. We retain data for as long as we provide you the Service, and thereafter in accordance with our Data Retention Policy. If you want us to delete your Personal Data, or if you want to learn more about where and how long your Personal Data is stored, and for more information on your rights of erasure and portability, please contact support@keywordrevealer.com. </span></p>
<p class="Default"><span style="font-size: 10.0pt; font-family: 'Times New Roman',serif;"> </span></p>
<p class="Default" style="margin-bottom: 8.45pt;"><span style="font-size: 10.0pt; font-family: 'Times New Roman',serif;">6.3. </span><span style="font-size: 10.0pt;">Children’s Privacy. The Sites and Service are not structured to attract children under the age of 16, and we do not intend to collect Personal Data from anyone we know to be under 16. If we learn that we have collected Personal Data from anyone under 16 years, we will delete that information as quickly as possible. If you believe that we might have any such information, please contact us at support@keywordrevealer.com. </span></p>
<p class="Default"><span style="font-size: 10.0pt;"> </span></p>
<p class="Default"><strong><span style="font-size: 10.0pt; font-family: 'Times New Roman',serif;">7. Questions, concerns or complaints </span></strong></p>
<p class="Default"><span style="font-size: 10.0pt;">If you have any comments or questions regarding our Privacy Policy or if you have any concerns regarding your Privacy, you can contact keywordrevealer’s support at support@keywordrevealer.com. If you are a GDPR-protected individual, you also have the right to lodge a complaint with an EU supervisory authority. </span></p>
<p class="MsoNormal"><strong><em><span style="font-size: 10.0pt; line-height: 107%;"> </span></em></strong></p>
<p class="MsoNormal"><strong><em><span style="font-size: 10.0pt; line-height: 107%;">Last updated: May 24<sup>rd</sup>, 2018</span></em></strong></p>
</div></section></div>
</div></div></main><!-- close content main element --> <!-- section close by builder template -->		</div><!--end builder template--></div><!-- close default .container_wrap element -->
        <style>

            #footer .container {
                display: flex;
            }

            .footer_menu_wrap {
                padding: 20px 0;
            }

            .footer_menu {
                display: grid;
                grid-template-columns: repeat(3, 250px);
            }

            .menu_heading {
                margin-bottom: 12px;
            }
            
            .menu_heading > a {
                text-decoration: none;
                color: #919191;
                font-size: 18px;
            }

            .menu_heading .sub-menu {
                margin-left: 0;
            }

            .menu_heading .sub-menu a {
                font-size: 14px;
            }





            /* hubspot form styles */

            .footer_contact_form {
                padding: 20px 0;
                flex-grow: 1;
            }

            .footer_form label span {
                color: #fff !important;
            }

            .footer_form div {
                position: relative;
            }

            .footer_form .error_message {
                display: block;
                position: absolute;
                top: 0;
                right: 0;
                color: #b23737;
            }

            .footer_form input.submit_btn {
                background: #4e9fd5;
                color: #fff;
                border-radius: 3px;
                border: 1px solid #4e9fd5;
                width: 100%;
            }

            .footer_form input.submit_btn:hover {
                background: transparent;
                border-color: #4e9fd5;
                color: #4e9fd5;
            }

            .footer_form div.input input {
                color: #fff !important;
                margin-bottom: 10px !important;
            }

            @media (max-width: 767px) {
                .footer_menu {
                    grid-template-columns: repeat(1, 250px);
                }
            }

            .hs-form-booleancheckbox-display span{
                display: inline-block;
                width: 90%;
                margin-left: 10px;
            }

            .hs-form-booleancheckbox-display input{
                position: relative;
                top: -16px;
            }
            
        </style>
@stop