@if(Route::currentRouteName() != 'front.pages.landing')
<div class="card_section">
 <div class="container">
  <div class="row">
   <div class="col-md-12 col-sm-12">
    <div class="card"><img src="{{ asset('assets/frontend/new_html/images/card.png') }}" class="img-responsive"> <h3>100% Secure with 14-Day Money Back Guarantee</h3></div>
   </div><!-- col end here -->  
  </div><!-- row end here -->
 </div><!-- container end here -->
</div><!-- card_section end here -->
@endif

<div class="question_section" style="{{ Route::currentRouteName() == 'front.pages.landing' ? 'margin:0;-webkit-padding-before:100px;' : ''}}">
 <h3>Frequently Asked Questions</h3>
 <center><img src="{{ asset('assets/frontend/new_html/images/line1.jpg') }}" class="img-responsive"></center>
 
 <div class="container">
  <div class="row">
   <div class="col-md-12 col-sm-12">
    <div class="accordion_container">   
     <div class="accordion_head">01. Can I cancel anytime or change my subscription plan?<i class="fa fa-chevron-down plan-view"></i></div> 
     <div style="display: none;" class="accordion_body">
      <p>Yes. KeywordRevealer is a pay-as-you-go service. There are no long term contracts or commitments to worry about.</p> 
      <p>You can change or cancel your subscription plan at anytime right from your <a href="https://www.keywordrevealer.com/auth/profile"><b>account dashboard</b></a>. Just <a href="https://www.keywordrevealer.com/contact"><b>contact us</b></a> if you have any questions and our support team will be happy to help.</p>
     </div><!-- accordion_body end here -->
     
     <div class="accordion_head">02. How does the money-back guarantee work?<i class="fa fa-chevron-down plan-view"></i></div> 
     <div style="display: none;" class="accordion_body">
      <p>We offer a full money-back guarantee within 14 days of signing up. Just contact our <a href="https://www.keywordrevealer.com/contact"><b
        >support team</b></a> and we will refund you 100% of your initial payment. We stand behind this guarantee because we're that confident you'll love what KeywordRevealer has to offer!</p>
     </div><!-- accordion_body end here -->
     
     <div class="accordion_head">03. Will I receive free updates to Keyword Revealer?<i class="fa fa-chevron-down plan-view"></i></div> 
     <div style="display: none;" class="accordion_body">
       <p>Yes. We are always working to improve our service by adding more features and functionality. All active customers will have access to all features within their plan, without any additional fees. </p>

     </div><!-- accordion_body end here -->
    </div><!-- accordion_container end here -->
    
    <div class="still"><h3>Still Have Questions?</h3> <p>Feel Free to</p></div>
    <center>
	 <a href="{{ URL::route('front.contact') }}" class="get_btn">Contact Us</a>
	</center>
   </div><!-- col end here -->  
  </div><!-- row end here -->
 </div><!-- container end here -->
</div><!-- question_section end here -->