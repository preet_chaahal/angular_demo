<div class="our_client">
 <h3>What Our Clients Say</h3>
 <center><img src="{{ asset('assets/frontend/new_html/images/line.jpg') }}" class="img-responsive"></center>
     
 <div id="testimonial-slider" class="owl-carousel">
  <div class="testimonial">
   <div class="container">
    <div class="row">
     <div class="col-md-6 col-sm-6">
      <div class="text"><img src="{{ asset('assets/frontend/img/new_design/client_1.png') }}" class="img-circle img-responsive"> <h3>Swaraj Nandedkar</h3> <p>SEO Specialist</p></div>
      <p>"The Keyword Revealer is an Amazing keyword Research tool for every Internet Marketer. With Great User Interface and the fantastic Keyword Research results, it helped me to build up my Niche site and make it big. In the times when Google Adwords Keyword Planner" tool stopped showing exact search volume to the Non-Advertisers, Keyword Revealer Helped me to come up with my Keyword Research."</p>
     </div><!-- col end here -->
    
     <div class="col-md-6 col-sm-6">
       <div class="text"><img src="{{ asset('assets/frontend/img/new_design/client_2.png') }}" class="img-circle img-responsive"> <h3>John Zoro</h3> <p>Business Owner</p></div>
      <p>"I love keyword revealer. It's so easy to use and accurate! I recommend it to all my friends and coworkers as the best keyword research tool!"</p>
     </div><!-- col end here -->  
    </div><!-- row end here --> 
   </div><!-- container end here -->  
  </div><!-- testimonial end here -->
  
  <div class="testimonial">
   <div class="container">
    <div class="row">
     <div class="col-md-6 col-sm-6">
      <div class="text"><img src="{{ asset('assets/frontend/img/new_design/client_3.png') }}" class="img-circle img-responsive"> <h3>John Green</h3> <p>SEO Agency</p></div>
      <p>"I have been using keyword revealer for quite a few months and I must say, its my go to software to find and examine keywords! Whenever I think I found a good seed keyword, I head over to keywordrevealer, as I know ill be able to find a lot more keywords with the seed keyword! This tool is great for finding the difficulty of keywords as well, which helps me determine if I should build a site on the keyword or not, which makes keyword revealer one of the best sites I have used!"</p>
     </div><!-- col end here -->
     
     <div class="col-md-6 col-sm-6">
      <div class="text"><img src="{{ asset('assets/frontend/img/new_design/client_4.png') }}" class="img-circle img-responsive"> <h3>David Kim</h3> <p>SEO Consultant</p></div>
      <p>"I used Keyword Revealer above all other keyword research tools out there for quick and extensive results. I rarely needed other tools for my extensive keyword research tasks. Thanks for this wonderful tool."</p>
     </div><!-- col end here -->  
    </div><!-- row end here --> 
   </div><!-- container end here -->  
  </div><!-- testimonial end here -->
  
  <div class="testimonial">
   <div class="container">
    <div class="row">
     <div class="col-md-6 col-sm-6">
      <div class="text"><img src="{{ asset('assets/frontend/img/new_design/client_5.png') }}" class="img-circle img-responsive"> <h3>Earl Gardner</h3> <p>Business Owner</p></div>
      <p>"I have been using KeywordRevealer for quite a while now, and it saves me so much time when it comes to identifying primary and secondary keywords to target. I often recommend it to others, especially to members of our Facebook SEO group."</p>
     </div><!-- col end here -->
    
      <div class="col-md-6 col-sm-6">
      <div class="text"><img src="{{ asset('assets/frontend/img/new_design/client_1.png') }}" class="img-circle img-responsive"> <h3>Swaraj Nandedkar</h3> <p>SEO Specialist</p></div>
      <p>"The Keyword Revealer is an Amazing keyword Research tool for every Internet Marketer. With Great User Interface and the fantastic Keyword Research results, it helped me to build up my Niche site and make it big. In the times when Google Adwords Keyword Planner" tool stopped showing exact search volume to the Non-Advertisers, Keyword Revealer Helped me to come up with my Keyword Research."</p>
     </div><!-- col end here -->
    </div><!-- row end here --> 
   </div><!-- container end here -->  
  </div><!-- testimonial end here -->
  
 
 </div><!-- testimonial-slider end here -->
</div><!-- our_client end here -->