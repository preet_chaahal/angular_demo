<footer>
 <div class="container">
  <div class="row">
   <div class="col-md-2 col-sm-1"></div>
   
   <div class="col-md-8 col-sm-10">
    <img src="{{ asset('assets/frontend/new_html/images/logo_2.png') }}" class="img-responsive">
    <p>KeywordRevealer is dedicated to providing actionable data to help fuel & recharge your keyword research and search marketing campaigns. Armed with the right data, SEO’s, digital agencies and bloggers alike can prioritize their plan of attack and discover keyword ideas to level the playing field in some of the most competitive verticals.</p>
    
    <div class="social">
     <a href="https://www.facebook.com/keywordrevealer"><i class="fa fa-facebook"></i></a>
     <a href="https://twitter.com/keywordrevealer"><i class="fa fa-twitter"></i></a>
    </div>
   </div><!-- col end here -->  
  </div><!-- row end here -->
 </div><!-- container end here -->
</footer><!-- footer end here -->