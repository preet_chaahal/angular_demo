	@if (request()->is('/') || Route::currentRouteName() == 'front.pages.landing')
	   <header>
	@else
		<header class="inner_header">
	@endif
	
 <div class="container">
  <div class="row">
	   <div class="col-md-3 col-sm-4">
			@if (request()->is('/') || Route::currentRouteName() == 'front.pages.landing')
		    <a href="{{ URL::route('front.home') }}"><img src="{{ asset('assets/frontend/new_html/images/logo.png') }}" alt="Keyword Revealer" class="img-responsive logo"></a>
			<a href="{{ URL::route('front.home') }}"><img src="{{ asset('assets/frontend/new_html/images/logo_1.png') }}" alt="Keyword Revealer" class="img-responsive logo_1"></a>
			@else
				 <a href="{{ URL::route('front.home') }}"><img src="{{ asset('assets/frontend/new_html/images/inner_logo_new.png') }}" alt="Keyword Revealer" class="img-responsive logo"></a>
			  <a href="{{ URL::route('front.home') }}"><img src="{{ asset('assets/frontend/new_html/images/inner_logo_new.png') }}" alt="Keyword Revealer" class="img-responsive logo_1"></a>
			@endif
   	   </div><!-- col end here -->
    
   <div class="col-md-9 col-sm-8">
    <ul class="menu">
     <li>
      <!-- Navbar Start Here -->
      <div id="netsec_menu">
	   <div class="navbar netsec_navbar">
	    <nav class="navbar-default netsec_navbar-default" role="navigation">
	     <!-- Toggle button for mobile display -->
	     <div class="navbar-header">
	      <button type="button" class="navbar-toggle netsec_navbar-toggle collapsed" data-toggle="collapse" data-target="#netsec_mainmenu">
	       <span class="sr-only">Toggle navigation</span> 
		   <span class="icon-bar"></span> 
		   <span class="icon-bar"></span> 
		   <span class="icon-bar"></span>
		  </button>                                                                        
	     </div>
	     <!-- Menu Content -->
         <div class="collapse navbar-collapse" id="netsec_mainmenu">
          <ul class="nav navbar-nav netsec_menucontent">
          	@if(Route::currentRouteName() != 'front.pages.landing' && Route::currentRouteName() != 'front.pages.landing.pricing')
           <li><a href="https://www.keywordrevealer.com/#pricing">Pricing</a></li>
           <li><a href="https://www.keywordrevealer.com/blog/">Blog</a></li>            
           <li><a href="{{ URL::route('front.contact') }}">Contact</a></li>
           <li><a href="https://docs.keywordrevealer.com/">Support</a></li>                                           
           @endif
           <li class="sign_1">
		   
				@if (Auth::check()) 
					<a href="{{ URL::route('backend.tools.keywordresearch') }}">Research</a>
				 @else
					 <a href="{{ URL::route('auth.login') }}">Sign In</a>
				@endif
		  
		   </li>                                           
          </ul><!-- ul end here -->
         </div>
	    </nav><!-- nav end here -->
	   </div>
      </div>
      <!-- Navbar End Here -->
     </li><!-- li end here -->
     
     <li>
	 @if (Auth::check()) 
	 <a class="sign_btn" href="{{ URL::route('backend.tools.keywordresearch') }}">Research</a>
	 @else
	 <a class="sign_btn" href="{{ URL::route('auth.login') }}">Sign In</a>
	@endif
	 
	 </li>
    </ul><!-- ul end here --> 
   </div><!-- col end here -->
  </div><!-- row end here -->
 </div><!-- container end here -->
</header><!-- header End Here -->