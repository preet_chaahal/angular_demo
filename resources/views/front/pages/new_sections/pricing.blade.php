<div class="pricing_plan" id="pricing">
 <h3>Plans & Pricing</h3>
 <p class="plan_offer">Want to save up to 40%? Select one of our annual plans!</p>
 <center><img src="{{ asset('assets/frontend/new_html/images/line.jpg') }}" class="img-responsive"></center>
 <div class="container"> 
 <!--------For mobile view price table---------------->
   <div class="row">
   <div class="col-md-12 col-sm-12">
    <div class="inter" style="overflow-x:auto;">
      <?php
        $tp = false;
        if(isset($data_trial) && !empty($data_trial)) {
          $tp = true;
        }
      ?>
     <table>
      <tr>
       <td width="33.33%">Select PLan</td>
       @if($tp == true)
       <td width="16.66%">{{ (isset($data_trial['role']->name) ? ucfirst($data_trial['role']->name) : '')}}</td> 
       @endif
       <?php $k = 1; ?>
       @foreach($data as $m_row)
       <?php
        if($k == 4) {
          break;
        }
        $pname2 = explode("_", $m_row['role']->screen_name);
       ?>
       <td width="16.66%">{{(isset($pname2[0]) ? $pname2[0] : '')}}</td>
       <?php $k++; ?>
       @endforeach
      </tr><!-- tr end here -->
      <tr>
        <td>Monthly <label class="switch"><span class="toggle"></span></label> <span>Annual (save 40%)</span></td>
        <?php $i2 = 1; ?>
        @if ($tp == true) 
          <td><span>$</span>0<strong>/m</strong></td>
        @endif       
        @foreach($data as $m_row02)
           <?php
            if($i2 == 4) {
              break;
            }
           ?>        
            <td> <li class="annually"><span>$</span>{{ number_format(($m_row02['chargebee']['price'] /100), 2, '.', ' ') }}<strong></strong> <b> Paid Annually</b></li>
            <li class="monthly active"><span>$</span>{{ number_format(($m_row02['chargebee']['price'] /100), 2, '.', ' ') }}<strong>/m</strong> <b> Paid Monthly</b></li></td> 
         <?php $i2++; ?>
        @endforeach
      </tr><!-- tr end here -->

      <tr>
       <td>Daily Searches</td>
       @if($tp == true)
       <td>{{ (isset($data_trial['role_limits']['research']) ? $data_trial['role_limits']['research'] : 0)}}</td>
       @endif
       <td>{{(isset($data[0]['role_limits']['research']) ? $data[0]['role_limits']['research'] : '')}}</td>
       <td>{{(isset($data[1]['role_limits']['research']) ? $data[1]['role_limits']['research'] : '')}}</td>
       <td>{{(isset($data[2]['role_limits']['research']) ? $data[2]['role_limits']['research'] : '')}}</td>
      </tr><!-- tr end here -->

      <tr>
       <td>Daily Evaluations</td>
       @if($tp == true) 
       <td>{{ (isset($data_trial['role_limits']['evolution']) ? $data_trial['role_limits']['evolution'] : 0)}}</td>
       @endif
       <td>{{(isset($data[0]['role_limits']['evolution']) ? $data[0]['role_limits']['evolution'] : '')}}</td>
       <td>{{(isset($data[1]['role_limits']['evolution']) ? $data[1]['role_limits']['evolution'] : '')}}</td>
       <td>{{(isset($data[2]['role_limits']['evolution']) ? $data[2]['role_limits']['evolution'] : '')}}</td>
      </tr><!-- tr end here -->

      <tr>
       <td>Brainstorming Usage</td>
       @if($tp == true)
       <td>{{ (isset($data_trial['role_limits']['brainstorm']) ? $data_trial['role_limits']['brainstorm'] : 0)}}</td>
       @endif 
       <td>{{(isset($data[0]['role_limits']['brainstorm']) ? $data[0]['role_limits']['brainstorm'] : '')}}</td>
       <td>{{(isset($data[1]['role_limits']['brainstorm']) ? $data[1]['role_limits']['brainstorm'] : '')}}</td>
       <td>{{(isset($data[2]['role_limits']['brainstorm']) ? $data[2]['role_limits']['brainstorm'] : '')}}</td>
      </tr><!-- tr end here -->

      <tr>
       <td>Rank Tracker (keywords)</td>
       @if($tp == true)
       <td>{{ (isset($data_trial['role_limits']['tracking']) ? $data_trial['role_limits']['tracking'] : 0)}}</td>
       @endif 
       <td>{{(isset($data[0]['role_limits']['tracking']) ? $data[0]['role_limits']['tracking'] : '')}}</td>
       <td>{{(isset($data[1]['role_limits']['tracking']) ? $data[1]['role_limits']['tracking'] : '')}}</td>
       <td>{{(isset($data[2]['role_limits']['tracking']) ? $data[2]['role_limits']['tracking'] : '')}}</td>        
      </tr><!-- tr end here -->

      <tr>
       <td>Save Projects</td>
       @if($tp == true)
        @if(isset($data_trial['role_limits']['keyword_save']) && $data_trial['role_limits']['keyword_save'] == 1)
          <td>{{ (isset($data_trial['role_limits']['project']) ? $data_trial['role_limits']['project'] : 0) }}</td>
        @else
          <td><i class="fa fa-times"></i></td>
        @endif
       @endif

       @if(isset($data[0]['role_limits']['can_save']) && $data[0]['role_limits']['can_save'])
          <td>{{$data[0]['role_limits']['can_save']}}</td>
       @else
          <td><i class="fa fa-times"></i></td>
       @endif

       @if(isset($data[1]['role_limits']['can_save']) && $data[1]['role_limits']['can_save'])
          <td>{{$data[1]['role_limits']['can_save']}}</td>
       @else
          <td><i class="fa fa-times"></i></td>
       @endif

       @if(isset($data[2]['role_limits']['can_save']) && $data[2]['role_limits']['can_save'])
          <td>{{$data[2]['role_limits']['can_save']}}</td>
       @else
          <td><i class="fa fa-times"></i></td>
       @endif
      </tr><!-- tr end here -->

      <tr>
       <td>Save Keywords</td>
      @if($tp == true) 
        @if(isset($data_trial['role_limits']['keyword_save']) && $data_trial['role_limits']['keyword_save'] == 1)
          <td>{{ (isset($data_trial['role_limits']['can_save']) ? $data_trial['role_limits']['can_save'] : 0) }}</td>
        @else
          <td><i class="fa fa-times"></i></td>
        @endif
      @endif   

       @if(isset($data[0]['role_limits']['project']) && $data[0]['role_limits']['project'])
          <td>{{$data[0]['role_limits']['project']}}</td>
       @else
          <td><i class="fa fa-times"></i></td>
       @endif

       @if(isset($data[1]['role_limits']['project']) && $data[1]['role_limits']['project'])
          <td>{{$data[1]['role_limits']['project']}}</td>
       @else
          <td><i class="fa fa-times"></i></td>
       @endif

       @if(isset($data[2]['role_limits']['project']) && $data[2]['role_limits']['project'])
          <td>{{$data[2]['role_limits']['project']}}</td>
       @else
          <td><i class="fa fa-times"></i></td>
       @endif
      </tr><!-- tr end here -->

      <tr>
       <td>Exact Match Domain Finder</td>
       @if($tp == true) 
         @if(isset($data_trial['role_limits']['domain_finder']) && $data_trial['role_limits']['domain_finder'] == 1)  
         <td><i class="fa fa-check"></i></td>
         @else
         <td><i class="fa fa-times"></i></td>
         @endif
       @endif

       @if(isset($data[0]['role_limits']['domain_finder']) && $data[0]['role_limits']['domain_finder'] == '1')
       <td><i class="fa fa-check"></i></td>
       @else
       <td><i class="fa fa-times"></i></td>
       @endif

       @if(isset($data[1]['role_limits']['domain_finder']) && $data[1]['role_limits']['domain_finder'] == '1')
       <td><i class="fa fa-check"></i></td>
       @else
       <td><i class="fa fa-times"></i></td>
       @endif

       @if(isset($data[2]['role_limits']['domain_finder']) && $data[2]['role_limits']['domain_finder'] == '1')
       <td><i class="fa fa-check"></i></td>
       @else
       <td><i class="fa fa-times"></i></td>
       @endif
      </tr><!-- tr end here -->

      <tr>
       <td>Bulk Keyword Upload</td>
       @if($tp == true) 
         @if(isset($data_trial['role_limits']['import_keywords']) && $data_trial['role_limits']['import_keywords'] == 1)
         <td><i class="fa fa-check"></i></td>
         @else
         <td><i class="fa fa-times"></i></td>
         @endif
       @endif

       @if(isset($data[0]['role_limits']['import_keywords']) && $data[0]['role_limits']['import_keywords'] == '1')
       <td><i class="fa fa-check"></i></td>
       @else
       <td><i class="fa fa-times"></i></td>
       @endif

       @if(isset($data[1]['role_limits']['import_keywords']) && $data[1]['role_limits']['import_keywords'] == '1')
       <td><i class="fa fa-check"></i></td>
       @else
       <td><i class="fa fa-times"></i></td>
       @endif

       @if(isset($data[2]['role_limits']['import_keywords']) && $data[2]['role_limits']['import_keywords'] == '1')
       <td><i class="fa fa-check"></i></td>
       @else
       <td><i class="fa fa-times"></i></td>
       @endif

      </tr><!-- tr end here -->
      <tr>
       <td></td>
       <td>
	   @if (Auth::check())
			<a href="{{ URL::route('subscription-change') }}" class="get_btn1">Get Started</a>
		 @else
			<a href="{{ URL::route('subscription.free') }}" class="get_btn1">Get Started</a>
		 @endif
	   </td>
       <td>@if (Auth::check())
			<a href="{{ URL::route('subscription-change') }}" class="get_btn1">SIGN UP</a>
		@else
			<a href="{{ URL::route('subscription.free') }}" class="get_btn1">SIGN UP</a>
		@endif</td>
      <td>@if (Auth::check())
			<a href="{{ URL::route('subscription-change') }}" class="get_btn1">SIGN UP</a>
		@else
			<a href="{{ URL::route('subscription.free') }}" class="get_btn1">SIGN UP</a>
		@endif</td>
      <td>@if (Auth::check())
			<a href="{{ URL::route('subscription-change') }}" class="get_btn1">SIGN UP</a>
		@else
			<a href="{{ URL::route('subscription.free') }}" class="get_btn1">SIGN UP</a>
		@endif</td>
      </tr><!-- tr end here -->
     </table><!-- table end here -->
    </div><!-- div end here -->
   </div><!-- col end here -->
  </div><!-- row end here -->
  <?php
    $t = 0;
    if(isset($data_trial) && !empty($data_trial)) {
      $t = 1;
    }

    $dataSizeM = count($data)+$t;
    $dataSizeY = count($records_y)+$t;
    $colmH     = 4;
    $colyH     = 4;
    $colm      = 2;
    $coly      = 2;
    if($dataSizeM == 3) {
      $colm    = 3;
      $colmH   = 3;
    } else if($dataSizeM == 2) {
      $colm    = 4;
      $colmH   = 4;
    } else if($dataSizeM == 1) {
      $colm    = 6;
      $colmH   = 6;
    } else if($dataSizeM == 0) {
      $colm    = 4;
      $colmH   = 4;
    }

    if($dataSizeY == 3) {
      $coly    = 3;
      $colyH   = 3;
    } else if($dataSizeY == 2) {
      $coly    = 4;
      $colyH   = 4;
    } else if($dataSizeY == 1) {
      $coly    = 6;
      $colyH   = 6;
    } else if($dataSizeY == 0) {
      $coly    = 4;
      $colyH   = 4;
    }
  ?>
  <input type="hidden" id="tabM" value="{{$colmH}}" />
  <input type="hidden" id="tabY" value="{{$colyH}}" />
 <!----------for mobile view price table-------------->
  <div class="row" id="plans_for_month">
   <div class="col-md-{{$colmH}} col-sm-{{$colmH}}" id="plan_tab">
    <ul class="price_txt">
     <li>Select PLan</li>
     <li>Monthly <label class="switch"><span class="toggle"></span> </label> 
        <span>Annual (save 40%)</span>
     </li>
     <li>Daily Searches</li>
     <li>Daily Evaluations</li>
     <li>Brainstorming Usage</li>
     <li>Rank Tracker (keywords)</li>
     <li>Save Projects</li>
     <li>Save Keywords</li>
     <li>Exact Match Domain Finder</li>
     <li>Export Data</li>
     <li>Bulk Keyword Upload</li>
    </ul><!-- ul end here -->
   </div><!-- col end here -->

   @if(isset($data_trial) && !empty($data_trial))
   <div class="col-md-{{$colm}} col-sm-{{$colm}} plans_for_month">
    <ul class="price_1">
     <li>{{ (isset($data_trial['role']->screen_name) ? ucfirst($data_trial['role']->screen_name) : '')}}</li>
     <li><span>$</span>0<strong>/m</strong></li>
     <li>{{ (isset($data_trial['role_limits']['research']) ? $data_trial['role_limits']['research'] : 0)}}</li>
     <li>{{ (isset($data_trial['role_limits']['evolution']) ? $data_trial['role_limits']['evolution'] : 0)}}</li>
     <li>{{ (isset($data_trial['role_limits']['brainstorm']) ? $data_trial['role_limits']['brainstorm'] : 0)}}</li>
     <li>{{ (isset($data_trial['role_limits']['tracking']) ? $data_trial['role_limits']['tracking'] : 0)}}</li>
     @if(isset($data_trial['role_limits']['keyword_save']) && $data_trial['role_limits']['keyword_save'] == 1)
        <li>{{ (isset($data_trial['role_limits']['project']) ? $data_trial['role_limits']['project'] : 0) }}</li>
        <li>{{ (isset($data_trial['role_limits']['can_save']) ? $data_trial['role_limits']['can_save'] : 0) }}</li>
     @else
        <li><i class="fa fa-times"></i></li>
        <li><i class="fa fa-times"></i></li>
     @endif
     @if(isset($data_trial['role_limits']['domain_finder']) && $data_trial['role_limits']['domain_finder'] == 1)                                
        <li><i class="fa fa-check"></i></li>
     @else
        <li><i class="fa fa-times"></i></li>
     @endif

     @if(isset($data_trial['role_limits']['export_data']) && $data_trial['role_limits']['export_data'] == 1)                                
        <li><i class="fa fa-check"></i></li>
     @else
        <li><i class="fa fa-times"></i></li>
     @endif     

     @if(isset($data_trial['role_limits']['import_keywords']) && $data_trial['role_limits']['import_keywords'] == 1) 
        <li><i class="fa fa-check"></i></li>
     @else
        <li><i class="fa fa-times"></i></li>
     @endif

     <li><center>
    @if (Auth::check())

      <a href="{{ URL::route('subscription-change') }}" class="get_btn1">Get Started</a>

    @else

      <a href="{{ URL::route('subscription.free') }}" class="get_btn1">Get Started</a>

    @endif
   
   </center></li>
    </ul><!-- ul end here -->
   </div><!-- col end here -->

   <div class="col-md-{{$coly}} col-sm-{{$coly}} plans_for_year" style="display:none;">
    <ul class="price_1">
     <li>{{ (isset($data_trial['role']->screen_name) ? ucfirst($data_trial['role']->screen_name) : '')}}</li>
     <li><span>$</span>0<strong>/m</strong></li>
     <li>{{ (isset($data_trial['role_limits']['research']) ? $data_trial['role_limits']['research'] : 0)}}</li>
     <li>{{ (isset($data_trial['role_limits']['evolution']) ? $data_trial['role_limits']['evolution'] : 0)}}</li>
     <li>{{ (isset($data_trial['role_limits']['brainstorm']) ? $data_trial['role_limits']['brainstorm'] : 0)}}</li>
     <li>{{ (isset($data_trial['role_limits']['tracking']) ? $data_trial['role_limits']['tracking'] : 0)}}</li>
     @if(isset($data_trial['role_limits']['keyword_save']) && $data_trial['role_limits']['keyword_save'] == 1)
        <li>{{ (isset($data_trial['role_limits']['project']) ? $data_trial['role_limits']['project'] : 0) }}</li>
        <li>{{ (isset($data_trial['role_limits']['can_save']) ? $data_trial['role_limits']['can_save'] : 0) }}</li>
     @else
        <li><i class="fa fa-times"></i></li>
        <li><i class="fa fa-times"></i></li>
     @endif
     @if(isset($data_trial['role_limits']['domain_finder']) && $data_trial['role_limits']['domain_finder'] == 1)                                
        <li><i class="fa fa-check"></i></li>
     @else
        <li><i class="fa fa-times"></i></li>
     @endif

     @if(isset($data_trial['role_limits']['export_data']) && $data_trial['role_limits']['export_data'] == 1)                                
        <li><i class="fa fa-check"></i></li>
     @else
        <li><i class="fa fa-times"></i></li>
     @endif     

     @if(isset($data_trial['role_limits']['import_keywords']) && $data_trial['role_limits']['import_keywords'] == 1) 
        <li><i class="fa fa-check"></i></li>
     @else
        <li><i class="fa fa-times"></i></li>
     @endif

     <li><center>
    @if (Auth::check())

      <a href="{{ URL::route('subscription-change') }}" class="get_btn1">Get Started</a>

    @else

      <a href="{{ URL::route('subscription.free') }}" class="get_btn1">Get Started</a>

    @endif
   
   </center></li>
    </ul><!-- ul end here -->
   </div><!-- col end here -->   
  @endif

  <?php
    $i = 2;
    if($t == 0) {
      $i = 1;
    }
  ?>
  @foreach($data as $row)
  <?php
  if($i == 5) {
      break;
  } 
  ?>
  <div class="col-md-{{$colm}} col-sm-{{$colm}} plans_for_month">
    <ul class="price_{{ $i }}">
     <li>{{(isset($row['role']->screen_name) ? $row['role']->screen_name : '')}}</li>
     @if ($row['role']->name == 'free') 
     <li><span>$</span>0<strong>/m</strong></li>
     @else
     <li class="monthly active"><span>$</span>{{ substr(number_format(($row['chargebee']['price'] /100), 2, '.', ' '),0,6) }}<strong>/m</strong> <b> Paid Monthly</b></li>
    <li class="annually"></li>     
     @endif

     <li>{{(isset($row['role_limits']['research']) ? $row['role_limits']['research'] : 0)}}</li>
     <li>{{(isset($row['role_limits']['evolution']) ? $row['role_limits']['evolution'] : 0)}}</li>
     <li>{{(isset($row['role_limits']['brainstorm']) ? $row['role_limits']['brainstorm'] : 0)}}</li>
     <li>{{(isset($row['role_limits']['tracking']) ? $row['role_limits']['tracking'] : 0)}}</li>

     @if(isset($row['role_limits']['project']) && $row['role_limits']['project'] > 0)
     <li>{{$row['role_limits']['project']}}</li>
     @else
     <li><i class="fa fa-times"></i></li>
     @endif

     @if(isset($row['role_limits']['can_save']) && $row['role_limits']['can_save'] > 0)
     <li>{{$row['role_limits']['can_save']}}</li>
     @else
     <li><i class="fa fa-times"></i></li>
     @endif

     @if(isset($row['role_limits']['domain_finder']) && $row['role_limits']['domain_finder'] == '1')
     <li><i class="fa fa-check"></i></li>
     @else
     <li><i class="fa fa-times"></i></li>
     @endif

     @if(isset($row['role_limits']['export_data']) && $row['role_limits']['export_data'] == '1')
     <li><i class="fa fa-check"></i></li>
     @else
     <li><i class="fa fa-times"></i></li>
     @endif

     @if(isset($row['role_limits']['import_keywords']) && $row['role_limits']['import_keywords'] == '1')
     <li><i class="fa fa-check"></i></li>
     @else
     <li><i class="fa fa-times"></i></li>
     @endif
     <li><center>
     @if (isset($row['role']['is_trial']) && $row['role']['is_trial'] == 1)  
      @if (Auth::check())
        <a href="{{ URL::route('subscription-change') }}" class="get_btn1">Get Started</a>
      @else
        <a href="{{ URL::route('subscription.free') }}" class="get_btn1">Get Started</a>
      @endif
    @else
      @if (Auth::check())
        <a href="{{ URL::route('subscription-change') }}" class="get_btn1">SIGN UP</a>
      @else
        <a href="{{ URL::route('subscription.free') }}" class="get_btn1">SIGN UP</a>
      @endif
    @endif

   </center></li>
    </ul><!-- ul end here -->
   </div><!-- col end here -->
    <?php $i++; ?>
    @endforeach

  <?php
    $i = 2;
    if($t == 0) {
      $i = 1;
    }
  ?>  
  @foreach($records_y as $row)
  <?php
  if($i == 5) {
      break;
  }
  $pname = explode("_", $row['role']->screen_name);
  ?>
  <div class="col-md-{{$coly}} col-sm-{{$coly}} plans_for_year" style="display:none;">
    <ul class="price_{{ $i }}">
     <li>{{(isset($pname[0]) ? $pname[0] : '')}}</li>
     @if ($row['role']->name == 'free') 
     <li><span>$</span>0<strong>/m</strong></li>
     @else
     <li class="annually"><span>$</span>{{ substr(number_format(($row['chargebee']['price'] /100), 2, '.', ' '),0,6) }}<strong></strong> <b>Paid Annually</b></li>
     <li class="monthly active"><span>$</span>9.97<strong>/m</strong> <b> Paid Monthly</b></li>
     @endif

     <li>{{(isset($row['role_limits']['research']) ? $row['role_limits']['research'] : 0)}}</li>
     <li>{{(isset($row['role_limits']['evolution']) ? $row['role_limits']['evolution'] : 0)}}</li>
     <li>{{(isset($row['role_limits']['brainstorm']) ? $row['role_limits']['brainstorm'] : 0)}}</li>
     <li>{{(isset($row['role_limits']['tracking']) ? $row['role_limits']['tracking'] : 0)}}</li>

     @if(isset($row['role_limits']['project']) && $row['role_limits']['project'] > 0)
     <li>{{$row['role_limits']['project']}}</li>
     @else
     <li><i class="fa fa-times"></i></li>
     @endif

     @if(isset($row['role_limits']['can_save']) && $row['role_limits']['can_save'] > 0)
     <li>{{$row['role_limits']['can_save']}}</li>
     @else
     <li><i class="fa fa-times"></i></li>
     @endif

     @if(isset($row['role_limits']['domain_finder']) && $row['role_limits']['domain_finder'] == '1')
     <li><i class="fa fa-check"></i></li>
     @else
     <li><i class="fa fa-times"></i></li>
     @endif

     @if(isset($row['role_limits']['export_data']) && $row['role_limits']['export_data'] == '1')
     <li><i class="fa fa-check"></i></li>
     @else
     <li><i class="fa fa-times"></i></li>
     @endif     

     @if(isset($row['role_limits']['import_keywords']) && $row['role_limits']['import_keywords'] == '1')
     <li><i class="fa fa-check"></i></li>
     @else
     <li><i class="fa fa-times"></i></li>
     @endif
     <li><center>
     @if (isset($row['role']['is_trial']) && $row['role']['is_trial'] == 1)  
      @if (Auth::check())
        <a href="{{ URL::route('subscription-change') }}" class="get_btn1">Get Started</a>
      @else
        <a href="{{ URL::route('subscription.free') }}" class="get_btn1">Get Started</a>
      @endif
    @else
      @if (Auth::check())
        <a href="{{ URL::route('subscription-change') }}" class="get_btn1">SIGN UP</a>
      @else
        <a href="{{ URL::route('subscription.free') }}" class="get_btn1">SIGN UP</a>
      @endif
    @endif

   </center></li>
    </ul><!-- ul end here -->
   </div><!-- col end here -->
    <?php $i++; ?>
    @endforeach    
  </div><!-- row end here -->
 </div><!-- container end here -->
</div><!-- pricing_plan end here -->