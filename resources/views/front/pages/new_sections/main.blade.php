<div class="main">
 <!-- carousel Start Here -->
 <div id="myCarousel" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
   <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
  </ol>
  <div class="carousel-inner">
   <div class="item active">
    <img width="100%" src="{{ asset('assets/frontend/new_html/images/banner_new.jpg') }}" class="img-responsive dd">
	<img width="100%" src="{{ asset('assets/frontend/new_html/images/mobile_view_banner.jpg') }}" class="img-responsive dd1">
    <div class="container">
     <div class="carousel-caption ltd-fans">
      <div class="row">
       <div class="home-banner">
        @if(Route::currentRouteName() == 'front.pages.landing')
          <h1 align="center">Get Lifetime Access Now!</h1>
            <h3 align="center">The Ultimate Keyword Intelligence Tool</h3>
            <div class="main-counter-container index-page">
              <div class="sub-counter-container" align="center">
                <div class="countdown-container" id="countdown-container"></div>
                <div class="grab-btn-container">
                  <a class="grab-btn" href="https://www.keywordrevealer.com">get lifetime access now!</a>
                </div>
              </div>
            </div>
          </div>
        @else
          <h1>The Ultimate Keyword Intelligence Tool</h1>
          <h3>Get Your FREE KeywordRevealer Trial Account!</h3>
      		<form action="{{ URL::route('subscription.free') }}" method="get" autocomplete="off">
              <input class="form-control box" type="email" name="email" placeholder="Enter Email Address">
              <button class="start_btn" type="submit" value="">START NOW</button>
      		</form>
        @endif
       </div><!-- col end here -->
      </div><!-- row end here -->
     </div>
    </div>
   </div><!-- item end here -->
      
   </div>
   <a class="left carousel-control" href="#myCarousel" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
   <a class="right carousel-control" href="#myCarousel" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
 </div>
 <!-- carousel End Here -->
</div><!-- main end here -->




<!--<div class="mobile_main">

  	<h1>>The Ultimate Keyword Intelligence Tool</h1>
    @if(Route::currentRouteName() == 'front.pages.landing')
    <div class="text-center">
      
      <h3>Get Lifetime Access Now For A Limited Time!</h3>
      <div class="main-counter-container index-page text-center">
        <div class="sub-counter-container">
          <div class="countdown-container" id="countdown-container"></div>
          <div class="grab-btn-container">
          </div>
        </div>
      </div>
      <a class="grab-btn" href="{{ route('front.pages.landing.pricing') }}">grab this limited time deal!</a>
    </div>
    @else
    
    	<h3>Get Your FREE KeywordRevealer Trial Account!</h3>
    	<form action="{{ URL::route('subscription.free') }}" method="get" autocomplete="off">
    	<input class="form-control box" type="email" name="email" placeholder="Enter Email Address">
    	<button class="start_btn" type="submit" value="">START NOW</button>
    	</form>
    @endif
</div>--><!-- mobile_main end here -->

<div class="discover_low">
 <div class="container">
  <div class="row">
   <div class="col-md-2 col-sm-1"></div>
   
   <div class="col-md-8 col-sm-10">
    <h3>Discover low-competition keywords Guaranteed!</h3>
    <center><img src="{{ asset('assets/frontend/new_html/images/line.jpg') }}" class="img-responsive"></center>
    <p>Start spending less time on keywords that are simply out of reach!</p>
   </div><!-- col end here -->
  </div><!-- row end here -->
  
  <div class="row">
   <div class="col-md-6 col-sm-12">
    <div class="main_1">
     <img src="{{ asset('assets/frontend/new_html/images/icon.png') }}" class="img-responsive">     
     <h3>Keyword Research</h3>
     <p>Find hundreds of long tail keyword ideas in seconds with the amazing keyword research tool.</p>
    </div><!-- main_1 end here -->
   </div><!-- col end here -->
   
   <div class="col-md-6 col-sm-12">
    <div class="main_1">
     <img src="{{ asset('assets/frontend/new_html/images/icon_1.png') }}" class="img-responsive">     
     <h3>Discover Keywords</h3>
     <p>Identify keywords with high search volume and low competition, you can also check domain availability for each term as well.</p>
    </div><!-- main_1 end here -->
   </div><!-- col end here -->
   
   <div class="col-md-6 col-sm-12">
    <div class="main_1">
     <img src="{{ asset('assets/frontend/new_html/images/icon_2.png') }}" class="img-responsive">     
     <h3>Evaluate Keywords</h3>
     <p>Instantly reveal the ranking difficulty and metrics for hundreds of keywords!</p>
    </div><!-- main_1 end here -->
   </div><!-- col end here -->
   
   <div class="col-md-6 col-sm-12">
    <div class="main_1">
     <img src="{{ asset('assets/frontend/new_html/images/icon_3.png') }}" class="img-responsive">     
     <h3>Rank Keywords</h3>
     <p>Apply the best keyword terms to your SEO strategy & start ranking on page one!</p>
    </div><!-- main_1 end here -->
   </div><!-- col end here -->   
  </div><!-- row end here -->
 </div><!-- container end here -->
@if(Route::currentRouteName() == 'front.pages.landing')
<div class="text-center centered-grab-btn">
  <a class="grab-btn" href="https://www.keywordrevealer.com">get lifetime access now!</a>
</div>
@endif
</div><!-- discover_low end here -->


<div class="discover_our">
 <div class="container">
  <div class="row">
   <div class="col-md-2 col-sm-1"></div>
   
   <div class="col-md-8 col-sm-10">
    <h3>Discover Our Great Features</h3>
    <center><img src="{{ asset('assets/frontend/new_html/images/line.jpg') }}" class="img-responsive"></center>
    <p>Uncover a Goldmine of Keyword Opportunities, Pull accurate keyword-data directly from Google combined with a difficulty score to find the easiest to rank keywords.</p>
   </div><!-- col end here -->
  </div><!-- row end here -->
  
  <div class="row"> 
   <div class="col-md-7 col-sm-7">
    <img width="100%" src="{{ asset('assets/frontend/new_html/images/key_research.jpg') }}" class="img-responsive">
   </div><!-- col end here -->
   
   <div class="col-md-4 col-sm-5">
    <div class="icon"><img src="{{ asset('assets/frontend/new_html/images/research_icon.png') }}" class="img-responsive"></div>
    <h3>Keyword Research</h3>
    <img src="{{ asset('assets/frontend/new_html/images/line.jpg') }}" class="img-responsive">
    <p>Keyword Revealer is a huge time saver for anyone looking to uncover low-competition keywords. Start focusing your SEO efforts on easy-to-rank terms with low difficulty scores.</p>

<p>Our Keyword tool will uncover thousands of untapped long-tail keywords for any niche imaginable. Once keywords are discovered you will be able to view the difficulty score for each phrase and analyze the competition currently ranking on the first page of Google.</p>
   </div><!-- col end here -->  
   
   <div class="col-md-12 col-sm-12">
    <img width="100%" src="{{ asset('assets/frontend/new_html/images/key_research.jpg') }}" class="img-responsive">
   </div><!-- col end here -->   
  </div><!-- row end here -->
 </div><!-- container end here -->
</div><!-- discover_our end here -->


<div class="key_brain">
 <div class="container"> 
  <div class="row"> 
   <div class="col-md-1"></div>
   
   <div class="col-md-4 col-sm-5">
    <div class="icon"><img src="{{ asset('assets/frontend/new_html/images/brain_icon.png') }}" class="img-responsive"></div>
    <h3>Keyword Brainstorming</h3>
    <img src="{{ asset('assets/frontend/new_html/images/line.jpg') }}" class="img-responsive">
    <p>One of the most difficult aspects of keyword research is discovering new keyword phrases based on searcher intent. Our Keyword Brainstorming tool will allow you to discover the specific context in which your keywords are being used within the search engines.</p>

<p>Use the Keyword Brainstorming tool to explore new niche ideas and group them in the correct silo structure.</p>
   </div><!-- col end here -->
   
   <div class="col-md-7 col-sm-7">
    <img width="100%" src="{{ asset('assets/frontend/new_html/images/key_brain.jpg') }}" class="img-responsive">
   </div><!-- col end here -->   
  </div><!-- row end here -->
 </div><!-- container end here -->
</div><!-- key_brain end here -->


<div class="rank_tracker">
 <div class="container">  
  <div class="row">   
   <div class="col-md-7 col-sm-7">
    <img width="100%" src="{{ asset('assets/frontend/new_html/images/Key_tracker.jpg') }}" class="img-responsive">
   </div><!-- col end here -->
   
   <div class="col-md-4 col-sm-5">
    <div class="icon"><img src="{{ asset('assets/frontend/new_html/images/tracker_icon.png')}}" class="img-responsive"></div>
    <h3>Rank Tracker</h3>
    <img src="{{ asset('assets/frontend/new_html/images/line.jpg') }}" class="img-responsive">
    <p>Save time and money by automating the process of checking your keyword rankings. Our Rank tracking tool will automatically check your keyword position in Google every day!</p>

<p>View your current rank and measure changes in rankings since yesterday, last week, last month, or since you started tracking the keyword. It's the rank tracker you've been waiting for.</p>
   </div><!-- col end here -->  
   
   <div class="col-md-12 col-sm-12">
    <img width="100%" src="{{ asset('assets/frontend/new_html/images/Key_tracker.jpg') }}" class="img-responsive">
   </div><!-- col end here -->   
  </div><!-- row end here -->
 </div><!-- container end here -->
</div><!-- rank_tracker end here -->


<div class="get_section">
 <div class="container">  
  <div class="row">   
   <div class="col-md-2 col-sm-1"></div>
   
   <div class="col-md-8 col-sm-10">
    <h3>Reveal low competition keywords in no time at all.</h3>
    <center>
    @if(Route::currentRouteName() == 'front.pages.landing')
    <a href="https://www.keywordrevealer.com" class="get_btn">get lifetime access now!</a>
    @else
    <a href="{{ URL::route('subscription.free') }}" class="get_btn">GET STARTED</a>
    @endif

	
	</center>
   </div><!-- col end here -->   
  </div><!-- row end here -->
 </div><!-- container end here -->
</div><!-- get_section end here -->
