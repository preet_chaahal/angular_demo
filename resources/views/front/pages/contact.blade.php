@extends('layouts.front.master')

@section('content')
@include('front.pages.new_sections.navigation')
<div class="sign_up_main">
 <div class="container">
  <div class="row">
   <div class="col-md-3 col-sm-2"></div>
   
   <div class="col-md-6 col-sm-8">
    <img src="{{ asset('assets/frontend/new_html/images/logo_2.png') }}" class="img-responsive">
    <h3>Contact</h3>
	 @include('layouts.partials.messages')
	  <form class="form-custom" action="{{ URL::route('front.contact.post') }}" method="post" autocomplete="off">
	   {!! csrf_field() !!}
     <p>Your Name</p>
	<input type="text" class="form-control box_1" name="name" value="{{ old('name') }}" placeholder="Type your Name">     
    
           
     <p>Your Email Address</p>
	  <input type="text" class="form-control box_1" name="email" value="{{ old('email') }}" placeholder="Type your Email">
          
     <p>Reason for Contact</p>
	   <input type="text" class="form-control box_1" name="reason" value="{{ old('reason') }}">
     
     <p>Your Message</p>
	  <textarea cols="30" rows="5" class="form-control box_1" name="message" placeholder="Your Message">{{ old('message') }}</textarea>
     
     <p>Can you solve this: {{ $captchaA }} + {{ $captchaB }}?</p>
	  <input type="text" class="form-control box_1" name="captcha" id="captcha">    
     <center><button value="" type="submit" class="create_btn">Send Message</button></center>
    </form><!-- form end here -->
   </div><!-- col end here -->
  </div><!-- row end here -->
 </div><!-- container end here -->
</div><!-- sign_up_main end here -->
@stop