@extends('layouts.front.master')

@section('content')

@section('meta-og-section')
       <meta property="og:locale" content="en_US" />
       <meta property="og:type" content="article" />
       <meta property="og:title" content="KeywordRevealer Exclusive Lifetime Deal for Lifetime Tech Deal Fans" />
       <meta property="og:description" content="Exclusive Lifetime Deal for finding hundreds of long tail keyword ideas in seconds." />
       <meta property="og:url" content="https://www.kwrev.com/LTD" />
       <meta property="og:site_name" content="KeywordRevealer" />
       <meta property="og:image" content="https://www.kwrev.com/assets/frontend/new_html/images/kr-facebook-preview.jpg" />
       <meta name="twitter:card" content="summary_large_image" />
       <meta name="twitter:description" content="Exclusive Lifetime Deal for finding hundreds of long tail keyword ideas in seconds." />
       <meta name="twitter:title" content="KeywordRevealer Exclusive Lifetime Deal for Lifetime Tech Deal Fans" />
       <meta name="twitter:image" content="https://www.kwrev.com/assets/frontend/new_html/images/kr-twitter-preview.jpg" />
@endsection	   

@include('front.pages.new_sections.navigation')

@if(Auth::guest())
<form action="{{ URL::route('subscription.free') }}" method="POST" autocomplete="off">
@else
<form action="{{ URL::route('front.pages.landing.pricing.checkout') }}" method="get" id="subscription-form">
@endif
	<input type="hidden" name="_token" value="{{ csrf_token() }}">
	<input type="text" hidden name="landing_page" value="true">

	<div class="landing-pricing">
		<div class="container-fluid" style="padding-top:100px;">
	    	<div class="col-md-7 col-lg-6 col-lg-offset-1" style="background:#f9fcfe">

				<div class="pricing_plan" id="pricing">
					<h3 class="title">1. Select your lifetime plan</h3>
					<center><img src="{{ asset('assets/frontend/new_html/images/line.jpg') }}" class="img-responsive"></center>

					<div class="priceBox">

					<div class="row priceBox-inner">
						<div class="col-xs-3">
						    <ul class="price_txt">
							    <li>Select PLan</li>
							    <li><b>Lifetime Access</b></li>
							    <li>Search Results</li>
							    <li>Daily Searches</li>
							    <li>Daily Evaluations</li>
							    <li>Brainstorming Usage</li>
							    <li>Rank Tracker (keywords)</li>
							    <li>Save Projects</li>
							    <li>Save Keywords</li>
							    <li>Domain Finder</li>
							    <li>Bulk Keyword Upload</li>
						    </ul><!-- ul end here -->
						</div><!-- col end here -->

						<div class="col-xs-3 plan-holder">
							<ul class="price_2">
								<li>CONSULTANT</li>
								<li class="annually"></li>
								<li class="monthly active"><span>$</span>39<strong></strong> <b class="hidden-xs"> One Time Payment</b></li>
								<li>Unlimited</li>
								<li>10</li>
								<li>15</li>
								<li>5</li>
								<li>0</li>
								<li>3</li>
								<li>10</li>
								<li><i class="fa fa-check"></i></li>
								<li><i class="fa fa-times"></i></li>
								<li class="last-li">
									Sign Up
									<div class="checkbox-holder">
										<label class="checkbox-container">
											<input type="radio" name="plan" value="lifetime_consultant">
											<span class="checkmark"></span>
										</label>
									</div>
								</li>
							</ul><!-- ul end here -->
						</div><!-- col end here -->

						<div class="col-xs-3 plan-holder">
						    <ul class="price_2">
							    <li>MARKETER</li>
							    <li class="annually"></li>
							    <li class="monthly active"><span>$</span>69<strong></strong> <b class="hidden-xs"> One Time Payment</b></li>
							    <li>Unlimited</li>
							    <li>25</li>
							    <li>35</li>
							    <li>20</li>
							    <li>20</li>
							    <li>10</li>
							    <li>30</li>
							    <li><i class="fa fa-check"></i></li>
							    <li><i class="fa fa-check"></i></li>
							    <li class="last-li">
							    	Sign Up
							    	<div class="checkbox-holder">
							    		<label class="checkbox-container">
											<input type="radio" name="plan" value="lifetime_marketer">
											<span class="checkmark"></span>
										</label>
							    	</div>
								</li>
						    </ul><!-- ul end here -->
						</div><!-- col end here -->

						<div class="col-xs-3 plan-holder">
						    <ul class="price_3">
							    <li>AGENCY</li>
							    <li class="annually"></li>
							    <li class="monthly active"><span>$</span>199<strong></strong> <b class="hidden-xs">One Time Payment</b></li>
			 				    <li>Unlimited</li>
							    <li>70</li>
							    <li>120</li>
							    <li>50</li>
							    <li>120</li>
							    <li>Unlimited</li>
							    <li>Unlimited</li>
							    <li><i class="fa fa-check"></i></li>
							    <li><i class="fa fa-check"></i></li>
							    <li class="last-li">
							    	Sign Up
							    	<div class="checkbox-holder">
							    		<label class="checkbox-container">
											<input type="radio" name="plan" value="lifetime_agency" checked>
											<span class="checkmark"></span>
										</label>
							    	</div>
								</li>
						    </ul><!-- ul end here -->
						</div><!-- col end here -->


					  </div><!-- row end here -->
					 </div><!-- container end here -->
				</div><!-- pricing_plan end here -->

	    	</div>

	    	<div class="col-md-5 col-lg-4" style="background:#f9fcfe">
	    		<div class="sign_up_main">
					 <h3 class="title">2. Enter your info</h3>
					 <center><img src="{{ asset('assets/frontend/new_html/images/line.jpg') }}" class="img-responsive"></center>


					<div class="row">
						<div class="col-sm-8 col-sm-offset-2 col-md-12  col-md-offset-0">
				    		<div style="padding-top:70px;">
					    		@if ($registration->disabled == 1)
					    		<div class="alert alert-info" role="alert">Registration is currently disabled.Please try again later.
					    		</div>
					    		@else
					    		@include('layouts.partials.messages')
					    			<div class="form" {!! Auth::check()? 'style="padding:45px 0"':''!!}>
								    	@if(Auth::guest())
					    				<img src="{{ asset('assets/frontend/new_html/images/ltd_signup_logo.png') }}" class="img-responsive">    <h3>SIGN UP</h3>
						    			<!-- Post Affiliate Pro START -->
						    			<input type="hidden" name="pap_cookie" id="pap_cookie" value="">

						    			<script type="text/javascript" id="pap_x2s6df8d" src="https://keywordrevealer.postaffiliatepro.com/scripts/vdzfn3n4jm8"></script>		<script type="text/javascript">		PostAffTracker.setAccountId('default1');		try {		PostAffTracker.track();		PostAffTracker.writeCookieToCustomField('pap_cookie');		} catch (err) { }		</script>		<!-- Post Affiliate Pro END -->     <p>Email</p>          <i class="fa fa-envelope-o"></i>	   <input type="text" id="email" name="email" class="form-control box_1" value="{{ app('request')->input('email') }}" placeholder="E-mail">                <p>Password</p>     <i class="fa fa-lock"></i>	  <input type="password" id="password" name="password" class="form-control box_1" placeholder="Password">               <p>Confirm Password</p>     <i class="fa fa-lock"></i>	 <input type="password" id="password_confirmation" name="password_confirmation" class="form-control box_1" placeholder="Repeat Password">      <center>	   <input type="submit" class="create_btn" value="Create an Account">	 	 </center>
								    	@else
						    			<div class="text-center form-big-button">
											<input type="submit" class="create_btn" value="grab this limited time deal!">
										</div>
								    	@endif
					    			</div>
					    		@endif
				    		</div>
						</div>
					</div>
	    		</div>
	    	</div><!-- col end here -->

		</div><!-- container end here -->


		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<div class="main-counter-container">

						<div class="sub-counter-container">
				            <div class="ends"><b>Deal ends in</b></div>
							<div class="countdown-container" align="center" id="countdown-container"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="container" style="padding-top: 30px;">
			<div class="row">
				<div class="col-xs-12">
					<div class="card_section">
						<div class="col-md-12 col-sm-12">
							<div class="card"><img src="{{ asset('assets/frontend/new_html/images/card.png') }}" class="img-responsive">
								<h3>100% Secure with 14-Day Money Back Guarantee</h3>
							</div>
						</div><!-- col end here -->
					</div><!-- row end here -->
				</div>
			</div>
		</div>
	</div>

</form>


@include('front.pages.new_sections.testimonial')@include('front.pages.new_sections.footer-up')

@stop


