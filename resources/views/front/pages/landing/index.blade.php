@extends('layouts.front.master')

@section('content')
	  @include('front.pages.new_sections.navigation')
	  @include('front.pages.new_sections.main')
	  @include('front.pages.new_sections.testimonial')
	  @include('front.pages.new_sections.faq')
	  @include('front.pages.new_sections.footer-up')
@stop

@section('meta-og-section')
       <meta property="og:locale" content="en_US" />
       <meta property="og:type" content="article" />
       <meta property="og:title" content="KeywordRevealer Exclusive Lifetime Deal for Lifetime Tech Deal Fans" />
       <meta property="og:description" content="Exclusive Lifetime Deal for finding hundreds of long tail keyword ideas in seconds." />
       <meta property="og:url" content="https://www.kwrev.com/LTD" />
       <meta property="og:site_name" content="KeywordRevealer" />
       <meta property="og:image" content="https://www.kwrev.com/assets/frontend/new_html/images/kr-facebook-preview.jpg" />
       <meta name="twitter:card" content="summary_large_image" />
       <meta name="twitter:description" content="Exclusive Lifetime Deal for finding hundreds of long tail keyword ideas in seconds." />
       <meta name="twitter:title" content="KeywordRevealer Exclusive Lifetime Deal for Lifetime Tech Deal Fans" />
       <meta name="twitter:image" content="https://www.kwrev.com/assets/frontend/new_html/images/kr-twitter-preview.jpg" />
@endsection	   