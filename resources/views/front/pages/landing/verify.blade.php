@extends('layouts.backend.master')

@section('content')
    <div style="text-align: center;vertical-align: middle">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="error-template">
                        <h1>
                            Oops!
                        </h1>
                        <h2>
                            Access Denied
                        </h2>
                        <div class="error-details">
                            Please check your email to authorize your email address
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
