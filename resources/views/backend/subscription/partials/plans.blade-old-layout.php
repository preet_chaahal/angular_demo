<div>
    <h4 class="m-t-0 header-title"><b>SUBSCRIPTION</b></h4>
    <p class="text-muted m-b-20 font-13">
        Choose the package you'd like to use.
    </p>
</div>

<div class="row pricing-plan">
    <div class="col-md-12">
        <div class="">

        @if((bool)$user->stripe_active)
            <form action="{{ URL::route('subscription-change-post') }}" method="post" id="subscription-form">
        @else
            <form action="{{ URL::route('subscription-join') }}" method="get" id="subscription-form">
        @endif

                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="col-sm-12">
                    <div class="plan-switcher">
                        <a id="monthly" href="javascript:void(0)" class="button button-rounded button-switcher pull-left active">
                            <div class="text-center">Monthly</div>
                        </a>
                        <a id="annual" href="javascript:void(0)" class="button button-rounded button-switcher pull-right" style="font-size: 22px; padding: 10px 20px;">
                            <div class="text-center">Annual</div>
                            <!-- <div class="switcher-discount text-center">-40% OFF</div> -->
                        </a>
                        {{--<a id="ltd" href="javascript:void(0)" class="button button-rounded button-switcher pull-left active">--}}
                            {{--<div class="text-center">LTD</div>--}}
                        {{--</a>--}}
                        <div class="clearfix"></div>
                    </div>
                </div>

                <?php
                    $cntM=$cntY=$cntT=0;
                    if(isset($data) && !empty($data)) {
                        $cntM = count($data);
                        if($cntM >= 3) {
                            $cntM = 3;
                        }
                    }
                    if(isset($records_y) && !empty($records_y)) {
                        $cntY = count($records_y);
                        if($cntY >= 3) {
                            $cntY = 3;
                        }    
                    }
                    if(isset($data_trial) && !empty($data_trial)) {
                        $cntT = count($data_trial);
                        if($cntT >= 1) {
                            $cntT = 1;
                        }    
                    }

                    $price_block_m = $cntM+$cntT+1;
                    $price_block_y = $cntY+$cntT+1;
                    $pbm = "";
                    $pby = "";
                    if($price_block_m > 0) {
                        $pbm = "col-".$price_block_m;
                    }
                    if($price_block_y > 0) {
                        $pby = "col-".$price_block_y;
                    }
                ?>

                <div class="row">
                    <div class="col-sm-12">
                        <div class="plan-table {{$pby}}" id="pricing_box_01">
                            <div class="pt-item pt-item-label pt-item-first">
                                <div class="pti-cell pti-cell-header"></div>
                                <div class="pti-cell pti-cell-price">Price</div>
                                <div class="pti-cell">Daily Searches</div>
                                <div class="pti-cell">Daily Evaluations</div>
                                <div class="pti-cell">Brainstorming Usage</div>
                                <div class="pti-cell">Rank Tracker (keywords)</div>
                                <div class="pti-cell">Save Projects</div>
                                <div class="pti-cell">Save Keywords</div>
                                <div class="pti-cell">Exact Match Domain Finder</div>
                                <div class="pti-cell">Export Data</div>
                                <div class="pti-cell">Bulk Keyword Upload</div>
                                <div class="pti-cell pti-cell-btn"></div>
                            </div>
                            @if(isset($data_trial) && !empty($data_trial))
                            <?php
                                $ptnw = "";
                                if(count($data) == 0 && count($records_y) == 0) {
                                    $ptnw = "pt-item-last";
                                }
                                if(isset($data_trial['role']->name) && $user->isCurrentPlan($data_trial['role']->name)) {
                                    $ptnw = "pt-item-main";
                                }
                            ?>
                            <div class="pt-item {{$ptnw}}">
                                <div class="pti-cell pti-cell-header">{{ (isset($data_trial['role']->screen_name) ? ucfirst($data_trial['role']->screen_name) : '')}}</div>
                                <div class="pti-cell pti-cell-price">
                                    <div class="price-free">
                                        {{ (isset($data_trial['role']->screen_name) ? ucfirst($data_trial['role']->screen_name) : '') }}
                                    </div>
                                </div>
                                <div class="pti-cell">{{ (isset($data_trial['role_limits']['research']) ? $data_trial['role_limits']['research'] : 0)}}</div>
                                <div class="pti-cell">{{ (isset($data_trial['role_limits']['evolution']) ? $data_trial['role_limits']['evolution'] : 0)}}</div>
                                <div class="pti-cell">{{ (isset($data_trial['role_limits']['brainstorm']) ? $data_trial['role_limits']['brainstorm'] : 0)}}</div>
                                <div class="pti-cell">{{ (isset($data_trial['role_limits']['tracking']) ? $data_trial['role_limits']['tracking'] : 0)}}</div>
                                @if(isset($data_trial['role_limits']['keyword_save']) && $data_trial['role_limits']['keyword_save'] == 1)
                                <div class="pti-cell">{{ (isset($data_trial['role_limits']['project']) ? $data_trial['role_limits']['project'] : 0) }}</div>
                                <div class="pti-cell">{{ (isset($data_trial['role_limits']['can_save']) ? $data_trial['role_limits']['can_save'] : 0) }}</div>
                                @else
                                <div class="pti-cell"><i class="fa fa-times-circle" aria-hidden="true"></i></div>
                                <div class="pti-cell"><i class="fa fa-times-circle" aria-hidden="true"></i></div>
                                @endif

                                @if(isset($data_trial['role_limits']['domain_finder']) && $data_trial['role_limits']['domain_finder'] == 1)                                
                                    <div class="pti-cell"><i class="fa fa-check-circle" aria-hidden="true"></i></div>
                                @else
                                    <div class="pti-cell"><i class="fa fa-times-circle" aria-hidden="true"></i></div>
                                @endif

                                @if(isset($data_trial['role_limits']['export_data']) && $data_trial['role_limits']['export_data'] == 1)                                
                                    <div class="pti-cell"><i class="fa fa-check-circle" aria-hidden="true"></i></div>
                                @else
                                    <div class="pti-cell"><i class="fa fa-times-circle" aria-hidden="true"></i></div>
                                @endif

                                @if(isset($data_trial['role_limits']['import_keywords']) && $data_trial['role_limits']['import_keywords'] == 1) 
                                    <div class="pti-cell"><i class="fa fa-check-circle" aria-hidden="true"></i></div>
                                @else
                                    <div class="pti-cell"><i class="fa fa-times-circle" aria-hidden="true"></i></div>
                                @endif
                                <div class="pti-cell pti-cell-btn">
                                    <!-- <a href="#" class="button button-rounded button-default">Get Started</a> -->
                                    Sign Up
                                    <span class="upgraderadio">
                                        <span class="upgraderadio-success blue">
                                            <input type="radio" name="plan" id="{{$data_trial['role']->name}}"
                                                   value="{{$data_trial['role']->name}}"
                                                   @if($user->isCurrentPlan($data_trial['role']->name)) checked="checked" @endif/>
                                            <label for="{{$data_trial['role']->name}}"></label>
                                        </span>
                                    </span>
                                </div>
                            </div>
                            @endif
                            <div class="price-wrapper" id="plans_for_month">
                               <?php $i = 1; 
                               if(isset($data)) {
                               $dataMCnt = count($data);
                               ?>
                               @foreach($data as $row) 
                                <?php
                                    $ptnw = "";
                                    if($i == $dataMCnt || $i == 3) {
                                        $ptnw = "pt-item-last";
                                    }
                                    if(isset($row['role']->name) && $user->isCurrentPlan($row['role']->name)) {
                                        $ptnw = "pt-item-main";
                                    }
                                    if($i == 4) {
                                        break;
                                    }

                                ?>
                                <div class="pt-item {{$ptnw}}">
                                    <div class="pti-cell pti-cell-header">{{(isset($row['role']->screen_name) ? ucfirst($row['role']->screen_name) : '')}}</div>

                                    <div class="pti-cell pti-cell-price monthly active">
                                        <div class="price-line">
                                            <div class="price-value">${{ substr(number_format(($row['chargebee']['price'] /100), 2, '.', ' '),0,5) }}/</div>
                                            <div class="price-period">
                                                <span class="period-full">month</span>
                                                <span class="period-short">mo.</span>
                                            </div>
                                        </div>
                                        <div class="price-type">Paid Monthly</div>
                                    </div>

                                    <div class="pti-cell">{{(isset($row['role_limits']['research']) ? $row['role_limits']['research'] : 0)}}</div>
                                    <div class="pti-cell">{{(isset($row['role_limits']['evolution']) ? $row['role_limits']['evolution'] : 0)}}</div>
                                    <div class="pti-cell">{{(isset($row['role_limits']['brainstorm']) ? $row['role_limits']['brainstorm'] : 0)}}</div>
                                    <div class="pti-cell">{{(isset($row['role_limits']['tracking']) ? $row['role_limits']['tracking'] : 0)}}</div>

                                     @if(isset($row['role_limits']['project']) && $row['role_limits']['project'] > 0)
                                     <div class="pti-cell">{{$row['role_limits']['project']}}</div>
                                     @else
                                     <div class="pti-cell"><i class="fa fa-times-circle" aria-hidden="true"></i></div>
                                     @endif

                                     @if(isset($row['role_limits']['can_save']) && $row['role_limits']['can_save'] > 0)
                                     <div class="pti-cell">{{$row['role_limits']['can_save']}}</div>
                                     @else
                                     <div class="pti-cell"><i class="fa fa-times-circle" aria-hidden="true"></i></div>
                                     @endif

                                    <div class="pti-cell">
                                        @if(isset($row['role_limits']['domain_finder']) && $row['role_limits']['domain_finder'] == '1')
                                            <i class="fa fa-check-circle" aria-hidden="true"></i>
                                        @else
                                            <i class="fa fa-times-circle" aria-hidden="true"></i>
                                        @endif
                                    </div>
                                    <div class="pti-cell">
                                        @if(isset($row['role_limits']['export_data']) && $row['role_limits']['export_data'] == '1')
                                            <i class="fa fa-check-circle" aria-hidden="true"></i>
                                        @else
                                            <i class="fa fa-times-circle" aria-hidden="true"></i>
                                        @endif
                                    </div>                                    
                                    <div class="pti-cell">
                                        @if(isset($row['role_limits']['import_keywords']) && $row['role_limits']['import_keywords'] == '1')
                                            <i class="fa fa-check-circle" aria-hidden="true"></i>
                                        @else
                                            <i class="fa fa-times-circle" aria-hidden="true"></i>
                                        @endif
                                    </div>
                                    <div class="pti-cell pti-cell-btn">
                                        Sign Up
                                        <span class="upgraderadio annual active">
                                            <span class="upgraderadio-success blue">
                                                <input type="radio" name="plan" id="m_{{$row['role']->name}}1"
                                                       value="{{$row['role']->name}}"
                                                       @if($user->isCurrentPlan('basic_yearly') || $user->isCurrentPlan('new_basic_yearly')) checked="checked" @endif/>
                                                <label for="m_{{$row['role']->name}}1"></label>
                                            </span>
                                        </span>
                                        <span class="upgraderadio monthly">
                                            <span class="upgraderadio-success blue">
                                                <input type="radio" name="plan" id="m_{{$row['role']->name}}2"
                                                       value="{{$row['role']->name}}"
                                                       @if($user->isCurrentPlan($row['role']->name)) checked="checked" @endif/>
                                                <label for="m_{{$row['role']->name}}2"></label>
                                            </span>
                                        </span>
                                    </div>
                                </div>
                               <?php $i++; ?>
                               @endforeach()
								<?php } ?>
                            </div>

                            <div class="price-wrapper" id="plans_for_year" style="display:none;">
                               <?php 
                               $i = 1; 
                               $dataYCnt = count($records_y);
                               ?>
                               @foreach($records_y as $row) 
                                <?php
                                    $ptnw = "";
                                    if($i == $dataYCnt || $i == 3) {
                                        $ptnw = "pt-item-last";
                                    }                                    
                                    if(isset($row['role']->name) && $user->isCurrentPlan($row['role']->name)) {
                                        $ptnw = "pt-item-main";
                                    }
                                    if($i == 4) {
                                        break;
                                    }
                                    $pname = explode("_", $row['role']->screen_name);
                                ?>
                                <div class="pt-item {{$ptnw}}">
                                    <div class="pti-cell pti-cell-header">{{(isset($pname[0]) ? ucfirst(str_replace("_"," ",$pname[0])) : '')}}</div>

                                    <div class="pti-cell pti-cell-price annual active">
                                            <div class="price-line">
                                            <div class="price-value">${{ substr(number_format(($row['chargebee']['price'] /100), 2, '.', ' '),0,6) }}/</div>
                                        </div>
                                        <div class="price-type">Paid Annually<br> 
                                            <!--<span class="badge"> Save 25%</span>-->
                                            </div>
                                    </div>

                                    <div class="pti-cell">{{(isset($row['role_limits']['research']) ? $row['role_limits']['research'] : 0)}}</div>
                                    <div class="pti-cell">{{(isset($row['role_limits']['evolution']) ? $row['role_limits']['evolution'] : 0)}}</div>
                                    <div class="pti-cell">{{(isset($row['role_limits']['brainstorm']) ? $row['role_limits']['brainstorm'] : 0)}}</div>
                                    <div class="pti-cell">{{(isset($row['role_limits']['tracking']) ? $row['role_limits']['tracking'] : 0)}}</div>

                                     @if(isset($row['role_limits']['project']) && $row['role_limits']['project'] > 0)
                                     <div class="pti-cell">{{$row['role_limits']['project']}}</div>
                                     @else
                                     <div class="pti-cell"><i class="fa fa-times-circle" aria-hidden="true"></i></div>
                                     @endif

                                     @if(isset($row['role_limits']['can_save']) && $row['role_limits']['can_save'] > 0)
                                     <div class="pti-cell">{{$row['role_limits']['can_save']}}</div>
                                     @else
                                     <div class="pti-cell"><i class="fa fa-times-circle" aria-hidden="true"></i></div>
                                     @endif

                                    <div class="pti-cell">
                                        @if(isset($row['role_limits']['domain_finder']) && $row['role_limits']['domain_finder'] == '1')
                                            <i class="fa fa-check-circle" aria-hidden="true"></i>
                                        @else
                                            <i class="fa fa-times-circle" aria-hidden="true"></i>
                                        @endif
                                    </div>

                                    <div class="pti-cell">
                                        @if(isset($row['role_limits']['export_data']) && $row['role_limits']['export_data'] == '1')
                                            <i class="fa fa-check-circle" aria-hidden="true"></i>
                                        @else
                                            <i class="fa fa-times-circle" aria-hidden="true"></i>
                                        @endif
                                    </div>
                                    
                                    <div class="pti-cell">
                                        @if(isset($row['role_limits']['import_keywords']) && $row['role_limits']['import_keywords'] == '1')
                                            <i class="fa fa-check-circle" aria-hidden="true"></i>
                                        @else
                                            <i class="fa fa-times-circle" aria-hidden="true"></i>
                                        @endif
                                    </div>
                                    <div class="pti-cell pti-cell-btn">
                                        Sign Up
                                        <span class="upgraderadio annual active">
                                            <span class="upgraderadio-success blue">
                                                <input type="radio" name="plan" id="y_{{$row['role']->name}}1"
                                                       value="{{$row['role']->name}}"
                                                       @if($user->isCurrentPlan($row['role']->name)) checked="checked" @endif/>
                                                <label for="y_{{$row['role']->name}}1"></label>
                                            </span>
                                        </span>
                                    </div>
                                </div>
                               <?php $i++; ?>
                               @endforeach()
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <!-- <div id="table-submit-wrapper">
                            <button id="submit"
                                    type="button"
                                    disabled="disabled"
                                    class="btn btn-default waves-effect waves-light pull-right w-150"
                            >Chargebee Payment</button>
                        </div> -->
                        <div id="table-submit-wrapper">
                            <button id="submitST"
                                    type="button"
                                    disabled="disabled"
                                    class="btn btn-default waves-effect waves-light pull-right w-150"
                            >Credit Card</button>
                        </div>
                        &nbsp;
                        <div id="table-submit-wrapper">
                            <button id="submitPP"
                                    type="button"
                                    disabled="disabled"
                                    class="btn btn-default waves-effect waves-light pull-right w-150"
                            >PayPal</button>
                        </div>                                            
                    </div>
                </div>
                <input type="hidden" id="payment_gateway" name="payment_gateway" value="">
                <input type="submit" id="realSubmit" value="Submit" class="hidden">
            </form>
        </div>
    </div>
</div>

@section('scripts')
    <script type="text/javascript">
        $('input:radio[name=plan]').on('click', function(event) {
            $('#submit').prop("disabled", false);
            $('#submitST').prop("disabled", false);
            $('#submitPP').prop("disabled", false);
        });

        var switcherBtns = $('.plan-switcher .button-switcher');
        switcherBtns.on('click', function(e) {
            var id = $(this).attr('id');

            switcherBtns.removeClass('active');
            $(this).addClass('active');

            $('.pricing-plan .price-wrapper .upgraderadio').removeClass('active');
            $('.pricing-plan .price-wrapper .upgraderadio.' + id).addClass('active');

            $('.pricing-plan .price-wrapper .pti-cell-price').removeClass('active');
            $('.pricing-plan .price-wrapper .pti-cell-price.' + id).addClass('active');
        });

        $("#monthly").on('click', function(event) {
            $("#plans_for_year").css("display","none");
            $("#plans_for_month").css("display","block");
            $("#pricing_box_01").removeClass("plan-table {{$pby}}");
            $("#pricing_box_01").addClass("plan-table {{$pbm}}");
        });

        $("#annual").on('click', function(event) {
            $("#plans_for_month").css("display","none");
            $("#plans_for_year").css("display","block");
            $("#pricing_box_01").removeClass("plan-table {{$pbm}}");
            $("#pricing_box_01").addClass("plan-table {{$pby}}");
        });

        $('#submit').on('click', function (event) {
            event.preventDefault();
            $('#submit').prop("disabled", true);
            $("#payment_gateway").val("chargebee");
            $('#realSubmit').click();
        });

        $('#submitST').on('click', function (event) {
            event.preventDefault();
            $('#submitST').prop("disabled", true);
            $("#payment_gateway").val("stripe");
            $('#realSubmit').click();
        });

        $('#submitPP').on('click', function (event) {
            event.preventDefault();
            $('#submitPP').prop("disabled", true);
            $("#payment_gateway").val("paypal");
            $('#realSubmit').click();
        });
    </script>
@stop
