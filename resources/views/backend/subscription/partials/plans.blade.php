<div ng-app="keywordRevealer">
<div ng-controller="keywordResearch">
<div ng-controller="ModalCtrl as $ctrl">

<div id ="bill-sec" class="billing-sec">
    <div class="container">
        <div class="main-content">
            <section class="table-sec">
                    <div class="row">
                        <div class="col-md-6 width-sm">
                            <div class="table-box-1 table-comon">
                            <div class="right-corner">

                            </div>
                            <i class="fa fa-calendar calnder"></i>
                                <h4>Current plan</h4>
                                <h3>{{ (isset($user_plan->screen_name) ? $user_plan->screen_name : '') }}</h3>
                                <!-- <p>$49/month</p> -->
                                <div class="table-responsive-width">
                                    <table class="table table-first">
                                        <tbody>
                                        @if(isset(Auth::user()->account_id) && Auth::user()->account_id != 1)                                            
                                            @if(isset(Auth::user()->payment_mode) && !empty(Auth::user()->payment_mode))
                                            <tr>
                                                <td>Payment Mode</td>
                                                <td>
                                                    <strong>
                                                        @if(Auth::user()->payment_mode == "2")
                                                            Credit Card
                                                        @elseif(Auth::user()->payment_mode == "3")
                                                            PayPal        
                                                        @endif
                                                    </strong>
                                                </td>
                                            </tr>
                                            @endif
                                            <?php
                                            $currentDate = date("Y-m-d");
                                            if(Auth::user()->is_trial == 1) {
                                                $trialDate   = Auth::user()->trial_expire_date;
                                                if(strtotime($currentDate) <= strtotime($trialDate)) {
                                                    $date1 = new DateTime($currentDate);
                                                    $date2 = new DateTime($trialDate);
                                                    $days  = $date2->diff($date1)->format('%a');
                                                    if($days <= 1) {
                                                        $d= $days." day";
                                                    } else {
                                                        $d= $days." days";
                                                    }
                                                    $remaining_days_msg = "You have ".$d." remaining in your trial subscription.";
                                                } else {
                                                    $remaining_days_msg = "Your trial subscription has been expired.";
                                                }
                                            ?>
                                            <tr>
                                                <td></td>
                                                <td style="text-align:center;"><strong>{{ $remaining_days_msg }} </strong></td>
                                            </tr>
                                            <?php
                                            } else {
                                                $d = "";
                                                $trialDate   = Auth::user()->chargebee_subscription_end_date;
                                                if(strtotime($currentDate) <= strtotime($trialDate)) {
                                                    $date1 = new DateTime($currentDate);
                                                    $date2 = new DateTime($trialDate);
                                                    $days  = $date2->diff($date1)->format('%a');
                                                    if($days <= 1) {
                                                        $d= "in ".$days." day";
                                                    } else {
                                                        $d= "in ".$days." days";
                                                    }
                                                }
                                                if($d != "") {
                                            ?>
                                            <tr>
                                                <td>Next payment 
                                                @if(Auth::user()->subscribed() && Auth::user()->payment_mode == getenv('PAYMENT_MODE_STRIPE'))
                                                <a href="{{ URL::route('subscription-card') }}" class="waves-effect" data-toggle="tooltip" data-placement="right" title="Update Card">( <i class="fa fa-credit-card"></i> update card info)</a>
                                                @endif
                                                </td>
                                                <td><strong>{{$d}} </strong></td>
                                            </tr>

                                            @if(Auth::user()->payment_mode == getenv('PAYMENT_MODE_STRIPE'))
                                            <tr>    
                                                <td>The credit card that is on file ends in </td>
                                                <td><strong>{{ Auth::user()->last_four }}</strong></td>
                                            </tr>
                                            @endif

                                            <?php } ?>
                                            <tr>
                                                <td>Coupon Code</td>
                                                <td><strong>{{$cb_coupon}}</strong></td>
                                            </tr>  
                                            <?php
                                            }
                                            ?>
                                            <tr class="border-last">
                                                <td> </td>
                                                <td><strong></strong></td>
                                            </tr>
                                        @endif     
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 width-sm">
                            <div class="table-box-2 table-comon">
                                <div class="table-responsive-width">
                                    <table class="table table-second">
                                        <thead>
                                            <tr>
                                                <th>Invoice Amount</th>
                                                <th>Date Paid</th>
                                                <th>Receipt</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                            $i = 0;
                                            foreach($invoices as $cb_row) {
                                                if($i == 6) {
                                                    break;
                                                }
                                                $i++;
                                        ?>
                                            <tr>
                                                <td>${{ (isset($cb_row->amount) ? $cb_row->amount : 0)}}</td>
                                                <td>{{(isset($cb_row->date_created) ? date("d M Y",strtotime($cb_row->date_created)) : '')}}</td>
                                                <td><a href="javascript:void()" ng-click="downloadInvoice({{$cb_row->cb_invoice_id}})">Download</a></td>
                                            </tr>
                                        <?php }  ?>
                                        </tbody>
                                    </table>
                                    <div class="paginaton">
                                        <div class="left-col">
                                            Total items: {{(isset($invoices) ? count($invoices) : 0)}}
                                        </div>
                                        <div class="right-col text-right">
                                        @if(count($invoices) > 6)    
                                        <a class="viw-more" href="{{ URL::route('userinvoice') }}">View More</a>
                                        @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </section>

            <section class="alert-sec">
                <div class="row">
                    <div class="col-md-12">
                    </div>
                </div>
            </section>

            <section class="tab-tsble-sec">
            @if((bool)$user->stripe_active)
                <form action="{{ URL::route('subscription-change-post') }}" method="post" id="subscription-form">
            @else
                <form action="{{ URL::route('subscription-join') }}" method="get" id="subscription-form">
            @endif
                <input type="hidden" name="_token" value="{{ csrf_token() }}"> 
                <?php
                    $cntM=$cntY=$cntT=0;
                    if(isset($data) && !empty($data)) {
                        $cntM = count($data);
                        if($cntM >= 3) {
                            $cntM = 3;
                        }
                    }
                    if(isset($records_y) && !empty($records_y)) {
                        $cntY = count($records_y);
                        if($cntY >= 3) {
                            $cntY = 3;
                        }    
                    }
                    if(isset($data_trial) && !empty($data_trial)) {
                        $cntT = count($data_trial);
                        if($cntT >= 1) {
                            $cntT = 1;
                        }    
                    }

                    $price_block_m = $cntM+$cntT+1;
                    $price_block_y = $cntY+$cntT+1;
                    $pbm = "";
                    $pby = "";
                    if($price_block_m > 0) {
                        $pbm = "col-".$price_block_m;
                    }
                    if($price_block_y > 0) {
                        $pby = "col-".$price_block_y;
                    }
                ?>

                <div class="row">
                    <div class="col-md-12">
                        <h2>Price Plan</h2>
                        <ul class="nav nav-tabs nav-tab-txt">
                            <li class="active month-list"><a data-toggle="tab" href="#monthly">Monthly</a></li>
                            <li class="year-list"><a data-toggle="tab" href="#annual">Annual</a></li>
                        </ul>
                        <div class="tab-content tab-txt-content">
                            <div id="monthly" class="tab-pane fade in active">
                                <div class="inner-items-pricing">
                                <div class="columns plans-col">
                                    <ul class="price">
                                        <li class="header price-head">
                                            <h4>Plans</h4>
                                            <p>Annual (save 40%)</p>
                                        </li>
                                        <li>Daily Searches</li>
                                        <li>Daily Evaluations</li>
                                        <li>Brainstorming Usage</li>
                                        <li>Rank Tracker (keywords)</li>
                                        <li>Save Projects</li>
                                        <li>Save Keywords</li>
                                        <li>Exact Match Domain Finder</li>
                                        <li>Export Data</li>
                                        <li>Bulk Keyword Upload</li>
                                    </ul>
                                </div>

                                @if(isset($data_trial) && !empty($data_trial))
                                <?php
                                    $ptnw = "";
                                    if(count($data) == 0 && count($records_y) == 0) {
                                        $ptnw = "pt-item-last";
                                    }
                                    if(isset($data_trial['role']->name) && $user->isCurrentPlan($data_trial['role']->name)) {
                                        $ptnw = "pt-item-main";
                                    }
                                ?>
                                <div class="columns free-col inr-col">
                                    <ul class="price">
                                        <li class="header price-head">
                                            <h4>{{ (isset($data_trial['role']->screen_name) ? ucfirst($data_trial['role']->screen_name) : '')}}</h4>
                                            <h5><sup>$</sup>0/<span class="pric">m</span></h5>
                                        </li>

                                        <li>{{ (isset($data_trial['role_limits']['research']) ? $data_trial['role_limits']['research'] : 0)}}</li>

                                        <li>{{ (isset($data_trial['role_limits']['evolution']) ? $data_trial['role_limits']['evolution'] : 0)}}</li>

                                        <li>{{ (isset($data_trial['role_limits']['brainstorm']) ? $data_trial['role_limits']['brainstorm'] : 0)}}</li>

                                        <li>{{ (isset($data_trial['role_limits']['tracking']) ? $data_trial['role_limits']['tracking'] : 0)}}</li>

                                        @if(isset($data_trial['role_limits']['project']) && $data_trial['role_limits']['project'] > 0)
                                        <li>{{ $data_trial['role_limits']['project'] }}</li>
                                        @else
                                        <li><img src="{{ asset('assets/backend/images/img-close.png') }}"></li>
                                        @endif

                                        @if(isset($data_trial['role_limits']['can_save']) && $data_trial['role_limits']['can_save'] > 0)
                                        <li>{{ $data_trial['role_limits']['can_save'] }}</li>
                                        @else
                                        <li><img src="{{ asset('assets/backend/images/img-close.png') }}"></li>
                                        @endif

                                        @if(isset($data_trial['role_limits']['domain_finder']) && $data_trial['role_limits']['domain_finder'] == 1)   
                                        <li><img src="{{ asset('assets/backend/images/img-check.png') }}"></li>
                                        @else
                                        <li><img src="{{ asset('assets/backend/images/img-close.png') }}"></li>
                                        @endif

                                        @if(isset($data_trial['role_limits']['export_data']) && $data_trial['role_limits']['export_data'] == 1)   
                                        <li><img src="{{ asset('assets/backend/images/img-check.png') }}"></li>
                                        @else
                                        <li><img src="{{ asset('assets/backend/images/img-close.png') }}"></li>
                                        @endif

                                        @if(isset($data_trial['role_limits']['import_keywords']) && $data_trial['role_limits']['import_keywords'] == 1)  
                                        <li><img src="{{ asset('assets/backend/images/img-check.png') }}"></li>
                                        @else
                                        <li><img src="{{ asset('assets/backend/images/img-close.png') }}"></li>
                                        @endif

                                        <li class="grey">
                                            <label class="rdo-check">
                                              <input type="radio" name="plan" id="{{$data_trial['role']->name}}"
                                                   value="{{$data_trial['role']->name}}"
                                                   @if($user->isCurrentPlan($data_trial['role']->name)) checked="checked" @endif/>
                                              <span class="checkmark"></span>
                                            </label>
                                            <a href="javascript:void();" class="button blue-btn">Sign Up</a>
                                        </li>
                                    </ul>
                                </div>
                                @endif

                                <?php $i = 1; 
                                if(isset($data)) {
                                $dataMCnt = count($data);
                                ?>
                                @foreach($data as $row) 
                                <?php
                                    $ptnw = "";
                                    if($i == $dataMCnt || $i == 3) {
                                        $ptnw = "";
                                    }

                                    if(isset($row['role']->name) && $user->isCurrentPlan($row['role']->name)) {
                                        $ptnw = "cb-active";
                                    }

                                    if($i == 4) {
                                        break;
                                    }
                                ?>
                                <div class="columns basics-col inr-col {{$ptnw}}">
                                    <ul class="price">
                                        <li class="header price-head">
                                            <h4>{{(isset($row['role']->screen_name) ? ucfirst($row['role']->screen_name) : '')}}</h4>
                                            <h5><sup>$</sup>{{ substr(number_format(($row['chargebee']['price'] /100), 2, '.', ' '),0,5) }}/<span class="pric">m</span></h5>
                                            <p>Paid Monthly</p>
                                        </li>

                                        <li>{{(isset($row['role_limits']['research']) ? $row['role_limits']['research'] : 0)}}</li>
                                        
                                        <li>{{(isset($row['role_limits']['evolution']) ? $row['role_limits']['evolution'] : 0)}}</li>
                                        
                                        <li>{{(isset($row['role_limits']['brainstorm']) ? $row['role_limits']['brainstorm'] : 0)}}</li>
                                        
                                        <li>{{(isset($row['role_limits']['tracking']) ? $row['role_limits']['tracking'] : 0)}}</li>

                                        @if(isset($row['role_limits']['project']) && $row['role_limits']['project'] > 0)
                                        <li>{{$row['role_limits']['project']}}</li>
                                        @else
                                        <li><img src="{{ asset('assets/backend/images/img-close.png') }}"></li>
                                        @endif

                                        @if(isset($row['role_limits']['can_save']) && $row['role_limits']['can_save'] > 0)
                                        <li>{{$row['role_limits']['can_save']}}</li>
                                        @else
                                        <li><img src="{{ asset('assets/backend/images/img-close.png') }}"></li>
                                        @endif

                                        @if(isset($row['role_limits']['domain_finder']) && $row['role_limits']['domain_finder'] == '1')
                                        <li><img src="{{ asset('assets/backend/images/img-check.png') }}"></li>
                                        @else
                                        <li><img src="{{ asset('assets/backend/images/img-close.png') }}"></li>
                                        @endif

                                        @if(isset($row['role_limits']['export_data']) && $row['role_limits']['export_data'] == '1')
                                        <li><img src="{{ asset('assets/backend/images/img-check.png') }}"></li>
                                        @else
                                        <li><img src="{{ asset('assets/backend/images/img-close.png') }}"></li>
                                        @endif

                                        @if(isset($row['role_limits']['import_keywords']) && $row['role_limits']['import_keywords'] == '1')
                                        <li><img src="{{ asset('assets/backend/images/img-check.png') }}"></li>
                                        @else
                                        <li><img src="{{ asset('assets/backend/images/img-close.png') }}"></li>
                                        @endif

                                         <li class="grey">
                                            <label class="rdo-check">
                                              <input type="radio" name="plan" id="m_{{$row['role']->name}}2"
                                                       value="{{$row['role']->name}}"
                                                       @if($user->isCurrentPlan($row['role']->name)) checked="checked" @endif/>
                                              <span class="checkmark"></span>
                                            </label>
                                            <a href="javascript:void();" class="button blue-btn">Sign Up</a>
                                        </li>
                                    </ul>
                                </div>
                                <?php $i++; ?>
                                @endforeach()
                                <?php } ?>
                                <!--<div class="columns Pro-col inr-col">
                                    <ul class="price">
                                        <li class="header price-head">
                                            <h4>Pro</h4>
                                            <h5><sup>$</sup>8.55/<span class="pric">m</span></h5>
                                            <p>Paid Monthly</p>
                                        </li>
                                        <li>34</li>
                                        <li>50</li>
                                        <li>25</li>
                                        <li>25</li>
                                        <li>3</li>
                                        <li>25</li>
                                        <li><img src="assets/images/img-check.png"></li>
                                        <li><img src="assets/images/img-check.png"></li>
                                        <li class="grey">
                                            <label class="rdo-check">
                                              <input type="radio" name="radio">
                                              <span class="checkmark"></span>
                                            </label>
                                            <a href="#" class="button blue-btn">Sign Up</a>
                                        </li>
                                    </ul>
                                </div>
                                
                                <div class="columns Elite-col inr-col">
                                    <ul class="price">
                                        <li class="header price-head">
                                            <h4>Elite</h4>
                                            <h5><sup>$</sup>9.98/<span class="pric">m</span></h5>
                                            <p>Paid Monthly</p>
                                        </li>
                                        <li>34</li>
                                        <li>50</li>
                                        <li>25</li>
                                        <li>25</li>
                                        <li>3</li>
                                        <li>25</li>
                                        <li><img src="assets/images/img-check.png"></li>
                                        <li><img src="assets/images/img-check.png"></li>
                                        <li class="grey">
                                            <label class="rdo-check">
                                              <input type="radio" name="radio">
                                              <span class="checkmark"></span>
                                            </label>
                                            <a href="#" class="button blue-btn">Sign Up</a>
                                        </li>
                                    </ul>
                                </div>-->
                                 </div>
                            </div>
                            <div id="annual" class="tab-pane fade">
                                <div class="inner-items-pricing">
                                <div class="columns plans-col">
                                    <ul class="price">
                                        <li class="header price-head">
                                            <h4>Plans</h4>
                                            <p>Annual (save 40%)</p>
                                        </li>
                                        <li>Daily Searches</li>
                                        <li>Daily Evaluations</li>
                                        <li>Brainstorming Usage</li>
                                        <li>Rank Tracker (keywords)</li>
                                        <li>Save Projects</li>
                                        <li>Save Keywords</li>
                                        <li>Exact Match Domain Finder</li>
                                        <li>Export Data</li>
                                        <li>Bulk Keyword Upload</li>
                                    </ul>
                                </div>
                                @if(isset($data_trial) && !empty($data_trial))
                                <?php
                                    $ptnw = "";
                                    if(count($data) == 0 && count($records_y) == 0) {
                                        $ptnw = "pt-item-last";
                                    }
                                    if(isset($data_trial['role']->name) && $user->isCurrentPlan($data_trial['role']->name)) {
                                        $ptnw = "pt-item-main";
                                    }
                                ?>
                                <div class="columns free-col inr-col">
                                    <ul class="price">
                                        <li class="header price-head">
                                            <h4>{{ (isset($data_trial['role']->screen_name) ? ucfirst($data_trial['role']->screen_name) : '')}}</h4>
                                            <h5><sup>$</sup>0/<span class="pric">m</span></h5>
                                        </li>
                                        
                                        <li>{{ (isset($data_trial['role_limits']['research']) ? $data_trial['role_limits']['research'] : 0)}}</li>
                                        
                                        <li>{{ (isset($data_trial['role_limits']['evolution']) ? $data_trial['role_limits']['evolution'] : 0)}}</li>
                                        
                                        <li>{{ (isset($data_trial['role_limits']['brainstorm']) ? $data_trial['role_limits']['brainstorm'] : 0)}}</li>
                                        
                                        <li>{{ (isset($data_trial['role_limits']['tracking']) ? $data_trial['role_limits']['tracking'] : 0)}}</li>

                                        @if(isset($data_trial['role_limits']['project']) && $data_trial['role_limits']['project'] > 0)
                                        <li>{{ $data_trial['role_limits']['project'] }}</li>
                                        @else
                                        <li><img src="{{ asset('assets/backend/images/img-close.png') }}"></li>
                                        @endif

                                        @if(isset($data_trial['role_limits']['can_save']) && $data_trial['role_limits']['can_save'] > 0)
                                        <li>{{ $data_trial['role_limits']['can_save'] }}</li>
                                        @else
                                        <li><img src="{{ asset('assets/backend/images/img-close.png') }}"></li>
                                        @endif

                                        @if(isset($data_trial['role_limits']['domain_finder']) && $data_trial['role_limits']['domain_finder'] == 1)   
                                        <li><img src="{{ asset('assets/backend/images/img-check.png') }}"></li>
                                        @else
                                        <li><img src="{{ asset('assets/backend/images/img-close.png') }}"></li>
                                        @endif

                                        @if(isset($data_trial['role_limits']['export_data']) && $data_trial['role_limits']['export_data'] == 1)   
                                        <li><img src="{{ asset('assets/backend/images/img-check.png') }}"></li>
                                        @else
                                        <li><img src="{{ asset('assets/backend/images/img-close.png') }}"></li>
                                        @endif

                                        @if(isset($data_trial['role_limits']['import_keywords']) && $data_trial['role_limits']['import_keywords'] == 1)  
                                        <li><img src="{{ asset('assets/backend/images/img-check.png') }}"></li>
                                        @else
                                        <li><img src="{{ asset('assets/backend/images/img-close.png') }}"></li>
                                        @endif

                                        <li class="grey">
                                            <label class="rdo-check">
                                              <input type="radio" name="plan" id="{{$data_trial['role']->name}}"
                                                   value="{{$data_trial['role']->name}}"
                                                   @if($user->isCurrentPlan($data_trial['role']->name)) checked="checked" @endif/>
                                              <span class="checkmark"></span>
                                            </label>
                                            <a href="javascript:void();" class="button blue-btn">Sign Up</a>
                                        </li>
                                    </ul>
                                </div>
                                @endif  
                                <?php 
                                $i = 1; 
                                $dataYCnt = count($records_y);
                                ?>
                                @foreach($records_y as $row) 
                                <?php
                                    $ptnw = "";
                                    if($i == $dataYCnt || $i == 3) {
                                        $ptnw = "";
                                    }                                    
                                    if(isset($row['role']->name) && $user->isCurrentPlan($row['role']->name)) {
                                        $ptnw = "cb-active";
                                    }
                                    if($i == 4) {
                                        break;
                                    }
                                    $pname = (isset($row['role']->screen_name) ? $row['role']->screen_name : '');
                                ?>
                                <div class="columns basics-col inr-col {{$ptnw}}">
                                    <ul class="price">
                                        <li class="header price-head">
                                            <h4>{{(isset($pname) ? ucfirst($pname) : '')}}</h4>
                                            <h5><sup>$</sup>{{ round(substr(number_format((($row['chargebee']['price'] /100)/12), 2, '.', ' ') ,0,6)) }}/<span class="pric">m</span></h5>
                                            <p><sup>$</sup>{{ substr(number_format(($row['chargebee']['price'] /100), 2, '.', ' '),0,6) }} Paid Annually</p>
                                        </li>
                                        
                                        <li>{{(isset($row['role_limits']['research']) ? $row['role_limits']['research'] : 0)}}</li>
                                        
                                        <li>{{(isset($row['role_limits']['evolution']) ? $row['role_limits']['evolution'] : 0)}}</li>
                                        
                                        <li>{{(isset($row['role_limits']['brainstorm']) ? $row['role_limits']['brainstorm'] : 0)}}</li>
                                        
                                        <li>{{(isset($row['role_limits']['tracking']) ? $row['role_limits']['tracking'] : 0)}}</li>
                                        
                                        @if(isset($row['role_limits']['project']) && $row['role_limits']['project'] > 0)
                                        <li>{{$row['role_limits']['project']}}</li>
                                        @else
                                        <li><img src="{{ asset('assets/backend/images/img-check.png') }}"></li>
                                        @endif

                                        @if(isset($row['role_limits']['can_save']) && $row['role_limits']['can_save'] > 0)
                                        <li>{{$row['role_limits']['can_save']}}</li>
                                        @else
                                        <li><img src="{{ asset('assets/backend/images/img-close.png') }}"></li>
                                        @endif

                                        @if(isset($row['role_limits']['domain_finder']) && $row['role_limits']['domain_finder'] == '1')
                                        <li><img src="{{ asset('assets/backend/images/img-check.png') }}"></li>
                                        @else
                                        <li><img src="{{ asset('assets/backend/images/img-close.png') }}"></li>
                                        @endif

                                        @if(isset($row['role_limits']['export_data']) && $row['role_limits']['export_data'] == '1')
                                        <li><img src="{{ asset('assets/backend/images/img-check.png') }}"></li>
                                        @else
                                        <li><img src="{{ asset('assets/backend/images/img-close.png') }}"></li>
                                        @endif

                                        @if(isset($row['role_limits']['import_keywords']) && $row['role_limits']['import_keywords'] == '1')
                                        <li><img src="{{ asset('assets/backend/images/img-check.png') }}"></li>
                                        @else
                                        <li><img src="{{ asset('assets/backend/images/img-close.png') }}"></li>
                                        @endif

                                        <li class="grey">
                                            <label class="rdo-check">
                                               <input type="radio" name="plan" id="y_{{$row['role']->name}}1"
                                                       value="{{$row['role']->name}}"
                                                       @if($user->isCurrentPlan($row['role']->name)) checked="checked" @endif/>
                                              <span class="checkmark"></span>
                                            </label>
                                            <a href="javascript:void();" class="button blue-btn">Sign Up</a>
                                        </li>
                                    </ul>
                                </div>
                               <?php $i++; ?>
                               @endforeach()
                                <!--<div class="columns Pro-col inr-col">
                                    <ul class="price">
                                        <li class="header price-head">
                                            <h4>Pro</h4>
                                            <h5><sup>$</sup>8.55/<span class="pric">m</span></h5>
                                            <p>Paid Monthly</p>
                                        </li>
                                        <li>34</li>
                                        <li>50</li>
                                        <li>25</li>
                                        <li>25</li>
                                        <li>3</li>
                                        <li>25</li>
                                        <li><img src="assets/images/img-check.png"></li>
                                        <li><img src="assets/images/img-check.png"></li>
                                        <li class="grey">
                                            <label class="rdo-check">
                                              <input type="radio" name="radio">
                                              <span class="checkmark"></span>
                                            </label>
                                            <a href="#" class="button blue-btn">Sign Up</a>
                                        </li>
                                    </ul>
                                </div>
                                
                                <div class="columns Elite-col inr-col">
                                    <ul class="price">
                                        <li class="header price-head">
                                            <h4>Elite</h4>
                                            <h5><sup>$</sup>9.98/<span class="pric">m</span></h5>
                                            <p>Paid Monthly</p>
                                        </li>
                                        <li>34</li>
                                        <li>50</li>
                                        <li>25</li>
                                        <li>25</li>
                                        <li>3</li>
                                        <li>25</li>
                                        <li><img src="assets/images/img-check.png"></li>
                                        <li><img src="assets/images/img-check.png"></li>
                                        <li class="grey">
                                            <label class="rdo-check">
                                              <input type="radio" name="radio">
                                              <span class="checkmark"></span>
                                            </label>
                                            <a href="#" class="button blue-btn">Sign Up</a>
                                        </li>
                                    </ul>
                                </div>-->
                            </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 text-right btn-cols">
                        <!--<button type="button" class="btn btn-card-payment btn-comn blue-btn">Credit Card</button>
                        <button type="button" class="btn btn-paypal btn-comn blue-btn">Paypal</button>-->
                        <button id="submitST"
                                type="button"
                                disabled="disabled"
                                class="btn btn-card-payment btn-comn blue-btn"
                        >Credit Card</button>
                        <button id="CouponPP"
                                type="button"
                                disabled="disabled"
                                class="btn btn-paypal btn-comn blue-btn"
                                data-toggle="modal" data-target=".confirm-modal"
                        >PayPal</button>
                    </div>
                </div>
                <input type="hidden" id="selected_plan" name="selected_plan" value="">
                <input type="hidden" id="payment_gateway" name="payment_gateway" value="">
                <input type="submit" id="realSubmit" value="Submit" class="hidden">
                </form>                
            </section>
        </div>
    </div>
</div>

<!-- Confirm Modal -->
<div class="modal fade confirm-modal" id="model-set-n" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="popup-heading">Do you have a coupon code?</h4>
                <div class="row">
                    <div class="col-sm-12">
                            <!-- <label for="coupon_code">Coupon Code</label> -->
                            <div class="row" id="popup-setting-n">
                                <div class="col-sm-12">
                                    <input type="text" name="coupon_code" class="form-control" maxlength="40" id="coupon_code" ng-model="promo_code" ng-keyup="updateCoupon()" placeholder="Enter Coupon Code here"/> 
                                </div>
                                <div class="col-sm-12">
                                    <input type="button" name="apply_promo_code" id="apply_promo_code" class="btn waves-effect waves-light" value="Apply Promo Code" ng-click="validatePromoCodePP()" />
                                </div>
                                <div class="col-sm-12">
                                    <small id="coupon_code_success" for="coupon_code" class="" style="color:green;text-align: center; display: block;margin-top: 5px;"></small>
                                    <small id="coupon_code_error" for="coupon_code" class="text-danger" style="text-align: center; display: block;margin-top: 5px;"></small>
                                </div>
                                <div class="col-sm-12">
                                    <button id="submitPP"
                                    type="button"
                                    disabled="disabled"
                                    class="btn waves-effect waves-light"
                                    >Proceed with PayPal</button>
                                </div>
                            </div>
                            
                        </div>
                        
                    </div>
                </div>                                            
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

</div>
</div>
</div>
             
@section('scripts')
    <script type="text/javascript">
        $('input:radio[name=plan]').on('click', function(event) {
            $('#submit').prop("disabled", false);
            $('#submitST').prop("disabled", false);
            $('#submitPP').prop("disabled", false);
            $('#CouponPP').prop("disabled", false);
        });

        var switcherBtns = $('.plan-switcher .button-switcher');
        switcherBtns.on('click', function(e) {
            var id = $(this).attr('id');

            switcherBtns.removeClass('active');
            $(this).addClass('active');

            $('.pricing-plan .price-wrapper .upgraderadio').removeClass('active');
            $('.pricing-plan .price-wrapper .upgraderadio.' + id).addClass('active');

            $('.pricing-plan .price-wrapper .pti-cell-price').removeClass('active');
            $('.pricing-plan .price-wrapper .pti-cell-price.' + id).addClass('active');
        });

        $("#monthly").on('click', function(event) {
            $("#plans_for_year").css("display","none");
            $("#plans_for_month").css("display","block");
            $("#pricing_box_01").removeClass("plan-table {{$pby}}");
            $("#pricing_box_01").addClass("plan-table {{$pbm}}");
        });

        $("#annual").on('click', function(event) {
            $("#plans_for_month").css("display","none");
            $("#plans_for_year").css("display","block");
            $("#pricing_box_01").removeClass("plan-table {{$pbm}}");
            $("#pricing_box_01").addClass("plan-table {{$pby}}");
        });

        $('#submit').on('click', function (event) {
            event.preventDefault();
            $('#submit').prop("disabled", true);
            $("#payment_gateway").val("chargebee");
            $('#realSubmit').click();
        });

        $('#submitST').on('click', function (event) {
            event.preventDefault();
            $('#submitST').prop("disabled", true);
            $("#payment_gateway").val("stripe");
            $('#realSubmit').click();
        });

        $('#submitPP').on('click', function (event) {
            event.preventDefault();
            $('#submitPP').prop("disabled", true);
            $('#apply_promo_code').prop("disabled", true);
            $("#payment_gateway").val("paypal");
            $('#realSubmit').click();
        });
    </script>
@stop
