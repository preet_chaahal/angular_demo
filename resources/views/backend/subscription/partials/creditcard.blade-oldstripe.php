<div class="stripe-errors alert alert-info" style="display:none;"></div>
<!-- Post_Affiliate_Pro START 
<input type="hidden" name="pap_cookie" id="pap_cookie" value="">
<script type="text/javascript" id="pap_x2s6df8d" src="https://keywordrevealer.postaffiliatepro.com/scripts/vdzfn3n4jm8"></script>
<script type="text/javascript">
PostAffTracker.setAccountId('default1');
try {
PostAffTracker.track();
PostAffTracker.writeCookieToCustomField('pap_cookie');
} catch (err) { }
</script>
-->
<div class="form-group">
    <label for="card-number">Card Number</label><span style="color: red;padding-left:2px">*</span>
    <input type="text" data-stripe="number" id="card_number" class="form-control" maxlength="16" required="required">
</div>
<div class="row">
    <div class="col-md-4">
        <label>Expiry Month</label><span style="color: red;padding-left:2px">*</span>
        <select data-stripe="exp-month" id="month" class="form-control" required>
            <option value="01">01</option>
            <option value="02">02</option>
            <option value="03">03</option>
            <option value="04">04</option>
            <option value="05">05</option>
            <option value="06">06</option>
            <option value="07">07</option>
            <option value="08">08</option>
            <option value="09">09</option>
            <option value="10">10</option>
            <option value="11">11</option>
            <option value="12">12</option>
        </select>
    </div>
    <div class="col-md-4">
        <label>Expiry Year</label><span style="color: red;padding-left:2px">*</span>
        <select data-stripe="exp-year" id="year" class="form-control" required>
            @for ($i = $year; $i < $year + 20; $i++)
                <option value="{{$i}}">{{$i}}</option>
            @endfor
        </select>
    </div>
    <div class="col-md-4">
        <label>CVC</label><span style="color: red;padding-left:2px">*</span>
        <input type="text" data-stripe="cvc" id="cvc_number"  class="form-control" maxlength="4" required>
    </div>
</div>
<div class="clearfix">&nbsp;</div>
<div class="row">
    <div class="col-md-8">
        <label>Address</label><span style="color: red;padding-left:2px">*</span>
        <textarea data-stripe="address_line1" id="address" class="form-control" required></textarea>
    </div>
    <div class="col-md-4">
        <label>Zip</label><span style="color: red;padding-left:2px">*</span>
        <input type="text" data-stripe="address-zip" id="zip" class="form-control" required>
    </div>
</div>
<div class="clearfix">&nbsp;</div>
<div class="row">
    <div class="col-md-4">
        <label>City</label><span style="color: red;padding-left:2px">*</span>
        <input type="text" data-stripe="address-city" id="city" class="form-control" required>
    </div>
    <div class="col-md-4">
        <label>State</label><span style="color: red;padding-left:2px">*</span>
        <select id="state" data-stripe="address-state" class="form-control" required>
            <option value="">Please Select</option>
            <optgroup label="Australian Provinces">
                <option value="-AU-NSW">New South Wales</option>
                <option value="-AU-QLD">Queensland</option>
                <option value="-AU-SA">South Australia</option>
                <option value="-AU-TAS">Tasmania</option>
                <option value="-AU-VIC">Victoria</option>
                <option value="-AU-WA">Western Australia</option>
                <option value="-AU-ACT">Australian Capital Territory</option>
                <option value="-AU-NT">Northern Territory</option>
            </optgroup>
            <optgroup label="Canadian Provinces">
                <option value="AB">Alberta</option>
                <option value="BC">British Columbia</option>
                <option value="MB">Manitoba</option>
                <option value="NB">New Brunswick</option>
                <option value="NF">Newfoundland</option>
                <option value="NT">Northwest Territories</option>
                <option value="NS">Nova Scotia</option>
                <option value="NVT">Nunavut</option>
                <option value="ON">Ontario</option>
                <option value="PE">Prince Edward Island</option>
                <option value="QC">Quebec</option>
                <option value="SK">Saskatchewan</option>
                <option value="YK">Yukon</option>
            </optgroup>
            <optgroup label="US States">
                <option value="AL">Alabama</option>
                <option value="AK">Alaska</option>
                <option value="AZ">Arizona</option>
                <option value="AR">Arkansas</option>
                <option value="BVI">British Virgin Islands</option>
                <option value="CA">California</option>
                <option value="CO">Colorado</option>
                <option value="CT">Connecticut</option>
                <option value="DE">Delaware</option>
                <option value="FL">Florida</option>
                <option value="GA">Georgia</option>
                <option value="GU">Guam</option>
                <option value="HI">Hawaii</option>
                <option value="ID">Idaho</option>
                <option value="IL">Illinois</option>
                <option value="IN">Indiana</option>
                <option value="IA">Iowa</option>
                <option value="KS">Kansas</option>
                <option value="KY">Kentucky</option>
                <option value="LA">Louisiana</option>
                <option value="ME">Maine</option>
                <option value="MP">Mariana Islands</option>
                <option value="MPI">Mariana Islands (Pacific)</option>
                <option value="MD">Maryland</option>
                <option value="MA">Massachusetts</option>
                <option value="MI">Michigan</option>
                <option value="MN">Minnesota</option>
                <option value="MS">Mississippi</option>
                <option value="MO">Missouri</option>
                <option value="MT">Montana</option>
                <option value="NE">Nebraska</option>
                <option value="NV">Nevada</option>
                <option value="NH">New Hampshire</option>
                <option value="NJ">New Jersey</option>
                <option value="NM">New Mexico</option>
                <option value="NY">New York</option>
                <option value="NC">North Carolina</option>
                <option value="ND">North Dakota</option>
                <option value="OH">Ohio</option>
                <option value="OK">Oklahoma</option>
                <option value="OR">Oregon</option>
                <option value="PA">Pennsylvania</option>
                <option value="PR">Puerto Rico</option>
                <option value="RI">Rhode Island</option>
                <option value="SC">South Carolina</option>
                <option value="SD">South Dakota</option>
                <option value="TN">Tennessee</option>
                <option value="TX">Texas</option>
                <option value="UT">Utah</option>
                <option value="VT">Vermont</option>
                <option value="USVI">VI  U.S. Virgin Islands</option>
                <option value="VA">Virginia</option>
                <option value="WA">Washington</option>
                <option value="DC">Washington, D.C.</option>
                <option value="WV">West Virginia</option>
                <option value="WI">Wisconsin</option>
                <option value="WY">Wyoming</option>
            </optgroup>
            <!-- FOR STRIPE UK -->
            <optgroup label="England">
                <option>Bedfordshire</option>
                <option>Berkshire</option>
                <option>Bristol</option>
                <option>Buckinghamshire</option>
                <option>Cambridgeshire</option>
                <option>Cheshire</option>
                <option>City of London</option>
                <option>Cornwall</option>
                <option>Cumbria</option>
                <option>Derbyshire</option>
                <option>Devon</option>
                <option>Dorset</option>
                <option>Durham</option>
                <option>East Riding of Yorkshire</option>
                <option>East Sussex</option>
                <option>Essex</option>
                <option>Gloucestershire</option>
                <option>Greater London</option>
                <option>Greater Manchester</option>
                <option>Hampshire</option>
                <option>Herefordshire</option>
                <option>Hertfordshire</option>
                <option>Isle of Wight</option>
                <option>Kent</option>
                <option>Lancashire</option>
                <option>Leicestershire</option>
                <option>Lincolnshire</option>
                <option>Merseyside</option>
                <option>Norfolk</option>
                <option>North Yorkshire</option>
                <option>Northamptonshire</option>
                <option>Northumberland</option>
                <option>Nottinghamshire</option>
                <option>Oxfordshire</option>
                <option>Rutland</option>
                <option>Shropshire</option>
                <option>Somerset</option>
                <option>South Yorkshire</option>
                <option>Staffordshire</option>
                <option>Suffolk</option>
                <option>Surrey</option>
                <option>Tyne and Wear</option>
                <option>Warwickshire</option>
                <option>West Midlands</option>
                <option>West Sussex</option>
                <option>West Yorkshire</option>
                <option>Wiltshire</option>
                <option>Worcestershire</option>
            </optgroup>
            <optgroup label="Scotland">
                <option>Aberdeenshire</option>
                <option>Angus</option>
                <option>Argyllshire</option>
                <option>Ayrshire</option>
                <option>Banffshire</option>
                <option>Berwickshire</option>
                <option>Buteshire</option>
                <option>Cromartyshire</option>
                <option>Caithness</option>
                <option>Clackmannanshire</option>
                <option>Dumfriesshire</option>
                <option>Dunbartonshire</option>
                <option>East Lothian</option>
                <option>Fife</option>
                <option>Inverness-shire</option>
                <option>Kincardineshire</option>
                <option>Kinross</option>
                <option>Kirkcudbrightshire</option>
                <option>Lanarkshire</option>
                <option>Midlothian</option>
                <option>Morayshire</option>
                <option>Nairnshire</option>
                <option>Orkney</option>
                <option>Peeblesshire</option>
                <option>Perthshire</option>
                <option>Renfrewshire</option>
                <option>Ross-shire</option>
                <option>Roxburghshire</option>
                <option>Selkirkshire</option>
                <option>Shetland</option>
                <option>Stirlingshire</option>
                <option>Sutherland</option>
                <option>West Lothian</option>
                <option>Wigtownshire</option>
            </optgroup>
            <optgroup label="Wales">
                <option>Anglesey</option>
                <option>Brecknockshire</option>
                <option>Caernarfonshire</option>
                <option>Carmarthenshire</option>
                <option>Cardiganshire</option>
                <option>Denbighshire</option>
                <option>Flintshire</option>
                <option>Glamorgan</option>
                <option>Merioneth</option>
                <option>Monmouthshire</option>
                <option>Montgomeryshire</option>
                <option>Pembrokeshire</option>
                <option>Radnorshire</option>
            </optgroup>
            <optgroup label="Northern Ireland">
                <option>Antrim</option>
                <option>Armagh</option>
                <option>Down</option>
                <option>Fermanagh</option>
                <option>Londonderry</option>
                <option>Tyrone</option>
            </optgroup>
            <!-- FOR STRIPE UK END-->
            <option value="N/A">Other</option>
        </select>
    </div>

    <div class="col-md-4">
        <label>Country</label><span style="color: red;padding-left:2px">*</span>
        <select data-stripe="address-country" id="country" class="form-control">
            <option value="select">Please Select</option>
            <option value="US">United States</option>
            <option value="CA">Canada</option>
            <option value="UK">United Kingdom</option>
            <option value="AU">Australia</option>
            <option value="AF">Afghanistan</option>
            <option value="AL">Albania</option>
            <option value="DZ">Algeria</option>
            <option value="AS">American Samoa</option>
            <option value="AD">Andorra</option>
            <option value="AO">Angola</option>
            <option value="AI">Anguilla</option>
            <option value="AQ">Antarctica</option>
            <option value="AG">Antigua and Barbuda</option>
            <option value="AR">Argentina</option>
            <option value="AM">Armenia</option>
            <option value="AW">Aruba</option>
            <option value="AT">Austria</option>
            <option value="AZ">Azerbaijan</option>
            <option value="BS">Bahamas</option>
            <option value="BH">Bahrain</option>
            <option value="BD">Bangladesh</option>
            <option value="BB">Barbados</option>
            <option value="BY">Belarus</option>
            <option value="BE">Belgium</option>
            <option value="BZ">Belize</option>
            <option value="BJ">Benin</option>
            <option value="BM">Bermuda</option>
            <option value="BT">Bhutan</option>
            <option value="BO">Bolivia</option>
            <option value="BA">Bosnia and Herzegovina</option>
            <option value="BW">Botswana</option>
            <option value="BR">Brazil</option>
            <option value="BN">Brunei Darussalam</option>
            <option value="BG">Bulgaria</option>
            <option value="BF">Burkina Faso</option>
            <option value="BI">Burundi</option>
            <option value="KH">Cambodia</option>
            <option value="CM">Cameroon</option>
            <option value="CV">Cape Verde</option>
            <option value="KY">Cayman Islands</option>
            <option value="CF">Central African Republic</option>
            <option value="TD">Chad</option>
            <option value="CL">Chile</option>
            <option value="CN">China</option>
            <option value="CX">Christmas Island</option>
            <option value="CC">Cocos (Keeling) Islands</option>
            <option value="CO">Colombia</option>
            <option value="KM">Comoros</option>
            <option value="CG">Congo</option>
            <option value="CD">Congo, The Democratic Republic of the</option>
            <option value="CK">Cook Islands</option>
            <option value="CR">Costa Rica</option>
            <option value="CI">Cote D`Ivoire</option>
            <option value="HR">Croatia</option>
            <option value="CY">Cyprus</option>
            <option value="CZ">Czech Republic</option>
            <option value="DK">Denmark</option>
            <option value="DJ">Djibouti</option>
            <option value="DM">Dominica</option>
            <option value="DO">Dominican Republic</option>
            <option value="EC">Ecuador</option>
            <option value="EG">Egypt</option>
            <option value="SV">El Salvador</option>
            <option value="GQ">Equatorial Guinea</option>
            <option value="ER">Eritrea</option>
            <option value="EE">Estonia</option>
            <option value="ET">Ethiopia</option>
            <option value="FK">Falkland Islands (Malvinas)</option>
            <option value="FO">Faroe Islands</option>
            <option value="FJ">Fiji</option>
            <option value="FI">Finland</option>
            <option value="FR">France</option>
            <option value="GF">French Guiana</option>
            <option value="PF">French Polynesia</option>
            <option value="GA">Gabon</option>
            <option value="GM">Gambia</option>
            <option value="GE">Georgia</option>
            <option value="DE">Germany</option>
            <option value="GH">Ghana</option>
            <option value="GI">Gibraltar</option>
            <option value="GR">Greece</option>
            <option value="GL">Greenland</option>
            <option value="GD">Grenada</option>
            <option value="GP">Guadeloupe</option>
            <option value="GU">Guam</option>
            <option value="GT">Guatemala</option>
            <option value="GN">Guinea</option>
            <option value="GW">Guinea-Bissau</option>
            <option value="GY">Guyana</option>
            <option value="HT">Haiti</option>
            <option value="HN">Honduras</option>
            <option value="HK">Hong Kong</option>
            <option value="HU">Hungary</option>
            <option value="IS">Iceland</option>
            <option value="IN">India</option>
            <option value="ID">Indonesia</option>
            <option value="IR">Iran (Islamic Republic Of)</option>
            <option value="IQ">Iraq</option>
            <option value="IE">Ireland</option>
            <option value="IL">Israel</option>
            <option value="IT">Italy</option>
            <option value="JM">Jamaica</option>
            <option value="JP">Japan</option>
            <option value="JO">Jordan</option>
            <option value="KZ">Kazakhstan</option>
            <option value="KE">Kenya</option>
            <option value="KI">Kiribati</option>
            <option value="KP">Korea North</option>
            <option value="KR">Korea South</option>
            <option value="KW">Kuwait</option>
            <option value="KG">Kyrgyzstan</option>
            <option value="LA">Laos</option>
            <option value="LV">Latvia</option>
            <option value="LB">Lebanon</option>
            <option value="LS">Lesotho</option>
            <option value="LR">Liberia</option>
            <option value="LI">Liechtenstein</option>
            <option value="LT">Lithuania</option>
            <option value="LU">Luxembourg</option>
            <option value="MO">Macau</option>
            <option value="MK">Macedonia</option>
            <option value="MG">Madagascar</option>
            <option value="MW">Malawi</option>
            <option value="MY">Malaysia</option>
            <option value="MV">Maldives</option>
            <option value="ML">Mali</option>
            <option value="MT">Malta</option>
            <option value="MH">Marshall Islands</option>
            <option value="MQ">Martinique</option>
            <option value="MR">Mauritania</option>
            <option value="MU">Mauritius</option>
            <option value="MX">Mexico</option>
            <option value="FM">Micronesia</option>
            <option value="MD">Moldova</option>
            <option value="MC">Monaco</option>
            <option value="MN">Mongolia</option>
            <option value="MS">Montserrat</option>
            <option value="MA">Morocco</option>
            <option value="MZ">Mozambique</option>
            <option value="NA">Namibia</option>
            <option value="NP">Nepal</option>
            <option value="NL">Netherlands</option>
            <option value="AN">Netherlands Antilles</option>
            <option value="NC">New Caledonia</option>
            <option value="NZ">New Zealand</option>
            <option value="NI">Nicaragua</option>
            <option value="NE">Niger</option>
            <option value="NG">Nigeria</option>
            <option value="NO">Norway</option>
            <option value="OM">Oman</option>
            <option value="PK">Pakistan</option>
            <option value="PW">Palau</option>
            <option value="PS">Palestine Autonomous</option>
            <option value="PA">Panama</option>
            <option value="PG">Papua New Guinea</option>
            <option value="PY">Paraguay</option>
            <option value="PE">Peru</option>
            <option value="PH">Philippines</option>
            <option value="PL">Poland</option>
            <option value="PT">Portugal</option>
            <option value="PR">Puerto Rico</option>
            <option value="QA">Qatar</option>
            <option value="RE">Reunion</option>
            <option value="RO">Romania</option>
            <option value="RU">Russian Federation</option>
            <option value="RW">Rwanda</option>
            <option value="VC">Saint Vincent and the Grenadines</option>
            <option value="MP">Saipan</option>
            <option value="SM">San Marino</option>
            <option value="SA">Saudi Arabia</option>
            <option value="SN">Senegal</option>
            <option value="SC">Seychelles</option>
            <option value="SL">Sierra Leone</option>
            <option value="SG">Singapore</option>
            <option value="SK">Slovak Republic</option>
            <option value="SI">Slovenia</option>
            <option value="SO">Somalia</option>
            <option value="ZA">South Africa</option>
            <option value="ES">Spain</option>
            <option value="LK">Sri Lanka</option>
            <option value="KN">St. Kitts/Nevis</option>
            <option value="LC">St. Lucia</option>
            <option value="SD">Sudan</option>
            <option value="SR">Suriname</option>
            <option value="SZ">Swaziland</option>
            <option value="SE">Sweden</option>
            <option value="CH">Switzerland</option>
            <option value="SY">Syria</option>
            <option value="TW">Taiwan</option>
            <option value="TI">Tajikistan</option>
            <option value="TZ">Tanzania</option>
            <option value="TH">Thailand</option>
            <option value="TG">Togo</option>
            <option value="TK">Tokelau</option>
            <option value="TO">Tonga</option>
            <option value="TT">Trinidad and Tobago</option>
            <option value="TN">Tunisia</option>
            <option value="TR">Turkey</option>
            <option value="TM">Turkmenistan</option>
            <option value="TC">Turks and Caicos Islands</option>
            <option value="TV">Tuvalu</option>
            <option value="UG">Uganda</option>
            <option value="UA">Ukraine</option>
            <option value="AE">United Arab Emirates</option>
            <option value="UY">Uruguay</option>
            <option value="UZ">Uzbekistan</option>
            <option value="VU">Vanuatu</option>
            <option value="VE">Venezuela</option>
            <option value="VN">Viet Nam</option>
            <option value="VG">Virgin Islands (British)</option>
            <option value="VI">Virgin Islands (U.S.)</option>
            <option value="WF">Wallis and Futuna Islands</option>
            <option value="YE">Yemen</option>
            <option value="YU">Yugoslavia</option>
            <option value="ZM">Zambia</option>
            <option value="ZW">Zimbabwe</option>
        </select>
    </div>
</div>
<div class="clearfix">&nbsp;</div>