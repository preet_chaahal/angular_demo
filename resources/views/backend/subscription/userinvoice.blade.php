@extends('layouts.backend.master')

 @section('style') 
    <link rel="stylesheet" href="{{ asset('assets/backend/css/vendor/selectize.default.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/loaders.css') }}">
    <script type="text/javascript" src="{{ asset('assets/backend/js/chart.min.js') }}"></script>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.5/angular.min.js"></script>
    <script type="text/javascript" src="{{ asset('assets/backend/js/angular-chart.min.js') }}"></script>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/2.5.0/ui-bootstrap-tpls.min.js"></script>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/easy-pie-chart/2.1.6/angular.easypiechart.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-charts/0.2.7/angular-charts.min.js"></script>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.5/angular-animate.min.js"></script>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/angular-sanitize/1.6.7/angular-sanitize.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-morris/1.3.0/angular-morris.min.js"></script>
    <script type="text/javascript" src="{{ asset('assets/backend/js/search.js') }}"></script>  
        <style>
        .has-error{
            border-color: #f00 !important;
            background: #FFF;
            color: #f00;
        }
    </style>    
@stop
@section('content')

<div class="row">
    <div class="col-sm-12 mb-20">
        <h4 class="page-title">My Account</h4>
    </div>
</div>

<div class="row">
    <div class="col-lg-4 col-md-5">
        <div class="card-box pl-0 pr-0">
            @include('backend.user.partials.info', ['user' => Auth::user()])
        </div>
    </div>
    <div class="col-lg-8 col-md-7">
        <div class="card-box" style="overflow: hidden;">
            <div id="tabList">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs tabs m--20 mb-0" role="tablist" style="width:auto;">
                    @include('backend.user.partials.tabs', ['currentTab' => 'user-invoice'])
                </ul>
                <!-- Tab panes -->
                <div ng-app="keywordRevealer">
                <div ng-controller="keywordResearch">                
                <div id ="bill-sec" class="billing-sec">
                    <div class="container">
                        <div class="main-content">
                            <section class="table-sec" style="margin-top:20px;">
                                <div class="row">
                                    <div class="col-md-6 width-sm">
                                    </div>
                                </div>
                            </section>                            
                            <section class="table-sec">
                                    <div class="row">
                                        <div class="col-md-12 width-sm">
                                            <div class="table-box-2 table-comon">
                                                <div class="table-responsive-width">
                                                    <table class="table table-second">
                                                        <thead>
                                                            <tr>
                                                                <th>Invoice Amount</th>
                                                                <th>Date Paid</th>
                                                                <th>Receipt</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        <?php
                                                            foreach($invoices as $cb_row) {
                                                        ?>
                                                            <tr>
                                                                <td>${{ (isset($cb_row->amount) ? $cb_row->amount : 0)}}</td>
                                                                <td>{{(isset($cb_row->date_created) ? date("d M Y",strtotime($cb_row->date_created)) : '')}}</td>
                                                                <td><a href="javascript:void()" ng-click="downloadInvoice({{$cb_row->cb_invoice_id}})">Download</a></td>
                                                            </tr>
                                                        <?php }  ?>
                                                        </tbody>
                                                    </table>
                                                    <div class="paginaton">
                                                        <div class="left-col">
                                                            Total items: {{(isset($invoices) ? count($invoices) : 0)}}
                                                        </div>
                                                        <div class="right-col text-right">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </section>
                        </div>
                    </div>
                </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
