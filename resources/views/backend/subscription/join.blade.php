@extends('layouts.backend.master')

@section('style')
    <link rel="stylesheet" href="{{ asset('assets/css/loaders.css') }}">
    <style>
        .has-error{
            border-color: #f00 !important;
            background: #FFF;
            color: #f00;
        }
    </style>
@stop

@section('content')

    <!-- Modal -->
    <div id="loader" class="modal-backdrop fade">
        <div class="line-spin-fade-loader center">
            <div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12 mb-20">
            <h4 class="page-title">My Account</h4>
            <!-- <ol class="breadcrumb">
                <li><a href="{{ URL::route('dashboard') }}">Dashboard</a></li>
                <li class="active">My Account</li>
            </ol> -->
        </div>
    </div>

    <div class="row">
        <div class="col-lg-4 col-md-5">
            <div class="card-box pl-0 pr-0">
                @include('backend.user.partials.info', ['user' => Auth::user()])
            </div>
        </div>

        <div class="col-lg-8 col-md-7">
            <div class="card-box" style="overflow: hidden;">
                <div id="tabList">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs tabs m--20 mb-0" role="tablist" style="width:auto;">
                        @include('backend.user.partials.tabs', ['currentTab' => 'subscription'])
                    </ul>
                    <!-- Tab panes -->
                    <div class="tile-body mt-20">

                        @include('layouts.backend.partials.messages')

                        <div id="vueApp">

                            <!-- stripe -->
                            <div v-if="isStripe">
                                <div class="pull-left">
                                    <h4 class="m-t-0 header-title"><b>Credit card</b></h4>
                                    <p class="text-muted m-b-20 font-13">
                                        To update your card please fill in all fields.
                                    </p>
                                </div>

                                <!--<a href="" id="paypal" class="btn btn-primary waves-effect waves-light pull-right">
                                    <i class="fa fa-paypal m-r-5"></i> PayPal
                                </a>-->

                                <form action="{{ URL::route('subscription-post') }}" method="post" id="subscription-form" class="cb">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" name="gateway" value="stripe">
                                    
                                    <div class="form-group">
                                        <div class="col-md-3">
                                        <label for="">Subscription Plan</label>
                                        </div>
                                        @foreach($plans as $plan)
                                            @if (in_array($selectedPlan, [$plan->stripe_plan, $plan->name]))
                                                <div class="col-md-9">
                                                <input type="text" name="description" id="description" value="{{$screen_name}}" class="form-control" readonly>
                                                <input type="hidden" name="plan" id="plan" value="{{$plan->name}}" class="form-control" >
                                                </div>

                                                <div class="col-md-3">
                                                <label for="">Billing</label>
                                                </div>
                                                <div class="col-md-9">
                                                <input type="text" name="Billing" id="Billing" value="{{ucfirst(strtolower($periodUnit))}}" class="form-control" readonly="">
                                                </div>

                                                <div class="col-md-3">
                                                <label for="">Start Date</label>
                                                </div>
                                                <div class="col-md-9">
                                                <input type="text" name="Dat" id="Dat" value="{{Carbon\Carbon::now()->format('j  F, Y')}}" class="form-control" readonly="">
                                                </div>

                                                <div class="col-md-3">
                                                <label for="">Price</label>
                                                </div>
                                                <div class="col-md-9">
                                                <input type="text" name="Price" id="Price" value="${{$TotalCharges}}" class="form-control" readonly="">
                                                </div>
                                            @endif
                                        @endforeach
                                    </div>

                                    <div class="col-md-12"><hr>
                                    </div>
                                    <div>
                                        @include('backend.subscription.partials.creditcard')
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12 text-right mb-0 mt-30">
                                            @if($setupCost > 0)
                                            <p>Setup Fee ${{$setupCost}}</p>
                                            <p>Today's Sub Charge ${{$amt}}</p>
                                            @endif
                                            <p style="font-size: x-large;">Today's Total Charge ${{$TotalCharges}}</p>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group text-right">
                                                <button id="submit" class="btn btn-default waves-effect waves-light">
                                                    Subscribe Now
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="submit" id="realSubmit" value="Submit" class="hidden">
                                </form>

                            </div>
                            <!-- /stripe -->

                            <!-- <div v-if="debug">
                                <pre>@{{ $data | json }}</pre>
                            </div> -->

                        </div> <!-- /vueApp -->
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop

@section('head-script')

    <script type="text/javascript">
        _fprom = window._fprom || [];
        window._fprom = _fprom;
        _fprom.push(["event", "signup"]);
        _fprom.push(["email", "{{$user_email}}"]);
        _fprom.push(["first_name", "{{$first_name}}"]);
        _fprom.push(["wid", "{{env('FP_WTID_2')}}"]);
        _fprom.push(["api_key","{{env('FP_API_KEY_2')}}"]);
        @if(isset($_COOKIE['_fprom_track']))
        _fprom.push(["tid", "{{$_COOKIE['_fprom_track']}}"]);
        @else
        _fprom.push(["tid", ""]);
        @endif
    </script>
@endsection

@section('scripts')

    <script>
        new Vue({
            el: '#vueApp',
            data: {
                debug: false,
                isStripe: true
            },
            methods: {
                switchPaymentMethod: function () {
                    this.isStripe = !this.isStripe;
                }
            }
        })
    </script>

    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>

    <script>
        $(document).ready(function () {

            Stripe.setPublishableKey('{{ env('STRIPE_KEY')}}');

            showLoader(false);
            $('#submit').on('click', function (event) {
                event.preventDefault();
                var form = $('#subscription-form');
                var submit = $('#submit');
                var submitInitialText = submit.text();
                var card = document.getElementById("card_number").value;
                var month = document.getElementById("month").value;
                var year = document.getElementById("year").value;
                var cvc = document.getElementById("cvc_number").value;
                var address = document.getElementById("address").value;
                var zip = document.getElementById("zip").value;
                var city = document.getElementById("city").value;
                var state = document.getElementById("state").value;
                var country = document.getElementById("country").value;
                var flag = false;
                if (card == null || card == "") {
                    $('#card_number').addClass('has-error');
                    !$('#card_number').next(".span-error").length ? $('<span class="span-error" style="color:red" id="span_card_number">Must Enter Credit Card Number</span>').insertAfter( $('#card_number')) : '';
                    flag = true;
                    // return false;
                } else {
                    $('#card_number').removeClass('has-error');
                    $('#span_card_number').remove();
                }
                if (month == null || month == "") {
                    $('#month').addClass('has-error');
                    flag = true;
                    // return false;
                } else {
                    $('#month').removeClass('has-error');

                }
                if (year == null || year == "") {
                    $('#year').addClass('has-error');
                    flag = true;
                    // return false;
                } else {
                    $('#year').removeClass('has-error');
                }
                if (cvc == null || cvc == "") {
                    $('#cvc_number').addClass('has-error');
                    !$('#cvc_number').next(".span-error").length ? $('<span class="span-error" style="color:red" id="span_cvc_number">Must Enter CVC</span>').insertAfter( $('#cvc_number')) : '';
                    flag = true;
                    // return false;
                } else {
                    $('#cvc_number').removeClass('has-error');
                    $('#span_cvc_number').remove();
                }
                if (address == null || address == "") {
                    $('#address').addClass('has-error');
                    !$('#address').next(".span-error").length ? $('<span class="span-error" style="color:red" id="span_address">Must Enter Address</span>').insertAfter( $('#address')) : '';
                    flag = true;
                    // return false;
                } else {
                    $('#address').removeClass('has-error');
                    $('#span_address').remove();
                }
                if (zip == null || zip == "") {
                    $('#zip').addClass('has-error');
                    !$('#zip').next(".span-error").length ? $('<span class="span-error" style="color:red" id="span_zip">Must Enter Zip</span>').insertAfter( $('#zip')) : '';
                    flag = true;
                    // return false;
                } else {
                    $('#zip').removeClass('has-error');
                    $('#span_zip').remove();
                }
                if (city == null || city == "") {
                    $('#city').addClass('has-error');
                    !$('#city').next(".span-error").length ? $('<span class="span-error" style="color:red" id="span_city">Must Enter City</span>').insertAfter( $('#city')) : '';
                    flag = true;
                    // return false;
                } else{
                    $('#city').removeClass('has-error');
                    $('#span_city').remove();
                }
                if (state == null || state == "") {
                    $('#state').addClass('has-error');
                    !$('#state').next(".span-error").length ? $('<span class="span-error" style="color:red" id="state_span">Please Select State in Dropdown</span>').insertAfter( $('#state')) : '' ;
                    flag = true;
                    // return false;
                }else{
                    $('#state').removeClass('has-error');
                    $('#state_span').remove();
                }

                if(country == 'select'){
                    $('#country').addClass('has-error');
                    !$('#country').next(".span-error").length ? $('<span class="span-error" style="color:red" id="country_span">Please Select Country in Dropdown</span>').insertAfter( $('#country')) : '';
                    flag = true;
                } else {
                    $('#country').removeClass('has-error');
                    $('#country_span').remove();                    
                }

                if(flag == true) {
                    return false;
                }

                if (country != 'select') {
                    submit.attr('disabled', 'disabled').text('Processing...');
                    $('#country').removeClass('has-error');
                    $('#country_span').remove();
                    $('#realSubmit').click();
                    return false;
                }
            });
        });

        $('#paypal').click(function () {
			//var papCookie = $('#pap_cookie').val(); // Post_Affiliate_Pro get cookie value and append to request
            var baseUrl = '{{ URL::route('subscription-get', 'PLACEHOLDER') }}';
            var plan = $('#plan').val();

            $(this).attr({
                href: baseUrl.replace('PLACEHOLDER', encodeURIComponent(plan)) //+ '?pap_cookie=' + papCookie //Post_Affiliate_Pro
            });

            showLoader(true);
        });

        function showLoader(show) {
            if (show === true) {
                $("#loader").show();
                $("#loader").addClass('in');
            } else {
                $("#loader").hide();
                $("#loader").removeClass('in');
            }
        };
    </script>
@endsection