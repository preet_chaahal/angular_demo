@extends('layouts.backend.master')

@section('style')
    <link rel="stylesheet" href="{{ asset('assets/backend/css/vendor/selectize.default.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/loaders.css') }}">
    <script type="text/javascript" src="{{ asset('assets/backend/js/chart.min.js') }}"></script>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.5/angular.min.js"></script>
    <script type="text/javascript" src="{{ asset('assets/backend/js/angular-chart.min.js') }}"></script>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/2.5.0/ui-bootstrap-tpls.min.js"></script>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/easy-pie-chart/2.1.6/angular.easypiechart.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-charts/0.2.7/angular-charts.min.js"></script>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.5/angular-animate.min.js"></script>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/angular-sanitize/1.6.7/angular-sanitize.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-morris/1.3.0/angular-morris.min.js"></script>
    <script type="text/javascript" src="{{ asset('assets/backend/js/search.js') }}"></script>   
@stop

@section('content')

    <div class="row">
        <div class="col-sm-12 mb-20">
            <h4 class="page-title">My Account</h4>
            <!-- <ol class="breadcrumb">
                <li><a href="{{ URL::route('dashboard') }}">Dashboard</a></li>
                <li class="active">My Account</li>
            </ol> -->
        </div>
    </div>

    @include('backend.user.partials.statistics')

    <div class="row">
        <div class="col-lg-12">
            <div class="card-box" style="overflow: hidden;">
                <div id="tabList">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs tabs m--20 mb-0" role="tablist" style="width:auto;">
                        @include('backend.user.partials.tabs', ['currentTab' => 'subscription'])
                    </ul>
                    <!-- Tab panes -->
                    <div class="tile-body mt-20">

                        @include('layouts.backend.partials.messages')

                        @if(Auth::user()->account_id != App\Account::FreeAccountId())
                            <div class="mt-30">
                                @if(Auth::user()->subscribed() && Auth::user()->payment_mode == getenv('PAYMENT_MODE_STRIPE'))
                                    <a href="{{ URL::route('subscription-card') }}" class="btn btn-primary waves-effect waves-light pull-right">Update Card</a>
                                @endif
                                <?php $dt = \Carbon\Carbon::now();
                                      $trial_date = date('Y-m-d',strtotime(Auth::user()->trial_ends_at));
                                      $dt->toDateString();
                                ?>
                                @if($dt->toDateString() > $trial_date && Auth::user()->trial_ends_at != NULL)
                                    <a href="#" data-toggle="modal" data-target=".confirm-modalC" class="btn btn-pink waves-effect waves-light pull-left">Cancel my subscription</a>
                                @elseif(!Auth::user()->subscription_ends_at == NULL && Auth::user()->trial_ends_at != NULL)
                                    <a href="#" data-toggle="modal" data-target=".confirm-modalC" class="btn btn-pink waves-effect waves-light pull-left">Cancel my subscription</a>
                                @elseif(!Auth::user()->cancelled())
                                    <a href="#" data-toggle="modal" data-target=".confirm-modalC" class="btn btn-pink waves-effect waves-light pull-left">Cancel my subscription</a>
                                @elseif(Auth::user()->chargebee_subscription_id != "")
                                    <a href="#" data-toggle="modal" data-target=".confirm-modalC" class="btn btn-pink waves-effect waves-light pull-left">Cancel my subscription</a>
                                @else
                                    @if(Auth::user()->subscription_ends_at)
                                        <a href="{{ URL::route('subscription-resume') }}" class="btn btn-info waves-effect waves-light pull-left">Resume subscription</a>
                                    @endif
                                @endif
                            </div>

                            <!-- Confirm Modal -->
                            <div class="modal fade confirm-modalC" tabindex="-1" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title">Confirm please</h4>
                                        </div>
                                        <div class="modal-body">
                                            @if(Auth::user()->subscribed() && Auth::user()->payment_type !== 'paypal')
                                                <p>Are you sure you want to cancel your subscription?</p>
                                            @else
                                                <p>Your subscription will end now and all of your saved data will be erased.</p>
                                            @endif

                                            <div class="alert alert-danger" id="errorConfirmMessage" style="display:none">
                                                <button type="button" class="close">�</button>
                                                Please Choose a <b>Cancellation Reason</b> before you conform your cancellation.
                                            </div>
                                            <div class="mt-20" id="cancellationReasons" style="display:none">
                                                <div class="mb-10">Please select <b>Reasons</b> for cancellation</div>
                                                @foreach($cancellation_reasons as $i => $row)
                                                <div class="checkbox checkbox-primary mb-20">
                                                    <input id="checkbox{{$i}}" value="{{$row->id}}" type="checkbox">
                                                    <label for="checkbox{{$i}}">{{$row->reason}}</label>
                                                </div>
                                                @endforeach
                                                <input type="text" id="otherReason" name="other" class="form-control" placeholder="Other reason ...">
                                                <span style="color:red;font-size:11px;display:none" id="otherError">Other reason must be bigger than <b>10</b> characters.</span>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <div class="row">
                                                <div class="col-md-6 col-sm-6 col-xs-6">
                                                    <button type="button" class="btn btn-default confirm-button" data-dismiss="modal">No</button>
                                                </div>
                                                <div class="col-md-6 col-sm-6 col-xs-6">
                                                    <a href="#" class="btn btn-pink waves-effect waves-light pull-left confirm-button" id="handleCancellationReasons">Yes</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- /.modal-content -->
                                </div><!-- /.modal-dialog -->
                            </div><!-- /.modal -->
                        @endif
                    </div>
                </div>
            </div>           
        </div>

            {{-- handle confirmation button | cancellation reasons --}}
            <script type="text/javascript">
                $(document).ready(function(){
                    var confimed = false, other = false, valid = false;
                    $('#handleCancellationReasons').click(function(event){
                        if (confimed){
                            var reasons=[];
                            $('#cancellationReasons input').each(function(){
                                if ($(this).is(":checked")){ reasons.push($(this).val()); }
                            });

                            // Other reason error
                            var ol = $('#otherReason').val().length;
                            if (ol > 0 && ol < 10 ){
                                $('#otherReason').css('border-color','red');
                                $('#otherError').show();
                                other = false;
                            }else if(ol > 10){
                                other = true;
                            }
                            if (ol == 0 || other){
                                $('#otherError').hide();
                                $('#otherReason').css('border-color','#E3E3E3');
                            }

                            valid = (reasons.length>0&&ol==0)||(reasons.length>0&&ol>10)||(reasons.length==0&&ol>10)?true:false;

                            if (valid){
                                $('#otherError').hide();
                                $('#errorConfirmMessage').hide()
                                $(this).attr('href',"{{ URL::route('subscription-cancel') }}?"+jQuery.param( {reasons:reasons, other: $('#otherReason').val()}));
                            }else{
                                $('#errorConfirmMessage').show()
                            }
                        }else{
                            event.preventDefault();
                            $('#cancellationReasons').show();
                            $(this).text('Confirm');
                            confimed = true;
                        }
                    });
                    $('#errorConfirmMessage .close').click(function(){
                        $(this).parent().hide()
                    })
                })
            </script>
 
        <div class="tile-body">
            @include('backend.subscription.partials.plans')
        </div>         
    </div>

    @if(Session::has('ga_track_event'))
        <?php
            $planData = Session::get('ga_track_event');
        ?>
        <img src="{{ asset('assets/frontend/img/1x1.png') }}" class="google_analytics" onload="ga('send', 'event', '{{$planData['plan_type']}}', '{{$planData['plan_name']}}', '{{$planData['order_id']}}', {{$planData['plan_amount']}})"/>
    @endif
@stop

@section('scripts')
    {{-- track specific events in google analytics --}}
    <script type="text/javascript">
        var gaTrackEvent = Boolean('{{Session::has('ga_track_event')}}');
        if (gaTrackEvent) {
            var planData = {!! json_encode(Session::get('ga_track_event')) !!};
            fbq('track', 'Purchase', { content_ids: ['1234', '4642', '35838'], content_type: planData.plan_name, value: parseInt(planData.plan_amount), currency: 'USD' })
        }
    </script>
    <!-- Event snippet for New Subscription (2018) conversion page -->
    <script>
        var gaTrackEvent = Boolean('{{Session::has('ga_track_event')}}');
        if (gaTrackEvent) {
            var planData = {!! json_encode(Session::get('ga_track_event')) !!};
            gtag('event', 'conversion', {
              'send_to': 'AW-805251890/MKv_CM-4iIIBELLW_P8C',
              'value': parseInt(planData.plan_amount),
              'currency': 'USD',
              'transaction_id': planData.plan_name
            });
        }
    </script>
@endsection
