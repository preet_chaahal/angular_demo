@extends('app')
@section('title')
Subscription Management
@endsection
@section('content')
<div class="container"> 
    <div class="col-md-12">
        <div class="panel panel-default">
        <div class="panel-heading">Invoices</div>

            <div class="panel-body">                
                <ul>
                    @foreach ($invoices as $invoice)
                    <li>{{ $invoice->dateString() }} - {{ $invoice->dollars() }} - <a href="/invoice/{{ $invoice->id }}">Download</a></li>
                    @endforeach
                </ul>                    
            </div>                
        </div>
    </div>
</div>


@endsection