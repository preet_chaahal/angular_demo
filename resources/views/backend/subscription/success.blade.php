@extends('layouts.backend.master')

@section('style')
@stop


@section('content')

    <div class="row">
        <div class="col-sm-12 mb-20">
            <h4 class="page-title">My Account</h4>
            <!-- <ol class="breadcrumb">
                <li><a href="{{ URL::route('dashboard') }}">Dashboard</a></li>
                <li class="active">My Account</li>
            </ol> -->
        </div>
    </div>

    @include('layouts.backend.partials.messages')

    <div class="row">
        <div class="col-md-12">
            <div class="card-box">
                <!-- tile -->
                <section class="tile tile-simple row mb-0">
                    <!-- tile body -->
                    <div class="col-md-12">
                        <div class="alert alert-success mb-0">
                            Thanks for the Subscription<br>
                            We will confirm your payment once your payment is confirmed from payment
                            gateway which generally takes few minutes.
                            <a href="{{url(route('subscription'))}}">click here</a>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

    @if(Session::has('ga_track_event'))
        <?php
            $planData = Session::get('ga_track_event');
        ?>
        <img src="{{ asset('assets/frontend/img/1x1.png') }}" class="google_analytics" onload="ga('send', 'event', '{{$planData['plan_type']}}', '{{$planData['plan_name']}}','{{$planData['order_id']}}', {{$planData['plan_amount']}})"/>
    @endif
@stop


@section('scripts')

    <!-- track specific events in google analytics -->
    <script type="text/javascript">
        var gaTrackEvent = Boolean('{{Session::has('ga_track_event')}}');
        if (gaTrackEvent) {
            var planData = {!! json_encode(Session::get('ga_track_event')) !!};
            fbq('track', 'Purchase', { content_ids: ['1234', '4642', '35838'], content_type: planData.plan_name, value: parseInt(planData.plan_amount), currency: 'USD' })
        }
    </script>
    
    <!-- Event snippet for New Subscription (2018) conversion page -->
    <script>
         var gaTrackEvent = Boolean('{{Session::has('ga_track_event')}}');
            if (gaTrackEvent) {
                var planData = {!! json_encode(Session::get('ga_track_event')) !!};
                gtag('event', 'conversion', {
                  'send_to': 'AW-805251890/MKv_CM-4iIIBELLW_P8C',
                  'value': parseInt(planData.plan_amount),
                  'currency': 'USD',
                  'transaction_id': planData.plan_name
                  });
            }
    </script>

@endsection
