@extends('layouts.backend.master')

@section('style')
@stop


@section('content')

    <div class="row">
        <div class="col-sm-12 mb-20">
            <h4 class="page-title">My Account</h4>
            <!-- <ol class="breadcrumb">
                <li><a href="{{ URL::route('dashboard') }}">Dashboard</a></li>
                <li class="active">My Account</li>
            </ol> -->
        </div>
    </div>

    <div class="row">
        <div class="col-lg-4 ">
            <div class="card-box pl-0 pr-0">
                @include('backend.user.partials.info', ['user' => Auth::user()])
            </div>
        </div>

        <div class="col-lg-8 ">
            <div class="card-box" style="overflow: hidden;">
                <div id="tabList">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs tabs m--20 mb-0" role="tablist" style="width:auto;">
                        @include('backend.user.partials.tabs', ['currentTab' => 'subscription'])
                    </ul>
                    <!-- Tab panes -->
                    <div class="tile-body mt-20">
					<iframe src="' . $hostedPage->url . '" frameborder="0" style="overflow:hidden;overflow-x:hidden;overflow-y:hidden;height:100%;width:100%;position:absolute;top:0px;left:0px;right:0px;bottom:0px" height="100%" width="100%"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
