@extends('layouts.backend.master')

@section('style')
	<style>
		.has-error{
			border-color: #f00 !important;
			background: #FFF;
			color: #f00;
		}
	</style>
@stop


@section('content')

	<div class="row">
		<div class="col-sm-12 mb-20">
			<h4 class="page-title">My Account</h4>
			<!-- <ol class="breadcrumb">
				<li><a href="{{ URL::route('dashboard') }}">Dashboard</a></li>
				<li class="active">My Account</li>
			</ol> -->
		</div>
	</div>

	<div class="row">
		<div class="col-lg-4 col-md-12">
			<div class="card-box pl-0 pr-0">
				@include('backend.user.partials.info', ['user' => Auth::user()])
			</div>
		</div>

		<div class="col-lg-8 col-md-12">
			<div class="card-box" style="overflow: hidden;">
				<div id="tabList">
					<!-- Nav tabs -->
					<ul class="nav nav-tabs tabs m--20 mb-0" role="tablist" style="width:auto;">
						@include('backend.user.partials.tabs', ['currentTab' => 'subscription'])
					</ul>

					<!-- Tab panes -->
					<div class="tile-body mt-20">
						<div>
							<h4 class="m-t-0 header-title"><b>Update Card</b></h4>
							<p class="text-muted m-b-20 font-13">
								To update your card please fill in all fields.
							</p>
						</div>

						@include('layouts.backend.partials.messages')


                                <form action="{{ URL::route('subscription-card') }}" method="post" id="subscription-form" class="cb">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" name="gateway" value="stripe">
                                    <input type="hidden" id="stripeTmpToken" name="stripeTmpToken" value="">
                                    <div class="col-md-12"><hr>
                                    </div>
                                    <div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="customer[first_name]">First Name</label>
                                    <input type="text" class="form-control" id="st_first_name" name="customer[first_name]" maxlength=50 required data-msg-required="cannot be blank">
                                    <small id="st_first_name_error" for="customer[first_name]" class="text-danger"></small>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="customer[last_name]">Last Name</label>
                                    <input type="text" class="form-control" id="st_last_name" name="customer[last_name]" maxlength=50 required data-msg-required="cannot be blank">
                                    <small id="st_last_name_error" for="customer[last_name]" class="text-danger"></small>
                                </div>
                            </div>
                        </div>
                        <h3 class="page-header">Additional Information</h3>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="addr">Address</label>
                                    <input type="text" class="form-control" id="st_addr" name="addr" 
                                           maxlength=50 required data-msg-required="cannot be blank">
                                    <small id="st_addr_error" for="addr" class="text-danger"></small>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="extended_addr">Address2</label>
                                    <input type="text" class="form-control" id="extended_addr" name="extended_addr" maxlength=50>
                                    <small id="extended_addr_error" for="extended_addr" class="text-danger"></small>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="city">City</label>
                                    <input type="text" class="form-control" id="st_city" name="city" maxlength=50
                                           required data-msg-required="cannot be blank">
                                    <small id="st_city_error" for="city" class="text-danger"></small>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="state">State</label>
                                    <input type="text" class="form-control" id="st_state"  name="state" maxlength=20
                                           required data-msg-required="cannot be blank">
                                    <small id="st_state_error" for="state" class="text-danger"></small>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="country">Country</label>
                                    <input type="text" class="form-control" id="st_country"  name="country" maxlength=20
                                           required data-msg-required="cannot be blank">
                                    <small id="st_country_error" for="country" class="text-danger"></small>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="zip_code">Zip Code</label>
                                    <input id="zip_code" type="text" class="form-control" name="zip_code" 
                                           maxlength=10 required number data-msg-required="cannot be blank">
                                    <small id="zip_code_error" for="zip_code" class="text-danger"></small>
                                </div>
                            </div>                                             
                        </div>
                        <h3 class="page-header">Payment Information</h3>
                        <div class="row">                     
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="card_no">Credit Card Number</label>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <input type="text" name="card_number" class="card-number form-control" maxlength="16" id="card_no" required data-msg-required="cannot be blank"> 
                                        </div>
                                        <div class="col-sm-6">          
                                            <span class="cb-cards hidden-xs">
                                                <span class="visa"></span>
                                                <span class="mastercard"></span>
                                                <span class="american_express"></span>
                                                <span class="discover"></span>
                                            </span>
                                        </div>
                                    </div>
                                    <small id="card_no_error" for="card_no" class="text-danger"></small>
                                </div>
                            </div>
                        </div>
                        <div class="row">                
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="expiry_month">Card Expiry</label>
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <select class="card-expiry-month form-control" id="expiry_month" 
                                                    required data-msg-required="empty">
                                                <option selected>01</option>
                                                <option>02</option>
                                                <option>03</option>
                                                <option>04</option>
                                                <option>05</option>
                                                <option>06</option>
                                                <option>07</option>
                                                <option>08</option>
                                                <option>09</option>
                                                <option>10</option>
                                                <option>11</option>
                                                <option>12</option>
                                            </select>
                                        </div>
                                        <div class="col-xs-6">
                                            <select class="card-expiry-year form-control" id="expiry_year" 
                                                    required data-msg-required="empty">
                                                <option>2018</option>
                                                <option>2019</option>
                                                <option selected="">2020</option>
                                                <option>2021</option>
                                                <option>2022</option>
                                                <option>2023</option>
                                                <option>2024</option>
                                                <option>2025</option>
                                                <option>2026</option>
                                                <option>2027</option>
                                                <option>2028</option>
                                                <option>2029</option>
                                                <option>2030</option>
                                            </select>
                                        </div>
                                    </div> 
                                    <small id="expiry_month_error" for="expiry_month" class="text-danger"></small>
                                </div>                                       
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="ccv">CVC</label>
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <input type="text" class="card-cvc form-control" id="cvc" maxlength="4" placeholder="CVC" 
                                                   required data-msg-required="empty">
                                        </div>
                                        <div class="col-xs-6">
                                            <h6 class="cb-cvv"><small>(Last 3-4 digits)</small></h6>
                                        </div>
                                    </div>
                                    <small id="cvc_error" for="cvc" class="text-danger"></small>
                                </div>
                            </div>                                      
                        </div>
                                    </div>


 

                                    <button id="subscribe-form-button" class="btn btn-default waves-effect waves-light pull-right w-150 mb-0 mt-30">Update Card</button>
                                    <input type="submit" id="realSubmit" value="Submit" class="hidden">
                                </form>


					</div>
				</div>
			</div>
		</div>
	</div>

@stop


@section('head-script')
        <!-- Adding HTML5.js -->
        <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.6.2/html5shiv.js"></script>
        <script type="text/javascript" src="https://js.stripe.com/v2/">
        </script>
        <script type="text/javascript" src="http://malsup.github.io/jquery.form.js"></script>
        <script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.js"></script>
        <!-- Setting the stripe publishable key.-->
        <script>Stripe.setPublishableKey("pk_test_mM4uvM4O6owXHLw5BdrIp5Jj");
        </script>
        <!-- It is better to have the below script as separate file.-->
        <script type="text/javascript">
        // Setting the error class and error element for form validation.
        jQuery.validator.setDefaults({
            errorClass: "text-danger",
            errorElement: "small"
        });

        // Call back function for stripe response.
        function stripeResponseHandler(status, response) {
            if (response.error) {
                // Re-enable the submit button
                $('.submit-button').removeAttr("disabled");
                // Show the errors on the form
                stripeErrorDisplayHandler(response);
                $('.subscribe_process').hide();
            } else {
                var form = $("#subscribe-form");
                // Getting token from the response json.
                var token = response['id'];
                // insert the token into the form so it gets submitted to the server
                if ($("input[name='stripeToken']").length == 1) {
                    $("input[name='stripeToken']").val(token);
                    $("#stripeTmpToken").val(token);
                } else {
                    form.append("<input type='hidden' name='stripeToken' value='" + token + "' />");
                    $("#stripeTmpToken").val(token);
                }

				if(token != "" && token != null) {
					$('#realSubmit').click();
					$("#subscribe-form-button").attr('disabled', 'disabled').text('Processing...');
				}

                /*
                var options = {
                    // post-submit callback when error returns
                    error: subscribeErrorHandler, 
                    // post-submit callback when success returns
                    success: subscribeResponseHandler, 
                    complete: function() {
                        $('.subscribe_process').hide()
                    },
                    contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
                    dataType: 'json'
                };
                // Doing AJAX form submit to your server.
                form.ajaxSubmit(options);
                */
                return false;
            }
        }

        // Handling and displaying error during form submit.
        function subscribeErrorHandler(jqXHR, textStatus, errorThrown) {
                try{
                    var resp = JSON.parse(jqXHR.responseText);
                    if ('error_param' in resp) {
                        var errorMap = {};
                        var errParam = resp.error_param;
                        var errMsg = resp.error_msg;
                        errorMap[errParam] = errMsg;
                        $("#subscribe-form").validate().showErrors(errorMap);
                    } else {
                        var errMsg = resp.error_msg;
                        $(".alert-danger").show().text(errMsg);
                    }
                } catch(err) {
                    $(".alert-danger").show().text("Error while processing your request");
                }
        }
        
        // Forward to thank you page after receiving success response.
        function subscribeResponseHandler(responseJSON) {
            window.location.replace(responseJSON.forward);
        }
        
        // Handling the error from stripe server due to invalid credit card credentials.
        function stripeErrorDisplayHandler(response) {
            //Card field map - the keys are taken from error param values sent from stripe 
            //and the values are error class name in the form.
            var errorMap = {"number": "card-number",
                "cvc": "card-cvc",
                "exp_month": "card-expiry-month",
                "exp_year": "card-expiry-year"
            };
            //Check if param exist in error
            if (response.error.param) {
                var paramClassName = errorMap[response.error.param];
                if (paramClassName) {
                    //Display error in found class
                    $('.' + paramClassName)
                            .parents('.form-group')
                            .find('.text-danger')
                            .text(response.error.message).show();
                } else {
                    $(".alert-danger").show().text(response.error.message);
                }
            } else {
                $(".alert-danger").show().text(response.error.message);
            }
        }

        $(document).ready(function() {
            $("#subscribe-form").validate({
                rules: {
                    zip_code: {number: true},
                }
            });

            function formValidationCheck(form) {
                // Checking form has passed the validation.
                if (!$(form).valid()) {
                    return false;
                }
                $(".alert-danger").hide();
                $('.subscribe_process').show();
            }

            $("#subscribe-form-button").on('click', function(e) {
                var form = $('#subscription-form');
                var st_first_name = document.getElementById("st_first_name").value;
                var st_last_name  = document.getElementById("st_last_name").value;
                //var email         = document.getElementById("email").value;
                //var phone         = document.getElementById("phone").value;
                var st_addr       = document.getElementById("st_addr").value;
                var extended_addr = document.getElementById("extended_addr").value;
                var st_city       = document.getElementById("st_city").value;
                var st_state      = document.getElementById("st_state").value;
                var st_country    = document.getElementById("st_country").value;
                var zip_code      = document.getElementById("zip_code").value;
                var card_no       = document.getElementById("card_no").value;
                var expiry_month  = document.getElementById("expiry_month").value;
                var expiry_year   = document.getElementById("expiry_year").value;
                var cvc  = document.getElementById("cvc").value;        
                var flag = false;
                if (st_first_name == null || st_first_name == "") {
                    $("#st_first_name_error").text("Must Enter First Name");
                    $("#st_first_name_error").show();
                    flag = true;
                } else {
                    $("#st_first_name_error").hide();
                }

                if (st_last_name == null || st_last_name == "") {
                    $("#st_last_name_error").text("Must Enter Last Name");
                    $("#st_last_name_error").show();
                    flag = true;
                } else {
                    $("#st_last_name_error").hide();
                }

                /*if (email == null || email == "") {
                    $("#email_error").text("Must Enter Email Address");
                    $("#email_error").show();
                    flag = true;
                } else {
                    $("#email_error").hide();
                }

                if (phone == null || phone == "") {
                    $("#phone_error").text("Must Enter Phone Number");
                    $("#phone_error").show();
                    flag = true;
                } else {
                    $("#phone_error").hide();
                }*/

                if (st_addr == null || st_addr == "") {
                    $("#st_addr_error").text("Must Enter Address");
                    $("#st_addr_error").show();
                    flag = true;
                } else {
                    $("#st_addr_error").hide();
                }

                if (st_city == null || st_city == "") {
                    $("#st_city_error").text("Must Enter City");
                    $("#st_city_error").show();
                    flag = true;
                } else {
                    $("#st_city_error").hide();
                }

                if (st_state == null || st_state == "") {
                    $("#st_state_error").text("Must Enter State");
                    $("#st_state_error").show();
                    flag = true;
                } else {
                    $("#st_state_error").hide();
                }

                if (st_country == null || st_country == "") {
                    $("#st_country_error").text("Must Enter Country");
                    $("#st_country_error").show();
                    flag = true;
                } else {
                    $("#st_country_error").hide();
                }

                if (zip_code == null || zip_code == "") {
                    $("#zip_code_error").text("Must Enter Zip Code");
                    $("#zip_code_error").show();
                    flag = true;
                } else {
                    $("#zip_code_error").hide();
                }

                if (card_no == null || card_no == "") {
                    $("#card_no_error").text("Must Enter Card Number");
                    $("#card_no_error").show();
                    flag = true;
                } else {
                    $("#card_no_error").hide();
                }

                if (expiry_month == null || expiry_month == "") {
                    $("#expiry_month_error").text("Must Enter Month");
                    $("#expiry_month_error").show();
                    flag = true;
                } else {
                    $("#expiry_month_error").hide();
                }

                if (cvc == null || cvc == "") {
                    $("#cvc_error").text("Must Enter CVC Number");
                    $("#cvc_error").show();
                    flag = true;
                } else {
                    $("#cvc_error").hide();
                }

                if(flag == true) {
                    return false;
                }

                //form validation
                formValidationCheck(this);
                if(!$(this).valid()){
                    return false;
                }
                // Disable the submit button to prevent repeated clicks and form submit
                $('.submit-button').attr("disabled", "disabled");
                // createToken returns immediately - the supplied callback 
                // submits the form if there are no errors
                Stripe.createToken({
                    number: $('.card-number').val(),
                    cvc: $('.card-cvc').val(),
                    exp_month: $('.card-expiry-month').val(),
                    exp_year: $('.card-expiry-year').val()
                }, stripeResponseHandler);
                return false; // submit from callback
            });
        });
    </script>
@endsection
