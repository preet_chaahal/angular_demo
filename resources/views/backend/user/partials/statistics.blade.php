<!-- user statistics -->
<div class="row">
    <div class="col-lg-6">
        <div class="widget-panel widget-style-2 bg-white">
            <div id="keywordSearchesBar">
                <p class="font-600">Keyword Searches<span class="text-primary pull-right">0 / 0</span></p>
                <div class="progress m-b-30">
                  <div class="progress-bar progress-bar-primary progress-animated wow animated" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="0" style="width: 0%">
                  </div>
                </div>
            </div>
            <div id="keywordEvaluationsBar">
            <p class="font-600">Keyword Evaluations <span class="text-custom pull-right">0 / 0</span></p>
                <div class="progress">
                  <div class="progress-bar progress-bar-custom progress-animated wow animated" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="0" style="width: 0%">
                  </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="widget-panel widget-style-2 bg-white">
            <div id="projectsBar">
            <p class="font-600">Saved Projects <span class="text-primary pull-right">0 / 0</span></p>
                <div class="progress m-b-30">
                  <div class="progress-bar progress-bar-primary progress-animated wow animated" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="0" style="width: 0%">
                  </div>
                </div>
            </div>
            <div id="keywordsBar">
            <p class="font-600">Saved Keywords <span class="text-custom pull-right">0 / 0</span></p>
                <div class="progress">
                  <div class="progress-bar progress-bar-custom progress-animated wow animated" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="0" style="width: 0%">
                  </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function(){
          function getUserCountsAndLimits() {
              $.ajax({
                  url: '{{ URL::route('backend.tools.keywordresearch.userCountsAndLimits')}}',
                  type: 'get',
                  success: function (data, textStatus, jqXHR) {
                    for (var i in data) {
                        if(data[i].limit == 9999){
                            $('#'+data[i].id+' span').text("Unlimited");
                        }else{
                            $('#'+data[i].id+' span').text(data[i].count + " / " + data[i].limit);
                        }
                      $('#'+data[i].id+' .progress-bar')
                      .attr('aria-valuenow', data[i].count).attr('aria-valuemax', data[i].limit)
                      .width(data[i].count * 100 / data[i].limit + '%');
                    }
                  }
              });
          };
          getUserCountsAndLimits();
        })
    </script>
</div>
