<!-- tile -->
<section class="tile tile-simple">
    <!-- tile widget -->
    <div class="tile-widget text-center">
        <div class="mb-30">
            <img class="img-circle" src="{{ $user->gravatar }}" alt="User avatar" style="width: 160px;">
        </div>
        <h4 class="mb-0 mt-20"><strong>{{ $user->name }}</strong></h4>
        <div class="mt-20">
            <span class="text-muted">{{ $user->username }}</span>
        </div>

        @if(Auth::user()->subscribed() && Auth::user()->payment_mode == getenv('PAYMENT_MODE_STRIPE'))
            <div class="mt-20">
                <span class="text-muted">Credit card: **** **** **** {{ Auth::user()->last_four }}
                </span>
                <a href="{{ URL::route('subscription-card') }}" class="waves-effect" data-toggle="tooltip" data-placement="right" title="Update Card">
                    <i class="icon-pencil pl-10"></i>
                </a>
            </div>
        @endif

        @if(Auth::user()->chargebee_active != 0 || Auth::user()->chargebee_subscription_id == null)
            @if(Auth::user()->payment_type == getenv('CHARGEBEE_TYPE'))
                <div class="mt-20">
                    <a href="{{ URL::route('subscription') }}" class="btn btn-default btn-custom btn-rounded waves-effect waves-light" data-toggle="tooltip" data-placement="right" title="Change Plan">{{ Auth::user()->account->description }}</a>
                </div>
            @else
                <div class="mt-20">
                    <a href="{{ URL::route('subscription-change') }}" class="btn btn-default btn-custom btn-rounded waves-effect waves-light" data-toggle="tooltip" data-placement="right" title="Change Plan">{{ Auth::user()->account->description }}</a>
                </div>
            @endif
        @endif

        @if(Auth::user()->is_trial == 1)
        <?php
            $currentDate = date("Y-m-d");
            $trialDate   = Auth::user()->trial_expire_date;
            if(strtotime($currentDate) <= strtotime($trialDate)) {
                $date1 = new DateTime($currentDate);
                $date2 = new DateTime($trialDate);
                $days  = $date2->diff($date1)->format('%a');
                if($days <= 1) {
                    $d= $days." day";
                } else {
                    $d= $days." days";
                }
                $remaining_days_msg = "You have ".$d." remaining in your trial subscription.";
            } else {
                $remaining_days_msg = "Your trial subscription has been expired.";
            }
        ?>
        <div class="mt-20">
            <span class="btn-rounded waves-effect waves-light">{{ $remaining_days_msg }}</span>
        </div>
        @endif
    </div>
    <!-- /tile widget -->
</section>
<!-- /tile -->
