<li class="{{ $currentTab == 'my account' ? 'active' : '' }}"><a href="{{ URL::route('auth.profile') }}">My Account</a></li>
<li class="{{ $currentTab == 'password' ? 'active' : '' }}"><a href="{{ URL::route('auth.profile.password') }}">Change Password</a></li>
<li class="{{ $currentTab == 'subscription' ? 'active' : $currentTab == 'user-invoice' ? 'active' : '' }}"><a href="{{ URL::route('subscription') }}">Subscription</a></li>
<li class="{{ $currentTab == 'delete-account' ? 'active' : '' }}"><a href="{{ URL::route('auth.profile.account.delete') }}">Delete Account</a></li>