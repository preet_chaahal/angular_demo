@extends('layouts.backend.master')

@section('style')
@stop


@section('content')

    <div class="row">
        <div class="col-sm-12 mb-20">
            <h4 class="page-title">My Account</h4>
            <!-- <ol class="breadcrumb">
                <li><a href="{{ URL::route('dashboard') }}">Dashboard</a></li>
                <li class="active">My Account</li>
            </ol> -->
        </div>
    </div>

    @include('backend.user.partials.statistics')

    <div class="row">
        <div class="col-lg-4 col-md-5">
            <div class="card-box pl-0 pr-0">
                @include('backend.user.partials.info', ['user' => Auth::user()])
            </div>
        </div>

        <div class="col-lg-8 col-md-7">
            <div class="card-box" style="overflow: hidden;">
                <div id="tabList">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs tabs m--20 mb-0" role="tablist" style="width:auto;">
                        @include('backend.user.partials.tabs', ['currentTab' => 'password'])
                    </ul>
                    <!-- Tab panes -->
                    <div class="tile-body mt-20">

                        @include('layouts.backend.partials.messages')

                        <form method="POST" action="{{ URL::route('auth.profile.password.update') }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <!-- current_password -->
                            <div class="form-group">
                                <label for="current_password">Your Current Password</label>
                                <input type="password" class="form-control" id="current_password" name="current_password">
                            </div> <!-- current_password -->

                            <!-- newpassword -->
                            <div class="form-group">
                                <label for="newpassword">New Password</label>
                                <input type="password" class="form-control" id="newpassword" name="newpassword">
                            </div> <!-- newpassword -->

                            <!-- newpassword_confirmation -->
                            <div class="form-group">
                                <label for="newpassword_confirmation">Password Confirm</label>
                                <input type="password" class="form-control" id="newpassword_confirmation" name="newpassword_confirmation">
                            </div> <!-- newpassword_confirmation -->

                            <div class="form-group text-right mb-0 mt-30">
                                <input type="submit" class="btn btn-default waves-effect waves-light" value="Save Account Settings">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop


@section('scripts')
@stop
