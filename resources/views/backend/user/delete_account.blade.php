@extends('layouts.backend.master')

@section('style')
@stop


@section('content')

    <div class="row">
        <div class="col-sm-12 mb-20">
            <h4 class="page-title">My Account</h4>
        <!-- <ol class="breadcrumb">
                <li><a href="{{ URL::route('dashboard') }}">Dashboard</a></li>
                <li class="active">My Account</li>
            </ol> -->
        </div>
    </div>

    @include('backend.user.partials.statistics')

    <div class="row">
        <div class="col-lg-4 col-md-5">
            <div class="card-box pl-0 pr-0">
                @include('backend.user.partials.info', ['user' => Auth::user()])
            </div>
        </div>

        <div class="col-lg-8 col-md-7">
            <div class="card-box" style="overflow: hidden;">
                <div id="tabList">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs tabs m--20 mb-0" role="tablist" style="width:auto;">
                        @include('backend.user.partials.tabs', ['currentTab' => 'delete-account'])
                    </ul>
                    <!-- Tab panes -->
                    <div class="tile-body mt-20">

                        @if(isset($errors))
                                @include('layouts.backend.partials.messages')
                        @else

                        @endif

                        <form method="POST" id="delete-account-form" action="{{ URL::route('auth.profile.account.delete')}}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <span>When you delete your account, All of your data will be permanently removed. After you delete your account.you can't sign up again with same username,and we can't reactivate deleted accounts</span>
                            <div class="form-group text-right mb-0 mt-30">
                                <input type="submit" id="delete-account-btn" class="btn btn-default waves-effect waves-light" value="Confirm"/>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop


@section('scripts')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>

    <script type="text/javascript">

        $(document).ready(function(){
            $("#delete-account-btn").click(function(event){
                $.confirm({
                    title: 'Delete Account Confirmation!',
                    content: 'Are you sure want to delete your account!',
                    buttons: {
                        confirm: function () {
                            $( "#delete-account-form" ).submit();
                        },
                        cancel: function () {
                            $.alert('Canceled!');
                        },
                    }
                });
                event.preventDefault();
            });
        });

    </script>
@stop
