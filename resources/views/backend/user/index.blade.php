@extends('layouts.backend.master')

@section('style')
    <link rel="stylesheet" href="{{ asset('assets/backend/css/vendor/selectize.default.css') }}">

    <style>
        .selectItem >span { color: #000; }
        .selectize-dropdown-content {
            max-height: 140px;
        }
    </style>
@stop


@section('content')

    <div class="row">
        <div class="col-sm-12 mb-20">
            <h4 class="page-title">My Account</h4>
            <!-- <ol class="breadcrumb">
                <li><a href="{{ URL::route('dashboard') }}">Dashboard</a></li>
                <li class="active">My Account</li>
            </ol> -->
        </div>
    </div>

    @include('backend.user.partials.statistics')

    <div class="row">
        <div class="col-lg-4 col-md-5">
            <div class="card-box pl-0 pr-0">
                @include('backend.user.partials.info', ['user' => Auth::user()])
            </div>
        </div>

        <div class="col-lg-8 col-md-7">
            <div class="card-box" style="overflow: hidden;">
                <div id="tabList">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs tabs m--20 mb-0" role="tablist" style="width:auto;">
                        @include('backend.user.partials.tabs', ['currentTab' => 'my account'])
                    </ul>
                    <!-- Tab panes -->
                    <div class="tile-body mt-20">

                        @include('layouts.backend.partials.messages')

                        <form method="POST" action="{{ URL::route('auth.profile.update') }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="id" value="{{ Auth::user()->id }}">

                            <!-- name -->
                            <div class="form-group">
                                <label for="name">Your Name</label>
                                <input type="text" class="form-control" id="name" name="name" value="{{ old('name', Auth::user()->name) }}">
                            </div> <!-- name -->

                            <!-- email -->
                            <div class="form-group">
                                <label for="email">Email Address</label>
                                <input type="text" class="form-control" id="email" name="email" value="{{ old('email', Auth::user()->email) }}">
                            </div> <!-- email -->

                            <!-- default search engine -->
                            <div class="form-group">
                                <label class="control-label" for="tld">Default Search Engine</label>
                                <select class="selectize tld-selectize" id="tld" name="defaultSearchEngine"></select>
                            </div> <!-- default search engine -->

                            <!-- default search engine -->
                            <div class="form-group">
                                <label class="control-label" for="lang">Default Search Language</label>
                                <select class="selectize lang-selectize" id="lang" name="defaultSearchLanguage"></select>
                            </div> <!-- default search engine -->

                            <!-- avatar -->
                            <div class="form-group mt-20">
                                <div class="alert alert-info">
                                    <p>To change your avatar please see <a href="https://en.gravatar.com/">gravatar</a></p>
                                </div>
                            </div> <!-- avatar -->

                            <div class="form-group text-right mb-0 mt-30">
                                <input type="submit" class="btn btn-default waves-effect waves-light" value="Save Account Settings">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if(Session::has('ga_track_event'))
        <img src="{{ asset('assets/frontend/img/1x1.png') }}" class="google_analytics" onload="ga('send', 'event', 'Registration', 'new', 'verified', 0)"/>
    @endif
@stop

@section('scripts')
    <script src="{{ asset('assets/backend/js/vendor/selectize/selectize.js') }}"></script>

    <!-- track specific events in google analytics -->
    <script type="text/javascript">
        var gaTrackEvent = Boolean('{{Session::has('ga_track_event')}}');
        if (gaTrackEvent) {
            fbq('track', 'Lead', { value: 0, currency: 'USD' });
        }
    </script>
    
    <!-- Event snippet for Free Account (2018) conversion page -->
    <script>
      var gaTrackEvent = Boolean('{{Session::has('ga_track_event')}}');
        if (gaTrackEvent) {
                  gtag('event', 'conversion', {'send_to': 'AW-805251890/1ca0COSi8IEBELLW_P8C'});
        }
    </script>

    <script>
        var searchEngines = {!! $searchEngines !!};
        var searchLanguages = {!! $searchLanguages !!};

        $('.tld-selectize').selectize({
            plugins: ['remove_button'],
            valueField: 'name',
            labelField: 'engine',
            searchField: ['name', 'engine'],
            delimiter: ',',
            persist: false,
            hideSelected: true,
            openOnFocus: true,
            items: ['{{ $selectedEngine }}'],
            render: {
                item: function(item, escape) {
                    return '<div>' +
                        (item.engine ? '<span class="email">' + escape(item.engine) + '</span>' : '') +
                        (item.name ? '<span class="name"> ‒ ' + escape(item.name) + '</span>' : '') +
                    '</div>';
                },
                option: function(item, escape) {
                    var label = item.name || item.engine;
                    var caption = item.name ? item.engine : null;
                    return '<div class="selectItem">' +
                        (caption ? '<span>' + escape(caption) + '</span>' : '') +
                        '<span> ‒ ' + escape(label) + '</span>' +
                    '</div>';
                }
            },
            options: searchEngines
        });

        $('.lang-selectize').selectize({
            plugins: ['remove_button'],
            valueField: 'code',
            labelField: 'name',
            searchField: ['name', 'code'],
            delimiter: ',',
            persist: false,
            hideSelected: true,
            openOnFocus: true,
            items: ['{{ $selectedLanguage }}'],
            render: {
                item: function (item, escape) {
                    return '<div>' +
                            '<span>' + item.name + ' (' + item.code + ')</span>' +
                            '</div>';
                },
                option: function (item, escape) {
                    return '<div>' +
                            '<span>' + item.name + ' (' + item.code + ')</span>' +
                            '</div>';
                }
            },
            options: searchLanguages
        });
    </script>
@stop
