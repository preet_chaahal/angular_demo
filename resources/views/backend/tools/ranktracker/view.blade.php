@extends('layouts.backend.master')

@section('style')
    <style type="text/css">
        .child-data.expand {
            display: table-row;
            transition-duration: 1s;
        }
        .parent-expand,
        .child-data.expand {
            background: #ebeff2;
        }
        .child-data {
            display: none;
            transition-duration: 1s;
        }
        table.dataTable thead th,
        table.dataTable thead td {
            padding-left: 10px;
        }
        .row-active,
        table.table tbody tr:hover{
            background: #ebeff2;
        }
        table.table tbody tr thead tr:hover{
            background: none;
        }
        table.table table tbody tr td {
            border-top: none;
        }
        .f-s-17 {
            font-size: 17px;
        }
        ul.controls {
            list-style: none;
            margin: 0;
            padding: 0;
        }
        .actions ul.dropdown-menu.pull-right {
            right: -11px;
            top: 26px;
        }
        .selectable tr{
            cursor: pointer;
        }
    </style>
@stop


@section('content')
    <div class="row mb-20">
        <div class="col-sm-12">
            <h4 class="page-title">Rank Tracker</h4>
            <!-- <ol class="breadcrumb">
                <li><a href="{{ URL::route('dashboard') }}">Dashboard</a></li>
                <li><a href="{{ URL::route('backend.tools.ranktracker') }}">Rank Tracker</a></li>
                <li class="active">Details</li>
            </ol> -->
        </div>
    </div>

    @include('layouts.backend.partials.messages')

    <div class="row">
        <div class="col-lg-8">
            <div class="card-box pb-50" style="height: 422px;">
                <h4 class="text-dark header-title pull-left m-t-0 m-b-30"><b>VIEWING RANKTRACKER</b></h4>
                <ul class="controls pull-right">
                    <li>
                        <a role="button" tabindex="0" class="pickDate2" id="datePicker">
                            <span id="dateSpan">{{ date('M d, Y', strtotime($dates['start'])) }}
                                - {{ date('M d, Y', strtotime($dates['stop'])) }}</span>&nbsp;&nbsp;<i
                                    class="fa fa-angle-down"></i>
                            <div class="hidden">
                                <form id="dateForm"
                                      action="{{ URL::route('backend.tools.ranktracker.setDate') }}"
                                      method="post">
                                    <input type="hidden" name="id"
                                           value="{{ isset($group->idGroup)?$group->idGroup:'' }}">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" name="start_date" id="start_date" value="">
                                    <input type="hidden" name="stop_date" id="stop_date" value="">
                                </form>
                            </div>
                        </a>
                    </li>
                </ul>
                <div class="chart-message cb">No data to display, We are collecting data for this project. Please check back in 24 hour hours.</div>
                <canvas id="lineChart" height="100"></canvas>
            </div>
        </div>

        <div class="col-lg-4">
            <div class="card-box" style="height: 422px;">
                <h4 class="text-dark header-title m-t-0 m-b-30">Keyword Position</h4>
                <div class="chart-message">No data to display, We are collecting data for this project. Please check back in 24 hour hours.</div>
                <div id="donut-chart-wrapper">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="widget-chart text-center">
                                <!-- <div id="sparkline3"></div> -->
                                <canvas id="donutChart" height="270"></canvas>
                            </div>
                        </div>
                    </div>

                    <div id="keywordPositionLabel" class="donut-chart-list">
                        <div class="row">
                            <div class="col-xs-4 pr-0">
                                Top 3: <span>0</span>
                            </div>
                            <div class="col-xs-4 p-0">
                                Top 10: <span>0</span>
                            </div>
                            <div class="col-xs-4 p-0">
                                Top 20: <span>0</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-4 pr-0">
                                Top 30: <span>0</span>
                            </div>
                            <div class="col-xs-4 p-0">
                                Top 100: <span>0</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-12">
            <div class="card-box">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="pull-left">
                            <h4 class="m-t-0 header-title"><b>SEARCH VOLUME & CPC</b></h4>
                            <p class="text-muted m-b-20 font-13">
                                Detailed information about keywords.
                            </p>
                        </div>
                        <div id="actions" class="input-group pull-right">
                            @if(count($casperResults) < count($group->keywords))
                                <a href="{{ URL::route('backend.tools.ranktracker.casper.generator', $group->idGroup) }}" onclick="return canRefreshData()"
                                   tabindex="0" class="btn btn-pink waves-effect waves-light mr-20">Obtain Data
                                </a>
                            @endif
                            <a href="#" tabindex="0" data-toggle="modal" data-target="#myModal" class="btn btn-primary waves-effect waves-light mr-20">
                                Add Event
                            </a>
                            <a href="{{ URL::route('backend.tools.ranktracker.edit', isset($group->idGroup)?$group->idGroup:'')}}"
                               tabindex="0" class="btn btn-default waves-effect waves-light">Edit Project
                            </a>
                            <a href="{{ URL::route('backend.tools.ranktracker.view', $projectId) }}?downloadCSV=1" class="btn btn-default waves-effect waves-light pull-right ml-20">
                                Download CSV
                                <span class="btn-label btn-label-right"><i class="fa fa-file-excel-o"></i></span>
                            </a>
                        </div>  
                        @if(count($casperResults))
                            <div class="table-container">
                                <table class="table">
                                    <thead style="white-space: nowrap">
                                        <tr>
                                            <th>
                                                <a href="#" onclick="toggleCharts()">Show Chart</a>
                                            </th><th>
                                                Date Added
                                            </th><th>
                                                Keyword
                                            </th><th class="text-right">
                                                Monthly Searches
                                            </th><th class="text-right">
                                                CPC
                                            </th><th class="text-right">
                                                Number Of Words
                                            </th><th class="text-right">
                                                Current
                                            </th><th class="text-right">
                                                Best
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody class="selectable">
                                    @foreach($casperResults as $result)
                                        @if(isset($result->keyword) && !empty($result->keyword))
                                            <tr id="keyword_{{$result->idKeyword}}" class="keyword-row">
                                                <td class="pt-0 pb-0">
                                                    <a href="#" data-chart-id="{{$result->idKeyword}}" class="link-icon waves-effect active-link-icon">
                                                        <i class="text-20 icon-graph"></i>
                                                    </a>
                                                </td>    
                                                <td>
                                                    {{ isset($result->updated_at)?$result->updated_at->format('m/d/Y'):'' }}
                                                </td>
                                                <td>
                                                    {{ isset($result->keyword)?$result->keyword:'' }}
                                                </td>
                                                <td class="text-right">
                                                    {{ isset($result->avg_month_searches)? number_format(intval($result->avg_month_searches), 0, '', ','):'0' }}
                                                </td>
                                                <td class="text-right">
                                                    {{ isset($result->suggested_bid)?$result->suggested_bid:'' }}
                                                </td>
                                                <td class="text-right">
                                                    {{ isset($result->keyword) ? count(explode(" ", $result->keyword)) : '0' }}
                                                </td>
                                                <td class="text-right">
                                                    {{isset($result->newRank)?$result->newRank:'N/A'}}
                                                </td>
                                                <td class="text-right">
                                                    {{isset($result->change)?$result->bestRank:''}}
                                                </td>
                                            </tr>
                                        @endif
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        @else
                            <div class="alert alert-warning cb">
                                <strong>Sorry,</strong>
                                there currently is no data found. You can refresh the page by <a
                                        href="{{ URL::route('backend.tools.ranktracker.casper.generator', $group->idGroup) }}"
                                        onclick="return canRefreshData();">clicking here</a>, or wait until our
                                engine picks up your domain.
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

{{--    <div class="page page-dashboard">
        <div class="row">
            <div class="col-md-8">
                <!-- chart -->
                <section class="tile">
                    <!-- tile header -->
                    <div class="tile-header bg-slategray">
                        <h4 class="custom-font text-uppercase word-wrap">Viewing Ranktracker:
                            <strong>
                               <span data-toggle="tooltip" data-placement="top"
                                     title="{{$group->name}}">{{ isset($group->name)?str_limit($group->name,25):''}}</span>
                            </strong></h4>
                        <ul class="controls">
                            <li>
                                <a role="button" tabindex="0" class="pickDate2" id="datePicker">
                                    <span id="dateSpan">{{ date('M d, Y', strtotime($dates['start'])) }}
                                        - {{ date('M d, Y', strtotime($dates['stop'])) }}</span>&nbsp;&nbsp;<i
                                            class="fa fa-angle-down"></i>
                                    <div class="hidden">
                                        <form id="dateForm"
                                              action="{{ URL::route('backend.tools.ranktracker.setDate') }}"
                                              method="post">
                                            <input type="hidden" name="id"
                                                   value="{{ isset($group->idGroup)?$group->idGroup:'' }}">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <input type="hidden" name="start_date" id="start_date" value="">
                                            <input type="hidden" name="stop_date" id="stop_date" value="">
                                        </form>
                                    </div>
                                </a>
                            </li>
                            <li class="dropdown">
                                <a role="button" tabindex="0" class="dropdown-toggle settings" data-toggle="dropdown">
                                    <i class="fa fa-cog"></i>
                                    <i class="fa fa-spinner fa-spin"></i>
                                </a>
                                <ul class="dropdown-menu pull-right with-arrow animated littleFadeInUp">
                                    <li>
                                        <a role="button" tabindex="0" class="tile-toggle">
                                            <span class="minimize"><i class="fa fa-angle-down"></i>&nbsp;&nbsp;&nbsp;Minimize</span>
                                            <span class="expand"><i class="fa fa-angle-up"></i>&nbsp;&nbsp;&nbsp;Expand</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ URL::route('backend.tools.ranktracker.edit', isset($group->idGroup)?$group->idGroup:'')}}"
                                           tabindex="0" class="">
                                            <i class="fa fa-pencil"></i> Edit
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ URL::route('backend.tools.ranktracker.download', isset($group->idGroup)?$group->idGroup:'')}}"
                                           tabindex="0" class="">
                                            <i class="fa fa-cloud-download"></i> Download Data
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ URL::route('backend.tools.ranktracker.delete', isset($group->idGroup)?$group->idGroup:'') }}"
                                           tabindex="0" class=""
                                           onclick="return confirm('Are you sure you wish to delete this group?');">
                                            <i class="fa fa-trash-o"></i> Delete
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div> <!-- /tile header -->
                    <!-- tile body -->
                    <div class="tile-body">
                        <!-- chart -->
                        <div>
                            <canvas id="chart" style="width: 100%; height: 500px;"></canvas>
                        </div>
                        <!-- /chart -->
                    </div> <!-- /tile body -->
                </section>
                <!-- /chart -->

                <!-- search volume and cpc -->

                <!-- /search volume and cpc -->
            </div> <!-- col-md-8 -->
            <div class="col-md-4">
                <!-- Visibility Score -->

                <section class="tile">
                    <!-- tile header -->
                    <div class="tile-header bg-slategray text-center">
                        <h4 class="custom-font text-uppercase"><strong>Visibility Score</strong></h4>
                    </div> <!-- /tile header -->
                    <!-- tile body -->
                    <div class="tile-body">
                        <ul class="legend">
                            <li>
                                <span><b>Visibility Score:</b> {{$visibilityScore}}</span>
                            </li>
                        </ul>
                        <ul class="legend">
                            <li>
                                <span><b>Visibility Percent:</b> {{$visibilityPercentage}}%</span>
                            </li>
                        </ul>
                    </div> <!-- /tile body -->
                </section> <!-- Legend -->

                <!-- Legend -->
                <section class="tile">
                    <!-- tile header -->
                    <div class="tile-header bg-slategray text-center">
                        <h4 class="custom-font text-uppercase"><strong>Legend</strong></h4>
                    </div> <!-- /tile header -->
                    <!-- tile body -->
                    <div class="tile-body">
                        @foreach($chartJs['legend'] as $legend)
                            <ul class="legend">
                                <li>
                                    <span style="background: {{ isset($legend['fillColor'])?$legend['fillColor']:'' }}; border: 1px solid {{ isset($legend['strokeColor'])?$legend['strokeColor']:'' }};"></span> {{ isset($legend['label'])?$legend['label']:''}}
                                </li>
                            </ul>
                        @endforeach
                    </div> <!-- /tile body -->
                </section> <!-- Legend -->

                <!-- Events -->
                <section class="tile">
                    <!-- tile header -->
                    <div class="tile-header bg-slategray text-center">
                        <h4 class="custom-font text-uppercase">
                            <strong>Events</strong>
                        </h4>
                        <ul class="controls">
                            <li>
                                <a href="#" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i> Add
                                    Event</a>
                            </li>
                        </ul>
                    </div> <!-- /tile header -->
                    <!-- tile body -->
                    <div class="tile-body">
                        @if(isset($group->target->events) && $group->target->events->count() > 0)
                            <table class="table table-bordered table-hover table-striped">
                                <thead>
                                <tr>
                                    <th width="25%">
                                        Date
                                    </th>
                                    <th>
                                        Description
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($group->target->events->sortBy('date') as $event)
                                    <tr>
                                        <td>
                                            {{ date('M d, Y', strtotime($event->date)) }}
                                        </td>
                                        <td>
                                            {{ isset($event->event)?$event->event:'' }}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="alert alert-warning alert-dismissable">
                                <strong>No Events Found!</strong>
                            </div>
                        @endif
                    </div> <!-- /tile body -->
                </section> <!-- Events -->


                <section class="tile">
                    <!-- tile header -->
                    <div class="tile-header bg-slategray text-center">
                        <h4 class="custom-font text-uppercase"><strong>Keyword Position</strong></h4>
                    </div> <!-- /tile header -->
                    <!-- tile body -->
                    <div class="tile-body">
                        <table>
                            <tr>
                                <td>
                                    <div id="container"
                                         style="min-width: 210px; height: 145px; max-width: 250px; margin-top: -14px; margin-bottom: -10px">
                                    </div>
                                </td>
                                <td style="vertical-align: top">
                                    <table>
                                        <tr>
                                            <td>
                                                <span class="legend">Top 3: </span>
                                            </td>
                                            <td>
                                                <span class="legend"
                                                      style="color: #00dfdf;">{{$keywordPositionChartTableData[3]}}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="legend">Top 10: </span>
                                            </td>
                                            <td>
                                                <span class="legend"
                                                      style="color: #00dfdf;">{{$keywordPositionChartTableData[10]}}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="legend">Top 20: </span>
                                            </td>
                                            <td>
                                                <span class="legend"
                                                      style="color: #00dfdf;">{{$keywordPositionChartTableData[20]}}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="legend">Top 30: </span>
                                            </td>
                                            <td>
                                                <span class="legend"
                                                      style="color: #00dfdf;">{{$keywordPositionChartTableData[30]}}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="legend">Top 100: </span>
                                            </td>
                                            <td>
                                                <span class="legend"
                                                      style="color: #00dfdf;">{{$keywordPositionChartTableData[100]}}</span>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                        </table>

                    </div> <!-- /tile body -->
                </section>


            </div> <!-- col-md-4 -->

            <div class="col-md-12">
                <section class="tile">
                    <!-- tile header -->
                    <div class="tile-header bg-slategray text-center">
                        <h4 class="custom-font text-uppercase">Search Volume &amp; CPC</h4>
                        <ul class="controls">
                            <li class="dropdown">
                                <a role="button" tabindex="0" class="dropdown-toggle settings" data-toggle="dropdown">
                                    <i class="fa fa-cog"></i>
                                    <i class="fa fa-spinner fa-spin"></i>
                                </a>
                                <ul class="dropdown-menu pull-right with-arrow animated littleFadeInUp">
                                    <li>
                                        <a role="button" tabindex="0" class="tile-toggle">
                                            <span class="minimize"><i class="fa fa-angle-down"></i>&nbsp;&nbsp;&nbsp;Minimize</span>
                                            <span class="expand"><i class="fa fa-angle-up"></i>&nbsp;&nbsp;&nbsp;Expand</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ URL::route('backend.tools.ranktracker.casper.generator', isset($group->idGroup)?$group->idGroup:'') }}"
                                           tabindex="0" class="" onclick="return canRefreshData();">
                                            <i class="fa fa-refresh"></i> Refresh
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div> <!-- /tile header -->
                    <!-- tile body -->
                    <div class="tile-body" id="cpc_results">
                        <div class="table-responsive">

                            @if($casperResults)
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>
                                            Keyword
                                        </th>
                                        <th class="text-center">
                                            Currency
                                        </th>
                                        <th class="text-center">
                                            Avg Monthly Searches
                                        </th>
                                        <th class="text-center">
                                            CPC
                                        </th>
                                        <th class="text-center">
                                            Number Of Words
                                        </th>
                                        <th class="text-center">
                                            Old
                                        </th>
                                        <th class="text-center">
                                            Current
                                        </th>
                                        <th>
                                            +/-
                                        </th>
                                        <th>
                                            Best
                                        </th>
                                        <th class="text-center">
                                            Updated at
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($casperResults as $result)
                                        @if(isset($result->keyword) && !empty($result->keyword))
                                            <tr>
                                                <td>
                                                    {{ isset($result->keyword)?$result->keyword:'' }}
                                                </td>
                                                <td class="text-center">
                                                    {{ isset($result->currency)?$result->currency:'' }}
                                                </td>
                                                <td class="text-center">
                                                    {{ isset($result->avg_month_searches)?$result->avg_month_searches:'' }}
                                                </td>
                                                <td class="text-center">
                                                    {{ isset($result->suggested_bid)?$result->suggested_bid:'' }}
                                                </td>
                                                <td class="text-center">
                                                    {{ isset($result->keyword) ? count(explode(" ", $result->keyword)) : '0' }}
                                                </td>
                                                <td class="text-center">
                                                    {{isset($result->oldRank)?$result->oldRank:'N/A'}}
                                                </td>
                                                <td class="text-center">
                                                    {{isset($result->newRank)?$result->newRank:'N/A'}}
                                                </td>
                                                <td class="text-center">
                                                    {{isset($result->change)?$result->change:''}}
                                                </td>
                                                <td class="text-center">
                                                    {{isset($result->change)?$result->bestRank:''}}
                                                </td>
                                                <td class="text-center">
                                                    {{ isset($result->updated_at)?$result->updated_at:'' }}
                                                </td>
                                            </tr>
                                        @endif
                                    @endforeach
                                    </tbody>
                                </table>
                            @else
                                <div class="alert alert-warning cb">
                                    <strong>Sorry,</strong>
                                    there currently is no data found. You can refresh the page by <a
                                            href="{{ URL::route('backend.tools.ranktracker.casper.generator', $group->idGroup) }}"
                                            onclick="return canRefreshData();">clicking here</a>, or wait until our
                                    engine picks up your domain.
                                </div>
                            @endif
                        </div>
                    </div> <!-- /tile body -->
            </div>
        </div>

    </div>
    @stop

    @section('modals')
    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form method="post" action="{{ URL::route('backend.tools.ranktracker.events.insert') }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="target_id"
                           value="{{ isset($group->target->idTarget)?$group->target->idTarget:'' }}">

                    <div class="modal-header">
                        <h3 class="modal-title custom-font">
                            Add New Event
                        </h3>
                    </div>
                    <div class="modal-body">
                        <label for="dtp_eventDate">Event Date</label>
                        <div class='input-group date datepicker' id='dtp_eventDate' data-format="L">
                            <input type='text' class="form-control" name="date"/>
                            <span class="input-group-addon">
                                <span class="fa fa-calendar"></span>
                            </span>
                        </div>

                        <div class="form-group">
                            <label for="description" style="display: block;">Description of Event</label>
                            <textarea name="description" cols="60" rows="10" class="form-control"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="submit" class="btn btn-success btn-ef btn-ef-3 btn-ef-3c" value="Save">
                        <button class="btn btn-lightred btn-ef btn-ef-4 btn-ef-4c" data-dismiss="modal"><i
                                    class="fa fa-arrow-left"></i> Cancel
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
--}}
@stop

@section('modals')
    <div class="newsearch-modal">
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="row">
                        <!-- col -->
                        <div class="col-md-12">
                            <!-- Tab panes -->
                            <div class="card-box mb-0">
                                <h4 class="m-t-0 header-title"><b>Add New Event</b></h4>
                                <p class="text-muted m-b-20 font-13">
                                    The first step in keyword research.
                                </p>
                                
                                <div class="row">
                                    <form method="post" action="{{ URL::route('backend.tools.ranktracker.events.insert') }}">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="hidden" name="target_id" value="{{ isset($group->target->idTarget)?$group->target->idTarget:'' }}">

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label" for="dtp_eventDate">Event Date</label>
                                                <div class='input-group date datepicker' id='dtp_eventDate' data-format="L">
                                                    <input type='text' class="form-control" name="date"/>
                                                    <span class="input-group-addon">
                                                        <span class="fa fa-calendar"></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label" for="description">Description of Event</label>
                                                <textarea name="description" cols="60" rows="10" class="form-control"></textarea>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-12">
                                            <div class="form-group mb-0">
                                                <label class="control-label">&nbsp;</label>
                                                <input type="submit" class="btn btn-default waves-effect waves-light w-100 pull-right" value="Save">
                                                <button type="button" data-dismiss="modal" class="btn btn btn-cancel waves-effect waves-light w-100 pull-left">
                                                    Cancel
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('scripts')

    <!-- <script src="{{ asset('assets/js/chart.min.js') }}"></script> -->
    <!-- <script src="{{ asset('assets/js/charts/LineAlt.js') }}"></script> -->


    <script src="{{ asset('assets/backend/js/vendor/flot/jquery.flot.resize.min.js') }}"></script>
    <script>
        
        function downloadCSV(id) {
            var ajaxUrl = '{{ URL::route('backend.tools.ranktracker.view', 'projectId') }}'.replace('projectId', id) + '?downloadCSV=1';
            window.location.href = ajaxUrl;
        }

        $(function() {
            var casperResults = {!! $casperResults !!};
            if (casperResults.length) {
                drawLineChart();
            } else {
                $('#donut-chart-wrapper').hide();
                $('.chart-message').show();
            }
        });

        var keywordPositionData = {!! $keywordPositionChartData !!};

        var labels = $('#keywordPositionLabel span');
        for (var i = 0; i < keywordPositionData[1].length; i++) {
            $( labels[i] ).text(keywordPositionData[1][i]);
        }
        var donutChatrContainer = document.getElementById("donutChart");
        var lineChartContainer = document.getElementById("lineChart");
        Chart.defaults.global.legend.display = false;
        var lineChart,
            donutChatr = new Chart(donutChatrContainer, {
            type: 'doughnut',
            data: {
                labels: keywordPositionData[0],
                datasets: [
                    {
                        data: keywordPositionData[1],
                        backgroundColor: [
                            "#FF6384",
                            "#36A2EB",
                            "#FFCE56",
                            "#5fbeaa",
                            "#546f8a"
                        ],
                        hoverBackgroundColor: [
                            "#FF6384",
                            "#36A2EB",
                            "#FFCE56",
                            "#5fbeaa",
                            "#546f8a"
                        ]
                    }
                ]
            },
            options: {
                responsive: true,
                maintainAspectRatio: false
            }
        });

// =======================================================================
        $('.link-icon').on('click', function(event) {
            var id = parseInt( $(this).attr('data-chart-id') );
            $(this).toggleClass('active-link-icon');
            // $('#keyword_' + id).toggleClass('row-active');
            drawLineChart(id);
            event.stopPropagation();
        });

        function getIntKeys(obj) {
            var strKeysArr = Object.keys(obj),
                intKeysArr = [];
            for (var i = 0; i < strKeysArr.length; i++) {
                intKeysArr.push(parseInt(strKeysArr[i]));
            }
            return intKeysArr;
        };

        var chartLabels = {!! $chartJs['labels'] !!},
            datasets = {!! $chartJs['datasets'] !!},
            // datasets = JSON.parse('{"194":{"label":"one word","data":[2,3,4,1,2,5,3,2,5,4,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null]},"195":{"label":"two word","data":[5,6,7,8,7,6,8,6,5,8,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null]},"196":{"label":"three word","data":[9,7,8,5,6,7,8,4,6,7,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null]},"197":{"label":"four word","data":[2,4,7,5,2,5,8,5,6,3,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null]}}'),
            chartEvents = {!! $chartJs['events'] !!},
            keywordIdList = getIntKeys(datasets),
            lineChartDatasets = [],
            lineChartColors = ["#5fbeaa","#5d9cec","#34d3eb","#fb6d9d","#ffbd4a","#81c868","#f05050","#546f8a","#4c5667","#d7dadc"],
            initLineObj = {
                label: '',
                data: [],
                borderColor: "",
                pointHoverBackgroundColor: "",
                pointBorderColor: "",
                pointHoverBorderColor: "#546f8a",
                fill: false,
                lineTension: 0.1,
                borderCapStyle: 'butt',
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: 'miter',
                pointBorderWidth: 1,
                pointHoverRadius: 5,
                pointHoverBorderWidth: 2,
                pointRadius: 1,
                pointHitRadius: 10,
                spanGaps: false
            };

        function toggleCharts(argument) {
            keywordIdList = keywordIdList.length ? [] : getIntKeys(datasets);
            if (keywordIdList.length) {
                $('.link-icon').addClass('active-link-icon');
            } else {
                $('.link-icon').removeClass('active-link-icon');
            }
            drawLineChart();
        }

        function drawLineChart(id, drop) {
            keywordIdList = drop === true ? [] : keywordIdList;
            if (id) {
                var index = keywordIdList.indexOf(id)
                if (index !== -1) {
                    keywordIdList.splice(index, 1);
                } else {
                    keywordIdList.push(id);
                }
            }
            lineChartDatasets = [];
            for (var i = 0; i < keywordIdList.length; i++) {
                lineChartDatasets.push( Object.assign( {}, initLineObj, {
                    label: datasets[keywordIdList[i]].label,
                    data: datasets[keywordIdList[i]].data,
                    borderColor:lineChartColors[i],
                    pointBorderColor:lineChartColors[i],
                    pointHoverBackgroundColor:lineChartColors[i]
                }));
            }
            if ( Object.keys(chartEvents).length ) {
                for (var key in chartEvents) {
                    lineChartDatasets.push( Object.assign( {}, initLineObj, {
                        label: chartEvents[key].label,
                        data: chartEvents[key].data,
                        borderColor:"#7ba754",
                        pointBorderColor:"#7ba754",
                        pointHoverBackgroundColor:"#7ba754"
                    }));
                }
            }
            redrawLineChart();
        }

        function redrawLineChart() {
            if (lineChart) {
                lineChart.destroy();
            }
            lineChart = new Chart(lineChartContainer, {
                type: 'line',
                data: {
                    labels: chartLabels,
                    datasets: lineChartDatasets
                },
                options: {
                    responsive: true,
                    maintainAspectRatio: false,
                    scales : {
                        yAxes : [ {
                            ticks: {
                                reverse: true
                            }
                        } ]
                    }
                }
            });
        }

        $('#datePicker').daterangepicker(
            {
                format: 'MM/DD/YYYY',
                startDate: {{ isset($dates['start'])?$dates['start']:' ' }},
                endDate: {{ isset($dates['stop'])?$dates['stop']:' ' }},
                dateLimit: {days: 60},
                showDropdowns: true,
                showWeekNumbers: true,
                timePicker: false,
                timePickerIncrement: 1,
                timePicker12Hour: true,
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },
                opens: 'left',
                drops: 'down',
                buttonClasses: ['btn', 'btn-sm'],
                applyClass: 'btn-success',
                cancelClass: 'btn-default',
                separator: ' to ',
                locale: {
                    applyLabel: 'Submit',
                    cancelLabel: 'Cancel',
                    fromLabel: 'From',
                    toLabel: 'To',
                    customRangeLabel: 'Custom',
                    daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                    monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                    firstDay: 1
                }
            }, function (start, end, label) {
                $('#dateSpan').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                $("#start_date").val(start.format('YYYY-MM-DD'));
                $("#stop_date").val(end.format('YYYY-MM-DD'));
                $("#dateForm").submit();
            }
        );
    

// ================================================================================
// ================================================================================
        //     var ctx = $("#chart").get(0).getContext("2d");

        //     $('#datePicker').daterangepicker(
        //             {
        //                 format: 'MM/DD/YYYY',
        //                 startDate: {{ isset($dates['start'])?$dates['start']:' ' }},
        //                 endDate: {{ isset($dates['stop'])?$dates['stop']:' ' }},
        //                 dateLimit: {days: 60},
        //                 showDropdowns: true,
        //                 showWeekNumbers: true,
        //                 timePicker: false,
        //                 timePickerIncrement: 1,
        //                 timePicker12Hour: true,
        //                 ranges: {
        //                     'Today': [moment(), moment()],
        //                     'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        //                     'Last 7 Days': [moment().subtract(6, 'days'), moment()],
        //                     'Last 30 Days': [moment().subtract(29, 'days'), moment()],
        //                     'This Month': [moment().startOf('month'), moment().endOf('month')],
        //                     'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        //                 },
        //                 opens: 'left',
        //                 drops: 'down',
        //                 buttonClasses: ['btn', 'btn-sm'],
        //                 applyClass: 'btn-success',
        //                 cancelClass: 'btn-default',
        //                 separator: ' to ',
        //                 locale: {
        //                     applyLabel: 'Submit',
        //                     cancelLabel: 'Cancel',
        //                     fromLabel: 'From',
        //                     toLabel: 'To',
        //                     customRangeLabel: 'Custom',
        //                     daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
        //                     monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
        //                     firstDay: 1
        //                 }
        //             }, function (start, end, label) {
        //                 $('#dateSpan').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        //                 $("#start_date").val(start.format('YYYY-MM-DD'));
        //                 $("#stop_date").val(end.format('YYYY-MM-DD'));
        //                 $("#dateForm").submit();
        //             }
        //     );
        // });


        // window.onload = function () {
        //     var ctx = $("#chart").get(0).getContext("2d");
        //     var chartjs = new Chart(ctx).LineAlt(data, {
        //         responsive: true,
        //         maintainAspectRatio: true,
        //         showScale: true,
        //         ///Boolean - Whether grid lines are shown across the chart
        //         scaleShowGridLines: true,
        //         //String - Colour of the grid lines
        //         scaleGridLineColor: "rgba(0,0,0,0.05)",
        //         //Number - Width of the grid lines
        //         scaleGridLineWidth: 3,
        //         //Boolean - Whether to show horizontal lines (except X axis)
        //         scaleShowHorizontalLines: false,
        //         //Boolean - Whether to show vertical lines (except Y axis)
        //         scaleShowVerticalLines: true,
        //         //Boolean - Whether the line is curved between points
        //         bezierCurve: false,
        //         //Number - Tension of the bezier curve between points
        //         bezierCurveTension: 1,
        //         //Boolean - Whether to show a dot for each point
        //         pointDot: true,
        //         //Number - Radius of each point dot in pixels
        //         pointDotRadius: 6,
        //         //Number - Pixel width of point dot stroke
        //         pointDotStrokeWidth: 4,
        //         //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
        //         pointHitDetectionRadius: 6,
        //         //Boolean - Whether to show a stroke for datasets
        //         datasetStroke: true,
        //         //Number - Pixel width of dataset stroke
        //         datasetStrokeWidth: 2,
        //         //Boolean - Whether to fill the dataset with a colour
        //         datasetFill: false,
        //         //Set y axis
        //         scaleOverride: true,
        //         scaleSteps: 11,
        //         scaleStepWidth: -10,
        //         scaleStartValue: 100,
        //         // String - Template string for single tooltips
        //         multiTooltipTemplate: function (chartData) {
        //             //console.log(chartData);
        //             return chartData.datasetLabel + " : " + chartData.value;
        //         }
        //     });
        // };

        // $(function () {
        //     $('#container').highcharts({
        //         chart: {
        //             plotBackgroundColor: null,
        //             plotBorderWidth: 0,
        //             plotShadow: false
        //         },
        //         title: {
        //             text: '',
        //             align: 'center',
        //             verticalAlign: 'middle',
        //             y: 40
        //         },
        //         colors: ['#00dfdf', '#ccffff', '#3399ff', '#9999ff', '#8cb3d9'],
        //         tooltip: {
        //             pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        //         },
        //         credits: {
        //             enabled: false
        //         },
        //         plotOptions: {
        //             pie: {
        //                 dataLabels: {
        //                     enabled: true,
        //                     distance: -50,
        //                     style: {
        //                         fontWeight: 'bold',
        //                         color: 'white',
        //                         textShadow: '0px 1px 2px black'
        //                     }
        //                 },
        //                 startAngle: 0,
        //                 endAngle: 360,

        //             }
        //         },
        //         series: [{
        //             type: 'pie',
        //             name: 'Keyword Position',
        //             innerSize: '50%',
        //             data: [
        //                 {!!$keywordPositionChartData!!},
        //                 {
        //                     name: 'Proprietary or Undetectable',
        //                     y: 0.2,
        //                     dataLabels: {
        //                         enabled: true
        //                     }
        //                 }
        //             ]
        //         }]
        //     });
        // });


        function canRefreshData() {
            var answer = confirm('Are you sure you wish to refresh this data?');

            if (answer) {
                $("#cpc_results").block({
                    message: '<h4 class="text-success"><img src="{{ asset('assets/backend/images/success-loader.gif') }}"> &nbsp; Please Wait, Refreshing Data ...</h4>',
                    css: {
                        border: '3px solid rgb(63, 78, 98)'
                    }
                });
            }

            return answer;
        }
    </script>
@stop