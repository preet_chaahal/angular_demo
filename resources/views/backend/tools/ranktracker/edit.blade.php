@extends('layouts.backend.master')

@section('style')
    <link rel="stylesheet" href="{{ asset('assets/backend/css/vendor/selectize.default.css') }}">

    <style>
        .selectItem >span { color: #000; }
    </style>
@stop


@section('content')
    <div class="row mb-20">
        <div class="col-sm-12">
            <h4 class="page-title">Rank Tracker</h4>
            <!-- <ol class="breadcrumb">
                <li><a href="{{ URL::route('dashboard') }}">Dashboard</a></li>
                <li><a href="{{ URL::route('backend.tools.ranktracker') }}">Rank Tracker</a></li>
                <li class="active">Edit Tracking Project</li>
            </ol> -->
        </div>
    </div>

    @include('layouts.backend.partials.messages')
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                <div class="row">
                    <div class="col-lg-6">
                        <h4 class="m-t-0 header-title"><b>Edit Tracking Project</b></h4>
                        <p class="text-muted m-b-30 font-13">
                            Edit domain and keywords in your Tracking Project.
                        </p>

                        <form role="form" method="POST" action="{{ URL::route('backend.tools.ranktracker.update') }}">
                            <input type="hidden" name="id" value="{{ $group->idGroup }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <!-- name -->
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" id="name" name="name" value="{{ old('name', $group->name) }}" required>
                            </div> <!-- name -->

                            <!-- keywords -->
                            <div class="form-group">
                                <label for="keywords">Keywords</label>
                                <textarea name="keywords" id="keywords" rows="10" class="form-control" required>{{ old('keywords', $keywordString) }}</textarea>
                                <p class="help-block">One keyword per line</p>
                            </div> <!-- keywords -->

                            <!-- domain -->
                            <div class="form-group">
                                <label for="domain">Domain</label>
                                <input type="text" class="form-control" id="domain" name="domain" value="{{ $group->target->name }}" disabled>
                                <p class="help-block">*.domain.com or www.domain.com</p>
                            </div>
                            <!-- domain -->

                            <!-- module -->
                            <div class="form-group">
                                <label for="module">Module</label>
                                <select name="module" id="module" class="form-control" required>
                                    <option value="google">Google</option>
                                </select>
                            </div> <!-- module -->

                            <!-- tld -->
                            <div class="form-group">
                                <label for="tld">tld</label>
                                <select class="selectize tld-selectize" name="tld"></select>
                                <p class="help-block">The google search engine top level domain: google.com, google.co.uk</p>
                            </div> <!-- tld -->

                            <div class="form-group">
                                <label class="control-label" for="lang">Search Language</label>
                                <select class="selectize lang-selectize" id="lang" name="lang"></select>
                            </div>
                            
                            <button type="submit" class="btn btn-default waves-effect waves-light btn-md pull-right">
                                Update Group
                            </button>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop


@section('scripts')
    <script src="{{ asset('assets/backend/js/vendor/selectize/selectize.js') }}"></script>

    <script>
        var tldOptions = {!! $tldOptions !!};
        var languages = {!! $languages !!};

        $('.tld-selectize').selectize({
            plugins: ['remove_button'],
            valueField: 'name',
            labelField: 'engine',
            searchField: ['name', 'engine'],
            delimiter: ',',
            persist: false,
            hideSelected: true,
            openOnFocus: true,
            items: ['{{ $selectedEngine }}'],
            render: {
                item: function(item, escape) {
                    return '<div>' +
                        (item.engine ? '<span class="email">' + escape(item.engine) + '</span>' : '') +
                        (item.name ? '<span class="name"> ‒ ' + escape(item.name) + '</span>' : '') +
                    '</div>';
                },
                option: function(item, escape) {
                    var label = item.name || item.engine;
                    var caption = item.name ? item.engine : null;
                    return '<div class="selectItem">' +
                        (caption ? '<span>' + escape(caption) + '</span>' : '') +
                        '<span> ‒ ' + escape(label) + '</span>' +
                    '</div>';
                }
            },
            options: tldOptions
        });
        
        $('.lang-selectize').selectize({
            plugins: ['remove_button'],
            valueField: 'code',
            labelField: 'name',
            searchField: ['name', 'code'],
            delimiter: ',',
            persist: false,
            hideSelected: true,
            openOnFocus: true,
            items: ['{{ $selectedLanguage }}'],
            render: {
                item: function (item, escape) {
                    return '<div>' +
                            '<span>' + item.name + ' (' + item.code + ')</span>' +
                            '</div>';
                },
                option: function (item, escape) {
                    return '<div>' +
                            '<span>' + item.name + ' (' + item.code + ')</span>' +
                            '</div>';
                }
            },
            options: languages
        });
    </script>
@stop