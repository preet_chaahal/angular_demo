@extends('layouts.backend.master')

@section('style')
    <link rel="stylesheet" href="{{ asset('assets/backend/js/vendor/jquery-circliful/css/jquery.circliful.css') }}">
@stop


@section('content')

    <div class="row mb-20">
        <div class="col-sm-12">
            <h4 class="page-title">Rank Tracker</h4>
            <!-- <ol class="breadcrumb">
                <li><a href="{{ URL::route('dashboard') }}">Dashboard</a></li>
                <li class="active">Rank Tracker</li>
            </ol> -->
        </div>
    </div>

    @include('layouts.backend.partials.messages')

    <div class="row">
        <div class="col-lg-4">
            <div id="circliful-chart" class="card-box pr-0 pl-0 rt-widget-height-sm">
                <h4 class="text-dark header-title m-t-0 m-b-30 pl-20">Visibility Score</h4>
            </div>
        </div>

        <div class="col-lg-4">
            <div class="card-box pb-0">
                <h4 class="text-dark header-title m-t-0 m-b-30">Average Ranking</h4>
                <div id="vr-chart" class="chart-message cb sm">There is no data to display.<br>We are collecting data<br>for this project.</div>
                <div id="line-chart-wrapper" class="widget-chart text-center">
                    <canvas id="lineChart" height="250"></canvas>
                </div>
            </div>
        </div>

        <div class="col-lg-4">
            <div class="card-box"> <!-- rt-widget-height-xs -->
                <h4 class="text-dark header-title m-t-0">Keyword Position</h4>
                <div id="kp-chart" class="chart-message cb sm">There is no data to display.<br>We are collecting data<br>for this project.</div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="widget-chart text-center">
                            <!-- <div id="sparkline3"></div> -->
                            <canvas id="myChart" height="190"></canvas>
                        </div>
                    </div>
                </div>
                <div id="keywordPositionLabel" class="donut-chart-list">
                    <div class="row">
                        <div class="col-xs-4 pr-0">
                            Top 3: <span>0</span>
                        </div>
                        <div class="col-xs-4 p-0">
                            Top 10: <span>0</span>
                        </div>
                        <div class="col-xs-4 p-0">
                            Top 20: <span>0</span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-4 pr-0">
                            Top 30: <span>0</span>
                        </div>
                        <div class="col-xs-4 p-0">
                            Top 100: <span>0</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- row -->
    <div class="row">
        <div class="col-md-12">
            <div class="card-box">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="pull-left">
                            <h4 class="m-t-0 header-title"><b>Saved Domains</b></h4>
                            <p class="text-muted m-b-20 font-13">
                                Previous saved and evaluated domains.
                            </p>
                        </div>
                        <div id="actions" class="input-group pull-right">
                            <input type="submit" name="Submit" class="btn btn-default btn-block projectsubmit" value="Add New Tracking Project">
                        </div>
                        @if($domains->count() > 0)
                            <?php $domainId = $domains[0]->group->idGroup ?>
                            <div class="table-container">
                                <table class="table">
                                    <thead style="white-space: nowrap">
                                        <tr>
                                            <th>
                                                Project
                                            </th>
                                            <th>
                                                Domain
                                            </th>
                                            <th>
                                                Keywords
                                            </th>
                                            <th>
                                                Last Checked
                                            </th>
                                            <!-- <th class="text-right">
                                                Best Pos
                                            </th> -->
                                            <th class="text-right">
                                                Actions
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody class="selectable">
                                        @foreach($domains as $domain)
                                            @if($domain->group)
                                            <tr id="ranktracker_{{ $domain->group->idGroup }}" data-id="{{ $domain->group->idGroup }}" class="{{ ($domain->group->keywords->count() > 0) ? ('parent-data') : ('') }}" onclick="getDomainKeywordsData({{ $domain->group->idGroup}}, '{{$domain->group->name}}')" style="white-space: nowrap">
                                                <td>
                                                    {{--<a href="{{ URL::route('backend.tools.ranktracker.view', $domain->group->idGroup) }}">
                                                        {{ $domain->group->name }}
                                                    </a>--}}

                                                    {{ $domain->group->name }}
                                                    <input type="hidden" name="forSingleDelete" id="forSingleDelete{{$domain->group->idGroup}}"
                                                           value="{{$domain->group->idGroup}}">
                                                </td>
                                                <td>
                                                    {{ isset($domain->group->target->name)?$domain->group->target->name:'' }}
                                                </td>
                                                <td>
                                                    <span class="label label-default">{{ $domain->group->keywords->count() }} Keywords</span>
                                                </td>
                                                <td>
                                                    @if($domain->group->lastRan->count() <= 0) Never
                                                    @else
                                                        {{ $domain->group->lastRan->last()->date->format('m/d/Y') }}
                                                    @endif
                                                </td>
                                                <!-- <td class="text-right">
                                                    30
                                                </td> -->
                                                <td class="text-right actions">
                                                    <!-- <ul class="controls">
                                                        <li class="dropdown">
                                                            <a role="button" tabindex="0" class="dropdown-toggle settings">
                                                                <i class="fa fa-gears f-s-17"></i>
                                                            </a>
                                                            <ul class="dropdown-menu pull-right with-arrow animated littleFadeInUp">
                                                                <li> -->
                                                                    <a href="{{ URL::route('backend.tools.ranktracker.view', $domain->group->idGroup) }}">
                                                                        <i class="action-icon icon-arrow-right-circle"></i>
                                                                    </a>
                                                                <!-- </li>
                                                                <li> -->
                                                                    <a href="javascript:void(0);" onclick="onDelete({{ $domain->group->idGroup }})" class="single-delete">
                                                                        <i class="action-icon icon-trash"></i>
                                                                    </a>
                                                                <!-- </li>
                                                            </ul>
                                                        </li>
                                                    </ul> -->
                                                </td>
                                                {{--<td class="text-right pr-18">
                                                    <a href="javascript:void(0);" onclick="onDelete({{ $domain->group->idGroup}})" class="single-delete"><i class="fa fa-gears f-s-17"></i></a>
                                                </td>--}}
                                            </tr>

                                                @if( $domain->group->keywords->count() > 0 )
                                                <tr id="child-data-{{ $domain->group->idGroup }}" class="child-data">
                                                    <td colspan="5" class="child-data-td">
                                                        <div class="sub-table">
                                                            <table class="table unselectable">
                                                                <thead>
                                                                    <tr role="row">
                                                                        <th>
                                                                            Date Added
                                                                        </th><th>
                                                                            Keyword
                                                                        </th><th class="text-right">
                                                                            Monthly Searches
                                                                        </th><th class="text-right">
                                                                            CPC
                                                                        </th><th class="text-right">
                                                                            Number Of Words
                                                                        </th><th class="text-right">
                                                                            Current
                                                                        </th><th class="text-right">
                                                                            Best
                                                                        </th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody id="saveTbl">
                                                                    @foreach($domain->group->keywords as $keyword)
                                                                    <tr id="keyword-{{$keyword->idKeyword}}">
                                                                        <td colspan="7" class="kw-not-available">Sorry, there currently is no data found for <strong>{{$keyword->name}}</strong> keyword. Please wait until our engine picks up your domain.</td>
                                                                        <td class="kw-available keyword-date pl-15"></td>
                                                                        <td class="kw-available keyword-name"></td>
                                                                        <td class="kw-available keyword-ams text-right"></td>
                                                                        <td class="kw-available keyword-bid text-right"></td>
                                                                        <td class="kw-available keyword-number text-right"></td>
                                                                        <td class="kw-available keyword-current text-right"></td>
                                                                        <td class="kw-available keyword-best text-right"></td>
                                                                    </tr>
                                                                    @endforeach
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </td>
                                                </tr>
                                                @endif
                                            @endif
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        @else
                            <?php $domainId = 0 ?>
                            <div class="alert alert-warning cb">
                                <strong>Oh No!</strong>
                                You do not have any researching happening
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

        <!-- row -->
    {{--<div class="row">
        <div class="col-md-12">
            <div class="card-box">
                <h4 class="m-t-0 header-title"><b>Project Name</b></h4>
                <p class="text-muted m-b-20 font-13">
                    The first step in domain research.
                </p>
                
                <div class="row">
                    <form method="POST" action="" autocomplete="off" id="projectform">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <div class="col-md-9">
                            <div class="form-group">
                                <label class="control-label" for="projectname">Enter the name of your project.</label>
                                <input type="text" class="form-control" id="projectname" name="projectname" required="required">
                            </div>
                        </div>
                        
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="control-label">&nbsp;</label>
                                <div id="actions" class="input-group">
                                    <input type="submit" name="Submit" class="btn btn-default btn-block projectsubmit" value="Add New Tracking Project">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>--}}

    <div id="mainContainer"></div>
@stop


@section('scripts')
    <script src="{{ asset('assets/backend/js/vendor/jquery-circliful/js/jquery.circliful.min.js') }}"></script>

    <script type="text/javascript">
// ===========================================================================

        var domains = {!! json_encode($domains->toArray()) !!},
            proxyStatus = {!! json_encode($proxyStatus) !!},
            lineChartContainer = document.getElementById("lineChart"),
            idGroupList = [],
            dataObject = {},
            lineChart,
            myChart;

        Chart.defaults.global.legend.display = false;

        if (!proxyStatus.isAvailable) {
            $("#errMsg").html("");
            $("#errMsg").append(proxyStatus.errMessage);
            $("#error").show();
        }

        function getDomainKeywordsData(id, project) {
            var ajaxUrl = '{{ URL::route('backend.tools.ranktracker.view', 'idGroup') }}'
                    .replace('idGroup', id);

            if ( idGroupList.indexOf(id) < 0 ) {
                $.ajax({
                    url: ajaxUrl,
                    type: 'get',
                    success: function (res) {
                        idGroupList.push(id);
                        dataObject[id] = res;
                        setData(id, project);
                    },
                    // error:function(result){
                    // }
                });
            }
            else {
                setData(id, project);
            }
        }

        function setData(id, project) {
            var data = dataObject[id].casperResults,
                childData = $('#child-data-' + id);

            if (data.length) {
                for (var i = 0; i < data.length; i++) {
                    var childDataTr = childData.find('#keyword-' + data[i].idKeyword);
                    var date = data[i].created_at;
                    var dateStr = date.substring(5,7) + '/' + date.substring(8,10) + '/' + date.substring(0,4);
                    childDataTr.find('.kw-not-available').hide();
                    childDataTr.find('.kw-available').show();
                    childDataTr.find('.keyword-date').text(dateStr);
                    childDataTr.find('.keyword-name').text(data[i].keyword);
                    childDataTr.find('.keyword-ams').text( parseInt(data[i].avg_month_searches).formatNumber(0, '.', ',') );
                    childDataTr.find('.keyword-bid').text(data[i].suggested_bid);
                    childDataTr.find('.keyword-number').text(data[i].keyword.split(' ').length);
                    childDataTr.find('.keyword-current').text(data[i].newRank);
                    childDataTr.find('.keyword-best').text(data[i].bestRank);
                }
            }
            else {
                domains.forEach(function(proj, i, arr) {
                    if (proj.group_id == id) {
                        var kws = proj.group.keywords;
                        for (var i = 0; i < kws.length; i++) {
                            $('#keyword-' + kws[i].idKeyword + ' .kw-available').hide();
                        }
                    }
                });
            }

            $('#visibilityScore').remove();
            $('#circliful-chart').append('<div id="visibilityScore" class="circliful-chart" data-dimension="280" data-text="' + dataObject[id].visibilityPercentage + '%" data-info="Visibility Score: ' + dataObject[id].visibilityScore + '" data-width="30" data-fontsize="24" data-percent="' + dataObject[id].visibilityPercentage + '" data-fgcolor="#34d3eb" data-bgcolor="#ebeff2" data-type="half" data-fill="#f4f8fb"></div>');
            $('#visibilityScore').circliful();

            redrawDoughnutChart(id, project);
            redrawLineChart(id, project);
        }

        function redrawDoughnutChart(id, project) {
            var keywordPositionData =  JSON.parse( dataObject[id].keywordPositionChartData ),
                labels = $('#keywordPositionLabel span'),
                ctx = document.getElementById("myChart"),
                isDataAvailable = false;

            for (var i = 0; i < keywordPositionData[1].length; i++) {
                if ( keywordPositionData[1][i] > 0 ) {
                    isDataAvailable = true;
                }
                $( labels[i] ).text(keywordPositionData[1][i]);
            }

            if (myChart) {
                myChart.destroy();
            }

            if (!isDataAvailable) {
                $('#kp-chart').show();
                return;
            }
            $('#kp-chart').hide();

            myChart = new Chart(ctx, {
                type: 'doughnut',
                data: {
                    labels: keywordPositionData[0],
                    datasets: [
                        {
                            data: keywordPositionData[1],
                            backgroundColor: [
                                "#FF6384",
                                "#36A2EB",
                                "#FFCE56",
                                "#5fbeaa",
                                "#546f8a"
                            ],
                            hoverBackgroundColor: [
                                "#FF6384",
                                "#36A2EB",
                                "#FFCE56",
                                "#5fbeaa",
                                "#546f8a"
                            ]
                        }
                    ]
                },
                options: {
                    responsive: true,
                    maintainAspectRatio: false
                }
            });
        }

        function redrawLineChart(id, project) {
            var lineChartData = JSON.parse( dataObject[id].chartJs.datasets ),
                lineChartLabelsData = JSON.parse( dataObject[id].chartJs.labels ),
                keys = Object.keys(lineChartData),
                filteredLineChartData = {},
                lineChartDatasets = [],
                lineChartLabels = [];

            keys.forEach(function(key, j, arr) {
                for (var i = 0; i < lineChartData[key].data.length; i++) {
                    if ( lineChartData[key].data[i] !== null ) {
                        filteredLineChartData[key] = lineChartData[key].data;
                        break;
                    }
                }
            });

            keys = Object.keys(filteredLineChartData);

            if (keys.length === 1) {
                lineChartDatasets = lineChartData[ keys[0] ].data;
            } else if (keys.length > 1) {
                for (var i = 0; i < lineChartLabelsData.length; i++) {
                    var tempResult = null;
                    keys.forEach(function(key, j, arr) {
                        if ( lineChartData[key].data[i] !== null ) {
                            tempResult = tempResult === null ? 0 : tempResult;
                            tempResult += parseInt(lineChartData[key].data[i]);
                        }
                    });
                    if (tempResult !== null) {
                        tempResult = Math.round(tempResult / keys.length * 100) / 100;
                    }
                    lineChartDatasets.push(tempResult);
                }
            }

            lineChartDatasets = lineChartDatasets.filter(function(number) {
                return number !== null;
            });

            if (lineChart) {
                lineChart.destroy();
            }

            if (lineChartDatasets.length === 0) {
                $('#vr-chart').show();
                return;
            }
            $('#vr-chart').hide();

            for (var i = 0; i < lineChartDatasets.length; i++) {
                lineChartLabels.push("");
            }

            lineChart = new Chart(lineChartContainer, {
                type: 'line',
                data: {
                    labels: lineChartLabels,
                    datasets: [
                        {
                            label: project,
                            data: lineChartDatasets,
                            borderColor: "#5fbeaa",
                            pointBorderColor: "#5fbeaa",
                            pointHoverBackgroundColor: "#5fbeaa",
                            pointHoverBorderColor: "#546f8a",
                            fill: false,
                            lineTension: 0.1,
                            borderCapStyle: 'butt',
                            borderDash: [],
                            borderDashOffset: 0.0,
                            borderJoinStyle: 'miter',
                            pointBorderWidth: 1,
                            pointHoverRadius: 5,
                            pointHoverBorderWidth: 2,
                            pointRadius: 1,
                            pointHitRadius: 10,
                            spanGaps: false
                        }
                    ]
                },
                options: {
                    responsive: true,
                    maintainAspectRatio: false,
                    scales : {
                        xAxes : [ {
                            gridLines : {
                                display : false
                            }
                        } ],
                        yAxes : [ {
                            gridLines : {
                                display : false
                            },
                            ticks: {
                                reverse: true
                            }
                        } ]
                    }
                }
            });
        }

        $('.parent-data').on("click", function () {
            var isExpanded = $(this).hasClass('parent-expand');
            var id = $(this).attr('data-id');
            $(this).removeClass('parent-expand');
            $('#child-data-' + id).removeClass('expand');
            $('.child-data .sub-table').removeClass('sub-table-open');

            if (!isExpanded) {
                $('.parent-data').removeClass('parent-expand');
                $('.child-data').removeClass('expand');
                $(this).addClass('parent-expand');
                $('#child-data-' + id).addClass('expand');
                $('#child-data-' + id + ' .sub-table').addClass('sub-table-open');
            }
        });

        $('.actions a').on('click', function(event){
            event.stopPropagation();
        });

        var domainId = '{{$domainId}}';
        if (domainId !== 0) {
            $('#ranktracker_' + domainId).trigger('click');
        }

// ===========================================================================

        $('#actions').on('click', function(){
            window.location = '/tools/ranktracker/add?projectname= ';
        });

        $("#projectform").on('submit', function(ev) {
            var frm = $('#projectform');
            ev.preventDefault();

            var projectname = $('#projectname').val();

            var datastring = '?projectname=' + projectname;

            window.location = '/tools/ranktracker/add' + datastring;
        });
    </script>

    <script type="text/javascript">

        function onDelete(val) {
            singleDeleteKeyword(val);
        }

        function singleDeleteKeyword(val) {
            bootbox.dialog({
                message: "Are you sure you want to perform delete?",
                title: "Delete confirmation!",
                buttons: {
                    success: {
                        label: "Yes",
                        className: "btn btn-danger waves-effect waves-light w-100",
                        callback: function () {
                            //Example.show("great success");
                            $.ajax({
                                url: '{{ URL::route('backend.tools.ranktracker.delete') }}',
                                data: {'id': val},
                                type: 'get',
                                dataType: 'json',
                                success: function (result) {
                                    bootbox.hideAll();
                                    $("#ranktracker_" + val).remove();
                                    $("#child-data-" + val).remove();
                                    // $('#saveTbl').DataTable().ajax.reload();
                                }
                            })
                            return true;
                        }
                    },
                    danger: {
                        label: "No",
                        className: "btn btn-default waves-effect waves-light w-100",
                        callback: function () {
                            //Example.show("uh oh, look out!");
                        }
                    },
                }
            });
        }
    </script>

@stop