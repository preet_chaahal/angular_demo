@extends('layouts.backend.master')

@section('style')
    <link rel="stylesheet" href="{{ asset('assets/backend/css/vendor/selectize.default.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/loaders.css') }}">

    <style>
        .selectItem >span { color: #000; }
    </style>
@stop


@section('content')
    <div class="row mb-20">
        <div class="col-sm-12">
            <h4 class="page-title">Rank Tracker</h4>
            <!-- <ol class="breadcrumb">
                <li><a href="{{ URL::route('dashboard') }}">Dashboard</a></li>
                <li><a href="{{ URL::route('backend.tools.ranktracker') }}">Rank Tracker</a></li>
                <li class="active">New Domain</li>
            </ol> -->
        </div>
    </div>

    <!-- <div class="page page-dashboard">
        <div class="pageheader">
            <h2>Rank Tracker <span>// Save time and money by automating the process of checking your keyword rankings</span></h2>
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <a href="{{ URL::route('dashboard') }}"><i class="fa fa-home"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href="{{ URL::route('backend.tools.ranktracker') }}">Rank Tracker</a>
                    </li>
                </ul>
            </div>
        </div> -->

        @include('layouts.backend.partials.messages')
        <div class="row">
            <div class="col-sm-12">
                <div class="card-box">
                    <div class="row">
                        <div class="col-lg-6">
                            <h4 class="m-t-0 header-title"><b>New Tracking Project</b></h4>
                            <p class="text-muted m-b-30 font-13">
                                Add domain and keywords to your new Tracking Project.
                            </p>
                        
                            <form role="form" method="POST" action="{{ URL::route('backend.tools.ranktracker.insert') }}">
                                <!-- name -->
                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <input type="text" class="form-control" id="name" name="name" value="<?php echo $_GET['projectname'] ?>" required>
                                </div> <!-- name -->

                                <!-- keywords -->
                                <div class="form-group">
                                    <label for="keywords">Keywords</label>
                                    <textarea name="keywords" id="keywords" rows="10" class="form-control" required></textarea>
                                    <p class="help-block">One keyword per line</p>
                                </div> <!-- keywords -->

                                <!-- domain -->
                                <div class="form-group">
                                    <label for="domain">Domain</label>
                                    <input type="text" class="form-control" id="domain" name="domain"  required>
                                    <p class="help-block">*.domain.com or www.domain.com</p>
                                </div>
                                <!-- domain -->

                                <!-- module -->
                                <div class="form-group">
                                    <label for="module">Module</label>
                                    <select name="module" id="module" class="form-control" required>
                                        <option value="google">Google</option>
                                    </select>
                                </div> <!-- module -->

                                <!-- tld -->
                                <div class="form-group">
                                    <label for="tld">tld</label>
                                    <select class="selectize tld-selectize" name="tld"></select>
                                    <p class="help-block">The google search engine top level domain: google.com, google.co.uk</p>
                                </div> <!-- tld -->

                                <div class="form-group">
                                    <label class="control-label" for="lang">Search Language</label>
                                    <select class="selectize lang-selectize" id="lang" name="lang"></select>
                                </div>

                                <button type="submit" class="btn btn-default waves-effect waves-light btn-md pull-right">
                                    Add New Search
                                </button>

                                <!-- <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-8 col-md-offset-2">
                                            <input type="submit" class="btn btn-success btn-lg btn-block" value="Add New Search">
                                        </div>
                                    </div>
                                </div> --> <!-- actions -->
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{--<!-- row -->
        <div class="row">
            <!-- col -->
            <div class="col-md-12">
                <!-- tile -->
                <section class="tile tile-simple">
                    <!-- tile body -->
                    <div class="tile-body p-0">
                        <div role="tabpanel">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs tabs-dark" role="tablist">
                                <li><a href="{{ URL::route('backend.tools.ranktracker') }}">Listing</a></li>
                                <li class="active"><a href="{{ URL::route('backend.tools.ranktracker.add') }}">New Domain</a></li>
                                <li class="tabs-title pull-right" style="padding-right: 15px;">Saved <strong>Domains</strong></li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tile-body">

                                <form method="POST" action="{{ URL::route('backend.tools.ranktracker.insert') }}">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                    <!-- name -->
                                    <div class="form-group">
                                        <label for="name">Name</label>
                                        <input type="text" class="form-control" id="name" name="name" value="">
                                    </div> <!-- name -->

                                    <!-- keywords -->
                                    <div class="form-group">
                                        <label for="keywords">Keywords</label>
                                        <textarea name="keywords" id="keywords" rows="10" class="form-control"></textarea>
                                        <p class="help-block">One keyword per line</p>
                                    </div> <!-- keywords -->

                                    <!-- domain -->
                                    <div class="form-group">
                                        <label for="domain">Domain</label>
                                        <input type="text" class="form-control" id="domain" name="domain">
                                        <p class="help-block">*.domain.com or www.domain.com</p>
                                    </div>
                                    <!-- domain -->

                                    <!-- module -->
                                    <div class="form-group">
                                        <label for="module">Module</label>
                                        <select name="module" id="module" class="form-control">
                                            <option value="google">Google</option>
                                        </select>
                                    </div> <!-- module -->

                                    <!-- tld -->
                                    <div class="form-group">
                                        <label for="tld">tld</label>
                                        <select id="selectize" name="tld"></select>
                                        <p class="help-block">The google search engine top level domain: google.com, google.co.uk</p>
                                    </div> <!-- tld -->
                                    --}}
                                    {{--<div style="display:none;">--}}
                                        {{--<!-- datacenter -->--}}
                                        {{--<div class="form-group">--}}
                                            {{--<label for="datacenter">Data Center</label>--}}
                                            {{--<input type="text" class="form-control" id="datacenter" name="datacenter">--}}
                                            {{--<p class="help-block">A specific datacenter. Leave empty to use standard google.tld</p>--}}
                                        {{--</div> <!-- datacenter -->--}}

                                        {{--<!-- parameters -->--}}
                                        {{--<div class="form-group">--}}
                                            {{--<label for="parameters">Parameters</label>--}}
                                            {{--<input type="text" class="form-control" id="parameters" name="parameters">--}}
                                            {{--<p class="help-block">Additional parameters in the request (like hl=fr&tbs=qdr:d)</p>--}}
                                        {{--</div> <!-- parameters -->--}}

                                        {{--<!-- local -->--}}
                                        {{--<div class="form-group">--}}
                                            {{--<label for="local">Local</label>--}}
                                            {{--<input type="text" class="form-control" id="local" name="local">--}}
                                            {{--<p class="help-block">City or place for local search, should be in the country of the tld</p>--}}
                                        {{--</div> <!-- local -->--}}
                                    {{--</div>--}}
                                    {{--
                                    <!-- actions -->
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-8 col-md-offset-2">
                                                <input type="submit" class="btn btn-success btn-lg btn-block" value="Add New Search">
                                            </div>
                                        </div>
                                    </div> <!-- actions -->

                                </form>

                            </div>
                            <!-- /Tab panes -->
                        </div>
                    </div>
                    <!-- /tile body -->
                </section>
                <!-- / tile -->
            </div>
            <!-- / col -->
        </div>
        <!-- / row -->
        --}}

    <!-- Modal -->
    <div id="loader" class="modal-backdrop fade">
        <div class="line-spin-fade-loader center">
            <div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div>
        </div>
    </div>

@stop


@section('scripts')
    <script src="{{ asset('assets/backend/js/vendor/selectize/selectize.js') }}"></script>

    <script>
        showLoader(false);

        var tldOptions = {!! $tldOptions !!};
        var languages = {!! $languages !!};

        $('.tld-selectize').selectize({
            plugins: ['remove_button'],
            valueField: 'name',
            labelField: 'engine',
            searchField: ['name', 'engine'],
            delimiter: ',',
            persist: false,
            hideSelected: true,
            openOnFocus: true,
            items: ['{{ $selectedEngine }}'],
            render: {
                item: function(item, escape) {
                    return '<div>' +
                        (item.engine ? '<span class="email">' + escape(item.engine) + '</span>' : '') +
                        (item.name ? '<span class="name"> ‒ ' + escape(item.name) + '</span>' : '') +
                    '</div>';
                },
                option: function(item, escape) {
                    var label = item.name || item.engine;
                    var caption = item.name ? item.engine : null;
                    return '<div class="selectItem">' +
                        (caption ? '<span>' + escape(caption) + '</span>' : '') +
                        '<span> ‒ ' + escape(label) + '</span>' +
                    '</div>';
                }
            },
            options: tldOptions
        });
        
        $('.lang-selectize').selectize({
            plugins: ['remove_button'],
            valueField: 'code',
            labelField: 'name',
            searchField: ['name', 'code'],
            delimiter: ',',
            persist: false,
            hideSelected: true,
            openOnFocus: true,
            items: ['{{ $selectedLanguage }}'],
            render: {
                item: function (item, escape) {
                    return '<div>' +
                            '<span>' + item.name + ' (' + item.code + ')</span>' +
                            '</div>';
                },
                option: function (item, escape) {
                    return '<div>' +
                            '<span>' + item.name + ' (' + item.code + ')</span>' +
                            '</div>';
                }
            },
            options: languages
        });
        
        function showLoader(show) {
            if (show === true) {
                $("#loader").show();
                $("#loader").addClass('in');
            } else {
                $("#loader").hide();
                $("#loader").removeClass('in');
            }
        };
    </script>
@stop