@extends('layouts.backend.master')

@section('style')
    <link rel="stylesheet" href="{{ asset('assets/css/loaders.css') }}">
    <style>
        .node {
            cursor: pointer;
        }

        .node circle {
            fill: #f0f;
            stroke: #546f8a;
            stroke-width: 2px;
        }

        .node text {
            font-size: 14px;
            font-weight: bold;
        }
        .node text.data {
            font: 12px Helvetica;
        }

        .link {
            fill: none;
            stroke: #ebeff2;
            stroke-width: 12px;
        }
    </style>
@stop

@section('content')
    
    <div class="row mb-20">
        <div class="col-sm-12">
            <h4 class="page-title">Brainstorm</h4>
            <!-- <ol class="breadcrumb">
                <li><a href="{{ URL::route('dashboard') }}">Dashboard</a></li>
                <li><a href="{{ URL::route('backend.tools.brainstorm') }}">Brainstorm</a></li>
                <li class="active">{{ $keyword }}</li>
            </ol> -->
        </div>
    </div>

    @include('layouts.backend.partials.messages')

    <!-- row -->
    <div class="row">
        <!-- col -->
        <div class="col-md-12">
            <div class="card-box">
                <div class="pull-left">
                    <h4 class="m-t-0 header-title"><b>Brainstorm</b></h4>
                    <p class="text-muted m-b-20 font-13">
                        Discover uncovered long tail keywords
                    </p>
                </div>
                <div class="input-group pull-right">
                    @if(!empty($brainstorm))
                        <a href="{{ URL::route('backend.tools.brainstorm.downloadSavedResearch', $brainstorm->id) }}" class="btn btn-default waves-effect waves-light pull-right ml-20">
                            Download CSV
                            <span class="btn-label btn-label-right"><i class="fa fa-file-excel-o"></i></span>
                        </a>
                    @else
                        <a href="#" id ="export" class="btn btn-default waves-effect waves-light pull-right ml-20">
                            Download CSV
                            <span class="btn-label btn-label-right"><i class="fa fa-file-excel-o"></i></span>
                        </a>
                    @endif
                </div>
                <div class="input-group pull-right">
                    <input type="button" id="saveButton" class="btn btn-primary w-100" value="Save" onclick="saveBrainstrom()">
                </div>

                <!-- Modal -->
                <div id="loader" class="modal-backdrop fade">
                    <div class="line-spin-fade-loader center">
                        <div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div>
                    </div>
                </div>

                {{--
                <div class="tile-header bg-slategray text-center">
                    <h4 class="custom-font text-uppercase">Viewing Brainstorm:
                        <strong>{{ $keyword }}</strong></h4>
                    <ul class="controls">
                        <li class="dropdown">
                            <a role="button" tabindex="0" class="dropdown-toggle settings" data-toggle="dropdown">
                                <i class="fa fa-cog"></i>
                                <i class="fa fa-spinner fa-spin"></i>
                            </a>
                            <ul class="dropdown-menu pull-right with-arrow animated littleFadeInUp">
                                <li>
                                    <a role="button" tabindex="0" class="tile-toggle">
                                        <span class="minimize"><i class="fa fa-angle-down"></i>&nbsp;&nbsp;&nbsp;Minimize</span>
                                        <span class="expand"><i class="fa fa-angle-up"></i>&nbsp;&nbsp;&nbsp;Expand</span>
                                    </a>
                                </li>
                                <li>
                                    @if(!empty($brainstorm))
                                        <a href="{{ URL::route('backend.tools.brainstorm.refresh', $brainstorm->id) }}"
                                           tabindex="0"
                                           onclick="swapElement('#alert-nodata', '#ajax-refresh');"> <i
                                                    class="fa fa-refresh"></i> Refresh Data
                                        </a>
                                    @elseif(empty($brainstorm))
                                        <a href="{{ URL::route('backend.tools.brainstorm.search', ['keyword'=>$keyword]) }}"
                                           tabindex="0"
                                           onclick="swapElement('#alert-nodata', '#ajax-refresh');"> <i
                                                    class="fa fa-refresh"></i> Refresh Data
                                        </a>
                                    @endif
                                </li>
                                @if(!empty($brainstorm))
                                    <li>
                                        <a href="{{ URL::route('backend.tools.brainstorm.delete', $brainstorm->id) }}"
                                           tabindex="0" class=""
                                           onclick="return confirm('Are you sure you wish to delete this keyword?');">
                                            <i class="fa fa-trash-o"></i> Delete
                                        </a>
                                    </li>
                                @endif
                            </ul>
                        </li>
                    </ul>
                </div> <!-- tile-header -->
                --}}
                <!-- tile body -->
                <div class="row">
                    <div class="col-md-12 cb">

                        {{--<div class="row">
                            <div class="pull-right" style="margin-right:15px;">
                                <input type="button" id="saveButton" class="btn btn-success" value="Save" onclick="saveBrainstrom()">
                                <div id="saveMessage" class="alert alert-success ajax-hidden">Saved</div>
                            </div>
                        </div>--}}

                        @if($hasData)
                            <div id="dthree" style="overflow: auto;"></div>
                        @elseif(!empty($brainstorm))
                            <div id="alert-nodata" class="alert alert-big alert-lightred alert-dismissable fade in">
                                <h4><strong>Heads up!</strong></h4>

                                <p>
                                    There was a problem quering for suggestions, please try again
                                </p>

                                <p>
                                    <a href="{{ URL::route('backend.tools.brainstorm.refresh', $brainstorm->id) }}"
                                       class="btn btn-default" onclick="swapElement('#alert-nodata', '#ajax-refresh');">Try
                                        Again</a>
                                </p>
                            </div>
                        @endif

                        <div id="ajax-refresh" class="ajax-hidden ajax-success-wait text-center" style="padding: 20px;">
                            <img src="{{ asset('assets/backend/images/success-loader.gif') }}" alt="">
                            <strong>
                                Please Wait, Refreshing Data ...
                            </strong>
                        </div>
                    </div>
                </div>
            </div>
            <!-- / tile -->
        </div>
        <!-- / col -->
    </div>
    <!-- / row -->

@stop


@section('scripts')


    <script src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.5/d3.min.js"></script>

    <script>
        showLoader(false);
        var jsonData = '{!! $keywords !!}';
        var cpcData = '{!!$cpc_data!!}';
        var id = '{!! (isset($brainstorm->id))?$brainstorm->id:''!!}';

        if (id != '') {
            $("#saveButton").hide();
            $("#saveMessage").show();
        }

        function countKeywordNumber(data) {
            var keywordNumber = 0;
            var presentNumber = data.children.length;
            if (presentNumber > 0) {
                keywordNumber += presentNumber;
                for (var i = 0; i < presentNumber; i++) {
                    keywordNumber += countKeywordNumber(data.children[i]);
                }
            }
            return keywordNumber;
        }

        //save brainstrom
        function saveBrainstrom() {
            $.ajax({
                url: '{{url('tools/brainstorm/insert')}}',
                type: 'POST',
                data: {
                    cpc_data: cpcData,
                    data: jsonData,
                    keyword: '{{$keyword}}',
                    keywordCount: countKeywordNumber(JSON.parse(jsonData)),
                    id: id
                },
                success: function (response) {
                    if(response.success){
                        // show alert
                        $("#saveButton").hide();
                        $("#saveMessage").show();
                        $('#success').show();

                        // Add item to navigation bar
                        // $('#brainstromItem').append('<li><a href="http://' + window.location.hostname+'/tools/brainstorm/view/'+response.data.id+'"><i class="fa fa-arrow-circle-right"></i>&nbsp;'+response.data.keyword+'</a></li>');
                    }
                    else {
                        $('#error').show();
                        $('#errMsg').html(response.message);
                    }
                },
                error: function (response) {
                    $('#error').show();
                    $('#errMsg').html(JSON.parse(response.responseText).message);
                }
            });
        }

        function showLoader(show) {
            if (show === true) {
                $("#loader").show();
                $("#loader").addClass('in');
            } else {
                $("#loader").hide();
                $("#loader").removeClass('in');
            }
        };

        var margin = {top: 0, right: 120, bottom: 0, left: 200},
                width = 1500 - margin.right - margin.left,
                height = 900 - margin.top - margin.bottom;
        var i = 0,
                duration = 750,
                root;
        var tree = d3.layout.tree().size([height, width]);
        var diagonal = d3.svg.diagonal().projection(function (d) {
            return [d.y, d.x];
        });
        var svg = d3.select("#dthree")
                .append("svg")
                .attr("width", width + margin.right + margin.left)
                .attr("height", height + margin.top + margin.bottom)
                .append("g")
                .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

        root = JSON.parse(jsonData);
        root.x0 = height / 2;
        root.y0 = 0;
        root.value = root.name;
        var keyStr = root.name + ',';
        function collapse(d) {
            d.value = d.name;
            keyStr += d.name + ',';
            if (d.children) {
                d._children = d.children;
                d._children.forEach(collapse);
                d.children = null;
            }
        }

        var extData = [];
        root.children.forEach(collapse);

        if (cpcData != '') {
            extData = JSON.parse(cpcData);
            update(root);
        }
        else {
            //update(root);
            showLoader(true);

            $.ajax({
                url: '{{url('tools/brainstorm/apiData')}}',
                type: 'POST',
                data: {id: keyStr},
                success: function (response) {
                    if (response.success && response.data.error) {
                        $('#error').show();
                        $('#errMsg').html('Sorry, server currently unable to provide full data, please try again later.');
                        showLoader(false);
                    }
                    else if (response.success) {
                        update(root);
                        cpcData = response.data;
                        extData = JSON.parse(cpcData);
                        showLoader(false);
                        if (cpcData != '') {
                            for (var i = 0; i < extData.length; i++) {
                                var str = "AMS: " + parseInt(extData[i]['avg']).formatNumber(0, '.', ',') + " | CPC: $" + extData[i]['bid'];
                                $("[id='"+extData[i]['name']+"']").text(str);
                            }

                            if (id != '') {
                                $.ajax({
                                    url: '{{url('tools/brainstorm/storeCpcData')}}',
                                    type: 'POST',
                                    data: {
                                        cpc_data: cpcData,
                                        id: id
                                    },
                                    success: function (response) {
                                    },
                                    error: function (response) {

                                    }
                                });
                            }
                        }
                        else {
                            return "";
                        }
                    }
                    else {
                        showLoader(false);
                    }
                }
            });
        }

        function update(source) {
            // Compute the new tree layout.
            var nodes = tree.nodes(root).reverse();
            // Collapse all branches except selected.
            nodes.forEach(function (d) {
                if (source.depth === d.depth && source.id !== d.id && d.children) {
                    d._children = d.children;
                    d.children = null;
                }
            });
            nodes = tree.nodes(root).reverse();
            var links = tree.links(nodes);
            // Normalize for fixed-depth.
            nodes.forEach(function (d) {
                d.y = d.depth * 400;
            });
            // Update the nodes…
            var node = svg.selectAll("g.node")
                    .data(nodes, function (d) {
                        return d.id || (d.id = ++i);
                    });
            // Enter any new nodes at the parent's previous position.
            var nodeEnter = node.enter().append("g")
                    .attr("class", "node")
                    .attr("transform", function (d) {
                        return "translate(" + source.y0 + "," + source.x0 + ")";
                    });
            nodeEnter.append("circle")
                    .attr("r", 1e-6)
                    .style("fill", function (d) {
                        return d._children ? "lightsteelblue" : "#fff";
                    })
                    .on("click", click);
            nodeEnter.append("text")
                    .attr("x", function (d) {
                        return d.children || d._children ? -10 : 10;
                    })
                    .attr("dy", ".35em")
                    .attr("class", function (d) {
                        return "node-title"
                    })
                    .attr("text-anchor", function (d) {
                        return d.children || d._children ? "end" : "start";
                    })
                    .text(function (d) {
                        return d.name;
                    })
                    .attr("class", "keyword")
                    .style("fill-opacity", 1e-6)
                    .on("click", click1);
            nodeEnter.append("text")
                    .attr("x", function (d) {
                        return d.children || d._children ? -10 : 10;
                    })
                    .attr("dy", "1.9em")
                    .attr("id", function (d) {
                        return d.value
                    })
                    .attr("text-anchor", function (d) {
                        return d.children || d._children ? "end" : "start";
                    })
                    .attr("class", "data")
                    .text(function (d) {
                        var str = "AMS: -  | CPC: - " ;
                        for (var i = 0; i < extData.length; i++) {
                            if (extData[i]['name'] == d.value) {
                                str = "AMS: " + parseInt(extData[i]['avg']).formatNumber(0, '.', ',') + " | CPC: $" + extData[i]['bid'];
                                $("#" + extData[i]['name']).text(str);
                                break;
                            }
                        }
                        return str;
                    });
            // Transition nodes to their new position.
            var nodeUpdate = node.transition()
                    .duration(duration)
                    .attr("transform", function (d) {
                        return "translate(" + d.y + "," + d.x + ")";
                    });
            nodeUpdate.select("circle")
                    .attr("r", 7)
                    .attr("class", function (d) {
                        var circleClass = '';
                        circleClass += d.depth ? "depth-" + d.depth : "depth-0";
                        circleClass += d._children == null ? " circle-opened" : " circle-closed";
                        return circleClass;
                    })
                    .style("fill", function (d) {
                        var color = d._children ? ( d._children.length ? "#ffce56" : "#36a2eb") : "#4dceb3";
                        color = (d.children === undefined && d._children === null) ? '#36a2eb' : color;
                        return color;
                    });
            nodeUpdate.select("text")
                    .style("fill-opacity", 1);
            // Transition exiting nodes to the parent's new position.
            var nodeExit = node.exit().transition()
                    .duration(duration)
                    .attr("transform", function (d) {
                        return "translate(" + source.y + "," + source.x + ")";
                    })
                    .remove();
            nodeExit.select("circle")
                    .attr("r", 1e-6);
            nodeExit.select("text")
                    .style("fill-opacity", 1e-6);
            // Update the links…
            var link = svg.selectAll("path.link")
                    .data(links, function (d) {
                        return d.target.id;
                    });
            // Enter any new links at the parent's previous position.
            link.enter().insert("path", "g")
                    .attr("class", "link")
                    .attr("d", function (d) {
                        var o = {x: source.x0, y: source.y0};
                        return diagonal({source: o, target: o});
                    });
            // Transition links to their new position.
            link.transition()
                    .duration(duration)
                    .attr("d", diagonal);
            // Transition exiting nodes to the parent's new position.
            link.exit().transition()
                    .duration(duration)
                    .attr("d", function (d) {
                        var o = {x: source.x, y: source.y};
                        return diagonal({source: o, target: o});
                    })
                    .remove();
            // Stash the old positions for transition.
            nodes.forEach(function (d) {
                d.x0 = d.x;
                d.y0 = d.y;
            });
        }

        function click1(event) {
            var tld = '{{$location}}';
            var lang = '{{$language}}';
            var keyword = $(this).html();

            bootbox.dialog({
                message: "Are you sure want to research " + $(this).html() + "?",
                title: "Research confirmation!",
                buttons: {
                    success: {
                        label: "Yes",
                        className: "btn btn-default waves-effect waves-light w-100",
                        callback: function () {
                            window.location.href = 'http://' + window.location.hostname + '/tools/keywordresearch?keyword=' + keyword;
                        }
                    },
                    danger: {
                        label: "No",
                        className: "btn btn-cancel waves-effect waves-light w-100",
                        callback: function () {
                            //Example.show("uh oh, look out!");
                        }
                    }
                }
            })
        }

        stopEventPropogat();

        function stopEventPropogat() {
            $('.node-title').click(function (event) {
                event.preventDefault();
                event.stopPropagation();
            });
        }

        // Toggle children on click.
        function click(d) {
            if (d.children) {
                d._children = d.children;
                d.children = null;
            } else {
                d.children = d._children;
                d._children = null;
            }
            update(d);
            stopEventPropogat();
        }

        function exportDataToCSV(extData, filename) {
            if (extData.length > 0) {
                var keys = Object.keys(extData[0]);
            } else {
                return
            }

            var tmpColDelim = String.fromCharCode(11), // vertical tab character
                tmpRowDelim = String.fromCharCode(0), // null character

                colDelim = '","',
                rowDelim = '"\r\n"',

                csv = '"' + ('Keyword' + tmpColDelim + 'AVG' + tmpColDelim + 'CPC' + tmpRowDelim + extData.map(function (i, obj) {
                    keywordObj = extData[obj];
                    return keys.map(function (j, col) {
                        var text = keywordObj[keys[col]];
                        return text;
                    }).join(tmpColDelim);
                }).join(tmpRowDelim))
                    .split(tmpRowDelim).join(rowDelim)
                    .split(tmpColDelim).join(colDelim) + '"';

            if (window.navigator.msSaveBlob) {
                var blob = new Blob([decodeURIComponent(csv)], {
                      type: 'text/csv;charset=utf8'
                });
                window.navigator.msSaveBlob(blob, filename);
            } else if (window.Blob && window.URL) {
                // HTML5 Blob        
                var blob = new Blob([csv], { type: 'text/csv;charset=utf8' });
                var csvUrl = URL.createObjectURL(blob);
                $(this).attr({
                    'download': filename,
                    'href': csvUrl
                });
            } else {
                // Data URI
                var csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(csv);

                $(this).attr({
                    'download': filename,
                    'href': csvData,
                    'target': '_blank'
                });
            }
        }

        $("#export").on('click', function (event) {
            var filename = "<?php echo $keyword ?>";
            var args = [extData, filename+'.csv'];
            exportDataToCSV.apply(this, args);
        });

    </script>
@stop
