@extends('layouts.backend.master')

@section('style')
<link rel="stylesheet" href="{{ asset('assets/backend/css/vendor/selectize.default.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/loaders.css') }}">
@stop


@section('content')
    <div class="row mb-20">
        <div class="col-sm-12">
            <h4 class="page-title">Brainstorm</h4>
            <!-- <ol class="breadcrumb">
                <li><a href="{{ URL::route('dashboard') }}">Dashboard</a></li>
                <li class="active">Brainstorm</li>
            </ol> -->
        </div>
    </div>

    @include('layouts.backend.partials.messages')

    <div class="row mb-20">
        <div class="col-md-12">
            <div class="card-box">
                <h4 class="m-t-0 header-title"><b>Brainstorm</b></h4>
                <p class="text-muted m-b-20 font-13">
                    Discover uncovered long tail keywords
                </p>
                
                <div class="row">
                    {!! Form::open(['route' => ['backend.tools.brainstorm.search'], 'method' => 'GET']) !!}
                        <div class="ccol-xs-12 col-md-5 mb-0">
                            <div class="form-group">
                                <label class="control-label" for="projectname">Please enter a keyword</label>
                                {!! Form::text('keyword', null, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                                                            <div class="col-xs-12 col-md-7">
                                                                <div class="row">
                                                                    <div class="col-md-5 col-xs-6 col-sm-5">
                                                                        <div class="form-group">
                                                                            <label class="control-label" for="tld">Search
                                                                                Location</label>
                                                                            <select class="selectize tld-selectize searchSelection"
                                                                                    data-target="location" id="tld"
                                                                                    name="tld"></select>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-4 col-xs-6 col-sm-4">
                                                                        <div class="form-group">
                                                                            <label class="control-label" for="lang">Search
                                                                                Language</label>
                                                                            <select class="selectize lang-selectize searchSelection"
                                                                                    data-target="languages" id="lang"
                                                                                    name="lang"></select>
                                                                        </div>
                                                                    </div>

                        <div class="col-md-3 col-xs-12 col-sm-3">
                            <div class="form-group mb-0">
                                <label class="control-label">&nbsp;</label>
                                <div id="actions" class="">
                                    <input type="submit" class="btn btn-large btn-primary btn-block projectsubmit"
                                           value="Start Search"
                                           onclick="showLoader(true);">
                                </div>
                                <!-- <div id="ajax" class="ajax-hidden ajax-success-wait">
                                    <img src="{{ asset('assets/backend/images/success-loader.gif') }}">
                                    Please Wait ...
                                </div> -->
                            </div>
                        </div>                        
                        </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

    <!-- row -->
    <div class="row">
        <!-- col -->
        <div class="col-md-12">
            <!-- tile -->
            <div class="card-box">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="pull-left">
                            <h4 class="m-t-0 header-title"><b>Recent Brainstorms</b></h4>
                            <p class="text-muted m-b-20 font-13">
                                Previous saved brainstorms
                            </p>
                        </div>
                        <div id="actions" class="input-group pull-right">
                        </div>

                        <table class="table">
                            <thead>
                            <tr>
                                <!-- <th class="no-sort chk-all-th" style="width: 20px">
                                    <input type="checkbox" id="checkAll" name="checkAll">
                                </th> -->
                                <th>
                                    Keyword
                                </th>
                                <th>
                                    Created date
                                </th>
                                <!-- <th>
                                    Group Count
                                </th> -->
                                <th>
                                    Keyword Count
                                </th>
                                <th class="text-right">
                                    Actions
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($brainstorms->count() > 0)
                                @foreach($brainstorms as $brainstorm)
                                    <tr id="brainstorm_{{ $brainstorm->id }}">
                                        <!-- <td class="no-sort chk-all-th" style="width: 20px">
                                            <input type="checkbox" value="{{$brainstorm->id}}">
                                        </td> -->
                                        <td>
                                            {{ $brainstorm->keyword }}
                                            <input type="hidden" name="forSingleDelete" id="forSingleDelete"
                                                   value="{{$brainstorm->id}}">
                                        </td>
                                        <td>
                                            {{ $brainstorm->created_at->format('m/d/Y') }}
                                        </td>
                                        <!-- <td>
                                                {{ $brainstorm->group_count }} </td> -->
                                        <td>
                                            {{ $brainstorm->keyword_count }}
                                        </td>
                                        <td class="text-right actions">
                                            <!-- <ul class="controls">
                                                <li class="dropdown">
                                                    <a role="button" tabindex="0" class="dropdown-toggle settings">
                                                        <i class="fa fa-gears f-s-17"></i>
                                                    </a>
                                                    <ul class="dropdown-menu pull-right with-arrow animated littleFadeInUp">
                                                        <li> -->
                                                            <a href="{{ URL::route('backend.tools.brainstorm.view', $brainstorm->id) }}">
                                                                <i class="action-icon icon-arrow-right-circle"></i>
                                                            </a>
                                                        <!-- </li>
                                                        <li> -->
                                                            <a href="javascript:void(0);" onclick="onDelete({{ $brainstorm->id}})" class="single-delete">
                                                                <i class="action-icon icon-trash"></i>
                                                            </a>
                                                        <!-- </li>
                                                    </ul>
                                                </li>
                                            </ul> -->
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="5">
                                        <div class="alert alert-warning cb">
                                            <strong>Oh No!</strong>
                                            You do not have any brainstorming happening
                                        </div>
                                    </td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                        <!-- <div class="form-group">
                            <a href="javascript:void(0);" class="delete-btn btn btn-danger"
                               style="margin: 15px">Delete</a>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div id="loader" class="modal-backdrop fade">
        <div class="line-spin-fade-loader center">
            <div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div>
        </div>
    </div>

@stop


@section('scripts')
	<script src="{{ asset('assets/backend/js/vendor/selectize/selectize.js') }}"></script>
	<script src="{{ asset('assets/backend/js/vendor/easypiechart/jquery.easypiechart.min.js') }}"></script>
    <script type="text/javascript">
                var selection = {
                    location: {
                        data: {!! $tldOptions !!},
                        items: '{{ $selectedEngine }}',
                        valueField: 'code',
                        labelField: 'name',
                        searchField: ['name', 'code']
                    },

                    languages: {
                        data: {!! $languages !!},
                        items: '{{ $selectedLanguage }}',
                        valueField: 'name',
                        labelField: 'engine',
                        searchField: ['name', 'engine']
                    },
                };		

                var tldOptions = {!! $tldOptions !!};
                var languages = {!! $languages !!};
                //var keyword = '{!! $keyword !!}';
                var Selected = {language: '{{ $selectedLanguage }}', location: '{{ $selectedEngine }}'};

                $('.tld-selectize').selectize({
                    plugins: ['remove_button'],
                    valueField: 'name',
                    labelField: 'engine',
                    searchField: ['name', 'engine'],
                    delimiter: ',',
                    persist: false,
                    hideSelected: true,
                    openOnFocus: true,
                    items: ['{{ $selectedEngine }}'],
                    render: {
                        item: function (item, escape) {
                            return '<div><span>' + item.engine + ' ‒ ' + item.name + '</span></div>';
                        },
                        option: function (item, escape) {
                            return '<div><span>' + item.engine + ' ‒ ' + item.name + '</span></div>';
                        }
                    },
                    options: tldOptions,
                    onChange: function (value) {
                        Selected.location = value;
                    }
                });
                
                $('.lang-selectize').selectize({
                    plugins: ['remove_button'],
                    valueField: 'code',
                    labelField: 'name',
                    searchField: ['name', 'code'],
                    delimiter: ',',
                    persist: false,
                    hideSelected: true,
                    openOnFocus: true,
                    items: ['{{ $selectedLanguage }}'],
                    render: {
                        item: function (item, escape) {
                            return '<div><span>' + item.name + ' (' + item.code + ')</span></div>';
                        },
                        option: function (item, escape) {
                            return '<div>' +
                                '<span>' + item.name + ' (' + item.code + ')</span>' +
                                '</div>';
                        }
                    },
                    options: languages,
                    onChange: function (value) {
                        Selected.language = value;
                    }
                });
                		
        showLoader(false);

        $('.actions a').on('click', function(event){
            event.stopPropagation();
        });

        function showLoader(show) {
            if (show === true) {
                $("#loader").show();
                $("#loader").addClass('in');
            } else {
                $("#loader").hide();
                $("#loader").removeClass('in');
            }
        };

        function onDelete(val) {
            singleDeleteKeyword(val);
        }

        function singleDeleteKeyword(val) {

            bootbox.dialog({
                message: "Are you sure you want to perform delete?",
                title: "Delete confirmation!",
                buttons: {
                    success: {
                        label: "Yes",
                        className: "btn btn-danger waves-effect waves-light w-100",
                        callback: function () {
                            //Example.show("great success");
                            $.ajax({
                                url: '{{ URL::route('backend.tools.brainstorm.delete') }}',
                                data: {'id': val},
                                type: 'get',
                                dataType: 'json',
                                success: function (result) {
                                    bootbox.hideAll();
                                    $("#brainstorm_" + val).remove();
                                    // $('#saveTbl').DataTable().ajax.reload();
                                }
                            })
                            return true;
                        }
                    },
                    danger: {
                        label: "No",
                        className: "btn btn-default waves-effect waves-light w-100",
                        callback: function () {
                            //Example.show("uh oh, look out!");
                        }
                    },
                }
            });
        }
    </script>

@stop
