@extends('layouts.backend.master')

@section('style')
@stop


@section('content')

    <div class="page page-dashboard">
        <div class="pageheader">
            <h2>Brainstorm <span>// Discover uncovered long tail keywords</span></h2>
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <a href="{{ URL::route('dashboard') }}"><i class="fa fa-home"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href="{{ URL::route('backend.tools.brainstorm') }}">Brainstorm</a>
                    </li>
                </ul>
            </div>
        </div>

        @include('layouts.backend.partials.messages')

        <!-- row -->
        <div class="row">
            <!-- col -->
            <div class="col-md-12">
                <!-- tile -->
                <section class="tile tile-simple">
                    <!-- tile body -->
                    <div class="tile-body p-0">
                        <div role="tabpanel">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs tabs-dark tabs-right" role="tablist">
                                <li class="tabs-title pull-left">Viewing <strong>KEYWORD HERE</strong></li>
                                <li><a href="{{ URL::route('backend.tools.brainstorm') }}">Listing</a></li>
                                <li><a href="{{ URL::route('backend.tools.ranktracker.add') }}">New Keyword</a></li>
                            </ul>

                            <!-- Content -->
                            <div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <section class="tile">
                                            <div class="tile-header dvd dvd-btm">
                                                <h1 class="custom-font"><strong>{{ $keywordSearch }}</strong> Results</h1>
                                            </div> <!-- /tile header -->
                                             <!-- tile body -->
                                             <div class="tile-body">
                                                <ul class="tabs-menu">
                                                    @foreach($keywords as $keyword => $ignore)
                                                        <li role="presentation">
                                                            <a href="#{{ str_replace(' ', '', $keyword) }}" aria-controler="{{ str_replace(' ', '', $keyword) }}" role="tab" data-toggle="tab">{{ $keyword }}</a>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                             </div> <!-- tile body -->
                                        </section>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="tab-content">
                                            @foreach($keywords as $keyword => $innerKeyword)
                                                <div role="tabpanel" class="tab-pane fade" id="{{ str_replace(' ', '', $keyword) }}">
                                                    <section>
                                                        <div class="tile-header dvd dvd-btm">
                                                            <h1 class="custom-font"><strong>{{ $keyword }}</strong></h1>
                                                        </div> <!-- /tile header -->
                                                         <!-- tile body -->
                                                         <div class="tile-body">
                                                            <table class="table table-bordered table-striped table-hover">
                                                                @foreach($innerKeyword as $keywordString)
                                                                    <tr>
                                                                        <td>
                                                                            {{ $keywordString }}
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                            </table>
                                                         </div> <!-- tile body -->
                                                    </section>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /Content -->
                        </div>
                    </div>
                    <!-- /tile body -->
                </section>
                <!-- / tile -->
            </div>
            <!-- / col -->
        </div>
        <!-- / row -->

    </div>
@stop


@section('scripts')
@stop