@extends('layouts.backend.master')

@section('style')
@stop


@section('content')

    <div class="page page-dashboard">
        <div class="pageheader">
            <h2>Brainstorm <span>// Discover uncovered long tail keywords</span></h2>
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <a href="{{ URL::route('dashboard') }}"><i class="fa fa-home"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href="{{ URL::route('backend.tools.brainstorm') }}">Brainstorm</a>
                    </li>
                </ul>
            </div>
        </div>

        <!-- row -->
        <div class="row">
            <!-- col -->
            <div class="col-md-12">
                <!-- tile -->
                <section class="tile tile-simple">
                    <!-- tile body -->
                    <div class="tile-body p-0">
                        <div role="tabpanel">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs tabs-dark" role="tablist">
                                <li><a href="{{ URL::route('backend.tools.brainstorm') }}">Listing</a></li>
                                <li class="active"><a href="{{ URL::route('backend.tools.brainstorm.add') }}">New Brainstorm</a></li>
                                <li class="tabs-title pull-right" style="padding-right: 15px;">New <strong>Brainstorm</strong></li>
                            </ul>

                            <div class="tile-body">
                                <div class="alert alert-cyan">
                                    <strong>Heads up!</strong>
                                    <p>
                                        Let's put a little information about what this tool is and does in here ok?
                                    </p>
                                </div>

                                @include('layouts.backend.partials.messages')

                                <form method="POST" action="{{ URL::route('backend.tools.brainstorm.insert') }}" autocomplete="off">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <label for="keyword">Please enter a keyword</label>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-9">
                                            <div class="form-group">
                                                <input type="text" class="form-control" id="keyword" name="keyword">
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <div id="actions">
                                                    <input type="submit" class="btn btn-success btn-block" value="Start Search" onclick="swapElement('#actions', '#ajax');">
                                                </div>
                                                <div id="ajax" class="ajax-hidden ajax-success-wait">
                                                    <img src="{{ asset('assets/backend/images/success-loader.gif') }}" > Please Wait ...
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- /tile body -->
                </section>
                <!-- / tile -->
            </div>
            <!-- / col -->
        </div>
        <!-- / row -->

    </div>
@stop


@section('scripts')
@stop