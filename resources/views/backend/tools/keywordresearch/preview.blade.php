@extends('layouts.backend.master')

@section('style')
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/plug-ins/1.10.10/sorting/numeric-comma.js"/>
    <link rel="stylesheet" href="{{ asset('assets/css/loaders.css') }}">
@stop

@section('content')

    <div id="fade"></div>

    <div class="row mb-20">
        <div class="col-sm-12">
            <h4 class="page-title">Keyword Research</h4>
        </div>
    </div>

    <!-- row -->
    <div class="row">
        <!-- col -->
        <div class="col-md-12">
            <!-- Nav tabs -->
            <ul id="tabList" class="nav nav-tabs tabs" role="tablist" style="background:#fff">
                <li id="tab-evalTbl" class="active">
                    <a href="evalTbl" aria-controls="evalTbl" role="tab" data-toggle="tab">Research Results</a>
                </li>
                <li id="tab-saved">
                    <a href="saved" aria-controls="saved" role="tab" data-toggle="tab">Saved Keywords</a>
                </li>
                <li id="tab-evaluated">
                    <a href="evaluated" aria-controls="evaluated" role="tab" data-toggle="tab">Unsaved Evaluated Keywords</a>
                </li>
            </ul>

            <div id="saveActions" style="position:absolute; top:8px; right:20px;"></div>

            <!-- Tab panes -->
            <div id="tabContent" class="tab-content p-0 mt-20">
                <!-- saved -->
                <div role="tabpanel" class="tab-panel" id="evaluated">
                    <div id="cover-eval" style="width: 100%; height: 100%; background: #ebeff2; position: absolute; z-index: 99;"></div>
                    <div id="savedDetailInfo"></div>
                    <div id="savedContentWrapper">
                        <div id="savedContent"></div>
                        <div class="alert alert-info">
                            This project does not have any saved keywords yet.
                        </div>
                    </div>
                    <div id="notSavedContentWrapper">
                        <div id="notSavedContent"></div>
                        <div class="alert alert-info">
                            There are no unsaved evaluated keywords for the past 12 hours
                        </div>
                    </div>
                </div>
                <!-- saved -->

                <!-- results -->
                <div role="tabpanel" class="tab-panel" id="evalTbl">

                @include('layouts.backend.partials.messages')

                @if($researchData)
                    <div id="cover-tbl" style="width: 100%; height: 100%; background: #ebeff2; position: absolute; z-index: 99;"></div>
                    <div class="card-box">
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-default">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" class="collapsed btn btn-primary">
                                                    Filter
                                                </a>
                                            </h4>
                                        </div>
                                    </div>
                                    <div class="col-xs-9 pull-right pl-0">
                                        <!-- <a href="{{ URL::route('backend.tools.keywordresearch.downloadPreview', $tmp_id) }}" 
                                            class="btn btn-default waves-effect waves-light pull-right ml-20">Download CSV
                                            <span class="btn-label btn-label-right"><i class="fa fa-file-excel-o"></i></span>
                                        </a> -->

                                        <a href="#" data-toggle="modal" data-target=".confirm-modal" class="btn btn-default waves-effect waves-light pull-right ml-20">
                                            Download CSV
                                            <span class="btn-label btn-label-right">
                                                <i class="fa fa-file-excel-o"></i>
                                            </span>
                                        </a>

                                        <button class="btn btn-default pull-right ml-20" onclick="dCheckAll()">Check Domains</button>
                                        <button class="btn btn-default pull-right" disabled="disabled">Save Project</button>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div id="collapseOne" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="control-label">Keyword Search</label>
                                                            <input type="text" class="form-control" id="minepc">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="control-label">Keyword Exclude</label>
                                                            <input type="text" class="form-control" id="mineke">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="control-label">Number Of Words</label>

                                                        <div class="row">
                                                            <div class="col-md-5">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control"
                                                                           id="nowordsmin">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-1">-</div>
                                                            <div class="col-md-5">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control"
                                                                           id="nowordsmax">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div> <!-- /row -->

                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <label class="control-label">Average Monthly
                                                            Searches</label>

                                                        <div class="row">
                                                            <div class="col-md-5">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control"
                                                                           placeholder="min" id="min">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-1">-</div>
                                                            <div class="col-md-5">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control"
                                                                           placeholder="max" id="max">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="control-label">Cost per Click</label>

                                                        <div class="row">
                                                            <div class="col-md-5">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control"
                                                                           placeholder="min" id="mincpc">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-1">-</div>
                                                            <div class="col-md-5">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control"
                                                                           placeholder="max" id="maxcpc">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="control-label">Estimated Potential
                                                            Earning</label>

                                                        <div class="row">
                                                            <div class="col-md-5">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control"
                                                                           placeholder="min" id="minlms">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-1">-</div>
                                                            <div class="col-md-5">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control"
                                                                           placeholder="max" id="maxlms">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">&nbsp;</div>
                                                </div> <!-- /row -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="modal">
                            <div class="loader-panel panel-default" style="border-radius:5px;">
                                <div class="model-header text-left">
                                    <h4 class="loader-panel-text">Processing...</h4>
                                </div>
                                <hr style="margin-top:0px;margin-bottom:20px;">
                                <div class="panel-body">
                                    <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:100%;margin-top:-20px;">
                                        &nbsp;
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="table-responsive">
                            <table class="table" id="researchTable">
                                <thead>
                                <tr>
                                    <th style="width: 40%">
                                        <span data-toggle="tooltip" data-placement="top" title="Generated Keyword Ideas">Keywords</span>
                                    </th>
                                    <th class=" {sorter: 'digit'}\ text-center column-width">
                                        <span data-toggle="tooltip" data-placement="top" title="Monthly Searches">AMS</span>
                                    </th>
                                    <th class="text-center column-width">
                                        <span data-toggle="tooltip" data-placement="top" title="The Approximate Cost Per Click">CPC</span>
                                    </th>
                                    <th class="text-center column-width">
                                        <span data-toggle="tooltip" data-placement="top" title="Potential Monthly Earning from Ads Clicks">Profit</span>
                                    </th>
                                    <th class="text-center column-width">
                                        <span data-toggle="tooltip" data-placement="top" title="Number of Words">Words</span>
                                    </th>
                                    @if(Auth::user()->can('domain_finder'))
                                        <th class="text-center " style="min-width: 88px">
                                            <span data-toggle="tooltip" data-placement="top" title="Exact Match Domain Availability">Domain</span>
                                        </th>
                                    @endif
                                    <th class="text-center column-width">
                                        <span data-toggle="tooltip" data-placement="top"
                                            title="Calculate the Ranking Difficulty and Opportunities, the Lower the Better">Difficulty</span>
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($researchData as $keyIndex=>$record)
                                    <tr>
                                        <td {{$record['keyword'] == $keyword ? 'style=color:#428bca;' : '' }}>
                                            {{ $record['keyword'] }}
                                        </td>
                                        <td class="text-center">
                                            {{ $record['searches'] }}
                                        </td>
                                        <td class="text-center">
                                            @if ($record['cpc'] == 0)
                                                {{'$0.00'}}
                                            @else
                                                {{'$'.$record['cpc'] }}
                                            @endif
                                        </td>
                                        <td class="text-center">
                                            @if ($record['cpc'] == 0)
                                                {{'$0'}}
                                            @else
                                                {{ '$'.number_format( (float)str_replace( ",", "", $record['profit']), 0, '', ',') }}
                                            @endif

                                        </td>
                                        <td class="text-center">
                                            {{ $record['word_count'] }}
                                        </td>
                                        @if(Auth::user()->can('domain_finder'))
                                            <td class="text-center domain" id="{{ $record['keyword'] }}"
                                                onclick="getDomain({{ $record['keyword'] }}, this)">
                                            </td>
                                        @endif
                                        <td class="text-center" style="color:#1693A5 ;font-size:20px;"
                                            id="evolution_{{ $keyIndex }}">
                                            @if(array_key_exists($record['keyword'], $eval_data))
                                                <a href="#" data-index="{{$keyIndex}}" onclick="showReEvaluationConfirm('{{ $record['keyword'] }}','{{ $record['keyword_id'] }}', '{{ $record['searches'] }}', '{{ $record['cpc'] }}', '{{ $record['profit'] }}', '{{ $record['word_count'] }}', '{{ $record['location'] }}', '{{ $record['language'] }}', '{{ $keyIndex }}','{{ $eval_data[$record['keyword']]['id'] }}',true, '{{$eval_data[$record['keyword']]['isUnsaved']}}'); return false;">{{$eval_data[$record['keyword']]['difficulty']}}</a>
                                            @else
                                                <a href="#" class="btn btn-primary btn-block" onclick="showEvaluation('{{ $record['keyword'] }}','{{ $record['keyword_id'] }}', '{{ $record['searches'] }}', '{{ $record['cpc'] }}', '{{ $record['profit'] }}', '{{ $record['word_count'] }}', '{{ $record['location'] }}', '{{ $record['language'] }}', '{{ $keyIndex }}',null,false); return false;">Evaluate</a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                @else
                    <div class="alert alert-warning" id="alert-error">
                        <strong>Sorry</strong>
                        an error occurred trying to retrieve the data, please try again.
                        <div class="text-right">
                            <a href="#" class="btn btn-primary"
                               onclick="location.reload(); showWait(); return false;">Retry</a>
                        </div>
                    </div>
                    <div class="ajax-hidden" id="ajax-wait">
                        <div class="alert alert-info">
                            <p class="text-center">
                                <strong>Please Wait ...</strong>
                            </p>
                        </div>
                    </div>
                @endif
                </div> <!-- results -->

            </div> <!-- tab-content -->
        </div> <!-- /col -->
    </div> <!-- /row -->

    <!-- Modal -->
    <div id="loader" class="modal-backdrop fade in">
        <div class="line-spin-fade-loader center">
            <div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div>
        </div>
    </div>

    <!-- Confirm Modal -->
    <div class="modal fade confirm-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Refine the action</h4>
                </div>
                <div class="modal-body">
                    <p>Do you want to download only the evaluated keywords or the entire list of keywords?</p>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-md-6">
                            <button type="button" onclick="downloadEvaluated('{{$tmp_id}}', false)" class="btn btn-default waves-effect waves-light pull-right confirm-button">Evaluated</button>
                            <!-- <a href="{{ URL::route('backend.tools.keywordresearch.downloadEvaluated', $tmp_id) }}" class="btn btn-default waves-effect waves-light pull-right confirm-button">Evaluated</a> -->
                        </div>
                        <div class="col-md-6">
                            <button type="button" onclick="downloadEvaluated('{{$tmp_id}}', true)" class="btn btn-default waves-effect waves-light pull-left confirm-button">All</button>
                            <!-- <a href="{{ URL::route('backend.tools.keywordresearch.downloadPreview', $tmp_id) }}" class="btn btn-default waves-effect waves-light pull-left confirm-button">All</a> -->
                        </div>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

@stop


@section('modals')
@stop

@section('scripts')
    <script src="{{ asset('assets/backend/js/vendor/easypiechart/jquery.easypiechart.min.js') }}"></script>

    <script>
        var keywordsObj = {!! json_encode($eval_data) !!},
            keywords = Object.keys(keywordsObj),
            keywordsWithDifficulties = {};

        for (var i = 0; i < keywords.length; i++) {
            keywordsWithDifficulties[keywords[i]] = keywordsObj[keywords[i]].difficulty;
        }

        $(".confirm-button").on('click', function (e) {
            $('.confirm-modal').modal('hide');
        });

        function downloadEvaluated(id, allFalg) {
            var ajaxUrl = '{{ URL::route('backend.tools.keywordresearch.downloadEvaluated', ['id', 'allFalg', 'eval_data']) }}'
                .replace('id', id)
                .replace('allFalg', allFalg)
                .replace('eval_data', JSON.stringify(keywordsWithDifficulties));

            window.location.href = ajaxUrl;
        }; 

        var isPageJustLoad = true,
            evaluatedId = null,
            indexArr = [],
            projectName = '{{ $keyword }}',
            tmp_id = '{{$tmp_id}}';

        $( "a[data-index]" ).each(function( index ) {
            indexArr.push( $( this ).attr('data-index') );
        });

        $(function () {
            var table = $('#researchTable').DataTable({
                "order": [[1, 'ms-case-desc']],
                columnDefs: [
                    {type: 'numeric-comma', targets: 0},
                    {"bSortable": false, targets: 5}
                ]
            });

            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                var target = $(e.target).attr("href");
                selectTab(target);
            });

            // keyword filter
            $('#minepc').keyup(function () {
                $('#researchTable').dataTable({"bRetrieve": true}).fnFilter($(this).val(), $(this).index(this));
            });
            $('#mineke').keyup(function () {
                $('#researchTable').dataTable({"bRetrieve": true}).fnDraw(true);
            });
            // monthly searches
            $('#min').keyup(function () {
                $('#researchTable').dataTable({"bRetrieve": true}).fnDraw(true);
            });
            $('#max').keyup(function () {
                $('#researchTable').dataTable({"bRetrieve": true}).fnDraw(true);
            });
            $('#mincpc').keyup(function () {
                $('#researchTable').dataTable({"bRetrieve": true}).fnDraw(true);
            })
            $('#maxcpc').keyup(function () {
                $('#researchTable').dataTable({"bRetrieve": true}).fnDraw(true);
            });
            $('#minlms').keyup(function () {
                $('#researchTable').dataTable({"bRetrieve": true}).fnDraw(true);
            });
            $('#maxlms').keyup(function () {
                $('#researchTable').dataTable({"bRetrieve": true}).fnDraw(true);
            });
            $('#nowordsmin').keyup(function () {
                $('#researchTable').dataTable({"bRetrieve": true}).fnDraw(true);
            });
            $('#nowordsmax').keyup(function () {
                $('#researchTable').dataTable({"bRetrieve": true}).fnDraw(true);
            });

            // load content
            $.ajax({
                url: '{{ URL::route('backend.tools.keywordresearch.saved',$keyword)}}/' + JSON.stringify(indexArr),
                type: 'get',
                dataType: 'json',
                success: function (data, textStatus, jqXHR) {
                    if (data.success) {
                        $("#savedContent").html(data.data);
                        $("#notSavedContent").html(data.data2);

                        $('#savedContent .card-box').outerHeight( 120 + savedKeywordsNumber * 40 + 440 );
                        $('#notSavedContent .card-box').outerHeight( 170 + notSavedKeywordsNumber * 40 + 440 );

                        $("#cover-tbl").hide();
                        $("#cover-eval").hide();

                        selectTab('evalTbl');
                        showLoader(false);
                    }
                },
                error: function (data) {
                    console.log('error keywordresearch.saved');
                    console.log(data);
                }
            });
        });

        function selectTab(tabName, init, keywordID, notSendRequest) {
            if (init) {
                $('#tabList li').removeClass('active');
                $('#tab-' + tabName).addClass('active');
            }

            if (tabName == 'saved') {
                $("#evalTbl").hide();
                $("#savedContentWrapper").show();
                $("#notSavedContentWrapper").hide();
                if (savedKeywordsNumber > 0) {
                    $("#savedContentWrapper .alert").hide();
                    $("#savedDetailInfo").show();
                    $("#savedContent").show();
                    if (!notSendRequest) {
                        if (keywordID) {
                            $('#saveTbl #research_'+ keywordID).trigger('click');
                        } else {
                            $('#saveTbl tr').first().trigger('click');
                        }
                    }
                } else {
                    $("#savedContentWrapper .alert").show();
                    $("#savedDetailInfo").hide();
                    $("#savedContent").hide();
                }
                $("#saveActions").hide();
            }
            else if (tabName == 'evaluated') {
                $("#evalTbl").hide();
                $("#savedContentWrapper").hide();
                $("#notSavedContentWrapper").show();
                if (notSavedKeywordsNumber > 0) {
                    $("#notSavedContentWrapper .alert").hide();
                    $("#savedDetailInfo").show();
                    $("#notSavedContent").show();
                    $("#saveActions").show();
                    if (!notSendRequest) {
                        if (keywordID) {
                            $('#ns_saveTbl #research_'+ keywordID +'_ns').trigger('click');
                        } else {
                            $('#ns_saveTbl tr').first().trigger('click');
                        }
                    }
                } else {
                    $("#notSavedContentWrapper .alert").show();
                    $("#savedDetailInfo").hide();
                    $("#notSavedContent").hide();
                }
            }
            else {
                $("#savedDetailInfo").hide();
                $("#savedContentWrapper").hide();
                $("#notSavedContentWrapper").hide();
                $("#saveActions").hide();
                $("#evalTbl").show();
            }
        }

        function showWait() {
            $("#alert-error").hide();
            $("#ajax-wait").show();
        }

        function showEvaluation(keyword, keyword_id, searches, cpc, profit, word_count, location, language, rowId, evaluated_id, isReevaluate, isUnsaved) {
            if (evaluated_id == null) {
                for (key in notSavedKeywordsArr) {
                    if (notSavedKeywordsArr[key] == keyword) {
                        selectTab('evaluated', true, key);
                        return;
                    }
                }
            }

            showLoader(true);
            var d = new Date();
            evaluatedId = evaluated_id;
            addTab(keyword, keyword_id, searches, cpc, profit, word_count, location, language, d.getTime(), rowId, isReevaluate, isUnsaved);
        }

        function closeTab(elementId) {
            // remove link
            $("#tabList").find("#newtab_" + elementId).remove();
            // remove content
            $("#tabContent").find("#" + elementId).remove();
            // select results
            $('#tabList a:first').tab('show')
        }

        function showLoader(show) {
            if (show === true) {
                $("#loader").show();
                $("#loader").addClass('in');
            } else {
                $("#loader").hide();
                $("#loader").removeClass('in');
            }
        };

        var evalActive = false;

        function addTab(displayName, keyword_id, searches, cpc, profit, word_count, location, language, elementId, rowId, isReevaluate, isUnsaved) {
            var searchKeyword = '{{ $keyword }}';
            var ajaxUrl = '{{ URL::route('backend.tools.keywordresearch.evaluate', ['ts_keyword', 'ts_keywordId','ts_searches', 'ts_cpc', 'ts_profit', 'ts_word_count', 'ts_location', 'ts_language', 'search_keyword', 'evaluated_id', 'is_reevaluate', 'is_unsaved', 'project_name', 'tmp_id'] ) }}'
                    .replace('ts_keyword', displayName)
                    .replace('ts_keywordId', keyword_id)
                    .replace('ts_searches', searches)
                    .replace('ts_cpc', cpc)
                    .replace('ts_profit', profit)
                    .replace('ts_word_count', word_count)
                    .replace('ts_location', location)
                    .replace('ts_language', language)
                    .replace('search_keyword', searchKeyword)
                    .replace('evaluated_id', evaluatedId)
                    .replace('is_reevaluate', isReevaluate)
                    .replace('is_unsaved', isUnsaved)
                    .replace('project_name', projectName)
                    .replace('tmp_id', tmp_id);

            var html = '';
            html += '<div id="content_' + elementId + '">';
            html += '<div class="text-right">';
            html += '</div>';
            html += '</div>';

            $('#saveTbl .parent-data').removeClass('parent-expand');
            $('#saveTbl .child-data').removeClass('expand');

            $('#saveActions').html("");

            // call ajax
            $.ajax({
                url: ajaxUrl,
                type: 'get',
                dataType: 'json',
                success: function (data, textStatus, jqXHR) {
                    if (data.success) {
                        $("#errorsBox").html("");
                        $("#savedDetailInfo").html("");
                        $("#savedDetailInfo").append('<div id="' + elementId + '">' + html + '</div>');
                        if (data.data.newEvaluate) {
                            notSavedKeywordsNumber++;
                            $('#content_' + elementId).append(data.data.html);
                            appendDataToUnsavedKeywordsTable(data.data);
                            notSavedKeywordsArr[data.data.unsavedResearch_id] = data.data.keyword;
                        }else{
                            $('#content_' + elementId).append(data.data.html);
                        }
                        $('#saveButtonContainer').appendTo('#saveActions');

                        if ( $('#child-data-' + evaluatedId + ' td').length > 0 && !isReevaluate){
                            evalActive = false;
                            expandChildTable(evaluatedId);
                        } else if (isReevaluate) {
                            expandChildTable(evaluatedId);
                            $('#tab-saved a').trigger('click');
                        } else {
                            evalActive = true;
                            expandChildTable(data.data.unsavedResearch_id + '_ns')
                            selectTab('evaluated', true, data.data.unsavedResearch_id, true);
                        }
                        $('#cover-eval').hide();
                        showLoader(false);

                        if (rowId) {
                            var htmlstr = "showReEvaluationConfirm(" + "'" + displayName + "'" + "," + "'" + keyword_id + "'" + "," + "'" + searches + "'" + "," + "'" + cpc + "'" + "," + "'" + profit + "'" + "," + "'" + word_count + "'" + "," + "'" + location + "'" + "," + "'" + language + "'" + "," + "'" + rowId + "'" + "," + null + ","+false+","+ data.data.newEvaluate +");return false;";
                            document.getElementById('evolution_' + rowId).innerHTML ='<a href="#" onclick="'+htmlstr+'">'+Math.round(data.data.difficulty)+'</a>';
                            keywordsWithDifficulties[displayName] = Math.round(data.data.difficulty);
                        }

                        $('.save-btn').attr('data-row-id', rowId);
                        renderGraph($("#bar"), 'bar', data.data.graphResults);
                        renderGraph($("#horizontalBar"), 'horizontalBar', data.data.graphResults);
                        renderProgressBar('progress-url', 'primary', 'Url', data.data.graphResults.meta_url);
                        renderProgressBar('progress-title', 'pink', 'Title', data.data.graphResults.meta_title);
                        renderProgressBar('progress-desc', 'warning', 'Desc', data.data.graphResults.meta_desc);
                        renderProgressBar('progress-h1', 'success', 'H1', data.data.graphResults.meta_h1);
                    }
                },
                error: function (data) {
                    showLoader(false);
                    $("#errorsBox").html("");
                    $("#errorsBox").append('<div class="alert alert-lightred alert-dismissable fade in"><h4>Whoops!</h4><ul><li>' + data.responseJSON.message + '</li></ul></div>');
                    $("#errorsBox").show();
                    closeTab(elementId);
                }
            });
        }

        function appendDataToUnsavedKeywordsTable(data) {
            var d = new Date(data.created_at.date);
            var parentRow = '<tr id="research_' + data.unsavedResearch_id + '_ns" data-id="' + data.unsavedResearch_id + '" class="parent-data"' +
                                'onclick="showEvaluation(\'' + data.keyword + '\',\'' +
                                                             data.project_name + '\',\'' +
                                                             data.searches + '\',\'' +
                                                             data.cpc + '\',\'' +
                                                             data.profit + '\',\'' +
                                                             data.word_count + '\',\'' +
                                                             data.location + '\',\'' +
                                                             data.language + '\',\'' +
                                                             '' + '\',\'' +
                                                             data.unsavedResearch_id + '_ns\',' +
                                                             'false, true); return false;">' +
                                    '</td><td>' +
                                    data.keyword +
                                    '</td><td>' +
                                    data.searches +
                                    '</td><td>$' +
                                    data.cpc +
                                    '</td><td>$' +
                                    data.profit +
                                    '</td><td>' +
                                    data.word_count +
                                    '</td><td>' +
                                    data.location +
                                    '</td><td>' +
                                    data.difficulty +
                                    '</td><td>' +
                                    (d.getMonth() + 1) + "/" + d.getDate() + "/" + d.getFullYear() +
                                    '</td><td class="text-right actions">' +
                                        '<a href="javascript:void(0);" onclick="bulkDeleteKeywords(' + data.unsavedResearch_id + ', this, true)" class="single-delete"><i class="action-icon icon-trash"></i></a>' +
                                    '</td></tr>' +
                                    '<tr id="child-data-' + data.unsavedResearch_id + '_ns" class="child-data"><td colspan="9" class="child-data-td"><div class="sub-table sub-table-open"></div></td></tr>';
            $('#ns_saveTbl').append(parentRow);
            keywordsWithDifficulties[data.keyword] = data.difficulty;
        }

        function renderGraph(selector, type, obj, option) {
            switch (type) {
                case 'horizontalBar':
                    var data = {
                        labels: [1,2,3,4,5,6,7,8,9,10],
                        datasets : [
                            { label: 'Moz Domains',backgroundColor: '#3B5998',data : obj.backlinks }
                        ]
                    };
                    var options = {
                        scales: {
                            xAxes: [{
                                gridLines: {
                                    display: false
                                },
                                stacked: true
                            }],
                            yAxes: [{
                                gridLines: {
                                    display: false
                                }
                            }],
                        },
                        responsive: true,
                        maintainAspectRatio: false,
                        tooltips: { mode : 'label' }
                    };
                    break;
                default:
                    var data = {
                        labels: [1,2,3,4,5,6,7,8,9,10],
                        datasets : [
                            { label: 'Facebook',backgroundColor: '#3B5998',data : obj.facebook_shares },
                            { label: 'Twitter',backgroundColor: '#1DA1F2',data : obj.tweets },
                            { label: 'Google+',backgroundColor: '#EC5F52',data : obj.googlePlus },
                            { label: 'Pinterest',backgroundColor: '#BD081C',data : obj.pinterest }
                        ]
                    };
                    var options = {
                        scales: {
                            xAxes: [{
                                gridLines: {
                                    display: false
                                }
                            }],
                            yAxes: [{
                                gridLines: {
                                    display: false
                                }
                            }],
                        },
                        responsive: true,
                        maintainAspectRatio: false,
                        tooltips: { mode : 'label' }
                    };
                    break;

            };
            var myBarChart = new Chart(selector, {
                type: type,
                data: data,
                options: options
            });
        }

        function renderProgressBar(selector, scheme, title, obj) {
            document.getElementById(selector).innerHTML = '<p class="font-600">' + title + ' <span class="text-' + scheme + ' pull-right">' + obj.positive * 10 + '%</span></p>' +
                    '<div class="progress progress-lg m-b-20">' +
                        '<div class="progress-bar progress-bar-' + scheme + ' progress-animated wow animated" role="progressbar" aria-valuenow="' + obj.positive * 10 + '" aria-valuemin="0" aria-valuemax="100" style="width: ' + obj.positive * 10 + '%">' +
                        '<span>Yes</span></div>' +
                    '</div>';
        }

        function expandChildTable(id) {
            $('.child-data .sub-table').removeClass('sub-table-open');
            $('.parent-data').removeClass('parent-expand');
            $('.child-data').removeClass('expand');

            $('#child-data-' + id + ' .sub-table').html("");
            $('#rankingTable').appendTo('#child-data-' + id + ' .sub-table');
            $('#research_' + id).addClass('parent-expand');
            $('#child-data-' + id).addClass('expand');
            $('#child-data-' + id + ' .sub-table').addClass('sub-table-open');
            $('#rankingTableRow').hide();
        }

        /* Create an array with the values of all the <a> texts in a column, parsed as numbers */
        $.fn.dataTableExt.order['dom-text-numeric'] = function  ( settings, col )
        {
            return this.api().column( col, {order:'index'} ).nodes().map( function ( td, i ) {
                var val = $('a', td).text() * 1;
                return isNaN(val) ? 999 : val;
            } );
        }

        /* Custom sorting function which will sorts the numerical values of the '3,246,965' type */
        $.fn.dataTableExt.oSort['ms-case-desc']  = function(x, y) {
            var realX = x == "-" ? 0 : x.replace(/,/g, "") * 1;
            var realY = y == "-" ? 0 : x.replace(/,/g, "") * 1;
            return ((realX < realY) ?  1 : ((realX > realY) ? -1 : 0));
        };

        /* Custom filtering function which will filter data with exclude keyword */
        $.fn.dataTableExt.afnFiltering.push(
            function (oSettings, aData, iDataIndex) {
                if (oSettings.nTable.id === "researchTable") {
                    var searchValue = $("#mineke").val();
                    var thisSearchValue = aData[0];
                    if (!(thisSearchValue.match(searchValue)) || searchValue == "") {
                        return true;
                    }
                    return false;
                }
                else {
                    return true;
                }
            }
        );

        /* Custom filtering function which will filter data in column four between two values */
        $.fn.dataTableExt.afnFiltering.push(
            function (oSettings, aData, iDataIndex) {
                if (oSettings.nTable.id === "researchTable") {
                    var iMin = document.getElementById('min').value * 1;
                    var iMax = document.getElementById('max').value * 1;
                    var iVersion = aData[1] == "-" ? 0 : aData[1].replace(/,/g, "") * 1;
                    if (iMin == "" && iMax == "") {
                        return true;
                    }
                    else if (iMin == "" && iVersion <= iMax) {
                        return true;
                    }
                    else if (iMin <= iVersion && "" == iMax) {
                        return true;
                    }
                    else if (iMin <= iVersion && iVersion <= iMax) {
                        return true;
                    }
                    return false;
                }
                else {
                    return true;
                }
            }
        );

        ///////////////////////cpc
        $.fn.dataTableExt.afnFiltering.push(
            function (oSettings, aData, iDataIndex) {
                if (oSettings.nTable.id === "researchTable") {
                    var iMincpc = document.getElementById('mincpc').value * 1;
                    var iMaxcpc = document.getElementById('maxcpc').value * 1;
                    var iVersioncpc = aData[2].substring(1) == "-" ? 0 : aData[2].substring(1) * 1;
                    if (iMincpc == "" && iMaxcpc == "") {
                        return true;
                    }
                    else if (iMincpc == "" && iVersioncpc <= iMaxcpc) {
                        return true;
                    }
                    else if (iMincpc <= iVersioncpc && "" == iMaxcpc) {
                        return true;
                    }
                    else if (iMincpc <= iVersioncpc && iVersioncpc <= iMaxcpc) {
                        return true;
                    }
                    return false;
                }
                else {
                    return true;
                }
            }
        );
        ///////////////////////lms
        $.fn.dataTableExt.afnFiltering.push(
            function (oSettings, aData, iDataIndex) {
                if (oSettings.nTable.id === "researchTable") {
                    var iMinlms = document.getElementById('minlms').value * 1;
                    var iMaxlms = document.getElementById('maxlms').value * 1;
                    var iVersionlms = aData[3].substring(1) == "-" ? 0 : aData[3].substring(1).replace(/,/g, "") * 1;
                    if (iMinlms == "" && iMaxlms == "") {
                        return true;
                    }
                    else if (iMinlms == "" && iVersionlms <= iMaxlms) {
                        return true;
                    }
                    else if (iMinlms <= iVersionlms && "" == iMaxlms) {
                        return true;
                    }
                    else if (iMinlms <= iVersionlms && iVersionlms <= iMaxlms) {
                        return true;
                    }
                    return false;
                }
                else {
                    return true;
                }
            }
        );

        ///////////////////////no of words
        $.fn.dataTableExt.afnFiltering.push(
            function (oSettings, aData, iDataIndex) {
                if (oSettings.nTable.id === "researchTable") {
                    var iMinwords = document.getElementById('nowordsmin').value * 1;
                    var iMaxwords = document.getElementById('nowordsmax').value * 1;
                    var iVersionwords = aData[4] == "-" ? 0 : aData[4] * 1;
                    if (iMinwords == "" && iMaxwords == "") {
                        return true;
                    }
                    else if (iMinwords == "" && iVersionwords <= iMaxwords) {
                        return true;
                    }
                    else if (iMinwords <= iVersionwords && "" == iMaxwords) {
                        return true;
                    }
                    else if (iMinwords <= iVersionwords && iVersionwords <= iMaxwords) {
                        return true;
                    }
                    return false;
                }
                else {
                    return true;
                }
            }
        );

        function deleteItem(id) {
            var ans = confirm('Are you sure?');
            var url = '{{ URL::route('backend.tools.keywordresearch.destroyAjax', ':id') }}';

            if (ans) {
                $.ajax({
                    url: url.replace(':id', id),
                    type: 'get',
                    dataType: 'json',
                    success: function (data, textStatus, jqXHR) {
                        $("#research_" + id).fadeOut();
                    }
                });
            }
        }

        /*hide and show function while click on domain availability*/
        function openModal() {
            document.getElementById('modal').style.display = 'block';
            document.getElementById('fade').style.display = 'block';
        }

        function closeModal() {
            document.getElementById('modal').style.display = 'none';
            document.getElementById('fade').style.display = 'none';
        }
        /*End Here Domain availability */

        function dCheckAll() {
            openModal();
            var table = $('#researchTable').DataTable();
            var allData = table.rows( { filter : 'applied'} ).data();
            var info = table.page.info();

            rowArr = new Array();
            for (i = info.start; i < info.end; i++) {
                rw = allData[i];
                cell = $(document.getElementById(rw[0]));
                if (!cell.hasClass('checked')) {
                    cell.html('<img src="{{ asset('assets/backend/images/success-loader.gif') }}">');
                    rowArr.push(rw[0]);
                }
            }

            var ajaxUrl = '{{ URL::route('backend.tools.keywordresearch.postDomainAvailability') }}';
            // load content
            $.ajax({
                url: ajaxUrl,
                type: 'post',
                dataType: 'json',
                data: {'keyword': rowArr},
                success: function ($domainResults) {
                    closeModal();
                    var table = $('#researchTable').DataTable();

                    for (result in $domainResults) {
                        $domainResult = $domainResults[result];
                        var html = '';

                        if($domainResult.flag==true){
                            html += '<a href="https://www.namecheap.com/domains/registration/results.aspx?domain=' + result + '.com" target="_blank"><span class="label label-success domain-margin" style="background-color: #08c!important;">.com</span></a>'
                            html += '<a href="https://www.namecheap.com/domains/registration/results.aspx?domain=' + result + '.net" target="_blank"><span class="label label-success domain-margin" style="background-color: #08c!important;">.net</span></a>'
                            html += '<a href="https://www.namecheap.com/domains/registration/results.aspx?domain=' + result + '.org" target="_blank"><span class="label label-success domain-margin" style="background-color: #08c!important;">.org</span></a>'
                        }
                        else {
                            if (!$domainResult['com']) {
                                html += '<span class="label label-danger domain-margin">.com</span>'
                            }
                            else {
                                html += '<a href="https://www.namecheap.com/domains/registration/results.aspx?domain=' + result + '.com" target="_blank"><span class="label label-success domain-margin" >.com</span></a>'
                            }
                            if (!$domainResult['net']) {
                                html += '<span class="label label-danger domain-margin">.net</span>'
                            }
                            else {
                                html += '<a href="https://www.namecheap.com/domains/registration/results.aspx?domain=' + result + '.net" target="_blank"><span class="label label-success domain-margin">.net</span></a>'
                            }
                            if (!$domainResult['org']) {
                                html += '<span class="label label-danger domain-margin">.org</span>'
                            }
                            else {
                                html += '<a href="https://www.namecheap.com/domains/registration/results.aspx?domain=' + result + '.org" target="_blank"><span class="label label-success domain-margin">.org</span></a>'
                            }
                        }

                        $(document.getElementById(result)).addClass('checked').html(html);
                    }
                }
            });
        }


        // check domain availability
        function getDomain(keyword, element) {
            var ajaxUrl = '{{ URL::route('backend.tools.keywordresearch.postDomainAvailability', ['tskeyword'] ) }}'
                    .replace('tskeyword', keyword);

            // load content
            $.ajax({
                url: ajaxUrl,
                type: 'get',
                dataType: 'json',
                data: {row: 'row'},
                success: function ($domainResult) {
                    var html = '';
                    if (!$domainResult['com']) {
                        html += '<span class="label label-danger domain-margin">.com</span>'
                    }
                    else {
                        html += '<a href="https://www.namecheap.com/domains/registration/results.aspx?domain=' + keyword + '.com" target="_blank"><span class="label label-success domain-margin">.com</span></a>'
                    }
                    if (!$domainResult['net']) {
                        html += '<span class="label label-danger domain-margin">.net</span>'
                    }
                    else {
                        html += '<a href="https://www.namecheap.com/domains/registration/results.aspx?domain=' + keyword + '.net" target="_blank"><span class="label label-success domain-margin">.net</span></a>'
                    }
                    if (!$domainResult['org']) {
                        html += '<span class="label label-danger domain-margin">.org</span>'
                    }
                    else {
                        html += '<a href="https://www.namecheap.com/domains/registration/results.aspx?domain=' + keyword + '.org" target="_blank"><span class="label label-success domain-margin">.org</span></a>'
                    }

                    element.innerHTML = html;
                }
            });
        }

        function showReEvaluationConfirm(keyword, keyword_id, searches, cpc, profit, word_count, location, language, rowId, evaluated_id, isReevaluate, isUnsaved) {
            isUnsaved = isUnsaved ? isUnsaved : false;
            if (isUnsaved) {
                for (key in notSavedKeywordsArr) {
                    if (notSavedKeywordsArr[key] == keyword) {
                        selectTab('evaluated', true, key);
                        return;
                    }
                }
            }

            bootbox.dialog({
                message: "Are you sure want to Re-Evaluate?",
                title: "Re-Evaluate confirmation!",
                buttons: {
                    success: {
                        label: "Yes",
                        className: "btn btn-default waves-effect waves-light w-100",
                        callback: function () {
                            showEvaluation(keyword, keyword_id, searches, cpc, profit, word_count, location, language, rowId, evaluated_id , isReevaluate)
                        }
                    },
                    danger: {
                        label: "No",
                        className: "btn btn-cancel waves-effect waves-light w-100",
                        callback: function () {
                            //Example.show("uh oh, look out!");
                        }
                    }
                }
            })
        }
    </script>
@stop
