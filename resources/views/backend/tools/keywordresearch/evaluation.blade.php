<div id="error{{ $uniqueId }}" class="alert alert-danger alert-dismissable fade in" style="display: none">
    <h4>Whoops!</h4>

    <ul>
        <li id="errMsg{{ $uniqueId }}">Fail</li>
    </ul>
</div>
<div id="success{{ $uniqueId }}" style="display: none" class="alert alert-success alert-dismissable fade in">
    <h4>Success!</h4>
    <ul>
        <li>Data saved successfully</li>
    </ul>
</div>

<div class="row">
    <div class="col-md-4 col-sm-12">
        <div class="card-box mt-20" style="height: 329px">
            <!-- tile header -->
            <h4 class="m-t-0 header-title">Keyword Difficulty</h4>
            <!-- /tile header -->

            <!-- tile body -->
            <div class="tile-body text-center">
                <h4><span id="gauge-text-{{ $uniqueId }}"></span></h4>
                <div class="easypiechart animate" data-percent="{{ round($difficulty, 2) }}" data-size="180"
                     data-scale-color="false"
                     data-line-cap="butt" data-line-width="20" style="width:180px;">
                    <div class="pie-percent custom-font" style="line-height: 170px;">
                        <span>{{ round($difficulty, 2) }}</span></div>
                </div>
                <h4>{{ $keyword }}</h4>

                {{--
                <div id="gauge-text"></div>
                <canvas id="gauge_{{ $keyword }}"></canvas>
                <div style="text-align: center; font-size: 19px; width: 300px; font-weight: bold;">
                    <div id="gauge-value"></div>
                </div>
                --}}
            </div>
            <!-- /tile body -->
        </div>
    </div>

    <div class="col-md-8 col-sm-12">
        <div class="card-box mt-20" style="height:329px; overflow:hidden;">
            <h4 class="m-t-0 header-title">Keyword Search Trend</h4>
            <div>
                <span style="display: inline-block;width:10px;height:10px;background:rgb(33, 150, 243);border-radius:50%"></span>
                {{$keyword}}
            </div>
            <div style="position:absolute; background:#fff; height:260px; width:20px; bottom:45px; right:16px;"></div>

            <div style="overflow: hidden;height:329px">
                <iframe src="/tools/keywordresearch/google/trends/{{$keyword}}"
                        scrolling="no" style="border:none; margin:0;position:relative;top:-110px;margin-left:-2%" height="360" width="105%">
                </iframe>
            </div>

            <div style="position:relative; background:#fff; height:20px; width:120%; bottom:45px;z-index: -1"></div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-4">
        <div class="card-box pb-10">
            <h4 class="text-dark  header-title m-t-0">Social Presence</h4>
            <div class="widget-chart text-center">
                <canvas id="bar{{ $uniqueId }}" height="274"></canvas>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="card-box pb-10">
            <h4 class="text-dark  header-title m-t-0">Domain Strength</h4>
            <div class="widget-chart text-center">
                <canvas id="horizontalBar{{ $uniqueId }}" height="274"></canvas>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="card-box">
            <div id="progress-url{{ $uniqueId }}"></div>
            <div id="progress-title{{ $uniqueId }}"></div>
            <div id="progress-desc{{ $uniqueId }}"></div>
            <div id="progress-h1{{ $uniqueId }}"></div>
        </div>
    </div>
</div>

<div id="saveButtonContainer">
    @if(Auth::user()->can('keyword_save'))
        <div id="saveButton{{$uniqueId}}">
            <form class="m-0">
                <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="keyword" id="keyword" value="{{ $keyword }}">
                <input type="hidden" name="searches" id="searches" value="{{ $searches }}">
                <input type="hidden" name="cpc" id="cpc" value="{{ $cpc }}">
                <input type="hidden" name="profit" id="profit" value="{{ $profit }}">
                <input type="hidden" name="word_count" id="word_count"
                       value="{{ $word_count }}">
                <input type="hidden" name="location" id="location" value="{{ $location }}">
                <input type="hidden" name="difficulty" id="difficulty" value="?">
                <input type="hidden" value="{{$research_id}}" id="id{{$uniqueId}}">
                @if($save==1)
                    {{--<div class="alert alert-success" disabled="disabled">Saved</div>--}}
                    {{--<button type="button" id="btn-resave{{ $uniqueId }}" class="btn btn-success">Re-save</button>--}}
                @else
                @endif
            </form>
        </div>

        <input type="button" id="submit{{ $uniqueId }}" data-row-id="" class="save-btn btn btn-default waves-effect waves-light w-100" value="Save">

        {{--<button type="button" id="btn-resave{{ $uniqueId }}" data-row-id="" class="save-btn btn btn-default waves-effect waves-light w-100">Re-save</button>--}}
    @endif
</div>


<table id="rankingTable" class="table table-bordered" style="background:#fff;">
    <thead>
    <tr>
        <th>#</th>
        <th class="text-left"><span data-toggle="tooltip" data-placement="top" title="Top Ten URLS on Google Search Engine Results">Google Top Ten Ranking</span></th>
        <th class="text-center"><span data-toggle="tooltip" data-placement="top" title="Moz Page Authority">PA</span></th>
        <th class="text-center"><span data-toggle="tooltip" data-placement="top" title="Moz Domain Authority">DA</span></th>
        <th class="text-center"><span data-toggle="tooltip" data-placement="top" title="External Links">Links</span></th>
        <th class="text-center"><span data-toggle="tooltip" data-placement="top" title="MozRank">MozRank</span></th>
        <th class="text-center"><span data-toggle="tooltip" data-placement="top" title="Pinterest shares">PS</span></th>
        <th class="text-center"><span data-toggle="tooltip" data-placement="top" title="FB Comments">FBC</span></th>
        <th class="text-center"><span data-toggle="tooltip" data-placement="top" title="FB Shares">FBS</span></th>
        <th class="text-center"><span data-toggle="tooltip" data-placement="top" title="Tweets">Tweets</span></th>
        <th class="text-center"><span data-toggle="tooltip" data-placement="top" title="Keyword Found in URL">Url</span></th>
        <th class="text-center"><span data-toggle="tooltip" data-placement="top" title="Keyword Found in Title">Title</span></th>
        <th class="text-center"><span data-toggle="tooltip" data-placement="top" title="Keyword Found in Description">Desc</span></th>
        <th class="text-center"><span data-toggle="tooltip" data-placement="top" title="Keyword Found in Header Tag">H1</span></th>
    </tr>
    </thead>
    <tbody>
    @if($googleResults)
        @foreach($googleResults as $idx => $googlePage)
            <tr>
                <td class="text-center">{{ ($idx + 1) }}</td>
                <td class="text-left"><a href="{{ $googlePage['url'] }}" target="_blank">{{ str_limit($googlePage['url'], $limit = 35, $end = '...') }}</a></td>
                <td class="text-center">{{ (isset($googlePage['page_authority']) ? round($googlePage['page_authority'],2) : '') }}</td>
                <td class="text-center">{{ (isset($googlePage['domain_authority']) ? round($googlePage['domain_authority'],2) : '') }}</td>
                <td class="text-center">{{ (isset($googlePage['backlinks']) ? round($googlePage['backlinks'], 0) : '') }}</td>
                <td class="text-center">{{ (isset($googlePage['score']) ? round($googlePage['score'], 0) : '') }}</td>
                <td class="text-center">{{ (isset($googlePage['pinterest']) ? $googlePage['pinterest'] : '') }}</td>
                <td class="text-center">{{ (isset($googlePage['googlePlus']) ? $googlePage['googlePlus'] : '') }}</td>
                <td class="text-center">{{ (isset($googlePage['facebook_shares']) ? $googlePage['facebook_shares'] : '') }}</td>
                <td class="text-center">{{ (isset($googlePage['tweets']) ? $googlePage['tweets'] : '') }}</td>
                <td class="text-center">{{ (isset($googlePage['meta']['url']) && $googlePage['meta']['url']==true ? 'YES' : 'NO') }}</td>
                <td class="text-center">{{ (isset($googlePage['meta']['title'])  && $googlePage['meta']['title']==true ? 'YES' : 'NO') }}</td>
                <td class="text-center">{{ (isset($googlePage['meta']['desc'])  && $googlePage['meta']['desc']==true ? 'YES' : 'NO') }}</td>
                <td class="text-center">{{ (isset($googlePage['meta']['h1'])  && $googlePage['meta']['h1']==true ? 'YES' : 'NO') }}</td>
            </tr>
        @endforeach
    @else
        <tr>
            <td colspan="15" class="alert alert-warning">
                <h5>Sorry, we could not process any google search results. Please try again later</h5>
            </td>
        </tr>
    @endif
    </tbody>
</table>


<!-- <script src="{{ asset('assets/backend/js/vendor/gaugejs/gauge.min.js') }}"></script> -->
<script>

    if('{{$save}}' == 0)
        $("#btn-resave"+'{{ $uniqueId }}').hide();

    $('#submit' + '{{ $uniqueId }}').click(function () {
        var token = '{{ csrf_token() }}';
        var keyword = '{{$keyword}}';
        var searches = '{{$searches}}';
        var cpc = '{{$cpc}}';
        var profit = '{{$profit}}';
        var word_count = '{{$word_count}}';
        var location = '{{$location}}';
        var language = '{{$language}}';
        var difficulty = '{{$difficulty}}';
        var googleResults = '<?php echo json_encode($googleResults); ?>';

        var ajaxData =  {
                _token: token,
                search_keyword: '{{$search_keyword}}',
                keyword: '{{$keyword}}',
                searches: searches,
                cpc: cpc,
                profit: profit,
                word_count: word_count,
                location: location,
                language: language,
                difficulty: difficulty,
                // keyword_id: keyword_id,
                googleResults:googleResults,
                tmp_id: tmp_id,
            };

        $.ajax({
            url: '{{ URL::route('backend.tools.keywordresearch.store') }}',
            type: 'post',
            dataType: 'json',
            data: ajaxData,
            success: function (data, textStatus, jqXHR) {
                if (data.success) {
                    // show alert
                    evalActive = false;
                    savedKeywordsNumber++;
                    notSavedKeywordsNumber--;

                    $("#ns_saveTbl .parent-data.parent-expand").appendTo('#saveTbl');
                    $("#ns_saveTbl .child-data.expand").appendTo('#saveTbl');

                    $("#savedContent").show();
                    $("#notSavedContent").hide();

                    $('#tab-saved a').tab('show');
                    $('#savedContent .card-box').outerHeight( $('#savedContent .card-box').outerHeight() + 40 );
                }
                else {
                    $('#error' + '{{ $uniqueId }}').show();
                    $('#errMsg' + '{{ $uniqueId }}').html(data.message);
                }
            },
            error: function (data) {
                $('#error' + '{{ $uniqueId }}').show();
                $('#errMsg' + '{{ $uniqueId }}').html(data.responseJSON.message);
            }
        });
    });

    $('#btn-resave'+'{{ $uniqueId }}').click(function(){
        var id=$("#id"+'{{$uniqueId}}').val();
        var keyword = '{{$keyword}}';
        var searches = '{{$searches}}';
        var cpc = '{{$cpc}}';
        var profit = '{{$profit}}';
        var word_count = '{{$word_count}}';
        var location = '{{$location}}';
        var difficulty = '{{$difficulty}}';
        var googleResults = '<?php echo json_encode($googleResults); ?>';
        $.ajax({
            url: '{{ URL::route('backend.tools.keywordresearch.updateKeyword') }}',
            type: 'post',
            dataType: 'json',
            data: {
                id:id,
                search_keyword: '{{$search_keyword}}',
                keyword: '{{$keyword}}',
                searches: searches,
                cpc: cpc,
                profit: profit,
                word_count: word_count,
                location: location,
                difficulty: difficulty,
                // keyword_id: keyword_id,
                googleResults:googleResults,
            },
            success: function (data, textStatus, jqXHR) {
                if (data.success) {
                    // show alert
                    $("#saveButton" + '{{ $uniqueId }}').hide();
                    $("#btn-resave" + '{{ $uniqueId }}').show();
                    $("#success" + '{{ $uniqueId }}').show();
                    // add to table
                    $('#saveTbl').append('<tr id="research_' + data.data.id + '"><td class="no-sort chk-all-th" style="width: 20px"><input type="checkbox" value="' + data.data.id + '">' +
                            '</td><td>' +
                            keyword +
                            '</td><td>' +
                            searches +
                            '</td><td>$' +
                            cpc +
                            '</td><td>$' +
                            profit +
                            '</td><td>' +
                            word_count +
                            '</td><td>' +
                            location +
                            '</td><td>' +
                            difficulty +
                            '</td><td>' +
                            data.data.created_at);

                    $('.dataTables_empty').parent().remove();

                }
                else {
                    $('#error' + '{{ $uniqueId }}').show();
                    $('#errMsg' + '{{ $uniqueId }}').html(data.message);

                }
            }
        });
    });


</script>

<div class="uniqueId" data-target="{{ $uniqueId }}" hidden></div>

<script type="text/javascript">
    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
        $('.easypiechart').easyPieChart({
            //your configuration goes here
            barColor: function (percent) {
                return (percent >= 0 && percent <= 30 ? '#5fbeaa' : percent > 30 && percent <= 60 ? '#36a2eb' : '#ff6384');
            }
        });
        var difficulty = '{{ $difficulty }}';
        var el = $('#gauge-text-{{ $uniqueId }}');

        if (difficulty >= 0 && difficulty <= 10) {
            el.html('No competition');
        }
        else if (difficulty >= 11 && difficulty <= 20) {
            el.html('Extremely low competition');
        }
        else if (difficulty >= 21 && difficulty <= 30) {
            el.html('Low competition');
        }
        else if (difficulty >= 31 && difficulty <= 40) {
            el.html('Moderate Competition');
        }
        else if (difficulty >= 41 && difficulty <= 50) {
            el.html('Somewhat High Competition');
        }
        else if (difficulty >= 51 && difficulty <= 65) {
            el.html('Very High Competition');
        }
        else {
            el.html('Do not even think about it!');
        }
    });


</script>
