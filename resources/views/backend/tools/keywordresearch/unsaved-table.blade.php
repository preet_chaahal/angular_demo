<div class="row">
    <div class="col-md-12">
        <div class="card-box">
            <div class="pull-left">
                <h4 class="m-t-0 header-title"><b>Search Engine Results Analysis</b></h4>
                <p class="text-muted m-b-20 font-13">
                    Previous evaluated but not saved keywords.
                </p>
            </div>
            <div class="table-container">
                <table class="table saveTbl">
                    <thead style="white-space: nowrap">
                        <tr>
                            <th>
                                Keyword
                            </th>
                            <th>
                                Monthly Searches
                            </th>
                            <th>
                                Adwords CPC
                            </th>
                            <th>
                                Estimated Profit
                            </th>
                            <th>
                                Word Count
                            </th>
                            <th>
                                Location
                            </th>
                            <th>
                                Keyword Difficulty
                            </th>
                            <th>
                                Created Date
                            </th>
                            <th class="hidden">
                                Actions
                            </th>
                        </tr>
                    </thead>
                    <tbody id="ns_saveTbl">
                        @foreach($saved as $key=>$research)
                            <tr id="research_{{ $research->id }}_ns" data-id="{{ $research->id }}" class="parent-data" 
                                onclick="showEvaluation('{{ $research->keyword }}',
                                                        '{{ $research->project_name }}',
                                                        '{{ $research->monthly_searches }}',
                                                        '{{ $research->keyword_cpc }}',
                                                        '{{ $research->estimated_profile }}',
                                                        '{{ $research->word_count }}',
                                                        '{{ $research->location }}',
                                                        '{{ $research->language }}',
                                                        '',
                                                        '{{$research->id}}_ns',
                                                        false,
                                                        true);
                                                    return false;">
                                <td>
                                    {{ $research->keyword }}
                                </td>
                                <td>
                                    {{ number_format($research->monthly_searches, 0, '', ',') }}
                                </td>
                                <td>
                                    ${{ $research->keyword_cpc}}
                                </td>
                                <td>
                                    ${{ number_format($research->estimated_profile, 0, '', ',') }}
                                </td>
                                <td>
                                    {{ $research->word_count}}
                                </td>
                                <td>
                                    {{ $research->location}}
                                </td>
                                <td>
                                    {{ $research->difficulty}}
                                </td>
                                <td>
                                    {{ $research->created_at }}
                                </td>
                                <td class="text-right actions">
                                    <a href="javascript:void(0);" onclick="bulkDeleteKeywords({{$research->id}}, this, true)" class="single-delete">
                                        <i class="action-icon icon-trash"></i>
                                    </a>
                                </td>
                            </tr>
                            <tr id="child-data-{{$research->id}}_ns" class="child-data">
                                <td colspan="9" class="child-data-td">
                                    <div class="sub-table">
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var notSavedKeywordsArr = {!! json_encode($keywordsArr) !!};
    var notSavedKeywordsNumber = {!! count($saved) !!};
</script>
