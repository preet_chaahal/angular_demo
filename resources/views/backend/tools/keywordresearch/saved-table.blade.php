<div class="row">
    <div class="col-md-12">
        <div class="card-box">
            <div class="pull-left">
                <h4 class="m-t-0 header-title"><b>Search Engine Results Analysis</b></h4>
                <p class="text-muted m-b-20 font-13">
                    Previous saved and evaluated keywords.
                </p>
            </div>
            <div class="table-container">
                <table class="table saveTbl">
                    <thead style="white-space: nowrap">
                        <tr>
                            <th>
                                Keyword
                            </th>
                            <th>
                                Monthly Searches
                            </th>
                            <th>
                                Adwords CPC
                            </th>
                            <th>
                                Estimated Profit
                            </th>
                            <th>
                                Word Count
                            </th>
                            <th>
                                Location
                            </th>
                            <th>
                                Keyword Difficulty
                            </th>
                            <th>
                                Created Date
                            </th>
                            <th>
                                Actions
                            </th>
                            {{--<th></th>--}}
                        </tr>
                    </thead>
                    <tbody id="saveTbl">
                        @foreach($saved as $research)
                            <tr id="research_{{ $research->id }}" data-id="{{ $research->id }}" class="parent-data" 
                                onclick="showEvaluation('{{ $research->keyword }}',
                                                        '{{ $research->keyword_search_id }}',
                                                        '{{ $research->monthly_searches }}',
                                                        '{{ $research->keyword_cpc }}',
                                                        '{{ $research->estimated_profile }}',
                                                        '{{ $research->word_count }}',
                                                        '{{ $research->location }}',
                                                        '{{ $research->language }}',
                                                        '{{ $research->keyIndex }}',
                                                        '{{ $research->id }}',
                                                        false,
                                                        false);
                                                    return false;">
                                <td>
                                    {{ $research->keyword }}
                                </td>
                                <td>
                                    {{ number_format($research->monthly_searches, 0, '', ',') }}
                                </td>
                                <td>
                                    ${{ $research->keyword_cpc}}
                                </td>
                                <td>
                                    ${{ number_format($research->estimated_profile, 0, '', ',') }}
                                </td>
                                <td>
                                    {{ $research->word_count}}
                                </td>
                                <td>
                                    {{ $research->location}}
                                </td>
                                <td>
                                    {{ $research->difficulty}}
                                </td>
                                <td>
                                    {{ $research->created_at->format('m/d/Y') }}
                                </td>
                                <td class="text-right actions">
                                    <a href="javascript:void(0);" onclick="bulkDeleteKeywords({{$research->id}}, this)" class="single-delete">
                                        <i class="action-icon icon-trash"></i>
                                    </a>
                                </td>
                                {{--<td>--}}
                                    {{--<a href="{{ URL::route('backend.tools.keywordresearch.destroy', $research->id) }}" class="btn btn-danger btn-xs" onclick="deleteItem('{{ $research->id }}'); return false;"><i class="fa fa-trash-o"></i></a>--}}
                                {{--</td>--}}
                            </tr>
                            <tr id="child-data-{{$research->id}}" class="child-data">
                                <td colspan="9" class="child-data-td">
                                    <div class="sub-table">
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

{{--<div class="form-group">
    <!-- <a href="" class="btn btn-danger"> Delete </a> -->
    <a href="javascript:void(0);" class="delete-btn btn btn-danger">Delete</a>
</div>--}}
<script type="text/javascript">
    $('#saveTbl tbody > tr:nth-child(1)').addClass('parent-expand');
    $('#saveTbl tbody > tr:nth-child(2)').addClass('expand');

    var savedKeywordsNumber = {!! count($saved) !!};

    function rebindParentClick() {
        $('.parent-data').off('click');
        $('.parent-data').on('click', function () {
            var isExpanded = $(this).hasClass('parent-expand');
            var id = $(this).attr('data-id');
            $(this).removeClass('parent-expand');
            $('#child-data-' + id).removeClass('expand');

            if (!isExpanded) {
                $('.parent-data').removeClass('parent-expand');
                $('.child-data').removeClass('expand');
                $('.child-data .sub-table').removeClass('sub-table-open');
                $(this).addClass('parent-expand');
                $('#child-data-' + id).addClass('expand');
                $('#child-data-' + id + ' .sub-table').addClass('sub-table-open');
            }
        });
    };

    function rebindActionsClick() {
        $('.actions a').off('click');
        $('.actions a').on('click', function(event){
            event.stopPropagation();
        });
    };

    rebindParentClick();
    rebindActionsClick();

    function bulkDeleteKeywords(id, element, isJustSaved) {

        var isResearachUnsaved = $(element).parent().parent().parent().attr('id') == 'ns_saveTbl' ? true : false;
        isJustSaved = isJustSaved !== undefined ? isJustSaved : false;

        bootbox.dialog({
            message: "Are you sure you want to delete keyword?",
            title: "Delete confirmation!",
            buttons: {
                success: {
                    label: "Yes",
                    className: "btn btn-danger waves-effect waves-light w-100",
                    callback: function () {
                        $.ajax({
                            url: '{{ URL::route('backend.tools.keywordresearch.destroyResearach') }}',
                            data: {'id': id, 'isJustSaved': isJustSaved, 'isResearachUnsaved': isResearachUnsaved},
                            type: 'get',
                            dataType: 'json',
                            success: function (result) {
                                if (isResearachUnsaved) {
                                    notSavedKeywordsNumber--;
                                    $('#research_'+ id +'_ns').remove();
                                    $('#child-data-'+ id +'_ns').remove();
                                    selectTab('evaluated');
                                }
                                else {
                                    savedKeywordsNumber--;
                                    $( "#research_"+ id).remove();
                                    $( "#child-data-"+ id).remove();
                                    selectTab('saved');
                                }
                                bootbox.hideAll();
                            }
                        })

                        return true;
                    }
                },
                danger: {
                    label: "No",
                    className: "btn btn-default waves-effect waves-light w-100",
                    callback: function () {
                        //Example.show("uh oh, look out!");
                    }
                },
            }
        });
    }
</script>
