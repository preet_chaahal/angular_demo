@extends('layouts.backend.master')

@section('style')
    <link rel="stylesheet" href="{{ asset('assets/backend/css/vendor/selectize.default.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/loaders.css') }}">
    <script type="text/javascript" src="{{ asset('assets/backend/js/chart.min.js') }}"></script>

    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.5/angular.min.js"></script>
    <script type="text/javascript" src="{{ asset('assets/backend/js/angular-chart.min.js') }}"></script>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/2.5.0/ui-bootstrap-tpls.min.js"></script>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/easy-pie-chart/2.1.6/angular.easypiechart.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-charts/0.2.7/angular-charts.min.js"></script>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.5/angular-animate.min.js"></script>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/angular-sanitize/1.6.7/angular-sanitize.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-morris/1.3.0/angular-morris.min.js"></script>

    <script type="text/javascript" src="{{ asset('assets/backend/js/search.js') }}"></script>
@stop

@section('content')
    <div id="fade"></div>

    @include('layouts.backend.partials.messages')


    <div ng-app="keywordRevealer">
        <div ng-controller="keywordResearch">

            <div ng-controller="ModalCtrl as $ctrl">

                <!-- Delete from SavedSearches Confirm Modal -->
                <script type="text/ng-template" id="myModalContent.html">
                    <div class="bootbox-ang">
                        <div class="modal-header">
                            <button type="button" class="bootbox-close-button close" ng-click="$ctrl.cancel()">×
                            </button>
                            <h4 class="modal-title">
                                <span ng-if="!$ctrl.reEvaluating">Delete confirmation!</span>
                                <span ng-if="$ctrl.reEvaluating">RE-EVALUATE CONFIRMATION!</span>
                            </h4>
                        </div>
                        <div class="modal-body" id="modal-body">
                            <div class="bootbox-body">
                                <span ng-if="!$ctrl.reEvaluating">Are you sure you want to perform delete?</span>
                                <span ng-if="$ctrl.reEvaluating">Are you sure want to Re-Evaluate?</span>
                                <div ng-if="$ctrl.reEvaluating && $ctrl.notSavedKeyword">
                                    <div class="alert alert-info mb-0 mt-20">
                                        Please save evaluated keyword first to have this option enabled
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <div ng-if="!$ctrl.reEvaluating">
                                <button data-bb-handler="success" type="button" ng-click="$ctrl.ok()"
                                        ng-disabled="$ctrl.deleting"
                                        class="btn btn btn-danger waves-effect waves-light w-100">
                                    <span ng-if="!$ctrl.deleting">Yes</span>
                                    <img src="/assets/backend/images/loading-icon.gif" ng-if="$ctrl.deleting">
                                </button>
                                <button data-bb-handler="danger" type="button" ng-click="$ctrl.cancel()"
                                        class="btn btn btn-default waves-effect waves-light w-100">No
                                </button>
                            </div>
                            <div ng-if="$ctrl.reEvaluating">
                                <button data-bb-handler="success" type="button" ng-click="$ctrl.ok()"
                                        class="btn btn btn-default waves-effect waves-light w-100">Yes
                                </button>
                                <button data-bb-handler="danger" type="button" ng-click="$ctrl.cancel()"
                                        class="btn btn btn-cancel waves-effect waves-light w-100">No
                                </button>
                                <button data-bb-handler="danger" type="button" ng-click="$ctrl.loadSaved()"
                                        class="btn btn btn-cancel waves-effect waves-light w-100">Load Saved
                                </button>
                            </div>
                        </div>
                    </div>
                </script>


                <div id="mainContainer">

                    <div class="row">

                        <div class="col-md-12">
                            <!-- tile -->
                            <div class="card-box" style="background:transparent;padding:0">
                                <div style="padding:20px;padding-bottom:0">
                                    <div id="tabList">
                                        <ul class="nav nav-tabs tabs m--20 mb-0" style="width:auto;">
                                            <li class="tab active" id="newSearchBtn"><a href="#newSearch"
                                                                                        data-toggle="tab"
                                                                                        aria-expanded="false">Keyword
                                                    Search</a></li>
                                            <li class="tab"><a href="#savedSearches" data-toggle="tab"
                                                               aria-expanded="false">Saved Searches</a></li>
                                            <li class="tab"><a href="#todaySearch" data-toggle="tab"
                                                               aria-expanded="false">Today's Searches</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="tab-content" style="box-shadow:none">


                                    <div class="tab-pane active" id="newSearch" style="padding:20px">
                                        <div class="row" style="background:#fff;margin:-20px;padding:10px">
                                            <div class="col-md-12" style="padding-bottom: 10px;">
                                                <div class="mb-0" style="padding:10px 0 0">
                                                    <div class="row">
                                                        <form ng-cloak>

                                                            <div class="col-xs-12 col-md-5 mb-0">
                                                                <div class="form-group mb-20">
                                                                    <label class="control-label" for="keyword"
                                                                           ng-if="!importKeyword">Please Enter a Keyword
                                                                        <span ng-if="userCanImportKeywords">
                                                                or <a href="#"
                                                                      ng-click="handleImportKeyword();$parent.search.searchOn=null"
                                                                      class="span5">Import Keywords</a>
                                                            </span>
                                                                    </label>
                                                                    <label class="control-label" for="areax_import"
                                                                           ng-if="importKeyword">Please Import Keywords
                                                                        or
                                                                        <a href="#"
                                                                           ng-click="handleImportKeyword();$parent.search.searchOn=null"
                                                                           class="span5">Enter single Keyword</a>
                                                                    </label>
                                                                    <input type="text"
                                                                           ng-keyup="$event.keyCode == 13 && $parent.search.searchOn && !search.searching ? newSearch() : ''"
                                                                           autocomplete="false"
                                                                           class="form-control no-animate" autofocus
                                                                           ng-model="$parent.search.searchOn"
                                                                           ng-style="$parent.search.searchOn.length > 60 ? {'border-color':'red'}:{}"
                                                                           ng-maxlength="60" ng-if="!importKeyword"
                                                                           name="keyword"
                                                                           placeholder="Enter the keyword">
                                                                    <textarea elastic-textarea
                                                                              ng-style="search.searchOnLines.length > 700 ? {'border-color':'red'}:{}"
                                                                              id="areax_import"
                                                                              ng-model="$parent.search.searchOn"
                                                                              ng-if="importKeyword && userCanImportKeywords"
                                                                              ng-keyup="importLines($event)"
                                                                              class="form-control no-animate" rows="6"
                                                                              required name="keyword" autofocus
                                                                              placeholder="Paste your keyword list here, one or more keyword per line... "
                                                                              style="overflow:hidden;resize:none"></textarea>
                                                                    <span ng-if="importKeyword"
                                                                          ng-style="search.searchOnLines.length > 700 ? {'color':'red','font-weight':'bold'}:{}"
                                                                          class="pull-right mt-10">
                                                            [[search.searchOnLines.length]] / 700
                                                        </span>
                                                                    <span ng-if="search.searchOnLines.length > 700"
                                                                          class="pull-left"
                                                                          style="color:red;font-size:11px;">
                                                            You have enter more than <b>700</b> lines
                                                        </span>
                                                                </div>
                                                            </div>

                                                            <div class="col-xs-12 col-md-7">
                                                                <div class="row">

                                                                    <div class="col-xs-6 col-sm-5">
                                                                        <div class="form-group">
                                                                            <label class="control-label" for="tld">Search
                                                                                Location</label>
                                                                            <select class="selectize tld-selectize searchSelection"
                                                                                    data-target="location" id="tld"
                                                                                    name="tld"></select>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-xs-6 col-sm-5">
                                                                        <div class="form-group">
                                                                            <label class="control-label" for="lang">Search
                                                                                Language</label>
                                                                            <select class="selectize lang-selectize searchSelection"
                                                                                    data-target="languages" id="lang"
                                                                                    name="lang"></select>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-xs-12 col-sm-2">
                                                                        <div class="form-group mb-0"
                                                                             style="margin-top:26px;">
                                                                            <div class="">
                                                                                <button type="button"
                                                                                        {{--ng-style="search.searching?{'width':'100px'}:{}"--}} ng-disabled="(!importKeyword && !search.searchOn) || (importKeyword && search.searchOnLines.length == 0) || search.searching || search.searchOnLines.length > 700"
                                                                                        ng-click="newSearch()"
                                                                                        class="btn btn-large btn-primary btn-block">
                                                                                    <span ng-if="!importKeyword && !search.searching">Search</span>
                                                                                    <span ng-if="importKeyword && !search.searching">Search</span>
                                                                                    <img src="/assets/backend/images/loading-icon.gif"
                                                                                         ng-if="search.searching">
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </form>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>

                                        <div style="margin:40px -20px 0;" ng-cloak ng-if="researchData">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="searching">
                                                        <div class="searching-tabs">
                                                            <div class="toggle-tabs"
                                                                 ng-repeat="e in tabs track by $index"
                                                                 ng-class="activePanelForSearchResults==$index?'active':''"
                                                                 uib-tooltip=[[e.keyword]] ng-click="changeTab($index)">
                                                                 <input type="hidden" id="[[e.keyword.split(' ').join('')]]"  ng-click="changeTab($index)"/>
                                                                <div ng-if="$index == 0 ">[[e.title]]</div>
                                                                <div ng-if="$index != 0">
                                                                    [[e.keyword.length > 11 ? e.keyword.slice(0,8)+' ..'
                                                                    : e.keyword]]
                                                                    <span class="item-options">
                                                            <span class="badge">[[e.difficulty]]</span>
                                                            <span ng-click="removeEvalauatedKeyword($index)"
                                                                  class="glyphicon glyphicon-remove remove"></span>
                                                        </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="searching-content">
                                                            <div ng-repeat="e in tabs track by $index">
                                                                <div ng-if="$index==0 && activePanelForSearchResults==$index">
                                                                    <div revearler-search></div>
                                                                </div>
                                                                <div ng-if="$index !=0 && activePanelForSearchResults==$index">
                                                                    <div evaluate-item></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-pane" style="padding: 20px;background: #fff" id="savedSearches"
                                         ng-cloak>
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="pull-left">
                                                    <h4 class="m-t-0 header-title"><b>Recent Keyword Research</b></h4>
                                                    <p class="text-muted m-b-20 font-13">
                                                        Previous saved and evaluated keywords.
                                                    </p>
                                                </div>
                                                <div id="actions" class="input-group pull-right">
                                                    <a href="{{ URL::route('backend.tools.keywordresearch.download') }}"
                                                       class="btn btn-default waves-effect waves-light pull-right ml-20">Download
                                                        CSV
                                                        <span class="btn-label btn-label-right"><i
                                                                    class="fa fa-file-excel-o"></i></span>
                                                    </a>
                                                </div>

                                                <div class="table-container" ng-if="savedSearches.length > 0">
                                                    <table class="table">
                                                        <thead>
                                                        <tr>
                                                            <th>Research</th>
                                                            <th class="text-right">Num of. Saved Keywords</th>
                                                            <th class="text-right">Best Difficulty</th>
                                                            <th class="text-right">Actions</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr ng-repeat="x in savedSearches track by $index"
                                                            ng-class="{'-parentexpand':currentOpenSubTable==x.index }"
                                                            ng-click="openSubTable(x)"
                                                            id="parent_[[x.id]]">

                                                            <td ng-if="!x.child">[[
                                                                handleSingleKeywordOrImported(x.keyword) ]]
                                                            </td>
                                                            <td ng-if="!x.child" class="text-right">[[ x.research.length
                                                                ]]
                                                            </td>
                                                            <td ng-if="!x.child" class="text-right best-difficulty">
                                                                [[ x.bestDifficulity ? x.bestDifficulity+'%' : 'N/A']]
                                                            </td>
                                                            <td ng-if="!x.child" class="text-right actions">
                                                                <span class="table-option"><i
                                                                            class="action-icon icon-arrow-down-circle ng-scope"></i></span>
                                                                <span class="table-option"
                                                                      ng-click="searchOnSavedKeyword(x);$event.stopPropagation()">
                                                            <img style="width:14px;"
                                                                 src="/assets/backend/images/loading-icon.gif"
                                                                 ng-if="x.getSearched">
                                                            <i ng-if="!x.getSearched"
                                                               class="action-icon icon-arrow-right-circle"></i>
                                                        </span>
                                                                <span class="table-option"
                                                                      ng-click="$ctrl.open(x,$index);$event.stopPropagation()">
                                                            <i class="action-icon icon-trash"></i>
                                                        </span>
                                                            </td>
                                                            <td colspan="4" class="child-data-td child-data expand"
                                                                ng-if="x.child && currentOpenSubTable == x.childOf && x.firstChild">
                                                                <div class="sub-table sub-table-open">
                                                                    <table class="table saveTbl">
                                                                        <thead>
                                                                        <tr role="row">
                                                                            <th class>Date</th>
                                                                            <th>Keyword</th>
                                                                            <th class="text-right">Monthly Searches</th>
                                                                            <th class="text-right">Adwords CPC</th>
                                                                            <th class="text-right">Estimated Profit</th>
                                                                            <th class="text-right">Word Count</th>
                                                                            <th>Location</th>
                                                                            <th class="text-right">Keyword Difficulty
                                                                            </th>
                                                                            <th class="text-right">Action</th>
                                                                        </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                        <tr ng-repeat="y in savedSearches[x.childOf].research track by $index" id="child_[[y.id]]">
                                                                            <td>[[ y.created_at ]]</td>
                                                                            <td>[[ y.keyword ]]</td>
                                                                            <td class="text-right">[[
                                                                                handleComma(y.monthly_searches,2) ]]
                                                                            </td>
                                                                            <td class="text-right">$[[
                                                                                handleComma(y.keyword_cpc,2) ]]
                                                                            </td>
                                                                            <td class="text-right">$[[
                                                                                handleComma(y.estimated_profile,2) ]]
                                                                            </td>
                                                                            <td class="text-right">[[ y.word_count ]]
                                                                            </td>
                                                                            <td>[[ y.location ]]</td>
                                                                            <td class="text-right pr-18">[[ y.difficulty
                                                                                ]]%
                                                                            </td>
                                                                            <td style="text-align: center"> <span class="table-option"
                                                                                       ng-click="$ctrl.open(y,$index);$event.stopPropagation()">
                                                                                    <i class="action-icon icon-trash"></i>
                                                                                </span>
                                                                            </td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </td>

                                                            {{--
                                                            <!--
                                                                                                                    <a href="URL::route('backend.tools.keywordresearch.findResearch',$search->id) " ng-if="x.keyword !== 'Saved Keywords' && x.research.length != 0" ng-click="showLoader(true)">
                                                                                                                        <i class="action-icon icon-arrow-right-circle"></i>
                                                                                                                    </a> -->
                                                            <!--
                                                                                                                    <span ng-disabled="x.getEvaluation" ng-click="getDataOfSavedKeyword(x)" >
                                                                                                                        <span ng-if="!x.getEvaluation"></span>
                                                                                                                        <img src="/assets/backend/images/loading-icon.gif" ng-if="x.getEvaluation">
                                                                                                                    </span>
                                                             -->
                                                             --}}
                                                        </tr>


                                                        <tr ng-if="x.research.length > 0" class="child-data expanded">

                                                        </tr>

                                                        </tbody>
                                                    </table>
                                                </div>

                                                <div class="alert alert-warning cb" ng-if="savedSearches.length == 0">
                                                    <strong>Oh No!</strong>
                                                    You do not have any researching happening
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-pane" style="padding: 20px;background: #fff" id="todaySearch"
                                         ng-cloak>
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="pull-left">
                                                    <h4 class="m-t-0 header-title"><b>Recent Keyword Research</b></h4>
                                                    <p class="text-muted m-b-20 font-13">
                                                        Keyword research activity from the last 24 hours.
                                                    </p>
                                                </div>
                                                <div class="table-container" ng-if="searchedToday.length > 0">
                                                    <table class="table">
                                                        <thead>
                                                        <tr>
                                                            <th>Researched Word</th>
                                                            <th class="text-right">Search Location</th>
                                                            <th class="text-right">Actions</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr ng-repeat="x in searchedToday track by $index ">
                                                            <td>[[ x.keyword ]]</td>
                                                            <td class="text-right">[[ x.location ]]</td>
                                                            <!-- <td class="text-right actions">
                                                                <span type="button" style="width:85px" ng-disabled="x.getEvaluation" ng-click="getDataOfSavedKeyword(x)" class="btn btn-primary">
                                                                    <span ng-if="!x.getEvaluation">Get Data</span>
                                                                    <img src="/assets/backend/images/loading-icon.gif" ng-if="x.getEvaluation">
                                                                </span>
                                                            </td> -->
                                                            <td class="text-right actions">
                                                        <span ng-if="!x.searching" class="table-option"
                                                              ng-click="searchOnTodaySearchKeyword(x);">
                                                            <i class="action-icon icon-arrow-right-circle"></i>
                                                        </span>
                                                                <img src="/assets/backend/images/loading-icon.gif"
                                                                     ng-if="x.searching">
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="alert alert-warning cb" ng-if="searchedToday.length == 0">
                                                    <strong>Oh No!</strong>
                                                    You do not have any research that have taken place over the past 24
                                                    hours.
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                                <!-- /tile body -->

                            </div>
                            <!-- / tile -->
                        </div>

                    </div>


                    <!-- Download Evaluated Keywords or all keywords -->
                    <div class="modal fade confirm-modal" id="DownloadCsvData" tabindex="-1" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                                aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title">Refine the action</h4>
                                </div>
                                <div class="modal-body">
                                    <p>Do you want to download only the evaluated keywords or the entire list of
                                        keywords?</p>
                                </div>
                                <div class="modal-footer">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <button type="button" ng-click="downloadEvaluated(true)"
                                                    class="btn btn-default waves-effect waves-light pull-right confirm-button">
                                                Evaluated
                                            </button>
                                            <!-- <a href="[[ URL::route('backend.tools.keywordresearch.downloadEvaluated', $tmp_id) ]]" class="btn btn-default waves-effect waves-light pull-right confirm-button">Evaluated</a> -->
                                        </div>
                                        <div class="col-md-6">
                                            <button type="button" ng-click="downloadEvaluated(false)"
                                                    class="btn btn-default waves-effect waves-light pull-left confirm-button">
                                                All
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- modal for check all domain -->
                    <div id="modal">
                        <div class="loader-panel panel-default" style="border-radius:5px;">
                            <div class="model-header text-left">
                                <h4 class="loader-panel-text">Processing...</h4>
                            </div>
                            <hr style="margin-top:0px;margin-bottom:20px;">
                            <div class="panel-body">
                                <div class="progress-bar progress-bar-striped active" role="progressbar"
                                     aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"
                                     style="width:100%;margin-top:-20px;">
                                    &nbsp;
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>


        <!-- </div> -->
        @stop

        @section('scripts')
            <script src="{{ asset('assets/backend/js/vendor/selectize/selectize.js') }}"></script>
            <script src="{{ asset('assets/backend/js/vendor/easypiechart/jquery.easypiechart.min.js') }}"></script>

            <script>

                function showLoader(show) {
                    if (show === true) {
                        $("#loader").show();
                        $("#loader").addClass('in');
                    } else {
                        $("#loader").hide();
                        $("#loader").removeClass('in');
                    }
                };
                $(".prog").hide();
                showLoader(false);

                var selection = {
                    location: {
                        data: {!! $tldOptions !!},
                        items: '{{ $selectedEngine }}',
                        valueField: 'code',
                        labelField: 'name',
                        searchField: ['name', 'code']
                    },

                    languages: {
                        data: {!! $languages !!},
                        items: '{{ $selectedLanguage }}',
                        valueField: 'name',
                        labelField: 'engine',
                        searchField: ['name', 'engine']
                    },
                };

                // $('.searchSelection').each(function(){
                //     var el = selection[$(this).data('target')];
                //     $(this).selectize({
                //         plugins: ['remove_button'],
                //         valueField: el.valueField,
                //         labelField: el.labelField,
                //         searchField: el.searchField,
                //         delimiter: ',',
                //         persist: false,
                //         hideSelected: true,
                //         openOnFocus: true,
                //         items: [el.items],
                //         render: {
                //             item: function (item, escape) {
                //                 console.log(item)
                //                 return '<div><span>' + item.name + ' (' + item.code + ')</span></div>';
                //             },
                //             option: function (item, escape) {
                //                 console.log(this.searchField)
                //                 return '<div><span>' + item.name + ' (' + item.code + ')</span></div>';
                //             }
                //         },
                //         options: el.data,
                //         onChange: function(value){
                //             console.log('hello there')
                //         }
                //     });
                // })


                var tldOptions = {!! $tldOptions !!};
                var languages = {!! $languages !!};
                var keyword = '{!! $keyword !!}';
                var Selected = {language: '{{ $selectedLanguage }}', location: '{{ $selectedEngine }}'};

                $('.tld-selectize').selectize({
                    plugins: ['remove_button'],
                    valueField: 'name',
                    labelField: 'engine',
                    searchField: ['name', 'engine'],
                    delimiter: ',',
                    persist: false,
                    hideSelected: true,
                    openOnFocus: true,
                    items: ['{{ $selectedEngine }}'],
                    render: {
                        item: function (item, escape) {
                            return '<div><span>' + item.engine + ' ‒ ' + item.name + '</span></div>';
                        },
                        option: function (item, escape) {
                            return '<div><span>' + item.engine + ' ‒ ' + item.name + '</span></div>';
                        }
                    },
                    options: tldOptions,
                    onChange: function (value) {
                        Selected.location = value;
                    }
                });

                $('.lang-selectize').selectize({
                    plugins: ['remove_button'],
                    valueField: 'code',
                    labelField: 'name',
                    searchField: ['name', 'code'],
                    delimiter: ',',
                    persist: false,
                    hideSelected: true,
                    openOnFocus: true,
                    items: ['{{ $selectedLanguage }}'],
                    render: {
                        item: function (item, escape) {
                            return '<div><span>' + item.name + ' (' + item.code + ')</span></div>';
                        },
                        option: function (item, escape) {
                            return '<div>' +
                                '<span>' + item.name + ' (' + item.code + ')</span>' +
                                '</div>';
                        }
                    },
                    options: languages,
                    onChange: function (value) {
                        Selected.language = value;
                    }
                });
            </script>

            <!-- Post_Affiliate_Pro START
            <script type="text/javascript" id="pap_x2s6df8d" src="https://keywordrevealer.postaffiliatepro.com/scripts/vdzfn3n4jm8"></script>
            <script type="text/javascript">
            PostAffTracker.setAccountId('default1');
            try {
            PostAffTracker.track();
            } catch (err) { }
            </script>
            -->
    </div>
    </div>
@stop
