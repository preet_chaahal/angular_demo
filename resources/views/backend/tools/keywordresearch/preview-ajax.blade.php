<div class="row">
    <div class="col-md-12">

        <ul class="nav nav-tabs" id="subTabList" style="background:#fff">
            <li class="active"><a data-toggle="tab" href="#researchResults">Search Results</a></li>
            <li><a data-toggle="tab" href="#savedKeywords">Saved Keywords</a></li>
        </ul>


        <div id="saveActions" style="position:absolute; top:8px; right:20px;"></div>

        <div>
        </div>

        <div id="tabContent" class="tab-content">
            <div id="researchResults" class="tab-pane active">
                <div id="cover-tbl" style="width: 100%; height: 100%; background: #ebeff2; position: absolute; z-index: 99;"></div>
                <div class="card-box" style="border: none">
                    <div class="panel-group" id="accordion">
                        <div class="panel panel-default">
                            <div class="row">
                                <div class="col-xs-3">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" class="collapsed btn btn-primary">
                                                Filter
                                            </a>
                                        </h4>
                                    </div>
                                </div>
                                <div class="col-xs-9 pull-right pl-0">
                                    <!-- <a href=" URL::route('backend.tools.keywordresearch.downloadPreview', $tmp_id) "
                                        class="btn btn-default waves-effect waves-light pull-right ml-20">Download CSV
                                        <span class="btn-label btn-label-right"><i class="fa fa-file-excel-o"></i></span>
                                    </a> -->
                                    <a href="#" data-toggle="modal" data-target=".confirm-modal" class="btn btn-default waves-effect waves-light pull-right ml-20">
                                        Download CSV
                                        <span class="btn-label btn-label-right">
                                            <i class="fa fa-file-excel-o"></i>
                                        </span>
                                    </a>

                                    <button class="btn btn-default pull-right ml-20" onclick="dCheckAll()">Check Domains</button>
                                    <button ng-disabled="isSaved" id="btn-save_keyword" class="btn btn-default pull-right" onclick="saveKeyword();">Save Project</button>
                                    <button class="btn btn-default pull-right" disabled="disabled">Save Project</button>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div id="collapseOne" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div class="row">

                                                <div class="col-md-4">
                                                    <div class="row">
                                                        <div class="col-md-11">
                                                            <label class="control-label">Keyword Search</label>
                                                            <div class="form-group">
                                                                <input type="text" class="form-control" id="minepc">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-11">
                                                            <div class="form-group">
                                                                <label class="control-label">Keyword Exclude</label>
                                                                <input type="text" class="form-control" id="mineke">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <label class="control-label">Number Of Words</label>
                                                    <div class="row">
                                                        <div class="col-xs-5">
                                                            <div class="form-group">
                                                                <input type="text" class="form-control" placeholder="min" id="nowordsmin">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-1 col-xs-2 text-center">-</div>
                                                        <div class="col-xs-5">
                                                            <div class="form-group">
                                                                <input type="text" class="form-control" placeholder="max" id="nowordsmax">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <label class="control-label">Cost per Click</label>
                                                    <div class="row">
                                                        <div class="col-xs-5">
                                                            <div class="form-group">
                                                                <input type="text" class="form-control" placeholder="min" id="mincpc">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-1 col-xs-2 text-center">-</div>
                                                        <div class="col-xs-5">
                                                            <div class="form-group">
                                                                <input type="text" class="form-control" placeholder="max" id="maxcpc">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <label class="control-label">Average Monthly Searches</label>
                                                    <div class="row">
                                                        <div class="col-xs-5">
                                                            <div class="form-group">
                                                                <input type="text" class="form-control" placeholder="min" id="min">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-1 col-xs-2 text-center">-</div>
                                                        <div class="col-xs-5">
                                                            <div class="form-group">
                                                                <input type="text" class="form-control" placeholder="max" id="max">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <label class="control-label">Estimated Potential Earning</label>
                                                    <div class="row">
                                                        <div class="col-xs-5">
                                                            <div class="form-group">
                                                                <input type="text" class="form-control" placeholder="min" id="minlms">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-1 col-xs-2 text-center">-</div>
                                                        <div class="col-xs-5">
                                                            <div class="form-group">
                                                                <input type="text" class="form-control" placeholder="max" id="maxlms">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                            </div> <!-- /row -->

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="modal">
                        <div class="loader-panel panel-default" style="border-radius:5px;">
                            <div class="model-header text-left">
                                <h4 class="loader-panel-text">Processing...</h4>
                            </div>
                            <hr style="margin-top:0px;margin-bottom:20px;">
                            <div class="panel-body">
                                <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:100%;margin-top:-20px;">
                                    &nbsp;
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table class="table" id="researchTable">
                            <thead>
                            <tr>
                                <th style="width: 40%">
                                    <span data-toggle="tooltip" data-placement="top" title="Generated Keyword Ideas">Keywords</span>
                                </th>
                                <th class=" {sorter: 'digit'}\ text-center column-width">
                                    <span data-toggle="tooltip" data-placement="top" title="Monthly Searches">AMS</span>
                                </th>
                                <th class="text-center column-width">
                                    <span data-toggle="tooltip" data-placement="top" title="The Approximate Cost Per Click">CPC</span>
                                </th>
                                <th class="text-center column-width">
                                    <span data-toggle="tooltip" data-placement="top" title="Potential Monthly Earning from Ads Clicks">Profit</span>
                                </th>
                                <th class="text-center column-width">
                                    <span data-toggle="tooltip" data-placement="top" title="Number of Words">Words</span>
                                </th>
                                @if(Auth::user()->can('domain_finder'))
                                    <th class="text-center " style="min-width: 88px">
                                        <span data-toggle="tooltip" data-placement="top" title="Exact Match Domain Availability">Domain</span>
                                    </th>
                                @endif
                                <th class="text-center column-width">
                                    <span data-toggle="tooltip" data-placement="top"
                                        title="Calculate the Ranking Difficulty and Opportunities, the Lower the Better">Difficulty</span>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="(k,v) in researchData track by $index">
                                    <td [[$record['keyword'] == $keyword ? 'style=color:#428bca;' : '' ]]>
                                        [[ v.keyword ]]
                                    </td>
                                    <td class="text-center">
                                        [[ x.searches ]]
                                    </td>
                                    <td class="text-center" ng-bind="x.cpc == 0 ? '$0.00' : ('$'+x.cpc) ">
                                    </td>
                                    <td class="text-center">
                                        if ($record['cpc']== 0)
                                            [['$0.00' ]]
                                        else
                                            [[ '$'.number_format( (float)str_replace( ",", "", $record['profit']), 0, '', ',') ]]
                                        endif

                                    </td>
                                    <td class="text-center">
                                        [[ x.word_count'] }}
                                    </td>
                                    <td ng-if="userCanFindDomain" class="text-center domain" id="[[ x.keyword'] ]]"
                                        onclick="getDomain([[ x.keyword ]], this)">
                                    </td>
                                    {{--
                                    <td id="evolution_[[k]]" class="text-center" style="color:#1693A5;font-size:20px;">
                                        if(array_key_exists($record['keyword'], $eval_data))
                                            <a href="#" onclick="showReEvaluationConfirm('[[ x.keyword'] }}','[[ x.keyword_id'] }}', '[[ x.searches'] }}', '[[ x.cpc'] }}', '[[ x.profit'] }}', '[[ x.word_count'] }}', '[[ x.location'] ]]', '[[ x.language'] }}', '[[k]]','[[ $eval_data[$record['keyword']]['id'] ]]',true,'[[ $eval_data[$record['keyword']]['isUnsaved'] ]]'); return false;">[[$eval_data[$record['keyword']]['difficulty']]]</a>
                                        else
                                            <a href="#" class="btn btn-primary btn-block" onclick="showEvaluation('[[ x.keyword'] }}','[[ x.keyword_id'] }}', '[[ x.searches'] ]]', '[[ x.cpc'] ]]', '[[ x.profit'] }}', '[[ x.word_count'] ]]', '[[ x.location'] ]]', '[[ x.language'] ]]', '[[k]]',null,false); return false;">Evaluate</a>
                                        endif
                                    </td>
                                    --}}
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
            <div id="savedKeywords" class="tab-pane">
                <div class="alert alert-info">
                    This project does not have any saved keywords yet.
                </div>
            </div>


        </div>
    </div>
</div>

<!-- Modal -->
<div id="loader" class="modal-backdrop fade">
    <div class="line-spin-fade-loader center">
        <div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div>
    </div>
</div>

<!-- Confirm Modal -->
<div class="modal fade confirm-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Refine the action</h4>
            </div>
            <div class="modal-body">
                <p>Do you want to download only the evaluated keywords or the entire list of keywords?</p>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-md-6">
                        <button type="button" onclick="downloadEvaluated('[[ tmpId ]]', false)" class="btn btn-default waves-effect waves-light pull-right confirm-button">Evaluated</button>
                        <!-- <a href="[[ URL::route('backend.tools.keywordresearch.downloadEvaluated', $tmp_id) ]]" class="btn btn-default waves-effect waves-light pull-right confirm-button">Evaluated</a> -->
                    </div>
                    <div class="col-md-6">
                        <button type="button" onclick="downloadEvaluated('[[ tmpId ]]', true)" class="btn btn-default waves-effect waves-light pull-left confirm-button">All</button>
                        <!-- <a href="[[ URL::route('backend.tools.keywordresearch.downloadPreview', $tmp_id) ]]" class="btn btn-default waves-effect waves-light pull-left confirm-button">All</a> -->
                    </div>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
    var evaluatedId = null,
        isImported = '{{ $isImported }}',
        projectName = '{{ $keyword }}',
        tmp_id = '[[ tmpId ]]',
        keywordsWithDifficulties = {},
        notSavedKeywordsNumber = 0;

    $(".confirm-button").on('click', function (e) {
        $('.confirm-modal').modal('hide');
    });
    
    function downloadEvaluated(id, allFalg) {
        var ajaxUrl = '{{ URL::route('backend.tools.keywordresearch.downloadEvaluated', ['id', 'allFalg', 'eval_data']) }}'
            .replace('id', id)
            .replace('allFalg', allFalg)
            .replace('eval_data', JSON.stringify(keywordsWithDifficulties));

        window.location.href = ajaxUrl;
    };

    $(function () {
        showLoader(false);
        $('[data-toggle="tooltip"]').tooltip();

        var table = $('#researchTable').DataTable({
            "order": [[1, 'ms-case-desc']],
            columnDefs: [
                {type: 'numeric-comma', targets: 0},
                {"bSortable": false, targets: 5}
            ],
        });

        // $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        //     var target = $(e.target).attr("href");
        //     selectTab(target);
        // });

        // keyword filter
        $('#minepc').keyup(function () {
            $('#researchTable').dataTable({"bRetrieve": true}).fnFilter($(this).val(), $(this).index(this));
        });
        $('#mineke').keyup(function () {
            $('#researchTable').dataTable({"bRetrieve": true}).fnDraw(true);
        });
        // monthly searches
        $('#min').keyup(function () {
            $('#researchTable').dataTable({"bRetrieve": true}).fnDraw(true);
        });
        $('#max').keyup(function () {
            $('#researchTable').dataTable({"bRetrieve": true}).fnDraw(true);
        });
        $('#mincpc').keyup(function () {
            $('#researchTable').dataTable({"bRetrieve": true}).fnDraw(true);
        })
        $('#maxcpc').keyup(function () {
            $('#researchTable').dataTable({"bRetrieve": true}).fnDraw(true);
        });
        $('#minlms').keyup(function () {
            $('#researchTable').dataTable({"bRetrieve": true}).fnDraw(true);
        });
        $('#maxlms').keyup(function () {
            $('#researchTable').dataTable({"bRetrieve": true}).fnDraw(true);
        });
        $('#nowordsmin').keyup(function () {
            $('#researchTable').dataTable({"bRetrieve": true}).fnDraw(true);
        });
        $('#nowordsmax').keyup(function () {
            $('#researchTable').dataTable({"bRetrieve": true}).fnDraw(true);
        });

        // load content
        $.ajax({
            url: '{{ URL::route('backend.tools.keywordresearch.saved', $keyword)}}/[]',
            type: 'get',
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                if (data.success) {
                    $("#savedContent").html(data.data);
                    $("#notSavedContent").html(data.data2);

                    $("#cover-tbl").hide();
                    $("#cover-eval").hide();
                    selectTab('evalTbl');
                    showLoader(false);
                }
            },
            error: function (data) {
                console.log('keywordresearch.saved error');
            }
        });
    });
    
    function selectTab(tabName, init, keywordID, notSendRequest) {
        if (init) {
            $('#subTabList li').removeClass('active');
            $('#tab-' + tabName).addClass('active');
        }

        if (tabName == 'saved') {
            $("#evalTbl").hide();
            $("#savedContentWrapper").show();
            $("#notSavedContentWrapper").hide();
            if (savedKeywordsNumber > 0) {
                $("#savedContentWrapper .alert").hide();
                $("#savedDetailInfo").show();
                $("#savedContent").show();
                if (!notSendRequest) {
                    if (keywordID) {
                        $('#saveTbl #research_'+ keywordID).trigger('click');
                    } else {
                        $('#saveTbl tr').first().trigger('click');
                    }
                }
            } else {
                $("#savedContentWrapper .alert").show();
                $("#savedDetailInfo").hide();
                $("#savedContent").hide();
            }
            $("#saveActions").hide();
        }
        else if (tabName == 'evaluated') {
            $("#evalTbl").hide();
            $("#savedContentWrapper").hide();
            $("#notSavedContentWrapper").show();
            if (notSavedKeywordsNumber > 0) {
                $("#notSavedContentWrapper .alert").hide();
                $("#savedDetailInfo").show();
                $("#notSavedContent").show();
                $("#saveActions").show();
                if (!notSendRequest) {
                    if (keywordID) {
                        $('#ns_saveTbl #research_'+ keywordID +'_ns').trigger('click');
                    } else {
                        $('#ns_saveTbl tr').first().trigger('click');
                    }
                }
            } else {
                $("#notSavedContentWrapper .alert").show();
                $("#savedDetailInfo").hide();
                $("#notSavedContent").hide();
            }
        }
        else {
            $("#savedDetailInfo").hide();
            $("#savedContentWrapper").hide();
            $("#notSavedContentWrapper").hide();
            $("#saveActions").hide();
            $("#evalTbl").show();
        }
    }

    function showWait() {
        $("#alert-error").hide();
        $("#ajax-wait").show();
    }

    function showEvaluation(keyword, keyword_id, searches, cpc, profit, word_count, location, language, rowId, evaluated_id, isReevaluate, isUnsaved) {
        // if (evaluated_id == null) {
        //     for (key in notSavedKeywordsArr) {
        //         if (notSavedKeywordsArr[key] == keyword) {
        //             selectTab('evaluated', true, key);
        //             return;
        //         }
        //     }
        // }

        showLoader(true);
        var d = new Date();
        evaluatedId = evaluated_id;
        addTab(keyword, keyword_id, searches, cpc, profit, word_count, location, language, d.getTime(), rowId, isReevaluate, isUnsaved);
    }

    var evalActive = false;

    $(document).on('click','.newEvaluatedItem', function(){
        // $("#savedDetailInfo .item").hide();
        // $('#tabContent #data_'+$(this).data('target')).show();
    })

    $(document).on('click','#subTabList a[data-toggle="tab"] .remove', function(){
        var p = $(this).parent();
        console.log(p.attr('href'))
        $(p.attr('href')).remove()
        console.log(p.parent().prev())
        p.parent().prev().children('a').trigger('click') //trigger the prev to focus
        p.parent().remove(); // remove the li
    })

    function addTab(displayName, keyword_id, searches, cpc, profit, word_count, location, language, elementId, rowId, isReevaluate, isUnsaved) {
        var searchKeyword = isImported == 1 ? displayName : '{{ $keyword }}';
        var ajaxUrl = '{{ URL::route('backend.tools.keywordresearch.evaluate', ['ts_keyword', 'ts_keywordId','ts_searches', 'ts_cpc', 'ts_profit', 'ts_word_count', 'ts_location', 'ts_language', 'search_keyword', 'evaluated_id', 'is_reevaluate', 'is_unsaved', 'project_name', 'tmp_id'] ) }}'
                .replace('ts_keyword', displayName)
                .replace('ts_keywordId', keyword_id)
                .replace('ts_searches', searches)
                .replace('ts_cpc', cpc)
                .replace('ts_profit', profit)
                .replace('ts_word_count', word_count)
                .replace('ts_location', location)
                .replace('ts_language', language)
                .replace('search_keyword', searchKeyword)
                .replace('evaluated_id', evaluatedId)
                .replace('is_reevaluate', isReevaluate)
                .replace('is_unsaved', isUnsaved)
                .replace('project_name', projectName)
                .replace('tmp_id', tmp_id);

        var id = 'data_' + elementId;

        $('#tabContent').append('<div id="'+id+'" class="tab-pane"></div>');

        $('#saveActions').html("");

        // call ajax
        $.ajax({
            url: ajaxUrl,
            type: 'get',
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                if (data.success) {

                    var xx = '<li><a data-toggle="tab" href="#'+id+'">';
                    xx += displayName.length > 11 ? displayName.slice(0,8)+' ..' : displayName;
                    xx += '<span class="badge">'+data.data.difficulty+'</span><span class="glyphicon glyphicon-remove remove"></span></a></li>';

                    $("#subTabList").append(xx);

                    $("#errorsBox").html("");
                    if (data.data.newEvaluate) {
                        notSavedKeywordsNumber++;
                        $('#'+id).append(data.data.html);
                        appendDataToUnsavedKeywordsTable(data.data);
                        // notSavedKeywordsArr[data.data.unsavedResearch_id] = data.data.keyword;
                    }else{
                        $('#'+id).append(data.data.html);
                    }
                    $('#saveButtonContainer').appendTo('#saveActions');

                    if ( $('#child-data-' + evaluatedId + ' td').length > 0 && !isReevaluate){
                        evalActive = false;
                        expandChildTable(evaluatedId);
                    } else if (isReevaluate) {
                        expandChildTable(evaluatedId);
                        $('#tab-saved a').trigger('click');
                    } else {
                        evalActive = true;
                        expandChildTable(data.data.unsavedResearch_id + '_ns')
                        selectTab('evaluated', true, data.data.unsavedResearch_id, true);
                        if (rowId) {
                            var htmlstr = "showReEvaluationConfirm(" + "'" + displayName + "'" + "," + "'" + keyword_id + "'" + "," + "'" + searches + "'" + "," + "'" + cpc + "'" + "," + "'" + profit + "'" + "," + "'" + word_count + "'" + "," + "'" + location + "'" + "," + "'" + language + "'" + "," + "'" + rowId + "'" + "," + null + ","+false+","+ data.data.newEvaluate +");return false;";
                            document.getElementById('evolution_' + rowId).innerHTML ='<a href="#" onclick="'+htmlstr+'">'+Math.round(data.data.difficulty)+'</a>';
                            keywordsWithDifficulties[displayName] = Math.round(data.data.difficulty);
                        }
                    }

                    $('#cover-eval').hide();
                    $('a[href="#'+id+'"]').trigger('click');
                    showLoader(false);

                    //get the unique id of the new element
                    var uId = $('.uniqueId').last().data('target')


                    renderGraph($("#bar"+uId), 'bar', data.data.graphResults);
                    renderGraph($("#horizontalBar"+uId), 'horizontalBar', data.data.graphResults);
                    renderProgressBar('progress-url'+uId, 'primary', 'Url', data.data.graphResults.meta_url);
                    renderProgressBar('progress-title'+uId, 'pink', 'Title', data.data.graphResults.meta_title);
                    renderProgressBar('progress-desc'+uId, 'warning', 'Desc', data.data.graphResults.meta_desc);
                    renderProgressBar('progress-h1'+uId, 'success', 'H1', data.data.graphResults.meta_h1);
                }
            },
            error: function (data) {
                showLoader(false);
                $("#errorsBox").html("");
                $("#errorsBox").append('<div class="alert alert-lightred alert-dismissable fade in"><h4>Whoops!</h4><ul><li>' + data.responseJSON.message + '</li></ul></div>');
                $("#errorsBox").show();
            }
        });
    }

    function appendDataToUnsavedKeywordsTable(data) {
        var d = new Date(data.created_at.date);
        var parentRow = '<tr id="research_' + data.unsavedResearch_id + '_ns" data-id="' + data.unsavedResearch_id + '" class="parent-data"' +
                            'onclick="showEvaluation(\'' + data.keyword + '\',\'' +
                                                         data.project_name + '\',\'' +
                                                         data.searches + '\',\'' +
                                                         data.cpc + '\',\'' +
                                                         data.profit + '\',\'' +
                                                         data.word_count + '\',\'' +
                                                         data.location + '\',\'' +
                                                         data.language + '\',\'' +
                                                         '1' + '\',\'' +
                                                         data.unsavedResearch_id + '_ns\',' +
                                                         'false, true); return false;">' +
                                '</td><td>' +
                                data.keyword +
                                '</td><td>' +
                                data.searches +
                                '</td><td>$' +
                                data.cpc +
                                '</td><td>$' +
                                data.profit +
                                '</td><td>' +
                                data.word_count +
                                '</td><td>' +
                                data.location +
                                '</td><td>' +
                                data.difficulty +
                                '</td><td>' +
                                (d.getMonth() + 1) + "/" + d.getDate() + "/" + d.getFullYear() +
                                '</td><td class="text-right actions">' +
                                    '<a href="javascript:void(0);" onclick="bulkDeleteKeywords(' + data.unsavedResearch_id + ', this, true)" class="single-delete"><i class="action-icon icon-trash"></i></a>' +
                                '</td></tr>' +
                                '<tr id="child-data-' + data.unsavedResearch_id + '_ns" class="child-data"><td colspan="9" class="child-data-td"><div class="sub-table sub-table-open"></div></td></tr>';
        $('#ns_saveTbl').append(parentRow);
        keywordsWithDifficulties[data.keyword] = data.difficulty;
    }

    function renderGraph(selector, type, obj, option) {
        switch (type) {
            case 'horizontalBar':
                var data = {
                    labels: [1,2,3,4,5,6,7,8,9,10],
                    datasets : [
                        { label: 'Moz Domains', backgroundColor: '#3B5998', data: obj.backlinks }
                    ]
                };
                var options = {
                    scales: {
                        xAxes: [{
                            gridLines: {
                                display: false
                            },
                            stacked: true
                        }],
                        yAxes: [{
                            gridLines: {
                                display: false
                            }
                        }],
                    },
                    responsive: true,
                    maintainAspectRatio: false,
                    tooltips: { mode : 'label' }
                };
                break;
            default:
                var data = {
                    labels: [1,2,3,4,5,6,7,8,9,10],
                    datasets : [
                        { label: 'Facebook',backgroundColor: '#3B5998',data : obj.facebook_shares },
                        { label: 'Twitter',backgroundColor: '#1DA1F2',data : obj.tweets },
                        { label: 'Google+',backgroundColor: '#EC5F52',data : obj.googlePlus },
                        { label: 'Pinterest',backgroundColor: '#BD081C',data : obj.pinterest }
                    ]
                };
                var options = {
                    scales: {
                        xAxes: [{
                            gridLines: {
                                display: false
                            }
                        }],
                        yAxes: [{
                            gridLines: {
                                display: false
                            }
                        }],
                    },
                    responsive: true,
                    maintainAspectRatio: false,
                    tooltips: { mode : 'label' }
                };
                break;

        };
        var myBarChart = new Chart(selector, {
            type: type,
            data: data,
            options: options
        });
    }

    function renderProgressBar(selector, scheme, title, obj) {
        document.getElementById(selector).innerHTML = '<p class="font-600">' + title + ' <span class="text-' + scheme + ' pull-right">' + obj.positive * 10 + '%</span></p>' +
                '<div class="progress progress-lg m-b-20">' +
                    '<div class="progress-bar progress-bar-' + scheme + ' progress-animated wow animated" role="progressbar" aria-valuenow="' + obj.positive * 10 + '" aria-valuemin="0" aria-valuemax="100" style="width: ' + obj.positive * 10 + '%">' +
                    '<span>Yes</span></div>' +
                '</div>';
    }

    function expandChildTable(id) {
        $('.child-data .sub-table').removeClass('sub-table-open');
        $('.parent-data').removeClass('parent-expand');
        $('.child-data').removeClass('expand');

        $('#child-data-' + id + ' .sub-table').html("");
        $('#rankingTable').appendTo('#child-data-' + id + ' .sub-table');
        $('#research_' + id).addClass('parent-expand');
        $('#child-data-' + id).addClass('expand');
        $('#child-data-' + id + ' .sub-table').addClass('sub-table-open');
        $('#rankingTableRow').hide();
    }

    /* Create an array with the values of all the <a> texts in a column, parsed as numbers */
    $.fn.dataTableExt.order['dom-text-numeric'] = function  ( settings, col )
    {
        return this.api().column( col, {order:'index'} ).nodes().map( function ( td, i ) {
            var val = $('a', td).text() * 1;
            return isNaN(val) ? 999 : val;
        } );
    }

    /* Custom sorting function which will sorts the numerical values of the '3,246,965' type */
    $.fn.dataTableExt.oSort['ms-case-desc']  = function(x, y) {
        var realX = x == "-" ? 0 : x.replace(/,/g, "") * 1;
        var realY = y == "-" ? 0 : x.replace(/,/g, "") * 1;
        return ((realX < realY) ?  1 : ((realX > realY) ? -1 : 0));
    };

    /* Custom filtering function which will filter data with exclude keyword */
    $.fn.dataTableExt.afnFiltering.push(
            function (oSettings, aData, iDataIndex) {
                if (oSettings.nTable.id === "researchTable") {
                    var searchValue = $("#mineke").val();
                    var thisSearchValue = aData[0];
                    if (!(thisSearchValue.match(searchValue)) || searchValue == "") {
                        return true;
                    }
                    return false;
                }
                else {
                    return true;
                }
            }
    );

    /* Custom filtering function which will filter data in column four between two values */
    $.fn.dataTableExt.afnFiltering.push(
            function (oSettings, aData, iDataIndex) {
                if (oSettings.nTable.id === "researchTable") {
                    var iMin = document.getElementById('min').value * 1;
                    var iMax = document.getElementById('max').value * 1;
                    var iVersion = aData[1] == "-" ? 0 : aData[1].replace(/,/g, "") * 1;
                    if (iMin == "" && iMax == "") {
                        return true;
                    }
                    else if (iMin == "" && iVersion <= iMax) {
                        return true;
                    }
                    else if (iMin <= iVersion && "" == iMax) {
                        return true;
                    }
                    else if (iMin <= iVersion && iVersion <= iMax) {
                        return true;
                    }
                    return false;
                }
                else {
                    return true;
                }
            }
    );

    ///////////////////////cpc
    $.fn.dataTableExt.afnFiltering.push(
            function (oSettings, aData, iDataIndex) {
                if (oSettings.nTable.id === "researchTable") {
                    var iMincpc = document.getElementById('mincpc').value * 1;
                    var iMaxcpc = document.getElementById('maxcpc').value * 1;
                    var iVersioncpc = aData[2].substring(1) == "-" ? 0 : aData[2].substring(1) * 1;
                    if (iMincpc == "" && iMaxcpc == "") {
                        return true;
                    }
                    else if (iMincpc == "" && iVersioncpc <= iMaxcpc) {
                        return true;
                    }
                    else if (iMincpc <= iVersioncpc && "" == iMaxcpc) {
                        return true;
                    }
                    else if (iMincpc <= iVersioncpc && iVersioncpc <= iMaxcpc) {
                        return true;
                    }
                    return false;
                }
                else {
                    return true;
                }
            }
    );
    ///////////////////////lms
    $.fn.dataTableExt.afnFiltering.push(
            function (oSettings, aData, iDataIndex) {
                if (oSettings.nTable.id === "researchTable") {
                    var iMinlms = document.getElementById('minlms').value * 1;
                    var iMaxlms = document.getElementById('maxlms').value * 1;
                    var iVersionlms = aData[3].substring(1) == "-" ? 0 : aData[3].substring(1).replace(/,/g, "") * 1;
                    if (iMinlms == "" && iMaxlms == "") {
                        return true;
                    }
                    else if (iMinlms == "" && iVersionlms <= iMaxlms) {
                        return true;
                    }
                    else if (iMinlms <= iVersionlms && "" == iMaxlms) {
                        return true;
                    }
                    else if (iMinlms <= iVersionlms && iVersionlms <= iMaxlms) {
                        return true;
                    }
                    return false;
                }
                else {
                    return true;
                }
            }
    );

    ///////////////////////no of words
    $.fn.dataTableExt.afnFiltering.push(
            function (oSettings, aData, iDataIndex) {
                if (oSettings.nTable.id === "researchTable") {
                    var iMinwords = document.getElementById('nowordsmin').value * 1;
                    var iMaxwords = document.getElementById('nowordsmax').value * 1;
                    var iVersionwords = aData[4] == "-" ? 0 : aData[4] * 1;
                    if (iMinwords == "" && iMaxwords == "") {
                        return true;
                    }
                    else if (iMinwords == "" && iVersionwords <= iMaxwords) {
                        return true;
                    }
                    else if (iMinwords <= iVersionwords && "" == iMaxwords) {
                        return true;
                    }
                    else if (iMinwords <= iVersionwords && iVersionwords <= iMaxwords) {
                        return true;
                    }
                    return false;
                }
                else {
                    return true;
                }
            }
    );

    function deleteItem(id) {
        var ans = confirm('Are you sure?');
        var url = '{{ URL::route('backend.tools.keywordresearch.destroyAjax', ':id') }}';

        if (ans) {
            $.ajax({
                url: url.replace(':id', id),
                type: 'get',
                dataType: 'json',
                success: function (data, textStatus, jqXHR) {
                    $("#research_" + id).fadeOut();
                }
            });
        }
    }

    /*hide and show function while click on domain availability*/
    function openModal() {
        document.getElementById('modal').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
    }

    function closeModal() {
        document.getElementById('modal').style.display = 'none';
        document.getElementById('fade').style.display = 'none';
    }
    /*End Here Domain availability */

    function dCheckAll() {
        openModal();
        var table = $('#researchTable').DataTable();
        var allData = table.rows( { filter : 'applied'} ).data();
        var info = table.page.info();

        rowArr = new Array();
        for (i = info.start; i < info.end; i++) {
            rw = allData[i];
            cell = $(document.getElementById(rw[0]));
            if (!cell.hasClass('checked')) {
                cell.html('<img src="{{ asset('assets/backend/images/success-loader.gif') }}">');
                rowArr.push(rw[0]);
            }
        }

        var ajaxUrl = '{{ URL::route('backend.tools.keywordresearch.postDomainAvailability') }}';
        // load content
        $.ajax({
            url: ajaxUrl,
            type: 'post',
            dataType: 'json',
            data: {'keyword': rowArr},
            success: function ($domainResults) {
                closeModal();
                var table = $('#researchTable').DataTable();
//                var info = table.page.info();

                for (result in $domainResults) {
                    $domainResult = $domainResults[result];
                    var html = '';

                    if($domainResult.flag==true){
                        html += '<a href="https://www.namecheap.com/domains/registration/results.aspx?domain=' + result + '.com" target="_blank"><span class="label label-success domain-margin" style="background-color: #08c!important;">.com</span></a>'
                        html += '<a href="https://www.namecheap.com/domains/registration/results.aspx?domain=' + result + '.net" target="_blank"><span class="label label-success domain-margin" style="background-color: #08c!important;">.net</span></a>'
                        html += '<a href="https://www.namecheap.com/domains/registration/results.aspx?domain=' + result + '.org" target="_blank"><span class="label label-success domain-margin" style="background-color: #08c!important;">.org</span></a>'
                    }
                    else {
                        if (!$domainResult['com']) {
                            html += '<span class="label label-danger domain-margin">.com</span>'
                        }
                        else {
                            html += '<a href="https://www.namecheap.com/domains/registration/results.aspx?domain=' + result + '.com" target="_blank"><span class="label label-success domain-margin" >.com</span></a>'
                        }
                        if (!$domainResult['net']) {
                            html += '<span class="label label-danger domain-margin">.net</span>'
                        }
                        else {
                            html += '<a href="https://www.namecheap.com/domains/registration/results.aspx?domain=' + result + '.net" target="_blank"><span class="label label-success domain-margin">.net</span></a>'
                        }
                        if (!$domainResult['org']) {
                            html += '<span class="label label-danger domain-margin">.org</span>'
                        }
                        else {
                            html += '<a href="https://www.namecheap.com/domains/registration/results.aspx?domain=' + result + '.org" target="_blank"><span class="label label-success domain-margin">.org</span></a>'
                        }
                    }

                    $(document.getElementById(result)).addClass('checked').html(html);
                }
            }
        });
    }


    // check domain availability
    function getDomain(keyword, element) {

        var ajaxUrl = '{{ URL::route('backend.tools.keywordresearch.postDomainAvailability', ['tskeyword'] ) }}'
                .replace('tskeyword', keyword);

        // load content
        $.ajax({
            url: ajaxUrl,
            type: 'get',
            dataType: 'json',
            data: {row: 'row'},
            success: function ($domainResult) {
                var html = '';
                if (!$domainResult['com']) {
                    html += '<span class="label label-danger domain-margin">.com</span>'
                }
                else {
                    html += '<a href="https://www.namecheap.com/domains/registration/results.aspx?domain=' + keyword + '.com" target="_blank"><span class="label label-success domain-margin">.com</span></a>'
                }
                if (!$domainResult['net']) {
                    html += '<span class="label label-danger domain-margin">.net</span>'
                }
                else {
                    html += '<a href="https://www.namecheap.com/domains/registration/results.aspx?domain=' + keyword + '.net" target="_blank"><span class="label label-success domain-margin">.net</span></a>'
                }
                if (!$domainResult['org']) {
                    html += '<span class="label label-danger domain-margin">.org</span>'
                }
                else {
                    html += '<a href="https://www.namecheap.com/domains/registration/results.aspx?domain=' + keyword + '.org" target="_blank"><span class="label label-success domain-margin">.org</span></a>'
                }

                element.innerHTML = html;
            }
        });
    }

    function showReEvaluationConfirm(keyword, keyword_id, searches, cpc, profit, word_count, location, language, rowId, evaluated_id, isReevaluate, isUnsaved) {
        isUnsaved = isUnsaved ? isUnsaved : false;
        // if (isUnsaved) {
        //     for (key in notSavedKeywordsArr) {
        //         if (notSavedKeywordsArr[key] == keyword) {
        //             selectTab('evaluated', true, key);
        //             return;
        //         }
        //     }
        // }

        bootbox.dialog({
            message: "Are you sure want to Re-Evaluate?",
            title: "Re-Evaluate confirmation!",
            buttons: {
                success: {
                    label: "Yes",
                    className: "btn btn-default waves-effect waves-light w-100",
                    callback: function () {
                        showEvaluation(keyword, keyword_id, searches, cpc, profit, word_count, location, language, rowId, evaluated_id, isReevaluate)
                    }
                },
                danger: {
                    label: "No",
                    className: "btn btn-cancel waves-effect waves-light w-100",
                    callback: function () {
                        //Example.show("uh oh, look out!");
                    }
                }
            }
        })
    }

    function saveKeyword() {
        var search_keyword = '{{$keyword}}',
            language = '{{$selectedLanguage}}',
            location = '{{$selectedLocation}}';
        $.ajax({
            url: '{{URL::route('backend.tools.keywordresearch.saveKeyword')}}',
            type: 'post',
            datatype: 'json',
            data: {
                search_keyword: search_keyword,
                location: location,
                language: language,
                research_tmp_id: tmp_id
            },
            success: function (result, textStatus, jqXHR) {
                if (result.success) {
                    var data = result.data;
                    $('#btn-save_keyword').hide();
                    $('#saveMessage').show();
                    var href = "window.location.href='http://" + window.location.hostname + "/tools/keywordresearch/findResearch/" + data.id+"'" ;

                    $('#keyRes').append('<li id="searchLi' + data.id + '" class="dropdown submenu open"><a href="http://' + window.location.hostname + '/tools/keywordresearch/findResearch/' + data.id + '"><i class="fa fa-arrow-circle-right"></i> <span onclick="'+href+'">' + data.keyword + '</span></a></li>');
                    MINOVATE.customClick.bindNewEvent($('#searchLi' + data.id).find('a'));
                    $('#searchLi' + data.id).addClass("dropdown").addClass("submenu").addClass("open");
                    $('#searchLi' + data.id).find('a').append('<i class="fa fa-plus"></i>');
                    $('#searchLi' + data.id).append('<ul id="evalLi' + data.id + '" style="display: block;"></ul>');
                }
            },
            error: function (data) {
                $("#errorsBox").html("");
                $("#errorsBox").append('<div class="alert alert-lightred alert-dismissable fade in"><h4>Whoops!</h4><ul><li>' + data.responseJSON.message + '</li></ul></div>');
                $("#errorsBox").show();
            }
        });
    }
</script>
