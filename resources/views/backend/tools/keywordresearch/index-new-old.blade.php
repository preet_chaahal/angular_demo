@extends('layouts.backend.master')

@section('style')
    <link rel="stylesheet" href="{{ asset('assets/backend/css/vendor/selectize.default.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/loaders.css') }}">
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.5/angular.min.js"></script>
    <script type="text/javascript" src="{{ asset('assets/backend/js/zarboxa.js') }}"></script>
@stop

@section('content')
    <div id="fade"></div>
<!--     
    <div class="row mb-20">
        <div class="col-sm-12">
            <h4 class="page-title">Keyword Research</h4>
        </div>
    </div>
 -->

@include('layouts.backend.partials.messages')

<div ng-app="keywordRevealer">
<div ng-controller="keywordResearch">
    
    <div id="mainContainer">
        {{--
        <!-- this part will be moved to user profile -->
        <div class="row">
            <div class="col-lg-6">
                <div class="widget-panel widget-style-2 bg-white">
                    <div id="keywordSearchesBar">
                        <p class="font-600">Keyword Searches<span class="text-primary pull-right">0 / 0</span></p>
                        <div class="progress m-b-30">
                          <div class="progress-bar progress-bar-primary progress-animated wow animated" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="0" style="width: 0%">
                          </div>
                        </div>
                    </div>
                    <div id="keywordEvaluationsBar">
                        <p class="font-600">Keyword Evaluations <span class="text-custom pull-right">0 / 0</span></p>
                        <div class="progress">
                          <div class="progress-bar progress-bar-custom progress-animated wow animated" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="0" style="width: 0%">
                          </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="widget-panel widget-style-2 bg-white">
                    <div id="projectsBar">
                        <p class="font-600">Saved Projects <span class="text-primary pull-right">0 / 0</span></p>
                        <div class="progress m-b-30">
                          <div class="progress-bar progress-bar-primary progress-animated wow animated" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="0" style="width: 0%">
                          </div>
                        </div>
                    </div>
                    <div id="keywordsBar">
                        <p class="font-600">Saved Keywords <span class="text-custom pull-right">0 / 0</span></p>
                        <div class="progress">
                          <div class="progress-bar progress-bar-custom progress-animated wow animated" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="0" style="width: 0%">
                          </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        --}}



        <div class="row">
            <!-- col -->
            <div class="col-md-12">
                <!-- tile -->
                <div class="card-box" style="overflow: hidden;">
                    <div id="tabList">
                        <ul class="nav nav-tabs tabs m--20 mb-0" style="width:auto;">
                            <li class="active tab"><a href="#newSearch" data-toggle="tab" aria-expanded="false">Keyword Search</a></li>
                            <li class="tab"><a href="#saved" data-toggle="tab" aria-expanded="false">Saved Searches</a></li>
                            <li class="tab"><a href="#history" data-toggle="tab" aria-expanded="false">Today's Searches</a></li>
                        </ul>
                    </div>
                    <div class="tab-content" style="box-shadow:none">
                        <div class="tab-pane" id="saved">
                            <div class="row mt-20">
                                <div class="col-lg-12">
                                    <div class="pull-left">
                                        <h4 class="m-t-0 header-title"><b>Recent Keyword Research</b></h4>
                                        <p class="text-muted m-b-20 font-13">
                                            Previous saved and evaluated keywords.
                                        </p>
                                    </div>
                                    <div id="actions" class="input-group pull-right">
                                        <a href="{{ URL::route('backend.tools.keywordresearch.download') }}""
                                            class="btn btn-default waves-effect waves-light pull-right ml-20">Download CSV
                                            <span class="btn-label btn-label-right"><i class="fa fa-file-excel-o"></i></span>
                                        </a>
                                    </div>
                                    @if(Auth::user()->researchKeyword->count() > 0)
                                    <div class="table-container">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        Research
                                                    </th>
                                                    <th class="text-right">
                                                        Num of Keywords
                                                    </th>
                                                    <th class="text-right">
                                                        Best Difficulty
                                                    </th>
                                                    <th class="text-right">
                                                        Actions
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                                @foreach(Auth::user()->researchKeyword as $search)
                                                    <tr id="research_{{ $search->id }}" data-id="{{ $search->id }}" class="{{ ($search->research->count() > 0) ? ('parent-data') : ('') }}">
                                                        <td>
                                                            {{ $search->keyword }}
                                                        </td>
                                                        <td class="text-right">
                                                            {{ $search->research->count() }}
                                                        </td>
                                                        <td class="text-right best-difficulty">
                                                            N/A
                                                        </td>
                                                        <td class="text-right actions">
                                                            @if($search->keyword !== 'Saved Keywords')
                                                                @if($search->research->count() > 0)
                                                                    <a href="{{ URL::route('backend.tools.keywordresearch.evaluated', $search->research[0]->id) }}" onclick="showLoader(true)">
                                                                        <i class="action-icon icon-arrow-right-circle"></i>
                                                                    </a>
                                                                @else
                                                                    <a href="{{ URL::route('backend.tools.keywordresearch.findResearch',$search->id) }}" onclick="showLoader(true)">
                                                                        <i class="action-icon icon-arrow-right-circle"></i>
                                                                    </a>
                                                                @endif
                                                            @endif
                                                            <a href="javascript:void(0);" onclick="onDelete({{ $search->id }})" class="single-delete">
                                                                <i class="action-icon icon-trash"></i>
                                                            </a>
                                                        </td>
                                                    </tr>

                                                    @if($search->research->count() > 0)
                                                        <tr id="child-data-{{$search->id}}" class="child-data">
                                                            <td colspan="4" class="child-data-td">
                                                                <div class="sub-table">
                                                                    <table class="table saveTbl">
                                                                        <thead>
                                                                            <tr role="row">
                                                                                <th class>
                                                                                    Date
                                                                                </th><th>
                                                                                    Keyword
                                                                                </th><th class="text-right">
                                                                                    Monthly Searches
                                                                                </th><th class="text-right">
                                                                                    Adwords CPC
                                                                                </th><th class="text-right">
                                                                                    Estimated Profit
                                                                                </th><th class="text-right">
                                                                                    Word Count
                                                                                </th><th>
                                                                                    Location
                                                                                </th><th class="text-right">
                                                                                    Keyword Difficulty
                                                                                </th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody id="saveTbl">

                                                                        @foreach($search->research as $eval)
                                                                            <tr>
                                                                                <td>
                                                                                    {{ $eval->created_at->format('m/d/Y') }}
                                                                                </td>
                                                                                <td>
                                                                                    {{ $eval->keyword }}
                                                                                </td>
                                                                                <td class="text-right">
                                                                                    {{ number_format($eval->monthly_searches, 0, '', ',') }}
                                                                                </td>
                                                                                <td class="text-right">
                                                                                   ${{ $eval->keyword_cpc }}
                                                                                </td>
                                                                                <td class="text-right">
                                                                                    ${{ number_format($eval->estimated_profile, 0, '', ',') }}
                                                                                </td>
                                                                                <td class="text-right">
                                                                                    {{ $eval->word_count }}
                                                                                </td>
                                                                                <td>
                                                                                    {{ $eval->location }}
                                                                                </td>
                                                                                <td class="text-right pr-18">
                                                                                    {{ $eval->difficulty }}%
                                                                                </td>
                                                                            </tr>
                                                                        @endforeach

                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    @endif

                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    @else
                                        <div class="alert alert-warning cb">
                                            <strong>Oh No!</strong>
                                            You do not have any researching happening
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="history">
                            <div class="row mt-20">
                                <div class="col-lg-12">
                                    <div class="pull-left">
                                        <h4 class="m-t-0 header-title"><b>Recent Keyword Research</b></h4>
                                        <p class="text-muted m-b-20 font-13">
                                            Keyword research activity from the last 24 hours.
                                        </p>
                                    </div>
                                    @if($searchedCount->count() > 0)
                                        <div class="table-container">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th>
                                                            Researched Word
                                                        </th>
                                                        <th class="text-right">
                                                            Search Location
                                                        </th>
                                                        <th class="text-right">
                                                            Actions
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($searchedCount as $search)
                                                        <tr >
                                                            <td>
                                                                {{ $search->keyword }}
                                                            </td>
                                                            <td class="text-right">
                                                                {{ $search->location }}
                                                            </td>
                                                            <td class="text-right actions">
                                                                <a href="#" onclick="addNewSearch('{{ $search->keyword }}', '{{ $search->location }}')">
                                                                    <i class="action-icon icon-arrow-right-circle"></i>
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    @else
                                        <div class="alert alert-warning cb">
                                            <strong>Oh No!</strong>
                                            You do not have any research that have taken place over the past 24 hours.
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane active" id="newSearch">
                            <!-- search on new keyword -->
                            <div class="row">
                                <!-- col -->
                                <div class="col-md-12">
                                    <!-- Tab panes -->
                                    <div class="mb-0" style="padding:20px 0 0">
                                        <h4 class="m-t-0 header-title"><b>Keyword Search</b></h4>
                                        <p class="text-muted m-b-20 font-13">
                                            The first step in keyword research.
                                        </p>

                                        <div id="singleKeywordModal" class="row">
                                            <form>
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label" for="keyword">Please Enter a Keyword
                                                            @if(Auth::user()->can('import_keywords'))
                                                                or
                                                                <a href="javascript:void(0);" onclick="switchModals(1)" class="span5">
                                                                    Import Keywords
                                                                </a>
                                                            @endif
                                                        </label>
                                                        <input type="text" class="form-control" id="keyword" name="keyword">
                                                    </div>
                                                </div>

                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="control-label" for="tld">Search Location</label>
                                                        <select class="selectize tld-selectize" id="tld" name="tld"></select>
                                                    </div>
                                                </div>

                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="control-label" for="lang">Search Language</label>
                                                        <select class="selectize lang-selectize" id="lang" name="lang"></select>
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <div class="form-group pull-right mb-0">
                                                        <div class="input-group">
                                                            <button type="button" id="btnSubmit" class="btn submit btn-large btn-primary">
                                                                <span>Add New Search</span>
                                                                <img src="">
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>


                                        <div id="importKeywordsModal" class="row" style="display:none;">
                                            <form>
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label" for="areax_import">Please Import Keywords or
                                                            <a href="javascript:void(0);" onclick="switchModals(2)" class="span5">
                                                                Enter single Keyword
                                                            </a>
                                                        </label>
                                                        <textarea id="areax_import" class="form-control" rows="12" required name="keyword"
                                                            placeholder="Paste your keyword list here, one or more keyword per line... "></textarea>
                                                        <div>
                                                            <span id="result"></span>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label" for="import-tld">Search Location</label>
                                                        <select class="selectize tld-selectize" name="import-tld" id="import-tld"></select>
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label" for="import-lang">Search Language</label>
                                                        <select class="selectize lang-selectize" id="import-lang" name="import-lang"></select>
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <div class="form-group pull-right mb-0">
                                                        <label class="control-label">&nbsp;</label>
                                                        <div class="input-group">
                                                            <button type="button" id="keywordForm" class="btn btn-large btn-primary">
                                                                Import Keywords
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <!-- / tile -->
                                </div>
                                <!-- / col -->
                            </div>
                            <div style="height:20px;background:#ebeff2;margin:0 -25px;position:relative;top:20px"></div>
                            <div id="searchResults" style="display: none" class="m--20 mb-0 mt-20"></div>

                        </div>

                    </div>
                    <!-- /tile body -->
                </div>
                <!-- / tile -->
            </div>
            <!-- / col -->
        </div>

        <!-- Modal -->
        <div id="loader" class="modal-backdrop fade">
            <div class="line-spin-fade-loader center">
                <div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div>
            </div>
        </div>

    </div>

        <div class="newsearch-modal">
            <div class="modal fade" id="mymodal-new-search" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="row">
                            <!-- col -->
                            <div class="col-md-12">
                                <!-- Tab panes -->
                                <div class="card-box mb-0">
                                    <h4 class="m-t-0 header-title"><b>Keyword Search</b></h4>
                                    <p class="text-muted m-b-20 font-13">
                                        The first step in keyword research.
                                    </p>

                                    <div id="singleKeywordModal" class="row">
                                        <form>
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label" for="keyword">Please Enter a Keyword or
                                                        @if(Auth::user()->can('import_keywords'))
                                                            <a href="javascript:void(0);" onclick="switchModals(1)" class="span5">
                                                                Import Keywords
                                                            </a>
                                                        @endif
                                                    </label>
                                                    <input type="text" class="form-control" id="keyword" name="keyword">
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label" for="tld">Search Location</label>
                                                    <select class="selectize tld-selectize" id="tld" name="tld"></select>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label" for="lang">Search Language</label>
                                                    <select class="selectize lang-selectize" id="lang" name="lang"></select>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group pull-right mb-0">
                                                    <label class="control-label">&nbsp;</label>
                                                    <div class="input-group">
                                                        <button type="button" type="submit" id="btnSubmit" class="btn submit btn-large btn-primary">
                                                            Add New Search
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>

                                    <div id="importKeywordsModal" class="row" style="display:none;">
                                        <form>
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label" for="areax_import">Please Import Keywords or
                                                        <a href="javascript:void(0);" onclick="switchModals(2)" class="span5">
                                                            Enter single Keyword
                                                        </a>
                                                    </label>
                                                    <textarea id="areax_import" class="form-control" rows="12" required name="keyword"
                                                        placeholder="Paste your keyword list here, one or more keyword per line... "></textarea>
                                                    <div>
                                                        <span id="result"></span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label" for="import-tld">Search Location</label>
                                                    <select class="selectize tld-selectize" name="import-tld" id="import-tld"></select>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label" for="import-lang">Search Language</label>
                                                    <select class="selectize lang-selectize" id="import-lang" name="import-lang"></select>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group pull-right mb-0">
                                                    <label class="control-label">&nbsp;</label>
                                                    <div class="input-group">
                                                        <button type="button" id="keywordForm" class="btn btn-large btn-primary">
                                                            Import Keywords
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <!-- / tile -->
                            </div>
                            <!-- / col -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
<!-- </div> -->
@stop

@section('scripts')
    <script src="{{ asset('assets/backend/js/vendor/selectize/selectize.js') }}"></script>
    <script type="text/javascript">
// ===========================================================================
        var keywords = {!! json_encode(Auth::user()->researchKeyword->toArray()) !!};
        for (var i = 0; i < keywords.length; i++) {
            if ( keywords[i].research.length ) {
                keywords[i].bestDifficulty = 100;
                for (var j = 0; j < keywords[i].research.length; j++) {
                    if ( keywords[i].research[j].difficulty < keywords[i].bestDifficulty ) {
                        keywords[i].bestDifficulty = keywords[i].research[j].difficulty;
                    }
                }
            } else {
                keywords[i].bestDifficulty = 'N/A';
            }
            var bestDiffPostfix = $.isNumeric( keywords[i].bestDifficulty ) ? '%' : '';
            $('#research_' + keywords[i].id + ' .best-difficulty').text(keywords[i].bestDifficulty + bestDiffPostfix);
        }

        function getUserCountsAndLimits() {
            $.ajax({
                url: '{{ URL::route('backend.tools.keywordresearch.userCountsAndLimits')}}',
                type: 'get',
                success: function (data, textStatus, jqXHR) {
                    if (data.success) {
                        $('#keywordSearchesBar span').text(data.searchedCount + " / " + data.userResearchLimit);
                        $('#keywordSearchesBar .progress-bar')
                            .attr('aria-valuenow', data.searchedCount)
                            .attr('aria-valuemax', data.userResearchLimit)
                            .width(data.searchedCount * 100 / data.userResearchLimit + '%');

                        $('#keywordEvaluationsBar span').text(data.evaluatedCount + " / " + data.userEvolutionLimit);
                        $('#keywordEvaluationsBar .progress-bar')
                            .attr('aria-valuenow', data.evaluatedCount)
                            .attr('aria-valuemax', data.userEvolutionLimit)
                            .width(data.evaluatedCount * 100 / data.userEvolutionLimit + '%');

                        var savedKeywords = 0;
                        var projectsNum = keywords.length;
                        for (var i = 0; i < projectsNum; i++) {
                            savedKeywords += keywords[i].research.length;
                        }
                        $('#keywordsBar span').text(savedKeywords + " / " + data.saveKeywordsLimit);
                        $('#keywordsBar .progress-bar')
                            .attr('aria-valuenow', savedKeywords)
                            .attr('aria-valuemax', data.saveKeywordsLimit)
                            .width(savedKeywords * 100 / data.saveKeywordsLimit + '%');

                        $('#projectsBar span').text(projectsNum + " / " + data.saveProjectsLimit);
                        $('#projectsBar .progress-bar')
                            .attr('aria-valuenow', projectsNum)
                            .attr('aria-valuemax', data.saveProjectsLimit)
                            .width(projectsNum * 100 / data.saveProjectsLimit + '%');
                    }
                }
            });
        };
        getUserCountsAndLimits();

        function switchModals(id) {
            if (id === 1) {
                $('#singleKeywordModal').hide();
                $('#importKeywordsModal').show();
            } else {
                $('#singleKeywordModal').show();
                $('#importKeywordsModal').hide();
            }
        };

        $('.parent-data').on("click", function () {
            var isExpanded = $(this).hasClass('parent-expand');
            var id = $(this).attr('data-id');
            $(this).removeClass('parent-expand');
            $('#child-data-' + id).removeClass('expand');
            $('.child-data .sub-table').removeClass('sub-table-open');

            if (!isExpanded) {
                $('.parent-data').removeClass('parent-expand');
                $('.child-data').removeClass('expand');
                $(this).addClass('parent-expand');
                $('#child-data-' + id).addClass('expand');
                $('#child-data-' + id + ' .sub-table').addClass('sub-table-open');
            }
        });

        $('.actions a').on('click', function(event){
            event.stopPropagation();
        });

        function showLoader(show) {
            if (show === true) {
                $("#loader").show();
                $("#loader").addClass('in');
            } else {
                $("#loader").hide();
                $("#loader").removeClass('in');
            }
        };
// ===========================================================================
    </script>

    <script src="{{ asset('assets/backend/js/vendor/easypiechart/jquery.easypiechart.min.js') }}"></script>

    <script>
        $(".prog").hide();
        showLoader(false);
        var tldOptions = {!! $tldOptions !!};
        var languages = {!! $languages !!};

        $('.tld-selectize').selectize({
            plugins: ['remove_button'],
            valueField: 'name',
            labelField: 'engine',
            searchField: ['name', 'engine'],
            delimiter: ',',
            persist: false,
            hideSelected: true,
            openOnFocus: true,
            items: ['{{ $selectedEngine }}'],
            render: {
                item: function (item, escape) {
                    return '<div>' +
                            '<span>' + item.engine + ' ‒ ' + item.name + '</span>' +
                            '</div>';
                },
                option: function (item, escape) {
                    return '<div>' +
                            '<span>' + item.engine + ' ‒ ' + item.name + '</span>' +
                            '</div>';
                }
            },
            options: tldOptions
        });

        $('.lang-selectize').selectize({
            plugins: ['remove_button'],
            valueField: 'code',
            labelField: 'name',
            searchField: ['name', 'code'],
            delimiter: ',',
            persist: false,
            hideSelected: true,
            openOnFocus: true,
            items: ['{{ $selectedLanguage }}'],
            render: {
                item: function (item, escape) {
                    return '<div>' +
                            '<span>' + item.name + ' (' + item.code + ')</span>' +
                            '</div>';
                },
                option: function (item, escape) {
                    return '<div>' +
                            '<span>' + item.name + ' (' + item.code + ')</span>' +
                            '</div>';
                }
            },
            options: languages
        });
    </script>
    <script>
        $(document).on("click", ".submit", function () {
            addNewSearch();
        });

        function addNewSearch(keyword, tld, lang) {
            showLoader(true);

            keyword = keyword === undefined ? $('#keyword').val() : keyword;
            tld = tld === undefined ? $('#tld').val() : tld;
            lang = lang === undefined ? $('#lang').val() : tld;

            var ajaxUrl = '{{ URL::route('backend.tools.keywordresearch.insert') }}';
            // load content
            $('#mymodal-new-search').modal('hide');

            $.ajax({
                url: ajaxUrl,
                type: 'post',
                data: {
                    'keyword': keyword,
                    'tld': tld,
                    'lang': lang,
                    'is_ajax_view': 1
                },
                success: function (res) {
                    if(res.success == false){
                        $("#errMsg").html(res.message);
                        $("#error").show();
                        setTimeout(function () {
                            $('#error').fadeOut('fast');
                        }, 3000);
                        showLoader(false);
                        document.getElementById("btnSubmit").disabled = false;
                    }
                    else {
                        showLoader(false);
                        $('#searchResults').show().html(res);
                    }
                },
                error:function(result){
                    $("#errMsg").html("");
                    var err = JSON.parse(result.responseText);
                    if (err.apiErrMessage) {
                        $("#errMsg").append(err.apiErrMessage);
                    } else {
                        err.keyword.forEach(function (item) {
                            $("#errMsg").append(item);
                        });
                    }
                    $("#error").show();
                    setTimeout(function () {
                        $('#error').fadeOut('fast');
                    }, 5000);
                    showLoader(false);
                    document.getElementById("btnSubmit").disabled = false;
                }
            });
        }

        function onSubmitEvent() {
            document.getElementById("btnSubmit").disabled = true;
        }

        $('#keywordForm').on('click', function () {
            showLoader(true);
            $('#mymodal-new-search').modal('hide');

            var el = document.getElementById("areax_import");

            var lines = el.value.split('\n').length;
            var values = el.value.split('\n');
            var charErr = false;

            for (i = 0; i < lines; i++) {
                var chars = values[i].split('').filter(function (v) {
                    return v != '\n';
                }).length;

                if (chars > 80) {
                    charErr = true;
                    break;
                }
            }

            if (lines > 800) {
                $('#result').html('You have enter more than ' + lines + ' lines');
                return false;
            }
            if (charErr) {
                $('#result').html('You have enter more than 80 characters');
                return false;
            }

            $('#result').html('');

            var ajaxUrl = '{{ URL::route('backend.tools.keywordresearch.importkeyword') }}';
            // load content
            $.ajax({
                url: ajaxUrl,
                type: 'post',
                data: {
                    'keyword': $('#areax_import').val(),
                    'tld': $('#import-tld').val(),
                    'lang': $('#import-lang').val()
                },
                success: function (data) {
                    if(data.success == false){
                        $("#errMsg").html(data.message);
                        $("#error").show();
                        setTimeout(function () {
                            $('#error').fadeOut('fast');
                        }, 3000);
                        showLoader(false);
                    }
                    else {
                        showLoader(false);
                        $('#mainContainer').html(data);
                    }
                },
                error:function(result){
                    $("#errMsg").html("");
                    showLoader(false);
                    var err = JSON.parse(result.responseText);
                    if (err.apiErrMessage) {
                        $("#errMsg").append(err.apiErrMessage);
                    } else {
                        err.keyword.forEach(function (item) {
                            $("#errMsg").append(item);
                        });
                    }
                    $("#error").show();
                    setTimeout(function () {
                        $('#error').fadeOut('fast');
                    }, 5000);
                    document.getElementById("btnSubmit").disabled = false;
                }
            });
        });
    </script>

    <script type="text/javascript">

        function onDelete(val) {
            singleDeleteKeyword(val);
        }

        function singleDeleteKeyword(val) {
            bootbox.dialog({
                message: "Are you sure you want to perform delete?",
                title: "Delete confirmation!",
                buttons: {
                    success: {
                        label: "Yes",
                        className: "btn btn-danger waves-effect waves-light w-100",
                        callback: function () {
                            //Example.show("great success");
                            $.ajax({//1212121
                                url: '{{ URL::route('backend.tools.keywordresearch.destroyAjax') }}',
                                data: {'id': val},
                                type: 'get',
                                dataType: 'json',
                                success: function (result) {
                                    bootbox.hideAll();
                                    $("#research_" + val).remove();
                                    $("#child-data-" + val).remove();
                                    // $('#saveTbl').DataTable().ajax.reload();
                                }
                            })
                            keywords = keywords.filter(function(keyword) {
                                return keyword.id !== val;
                            });
                            getUserCountsAndLimits();
                            return true;
                        }
                    },
                    danger: {
                        label: "No",
                        className: "btn btn-default waves-effect waves-light w-100",
                        callback: function () {
                            //Example.show("uh oh, look out!");
                        }
                    },
                }
            });
        }
    </script>
    <script type="text/javascript" id="pap_x2s6df8d" src="https://keywordrevealer.postaffiliatepro.com/scripts/vdzfn3n4jm8"></script>
    <script type="text/javascript">
    PostAffTracker.setAccountId('default1');
    try {
    PostAffTracker.track();
    } catch (err) { }
    </script>
</div>
</div>
@stop
