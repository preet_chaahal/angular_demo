@extends('layouts.backend.master')

@section('style')
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/plug-ins/1.10.10/sorting/numeric-comma.js"/>
    <style>
        .collapse-sidebar {
            padding-top: 19px !important;
        }
    </style>
@stop
@section('content')

    <div class="page page-dashboard">
        <div class="pageheader">
            <h2>Keyword Research <span>// uncover low-competition keywords</span></h2>
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <a href="{{ URL::route('dashboard') }}"><i class="fa fa-home"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href="{{ URL::route('backend.tools.keywordresearch') }}">Keyword Research</a>
                    </li>
                </ul>
            </div>
        </div>

                <!-- row -->
        <div class="row">
            <!-- col -->
            <div class="col-md-12">
                <section class="tile tile-simple">
                    <!-- tile body -->
                    <div class="tile-body p-0">
                        <!-- tabpanel -->
                        <div role="tabpanel">
                            <!-- Nav tabs -->
                            <ul id="tabList" class="nav nav-tabs tabs-dark" role="tablist">
                                <li class="tabs-title pull-right" style="padding-right: 15px;">Preview Keyword Research:
                                    <strong>{{$research_keyword}}</strong></li>
                            </ul>

                            <!-- Tab panes -->
                            <div id="tabContent" class="tab-content">
                                <!-- saved -->
                                <div role="tabpanel" class="tab-pane" id="saved">
                                    <div id="savedContent">
                                        <div class="alert alert-info">
                                            Please wait while we return your saved researched words
                                        </div>
                                    </div>
                                </div>
                                <!-- saved -->

                                <!-- results -->
                                <div role="tabpanel" class="tab-pane active" id="results">
                                    @include('layouts.backend.partials.messages')

                                    <div class="alert alert-warning" id="alert-error">
                                        <strong>Sorry</strong>
                                        an error occurred trying to retrieve the data, please try again.
                                        <div class="text-right">
                                            <a href="#" class="btn btn-primary"
                                               onclick="location.reload(); showWait(); return false;">Retry</a>
                                        </div>
                                    </div>
                                    <div class="ajax-hidden" id="ajax-wait">
                                        <div class="alert alert-info">
                                            <p class="text-center">
                                                <strong>Please Wait ...</strong>
                                            </p>
                                        </div>
                                    </div>
                            </div> <!-- results -->

                        </div> <!-- tab-content -->
                    </div> <!-- tabpanel -->
            </div> <!-- /tile body -->
            </section>
        </div> <!-- /col -->
    </div> <!-- /row -->
    </div> <!-- /row -->
@stop

@section('modals')
@stop

@section('scripts')
    <script src="{{ asset('assets/backend/js/vendor/easypiechart/jquery.easypiechart.min.js') }}"></script>

    <script>

        @if (!empty($researchData))
            showEvaluation('{{ $researchData['keyword'] }}','{{ $researchData['keyword_id'] }}', '{{ $researchData['searches'] }}', '{{ $researchData['cpc'] }}', '{{ $researchData['profit'] }}', '{{ $researchData['word_count'] }}', '{{ $researchData['location'] }}')
        @endif

            function showWait() {
                $("#alert-error").hide();
                $("#ajax-wait").show();
            }

            function showEvaluation(keyword, keyword_id, searches, cpc, profit, word_count, location) {
                var d = new Date();

                $("#researchTable_wrapper").hide();
                $("#saved").hide();

                addTab(keyword, keyword_id, searches, cpc, profit, word_count, location, d.getTime());
            }

            function addTab(displayName, keyword_id, searches, cpc, profit, word_count, location, elementId) {
                var ajaxUrl = '{{ URL::route('backend.tools.keywordresearch.evaluate', ['ts_keyword', 'ts_keywordId','ts_searches', 'ts_cpc', 'ts_profit', 'ts_word_count', 'ts_location'] ) }}'
                        .replace('ts_keyword', displayName)
                        .replace('ts_keywordId', keyword_id)
                        .replace('ts_searches', searches)
                        .replace('ts_cpc', cpc)
                        .replace('ts_profit', profit)
                        .replace('ts_word_count', word_count)
                        .replace('ts_location', location);

                var html = '';
                html += '<div id="holding_' + elementId + '">';
                html += '<h3 class="text-center">Please wait while your keyword is evaluated ...</h3>';
                html += '<div class="text-center"><img src="{{ asset('assets/backend/images/success-loader.gif') }}"></div>';
                html += '</div>';
                html += '<div id="content_' + elementId + '">';
                html += '<div class="text-right">';
                html += '</div>';
                html += '</div>';

                // add tab
                $("#tabList").append('<li id="newtab_' + elementId + '"><a href="#' + elementId + '" aria-controls="' + elementId + '" role="tab" data-toggle="tab">' + displayName + '</a></li>')
                // add panel
                $("#tabContent").append('<div role="tabpanel" class="tab-pane active" id="' + elementId + '">' + html + '</div>')
                // select new tab
                $('#tabList a:last').tab('show')

                // call ajax
                $.ajax({
                    url: ajaxUrl,
                    type: 'get',
                    dataType: 'json',
                    success: function (data, textStatus, jqXHR) {
                        if (data.success) {
                            $("#errorsBox").html("");
                            $('#holding_' + elementId).hide();
                            $('#content_' + elementId).append(data.data.html);
                        }
                    },
                    error: function (data) {
                        $("#errorsBox").html("");
                        $("#errorsBox").append('<div class="alert alert-lightred alert-dismissable fade in"><h4>Whoops!</h4><ul><li>' + data.responseJSON.message + '</li></ul></div>');
                        $("#errorsBox").show();
                        closeTab(elementId);
                    }
                });
            }


    </script>
@stop