@extends('layouts.backend.master')

@section('style')
    <link rel="stylesheet" href="{{ asset('assets/backend/css/vendor/selectize.default.css') }}">

    <style>
        .selectItem >span { color: #000; }
    </style>
@stop


@section('content')

    <div class="page page-dashboard">
        <div class="pageheader">
            <h2>Keyword Research <span>// uncover low-competition keywords</span></h2>
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <a href="{{ URL::route('dashboard') }}"><i class="fa fa-home"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href="{{ URL::route('backend.tools.keywordresearch') }}">Keyword Research</a>
                    </li>
                </ul>
            </div>
        </div>

        @include('layouts.backend.partials.messages')

        <!-- row -->
        <div class="row">
            <!-- col -->
            <div class="col-md-12">
                <!-- tile -->
                <section class="tile tile-simple">
                    <!-- tile body -->
                    <div class="tile-body p-0">
                        <div role="tabpanel">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs tabs-dark" role="tablist">
                                <li><a href="{{ URL::route('backend.tools.keywordresearch') }}">Listing</a></li>
                                <li class="active"><a href="{{ URL::route('backend.tools.keywordresearch.add') }}">New Keyword</a></li>
                                <li class="tabs-title pull-right" style="padding-right: 15px;">Saved <strong>Domains</strong></li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tile-body">

                                <form method="POST" action="{{ URL::route('backend.tools.keywordresearch.insert') }}">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                    <!-- keyword -->
                                    <div class="form-group">
                                        <label for="keyword">Keyword</label>
                                        <input type="text" class="form-control" id="keyword" name="keyword">
                                    </div> <!-- keyword -->

                                    <!-- location -->
                                    <div class="form-group">
                                        <label for="location">Search Location</label>
                                        <select id="selectize" name="tld"></select>
                                    </div> <!-- location -->

                                    <!-- actions -->
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-8 col-md-offset-2">
                                                <input type="submit" class="btn btn-success btn-lg btn-block" value="Add New Search">
                                            </div>
                                        </div>
                                    </div> <!-- actions -->

                                </form>

                            </div>
                            <!-- /Tab panes -->
                        </div>
                    </div>
                    <!-- /tile body -->
                </section>
                <!-- / tile -->
            </div>
            <!-- / col -->
        </div>
        <!-- / row -->

    </div>
@stop


@section('scripts')
    <script src="{{ asset('assets/backend/js/vendor/selectize/selectize.js') }}"></script>

    <script>
        var options = {!! $tldOptions !!};

        $('#selectize').selectize({
            plugins: ['remove_button'],
            valueField: 'engine',
            labelField: 'name',
            searchField: ['name', 'engine'],
            delimiter: ',',
            persist: false,
            hideSelected: true,
            openOnFocus: true,
            items: ['{{ $selectedEngine }}'],
            render: {
                item: function (item, escape) {
                    var name = escape(item.name).match(/\(.+\)/g);
                    name = name !== null ? name[0].substring(1, name[0].length - 1) : '';
                    return '<div>' +
                            '<span>' + name + '</span>' +
                            (item.engine ? ' <span> ‒ ' + escape(item.engine) + '</span>' : '') +
                            '</div>';
                },
                option: function (item, escape) {
                    var name = escape(item.name).match(/\(.+\)/g);
                    name = name !== null ? name[0].substring(1, name[0].length - 1) : '';
                    return '<div class="selectItem">' +
                            '<span>' + name + '</span>' +
                            (item.engine ? ' <span> ‒ ' + escape(item.engine) + '</span>' : '') +
                            '</div>';
                }
            },
            options: options
        });
    </script>
@stop
