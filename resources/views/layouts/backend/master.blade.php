<!DOCTYPE html>
<html lang="en">
    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Keyword Revealer</title>
        <link rel="icon" type="image/ico" href="{{ asset('favicon.ico') }}" />
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- vendor css files -->
        <link rel="stylesheet" href="{{ asset('assets/backend/css/vendor/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/backend/css/vendor/animate.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/backend/css/vendor/font-awesome.min.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/backend/js/vendor/animsition/css/animsition.min.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/backend/js/vendor/datetimepicker/css/bootstrap-datetimepicker.min.css') }}">

        <link rel="stylesheet" type="text/css" href="{{ asset('assets/backend/css/custom.css') . '?' . filemtime(public_path('assets/frontend/css/custom.css')) }}">

        <link rel="stylesheet" href="{{ asset('assets/backend/js/vendor/daterangepicker/daterangepicker-bs3.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/backend/js/vendor/datatables/css/jquery.dataTables.min.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/backend/js/vendor/datatables/datatables.bootstrap.min.css') }}">

        <!-- project main css files -->
        <link rel="stylesheet" href="{{ asset('assets/backend/css/main.css?1234567890') }}">

        <link rel="stylesheet" href="{{ asset('assets/backend/css/cb_style.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/backend/css/cb_responsive.css') }}">

        <link rel="stylesheet" href="{{ asset('assets/backend/css/light/core.css?1234567890') }}">
        <link rel="stylesheet" href="{{ asset('assets/backend/css/light/components.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/backend/css/light/icons.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/backend/css/light/pages.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/backend/css/light/responsive.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/backend/css/jquery-confirm.min.css') }}">
        <!--/ stylesheets -->

        @yield('style')

        <script src="{{ asset('assets/backend/js/vendor/modernizr/modernizr-2.8.3-respond-1.4.2.min.js') }}"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script src="{{ asset('assets/backend/js/js.jscookie.js') }}"></script>
        <script>
            $( document ).ready(function() {
                if (Cookies.get('sidebar-collapse') != null) {
                    $('body').addClass('sidebar-sm');
                }
            });

            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-42623625-1', 'auto');
            ga('send', 'pageview');


            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
			n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
			n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
			t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
			document,'script','https://connect.facebook.net/en_US/fbevents.js');
			fbq('init', '1346334148773436'); // Insert your pixel ID here.
			fbq('track', 'PageView');

        </script>

        <!-- Hotjar Tracking Code for keywordrevealer.com -->
        <script>
            (function(h,o,t,j,a,r){
                h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
                h._hjSettings={hjid:674052,hjsv:6};
                a=o.getElementsByTagName('head')[0];
                r=o.createElement('script');r.async=1;
                r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
                a.appendChild(r);
            })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
        </script>

        <!-- Global site tag (gtag.js) - Google AdWords: 805251890 -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=AW-805251890"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());
        
          gtag('config', 'AW-805251890');
        </script>

		<noscript>
			<img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=1346334148773436&ev=PageView&noscript=1"/>
		</noscript>
<!--
        <script type="text/javascript">
            (function(){var t=document.createElement("script");t.type="text/javascript",t.async=!0,t.src='https://cdn.firstpromoter.com/fprom.js',t.onload=t.onreadystatechange=function(){var t=this.readyState;if(!t||"complete"==t||"loaded"==t)try{$FPROM.init("h4qvnjws",".keywordrevealer.com")}catch(t){}};var e=document.getElementsByTagName("script")[0];e.parentNode.insertBefore(t,e)})();</script>
-->      

        <script type="text/javascript">
        (function(){var t=document.createElement("script");t.type="text/javascript",t.async=!0,t.src='https://cdn.firstpromoter.com/fprom.js',t.onload=t.onreadystatechange=function(){var t=this.readyState;if(!t||"complete"==t||"loaded"==t)try{$FPROM.init("rxqscow8",".kwrev.com")}catch(t){}};var e=document.getElementsByTagName("script")[0];e.parentNode.insertBefore(t,e)})();</script>

        @yield('head-script')

    </head>

    <body id="minovate" class="appWrapper fixed-left">

        <!-- Begin page -->
        <div id="wrapper" class="forced enlarged">

            <!-- Top Bar Start -->
            <div class="topbar">

                <!-- LOGO -->
                <div class="topbar-left">
                    <div class="logo-center">
                        <a class="logo" href="{{ URL::route('dashboard') }}">
                            <img src="{{ asset('assets/backend/images/logo.png') }}" alt="Keyword Revealer" class="img-responsive img-large">
                            {{--
                            <img src="{{ asset('assets/frontend/images/logo_small.png') }}" alt="Keyword Revealer" class="img-responsive img-small">
                            --}}
                        </a>
                    </div>
                </div>

                <!-- Button mobile view to collapse sidebar menu -->
                <div class="navbar navbar-default" role="navigation">
                    <div class="container">
                        <div class="">
                            <div class="pull-left">
                                <button class="button-menu-mobile open-left">
                                    <i class="ion-navicon"></i>
                                </button>
                                <span class="clearfix"></span>
                            </div>

                            <ul class="nav navbar-nav navbar-right pull-right">
                                <li class="navbar-item">
                                    <a href="/auth/subscription" class="waves-effect waves-light">Pricing</a>
                                </li>
                                <li class="navbar-item">
                                    <a href="/blog" class="waves-effect waves-light">Blog</a>
                                </li>
                                <li class="navbar-item">
                                    <a href="https://docs.keywordrevealer.com/" class="waves-effect waves-light">Support</a>
                                </li>
                                <li class="navbar-item">
                                    <a href="/contact" class="waves-effect waves-light">Contact Us</a>
                                </li>
                                <li class="dropdown">
                                    <a href="" class="dropdown-toggle profile" data-toggle="dropdown" aria-expanded="true">
                                        <img src="{{ Auth::user()->gravatar }}" alt="user-img" class="img-circle">
                                        <span id="user_email">{{ Auth::user()->username }}</span>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li><a href="{{ URL::route('auth.profile') }}"><i class="fa fa-user m-r-5"></i> Account</a></li>
                                        <li><a href="{{ URL::route('auth.logout') }}"><i class="fa fa-sign-out m-r-5"></i>Logout</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Top Bar End -->

            <!-- ========== Left Sidebar Start ========== -->

            <div class="left side-menu">
                <div class="sidebar-inner slimscrollleft">
                    <!--- Divider -->
                    <div id="sidebar-menu">

                        @include('layouts.backend.partials.navigation')

                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <!-- Left Sidebar End -->

            <!-- Start right Content here -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">

                        @yield('content')

                    </div>
                </div>
            </div>


        </div>

        @yield('modals')


        <script>window.jQuery || document.write('<script src="{{ asset('assets/backend/js/vendor/jquery/jquery-1.11.2.min.js') }}"><\/script>')</script>
        <script src="{{ asset('assets/backend/js/custom.js') }}"></script>

        <script src="{{ asset('assets/backend/js/highcharts.js') }}"></script>
        <script src="{{ asset('assets/backend/js/vendor/bootstrap/bootstrap.min.js') }}"></script>

        <script src="{{ asset('assets/backend/js/vendor/jRespond/jRespond.min.js') }}"></script>

        <script src="{{ asset('assets/backend/js/vendor/d3/d3.min.js') }}"></script>
        <script src="{{ asset('assets/backend/js/vendor/d3/d3.layout.min.js') }}"></script>

        <script src="{{ asset('assets/backend/js/vendor/rickshaw/rickshaw.min.js') }}"></script>

        <script src="{{ asset('assets/backend/js/vendor/sparkline/jquery.sparkline.min.js') }}"></script>
        <script src="{{ asset('assets/backend/js/vendor/chartjs/Chart.bundle.min.js') }}"></script>

        <script src="{{ asset('assets/backend/js/vendor/slimscroll/jquery.slimscroll.min.js') }}"></script>
        <script src="{{ asset('assets/backend/js/jquery.app.js') }}"></script>

        <script src="{{ asset('assets/backend/js/vendor/animsition/js/jquery.animsition.min.js') }}"></script>

        <script src="{{ asset('assets/backend/js/vendor/daterangepicker/moment.min.js') }}"></script>
        <script src="{{ asset('assets/backend/js/vendor/daterangepicker/daterangepicker.js') }}"></script>

        <script src="{{ asset('assets/backend/js/vendor/screenfull/screenfull.min.js') }}"></script>

        <script src="{{ asset('assets/backend/js/vendor/flot/jquery.flot.min.js') }}"></script>
        <script src="{{ asset('assets/backend/js/vendor/flot-tooltip/jquery.flot.tooltip.min.js') }}"></script>
        <script src="{{ asset('assets/backend/js/vendor/flot-spline/jquery.flot.spline.min.js') }}"></script>

        <script src="{{ asset('assets/backend/js/vendor/easypiechart/jquery.easypiechart.min.js') }}"></script>

        <script src="{{ asset('assets/backend/js/vendor/raphael/raphael-min.js') }}"></script>
        <script src="{{ asset('assets/backend/js/vendor/morris/morris.min.js') }}"></script>

        <script src="{{ asset('assets/backend/js/vendor/owl-carousel/owl.carousel.min.js') }}"></script>

        <script src="{{ asset('assets/backend/js/vendor/datetimepicker/js/bootstrap-datetimepicker.min.js') }}"></script>

        <script src="{{ asset('assets/backend/js/vendor/datatables/js/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('assets/backend/js/vendor/datatables/extensions/dataTables.bootstrap.js') }}"></script>

        <script src="{{ asset('assets/backend/js/vendor/chosen/chosen.jquery.min.js') }}"></script>

        <script src="{{ asset('assets/backend/js/vendor/summernote/summernote.min.js') }}"></script>

        <script src="{{ asset('assets/backend/js/vendor/coolclock/coolclock.js') }}"></script>
        <script src="{{ asset('assets/backend/js/vendor/coolclock/excanvas.js') }}"></script>
        <script src="{{ asset('assets/backend/js/main.js') }}"></script>
        <script src="{{ asset('assets/backend/js/vendor/blockui/blockui.js') }}"></script>
        <script src="{{ asset('assets/backend/js/vue.min.js') }}"></script>
        <script src="{{ asset('assets/backend/js/bootbox.min.js') }}"></script>
        <script src="{{asset('assets/backend/js/jquery-confirm.min.js')}}"></script>
        <!--/ custom javascripts -->

        <script>
            Number.prototype.formatNumber = function(c, d, t){
                var n = this,
                    c = isNaN(c = Math.abs(c)) ? 2 : c,
                    d = d == undefined ? "." : d,
                    t = t == undefined ? "," : t,
                    s = n < 0 ? "-" : "",
                    i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
                    j = (j = i.length) > 3 ? j % 3 : 0;
                return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
            };

            document.body.addEventListener("evaluateClick", function(evt) {

                if(evt.detail.isEvaluated == true)
                {
                    $('#diff'+evt.detail.evaluated_id).css('width',evt.detail.difficulty+'%');
                }
                else
                {
                    if($('#searchLi'+evt.detail.keyword_id).length > 0)
                    {
                        if($('#searchLi'+evt.detail.keyword_id).has("ul").length)
                        {
                            $('#evalLi'+evt.detail.keyword_id).append('<li id="eval'+evt.detail.evaluated_id+'" style="margin:0px 0px 0px 15px!important;"><a href="http://' + window.location.hostname+'/tools/keywordresearch/evaluated/'+evt.detail.evaluated_id+'"><i class="fa fa-arrow-circle-o-right"></i>'+ evt.detail.displayName+'</a><div class="progress progress-xs" style="margin:0px 15px 0px 30px!important;"><div class="progress-bar progress-bar-blue" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width:'+ evt.detail.difficulty+'%"></div></div></li>');
                        }
                        else {
                            $('#searchLi'+evt.detail.keyword_id).addClass("dropdown").addClass("submenu").addClass("open");
                            $('#searchLi'+evt.detail.keyword_id).find('a').append('<i class="fa fa-plus"></i>');
                            $('#searchLi'+evt.detail.keyword_id).append('<ul id="evalLi'+evt.detail.keyword_id+'" style="display: block;"></ul>');
                            $('#evalLi'+evt.detail.keyword_id).append('<li id="eval'+evt.detail.evaluated_id+'" style="margin:0px 0px 0px 15px!important;"><a href="http://' + window.location.hostname+'/tools/keywordresearch/evaluated/'+evt.detail.evaluated_id+'"><i class="fa fa-arrow-circle-o-right"></i>'+ evt.detail.displayName+'</a><div class="progress progress-xs" style="margin:0px 15px 0px 30px!important;"><div class="progress-bar progress-bar-blue" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width:'+ evt.detail.difficulty+'%"></div></div></li>');
                            MINOVATE.customClick.bindNewEvent($('#searchLi'+evt.detail.keyword_id).find('a'));
                        }
                    }
                    else {
                        $('#keyRes').append('<li id="searchLi'+evt.detail.keyword_id+'" class="dropdown submenu open"><a href="http://' + window.location.hostname+'/tools/keywordresearch/findResearch/'+evt.detail.keyword_id+'"><i class="fa fa-arrow-circle-right"></i> <span>'+evt.detail.search_keyword+'</span></a></li>');
                        MINOVATE.customClick.bindNewEvent($('#searchLi'+evt.detail.keyword_id).find('a'));
                        $('#searchLi'+evt.detail.keyword_id).addClass("dropdown").addClass("submenu").addClass("open");
                        $('#searchLi'+evt.detail.keyword_id).find('a').append('<i class="fa fa-plus"></i>');
                        $('#searchLi'+evt.detail.keyword_id).append('<ul id="evalLi'+evt.detail.keyword_id+'" style="display: block;"></ul>');
                        $('#evalLi'+evt.detail.keyword_id).append('<li id="eval'+evt.detail.evaluated_id+'" style="margin:0px 0px 0px 15px!important;"><a href="http://' + window.location.hostname+'/tools/keywordresearch/evaluated/'+evt.detail.evaluated_id+'"><i class="fa fa-arrow-circle-o-right"></i>'+ evt.detail.displayName+'</a><div class="progress progress-xs" style="margin:0px 15px 0px 30px!important;"><div class="progress-bar progress-bar-blue" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width:'+ evt.detail.difficulty+'%"></div></div></li>');
                    }
                }
            }, false);
        </script>

        @yield('scripts')

        <script>
            var u_email = $("#user_email").text();
  window.intercomSettings = {
    app_id: "nz3wpa1l",
    //name: Jane Doe, // Full name
    email: u_email
  };
  </script>
<script>(function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',intercomSettings);}else{var d=document;var i=function(){i.c(arguments)};i.q=[];i.c=function(args){i.q.push(args)};w.Intercom=i;function l(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://widget.intercom.io/widget/nz3wpa1l';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);}if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})()</script>


    </body>
</html>