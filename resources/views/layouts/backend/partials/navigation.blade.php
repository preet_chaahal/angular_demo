<ul>

	<li class="{{ Request::is('tools/keywordresearch*') ? 'active open' : '' }}" >
		<a href="{{ URL::route('backend.tools.keywordresearch') }}" class="waves-effect {{ Request::is('tools/keywordresearch*') ? 'active' : '' }}">
			<i class="fa fa-search"></i>
			<span>Keyword Research</span>
		</a>
		<!-- <ul id="keyRes">
			<li class="{{ Request::is('tools/keywordresearch*') ? 'active' : '' }}">
				<a href="{{ URL::route('backend.tools.keywordresearch') }}">
					<i class="fa fa-arrow-circle-right"></i> Current Research Projects
				</a>
			</li>
			@if(Auth::user()->researchKeyword->count() > 0)
				@foreach(Auth::user()->researchKeyword as $search)
					{{$value=''}}
					@foreach($search->research as $eval)
						<span style="display: none">{{Request::is('tools/keywordresearch/evaluated/'.$eval->id)?$value=$eval->id:''}}</span>
					@endforeach
					<li id="searchLi{{$search->id}}" class="{{ Request::is('tools/keywordresearch/evaluated/'.$value) ||  Request::is('tools/keywordresearch/findResearch/'.$search->id) ? 'active open' : '' }}">
						<a href="{{ URL::route('backend.tools.keywordresearch.findResearch',$search->id) }}">
							<i class="fa fa-arrow-circle-right"></i> <span onclick="window.location.href='{{ URL::route('backend.tools.keywordresearch.findResearch',$search->id)}}'">{{ $search->keyword }}</span>
						</a>
						@if($search->research->count() > 0)
							<ul id="evalLi{{$search->id}}">
								@foreach($search->research as $eval)
									<li id="eval{{$eval->id}}" style="margin:0px 0px 0px 15px!important;" class="eval{{$search->id}}     {{ Request::is('tools/keywordresearch/evaluated/'.$eval->id) || Request::is('tools/keywordresearch/findResearch/'.$search->id) ? 'active open' : '' }}">
										<a href="{{ URL::route('backend.tools.keywordresearch.evaluated',$eval->id) }}">
											<i class="fa fa-arrow-circle-o-right"></i>{{ $eval->keyword }}
										</a>

										<div class="progress progress-xs"
											 style="margin:0px 15px 0px 30px!important;">
											<div class="progress-bar progress-bar-blue" role="progressbar" id="diff{{$eval->id}}"
												 aria-valuemin="0" aria-valuemax="100" style="width: {{$eval->difficulty}}%">
											</div>
										</div>
									</li>
								@endforeach
							</ul>
						@endif
					</li>
				@endforeach
			@endif
		</ul> -->
	</li>
	@if(Auth::user()->can('rank_checker'))
		<li>
			<a href="{{ URL::route('backend.tools.ranktracker') }}" class="waves-effect {{ Request::is('tools/ranktracker*') ? 'active' : '' }}">
				<i class="fa fa-line-chart"></i>
				<span>Rank Tracker</span>
			</a>
			<!-- <ul>
				<li class="{{ Request::is('tools/ranktracker*') ? 'active' : '' }}">
					<a href="{{ URL::route('backend.tools.ranktracker') }}">
						<i class="fa fa-arrow-circle-right"></i> Current Rank Projects
					</a>
				</li>
				@if(Auth::user()->toolRankChecks->count() > 0)
					@foreach(Auth::user()->toolRankChecks as $domain)
						@if($domain->group)
							<li id="rank{{$domain->group->idGroup}}">
								<a href="{{ URL::route('backend.tools.ranktracker.view', $domain->group->idGroup) }}">
									<i class="fa fa-arrow-circle-right"></i> {{ str_limit($domain->group->name,25) }}
								</a>
							</li>
						@endif
					@endforeach
				@endif
			</ul> -->
		</li>
	@endif
	@if(Auth::user()->can('brainstorm_tool'))
		<li>
			<a href="{{ URL::route('backend.tools.brainstorm') }}" class="waves-effect {{ Request::is('tools/brainstorm*') ? 'active' : '' }}">
				<i class="fa fa-code-fork fa-rotate-90"></i>
				<span>Keyword Brainstorming</span>
			</a>
			<!-- <ul id="brainstromItem">
				<li class="{{ Request::is('tools/brainstorm*') ? 'active' : '' }}">
					<a href="{{ URL::route('backend.tools.brainstorm') }}">
						<i class="fa fa-arrow-circle-right"></i> Current Brainstorm Projects
					</a>
				</li>
				@if(Auth::user()->brainstorms->count() > 0)
					@foreach(Auth::user()->brainstorms as $brainstorm)
						<li id="brainstormItem{{$brainstorm->id}}">
							<a href="{{ URL::route('backend.tools.brainstorm.view', $brainstorm->id) }}">
								<i class="fa fa-arrow-circle-right"></i> {{ $brainstorm->keyword }}
							</a>
						</li>
					@endforeach
				@endif
			</ul> -->
		</li>
	@endif

	@if(Auth::user()->is_admin)
		<li class="has_sub">
			<a href="#" class="waves-effect">
				<i class="fa fa-user"></i>
				<span>Administration</span>
			</a>

			<ul class="list-unstyled">
				<li class="{{ Request::is('admin/proxies*') ? 'active' : '' }}">
					<a href="{{ URL::route('admin.proxies','rank-tool-proxy') }}">
						<span>Proxy Setup</span>
					</a>
				</li>
				<li class="{{ Request::is('admin/tools/ranktracker*') ? 'active' : '' }}">
					<a href="{{ URL::route('admin.tools.ranktracker.index') }}">
						<span>Rank Tracker</span>
					</a>
				</li>
				<li class="{{ Request::is('admin/tools/brainstorm*') ? 'active' : '' }}">
					<a href="{{ URL::route('admin.tools.brainstorm.index') }}">
						<span>Keyword Brainstorm</span>
					</a>
				</li>
				<li class="{{ Request::is('admin/plan*') ? 'active' : '' }}">
					<a href="{{ URL::route('admin.plan','free') }}">
						<span>Plan Management</span>
					</a>
				</li>
				<li class="{{ Request::is('admin/users*') ? 'active' : '' }}">
					<a href="{{ URL::route('admin.users') }}">
						<span>User Management</span>
					</a>
				</li>
				<li class="{{ Request::is('admin/email-template/*') ? 'active' : '' }}">
					<a href="{{ URL::route('admin.email-template') }}">
						<span>Email Template Management</span>
					</a>
				</li>
				<li class="separator">
					&nbsp;
				</li>
				<li class="{{ Request::is('admin/reports*') ? 'active' : '' }}">
					<a href="{{ URL::route('admin.reports') }}">
						<span>Reports</span>
					</a>
				</li>
				<li class="{{ Request::is('admin/registration-management*') ? 'active' : '' }}">
					<a href="{{ URL::route('admin.registration.management') }}">
						<span>Registration Management</span>
					</a>
				</li>
			</ul>
		</li>
	@endif
</ul>
