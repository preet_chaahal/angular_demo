<div id="success" style="display: none" class="alert alert-success alert-dismissable fade in">
    <h4>Success!</h4>
    <ul>
        <li>Data saved successfully</li>
    </ul>
</div>
<div id="error" class="alert alert-danger alert-dismissable fade in" style="display: none">
    <h4>Whoops!</h4>

    <ul>
        <li id="errMsg">Fail</li>
    </ul>
</div>
<div id="errorsBox">
    @if($errors)
        @if($errors->count() > 0)
            <div class="alert alert-{{ session()->get('errorClass', 'lightred')}} alert-dismissable fade in">
                <h4>Whoops!</h4>
                <ul>
                    @foreach($errors->all() as $message)
                        <li>{!! $message !!}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    @endif
</div>
@if(Session::has('success'))
    <div class="alert alert-success alert-dismissable fade in">
        <h4>Success!</h4>

        <ul>
            @foreach(Session::get('success') as $message)
                <li>{{ $message }}</li>
            @endforeach
        </ul>
    </div>
@endif
@if(Session::has('nosuccess'))
    <div class="alert alert-danger alert-dismissable fade in">
        <h4>Whoops!</h4>

        <ul>
          @if(Session::get('nosuccess') && is_array(Session::get('nosuccess')))
            @foreach(Session::get('nosuccess') as $message)
                <li>{{ $message }}</li>
            @endforeach
          @endif
        </ul>
    </div>
@endif
@if(Session::has('apiError'))
    <div class="alert alert-danger alert-dismissable fade in">
        <h4>Whoops!</h4>

        <ul>
            {{Session::get('apiError')}}
        </ul>
    </div>
@endif
