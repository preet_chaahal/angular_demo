<!DOCTYPE>
<html>
    <head>
        <title>
            Keyword Research
        </title>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

        <style>
            .navbar-default {
                background: none;
                border-color: #e7e7e7;
                min-height: 100px;
            }

            .navbar-default .navbar-collapse, .navbar-default .navbar-form {
                margin-top: 20px;
            }
        </style>

        @yield('style')
    </head>
    <body>
        @include('layouts.partials.navigation.front')

        <div class="container">
            @yield('content')
        </div>

        <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>


        @yield('scripts')
    </body>
</html>