<!DOCTYPE>
<html>
    <head>
        <title>
            Keyword Research
        </title>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <link rel="stylesheet" href="{{ asset('assets/css/tile.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/alerts.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/tabs.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/buttons.css') }}">
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

        <style>
            .navbar-default {
                background: none;
                border-color: #e7e7e7;
                min-height: 100px;
            }

            .navbar-default .navbar-collapse, .navbar-default .navbar-form {
                margin-top: 20px;
            }
        </style>

        @yield('style')

        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-42623625-1', 'auto');
            ga('send', 'pageview');


            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
            n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
            document,'script','https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1346334148773436'); // Insert your pixel ID here.
            fbq('track', 'PageView');

        </script>
        
        <!-- Hotjar Tracking Code for keywordrevealer.com -->
        <script>
            (function(h,o,t,j,a,r){
                h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
                h._hjSettings={hjid:674052,hjsv:6};
                a=o.getElementsByTagName('head')[0];
                r=o.createElement('script');r.async=1;
                r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
                a.appendChild(r);
            })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
        </script>

        <!-- Global site tag (gtag.js) - Google AdWords: 805251890 -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=AW-805251890"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());
        
          gtag('config', 'AW-805251890');
        </script>


        <noscript>
            <img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=1346334148773436&ev=PageView&noscript=1"/>
        </noscript>

    </head>
    <body>
        @include('layouts.partials.navigation')

        <div class="container">
            @yield('content')
        </div>

        <script type="text/javascript">
            (function(){var t=document.createElement("script");t.type="text/javascript",t.async=!0,t.src='https://cdn.firstpromoter.com/fprom.js',t.onload=t.onreadystatechange=function(){var t=this.readyState;if(!t||"complete"==t||"loaded"==t)try{$FPROM.init("5o6x18vy",".keywordrevealer.com")}catch(t){}};var e=document.getElementsByTagName("script")[0];e.parentNode.insertBefore(t,e)})();</script>
        <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        <script src="{{ asset('assets/js/chart.min.js') }}"></script>
        <script src="{{ asset('assets/js/charts/LineAlt.js') }}"></script>

        @yield('scripts')
    </body>
</html>