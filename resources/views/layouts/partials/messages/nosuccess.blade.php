@if(Session::has('nosuccess'))
    <div class="alert alert-danger alert-dismissable fade in">
        <h4>Whoops!</h4>

        <ul>
            @foreach(Session::get('nosuccess') as $message)
                <li>{!! $message !!}</li>
            @endforeach
        </ul>
    </div>
@endif