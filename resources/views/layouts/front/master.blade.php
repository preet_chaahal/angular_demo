<!DOCTYPE html>
<html>
   <head>
    <meta charset="utf-8">
    <title>Keyword Revealer - Discover Low Competition Keywords Instantly</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Find hundreds of long tail keyword ideas in seconds with this amazing keyword research tool.">
    <meta name="author" content="">
    <meta name="robots" content="@yield('robots')">


      @yield('meta-og-section')
    <!--[if lt IE 9]>
    <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    
    <link href="{{ asset('assets/frontend/new_html/css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/frontend/new_html/css/style.css') }}" rel="stylesheet">    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    
    <!-- small header js start here -->
    <script src="{{ asset('assets/frontend/new_html/js/classie.js') }}"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>

    <script>
     function init() {
      window.addEventListener('scroll', function(e){
	   var distanceY = window.pageYOffset || document.documentElement.scrollTop,
		   shrinkOn = 100,
		   header = document.querySelector("header");
	   if (distanceY > shrinkOn) {
		   classie.add(header,"smaller");
	   } else {
		   if (classie.has(header,"smaller")) {
			  classie.remove(header,"smaller");
		   }
	   }
      });
    }
    window.onload = init();
   </script>
   <!-- small header js start here -->
   
   <!-- testimonial CSS start here -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.theme.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.transitions.css">
   <!-- testimonial CSS end here -->

       <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
       <link rel="shortcut icon" href="//www.keywordrevealer.com/favicon.ico">

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-42623625-1', 'auto');
        ga('send', 'pageview');

        !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
        n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
        document,'script','https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '1346334148773436'); // Insert your pixel ID here.
        fbq('track', 'PageView');

    </script>

    <!-- Hotjar Tracking Code for keywordrevealer.com -->
   <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:674052,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>

  <!-- Global site tag (gtag.js) - Google AdWords: 805251890 -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=AW-805251890"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
  
    gtag('config', 'AW-805251890');
  </script>

    <noscript>
        <img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=1346334148773436&ev=PageView&noscript=1"/>
    </noscript>
<!--
       <script type="text/javascript"> (function(){var t=document.createElement("script");t.type="text/javascript",t.async=!0,t.src='https://cdn.firstpromoter.com/fprom.js',t.onload=t.onreadystatechange=function(){var t=this.readyState;if(!t||"complete"==t||"loaded"==t)try{$FPROM.init("h4qvnjws",".keywordrevealer.com")}catch(t){}};var e=document.getElementsByTagName("script")[0];e.parentNode.insertBefore(t,e)})();</script>
-->
      <script type="text/javascript">
       (function(){var t=document.createElement("script");t.type="text/javascript",t.async=!0,t.src='https://cdn.firstpromoter.com/fprom.js',t.onload=t.onreadystatechange=function(){var t=this.readyState;if(!t||"complete"==t||"loaded"==t)try{$FPROM.init("rxqscow8",".kwrev.com")}catch(t){}};var e=document.getElementsByTagName("script")[0];e.parentNode.insertBefore(t,e)})();</script>

        @yield('head-script')
  </head>
<body>

@yield('content')

<div class="copy">
 <div class="container">
  <div class="row">
   <div class="col-md-6 col-sm-5">
    <p>Copyrights &copy; Keyword Revealer 2018. All Rights Reserved.</p>
   </div><!-- col end here -->
   
   <div class="col-md-6 col-sm-7">
    <ul>
     <li><a href="{{ URL::route('front.terms') }}">Terms & Conditions</a></li>
     <li><a href="{{ URL::route('front.privacy') }}">Privacy Policy</a></li>
     <li><a href="{{ URL::route('front.contact') }}">Contact</a></li>
     <li><a href="https://affiliate.keywordrevealer.com">Affiliate Program</a></li>
     <!--<li><a href="https://keywordrevealer.postaffiliatepro.com/affiliates/">Affiliate Program</a></li>-->
    </ul><!-- ul end here -->
   </div><!-- col end here -->  
  </div><!-- row end here -->
 </div><!-- container end here -->
</div><!-- copy end here -->





@if(Route::currentRouteName() == 'front.pages.landing' || Route::currentRouteName() == 'front.pages.landing.pricing')
  <link rel="stylesheet" type="text/css" href="{{ asset('assets/frontend/css/landing.css') }}">
  <script type="text/template" id="counter-container-template">
    <div class="time <%= label %>">
      <span class="count curr top"><%= curr %></span>
      <span class="count next top"><%= next %></span>
      <span class="count next bottom"><%= next %></span>
      <span class="count curr bottom"><%= curr %></span>
      <span class="label"><%= label.length < 6 ? label : label.substr(0, 3)  %></span>
    </div>
  </script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.4/lodash.min.js"></script>
  <script type="text/javascript" src="{{ asset('assets/frontend/js/jquery.countdown.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('assets/frontend/js/landing.js') }}"></script>
@endif

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<!--<script src="{{ asset('assets/frontend/new_html/js/jquery.js') }}"></script>
<script src="{{ asset('assets/frontend/new_html/js/bootstrap.js') }}"></script>-->
<script src="{{ asset('assets/frontend/new_html/js/new_main.js') }}"></script>
<!--<script src="{{ asset('assets/frontend/js/vue.min.js') }}"></script>
<script src="{{ asset('assets/frontend/js/main.js') }}"></script>-->
<!--Post_Affiliate_Pro<script type="text/javascript" id="pap_x2s6df8d" src="https://keywordrevealer.postaffiliatepro.com/scripts/vdzfn3n4jm8"></script>
<script type="text/javascript">
PostAffTracker.setAccountId('default1');
try {
PostAffTracker.track();
} catch (err) { }
</script>-->
@yield('script')

<!-- testimonial js start here -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.js"></script>
<script> 
$(document).ready(function(){
  $("#testimonial-slider").owlCarousel({
	items:1,
	itemsDesktop:[1000,1],
	itemsDesktopSmall:[979,1],
	itemsTablet:[768,1],
	pagination:true,
	navigation:false,
	navigationText:["",""],
	slideSpeed:1000,
	singleItem:true,
	transitionStyle:"fade",
	autoPlay:true
  });
});
</script>
<!-- testimonial js end here -->

<script type='text/javascript'>
$(document).ready(function () {

$(".plan-view").click(function(){
if($(this).hasClass("fa-chevron-down"))
{
   $(this).removeClass("fa-chevron-down");   
   $(this).addClass("fa-chevron-up");
}
else
{
   $(this).removeClass("fa-chevron-up");   
   $(this).addClass("fa-chevron-down");
}
});
	
$(".accordion_head").click(function () {
if ($('.accordion_body').is(':visible')) {
$(".accordion_body").slideUp(300);
 }
if ($(this).next(".accordion_body").is(':visible')) {
$(this).next(".accordion_body").slideUp(300);
} else {
$(this).next(".accordion_body").slideDown(300);
}
});
});
</script>

<script>
  window.intercomSettings = {
    app_id: "nz3wpa1l"
  };
</script>
<script>(function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',intercomSettings);}else{var d=document;var i=function(){i.c(arguments)};i.q=[];i.c=function(args){i.q.push(args)};w.Intercom=i;function l(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://widget.intercom.io/widget/nz3wpa1l';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);}if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})()</script>

@yield('fp-script')

</body>

</html>